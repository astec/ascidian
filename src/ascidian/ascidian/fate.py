##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Sam 21 oct 2023 18:58:51 CEST
#
##############################################################
#
#
#
##############################################################

import astec.utils.common as common

import ascidian.ascidian.name as aname

#
#
#
#
#

monitoring = common.Monitoring()


########################################################################################
#
#
#
########################################################################################


cell_fate2 = {
    'Anterior Endoderm': ["a7.0001", "a7.0002", "a7.0005"],
    'Posterior Endoderm': ["b7.0001", "b7.0002", "b9.0034"],
    'germ line': ["b7.0006"],
    'Mesoderm 1 Notochord': ["a7.0003", 'a7.0007'],
    'Mesoderm 2 Notochord': ["b8.0006"],
    'Mesoderm Trunk Lateral Cell': ['a7.0006'],
    'Mesoderm Trunk ventral Cell': ['b7.0005'],
    'Mesoderm First Muscle': ['b7.0004', 'b7.0008'],
    'Mesoderm Second Muscle': ['a9.0031', 'b9.0033'],
    'Mesoderm Mesenchyme': ['b7.0007', 'b8.0005'],
    'Posterior ventral Neural Plate': ["a7.0004"],
    'Anterior + Dorsal Neural Plate': ['a7.0009', 'a7.0010', 'b8.0019'],
    'Lateral Neural Plate': ['a7.0013', 'a8.0015', 'a9.0032'],
    'Trunk Epidermis': ['a7.0011', 'a7.0012', 'a7.0014', 'a7.0015', 'a7.0016'],
    'Midline Tail Epidermis': ['b8.0020', 'b8.0018', 'b9.0041', 'b8.0027', 'b9.0056', 'b9.0062', 'b9.0064'],
    'Mediolateral Tail Epidermis':
        ['b8.0024', 'b9.0045', 'b9.0042', 'b9.0043', 'b9.0049', 'b9.0055', 'b9.0061', 'b9.0063'],
    'Lateral Tail Epidermis': ['b9.0044', 'b8.0026', 'b9.0050', 'b9.0046', 'b8.0029', 'b8.0030']
}


cell_fate3 = {
    'Head Endoderm': ["a6.0001", "a7.0001", "a7.0002", "a7.0005", "b7.0001", "b8.0003"],
    '1st Endodermal Lineage': ["b8.0004"],
    '2nd Endodermal Lineage': ["b9.0034"],
    '1st Lineage, Notochord': ["a7.0003", 'a7.0007'],
    '2nd Lineage, Notochord': ["b8.0006"],
    'Trunk Lateral Cell': ['a7.0006'],
    'Trunk Ventral Cell': ["b7.0005"],
    'Mesenchyme': ['b7.0007', 'b8.0005'],
    '1st Lineage, Tail Muscle': ['b7.0004', 'b7.0008'],
    '2nd Lineage, Tail Muscle': ['a9.0031', 'b9.0033'],
    'Anterior Dorsal Neural Plate': ['a7.0013'],
    'Anterior Ventral Neural Plate': ["a6.0005", "a7.0009", "a7.0010"],
    'Posterior Dorsal Neural Plate': ['b8.0019'],
    'Posterior Lateral Neural Plate': ['a8.0015', 'a9.0032'],
    'Posterior Ventral Neural Plate': ['a7.0004'],
    'Head Epidermis': ['a6.0006', 'a6.0008', 'a7.0014', 'a7.0015', 'a7.0016', 'a7.0011', 'a7.0012'],
    'Tail Epidermis':
    ['b7.0011', 'b7.0012', 'b7.0013', 'b7.0014', 'b7.0015', 'b7.0016' 'b9.0044', 'b8.0026', 'b9.0050', 'b9.0046',
     'b7.0015', 'b8.0029', 'b8.0030', 'b8.0024', 'b9.0045', 'b9.0042', 'b9.0043', 'b9.0049', 'b9.0055', 'b9.0061',
     'b9.0063', 'b8.0020', 'b8.0018', 'b9.0041', 'b8.0027', 'b9.0056', 'b9.0062', 'b9.0064'],
    'Germ Line': ["b7.0006"]
}


# new fate (fate4) for visualisation MorphoNet and Tulip tree
#
# Jeu 20 fév 2025 17:22:51 CET:
# - tissue fate of b9.0033 has changed from "2nd Lineage, Neural Plate" to "Tail-tip Cell"
#

cell_fate4 = {
    'Anterior Head Endoderm': ['a6.0001', 'a7.0005'],
    'Posterior Head Endoderm': ['b7.0001', 'b8.0003'],
    'Endodermal Strand': ['b8.0004'],
    'Tail-tip Cells': ['b9.0033'],
    '1st Lineage, Notochord': ['a7.0003', 'a7.0007'],
    '2nd Lineage, Notochord': ['b8.0006'],
    'Trunk Lateral Cells': ['a7.0006'],
    'Trunk Ventral Cells': ['b9.0018', 'b9.0020'],
    'Mesenchyme': ['b7.0007', 'b8.0005'],
    '1st Lineage, Tail Muscle': ['b7.0004', 'b7.0008', 'b9.0017', 'b9.0019'],
    '2nd Lineage, Tail Muscle': ['a9.0031', 'b9.0034'],
    'Anterior Dorsal Neural Plate': ['a7.0013'],
    'Anterior Ventral Neural Plate': ['a6.0005'],
    'Posterior Dorsal Neural Plate': ['b8.0019'],
    'Posterior Lateral Neural Plate': ['a8.0015', 'a9.0032'],
    'Posterior Ventral Neural Plate': ['a7.0004'],
    'Head Epidermis': ['a6.0006', 'a6.0008', 'a7.0014'],
    'Lateral Tail Epidermis': ['b7.0015', 'b8.0026', 'b9.0044', 'b9.0047', 'b9.0050'],
    'Medio-Lateral Tail Epidermis':
    ['b8.0023', 'b9.0042', 'b9.0043', 'b9.0048', 'b9.0049', 'b9.0055', 'b9.0061', 'b9.0063'],
    'Midline Tail Epidermis':
    ['b8.0018', 'b8.0020', 'b8.0027', 'b9.0041', 'b9.0056', 'b9.0062', 'b9.0064'],
    'Germ Line': ['b7.0006']
}


def _build_dict_indexed_by_name(lastgeneration=9):
    """
    Build a dictionary whose keys are cell names (without the trailing ['_','*'])
    up to the requested generation. Values are set to None.

    Parameters
    ----------
    lastgeneration: int

    Returns
    -------

    """
    dict_name = {'a2.0001': None, 'b2.0001': None}
    #
    # generate names for each generation
    #
    for g in range(2, lastgeneration+1):
        names = list(dict_name.keys())
        for n in names:
            # pick cell from generation g,
            # ie do not process names from older generations
            if int(n.split('.')[0][1:]) != g:
                continue
            daughters = aname.get_daughter_names(n + '_')
            for d in daughters:
                dict_name[d[:-1]] = None
    return dict_name


def build_dict_fate(cell_fate=None, lastgeneration=9):
    proc = "build_dict_fate"
    # build a dictionary with names as key and None values
    dict_fate = _build_dict_indexed_by_name(lastgeneration=lastgeneration)

    name_by_generation = {}
    for name in dict_fate:
        g = int(name.split('.')[0][1:])
        name_by_generation[g] = name_by_generation.get(g, []) + [name]
    generations = name_by_generation.keys()
    generations = sorted(generations)

    #
    # give fate to names
    #
    local_cell_fate = cell_fate
    if local_cell_fate is None:
        local_cell_fate = cell_fate4

    #
    # reverse dictionary name: fate
    #
    for fate in local_cell_fate:
        for name in local_cell_fate[fate]:
            if name not in dict_fate:
                continue
            if dict_fate[name] is None:
                dict_fate[name] = [fate]
            else:
                dict_fate[name] += [fate]

    #
    # forward propagation
    # fate[daughter] = fate[mother]
    #
    for g in generations:
        for name in name_by_generation[g]:
            if dict_fate[name] is None:
                continue
            daughters = aname.get_daughter_names(name + '_')
            for d in daughters:
                if d[:-1] not in dict_fate:
                    continue
                if dict_fate[d[:-1]] is not None:
                    continue
                dict_fate[d[:-1]] = dict_fate[name]

    #
    # backward propagation
    # fate[mother] = fate[daughter(s)]
    #
    generations = sorted(generations, reverse=True)
    for g in generations:
        for name in name_by_generation[g]:
            if dict_fate[name] is not None:
                continue
            fate = []
            daughters = aname.get_daughter_names(name + '_')
            for d in daughters:
                if d[:-1] not in dict_fate:
                    continue
                if dict_fate[d[:-1]] is None:
                    continue
                if isinstance(dict_fate[d[:-1]], str):
                    fate += [dict_fate[d[:-1]]]
                elif isinstance(dict_fate[d[:-1]], list):
                    fate += dict_fate[d[:-1]]
                else:
                    msg = ":type '" + str(type(dict_fate[d[:-1]])) + "' of '" + str(d[:-1]) + "' not handled yet"
                    monitoring.to_log_and_console(str(proc) + msg)
            fate = list(set(fate))
            if len(fate) == 1:
                dict_fate[name] = fate[0]
            elif len(fate) > 1:
                dict_fate[name] = fate

    return dict_fate


def _set_fate_from_names(d, keyfate, cell_fate):

    #
    # build a dictionary of fates indexed by name
    #
    dict_fate = build_dict_fate(cell_fate, lastgeneration=9)
    d[keyfate] = {}
    for c in d['cell_name']:
        if dict_fate[d['cell_name'][c][:-1]] is None:
            continue
        d[keyfate][c] = dict_fate[d['cell_name'][c][:-1]]

    #
    # forward propagation of fates (for cells with no name)
    #
    lineage = d['cell_lineage']
    cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
    cells = sorted(cells)

    for mother in cells:
        if mother not in d[keyfate]:
            continue
        if mother not in lineage:
            continue
        for c in lineage[mother]:
            if c in d[keyfate]:
                continue
            d[keyfate][c] = d[keyfate][mother]

    return d


def set_fate_from_names(d, keyfate="cell_tissue_fate", fate=4):
    proc = "set_fate_from_names"

    if fate == 4:
        cell_fate = cell_fate4
        if keyfate is None:
            keyfate = "cell_tissue_fate"
    else:
        monitoring.to_log_and_console(proc + ": fate index '" + str(fate) + "' not handled")
        return

    # clean properties from previous fates
    for f in ['cell_fate', 'cell_fate2', 'cell_fate3', 'cell_fate4', 'cell_tissue_fate']:
        if f in d:
            del d[f]

    d = _set_fate_from_names(d, keyfate, cell_fate)

    return d


def _set_color_from_fate(d, keyfate="cell_tissue_fate", colormap_version=2009):
    proc = "_set_color_from_fate"

    color_fate_2009 = {"Anterior Head Endoderm": 1, "Posterior Head Endoderm": 27,
                       "Endodermal Strand": 40, "2nd Lineage, Neural Plate": 28,
                       "Tail-tip Cells": 240,
                       "1st Lineage, Notochord": 78, "2nd Lineage, Notochord": 199,
                       "Trunk Lateral Cells": 62, "Trunk Ventral Cells": 72, "Mesenchyme": 63,
                       "1st Lineage, Tail Muscle": 135, "2nd Lineage, Tail Muscle": 110,
                       "Anterior Dorsal Neural Plate": 81, "Anterior Ventral Neural Plate": 123,
                       "Posterior Dorsal Neural Plate": 241, "Posterior Lateral Neural Plate": 75,
                       "Posterior Ventral Neural Plate": 58, "Head Epidermis": 76,
                       "Lateral Tail Epidermis": 61, "Medio-Lateral Tail Epidermis": 41, "Midline Tail Epidermis": 86,
                       "Germ Line": 99}

    if colormap_version == 2009:
        colormap = color_fate_2009
        keycolormap = 'morphonet_selection_tissuefate_lemaire_2009'
    else:
        monitoring.to_log_and_console(proc + ": colormap version '" + str(colormap_version) + "' not handled")
        return

    if keycolormap in d:
        del d[keycolormap]
    d[keycolormap] = {}

    for c in d[keyfate]:
        if isinstance(d[keyfate][c], str):
            if d[keyfate][c] not in colormap:
                monitoring.to_log_and_console(proc + ": fate '" + str(d[keyfate][c]) + "' not handled in color map")
                continue
            d[keycolormap][c] = [colormap[d[keyfate][c]]]
        elif isinstance(d[keyfate][c], list):
            for f in d[keyfate][c]:
                if not isinstance(f, str):
                    msg = ":type '" + str(f) + "' found in d[keyfate][" + str(c) + "]"
                    msg += "not handled yet"
                    monitoring.to_log_and_console(str(proc) + msg)
                    continue
                if f not in colormap:
                    monitoring.to_log_and_console(
                        proc + ": fate '" + str(f) + "' not handled in color map")
                    continue
                if c not in d[keycolormap]:
                    d[keycolormap][c] = [colormap[f]]
                else:
                    d[keycolormap][c].append(colormap[f])
        else:
            msg = ":type '" + str(d[keyfate][c]) + "' of d[keyfate][" + str(c) + "]"
            msg += "not handled yet"
            monitoring.to_log_and_console(str(proc) + msg)

    return d


def set_color_from_fate(d, keyfate="cell_tissue_fate"):
    for f in ['tissuefate_map']:
        if f in d:
            del d[f]
    d = _set_color_from_fate(d, keyfate=keyfate, colormap_version=2009)
    return d


def _has_single_fate(cell_fate, fate):
    proc = "_has_single_fate"
    if fate is not None:
        test_fate = False
        if isinstance(fate, str):
            if cell_fate.find(fate) > -1:
                test_fate = True
        elif isinstance(fate, list):
            for f in fate:
                if cell_fate.find(f) > -1:
                    test_fate = True
        else:
            msg = ":type '" + str(type(fate)) + "' of fate not handled yet, return True"
            monitoring.to_log_and_console(str(proc) + msg)
            return True
        return test_fate
    return True


def has_fate(cell_fate, fate=None, nonfate=None):
    proc = "has_fate"
    if cell_fate is None:
        return False
    if fate is None and nonfate is None:
        return True

    if fate is not None:
        test_fate = False
        if isinstance(cell_fate, str):
            if _has_single_fate(cell_fate, fate) is True:
                test_fate = True
        elif isinstance(cell_fate, list):
            for f in cell_fate:
                if _has_single_fate(f, fate) is True:
                    test_fate = True
        else:
            msg = ":type '" + str(type(cell_fate)) + "' of cell_fate not handled yet"
            monitoring.to_log_and_console(str(proc) + msg)
    else:
        test_fate = True

    if nonfate is not None:
        test_nonfate = True
        if isinstance(cell_fate, str):
            if _has_single_fate(cell_fate, nonfate) is True:
                test_nonfate = False
        elif isinstance(cell_fate, list):
            for f in cell_fate:
                if _has_single_fate(f, nonfate) is True:
                    test_nonfate = False
        else:
            msg = ":type '" + str(type(cell_fate)) + "' of cell_fate not handled yet"
            monitoring.to_log_and_console(str(proc) + msg)
    else:
        test_nonfate = True

    return test_fate and test_nonfate

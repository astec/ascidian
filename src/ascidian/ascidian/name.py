##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#           Haydar Jammoul
#       Creation date:
#           Lun 24 jui 2024 14:13:43 CEST
#
##############################################################
#
#
#
##############################################################

from functools import cmp_to_key


def is_name_valid(name):
    if name is None:
        return False
    if isinstance(name, str) is False:
        return False
    s = name.split('.')
    if len(s) != 2:
        return False
    if s[0][0] != 'a' and s[0][0] != 'A' and s[0][0] != 'b' and s[0][0] != 'B':
        return False
    if not s[0][1:].isdigit():
        return False
    if s[1][-1] != '*' and s[1][-1] != '_':
        return False
    if not s[1][:-1].isdigit():
        return False
    return True


def get_generation_name(generation=7):
    #
    # for stage p there is 2^(p-1) cells
    # ie 2^6 = 64 cells for stage 7
    #
    # names are of the form [a,b].n[*,_]
    # number of cells is to be divided by 2 to account for [a, b],
    # and by 2 to account for [*,_], thus for stage p,
    # n goes from 1 to 2^(p-3)
    #
    #
    names = []
    for i in range(1, pow(2, generation-3) + 1):
        p = '{:d}.{:04d}'.format(generation, i)
        names += ['a' + p + '_', 'a' + p + '*', 'b' + p + '_', 'b' + p + '*']
    names = sorted(names)
    return set(names)


##############################################################
#
#
#
##############################################################


def get_daughter_names(name):
    #
    # build daughter names from parent name
    #
    # anterior or posterior character 'a' or 'b'
    # stage (round of division)
    # '.'
    # p value (cell index)
    # left or right character '_' or '*'
    #

    # fake returns
    if name == 'background':
        return ['background', 'background']
    if name == 'other-half':
        return ['other-half', 'other-half']

    abvalue = name.split('.')[0][0]
    stage = name.split('.')[0][1:]
    p = name.split('.')[1][0:4]
    lrvalue = name.split('.')[1][4]
    #
    # build daughter names
    #
    daughters = [abvalue + str(int(stage) + 1) + "." + '{:0{width}d}'.format(2 * int(p) - 1, width=4) + lrvalue,
                 abvalue + str(int(stage) + 1) + "." + '{:0{width}d}'.format(2 * int(p), width=4) + lrvalue]
    # print("name = " + str(name) + " -> daughter names = " + str(daughters))
    return daughters


def get_mother_name(name):
    #
    # build daughter names from parent name
    #
    # anterior or posterior character 'a' or 'b'
    # stage (round of division)
    # '.'
    # p value (cell index)
    # left or right character '_' or '*'
    #

    # fake returns
    if name == 'background':
        return 'background'
    if name == 'other-half':
        return 'other-half'

    abvalue = name.split('.')[0][0]
    stage = name.split('.')[0][1:]
    p = name.split('.')[1][0:4]
    lrvalue = name.split('.')[1][4]
    #
    # build parent names
    #
    if int(stage) == 0:
        return None
    parent = abvalue + str(int(stage) - 1) + "."
    if int(p) % 2 == 1:
        parent += '{:0{width}d}'.format((int(p) + 1) // 2, width=4)
    else:
        parent += '{:0{width}d}'.format(int(p) // 2, width=4)
    parent += lrvalue
    # print("name = " + str(name) + " -> parent name = " + str(parent))
    return parent


def get_ancestor_name(name, generation):
    if name == 'background':
        return 'background'
    if name == 'other-half':
        return 'other-half'
    g = int(name.split('.')[0][1:])
    if int(generation) > g:
        return None
    if int(generation) == g:
        return name
    n = name
    for i in range(g-int(generation)):
        n = get_mother_name(n)
        return n


def get_sister_name(name):
    sister_names = get_daughter_names(get_mother_name(name))
    sister_names.remove(name)
    return sister_names[0]


def get_symmetric_name(name):
    symname = name[:-1]
    if name[-1] == '*':
        symname += '_'
    elif name[-1] == '_':
        symname += '*'
    else:
        return None
    return symname


##############################################################
#
#
#
##############################################################


def _names_64_cells():
    n64 = []
    for i in range(1, 17):
        p = '{:04d}'.format(i)
        n64 += ['a7.' + p + '_', 'a7.' + p + '*', 'b7.' + p + '_', 'b7.' + p + '*']
    n64 = sorted(n64)
    return set(n64)


def _names_76_cells():
    n64 = _names_64_cells()
    n76 = []
    for n in n64:
        #
        # a7.[3,4,7,8] and b7.[3,4] are divided
        #
        if n.split('.')[0][0] == 'a':
            if int(n.split('.')[1][0:4]) not in [3, 4, 7, 8]:
                n76 += [n]
                continue
        elif n.split('.')[0][0] == 'b':
            if int(n.split('.')[1][0:4]) not in [3, 4]:
                n76 += [n]
                continue
        n76 += get_daughter_names(n)
    return set(n76)


def _names_112_cells():
    n64 = _names_64_cells()
    n112 = []
    for n in n64:
        #
        # a7.[1,2,5] and b7.[1,2,5,6,7] are not divided
        #
        if n.split('.')[0][0] == 'a':
            if int(n.split('.')[1][0:4]) in [1, 2, 5]:
                n112 += [n]
                continue
        elif n.split('.')[0][0] == 'b':
            if int(n.split('.')[1][0:4]) in [1, 2, 5, 6, 7]:
                n112 += [n]
                continue
        n112 += get_daughter_names(n)
    return set(n112)


def ascidian_cell_names(n=64):
    if n == 64:
        return _names_64_cells()
    if n == 76:
        return _names_76_cells()
    if n == 112:
        return _names_112_cells()
    return None


##############################################################
#
#
#
##############################################################

def set_color_from_name(d):

    keycolormap = 'morphonet_selection_name_map'
    d[keycolormap] = {}

    name = set(d['cell_name'].values())
    stage = sorted(set([int(n.split('.')[0][1:]) for n in name]))

    #
    # build color indices for stages/generations present in dictionary
    # keys are names without [_,*]
    #
    colors  = {}
    for s in stage:
        #
        # from get_generation_name()
        #
        n = pow(2, s - 3) + 1
        gname = ['a{:d}.{:04d}'.format(s, i) for i in range(1, n)] + ['b{:d}.{:04d}'.format(s, i) for i in range(1, n)]
        i = 1
        for g in gname:
            colors[g] = i
            i += 1
            if i > 255:
                i = 1

    #
    # apply color map
    #
    for c, n in d['cell_name'].items():
        d[keycolormap][c] = colors[n[:-1]]
    return d


##############################################################
#
#
#
##############################################################


def _epidermis_names_8_cells():
    name = ['a5.0003', 'a5.0004', 'b5.0003', 'b5.0004']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _epidermis_names_14_cells():
    name = ['a6.0006', 'a6.0007', 'a6.0008', 'b6.0005', 'b6.0006', 'b6.0007', 'b6.0008']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _epidermis_names_26_cells():
    name = ['a7.0011', 'a7.0012', 'a7.0014', 'a7.0015', 'a7.0016', 'b7.0009', 'b7.0010', 'b7.0011', 'b7.0012', 
            'b7.0013', 'b7.0014', 'b7.0015', 'b7.0016']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _epidermis_names_48_cells():
    name = ['a8.0021', 'a8.0022', 'a8.0023', 'a8.0024', 'a8.0027', 'a8.0028', 'a8.0029', 'a8.0030', 'a8.0031',
            'a8.0032', 'b8.0018', 'b8.0020', 'b8.0021', 'b8.0022', 'b8.0023', 'b8.0024', 'b8.0025', 'b8.0026',
            'b8.0027', 'b8.0028', 'b8.0029', 'b8.0030', 'b8.0031', 'b8.0032']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _epidermis_names_96_cells():
    name = ['a9.0041', 'a9.0042', 'a9.0043', 'a9.0044', 'a9.0045', 'a9.0046', 'a9.0047', 'a9.0048', 'a9.0053',
            'a9.0054', 'a9.0055', 'a9.0056', 'a9.0057', 'a9.0058', 'a9.0059', 'a9.0060', 'a9.0061', 'a9.0062',
            'a9.0063', 'a9.0064', 'b9.0035', 'b9.0036', 'b9.0039', 'b9.0040', 'b9.0041', 'b9.0042', 'b9.0043',
            'b9.0044', 'b9.0045', 'b9.0046', 'b9.0047', 'b9.0048', 'b9.0049', 'b9.0050', 'b9.0051', 'b9.0052',
            'b9.0053', 'b9.0054', 'b9.0055', 'b9.0056', 'b9.0057', 'b9.0058', 'b9.0059', 'b9.0060', 'b9.0061',
            'b9.0062', 'b9.0063', 'b9.0064']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _epidermis_names_192_cells():
    name = ['a10.0081', 'a10.0082', 'a10.0083', 'a10.0084', 'a10.0085', 'a10.0086', 'a10.0087', 'a10.0088',
            'a10.0089', 'a10.0090', 'a10.0091', 'a10.0092', 'a10.0093', 'a10.0094', 'a10.0095', 'a10.0096',
            'a10.0105', 'a10.0106', 'a10.0107', 'a10.0108', 'a10.0109', 'a10.0110', 'a10.0111', 'a10.0112',
            'a10.0113', 'a10.0114', 'a10.0115', 'a10.0116', 'a10.0117', 'a10.0118', 'a10.0119', 'a10.0120',
            'a10.0121', 'a10.0122', 'a10.0123', 'a10.0124', 'a10.0125', 'a10.0126', 'a10.0127', 'a10.0128',
            'b10.0069', 'b10.0070', 'b10.0071', 'b10.0072', 'b10.0077', 'b10.0078', 'b10.0079', 'b10.0080',
            'b10.0081', 'b10.0082', 'b10.0083', 'b10.0084', 'b10.0085', 'b10.0086', 'b10.0087', 'b10.0088',
            'b10.0089', 'b10.0090', 'b10.0091', 'b10.0092', 'b10.0093', 'b10.0094', 'b10.0095', 'b10.0096',
            'b10.0097', 'b10.0098', 'b10.0099', 'b10.0100', 'b10.0101', 'b10.0102', 'b10.0103', 'b10.0104',
            'b10.0105', 'b10.0106', 'b10.0107', 'b10.0108', 'b10.0109', 'b10.0110', 'b10.0111', 'b10.0112',
            'b10.0113', 'b10.0114', 'b10.0115', 'b10.0116', 'b10.0117', 'b10.0118', 'b10.0119', 'b10.0120',
            'b10.0121', 'b10.0122', 'b10.0123', 'b10.0124', 'b10.0125', 'b10.0126', 'b10.0127', 'b10.0128']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _endoderm_12_cells():
    name = ['a7.0001', 'a7.0002', 'a7.0005', 'b7.0001', 'b7.0002', 'b7.0009']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _endoderm_22_cells():
    name = ['a8.0001', 'a8.0002', 'a8.0003', 'a8.0004', 'a8.0009', 'a8.0010', 'b8.0001', 'b8.0002', 'b8.0003',
            'b8.0004', 'b8.0017']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _meso_20_cells():
    name = ['a7.0003', 'a7.0006', 'a7.0007', 'a7.0008', 'b7.0003', 'b7.0004', 'b7.0005', 'b7.0007', 'b7.0008',
            'b7.0009']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _meso_28_cells():
    name = ['a7.0006', 'a8.0005', 'a8.0006', 'a8.0013', 'a8.0014', 'a8.0016', 'b7.0005', 'b7.0007', 'b7.0008',
            'b7.0009', 'b8.0005', 'b8.0006', 'b8.0007', 'b8.0008']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _meso_32_cells():
    name = ['a8.0005', 'a8.0006', 'a8.0011', 'a8.0012', 'a8.0013', 'a8.0014', 'a8.0016', 'b7.0005', 'b7.0007',
            'b8.0005', 'b8.0006', 'b8.0007', 'b8.0008', 'b8.0015', 'b8.0016', 'b8.0017']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _meso_56_cells():
    name = ['a9.0009', 'a9.0010', 'a9.0011', 'a9.0012', 'a9.0021', 'a9.0022', 'a9.0023', 'a9.0024', 'a9.0025',
            'a9.0026', 'a9.0027', 'a9.0028', 'a9.0031', 'b8.0009', 'b8.0010', 'b8.0013', 'b8.0014', 'b8.0015',
            'b8.0016', 'b9.0009', 'b9.0010', 'b9.0011', 'b9.0012', 'b9.0013', 'b9.0014', 'b9.0015', 'b9.0016',
            'b9.0033']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _ns_12_cells():
    name = ['a7.0004', 'a7.0008', 'a7.0009', 'a7.0010', 'a7.0013', 'b7.0010']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _ns_16_cells():
    name = ['a7.0009', 'a7.0010', 'a7.0013', 'a8.0007', 'a8.0008', 'a8.0015', 'a8.0016', 'b7.0010']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _ns_22_cells():
    name = ['a8.0007', 'a8.0008', 'a8.0015', 'a8.0016', 'a8.0017', 'a8.0018', 'a8.0019', 'a8.0020', 'a8.0025',
            'a8.0026', 'b8.0019']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _ns_28_cells():
    name = ['a8.0017', 'a8.0018', 'a8.0019', 'a8.0020', 'a8.0025', 'a8.0026', 'a9.0013', 'a9.0014', 'a9.0015',
            'a9.0016', 'a9.0029', 'a9.0030', 'a9.0032', 'b8.0019']
    return set([n + '_' for n in name] + [n + '*' for n in name])


def _ns_42_cells():
    name = ['a9.0013', 'a9.0014', 'a9.0015', 'a9.0016', 'a9.0029', 'a9.0030', 'a9.0032', 'a9.0033', 'a9.0034',
            'a9.0035', 'a9.0036', 'a9.0037', 'a9.0038', 'a9.0039', 'a9.0040', 'a9.0049', 'a9.0050', 'a9.0051',
            'a9.0052', 'b9.0037', 'b9.0038']
    return set([n + '_' for n in name] + [n + '*' for n in name])
# NS at 46 there's a plateau but there are heterochronies so not all cells in common


def ascidian_epidermis_cell_names(n=48):
    print("Obsolete?! Use \"ascidian_tissue_cell_names(tissue='epidermis', n=...)\" instead")
    return ascidian_tissue_cell_names(tissue='epidermis', n=n)


def ascidian_tissue_cell_names(tissue='epidermis', n=48):
    """
    Parameters
    ----------
    tissue : tissue type
    n : nb of cells in tissue

    Returns
    -------
    list of of cells names in tissue at the considered stage in reference

    """
    if tissue.lower() == 'epid.' or tissue.lower() == 'epiderm':
        if n == 8:
            return _epidermis_names_8_cells()
        if n == 14:
            return _epidermis_names_14_cells()
        if n == 26:
            return _epidermis_names_26_cells()
        if n == 48:
            return _epidermis_names_48_cells()
        if n == 96:
            return _epidermis_names_96_cells()
        if n == 192:
            return _epidermis_names_192_cells()
    elif tissue.lower() == 'end.' or tissue.lower() == 'endoderm':
        if n == 12:
            return _endoderm_12_cells()
        if n == 22:
            return _endoderm_22_cells()
    elif tissue.lower() == 'mes.' or tissue.lower() == 'mesoderm':
        if n == 20:
            return _meso_20_cells()
        if n == 28:
            return _meso_28_cells()
        if n == 32:
            return _meso_32_cells()
        if n == 56:
            return _meso_56_cells()
    elif tissue.lower() == 'ns':
        if n == 12:
            return _ns_12_cells()
        if n == 16:
            return _ns_16_cells()
        if n == 22:
            return _ns_22_cells()
        if n == 28:
            return _ns_28_cells()
        if n == 42:
            return _ns_42_cells()
    return None


def get_cell_name_parts(cell_name):
    """
    Returns part of the cell name
    """
    cell_name_splitted = cell_name.split('.')
    ant_post = cell_name_splitted[0][0]
    cell_gen = int(cell_name_splitted[0][1:])
    cell_q = cell_name_splitted[1].split('*')
    cell_dir = '*'
    if len(cell_q) == 1:
        # The name ends with '_': it's a cell from the right symmetry
        cell_dir = '*'
        cell_q = cell_name_splitted[1].split('_')
    cell_q = int(cell_q[0])
    return ant_post, cell_gen, cell_q, cell_dir


##############################################################
#
#
#
##############################################################


def _cmp_names(n1, n2):
    # right cells, then left cells
    # cells are then sorted
    #   - 'a' first then 'b'
    #   - id ie 'q' in [a,b]p.q
    # left/right
    #  '*' < '_' is True
    lr1 = n1.split('.')[1][4]
    lr2 = n2.split('.')[1][4]
    if lr1 < lr2:
        return -1
    if lr1 > lr2:
        return 1
    # compare a and b
    ab1 = n1.split('.')[0][0]
    ab2 = n2.split('.')[0][0]
    if ab1 < ab2:
        return -1
    if ab1 > ab2:
        return 1
    # generation
    g1 = int(n1.split('.')[0][1:])
    g2 = int(n2.split('.')[0][1:])
    if g1 < g2:
        anc2 = get_ancestor_name(n2, g1)
        p1 = n1.split('.')[1][0:4]
        p2 = anc2.split('.')[1][0:4]
        if p1 < p2:
            return -1
        if p1 > p2:
            return 1
        # if the ancestor is compared to descendant, prefer the ancestor
        return -1
    elif g1 > g2:
        anc1 = get_ancestor_name(n1, g2)
        p1 = anc1.split('.')[1][0:4]
        p2 = n2.split('.')[1][0:4]
        if p1 < p2:
            return -1
        if p1 > p2:
            return 1
        return 1
    else:
        p1 = n1.split('.')[1][0:4]
        p2 = n2.split('.')[1][0:4]
        if p1 < p2:
            return -1
        if p1 > p2:
            return 1
    return 0


def sort_by_name(names):
    return sorted(names, key=cmp_to_key(_cmp_names))

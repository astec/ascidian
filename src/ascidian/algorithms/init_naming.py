import os
import sys
import copy
import statistics

import astec.utils.common as common
import astec.utils.datadir as datadir
import astec.utils.ioproperties as ioproperties

import ascidian.core.ascidian_fate as ufate
import ascidian.core.icp as icp
import ascidian.core.atlas_division as atlasdivision
import ascidian.core.atlas_cell as atlascell
import ascidian.core.atlas_embryo as atlasembryo
import ascidian.core.symmetry_parameters as sympar
import ascidian.core.symmetry_embryo as symemb

import ascidian.core.figure_naming_registration as fignamingreg
import ascidian.core.naming_registration as anamingreg
import ascidian.core.naming_assign as anamingass
import ascidian.core.naming_util as anamingutil
#
#
#
#
#

monitoring = common.Monitoring()

_instrumented_ = False


########################################################################################
#
#
#
########################################################################################

def _get_correspondence(flo_time_range, embryo):
    #
    # for the cells of each time where the number of cells is equal to parameters.cell_number
    # gives the cell of the first time where the number of cells is equal to parameters.cell_number
    # it will allows to name only this first time while names can be issued from any time
    #
    correspondence = {}
    flotimesmin = min(flo_time_range)
    lineage = embryo.cell_lineage
    reverse_lineage = {v: k for k, values in lineage.items() for v in values}
    cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
    div = 10 ** embryo.time_digits_for_cell_id
    for c in cells:
        if (int(c) // div) not in flo_time_range or int(c) % div == 1:
            continue
        if int(c) // div == flotimesmin:
            correspondence[c] = c
            continue
        d = c
        while True:
            correspondence[c] = reverse_lineage[d]
            d = reverse_lineage[d]
            if int(d) // div == flotimesmin:
                break
    return correspondence


########################################################################################
#
#
#
########################################################################################

def init_naming_process(experiment, parameters):
    proc = "init_naming_process"

    #
    # parameter type checking
    #

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(parameters, anamingutil.InitNamingParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    time_digits_for_cell_id = experiment.get_time_digits_for_cell_id()

    atlasdivision.monitoring.copy(monitoring)
    atlascell.monitoring.copy(monitoring)
    icp.monitoring.copy(monitoring)
    fignamingreg.monitoring.copy(monitoring)
    sympar.monitoring.copy(monitoring)
    anamingreg.monitoring.copy(monitoring)
    anamingass.monitoring.copy(monitoring)
    anamingutil.monitoring.copy(monitoring)

    #
    # parameter is required here to set the reference atlas (if given)
    #   time warping is done wrt to this reference atlas
    #   properties are copied into each atlas (without normalisation)
    #   volume normalisation is also computed
    #
    atlases = atlasdivision.DivisionAtlases(parameters=parameters)
    atlases.add_atlases(parameters.atlasFiles, parameters, time_digits_for_cell_id=time_digits_for_cell_id)

    #
    #
    #
    fignamingreg.generate_figure(atlases, parameters, time_digits_for_cell_id=time_digits_for_cell_id)

    if atlases.n_atlases() == 0:
        monitoring.to_log_and_console(str(proc) + ": no atlases have been read")
        sys.exit(1)

    #
    # read input properties to be named
    #
    inputFile = None
    prop = {}
    reference_prop = {}

    if parameters.testFile is not None:
        reference_prop = ioproperties.read_dictionary(parameters.testFile, inputpropertiesdict={})
        if 'cell_name' not in reference_prop:
            monitoring.to_log_and_console(str(proc) + ": 'cell_name' is not in '" + str(parameters.testFile) + "'")
            sys.exit(1)
        inputFile = parameters.testFile
        prop = copy.deepcopy(reference_prop)
        del prop['cell_name']

    elif parameters.inputFile is not None:
        inputFile = parameters.inputFile
        prop = ioproperties.read_dictionary(parameters.inputFile, inputpropertiesdict={})

    if inputFile is None:
        return prop

    if prop == {}:
        monitoring.to_log_and_console(str(proc) + ": no properties?!")
        sys.exit(1)

    #
    # processing
    #

    name = inputFile.split(os.path.sep)[-1]
    if name.endswith(".xml") or name.endswith(".pkl"):
        name = name[:-4]

    #
    # build an atlas from the embryo to be named,
    # temporally align it with the reference embryo
    #
    embryo = atlasembryo.Atlas(prop, time_digits_for_cell_id=time_digits_for_cell_id)

    if False:
        #TODO: a bouger ailleurs
        # calcul des cellules symetriques
        #
        cell_number, flotimerange, ininterval = embryo.get_timerange(parameters.cell_number, change_cell_number=True)
        if cell_number is None:
            msg = "weird, requested cell number was " + str(parameters.cell_number) + ". "
            msg += "No cell range was found."
            monitoring.to_log_and_console(proc + ": " + msg)
        else:
            if cell_number != parameters.cell_number:
                msg = "will compute symmetric cells " + str(cell_number) + " instead of "
                msg += str(parameters.cell_number) + " cells"
                monitoring.to_log_and_console(proc + ": " + msg)
            timepoint = statistics.median_low(flotimerange)
            prop = symemb.set_symmetric_cells(prop, embryo, timepoint, parameters=parameters, verbose=True)
            if parameters.testFile is not None:
                anamingutil.test_symmetric_cells(prop, reference_prop, time_digits_for_cell_id=time_digits_for_cell_id)
        sys.exit()

    #
    # temporally align the test/read embryo with the reference embryo
    #
    ref_atlases = atlases.get_atlases()
    ref_atlas = atlases.get_reference_atlas()
    embryo.temporally_align_with(ref_atlases[ref_atlas])
    msg = "   ... "
    msg += "linear time warping of '" + str(name) + "' wrt '" + str(ref_atlas) + "' is "
    msg += "({:.3f}, {:.3f})".format(embryo.temporal_alignment[0], embryo.temporal_alignment[1])
    monitoring.to_log_and_console(msg, 1)

    #
    # get candidate names
    # names = dictionary indexed by cell id, value = list of cell names
    # time_embryo = time where the computation takes place
    # time_atlases = dictionary indexed by atlas name, value = time where the computation takes place
    #
    if parameters.naming_by_transfer.lower() == 'registration':
        names, time_embryo, time_atlases = anamingreg.naming_by_registration(embryo, atlases, parameters)
        prop = anamingreg.set_names_from_candidates(prop, names, parameters.minimal_consensus, verbose=True)
    elif parameters.naming_by_transfer.lower() == 'assignment':
        names, time_embryo, time_atlases = anamingass.naming_by_assignment(embryo, atlases, parameters)
        prop = anamingass.set_unlateralizednames_from_candidates(prop, names, parameters.minimal_consensus,
                                                                 verbose=True)
        if parameters.testFile is not None:
            anamingutil.test_unlateralizednaming(prop, reference_prop, time_digits_for_cell_id=time_digits_for_cell_id)
        prop = anamingass.set_names_from_unlateralizedones(prop, embryo, time_embryo, parameters, verbose=True)
        prop = anamingass.check_laterality(prop, embryo, atlases, time_atlases, verbose=True)
    else:
        msg = "naming by transfer method '" + str(parameters.naming_by_transfer) + "' not handled yet"
        monitoring.to_log_and_console(str(proc) + ": " + msg)
        monitoring.to_log_and_console("Exiting")
        sys.exit(1)

    #
    # eventually remove some names
    #
    if parameters.check_duplicate:
        monitoring.to_log_and_console("... removing duplicates")
        prop = anamingreg.remove_duplicate(prop, embryo)

    if parameters.check_volume:
        monitoring.to_log_and_console("... checking names wrt volumes")
        prop = anamingreg.check_initial_naming_volume(prop, embryo, atlases, verbose=True)
    #
    #
    #
    prop = anamingreg.evaluate_initial_naming(prop, embryo, atlases, time_embryo, time_atlases)

    #
    # set fate
    #
    prop = ufate.set_fate_from_names(prop)
    prop = ufate.set_color_from_fate(prop)

    #
    # is there a test to be conducted
    #
    if parameters.testFile is not None:
        anamingutil.test_naming(prop, reference_prop, time_digits_for_cell_id=time_digits_for_cell_id)
        # right_naming, wrong_naming, unnamed_cells, not_in_reference = output
        # used for figures

    if isinstance(parameters.outputFile, str):
        ioproperties.write_dictionary(parameters.outputFile, prop)

    return prop

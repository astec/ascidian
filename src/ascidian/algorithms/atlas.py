import sys

import astec.utils.common as common
import astec.utils.datadir as datadir

import ascidian.core.atlas_embryo as atlasembryo
import ascidian.core.atlas_division as atlasdivision

import ascidian.core.figure_division as figdivision

#
#
#
#
#

monitoring = common.Monitoring()

_instrumented_ = False

########################################################################################
#
# classes
# - computation parameters
#
########################################################################################


def atlas_process(experiment, parameters):
    proc = "atlas_process"
    #
    # parameter type checking
    #

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(parameters, atlasembryo.AtlasParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    time_digits_for_cell_id = experiment.get_time_digits_for_cell_id()

    #
    # create the object atlases,
    # the reference atlas (used for temporal alignment) may be set from parameters
    #
    atlases = atlasdivision.DivisionAtlases(parameters=parameters)

    #
    # read atlas/properties files
    # copy from files the properties of interest
    # - 'cell_lineage',
    # - 'cell_name',
    # - 'cell_contact_surface', and
    # - 'cell_contact_surface'
    # temporally register the atlases
    # method from class Atlases in utils/atlas_embryo.py
    #
    atlases.add_atlases(parameters.atlasFiles, parameters, time_digits_for_cell_id=time_digits_for_cell_id)

    if atlases.n_atlases() == 0:
        msg = proc + ": no atlases ?!"
        monitoring.to_log_and_console(msg, 1)
        return None

    #
    # division based part
    # build division atlas (utils/atlas_division.py)
    #
    atlases.build_division_atlases(parameters)

    #
    # figures (if required)
    #
    figdivision.generate_figure(atlases, parameters)

    return atlases

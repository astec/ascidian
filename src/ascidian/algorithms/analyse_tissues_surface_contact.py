import pickle as pkl
import os
import numpy as np
from sklearn.cluster import KMeans
from sklearn.metrics.cluster import adjusted_rand_score
import random
from collections import defaultdict, deque
# import networkx as nx

from ascidian.algorithms.division_analysis_init import get_temporal_alignment_coefficients_of_embryos
import ascidian.core.temporal_alignment as utimes
from ascidian.division_analysis.division_analysis_init_tools import get_embryo_at_t, volume_fitting_and_voxelsize_correction
import ascidian.ascidian.name as uname
from ascidian.division_analysis.division_analysis_tools import determine_fate_group


##################

###################
def group_by_connected_tissues(surface_of_contact_dict, fate_to_id):
    """
    Group cells into groups.
    Cells of same group have the same tissue type and are connected to the rest of the cells
    of the group.
    Cells are connected when either they have a surface of contact or they have surface of contact with
    other cells of the same tissue type that have surface of contact between each other.

    Parameters
    ----------
    surface_of_contact_dict : dict
        {'cell_id': {'cell_in_contact_id': surface_of_contact}}
    fate_to_id : dict
        {cell_id: fate}

    Returns
    -------
    groups : dict
        {'cell_id': 'fate+group_index'}
    """
    def dfs(cell_id, group_id):
        stack = [cell_id]
        while stack:
            current_cell = stack.pop()
            if current_cell in visited:
                continue
            visited.add(current_cell)
            groups[current_cell] = f"{fate}_{group_id}"
            for neighbor, _ in surface_of_contact_dict.get(current_cell, {}).items():
                if neighbor in fate_to_id and fate_to_id[neighbor] == fate and neighbor not in visited:
                    stack.append(neighbor)

    groups = {}
    visited = set()
    group_counter = {}

    for cell_id, fate in fate_to_id.items():
        if cell_id not in visited:  # every visited cell is a new group?
            if isinstance(fate,list):
                fate = fate[0]  # in some embryos some divisions occur too late, just consider one of the two tissues
            if fate not in group_counter:
                group_counter[fate] = 0
            else:
                group_counter[fate] += 1
            dfs(cell_id, group_counter[fate])

    return groups

def permute_tissue_neighbors(dict_cells_surface_of_contact_at_t, cell_ids_to_fates,
                             tissues_resolution=True):
    """
    Randomly permutes the tissue types of neighboring cells for each tissue, ensuring cells of the same tissue type stay neighbors.

    Parameters
    ----------
    dict_cells_surface_of_contact_at_t : dict
        Dictionary containing surface of contact information for cells.
        Format: {'cell_id': {'neighbour_cell_id': surface_of_contact}}
    cell_ids_to_fates : dict
        Dictionary containing the tissue types (fates) of cells.
        Format: {'cell_id': 'cell_fate'}
    tissues_resolution : bool
        If true, consider the tissue resolution (e.g., meso, endo, ns, epid).

    Returns
    -------
    permuted_cell_ids_to_fates : dict
        Dictionary with permuted tissue types for neighboring cells.
    """
    cells_fates = extract_unique_fates(cell_ids_to_fates)
    if tissues_resolution:
        tissue_types = set(determine_fate_group(fate) for fate in cells_fates)
    else:
        tissue_types = set(fate for fate in cells_fates)

    permuted_cell_ids_to_fates = cell_ids_to_fates.copy()
    reassigned_ids = set()

    for tissue in tissue_types:
        if tissue == 'Unknown':
            continue
        tissue_cells = [cell_id for cell_id, fate in cell_ids_to_fates.items() if fate == tissue]
        if tissues_resolution:
            tissue_cells = [cell_id for cell_id, fate in cell_ids_to_fates.items() if
                            determine_fate_group(fate) == tissue]
        neighboring_cells = defaultdict(list)

        for cell_id in tissue_cells:
            for neighbor_id, _ in dict_cells_surface_of_contact_at_t.get(cell_id, {}).items():
                if tissues_resolution:
                    if (determine_fate_group(cell_ids_to_fates.get(neighbor_id)) != tissue and
                            determine_fate_group(cell_ids_to_fates.get(neighbor_id)) != 'Unknown'):
                        if neighbor_id in cell_ids_to_fates:
                            neighboring_cells[determine_fate_group(cell_ids_to_fates[neighbor_id])].append(neighbor_id)
                else:
                    if (cell_ids_to_fates.get(neighbor_id) != tissue and
                            cell_ids_to_fates.get(neighbor_id) != 'Unknown'):
                        if neighbor_id in cell_ids_to_fates:
                            neighboring_cells[cell_ids_to_fates[neighbor_id]].append(neighbor_id)
        #### assignement of tissue's neighbouring cells to tissue type
        ###########
        neighbor_cells_flattened = [cell for cells in neighboring_cells.values() for cell in cells]
        neighbor_cells_flattened = list(set(neighbor_cells_flattened) - set(reassigned_ids))
        ##
        neighboring_tissue_types = list(neighboring_cells.keys())
        total_neighbors = len(neighbor_cells_flattened)
        print('total neighbours lenght:', total_neighbors)
        if total_neighbors == 0:
            continue
        portions = {}
        remaining_cells = total_neighbors
        for tissue_type in neighboring_tissue_types[:-1]:
            count = random.randint(1, max(1, remaining_cells - len(neighboring_tissue_types) + len(portions) + 1))
            portions[tissue_type] = count
            remaining_cells -= count
        portions[neighboring_tissue_types[-1]] = remaining_cells
        #
        for neighboring_tissue_type, count in portions.items():
            print('doing assignement for neighbour tissue:', neighboring_tissue_type, count)
            assigned_cells = 0

            # choose random cell for neighbouring tissue first assignement
            if len(neighbor_cells_flattened) < 1:
                continue
            random.shuffle(neighbor_cells_flattened)
            neighbor_cell = neighbor_cells_flattened[0]
            while neighbor_cell in reassigned_ids:
                random.shuffle(neighbor_cells_flattened)
                neighbor_cell = neighbor_cells_flattened[0]

            permuted_cell_ids_to_fates[neighbor_cell] = neighboring_tissue_type
            assigned_cells += 1
            reassigned_ids.add(neighbor_cell)
            neighbor_cells_flattened.remove(neighbor_cell)
            # some cells are common for multiple tissues so will be reassigned multiple times

            neighbor_queue = deque([neighbor_cell])
            while neighbor_queue and assigned_cells < count:
                current_cell = neighbor_queue.popleft()
                for next_neighbor_id in dict_cells_surface_of_contact_at_t.get(current_cell, {}).keys():
                    if next_neighbor_id in neighbor_cells_flattened and next_neighbor_id not in reassigned_ids:
                        permuted_cell_ids_to_fates[next_neighbor_id] = neighboring_tissue_type
                        assigned_cells += 1
                        reassigned_ids.add(next_neighbor_id)
                        neighbor_cells_flattened.remove(next_neighbor_id)
                        if assigned_cells >= count:
                            break
                        neighbor_queue.append(next_neighbor_id)
                if not neighbor_queue and assigned_cells < count:
                    print('shuffling', assigned_cells, count)
                    random.shuffle(neighbor_cells_flattened)
                    neighbor_cell = neighbor_cells_flattened[0]

                    permuted_cell_ids_to_fates[neighbor_cell] = neighboring_tissue_type
                    assigned_cells += 1
                    reassigned_ids.add(neighbor_cell)
                    neighbor_cells_flattened.remove(neighbor_cell)
                    neighbor_queue.append(neighbor_cell)

            print('assigned cells to the neighbouring tissue:',assigned_cells)
        print('not permuted cells',len(neighbor_cells_flattened))
    return permuted_cell_ids_to_fates

#########
#####
#########

def compare_clusterings(id_to_clusters1, id_to_clusters2, tissuresolution= True):
    """
    Compare two clusterings of cells using the Adjusted Rand Index.

    Parameters
    ----------
    id_to_clusters1 : dict
        Dictionary where keys are cell IDs and values are cluster indices for the first clustering.
    id_to_clusters2 : dict
        Dictionary where keys are cell IDs and values are cluster indices for the second clustering.

    Returns
    -------
    float
        Adjusted Rand Index between the two clusterings.
    """
    # Ensure both dictionaries have the same keys
    common_ids = set(id_to_clusters1.keys()).intersection(set(id_to_clusters2.keys()))

    if not common_ids:
        raise ValueError("No common cell IDs found in the two clusterings.")

    if tissuresolution:
        labels2 = [determine_fate_group(id_to_clusters2[cell_id]) for cell_id in common_ids]
    else:
        labels2 = [id_to_clusters2[cell_id][0] if isinstance(id_to_clusters2[cell_id], list) else id_to_clusters2[cell_id] for cell_id in common_ids]
    # Extract cluster assignments for the common IDs
    labels1 = [id_to_clusters1[cell_id] for cell_id in common_ids]

    # Compute the Adjusted Rand Index
    ari = adjusted_rand_score(labels1, labels2)

    return ari

def clusterCells(cells_barycenters_at_t, contact_dict, nb_of_clusters=4, n_init=10):
    """
    Cluster cells based on their barycenters and contact surfaces ensuring that cells within the same cluster
     have neighbors within the same cluster.

    Parameters
    ----------
    cells_barycenters_at_t : dict
        A dictionary where keys are cell IDs and values are 3D coordinates of cell barycenters.
        Example: {'cell_id': [x, y, z]}

    contact_dict : dict
        A dictionary where keys are cell IDs and values are dictionaries of neighboring cell IDs with contact surfaces.
        Example: {'cell_id': {'neighbour_cell_id': surface_of_contact}}

    nb_of_clusters : int, optional (default=4)
        The number of clusters to form.

    Returns
    -------
    clustered_cells : dict
        A dictionary where keys are cell IDs and values are cluster indices.
        Example: {'cell_id': cluster_idx}
    """
    # Convert cells_barycenters_at_t to a list of coordinates and cell IDs
    cell_ids = list(cells_barycenters_at_t.keys())
    cell_coordinates = np.array(list(cells_barycenters_at_t.values()))

    # Step 1: Perform initial K-means clustering
    kmeans = KMeans(n_clusters=nb_of_clusters, n_init=n_init, random_state=0).fit(cell_coordinates)
    labels = kmeans.labels_

    # Step 2: Refine clusters to ensure neighbor constraint
    clustered_cells = {cell_id: label for cell_id, label in zip(cell_ids, labels)}
    for cluster_idx in range(nb_of_clusters):
        cluster_cells = [cell_id for cell_id in cell_ids if clustered_cells[cell_id] == cluster_idx]
        for cell_id in cluster_cells:
            neighbors = contact_dict[cell_id].keys()
            if not any(clustered_cells[neighbor_id] == cluster_idx for neighbor_id in neighbors if neighbor_id in clustered_cells):
                # If no neighbors are in the same cluster, move cell to the cluster of its neighbors
                neighbor_clusters = [clustered_cells[neighbor_id] for neighbor_id in neighbors if neighbor_id in clustered_cells]
                if neighbor_clusters:
                    most_common_neighbor_cluster = max(set(neighbor_clusters), key=neighbor_clusters.count)
                    # in the previous line .count is applied on neighbor_clusters list not the set
                    clustered_cells[cell_id] = most_common_neighbor_cluster

    return clustered_cells


def propagate_cellsClusters_from_ref_at_tstart(cell_names_dict_t_pmx, cell_names_dict_tstart_ref, clustered_cells_dict):
    """
    Propagate cell clusters from a reference embryo at tstart to another embryo at timepoint t_pmx.

    Parameters
    ----------
    cell_names_dict_t_pmx : dict
        Dictionary where keys are cell IDs and values are cell names in the target embryo at time t_pmx.
    cell_names_dict_tstart_ref : dict
        Dictionary where keys are cell IDs and values are cell names in the reference embryo at tstart.
    clustered_cells_dict : dict
        Dictionary where keys are cell IDs and values are cluster indices in the reference embryo at tstart.

    Returns
    -------
    clustered_cells_in_pmx : dict
        A dictionary where keys are cell IDs and values are cluster indices.
        Example: {'cell_id': cluster_idx}
    """
    clustered_cells_in_pmx = {}
    ref_name_to_cluster = {cell_name: clustered_cells_dict[cell_id] for cell_id, cell_name in
                           cell_names_dict_tstart_ref.items()}
    pmx_name_to_id = {cell_name: cell_id for cell_id, cell_name in cell_names_dict_t_pmx.items()}

    for cell_id, cell_name in cell_names_dict_t_pmx.items():
        if cell_name in ref_name_to_cluster:
            clustered_cells_in_pmx[cell_id] = ref_name_to_cluster[cell_name]
        else:
            _, gen, _, _ = uname.get_cell_name_parts(cell_name)
            queue = [(cell_name, gen)]
            # found_cluster = False

            while queue:
                current_name, generation = queue.pop(0)
                if generation > 13:
                    print('no descendant found for', cell_name)
                    break
                daughter1_name, daughter2_name = uname.get_daughter_names(current_name)
                if daughter1_name in ref_name_to_cluster:
                    #print('found daughter',daughter1_name)
                    #print(pmx_name_to_id.keys())
                    clustered_cells_in_pmx[pmx_name_to_id[cell_name]] = ref_name_to_cluster[daughter1_name]
                else:
                    queue.append((daughter1_name, generation + 1))
                if daughter2_name in ref_name_to_cluster:
                    # print('found daughter',daughter2_name)
                    # print(pmx_name_to_id.keys())
                    clustered_cells_in_pmx[pmx_name_to_id[cell_name]] = ref_name_to_cluster[daughter2_name]
                else:
                    queue.append((daughter2_name, generation + 1))

            # If no descendants were found within the generation limit, search for ascendants
            if queue:
                mother_name = uname.get_mother_name(cell_name)
                if mother_name in ref_name_to_cluster:
                    clustered_cells_in_pmx[pmx_name_to_id[cell_name]] = ref_name_to_cluster[mother_name]
                else:
                    print('no mother found for', cell_name)
            # if not found_cluster:
            #    # If no cluster was found, you may want to handle this case, e.g., assign a default cluster or leave it unclustered
            #    clustered_cells_in_pmx[cell_id] = -1  # Or some default value indicating no cluster found

    return clustered_cells_in_pmx


def extract_unique_fates(cell_ids_to_fates):
    """
    Extracts unique fates from a dictionary where some values are lists.

    Parameters
    ----------
    cell_ids_to_fates (dict): Dictionary mapping cell IDs to their fates, where some fates may be lists.

    Returns
    -------
    set: A set of unique fates.
    """
    unique_fates = set()

    for fates in cell_ids_to_fates.values():
        if isinstance(fates, list):
            unique_fates.update(fates)
        else:
            unique_fates.add(fates)
    return unique_fates


### from embryo surface of contact of cells at t get tissues surface of contacts
def get_tissues_surface_of_contact_from_cells(embryo_cells_surface_of_contact_at_t, cell_ids_to_fates,
                                              tissues_resolution=True, permutation_of_neighbours=None):
    """

    Parameters
    ----------
    embryo_cells_surface_of_contact_at_t (dict) : { 'cell_id':{{'neighbours_cell_id': surface_of_contact } } }
    cell_ids_to_fates (dict) : { 'cell_id': cell_fate_small_resolution  }
    tissues_resolution (boolean): if true consider the tissue meso, endo, ns and epid
    clusters (bool) : if true you're not grouping tissues but clusters indexes

    Returns
    -------
    tissues_contact_surface (dict): {'tissue1':{'tissue2': surface of contact}}

    """
    # get all fates in embryo
    # cell_tissuetype = determine_fate_group(cell_ids_to_fates[cell_id])
    # Get all unique tissue types
    cells_fates = extract_unique_fates(cell_ids_to_fates)
    if tissues_resolution:
        tissue_types = set(determine_fate_group(fate) for fate in cells_fates)
    else:
        tissue_types = set(fate for fate in cells_fates)

    print(tissue_types)
    # Initialize a dictionary to store the surface of contact between tissues
    tissues_contact_surface = {tissue: {other_tissue: 0 for other_tissue in tissue_types} for tissue in tissue_types}

    if permutation_of_neighbours is not None:
        print(len(permutation_of_neighbours), len(embryo_cells_surface_of_contact_at_t))
        # print(permutation_of_neighbours)
        # print(cell_ids_to_fates)

    # print('new time##########')
    # Iterate over each cell and its contact surfaces
    for cell_id, contacts in embryo_cells_surface_of_contact_at_t.items():
        if cell_id not in cell_ids_to_fates:
            continue
        if not tissues_resolution:
            cell_fate = cell_ids_to_fates[cell_id]
        else:
            cell_fate = determine_fate_group(cell_ids_to_fates[cell_id], True)
            if len(cell_fate)>1:
                continue
            if cell_ids_to_fates[cell_id] == 'Germ Line':
                # cell_fate = 'Germ Line'
                continue
            else:
                cell_fate = cell_fate[0]
        # print(cell_id)
        # print('cell fate', cell_fate, cell_ids_to_fates[cell_id])
        # print(contacts.keys())
        for neighbour_id, contact_surface in contacts.items():
            if neighbour_id not in cell_ids_to_fates:
                # print('breaking', neighbour_id)
                continue
            # neighbour fate should be brought from permuted surface of contact but cell fate from non-permuted
            if permutation_of_neighbours is not None:
                if neighbour_id not in permutation_of_neighbours:
                    continue

                if not tissues_resolution:
                    neighbour_fate = permutation_of_neighbours[neighbour_id]
                else:
                    neighbour_fate = determine_fate_group(permutation_of_neighbours[neighbour_id])

            else:
                if not tissues_resolution:
                    neighbour_fate = cell_ids_to_fates[neighbour_id]
                else:
                    neighbour_fate = determine_fate_group(cell_ids_to_fates[neighbour_id], True)
                    if len(neighbour_fate) > 1:
                        continue
                    if cell_ids_to_fates[neighbour_id] == 'Germ Line':
                        # neighbour_fate = 'Germ Line'
                        continue
                    else:
                        neighbour_fate = neighbour_fate[0]
                    #print('neighbour fate:', neighbour_fate, neighbour_id, cell_ids_to_fates[neighbour_id])
            if cell_fate != neighbour_fate:
                tissues_contact_surface[cell_fate][neighbour_fate] += contact_surface

    return tissues_contact_surface


##############
#### Compare stability and variability of tissues surface of contact to permuted tissues neighbours

def stability_tissues_surface_of_contacts_overtime(files, ref_filename, output_folder='', imaging_res=0.3,
                                                   embryo_res='halfs', tissues_resolution=True,
                                                   start_time_nb_of_cells=218):
    """

    Parameters
    ----------
    files
    ref_filename
    output_folder
    embryo_res : if 'halfs' left/right halfs of embryo will be processed
    tissues_resolution : if true tissue resolution considered is : epid, endo, NS, meso

    Returns
    -------

    """
    # imagin resolution is 0.3x0.3x0.3 um

    list_dict_contact_surfaces = []
    list_dict_names = []
    list_dict_mass = []
    list_dict_fates = []

    embryo_names = []
    embryos_props = {}
    embryo_idx = {}

    atimes_list = []
    times_list = []

    results = {}
    results_clusters = {}
    results_rd_neighbours = {}

    clusters_rank = {}

    list_surface_adjustment_factors = []  # list of dict [pmx_idx]{'taq': scaling_factor}

    with open(ref_filename, 'rb') as f:
        ref_props_data = {}
        ref_name = ref_filename.split('-')[1]
        ref_props = pkl.load(f)
        ref_props_data[ref_name] = ref_props

        # compute volume alignment parms
        volume_adjustement_factors = volume_fitting_and_voxelsize_correction(ref_props['cell_volume'],
                                                                             target_volume=60000000)

        volume_adjustement_factors = {t: factor * imaging_res for t, factor in volume_adjustement_factors.items()}

        list_dict_contact_surfaces.append(ref_props['cell_contact_surface'])
        list_dict_names.append(ref_props['cell_name'])
        # list_dict_mass.append(ref_props['cell_volume'])
        list_dict_fates.append(ref_props['cell_fate'])
        embryo_names.append(ref_name)
        i = len(embryo_idx)
        embryo_idx[ref_name] = i

        #  add
        list_surface_adjustment_factors.append(volume_adjustement_factors)

        ###

        # get surfaces of contacts instead of barycenters
        for filename in files:
            if filename == ref_filename:
                continue
            with open(filename, 'rb') as file:
                pm_props = pkl.load(file)
                embryo_name = filename.split('-')[1]

                embryos_props[embryo_name] = {
                    'surface_of_contact': pm_props['cell_contact_surface'],
                    'cell_name': pm_props['cell_name'],
                    'cell_volume': pm_props['cell_volume'],
                    'cell_lineage': pm_props['cell_lineage']
                }

                # compute volume alignment parms
                volume_adjustement_factors = volume_fitting_and_voxelsize_correction(pm_props['cell_volume'],
                                                                                     target_volume=60000000)
                volume_adjustement_factors = {t: factor * imaging_res for t, factor in
                                              volume_adjustement_factors.items()}

                # handle a case of embryo_resolution
                i = len(embryo_idx)
                embryo_idx[embryo_name] = i
                list_dict_contact_surfaces.append(pm_props['cell_contact_surface'])
                list_dict_names.append(pm_props['cell_name'])
                # list_dict_mass.append(pm_props['cell_volume'])
                list_dict_fates.append(pm_props['cell_fate'])

                embryo_names.append(embryo_name)

                #  add adjustement parameters of volumes
                list_surface_adjustment_factors.append(volume_adjustement_factors)

        output_file = os.path.join(output_folder, "log_file.log")
        file = open(output_file, "w")
        file.write('Hello')

        # do temporal alignment
        temporal_alignment_coefficient_dict, cell_per_time_in_ref = get_temporal_alignment_coefficients_of_embryos(
            embryos_props,
            ref_props_data)

        #
        #  delete embryos_props
        del embryos_props
        del ref_props_data

        # for each embryo compute adjusted times
        times_ref = utimes.get_embryo_times(ref_props['cell_lineage'])
        a, b = temporal_alignment_coefficient_dict[ref_name]
        a_times_ref = [a * t + b for t in times_ref]

        for i, embryo in enumerate(embryo_names):
            times = utimes.get_embryo_times(list_dict_contact_surfaces[i])
            a, b = temporal_alignment_coefficient_dict[embryo]
            a_times = [a * t + b for t in times]
            atimes_list.append(a_times)
            times_list.append(times)  # list of embryos timepoints, idx of embryos in embryo_idx

        # if embryo_res == 'halfs':
        #    ref_name = ref_name + '_left'

        ##
        ##
        #
        # start at timepoint of 76 cell-stage from cell_per_time_in_ref={'t':nb_of_cells}
        start_timepoint = None
        for t, nb_of_cells in sorted(cell_per_time_in_ref.items()):
            if nb_of_cells >= start_time_nb_of_cells:
                start_timepoint = t
                break
        print('start timepoint in ref', start_timepoint)

        # at start time cluster cell in ref embryo
        # print('starting cluster')
        cells_barycenters_at_t = get_embryo_at_t(ref_props['cell_barycenter'], start_timepoint)
        cell_names_dict_tstart_ref = get_embryo_at_t(ref_props['cell_name'], start_timepoint)
        clustered_cells = clusterCells(cells_barycenters_at_t, list_dict_contact_surfaces[0], nb_of_clusters=4,
                                       n_init=10)
        # plot embryo clustered
        embryo_plot_folder = os.path.join(output_folder, 'embryo_cell_clustering_propagation_plots')
        if not os.path.exists(embryo_plot_folder):
            os.makedirs(embryo_plot_folder)
        output_path = os.path.join(embryo_plot_folder, f'_{ref_name}.py')
        write_script_plot_embryo_fig(output_path, cells_barycenters_at_t, clustered_cells)

        # random convex reassignement of tissue's neighbours
        cell_ids_to_fates = get_embryo_at_t(ref_props['cell_fate'], start_timepoint)
        dict_cells_surface_of_contact_at_t = get_embryo_at_t(ref_props['cell_contact_surface'], start_timepoint)
        # if tissue resolution is false seperate fates by connected groups:
        if not tissues_resolution:
            cell_ids_to_fates = group_by_connected_tissues(dict_cells_surface_of_contact_at_t, cell_ids_to_fates)
        permuted_tissues_neighbours_dict = permute_tissue_neighbors(dict_cells_surface_of_contact_at_t,
                                                                    cell_ids_to_fates,
                                                                    tissues_resolution)

        # plot embryo
        embryo_plot_folder = os.path.join(output_folder, 'embryo_tissue_neighbours_permutation_propagation_plots')
        if not os.path.exists(embryo_plot_folder):
            os.makedirs(embryo_plot_folder)
        output_path = os.path.join(embryo_plot_folder, f'_{ref_name}.py')
        write_script_plot_embryo_fig(output_path, cells_barycenters_at_t, permuted_tissues_neighbours_dict)

        # at each timepoint
        for at in a_times_ref:
            if at < start_timepoint:
                continue
            # print('dev time', at)
            # for each embryo
            for pmx_name, pmx_idx in embryo_idx.items():
                print('in embryo', pmx_name)
                # get taq
                closest_at = min(atimes_list[pmx_idx], key=lambda x: abs(x - at))
                if abs(closest_at - at) > 2:
                    continue
                taq = times_list[pmx_idx][atimes_list[pmx_idx].index(closest_at)]

                # print(at, closest_at,taq)
                # get embryo at taq
                pm_cells_surface_of_contact_at_t = get_embryo_at_t(list_dict_contact_surfaces[pmx_idx], taq)
                cell_names_dict_t_pmx = get_embryo_at_t(list_dict_names[pmx_idx], taq)
                if not list(cell_names_dict_t_pmx.keys()):
                    continue

                if not tissues_resolution:
                    cell_ids_to_fates_pmx = get_embryo_at_t(list_dict_fates[pmx_idx], taq)
                    cell_ids_to_fates_pmx = group_by_connected_tissues(pm_cells_surface_of_contact_at_t,
                                                               cell_ids_to_fates_pmx)

                    pm_tissues_surface_of_contact_t = get_tissues_surface_of_contact_from_cells(
                        pm_cells_surface_of_contact_at_t,
                        cell_ids_to_fates_pmx, tissues_resolution)
                else:
                    # get vectors surface of contact of tissues
                    pm_tissues_surface_of_contact_t = get_tissues_surface_of_contact_from_cells(
                        pm_cells_surface_of_contact_at_t,
                        list_dict_fates[pmx_idx], tissues_resolution)
                # pm_tissues_surface_of_contact_t {'tissuetpe1':{'tissuetype2':surface_of_contact } }
                # scale pm_tissues_surface_of_contact_t
                scaling_factor = list_surface_adjustment_factors[pmx_idx][taq]
                pm_tissues_surface_of_contact_t = {k: {nk: nv * scaling_factor * scaling_factor for nk, nv in v.items()}
                                                   for k, v in
                                                   pm_tissues_surface_of_contact_t.items()}

                # store in dict {'t': {'embryo_name':{ } }}
                if at not in results:
                    results[at] = {}
                results[at][pmx_name] = pm_tissues_surface_of_contact_t

                # propagate clustering of cell names in this embryo at this time
                #print('propagating cluster to', pmx_name, at, taq)
                cell_ids_to_clusters = propagate_cellsClusters_from_ref_at_tstart(cell_names_dict_t_pmx,
                                                                                  cell_names_dict_tstart_ref,
                                                                                  clustered_cells)
                # get surface of contact between groups in embryo at t
                pm_clusters_surface_of_contact_t = get_tissues_surface_of_contact_from_cells(
                    pm_cells_surface_of_contact_at_t,
                    cell_ids_to_clusters, False)
                # scale the surface of contact
                pm_clusters_surface_of_contact_t = {k: {nk: nv * scaling_factor * scaling_factor for nk, nv in v.items()}
                                                   for k, v in
                                                   pm_clusters_surface_of_contact_t.items()}
                # store results
                if at not in results_clusters:
                    results_clusters[at] = {}
                results_clusters[at][pmx_name] = pm_clusters_surface_of_contact_t

                rank = compare_clusterings(cell_ids_to_clusters, list_dict_fates[pmx_idx], tissues_resolution)
                if at not in clusters_rank:
                    clusters_rank[at] = {}
                clusters_rank[at][pmx_name] = rank
                #print('rank:', rank)

                ###
                ######
                # for randomly reassigned tissues neighbours
                # propagate clustering of cell names in this embryo at this time
                #print('propagating random reassigned neighbours to', pmx_name, at, taq)
                cell_ids_to_clusters_permuted_t = propagate_cellsClusters_from_ref_at_tstart(cell_names_dict_t_pmx,
                                                                                  cell_names_dict_tstart_ref,
                                                                                  permuted_tissues_neighbours_dict)
                # get surface of contact between groups in embryo at t
                if not tissues_resolution:
                    #cell_ids_to_fates_pmx = get_embryo_at_t(list_dict_fates[pmx_idx], taq)
                    #cell_ids_to_fates_pmx = group_by_connected_tissues(pm_cells_surface_of_contact_at_t,
                    #                                                   cell_ids_to_fates_pmx)
                    pm_clusters_surface_of_contact_t = get_tissues_surface_of_contact_from_cells(
                        pm_cells_surface_of_contact_at_t,
                        cell_ids_to_clusters_permuted_t, tissues_resolution, cell_ids_to_clusters_permuted_t)
                else:
                    pm_clusters_surface_of_contact_t = get_tissues_surface_of_contact_from_cells(
                        pm_cells_surface_of_contact_at_t,
                        list_dict_fates[pmx_idx], tissues_resolution, cell_ids_to_clusters_permuted_t)
                # scale the surface of contact
                pm_clusters_surface_of_contact_t = {
                    k: {nk: nv * scaling_factor * scaling_factor for nk, nv in v.items()}
                    for k, v in
                    pm_clusters_surface_of_contact_t.items()}
                # store results
                if at not in results_rd_neighbours:
                    results_rd_neighbours[at] = {}
                results_rd_neighbours[at][pmx_name] = pm_clusters_surface_of_contact_t


        # distance between surface of contact between tissues between consecutive t, and % tstart
        output_path = os.path.join(output_folder, f'tissues_surface_of_contact_stability_TissueResolution_{tissues_resolution}.py')
        script_plot_tissue_surface_of_contact_stability_btw_consecutive_t(results, output_path)
        # stability of tissue surface of contact relative to first timepoint (76 cell stage)
        output_path = os.path.join(output_folder, f'tissues_surface_of_contact_stability_to_first_timepoint_TissueResolution_{tissues_resolution}.py')
        script_plot_tissue_surface_of_contact_stability_to_ref_t(results, output_path)
        output_path = os.path.join(output_folder, f'tissues_surface_of_contact_variability_TissueResolution_{tissues_resolution}.py')
        script_plot_tissue_surface_of_contact_variability(results, ref_name, output_path)

        """
        # same plots for random clustering
        # distance between surface of contact between tissues between consecutive t, and % tstart
        output_path = os.path.join(output_folder, f'random_clusters_surface_of_contact_stability_TissueResolution_{tissues_resolution}.py')
        script_plot_tissue_surface_of_contact_stability_btw_consecutive_t(results_clusters, output_path)
        # stability of tissue surface of contact relative to first timepoint (76 cell stage)
        output_path = os.path.join(output_folder, f'random_clusters_surface_of_contact_stability_to_first_timepoint_TissueResolution_{tissues_resolution}.py')
        script_plot_tissue_surface_of_contact_stability_to_ref_t(results_clusters, output_path)
        output_path = os.path.join(output_folder, f'random_clusters_surface_of_contact_variability_TissueResolution_{tissues_resolution}.py')
        script_plot_tissue_surface_of_contact_variability(results_clusters, ref_name, output_path)

        # compare clustering comparision of names to tissuesfate
        output_path = os.path.join(output_folder, f'random_clusters_tissues_similarity_TissueResolution_{tissues_resolution}.py')
        write_plot_clusters_similarity_script(clusters_rank, output_path)
        """

        # same plots for random clustering
        # distance between surface of contact between tissues between consecutive t, and % tstart
        output_path = os.path.join(output_folder, f'random_tissues_neighbours_surface_of_contact_stability_TissueResolution_{tissues_resolution}.py')
        script_plot_tissue_surface_of_contact_stability_btw_consecutive_t(results_rd_neighbours, output_path)
        # stability of tissue surface of contact relative to first timepoint (76 cell stage)
        output_path = os.path.join(output_folder, f'random_tissues_neighbours_surface_of_contact_stability_to_first_timepoint_TissueResolution_{tissues_resolution}.py')
        script_plot_tissue_surface_of_contact_stability_to_ref_t(results_rd_neighbours, output_path)
        output_path = os.path.join(output_folder, f'random_tissues_neighbours_surface_of_contact_variability_TissueResolution_{tissues_resolution}.py')
        script_plot_tissue_surface_of_contact_variability(results_rd_neighbours, ref_name, output_path)


def write_plot_clusters_similarity_script(clusters_rank, output_path):
    """
    Writes a Python script to plot clustering similarity.

    Parameters
    ----------
    clusters_rank : dict
        Dictionary where keys are timepoints and values are dictionaries mapping embryo names to similarity ranks.
        Example: {timepoint: {embryo_name: rank}}
    output_path : str
        Path to save the generated Python script.
    """
    script_content = f"""
import matplotlib.pyplot as plt

# Clustering similarity data
clusters_rank = {clusters_rank}

# Extracting timepoints and embryo names
timepoints = sorted(clusters_rank.keys())
all_embryo_names = set()
for timepoint in timepoints:
    all_embryo_names.update(clusters_rank[timepoint].keys())
all_embryo_names = sorted(all_embryo_names)

# Plotting
fig, ax = plt.subplots(figsize=(14, 8))

for embryo_name in all_embryo_names:
    similarities = []
    valid_timepoints = []
    for tp in timepoints:
        if embryo_name in clusters_rank[tp]:
            similarities.append(clusters_rank[tp][embryo_name])
            valid_timepoints.append(tp)
    if similarities:
        ax.plot(valid_timepoints, similarities, marker='o', label=embryo_name)

ax.set_xlabel('Timepoints')
ax.set_ylabel('Adjusted Rand Index')
ax.set_title('Clustering and Tissue Types Similarity Over Time')
ax.legend(title='Embryos')
ax.set_ylim(-0.5,1)

plt.tight_layout()
plt.show()
"""

    with open(output_path, 'w') as script_file:
        script_file.write(script_content)

    print(f"Plot similarity script saved to {output_path}")

def script_plot_tissue_surface_of_contact_stability_btw_consecutive_t(results, output_path):
    """
    Generates a Python script to plot the distances between the surfaces of contact between tissues
    in the same embryo at consecutive timepoints.

    Parameters
    ----------
    results (dict): Dictionary containing surface of contact data.
    output_path (str): Path to save the generated script.

    Returns
    -------
    None
    """
    #  handle background
    #  handle adjust distance between vectors of surface of contact

    # in compute distance ensure you're computing differences between same dimensions(neighbouring tissues)

    script_content = f"""
import os
import matplotlib.pyplot as plt
import numpy as np

def plot_tissue_contact_distances(results):
    def compute_distance(surface1, surface2):
        common_keys = set(surface1.keys()).intersection(surface2.keys())
        distance = 0
        for key in common_keys:
            distance += np.linalg.norm(np.array(surface1[key]) - np.array(surface2[key]))
        normalization_factor = np.sum(list(surface1.values())) + np.sum(list(surface2.values()))
        return distance / normalization_factor 
    distances = {{}}

    # Collect all tissue names
    tissues = set()
    for timepoint_data in results.values():
        for embryo_data in timepoint_data.values():
            tissues.update(embryo_data.keys())
    tissues = sorted(tissues, key=lambda x: x.split('_')[0])

    # Compute distances
    distances = {{tissue: {{}} for tissue in tissues}}
    sorted_times = sorted(results.keys())

    for tissue in tissues:
        for embryo_name in results[sorted_times[0]].keys():
            distances[tissue][embryo_name] = []
            for i in range(len(sorted_times) - 1):
                t1, t2 = sorted_times[i], sorted_times[i + 1]
                if embryo_name in results[t1] and embryo_name in results[t2]:
                    if tissue in results[t1][embryo_name] and tissue in results[t2][embryo_name]:
                        surface_of_contact_t1 = results[t1][embryo_name][tissue]
                        surface_of_contact_t2 = results[t2][embryo_name][tissue]
                        distance_t = compute_distance(surface_of_contact_t1, surface_of_contact_t2)
                        distances[tissue][embryo_name].append((t1, t2, distance_t))

    # distances = dict([embryoname][tissue][(t1,t2,dist between same tissue at different t)])
    # Plotting
    num_figures = (len(tissues) + 3) // 4  # Calculate the number of figures needed
    for fig_idx in range(num_figures):
        fig, axes = plt.subplots(4, 1, figsize=(14, 8), sharex=True)
        axes = axes.flatten()

        start_idx = fig_idx * 4
        end_idx = min(start_idx + 4, len(tissues))
        for idx, tissue in enumerate(tissues[start_idx:end_idx]):
            ax = axes[idx]
            embryo_dict = distances[tissue]
            for embryo_name, dist_list in embryo_dict.items():
                timepoints = [t2 for t1, t2, dist in dist_list]
                dists = [dist for t1, t2, dist in dist_list]
                ax.plot(timepoints, dists, marker='o', label=f'{{embryo_name}}')
                ax.set_ylim(0,1)

            if idx == 0:
                ax.legend(loc='center right')
            if tissue == 'Unknown':
                tissue = 'Background'
                continue
            ax.set_title(f'Stability of {{tissue}} Surface of Contact')

        # Remove unused subplots
        for idx in range(end_idx - start_idx, 4):
            fig.delaxes(axes[idx])

        # Adjust layout to make room for shared labels
        plt.tight_layout(rect=[0, 0, 1, 0.96])

        # Set xlabel, ylabel, and legend for the whole figure
        fig.text(0.5, 0.02, 'Timepoints', ha='center', va='center')
        fig.text(0.01, 0.5, 'Distance between surfaces of contact', ha='center', va='center', rotation='vertical')
        #fig.legend(loc='upper center', bbox_to_anchor=(0.5, 1.02), ncol=len(distances))

        plt.show()

results = {results}  
plot_tissue_contact_distances(results)
    """
    # Write the generated script to the specified file
    with open(output_path, 'w') as script_file:
        script_file.write(script_content)

    print(f"Plot script saved to {output_path}")


def script_plot_tissue_surface_of_contact_stability_to_ref_t(results, output_path):
    """
    Generates a Python script to plot the distances between the surfaces of contact between tissues
    in the same embryo at consecutive timepoints.

    Parameters
    ----------
    results (dict): Dictionary containing surface of contact data.
    output_path (str): Path to save the generated script.

    Returns
    -------
    None
    """
    #  handle background
    #  handle adjust distance between vectors of surface of contact

    # in compute distance ensure you're computing differences between same dimensions(neighbouring tissues)

    script_content = f"""
import os
import matplotlib.pyplot as plt
import numpy as np

def plot_tissue_contact_distances(results):
    def compute_distance(surface1, surface2):
        common_keys = set(surface1.keys()).intersection(surface2.keys())
        distance = 0
        for key in common_keys:
            distance += np.linalg.norm(np.array(surface1[key]) - np.array(surface2[key]))
        normalization_factor = np.sum(list(surface1.values())) + np.sum(list(surface2.values()))
        return distance / normalization_factor 

    distances = {{}}

    # Collect all tissue names
    tissues = set()
    for timepoint_data in results.values():
        for embryo_data in timepoint_data.values():
            tissues.update(embryo_data.keys())
    tissues = sorted(tissues)

    # Compute distances
    distances = {{tissue: {{}} for tissue in tissues}}
    sorted_times = sorted(results.keys())

    for tissue in tissues:
        for embryo_name in results[sorted_times[0]].keys():
            distances[tissue][embryo_name] = []
            for i in range(len(sorted_times) - 1):
                t1, t2 = sorted_times[0], sorted_times[i]
                if embryo_name in results[t1] and embryo_name in results[t2]:
                    if tissue in results[t1][embryo_name] and tissue in results[t2][embryo_name]:
                        surface_of_contact_t1 = results[t1][embryo_name][tissue]
                        surface_of_contact_t2 = results[t2][embryo_name][tissue]
                        distance_t = compute_distance(surface_of_contact_t1, surface_of_contact_t2)
                        distances[tissue][embryo_name].append((t1, t2, distance_t))

    # Plotting
    num_figures = (len(tissues) + 3) // 4  # Calculate the number of figures needed
    for fig_idx in range(num_figures):
        fig, axes = plt.subplots(4, 1, figsize=(14, 8), sharex=True)
        axes = axes.flatten()

        start_idx = fig_idx * 4
        end_idx = min(start_idx + 4, len(tissues))
        for idx, tissue in enumerate(tissues[start_idx:end_idx]):
            ax = axes[idx]
            embryo_dict = distances[tissue]
            for embryo_name, dist_list in embryo_dict.items():
                timepoints = [t2 for t1, t2, dist in dist_list]
                dists = [dist for t1, t2, dist in dist_list]
                ax.plot(timepoints, dists, marker='o', label=f'{{embryo_name}}')

            if idx == 0:
                ax.legend(loc='center right')
            if tissue == 'Unknown':
                tissue = 'Background'
                continue
            ax.set_title(f'Stability of {{tissue}} Surface of Contact')
            ax.set_ylim(0,1)

        # Remove unused subplots
        for idx in range(end_idx - start_idx, 4):
            fig.delaxes(axes[idx])

        # Adjust layout to make room for shared labels
        plt.tight_layout(rect=[0, 0, 1, 0.96])

        # Set xlabel, ylabel, and legend for the whole figure
        fig.text(0.5, 0.02, 'Timepoints', ha='center', va='center')
        fig.text(0.01, 0.5, 'Distance between surfaces of contact', ha='center', va='center', rotation='vertical')
        #fig.legend(loc='upper center', bbox_to_anchor=(0.5, 1.02), ncol=len(distances))

        plt.show()

results = {results}  
plot_tissue_contact_distances(results)
    """
    # Write the generated script to the specified file
    with open(output_path, 'w') as script_file:
        script_file.write(script_content)

    print(f"Plot script saved to {output_path}")


def script_plot_tissue_surface_of_contact_variability(results, reference_embryo, output_path):
    """
    Generates a Python script to plot the distances between the pairs of surfaces of contact between tissues in
    different embryos.

    Parameters
    ----------
    results (dict): Dictionary containing surface of contact data.
    reference_embryo (str): reference embryo name
    output_path (str): Path to save the generated script.

    Returns
    -------
    None
    """

    script_content = f"""
import os
import matplotlib.pyplot as plt
import numpy as np

def plot_tissue_contact_distances(results, reference_embryo):
    def compute_normalized_distance(surface1, surface2):
        common_keys = set(surface1.keys()).intersection(surface2.keys())
        distance = 0
        for key in common_keys:
            distance += np.linalg.norm(np.array(surface1[key]) - np.array(surface2[key]))
        normalization_factor = np.sum(list(surface1.values())) + np.sum(list(surface2.values()))
        return distance / normalization_factor 

    distances = {{}}
    tissues = list(set(tissue for timepoint in results.values() for embryo in timepoint.values() for tissue in embryo))
    tissues = sorted(tissues)
    sorted_times = sorted(results.keys())
    embryos = list(set(embryo for timepoint in results.values() for embryo in timepoint.keys()))

    for tissue in tissues:
        distances[tissue] = {{}}
        for t in sorted_times:
            distances[tissue][t] = []
            for i, embryo1 in enumerate(embryos):
                for j, embryo2 in enumerate(embryos):
                    if i >= j:
                        continue
                    if embryo1 in results[t] and embryo2 in results[t]:
                        if tissue in results[t][embryo1] and tissue in results[t][embryo2]:
                            dist = compute_normalized_distance(
                                results[t][embryo1][tissue], results[t][embryo2][tissue]
                            )
                            distances[tissue][t].append(dist)

    num_figures = (len(tissues) + 3) // 4  # Calculate the number of figures needed
    for fig_idx in range(num_figures):
        fig, axes = plt.subplots(4, 1, figsize=(14, 8), sharex=True, sharey=True)
        axes = axes.flatten()

        start_idx = fig_idx * 4
        end_idx = min(start_idx + 4, len(tissues))

        for idx, tissue in enumerate(tissues[start_idx:end_idx]):
            ax = axes[idx]
            time_dict = distances[tissue]
            data = [time_dict[t] for t in sorted_times if len(time_dict[t]) > 0]
            positions = [t for t in sorted_times if len(time_dict[t]) > 0]
            if data:
                ax.boxplot(data, positions=positions, widths=0.6)
            if tissue == 'Unknown':
                tissue = 'Background'
                continue
            ax.set_title(f'Variability of {{tissue}} Surface of Contact Across Embryos')
            ax.set_ylim(0,1)
            # Set x-tick labels selectively
            if idx == end_idx - start_idx - 1:  # Only modify the x-tick labels for the last subplot
                xtick_labels = ax.get_xticklabels()
                for i, label in enumerate(xtick_labels):
                    if i % 30 != 0:  # Show every 30th label
                        label.set_visible(False)

        # Remove unused subplots
        for idx in range(end_idx - start_idx, 4):
            fig.delaxes(axes[idx])

        plt.tight_layout(rect=[0, 0, 1, 0.96])
        fig.text(0.5, 0.01, 'Timepoints', ha='center', va='center')
        fig.text(0.01, 0.5, 'Distance between surfaces of contact', ha='center', va='center', rotation='vertical')
        #fig.legend(loc='upper center', bbox_to_anchor=(0.5, 1.02), ncol=len(distances))

        plt.show()

results = {results}
plot_tissue_contact_distances(results, '{reference_embryo}')
        """
    # Write the generated script to the specified file
    with open(output_path, 'w') as script_file:
        script_file.write(script_content)

    print(f"Plot script saved to {output_path}")

def write_script_plot_embryo_fig(output_file, cell_barycenters_at_t, cells_fate):
    """
    Writes a Python script that plots the 3D positions of cells with colors depending on their fates.

    Parameters
    ----------
    output_file : str
        The name of the file to write the script to.
    cell_barycenters_at_t : dict
        Dictionary with cell IDs as keys and their 3D barycenter positions as values.
        Format: {'cell_id': [x, y, z]}
    cells_fate : dict
        Dictionary with cell IDs as keys and their fates as values.
        Format: {'cell_id': 'cell_fate'}
    """
    unique_fates = list(set(cells_fate.values()))
    cell_barycenters_at_t_list = {cell_id: barycenter.tolist() for cell_id, barycenter in cell_barycenters_at_t.items()}

    script_content = f"""
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.colors as mcolors

unique_fates = {unique_fates}

colors = list(mcolors.TABLEAU_COLORS.values())  # Use a predefined color set
if len(unique_fates) > len(colors):
    # If there are more fates than available colors, generate additional colors
    cmap = plt.get_cmap('hsv', len(unique_fates))
    colors = [mcolors.rgb2hex(cmap(i)) for i in range(len(unique_fates))]

# Create color map for fates
color_map = {{fate: colors[i] for i, fate in enumerate(unique_fates)}}

def determine_fate_color(fate):
    return color_map.get(fate, 'black')  # default color is black if fate not found

def plot_embryo_fig(cell_barycenters_at_t, cells_fate):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    for cell_id, barycenter in cell_barycenters_at_t.items():
        fate = cells_fate.get(cell_id, 'unknown')
        color = determine_fate_color(fate)
        ax.scatter(barycenter[0], barycenter[1], barycenter[2], c=color, label=fate, s=20)

    # Create a legend
    handles, labels = ax.get_legend_handles_labels()
    by_label = dict(zip(labels, handles))
    ax.legend(by_label.values(), by_label.keys())

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.set_title('3D plot of cells colored by fate')

    plt.show()

cell_barycenters_at_t = {cell_barycenters_at_t_list}
cells_fate = {cells_fate}

plot_embryo_fig(cell_barycenters_at_t, cells_fate)
    """

    with open(output_file, 'w') as file:
        file.write(script_content)
import pickle as pkl
import os
import numpy as np

from ascidian.division_analysis.division_analysis_init_tools import get_first_last_timestamps, get_embryo_at_t
import ascidian.ascidian.name as uname


# todo move to core

# folder_path = "/Users/haydar/Desktop/gm2hj_with_3_way_junctions_btw_sisters"
keys_wanted = ['cell_volume', 'cell_surface', 'cell_barycenter',
               'cell_name', 'cell_lineage', 'cell_fate',
               'cell_principal_values', 'cell_principal_vectors',
               'cell_contact_surface',
               'cell_contact_edge_length', 'cell_contact_edge_segment', # 3 way junctions(edge) and their length
               'cell_contact_surface_principal_values', 'cell_contact_surface_principal_vectors', # computed from points of surface(all the interface) seperating 2 cells
               'cell_contact_surface_barycenter', # interface barycenter
               'cell_contact_edges_principal_values', 'cell_contact_edges_principal_vectors', # coputed from lines between cells 3way points
               'cell_contact_edges_barycenter', # 3way lines barycenters
               'morphonet_selection_tissuefate_guignard_2020', 'morphonet_selection_tissuefate_lemaire_2009']


def filter_property_file(files, folder_path):
    """
    Filters properties from pickle files based on a list of desired keys.
    transform 3 way junctions bteween sisters to vector normal to plane of the interface between sisters
    Filters 3 way junctions by only keeping apical 3way junctions

    Parameters:
        files (list): Path to the pkl files of embryos properties. (same location to store the filtered pickles)
        folder_path (str) : output folder
    Returns:
        None

    """
    # Get every file at its turn
    for filename in files:
        print("starting", filename)
        filtered_props = {}
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)

        for key in keys_wanted:
            if key == 'cell_contact_surface_principal_vectors':

                print("approximating sisters interfaces")
                # estimate plane between sisters from sisters hall interface
                filtered_props = filter_sisters_interface(filtered_props, pm_props[key], 'sisters_interface_vectors')
                filtered_props = filter_sisters_interface(filtered_props, pm_props['cell_contact_surface_principal_values'],
                                                     'sisters_surface_principal_values')
                print("approximating cell apical surface")
                # filter directions
                filtered_props = filter_apical_surface_from_interface(filtered_props, pm_props[key], 'apical_surface_principal_vectors')
                # filter principal values
                filtered_props = filter_apical_surface_from_interface(filtered_props, pm_props['cell_contact_surface_principal_values'],
                                                    'apical_surface_principal_values')

            elif key == 'cell_contact_edges_principal_vectors':

                print("approximating sisters interfaces, from lines")
                filtered_props = filter_sisters_interface(filtered_props, pm_props[key], 'sisters_interface_edges_vectors')
                filtered_props = filter_sisters_interface(filtered_props, pm_props['cell_contact_edges_principal_values'],
                                         'sisters_surface_edges_principal_values')
                print("approximating cell apical surface, from lines")
                filtered_props = filter_apical_surface_from_interface(filtered_props, pm_props[key], 'apical_surface_edges_principal_vectors')
                # filter principal values
                filtered_props = filter_apical_surface_from_interface(filtered_props, pm_props['cell_contact_edges_principal_values'],
                                                    'apical_surface_edges_principal_values')
            else:
                filtered_props[key] = pm_props[key]

        output_filename = os.path.join(folder_path, os.path.basename(filename))
        with open(output_filename, 'wb') as output_file:
            pkl.dump(filtered_props, output_file)
            print("done", output_filename)


def filter_apical_surface_from_interface(filtered_props, edge_segments_dict, key):
    """
    filter the principal vectors and values
    from edge_segments_dict (has the principal values/vectors  from lines/interfaces between 3way junctions)

    Parameters
    ----------
    filtered_props (dict): dict where to add ur data
    edge_segments_dict (dict): dict of properties to retrieve
    key (str) : key of values to be stored

    Returns
    -------
    filtered_props
    """
    # Get only background cell contact
    if key not in filtered_props:
        filtered_props[key] = {}
    for cell_id in edge_segments_dict.keys():
        for contact_cell_id in edge_segments_dict[cell_id].keys():
            t = int(contact_cell_id / 10000)
            label = contact_cell_id - (10000 * t)
            # print('id:', contact_cell_id,'t', t, 'label', label)
            if label == 1:
                # Only keep background contacts
                filtered_props[key][cell_id] = edge_segments_dict[cell_id][contact_cell_id]
    return filtered_props


def filter_sisters_interface(filtered_props, edge_segments_dict, key):
    """
    filter the principal vectors and values
    from edge_segments_dict (has the principal values/vectors
    from interfaces/lines between 3way junctions) of sister cells

    Parameters
    ----------
    filtered_props (dict): dict where to add ur data
    edge_segments_dict (dict): dict of properties to retrieve
    key (str): key of values to be stored

    Returns
    -------
    filtered_props
    """
    # get a name dict
    dict_names_pm = filtered_props['cell_name']
    # get aq times of embryo
    first_time, last_time = get_first_last_timestamps(dict_names_pm)

    # at each time frame
    for t in range(first_time, last_time + 1):
        # get embryo at t
        edge_segments_at_t = get_embryo_at_t(edge_segments_dict, t)

        # fr each cell
        for cell_id, neighbouring_edges_props in edge_segments_at_t.items():
            if cell_id not in dict_names_pm:
                continue
            cell_name = dict_names_pm[cell_id]
            sister_name = uname.get_sister_name(cell_name)

            for neighbour_id, edges_props in neighbouring_edges_props.items():
                # edges_props is either principal vectors or values
                if neighbour_id not in dict_names_pm:
                    continue

                if dict_names_pm[neighbour_id] == sister_name:  # if sister is found
                    # save principal values or vectors
                    if key not in filtered_props:
                        filtered_props[key] = {}
                    filtered_props[key][cell_id] = edge_segments_dict[cell_id][neighbour_id]
    return filtered_props
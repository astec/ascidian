import os

from ascidian.core.division_analysis_init_tools import (reduce_to_closest_ancestry, get_embryo_at_t,
                                                        global_similitude, nb_of_cells, apply_transformation)
from ascidian.core.figure_variability import determine_fate_group
from ascidian.core.registration_assessment_tools import (write_registration_residues_script, compute_residual_dictionary,
                                                        local_register, get_neighborhood_dicts, update_residues_dict)
from ascidian.core.ascidian_name import get_daughter_names


def local_registration_cells_residues(embryo_props, t1, t2, embryo_name, output_folder,
                                      transformation_type='similitude'):
    """

    Parameters
    ----------
    embryo_props :(nested dict) embryo properties
    t1 : (int) acquisition time of reference frame
    t2 : (int) acquisition time of floating frame
    embryo_name
    output_folder
    transformation_type

    Returns
    -------

    """
    # take frame t1 : ids
    pm_names = embryo_props['cell_name']
    dict_names_at_t1 = get_embryo_at_t(pm_names, t1)
    dict_names_at_t2 = get_embryo_at_t(pm_names, t2)

    # get barycenters at t1 and t2
    pm_barycenters = embryo_props['cell_barycenter']
    dict_barycenters_at_t1 = get_embryo_at_t(pm_barycenters, t1)
    dict_barycenters_at_t2 = get_embryo_at_t(pm_barycenters, t2)

    # get cell volumes at t1 and t2
    pm_cell_volumes = embryo_props['cell_volume']
    dict_cell_volume_at_t1 = get_embryo_at_t(pm_cell_volumes, t1)
    dict_cell_volume_at_t2 = get_embryo_at_t(pm_cell_volumes, t2)

    residues_dict = {}
    ##
    ##
    # for each cell, take its neighborhood
    for cell_id, cell_name in dict_names_at_t1.items():

        # take cell and its neighborhood's name, volume, barycenter if id in pm_contact_surfaces_dict[cell_id]
        neighborhood_names_t1, neighborhood_volumes_t1, neighborhood_barycenters_t1 = get_neighborhood_dicts(cell_id,
                                                                                                             dict_names_at_t1,
                                                                                                             dict_cell_volume_at_t1,
                                                                                                             dict_barycenters_at_t1,
                                                                                                             embryo_props)

        # do cell name exist in t2? else it daughters ?
        if cell_name in list(dict_names_at_t2.values()):
            # get its neighboorhood as above
            cell_id_t2 = next(i for i, name in dict_names_at_t2.items() if name == cell_name)
            neighborhood_names_t2, neighborhood_volumes_t2, neighborhood_barycenters_t2 = get_neighborhood_dicts(
                cell_id_t2,
                dict_names_at_t2,
                dict_cell_volume_at_t2,
                dict_barycenters_at_t2,
                embryo_props)


        else:
            # merge its daughters, merge their neighbors and continue
            # get daughter's names
            d1, d2 = get_daughter_names(cell_name)
            # get daughters ids, volumes, barycenters of neighbours
            d1_id = next(i for i, name in dict_names_at_t2.items() if name == d1)
            neighbouring_ids_t2 = list(embryo_props['cell_contact_surface'][d1_id].keys())
            d2_id = next(i for i, name in dict_names_at_t2.items() if name == d2)
            neighbouring_ids_t2.extend(list(embryo_props['cell_contact_surface'][d2_id].keys()))

            # get neighbouring ids, names, volumes
            neighborhood_names_t2 = {i: name for i, name in dict_names_at_t2.items() if i in neighbouring_ids_t2}
            neighborhood_volumes_t2 = {i: v for i, v in dict_cell_volume_at_t2.items() if i in neighbouring_ids_t2}
            neighborhood_barycenters_t2 = {i: bary for i, bary in dict_barycenters_at_t2.items() if
                                           i in neighbouring_ids_t2}

        # most common ancestry:
        # merge each daughter of a cell in ref

        reduced_barycenters_t1, reduced_barycenters_t2, \
            reduced_cell_name_t1, reduced_cell_name_t2 = reduce_to_closest_ancestry(neighborhood_barycenters_t1,
                                                                                    neighborhood_barycenters_t2,
                                                                                    neighborhood_volumes_t1,
                                                                                    neighborhood_volumes_t2,
                                                                                    neighborhood_names_t1,
                                                                                    neighborhood_names_t2)
        # compute local registration
        transform, ref, floating, sorted_cell_name = local_register(list(reduced_barycenters_t1.values()),
                                       list(reduced_cell_name_t1.values()),
                                       list(reduced_barycenters_t2.values()),
                                       list(reduced_cell_name_t2.values())
                                       )
        transformed_floating = apply_transformation(transform, floating)
        residues_dict = update_residues_dict(sorted_cell_name, cell_name, transformed_floating, ref, residues_dict)

    # get residue list, cell_names list
    cell_names = list(residues_dict.keys())
    residues = list(residues_dict.values())
    fates = give_cells_fates(list(residues_dict.keys()), embryo_props['cell_name'], embryo_props['cell_fate'])

    # script to plot
    output_directory = os.path.join(output_folder, "temporal_frames_registration_residues", embryo_name,
                                    f"Local{transformation_type}")
    os.makedirs(output_directory, exist_ok=True)
    output_file = os.path.join(output_directory, f"{t1}_{t2}.py")

    write_registration_residues_script(cell_names, residues, fates, embryo_name, t1, t2, output_file)


def global_registration_cells_residues(embryo_props, t1, t2, embryo_name, output_folder,
                                       transformation_type='similitude'):
    """
    Perform global similitude between two embryo frames

    Parameters
    ----------
    embryo_props (nested dict): embryo properties
    t1 (int): acquisition time of reference frame
    t2 (int): acquisition time of floating frame

    Returns
    -------
    registration residuals for each cell in ref frame
    """

    # take frame t1 : ids
    pm_names = embryo_props['cell_name']
    dict_names_at_t1 = get_embryo_at_t(pm_names, t1)
    dict_names_at_t2 = get_embryo_at_t(pm_names, t2)

    # get barycenters at t1 and t2
    pm_barycenters = embryo_props['cell_barycenter']
    dict_barycenters_at_t1 = get_embryo_at_t(pm_barycenters, t1)
    dict_barycenters_at_t2 = get_embryo_at_t(pm_barycenters, t2)

    # get cell volumes at t1 and t2
    pm_cell_volumes = embryo_props['cell_volume']
    dict_cell_volume_at_t1 = get_embryo_at_t(pm_cell_volumes, t1)
    dict_cell_volume_at_t2 = get_embryo_at_t(pm_cell_volumes, t2)

    # most common ancestry:
    # merge each daughter of a cell in ref

    reduced_barycenters_t1, reduced_barycenters_t2, \
        reduced_cell_name_t1, reduced_cell_name_t2 = reduce_to_closest_ancestry(
        dict_barycenters_at_t1, dict_barycenters_at_t2,
        dict_cell_volume_at_t1, dict_cell_volume_at_t2,
        dict_names_at_t1, dict_names_at_t2
    )

    # compute transform
    nb_cells_t1_after = nb_of_cells(reduced_barycenters_t1)
    nb_cells_t2_after = nb_of_cells(reduced_barycenters_t2)

    if nb_cells_t1_after == nb_cells_t2_after:
        # Global registration
        transform, floating, ref_vector, sorted_names = global_similitude(reduced_barycenters_t1,
                                                                          reduced_cell_name_t1,
                                                                          reduced_barycenters_t2,
                                                                          reduced_cell_name_t2)

        # apply transform on flo
        transformed_floating = apply_transformation(transform, floating)
        # transformed_floating shape : (3,N), N number of cells
        # names are in reduced_cell_name_t2

        # construct dictionary : {'cell_name': residue }
        distance_dict = compute_residual_dictionary(transformed_floating, ref_vector, sorted_names)

        #  write script boxplot of all cells color by fate
        output_directory = os.path.join(output_folder, "temporal_frames_registration_residues", embryo_name,
                                        f"Global{transformation_type}")
        os.makedirs(output_directory, exist_ok=True)
        output_file = os.path.join(output_directory, f"{t1}_{t2}.py")

        fates = give_cells_fates(list(distance_dict.keys()), embryo_props['cell_name'], embryo_props['cell_fate'])
        write_registration_residues_script(list(distance_dict.keys()), list(distance_dict.values()), fates,
                                           embryo_name, t1, t2, output_file)
        return distance_dict
    else:
        print("pb in most common ancestor")


def give_cells_fates(list_names, names_dict, fates_dict):
    """
    Retrieve the fates of cells from their names.

    Parameters:
    list_names (list): List of cell names.
    names_dict (dict): Dictionary mapping cell names to keys.
    fates_dict (dict): Dictionary mapping keys to cell fates.

    Returns:
    list: List of fates corresponding to the input list of names.
    """
    fates = []
    for name in list_names:
        key = next(key for key, value in names_dict.items() if value == name)
        cell_fates = determine_fate_group(fates_dict.get(key, None), check_multiple=True)
        fates.append(cell_fates)
    return fates

import pickle as pkl
import os
import numpy as np

from ascidian.core.division_analysis_init_tools import get_first_last_timestamps, get_embryo_at_t
import ascidian.core.ascidian_name as uname

# TODO file useless to be deleted, and move to core


# folder_path = "/Users/haydar/Desktop/gm2hj_with_3_way_junctions_btw_sisters"
keys_wanted = ['cell_name', 'cell_volume', 'cell_surface', 'cell_barycenter', 'cell_principal_values', 'cell_principal_vectors',
               'cell_contact_surface', 'cell_contact_edge_segment', 'cell_lineage', 'cell_fate',
               'morphonet_selection_tissuefate_guignard_2020', 'morphonet_selection_tissuefate_lemaire_2009']


def filter_properties_sister_junctions_to_plane_and_apical_junctions(files, folder_path):
    """
    Filters properties from pickle files based on a list of desired keys.
    transform 3 way junctions bteween sisters to vector normal to plane of the interface between sisters
    Filters 3 way junctions by only keeping apical 3way junctions

    Parameters:
        files (list): Path to the pkl files of embryos properties. (same location to store the filtered pickles)
        folder_path (str) : output folder
    Returns:
        None

    """

    # Get every file at its turn
    for filename in files:
        print("starting", filename)
        filtered_props = {}
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)

        for key in keys_wanted:
            if key == 'cell_contact_edge_segment':
                print("approximating sisters interfaces")
                # from 3way junctions between sisters estimate plane between sisters
                process_contact_edge_segments_to_vector_normal_to_sister_interface(filtered_props, pm_props[key], filename)
                # add apical coords as property, use filter fct
                print("filtering apical junctions")
                filter_apical_3way_junctions(filtered_props, pm_props[key])
            else:
                filtered_props[key] = pm_props[key]

            # filtered_props[key] = pm_props[key]

        output_filename = os.path.join(folder_path, os.path.basename(filename))
        with open(output_filename, 'wb') as output_file:
            pkl.dump(filtered_props, output_file)
            print("done", output_filename)


def filter_apical_3way_junctions(filtered_props, edge_segments_dict):
    """
    filter_apical_3way_junctions from edge_segments_dict (has all cells 3 way junctions)
    Parameters
    ----------
    filtered_props
    edge_segments_dict

    Returns
    -------
    filtered_props
    """
    # Get only background cell contact
    filtered_props['cell_apical_3way_junctions'] = {}
    for cell_id in edge_segments_dict.keys():
        filtered_props['cell_apical_3way_junctions'][cell_id] = {}
        for contact_cell_id in edge_segments_dict[cell_id].keys():
            t = int(contact_cell_id / 10000)
            label = contact_cell_id - (10000 * t)
            if label == 1:
                # Only keep background contacts
                filtered_props['cell_apical_3way_junctions'][cell_id][contact_cell_id] = edge_segments_dict[cell_id][contact_cell_id]
    return filtered_props


def process_contact_edge_segments_to_vector_normal_to_sister_interface(filtered_props, edge_segments_dict, filename):
    """
    Process the cell contact edge segments.
    Add sisters plane as a property to filtered_props
    Parameters:
        edge_segments (dict): Dictionary containing cell contact edge segments.

    Returns:
        None
    """
    # get a property dict
    dict_names_pm = filtered_props['cell_name']
    # get aq times of embryo
    first_time, last_time = get_first_last_timestamps(dict_names_pm)

    # at each time frame
    for t in range(first_time, last_time+1):
        # get embryo at t
        edge_segments_at_t = get_embryo_at_t(edge_segments_dict, t)

        # fr each cell
        for cell_id, neighbouring_edges_props in edge_segments_at_t.items():
            if cell_id not in dict_names_pm:
                continue
            cell_name = dict_names_pm[cell_id]
            sister_name = uname.get_sister_name(cell_name)
            if (cell_name == 'a10.0027*' or cell_name =='a10.0028*') and filename == '/Users/haydar/Desktop/gm2hj_full_props/Astec-pm4-fullproperties.pkl':
                print('last time', last_time)
                print("cell name", cell_name, "sister cell name", sister_name)

            for neighbour_id, edges_props in neighbouring_edges_props.items():
                if neighbour_id not in dict_names_pm:
                    if (cell_name == 'a10.0027*' or cell_name =='a10.0028*') and filename == '/Users/haydar/Desktop/gm2hj_full_props/Astec-pm4-fullproperties.pkl':
                        print("neighbour id without name", neighbour_id)
                    continue

                coordinates_list = []
                if dict_names_pm[neighbour_id] == sister_name:  # if sister is found
                    for t_id, t_coords in edge_segments_at_t[cell_id][neighbour_id].items():
                        for coord in t_coords:
                            coordinates_list.append(coord)
                if (cell_name == 'a10.0027*' or cell_name =='a10.0028*') and filename == '/Users/haydar/Desktop/gm2hj_full_props/Astec-pm4-fullproperties.pkl':
                    print("neighbour id", neighbour_id, 'neighbour name', dict_names_pm[neighbour_id], 'nb of coords', len(coordinates_list), )
                if len(coordinates_list) != 0: # if a sister has been found, add
                    # approximate vector normal to plane defined by
                    a,b,c,d = approximate_plane_by_least_square(coordinates_list)
                    vector_normal_to_plane = np.array([a,b,c]).reshape(3, 1)  # needs to have shape (3,1)

                    # store vector in filtered dict
                    if 'cell_vector_normal_to_sister_interface' not in filtered_props:
                        filtered_props['cell_vector_normal_to_sister_interface'] = {}
                    filtered_props['cell_vector_normal_to_sister_interface'][cell_id] = vector_normal_to_plane

                    if (cell_name == 'a10.0027*' or cell_name == 'a10.0028*') and filename == '/Users/haydar/Desktop/gm2hj_full_props/Astec-pm4-fullproperties.pkl':
                        print('cell name targeted saved info, to cell id:', cell_id, 'vector:',vector_normal_to_plane)

                #else:
                #    # if no sister has been found
                #    print('following cell has no neighbouring with sister cell')
                #    print("cell name", cell_name, 'cell id', cell_id,"sister name", sister_name)

    return filtered_props


def approximate_plane_by_least_square(coordinates):
    """
    Approximates the plane defined by a list of 3D coordinates using the least squares method.

    The system of equations is represented as:

    \[
    \begin{align*}
    ax_1 + by_1 + cz_1 + d &= z_1 \\
    ax_2 + by_2 + cz_2 + d &= z_2 \\
    & \vdots \\
    ax_n + by_n + cz_n + d &= z_n \\
    \end{align*}
    \]
    This system can be represented in matrix form as \( Ax = b \), where:

    \[
    A = \begin{bmatrix}
    x_1 & y_1 & 1 \\
    x_2 & y_2 & 1 \\
    \vdots & \vdots & \vdots \\
    x_n & y_n & 1 \\
    \end{bmatrix}
    \]

    \[
    x = \begin{bmatrix}
    a \\
    b \\
    d \\
    \end{bmatrix}
    \]

    \[
    b = \begin{bmatrix}
    z_1 \\
    z_2 \\
    \vdots \\
    z_n \\
    \end{bmatrix}
    \]

    The solution \( x \) obtained using the least squares method provides the coefficients \( a, b, \) and \( d \) of
    the plane's equation. Finally, the normal vector is normalized to unit length.


    Parameters:
        coordinates (list of arrays): List of 3D coordinates.

    Returns:
        tuple: A tuple containing the normal vector (a, b, c) and the distance (d) from the origin to the plane.
    """
    # Ensure coordinates are numpy arrays
    coordinates = np.array(coordinates)     # shape (N,3)
    # Construct the design matrix
    A = np.column_stack((coordinates[:, 0], coordinates[:, 1], np.ones(len(coordinates))))

    # Solve Ax = b using least squares method
    x, _, _, _ = np.linalg.lstsq(A, coordinates[:, 2], rcond=None)

    # Extract coefficients (a, b, c) and distance (d)
    a, b, d = x
    c = -1

    # Normalize the normal vector
    length = np.sqrt(a**2 + b**2 + c**2)
    a /= length
    b /= length
    c /= length
    d /= length

    return a, b, c, d
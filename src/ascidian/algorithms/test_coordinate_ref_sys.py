import pickle as pkl
import os
import numpy as np

from ascidian.algorithms.division_analysis_init import get_temporal_alignment_coefficients_of_embryos
import ascidian.core.temporal_alignment as utimes
import ascidian.ascidian.name as uname
from ascidian.division_analysis.division_analysis_tools import average_t_for_cells
from ascidian.division_analysis.division_analysis_init_tools import (get_embryo_at_t, estimate_sym_vector,
                                                        reduce_to_closest_ancestry,
                                                        global_similitude, nb_of_cells, apply_transformation,
                                                        update_barycenters)

############
######
# posterior_lineage_used_for_axis_computation = ['a6.0002', 'a7.0004',  'a8.0007','a8.0008', 'a9.0014', 'a9.0016',
# 'a10.0028', 'a10.0032', 'a10.0027', 'a10.0031']

# anterior_lineage_used_for_axis_computation = ['a6.0005','a7.0010', 'a7.0009', 'a8.0017', 'a8.0019', 'a9.0033','a9.0034',
#                                              'a9.0037', 'a9.0038']

#ant_names = ['a6.0005*', 'a6.0005_']
#post_names = ['a6.0002*', 'a6.0002_', 'a6.0004*', 'a6.0004_']

#ant_names = ['a7.0012*', 'a7.0012_']
#post_names = ['a7.0011*', 'a7.0011_']

left_lineage_used_for_axis_computation = ['']

right_lineage_used_for_axis_computation = ['']


############
#####


def change_embryo_time_range_in_aq_time(delta_t=30, coef=(0, 1)):
    """
    Translates embryo range of times delta_t from dev time to aq time

    Parameters
    ----------
    delta_t
    coef

    Returns
    -------
    delta_t (int): a*delta_t
    """
    return delta_t * coef[0]


def get_lineages_ids(dict_names_at_t, post_names_list):
    """
    Returns list of anterior cells and posterior cells ids
    Anterior cells are lineage of a6.5
    Posterior cells are lineage of A6.2 and A6.4

    Parameters
    ----------
    post_names_list (list) : list of lineage names to be extracted
    dict_names_at_t : dict
        Dictionary with cell names as values and their corresponding IDs as keys.

    Returns
    -------
    ids_post (list) : ids of posterior cells, lineage of A6.2 and A6.4
    """

    list_names = list(dict_names_at_t.values())
    post_ids = []

    # Search for posterior cells
    generation = 6
    while post_names_list and generation < 12:
        next_post_names = []
        for name in post_names_list:
            if name in list_names:
                post_ids.append(list(dict_names_at_t.keys())[list_names.index(name)])
            else:
                next_post_names.extend(uname.get_daughter_names(name))
        post_names_list = next_post_names
        generation += 1

    return post_ids


def estimate_lineage_axis(dict_barycenters, dict_names_at_t, dict_cell_mass, post_names, ant_names):
    """
    Gives estimation of posterior-anterior axis of embryo

    Given dictionary of barycenters and names:
    Posterior cells: lineage of A6.2 and A6.4
    Anterior cells: lineage of a6.5

    Parameters:
        dict_barycenters (dict): {cell_id: [barycenter]}
        dict_names_at_t (dict): {cell_id: cell_name}
        dict_cell_mass (dict): {cell_id: cell_mass}

    Returns:
        ante_post_axis (numpy array): The vector joining the barycenter of anterior cells with the barycenter of posterior cells
    """

    # Get posterior cells ids, anterior cells ids
    post_ids = get_lineages_ids(dict_names_at_t, post_names)
    ant_ids = get_lineages_ids(dict_names_at_t, ant_names)

    if post_ids == [] or ant_ids == []:
        return np.zeros((3, 1))

    # Get barycenters for posterior and anterior cells
    #print(post_ids, ant_ids)
    post_barycenters = np.array([dict_barycenters[cell_id] for cell_id in post_ids])
    ant_barycenters = np.array([dict_barycenters[cell_id] for cell_id in ant_ids])

    # Calculate mass-weighted barycenters
    post_weights = np.array([dict_cell_mass[cell_id] for cell_id in post_ids])
    ant_weights = np.array([dict_cell_mass[cell_id] for cell_id in ant_ids])

    # print('posterior cells barycenters', post_barycenters.shape, post_barycenters, )

    weighted_post_barycenter = np.average(post_barycenters, axis=0, weights=post_weights)
    weighted_ant_barycenter = np.average(ant_barycenters, axis=0, weights=ant_weights)

    # Calculate the posterior-anterior axis
    ante_post_axis = weighted_ant_barycenter - weighted_post_barycenter

    return ante_post_axis


def estimate_center_of_mass(dict_barycenters, dict_cell_mass):
    """
    Estimates the center of mass of the embryo.

    Parameters
    ----------
    dict_barycenters : dict
        Dictionary with cell IDs as keys and their barycenters as values.
    dict_cell_mass : dict
        Dictionary with cell IDs as keys and their masses as values.

    Returns
    -------
    center_of_mass : np.ndarray
        The estimated center of mass of the embryo.
    """

    # Extract barycenters and masses
    barycenters = np.array([dict_barycenters[cell_id] for cell_id in dict_barycenters])
    masses = np.array([dict_cell_mass[cell_id] for cell_id in dict_barycenters])

    # Calculate the mass-weighted average of the barycenters
    center_of_mass = np.average(barycenters, axis=0, weights=masses)

    return center_of_mass


def compute_adjusted_times(embryo_names, list_dict_barycenters, temporal_alignment_coefficient_dict, ref_name,
                           ref_props):
    atimes_list = []
    times_list = []

    times_ref = utimes.get_embryo_times(ref_props['cell_lineage'])
    a, b = temporal_alignment_coefficient_dict[ref_name]
    a_times_ref = [a * t + b for t in times_ref]

    for i, embryo in enumerate(embryo_names):
        times = utimes.get_embryo_times(list_dict_barycenters[i])
        a, b = temporal_alignment_coefficient_dict[embryo]
        a_times = [a * t + b for t in times]
        atimes_list.append(a_times)
        times_list.append(times)

    return atimes_list, times_list, a_times_ref


def perpendicular_component(e, i):
    """
    Returns the component of vector i that is perpendicular to vector e.

    Parameters:
    e (numpy.ndarray): A 3D vector.
    i (numpy.ndarray): A 3D vector.

    Returns:
    numpy.ndarray: The component of vector i that is perpendicular to vector e.
    """
    e = np.array(e)
    i = np.array(i)

    # Normalize the vector e
    e_unit = e / np.linalg.norm(e)

    # Project i onto e
    projection = np.dot(i, e_unit) * e_unit

    # Subtract the projection from i to get the perpendicular component
    perpendicular = i - projection

    return perpendicular



def assess_coord_sys_stability(files, ref_filename, delta_t=30, output_folder=''):
    """
    # todo make retained_fraction, and lineage names for axis estimation parameters of the fct
    Parameters
    ----------
    files
    output_folder

    Returns
    -------

    """
    # grab embryos
    list_dict_barycenters = []
    list_dict_names = []
    list_dict_mass = []

    embryo_names = []
    embryos_props = {}
    embryo_idx = {}

    angles_sym = {}
    angles_post_ant = {}
    distances_center_of_mass = {}

    angles_sym_t = {}
    angles_post_ant_t = {}
    distances_center_of_mass_t = {}

    # grab ref atlas
    with open(ref_filename, 'rb') as f:
        ref_props_data = {}
        ref_name = ref_filename.split('-')[1]
        ref_props = pkl.load(f)
        ref_props_data[ref_name] = ref_props

        #  handle case of embryo_resolution
        list_dict_barycenters.append(ref_props['cell_barycenter'])
        list_dict_names.append(ref_props['cell_name'])
        list_dict_mass.append(ref_props['cell_volume'])
        embryo_names.append(ref_name)
        i = len(embryo_idx)
        embryo_idx[ref_name] = i

    # grab rest of atlases
    for filename in files:
        if filename == ref_filename:
            continue
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)
            embryo_name = filename.split('-')[1]

            embryos_props[embryo_name] = {
                'cell_barycenter': pm_props['cell_barycenter'],
                'cell_name': pm_props['cell_name'],
                'cell_volume': pm_props['cell_volume'],
                'cell_lineage': pm_props['cell_lineage']
            }

            i = len(embryo_idx)
            embryo_idx[embryo_name] = i
            list_dict_barycenters.append(pm_props['cell_barycenter'])
            list_dict_names.append(pm_props['cell_name'])
            list_dict_mass.append(pm_props['cell_volume'])
            embryo_names.append(embryo_name)

    # temporal alignement
    temporal_alignment_coefficient_dict, nb_cells_at_t_in_ref = get_temporal_alignment_coefficients_of_embryos(embryos_props,
                                                                                            ref_props_data)

    # for each embryo compute adjusted times
    atimes_list, times_list, a_times_ref = compute_adjusted_times(embryo_names, list_dict_barycenters,
                                                                  temporal_alignment_coefficient_dict, ref_name,
                                                                  ref_props_data[ref_name])

    #
    #  delete embryos_props
    del embryos_props
    del ref_props_data

    zero_vector = np.zeros((3, 1))

    # get from list nb_cells_at_t_in_ref index of 64,76,112
    dev_times = average_t_for_cells(nb_cells_at_t_in_ref, [64, 76, 112])
    dev_times = dev_times[1:-1]

    # for each embryo
    for i, pm in enumerate(embryo_names):
        # take timesteps and time alignement coef
        timesteps = times_list[i]
        a, b = temporal_alignment_coefficient_dict[pm]
        # take interval of stability check
        delta_taq = int(delta_t / a)

        # get for pm closest atimes for 64, 76, 112
        closest_atimes = [min(atimes_list[i], key=lambda x: abs(x - t)) for t in dev_times]
        taq_times = [times_list[i][atimes_list[i].index(closest_at)] for closest_at in closest_atimes]

        print('in embryo', pm, 'span of time to be assessed:', delta_taq)

        # for each of the compute aq times instead
        #for t in range(timesteps[2], timesteps[-1], int(delta_taq / 2)):
        for l, t in enumerate(taq_times):
            #  get embryo at t
            dict_names_t = get_embryo_at_t(list_dict_names[i], t)
            dict_barycenters_t = get_embryo_at_t(list_dict_barycenters[i], t)
            dict_mass_t = get_embryo_at_t(list_dict_mass[i], t)

            print('at time of aq:', t, nb_of_cells(dict_names_t))
            if nb_of_cells(dict_names_t) < 64:
                continue

            ant_names = ['a4.0002*', 'a4.0002_', 'a4.0001*', 'a4.0001_']
            post_names = ['b4.0002*', 'b4.0002_', 'b4.0001*', 'b4.0001_']

            left_names = ['a6.0002*', 'a6.0004*', 'a6.0005*']
            right_names = ['a6.0002_', 'a6.0004_', 'a6.0005_']

            # animal_names = ['a4.0002*', 'a4.0002_', 'b4.0002*', 'b4.0002_']
            # vegetative_names = ['a4.0001*', 'a4.0001_', 'b4.0001*', 'b4.0001_']

            #   get posterior_anterior dir
            ante_post_axis = estimate_lineage_axis(dict_barycenters_t, dict_names_t, dict_mass_t, ant_names, post_names).reshape(3, 1)
            #   get sym dir
            #sym_vector = estimate_sym_vector(dict_barycenters_t, dict_names_t, dict_mass_t).reshape(3, 1)
            sym_vector = estimate_lineage_axis(dict_barycenters_t, dict_names_t, dict_mass_t, left_names,
                                                   right_names).reshape(3, 1)

            sym_vector = sym_vector.flatten()
            ante_post_axis = ante_post_axis.flatten()
            sym_vector = perpendicular_component(ante_post_axis, sym_vector)

            sym_vector = sym_vector/np.linalg.norm(sym_vector)
            ante_post_axis = ante_post_axis/np.linalg.norm(ante_post_axis)

            #   get coord ref, can be center of mass can be a6.6 lineage barycenter
            center_of_mass = estimate_center_of_mass(dict_barycenters_t, dict_mass_t).reshape(3, 1)

            if np.array_equal(center_of_mass, zero_vector) or np.array_equal(ante_post_axis,
                                                                             zero_vector) or np.array_equal(sym_vector,
                                                                                                            zero_vector):
                continue

            nb_cells_t = nb_of_cells(dict_names_t)
            nb_cells_n = nb_cells_t
            if nb_cells_t < 112:
                if nb_cells_t >= 76:
                    delta_taq_n = int(delta_taq/2)
                else:
                    delta_taq_n = int(delta_taq / 4)
            else:
                delta_taq_n = delta_taq
            print('range tested:', delta_taq_n)
            # For each n ranging from t+1 to t+delta_taq
            for n in range(t + 1, t + delta_taq_n + 1):
                if n < timesteps[-1]:
                    # Get embryo at t+n
                    dict_names_n = get_embryo_at_t(list_dict_names[i], n)
                    dict_barycenters_n = get_embryo_at_t(list_dict_barycenters[i], n)
                    dict_mass_n = get_embryo_at_t(list_dict_mass[i], n)

                    # if nb of cells decreasing => continue
                    if nb_of_cells(dict_names_n) < nb_cells_n:
                        continue

                    # Get symmetry direction at t+n
                    #sym_vector_n = estimate_sym_vector(dict_barycenters_n, dict_names_n, dict_mass_n).reshape(3, 1)
                    sym_vector_n = estimate_lineage_axis(dict_barycenters_n, dict_names_n,dict_mass_n,
                                                         left_names, right_names).reshape(3, 1)

                    # Get posterior_anterior direction at t+n
                    ante_post_axis_n = estimate_lineage_axis(dict_barycenters_n, dict_names_n,
                                                             dict_mass_n, ant_names, post_names).reshape(3, 1)

                    center_of_mass_n = estimate_center_of_mass(dict_barycenters_n, dict_mass_n).reshape(3, 1)

                    if np.array_equal(center_of_mass_n, zero_vector) or np.array_equal(ante_post_axis_n,
                                                                                       zero_vector) or np.array_equal(
                        sym_vector_n, zero_vector):
                        continue

                    reduced_barycenters_t, reduced_barycenters_n, \
                        reduced_names_t, reduced_names_n = reduce_to_closest_ancestry(
                        dict_barycenters_t, dict_barycenters_n,
                        dict_mass_t, dict_mass_n,
                        dict_names_t, dict_names_n
                    )

                    # Check the number of cells in each frame before and after reduction
                    nb_cells_t = nb_of_cells(reduced_names_t)
                    nb_cells_n = nb_of_cells(reduced_names_n)

                    # transform = compute similitude between t and t+n
                    if nb_cells_t == nb_cells_n:
                        # Global registration
                        transform, floating, _, _ = global_similitude(reduced_barycenters_t,
                                                                      reduced_names_t,
                                                                      reduced_barycenters_n,
                                                                      reduced_names_n, retained_fraction=0.9)
                        # transform is homogeneous coords of similarity transfom
                        #  apply rotation on sym dir at t+n, post-ant dir

                        # print(sym_vector_n.shape, ante_post_axis_n.shape, center_of_mass_n.shape)
                        sym_vector_n = apply_transformation(transform, sym_vector_n, translate=True)
                        sym_vector_n = sym_vector_n / np.linalg.norm(sym_vector_n)
                        ante_post_axis_n = apply_transformation(transform, ante_post_axis_n, translate=True)
                        ante_post_axis_n = ante_post_axis_n / np.linalg.norm(ante_post_axis_n)
                        center_of_mass_n = apply_transformation(transform, center_of_mass_n, translate=True)

                        sym_vector_n = sym_vector_n.flatten()
                        ante_post_axis_n = ante_post_axis_n.flatten()
                        sym_vector_n = perpendicular_component(ante_post_axis_n, sym_vector_n)

                        # Calculate angles and distances
                        sym_vector = sym_vector.flatten()
                        sym_vector_n = sym_vector_n.flatten()
                        angle_sym = np.arccos(np.clip(np.dot(sym_vector, sym_vector_n), -1.0, 1.0))
                        ante_post_axis = ante_post_axis.flatten()
                        ante_post_axis_n = ante_post_axis_n.flatten()
                        angle_post_ant = np.arccos(np.clip(np.dot(ante_post_axis, ante_post_axis_n), -1.0, 1.0))
                        distance_center_of_mass = np.linalg.norm(center_of_mass - center_of_mass_n) * 0.3

                        # distance in um, maybe compare it to embryo's volume?

                        # Store results in nested dictionaries
                        angles_sym.setdefault(n - t, {}).setdefault(pm, []).append(int(np.degrees(angle_sym)))
                        angles_post_ant.setdefault(n - t, {}).setdefault(pm, []).append(int(np.degrees(angle_post_ant)))
                        distances_center_of_mass.setdefault(n - t, {}).setdefault(pm, []).append(
                            int(distance_center_of_mass))

                        angles_sym_t.setdefault(nb_of_cells(dict_names_t), {}).setdefault(pm, []).append(int(np.degrees(angle_sym)))
                        angles_post_ant_t.setdefault(nb_of_cells(dict_names_t), {}).setdefault(pm, []).append(int(np.degrees(angle_post_ant)))
                        distances_center_of_mass_t.setdefault(nb_of_cells(dict_names_t), {}).setdefault(pm, []).append(
                            int(distance_center_of_mass))
                        #print(sym_vector, sym_vector_n)
                        print(pm,t,n-t,'nb_of_cells', nb_of_cells(dict_names_n),'sym angle', int(np.degrees(angle_sym)),
                              'ant post angle', int(np.degrees(angle_post_ant)), 'dist btw center of mass',
                              int(distance_center_of_mass), np.dot(ante_post_axis_n, sym_vector_n))

    # after storing data from all embryos
    # plot for sym dir boxplot of angles for each delta_t: boxplot for angles at t+1, t+2...t+n
    # plot for post-ant dir boxplot of angles for each delta_t: boxplot for angles at t+1, t+2...t+n
    # plot for coord_ref boxplot of distances for each delta_t: boxplot for distances at t+1, t+2...t+n
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    filename = os.path.join(output_folder, 'stability_coordinate_reference_system_all_ant_post.py')

    generate_plot_script(angles_sym, angles_post_ant, distances_center_of_mass,
                         "stability of coord ref for all stages", filename)
    filename = os.path.join(output_folder,
                            'stability_coordinate_reference_system_perdevStage_all_ant_post.py')
    generate_plot_script(angles_sym_t, angles_post_ant_t, distances_center_of_mass_t,
                         "stability of coord ref per stage", filename)


def generate_plot_script(angles_sym, angles_post_ant, distances_center_of_mass, title, filename):
    # ensure output_folder exist
    print('writing plot to', filename)
    with open(filename, 'w') as file:
        file.write(f"""
import numpy as np
import matplotlib.pyplot as plt

angles_sym = {angles_sym}
angles_post_ant = {angles_post_ant}
distances_center_of_mass = {distances_center_of_mass}

# Helper function to flatten dictionary for plotting
def flatten_dict_to_list_of_list(data_dict):
    flat_data = []
    d = []
    for key, subdict in data_dict.items(): # for delta t
        for subkey, values in subdict.items(): # for indiv
            d.extend(values)    
        flat_data.append(values) 
    return flat_data

# Plotting results
plt.figure(figsize=(12, 6))

# Symmetry direction angles
plt.subplot(1, 3, 1)
plt.boxplot(flatten_dict_to_list_of_list(angles_sym))
plt.title('Symmetry Direction Angles')
plt.xlabel('Delta t')
plt.xticks(rotation=90)
plt.ylabel('Angle (degree)')

# Posterior-anterior direction angles
plt.subplot(1, 3, 2)
plt.boxplot(flatten_dict_to_list_of_list(angles_post_ant))
plt.title('Posterior-Anterior Direction Angles')
plt.xlabel('Delta t')
plt.xticks(rotation=90)
plt.ylabel('Angle (degree)')

# Center of mass distances
plt.subplot(1, 3, 3)
plt.boxplot(flatten_dict_to_list_of_list(distances_center_of_mass))
plt.title('Center of Mass Distances(in um)')
plt.xlabel('Delta t')
plt.xticks(rotation=90)
plt.ylabel('Distance')

plt.tight_layout()
plt.show()
""")

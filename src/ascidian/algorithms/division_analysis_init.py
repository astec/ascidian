# This file will compare reference embryo to other population individuals
# And it will store divisions properties


from ascidian.division_analysis.division_analysis_init_tools import (compare_divisions_in_embryo_and_reference,
                                                        get_divisions_acquisition_times)
from ascidian.division_analysis.division_analysis_init_tools_local_reg import compare_divisions_in_embryo_and_reference_local_reg
from ascidian.division_analysis.division_analysis_tools import *
import ascidian.core.temporal_alignment as utimes


def get_temporal_alignment_coefficients_of_embryos(embryos_properties, reference_properties):
    """
    Uses utimes.temporal_alignment(ref_lineage, lineage) to compute alignment coefficients
    Parameters
    ----------
    embryos_properties (dict): Nested dictionary of dictionaries containing properties for each embryo.
                                embryos_data[atlas_name] = atlas_data
    reference_properties (dict): Properties of the reference embryo.
                                    reference_properties[atlas_name] = atlas_data

    Returns
    -------
    temporal_alignment_coefficient (dict) : {'embryo name': [a,b] }
    cells_per_time (dict): {'timePoint': nb_of_cells}
    """
    temporal_alignment_coefficient_dict = {}

    # Extract reference embryo data
    ref_name, ref_properties = next(iter(reference_properties.items()))
    ref_lineage = ref_properties['cell_lineage']

    cells_per_time_in_ref = utimes.count_cells_per_time(ref_lineage)

    # for each embryo get its cell-lineage properties and compute its alignment coefficients
    # for reference embryo coefficient are a=1 and b=0
    a_ref, b_ref = 1.0, 0
    temporal_alignment_coefficient_dict[ref_name] = [a_ref, b_ref]
    for embryo_name, embryo_properties in embryos_properties.items():
        # Extract lineage data for the current embryo
        embryo_lineage = embryo_properties['cell_lineage']

        # Calculate temporal alignment coefficients
        a, b = utimes.temporal_alignment(ref_lineage, embryo_lineage)

        # Store coefficients in the dictionary
        temporal_alignment_coefficient_dict[embryo_name] = [a, b]

    return temporal_alignment_coefficient_dict, cells_per_time_in_ref


def compare_embryos_each_by_its_turn(embryos_properties, reference_properties, delay, local_reg=False):
    """
    Compare properties of each embryo to the reference and compute division properties.

    Parameters:
    - embryos_properties (list): List of dictionaries containing properties for each embryo.
    - reference_properties (dict): Properties of the reference embryo.

    Returns:
    - division_properties (dict): Dictionary containing division properties to be saved in a pickle file.

    division_properties = { 'mother_name': { 'division_directions_after_symmetry': None, # for Left and Right /and/
    for each individual
     'division_barycenter_from_global_registration': {'left': {'first': {'individual'}, 'second': {}},
                                                                                 'right': {'first': {}, 'second': {}}}
     'division_mean_direction': None,
     'division_aq_time': None, # for left and right /and/ for each individual
     'all_div_aq_times' : {{}} # for left and right /and/ for each individual, values without delays
     'cell_radius': None, # for Left and Right /and/ for each individual
    'embryo_symmetry_plane_at_t': None, # for each individual
    'cell_fate': None,
    'nb_of_measurements': None,
     'std_from_mean_directions': None,
    'division_daughters_registration_dispersion': None,
    'daughters_fate': {'first_daughter': [], 'second_daughter': []},
    'ids_in_ref' : None # useful to generate fate maps for
                morphonet # for Left and Right /and/ for each individual
    'cell_ids': {'left': {'individual': }, 'right': {'individual': }}
    'gen' : int # mother's generation,
    'neighbors':{'left':{'individual': {'neighbor_name':{'surface_of_contact after delay':value }  } } ,
                 'right': {'individual': {'neighbor_name':{'surface_of_contact after delay':value }  } }
                 }
    'cell_surface':{'left':{'individual':   } } ,
                 'right': {'individual':   } }
                 }
    'apical_surface_pts_at_div_time' : {'direction':{'individual':   } }
        }
    }

    """
    divisions_properties = {}

    for embryo_name in embryos_properties:
        embryo_properties = {embryo_name: embryos_properties[embryo_name]}
        # Compare reference to embryo and get divisions dat

        if local_reg:
            compare_divisions_in_embryo_and_reference_local_reg(embryo_properties, reference_properties,
                                                            divisions_properties, delay, 'bary')
        else:
            print('global registration')
            # direction_type='interface' 'bary'
            compare_divisions_in_embryo_and_reference(embryo_properties, reference_properties, divisions_properties,
                                                      delay, 'bary')
        # print(divisions_properties)
        # Compare division times without delay
        get_divisions_acquisition_times(embryo_properties, reference_properties, divisions_properties)
    # compute mean direction, std, nb of mes,
    divisions_properties = compute_divisions_mean_direction(divisions_properties)
    divisions_properties = compute_division_nb_of_measurements(divisions_properties)
    divisions_properties = compute_std_from_mean(divisions_properties)

    # registration dispersion
    divisions_properties = compute_div_registration_dispersion_of_registered_barycenters(divisions_properties)
    print('Done')
    return divisions_properties

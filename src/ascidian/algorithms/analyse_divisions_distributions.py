import pickle as pkl
import os
import numpy as np
import matplotlib.pyplot as plt

import ascidian.division_analysis.vector_distribution as vdist
from ascidian.division_analysis.figure_variability import write_variability_plot

######################
#########

## cluster divisions depending on mode lifetime (choose mid intrval between two modes and one mode kernels)
def kde_divisions_grouping_lifetime(divisions_properties, nb_of_modes, generations=[7], output_folder='',
                           step_sigma=0.01, start_sigma=0.01, sphere_radius=20):


    # Folders to save : distribution_spheres_directory/ color_spheres_directory/ division_direction_colored_directory
    distribution_spheres_directory = os.path.join(output_folder, 'division_directions_density_two_modes_interval_analysis'
                                                                 '/distribution_spheres')
    # color_spheres_directory = os.path.join(output_folder, 'colored_spheres')
    division_direction_colored_directory = os.path.join(output_folder, 'division_directions_density_two_modes_interval_analysis'
                                                                       '/division_directions_clustered')
    dist_analysis_dir = os.path.join(output_folder, 'division_directions_density_two_modes_interval_analysis'
                                                    '/density_analysis')

    # filter cells from wanted generations
    for gen in generations:
        selected_divisions = [(mothers, properties) for mothers, properties in divisions_properties.items() if
                              properties.get('gen') == gen and properties.get('nb_of_measurements')>5]

        distribution_dict_for_daughter = None

        mother_names = []
        max_d_btw_max = []
        kernel_windows_m2 = []
        kernel_interval_m2 = []
        vectors_per_maxima = []
        for mother, mother_props in selected_divisions:
            print('computing for division :', mother)
            sigma = start_sigma
            found_n_maximums = False
            found_2_modes = False

            while not found_n_maximums:
                # get the division directions from divisions_properties
                left_directions = mother_props.get('division_directions_after_symmetry', {}).get('left', {})
                right_directions = mother_props.get('division_directions_after_symmetry', {}).get('right', {})

                left_individuals = list(left_directions.keys())
                right_individuals = list(right_directions.keys())

                left_directions = [direction.tolist() for direction in left_directions.values()]
                right_directions = [direction.tolist() for direction in right_directions.values()]

                left_individuals = [name + '_left' for name in left_individuals]
                right_individuals = [name + '_right' for name in right_individuals]

                vectors_list = left_directions + right_directions  # list of all directions vectors
                vector_labels = left_individuals + right_individuals  # list of all vectors labels/individual names

                distribution_dict_for_daughter = vdist.VectorDistribution(vectors=vectors_list, vector_labels=vector_labels,
                                                                          sphere_radius=20, kernel_sigma=sigma,
                                                                          WithColor=True)
                local_maxima_vectors, local_maxima_values, nb_vectors_per_maxima = distribution_dict_for_daughter.color_propagation(
                    maxima_threshold=0.07, maxima_number=None, verbose=False)
                nb_vectors_per_maxima = distribution_dict_for_daughter.get_color_per_maxima()

                if len(local_maxima_values) <= 2 and found_n_maximums == False and found_2_modes == False:
                    sigma_m2 = sigma
                    found_2_modes = True
                    print('2 modes kernel', np.degrees(sigma_m2))
                if len(local_maxima_values) <= 1 and found_n_maximums == False:
                    sigma_m1 = sigma
                    found_n_maximums = True
                    print('1 mode kernel', np.degrees(sigma_m1))
                sigma += step_sigma
            #_ = distribution_dict_for_daughter.color_vectors()

            # get distribution on mid interval of kernel width, get d_max, sigma_m2 and ratio from it
            sigma_m2_a = sigma_m2 + (sigma_m1 - sigma_m2)/2

            distribution_dict_for_daughter = vdist.VectorDistribution(vectors=vectors_list, vector_labels=vector_labels,
                                                                      sphere_radius=20, kernel_sigma=sigma_m2_a,
                                                                      WithColor=True)
            local_maxima_vectors, local_maxima_values, nb_vectors_per_maxima = distribution_dict_for_daughter.color_propagation(
                maxima_threshold=0.07, maxima_number=None, verbose=False)
            nb_vectors_per_maxima = distribution_dict_for_daughter.get_color_per_maxima()
            print('nb of maximas', len(local_maxima_values))
            d_max = distribution_dict_for_daughter.d_max
            nb_vec_per_maxes = nb_vectors_per_maxima

            _ = distribution_dict_for_daughter.color_vectors()

            # make sigmas in degree
            sigma_m1 = np.degrees(sigma_m1)
            sigma_m2 = np.degrees(sigma_m2)
            sigma_m2_a = np.degrees(sigma_m2_a)

            # save distribution plots, vectors coloring in folders called adaptative_sigma_n_modes
            mother_names.append(mother)
            max_d_btw_max.append(d_max)
            kernel_windows_m2.append(sigma_m2_a)
            vectors_per_maxima.append(max(nb_vec_per_maxes) / sum(nb_vec_per_maxes))
            kernel_interval_m2.append(sigma_m1-sigma_m2)

            sigma_str = str(sigma_m2_a).replace('.', '_')
            sphere_radius_str = str(sphere_radius).replace('.', '_')
            mother_name = mother.replace('.', '_')

            file_suffix = f'_division_{mother_name}_sigma_{sigma_str}_sphere_{sphere_radius_str}'

            distribution_dict_for_daughter.figure(filename='figure_vector_distribution', file_suffix=file_suffix,
                                                  output_dir=distribution_spheres_directory, cmap='autumn')
            # distribution_dict_for_daughter.color_figure(filename='figure_color_distribution', file_suffix=file_suffix,
            #                                            output_dir=color_spheres_directory, cmap='autumn')
            distribution_dict_for_daughter.figure_colored_directions(filename='figure_div_dir_coloring',
                                                                     file_suffix=file_suffix,
                                                                     output_dir=division_direction_colored_directory,
                                                                     cell_name=mother, distance_between_modes=d_max,
                                                                     kernel_size=sigma_m2_a,
                                                                     cmap='autumn')

            #  get the two vectors groups
            dict_vectors_labels_colors = distribution_dict_for_daughter._colored_direction_dict
            # {'vlabel': {'vector': coords, 'color': color} }
            # save vectors in dict
            divisions_properties = update_cells_props(divisions_properties, dict_vectors_labels_colors, sigma_m2_a,
                                                      mother)
            print('kernel widths in degrees(smallest 2modes, mid 2 modes, 1 mode)',int(sigma_m2), int(sigma_m2_a), int(sigma_m1))

        # write plot for analysis of all cells
        combined_data = sorted(zip(mother_names, max_d_btw_max, kernel_interval_m2, kernel_windows_m2, vectors_per_maxima),
                               key=lambda x: x[1])
        mother_names, max_d_btw_max, kernel_interval_m2, kernel_windows_m2, vectors_per_maxima = zip(*combined_data)

        # include m2 interval in plot
        write_variability_plot(mother_names, max_d_btw_max, kernel_interval_m2, vectors_per_maxima, kernel_windows_m2,
                               filename=f'distribution_analysis_with_kernel_interval_gen{gen}', file_suffix=None, output_dir=dist_analysis_dir)
        print("plot files written for gen", gen)

    return divisions_properties


## Cluster diviisons depending on smallest kernel that gives two modes
def kde_divisions_grouping(divisions_properties, nb_of_modes, generations=[7], output_folder='',
                           step_sigma=0.01, start_sigma=0.01, sphere_radius=20):


    # Folders to save : distribution_spheres_directory/ color_spheres_directory/ division_direction_colored_directory
    distribution_spheres_directory = os.path.join(output_folder, 'division_directions_density_two_modes'
                                                                 '/distribution_spheres')
    # color_spheres_directory = os.path.join(output_folder, 'colored_spheres')
    division_direction_colored_directory = os.path.join(output_folder, 'division_directions_density_two_modes'
                                                                       '/division_directions_clustered')
    dist_analysis_dir = os.path.join(output_folder, 'division_directions_density_two_modes'
                                                    '/density_analysis')

    # filter cells from wanted generations
    for gen in generations:
        selected_divisions = [(mothers, properties) for mothers, properties in divisions_properties.items() if
                              properties.get('gen') == gen and properties.get('nb_of_measurements')>5]

        distribution_dict_for_daughter = None

        mother_names = []
        max_d_btw_max = []
        kernel_windows_m2 = []
        vectors_per_maxima = []

        for mother, mother_props in selected_divisions:
            print('computing for division :', mother)
            sigma = start_sigma
            found_n_maximums = False
            while not found_n_maximums:
                # get the division directions from divisions_properties
                left_directions = mother_props.get('division_directions_after_symmetry', {}).get('left', {})
                right_directions = mother_props.get('division_directions_after_symmetry', {}).get('right', {})

                left_individuals = list(left_directions.keys())
                right_individuals = list(right_directions.keys())

                left_directions = [direction.tolist() for direction in left_directions.values()]
                right_directions = [direction.tolist() for direction in right_directions.values()]

                left_individuals = [name + '_left' for name in left_individuals]
                right_individuals = [name + '_right' for name in right_individuals]

                vectors_list = left_directions + right_directions  # list of all directions vectors
                vector_labels = left_individuals + right_individuals  # list of all vectors labels/individual names

                distribution_dict_for_daughter = vdist.VectorDistribution(vectors=vectors_list, vector_labels=vector_labels,
                                                                          sphere_radius=20, kernel_sigma=sigma,
                                                                          WithColor=True)
                local_maxima_vectors, local_maxima_values, nb_vectors_per_maxima = distribution_dict_for_daughter.color_propagation(
                    maxima_threshold=0.07, maxima_number=None, verbose=False)
                nb_vectors_per_maxima = distribution_dict_for_daughter.get_color_per_maxima()

                if len(local_maxima_values) <= 2 and found_n_maximums == False:
                    sigma_m2 = sigma
                    d_max = distribution_dict_for_daughter.d_max
                    nb_vec_per_maxes = nb_vectors_per_maxima
                    found_n_maximums = True
                sigma += step_sigma

            _ = distribution_dict_for_daughter.color_vectors()

            # save distribution plots, vectors coloring in folders called adaptative_sigma_n_modes
            mother_names.append(mother)
            max_d_btw_max.append(d_max)
            kernel_windows_m2.append(sigma_m2)
            vectors_per_maxima.append(max(nb_vec_per_maxes) / sum(nb_vec_per_maxes))

            sigma_str = str(sigma_m2).replace('.', '_')
            sphere_radius_str = str(sphere_radius).replace('.', '_')
            mother_name = mother.replace('.', '_')

            file_suffix = f'_sigma_{sigma_str}_sphere_{sphere_radius_str}_division_{mother_name}'

            distribution_dict_for_daughter.figure(filename='figure_vector_distribution', file_suffix=file_suffix,
                                                  output_dir=distribution_spheres_directory, cmap='autumn')
            # distribution_dict_for_daughter.color_figure(filename='figure_color_distribution', file_suffix=file_suffix,
            #                                            output_dir=color_spheres_directory, cmap='autumn')
            distribution_dict_for_daughter.figure_colored_directions(filename='figure_div_dir_coloring',
                                                                     file_suffix=file_suffix,
                                                                     output_dir=division_direction_colored_directory,
                                                                     cell_name=mother, distance_between_modes=d_max,
                                                                     kernel_size=sigma_m2,
                                                                     cmap='autumn')

            #  get the two vectors groups
            dict_vectors_labels_colors = distribution_dict_for_daughter._colored_direction_dict
            # {'vlabel': {'vector': coords, 'color': color} }
            # save vectors in dict
            divisions_properties = update_cells_props(divisions_properties, dict_vectors_labels_colors, sigma_m2, mother)
            print('kernel width', np.degrees(sigma_m2))

        # write plot for analysis of all cells
        combined_data = sorted(zip(mother_names, max_d_btw_max, kernel_windows_m2, vectors_per_maxima),
                               key=lambda x: x[1])
        mother_names, max_d_btw_max, kernel_windows_m2, vectors_per_maxima = zip(*combined_data)

        write_variability_plot(mother_names, max_d_btw_max, kernel_windows_m2, vectors_per_maxima,
                               filename=f'distribution_analysis_gen{gen}', file_suffix=None, output_dir=dist_analysis_dir)
        print("plot files written for gen", gen)

    return divisions_properties


def update_cells_props(divisions_properties, dict_vectors_labels_colors, sigma_m2, cell_name):
    """
    Given division properties, update it by adding property: observation color.

    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
               Example: {'mother_name': { 'division_mothers_lifetimes':
                                        {'left': {'individual name': lifetime (None if not valid)},
                                        'right': {'individual name': lifetime (None if not valid)} }
                                                    },
                                        'nb_of_measurements': int,
                                         'division_aq_time': {'left/right': {'embryo name': value_to_adjusted}},
                                         'gen': int }
    dict_vectors_labels_colors (dict): A dictionary with vectors, labels, and colors.
               Example: {'vlabel': {'vector': coords, 'color': color} }
    cell_name (str): The name of the cell to update.

    Returns
    -------
    divisions_properties (dict): The updated division properties dictionary.
    """
    for label, props in dict_vectors_labels_colors.items():
        ind = label.split('_')[0]
        dir = label.split('_')[1]
        color = props['color']

        if cell_name in divisions_properties:
            if 'color' not in divisions_properties[cell_name]:
                divisions_properties[cell_name]['color'] = {}
            if dir not in divisions_properties[cell_name]['color']:
                divisions_properties[cell_name]['color'][dir] = {}
            divisions_properties[cell_name]['color'][dir][ind] = color
            divisions_properties[cell_name]['two_modes_kernel_size'] = np.degrees(sigma_m2)

    return divisions_properties

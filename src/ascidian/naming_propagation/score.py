##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2023
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu  6 jul 2023 09:08:44 CEST
#
##############################################################
#
#
#
##############################################################

import sys
import copy

import astec.utils.common as common

# import ascidian.core.ascidian_name as uname
# import ascidian.core.neighborhood_distance as neighborhood
# import ascidian.core.atlas_division as atlasdivision
# import ascidian.core.atlas_cell as atlascell
import ascidian.ascidian.name as aname

import ascidian.components.embryo as cembryo
import ascidian.components.division_atlas as cdivatlas

import ascidian.naming_propagation.parameters as npparameters

monitoring = common.Monitoring()


##############################################################
#
#
#
##############################################################

def _print_neighborhood(neighborhood, start=""):
    neighbors = sorted(list(neighborhood.keys()))
    n = 0
    txt = start + "{"
    if 'background' in neighbors:
        txt += "'background': {:.3f}, ".format(neighborhood['background'])
        n += 1
        neighbors.remove('background')
    for i, neigh in enumerate(neighbors):
        txt += str(neigh) + ": {:.3f}".format(neighborhood[neigh])
        n += 1
        if i < len(neighbors) - 1:
            txt += ", "
        elif i == len(neighbors) - 1:
            txt += "} "
        if n % 4 == 0 or i == len(neighbors) - 1:
            monitoring.to_log_and_console(txt)
            txt = "      "


def _get_branch_length(cell, lineage):
    length = 0
    c = cell
    while c in lineage and len(lineage[c]) == 1:
        length += 1
        c = lineage[c][0]
    return length


def _get_neighborhoods(immediate_daughters, embryo, delay_from_division=0):
    """

    Parameters
    ----------
    immediate_daughters: cell ids of the daughter cells
    embryo
    delay_from_division

    Returns
    -------
    A dictionary 'contact' indexed by [0, 1]. The value 'contact[i]' is the neighborhood of 'daughter[i]',
    a neighborhood being a contact surface vector indexed by cell names.
    None if there exists a neighbors without any named ancestor (including itself)
    """
    proc = "_get_neighborhoods"

    if isinstance(embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryos' variable: " + str(type(embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    cell_lineage = embryo.cell_lineage
    if cell_lineage is None:
        monitoring.to_log_and_console(str(proc) + ": embryo to be named has no 'cell_lineage'")
        return None

    cell_contact_surface = embryo.rectified_cell_contact_surface
    if cell_contact_surface is None:
        monitoring.to_log_and_console(str(proc) + ": embryo to be named has no 'cell_contact_surface'")
        return None

    cell_name = embryo.cell_name
    if cell_name is None:
        monitoring.to_log_and_console(str(proc) + ": embryo to be named has no 'cell_name'")
        return None

    reverse_lineage = {v: k for k, values in cell_lineage.items() for v in values}
    #
    # since we change daughters, use a copy of immediate_daughters
    # else immediate_daughters will be changed
    #
    daughters = copy.deepcopy(immediate_daughters)

    #
    # delay from division
    #
    delay = 0
    if delay_from_division >= 0:
        delay = delay_from_division
    elif delay_from_division < 0:
        length0 = _get_branch_length(daughters[0], cell_lineage)
        length1 = _get_branch_length(daughters[1], cell_lineage)
        delay = min(length0, length1) + delay_from_division
        if delay < 0:
            delay = 0

    for i in range(delay):
        #
        # this test seems to be redundant with the next. Cells at the last time
        #
        if daughters[0] not in cell_lineage:
            msg = ": build neighborhoods with a delay of " + str(i)
            msg += " instead of " + str(delay)
            msg += " for cells " + str(immediate_daughters)
            msg += " (" + str(daughters[0]) + " was not in lineage)"
            monitoring.to_log_and_console(str(proc) + msg, 3)
            break
        if daughters[1] not in cell_lineage:
            msg = ": build neighborhoods with a delay of " + str(i)
            msg += " instead of " + str(delay)
            msg += " for cells " + str(immediate_daughters)
            msg += " (" + str(daughters[1]) + " was not in lineage)"
            monitoring.to_log_and_console(str(proc) + msg, 3)
            break
        if len(cell_lineage[daughters[0]]) == 1 and len(cell_lineage[daughters[1]]) == 1:
            daughters[0] = cell_lineage[daughters[0]][0]
            daughters[1] = cell_lineage[daughters[1]][0]
        else:
            msg = ": build neighborhoods with a delay of " + str(i)
            msg += " instead of " + str(delay_from_division)
            msg += " for cells " + str(immediate_daughters)
            monitoring.to_log_and_console(str(proc) + msg, 4)
            break

    contact = {}
    div = 10 ** embryo.time_digits_for_cell_id

    for index, d in enumerate(daughters):
        #
        # build contact surface as a dictionary of cell names
        # 1. background
        # 2. cell already named
        # 3. sister of d
        #    the key 'sister' is introduced here
        # 4. daughter cell not named, then named after its closest named ancestor (if any)
        #
        contact[index] = {}

        for c in cell_contact_surface[d]:
            if int(c) % div == 1 or int(c) % div == 0:
                contact[index]['background'] = contact[index].get('background', 0) + cell_contact_surface[d][c]
            elif c in cell_name:
                contact[index][cell_name[c]] = cell_contact_surface[d][c]
            elif c in daughters:
                if c != d:
                    contact[index]['sister'] = cell_contact_surface[d][c]
                else:
                    msg = ": weird, cell " + str(c) + " is in its contact surfaces"
                    monitoring.to_log_and_console(str(proc) + msg)
            else:
                #
                # cell without name, find a named ancestor
                #
                cell = c
                while cell in reverse_lineage and cell not in cell_name:
                    cell = reverse_lineage[cell]
                if cell in cell_name:
                    contact[index][cell_name[cell]] = contact[index].get(cell_name[cell], 0) + \
                                                      cell_contact_surface[d][c]
                else:
                    msg = ": unable to find a named ancestor for cell " + str(c)
                    msg += ". Skip naming of cells " + str(immediate_daughters)
                    monitoring.to_log_and_console(str(proc) + msg)
                    return None

    return contact


########################################################################################
#
#
#
########################################################################################


def naming_scores(mother, daughters, embryo, atlases, parameters, delay_from_division=0, debug=False):
    """

    Parameters
    ----------
    mother: cell id of the mother cell
    daughters: cell ids of the daughter cells
    embryo
    atlases:
    parameters:
    delay_from_division
    debug

    Returns
    -------
    a dictionary of distances (in [0,1]) indexed by [d][reference_name] where
        - d:
          d = 0: test (contacts[0], contacts[1]) <-> (daughter_names[0], daughter_names[1])
          d = 1: test (contacts[1], contacts[0]) <-> (daughter_names[0], daughter_names[1])
          where contacts[0] = contact vector for daughters[0]
                contacts[1] = contact vector for daughters[1]
        - reference_name is the name of a reference atlas/embryo
    """
    proc = "naming_scores"

    if isinstance(embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryos' variable: " + str(type(embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if isinstance(atlases, cdivatlas.DivisionAtlases) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'atlases' variable: " + str(type(atlases)))
        sys.exit(1)

    if not isinstance(parameters, npparameters.NamingPropagationParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    cell_name = embryo.cell_name
    if cell_name is None:
        monitoring.to_log_and_console(str(proc) + ": embryo to be named has no 'cell_name'")
        return None

    divisions = atlases.get_division()
    if cell_name[mother] not in divisions:
        msg = ": no reference neighborhoods for division of '" + str(cell_name[mother]) + "'"
        msg += ". Can not name cells " + str(daughters)
        msg += " from mother cell " + str(mother)
        monitoring.to_log_and_console(str(proc) + msg, 4)
        return None

    neighborhoods = atlases.get_cell_neighborhood(delay_from_division=delay_from_division)
    daughter_names = aname.get_daughter_names(cell_name[mother])

    #
    # returned the normalized neighborhood
    #
    contacts = _get_neighborhoods(daughters, embryo, delay_from_division=delay_from_division)
    #
    # contacts[0] = contact vector for daughters[0]
    # contacts[1] = contact vector for daughters[1]
    # contacts[i]['sister'] is the contact surface of the sister
    #
    if contacts is None:
        msg = ": can not extract contact vector for division of '" + str(cell_name[mother]) + "'"
        msg += ". Can not name cells " + str(daughters)
        msg += " from mother cell " + str(mother)
        monitoring.to_log_and_console(str(proc) + msg, 4)
        return None

    daughter_neighborhoods = {0: copy.deepcopy(neighborhoods[daughter_names[0]]),
                              1: copy.deepcopy(neighborhoods[daughter_names[1]])}

    if debug:
        monitoring.to_log_and_console("Distance computation:")
        msg = "- Neighborhood of cell '"
        if daughters[0] in cell_name:
            msg += str(cell_name[daughters[0]])
        else:
            msg += str(daughters[0])
        monitoring.to_log_and_console(msg + "' in embryo to be tested is: ")
        _print_neighborhood(contacts[0], start="   ")
        msg = "- Neighborhood of cell '"
        if daughters[1] in cell_name:
            msg += str(cell_name[daughters[1]])
        else:
            msg += str(daughters[1])
        monitoring.to_log_and_console(msg + "' in embryo to be tested is: ")
        _print_neighborhood(contacts[1], start="   ")

        monitoring.to_log_and_console("- Neighborhoods of cell '" + str(daughter_names[0]) + "' in atlases are: ")
        for r in sorted(daughter_neighborhoods[0].keys()):
            _print_neighborhood(daughter_neighborhoods[0][r], start="   " + str(r) + ": ")
        monitoring.to_log_and_console("- Neighborhoods of cell '" + str(daughter_names[1]) + "' in atlases are: ")
        for r in sorted(daughter_neighborhoods[1].keys()):
            _print_neighborhood(daughter_neighborhoods[1][r], start="   " + str(r) + ": ")

    scores = {0: {}, 1: {}}
    for i in range(2):
        #
        # i = 0: test (contacts[0], contacts[1]) <-> (daughter_names[0], daughter_names[1])
        # i = 1: test (contacts[1], contacts[0]) <-> (daughter_names[0], daughter_names[1])
        #
        if i == 0:
            if 'sister' in contacts[0]:
                contacts[0][daughter_names[1]] = contacts[0]['sister']
                del contacts[0]['sister']
            if 'sister' in contacts[1]:
                contacts[1][daughter_names[0]] = contacts[1]['sister']
                del contacts[1]['sister']
        else:
            if daughter_names[1] in contacts[0]:
                contacts[0][daughter_names[0]] = contacts[0][daughter_names[1]]
                del contacts[0][daughter_names[1]]
            if daughter_names[0] in contacts[1]:
                contacts[1][daughter_names[1]] = contacts[1][daughter_names[0]]
                del contacts[1][daughter_names[0]]
        #
        #
        #
        for ref in divisions[cell_name[mother]]:
            if i == 0:
                ic0 = 0
                ic1 = 1
            else:
                ic0 = 1
                ic1 = 0
            scores[i][ref] = cdivatlas.division_distance(daughter_neighborhoods[0][ref], daughter_neighborhoods[1][ref],
                                                         contacts[ic0], contacts[ic1], change_contact_surfaces=True,
                                                         innersurfaces=[])

    if debug:
        for i in scores:
            msg = "- distance of ("
            if i == 0:
                if daughters[0] in cell_name:
                    msg += str(cell_name[daughters[0]])
                else:
                    msg += str(daughters[0])
                msg += ", "
                if daughters[1] in cell_name:
                    msg += str(cell_name[daughters[1]])
                else:
                    msg += str(daughters[1])
            elif i == 1:
                if daughters[1] in cell_name:
                    msg += str(cell_name[daughters[1]])
                else:
                    msg += str(daughters[1])
                msg += ", "
                if daughters[0] in cell_name:
                    msg += str(cell_name[daughters[0]])
                else:
                    msg += str(daughters[0])
            msg += ") <-> (" + str(daughter_names[0]) + ", " + str(daughter_names[1]) + ")"
            monitoring.to_log_and_console(msg)
            for r in sorted(scores[i].keys()):
                monitoring.to_log_and_console("   - atlas " + str(r) + ": " + str(scores[i][r]))
    return scores

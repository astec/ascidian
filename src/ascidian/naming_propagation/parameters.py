##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Lun  8 jul 2024 14:06:07 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

import astec.utils.common as common

import ascidian.components.parameters as cparameters

monitoring = common.Monitoring()


############################################################
#
#
#
############################################################

class NamingPropagationParameters(cparameters.BaseParameters):
    ############################################################
    #
    # initialisation
    #
    ############################################################

    def __init__(self, prefix=None):

        if "doc" not in self.__dict__:
            self.doc = {}

        cparameters.monitoring.copy(monitoring)

        cparameters.BaseParameters.__init__(self, prefix=prefix)

        doc = "\t Input property file to be named. Must contain lineage, volumes and contact surfaces\n"
        self.doc['inputFile'] = doc
        self.inputFile = None
        doc = "\t Output property file."
        self.doc['outputFile'] = doc
        self.outputFile = None

        doc = "\t Delay from the division to extract the neighborhooods used for atlas building,\n"
        doc += "\t and thus for naming.\n"
        doc += "\t 0 means right after the division.\n"
        doc += "\t negative values means that the delay is counted backwards from the end of the branch.\n"
        self.doc['delay_from_division'] = doc
        self.delay_from_division = 3

        doc = "\t The same cell has different neighbors from an atlas to the other.\n"
        doc += "\t If 'True' build and keep an unique common neighborhood (set of\n"
        doc += "\t neighbors) for all atlases by keeping the closest ancestor for\n"
        doc += "\t neighboring cells. Eg, if a division has occurred in some embryos\n"
        doc += "\t and not in others, daughter cells will be fused so that all\n"
        doc += "\t neighborhoods only exhibit the parent cell."
        self.doc['use_common_neighborhood'] = doc
        self.use_common_neighborhood = True

        doc = "\t Method to name the daughters after a division. Distances are computed\n"
        doc += "\t between the couple of daughters as well as the couple of switched\n"
        doc += "\t daughters and all the atlas divisions.\n"
        doc += "\t - 'mean': choose the couple of names that yield the minimal average \n"
        doc += "\t   distance over all the atlases. \n"
        doc += "\t - 'minimum': choose the couple of names that yield a minimal distance.\n"
        doc += "\t   It comes to name after the closest atlas (for this division).\n"
        doc += "\t - 'sum': same as 'mean' \n"
        doc += "\t - 'majority': for each choice, order the distances in increasing order\n"
        doc += "\t   then compute the cumulated sum over the n first elements (so the first\n"
        doc += "\t   is the minimum distance, and the last one is the average over all\n"
        doc += "\t   atlases). Counts then the number of times when a choice is better than\n"
        doc += "\t   the switched one, and choose the name couple with majority choices."
        self.doc['selection_method'] = doc
        self.selection_method = 'mean'

        #
        # evaluation
        #
        doc = "\t Minimum number of atlases required to assess naming confidence. If there is not enough atlases\n"
        doc += "\t in the database for the aimed division, naming is not assessed."
        self.doc['confidence_atlases_nmin'] = doc
        self.confidence_atlases_nmin = 2
        doc = "\t Percentage of atlases used to assess naming confidence. If the percentage is less than\n"
        doc += "\t 'confidence_atlases_nmin', 'confidence_atlases_nmin' atlases are used."
        self.doc['confidence_atlases_percentage'] = doc
        self.confidence_atlases_percentage = 50

        #
        # for test:
        # names will be deleted, and tried to be rebuilt
        #
        doc = "\t Input property file to be tested (must include cell names).\n"
        doc += "\t If given, 'inputFile' is ignored.\n"
        self.doc['testFile'] = doc
        self.testFile = None

        doc = "\t If True, some diagnosis are conducted on the property file to\n"
        doc += "\t be named or to be tested.\n"
        self.doc['test_diagnosis'] = doc
        self.test_diagnosis = False

        doc = "\t True or False. Performs some diagnosis after building the division atlas. \n"
        doc += "\t Incrementing the verboseness ('-v' in the command line) may give more details.\n"
        self.doc['division_diagnosis'] = doc
        self.division_diagnosis = False

        doc = "\t If True, will propose some daughters switches in the atlases. For a given division,\n"
        doc += "\t a global score is computed as the sum of all pairwise division similarity.\n"
        doc += "\t A switch is proposed for an atlas if it allows to decrease this global score.\n"
        self.doc['division_permutation_proposal'] = doc
        self.division_permutation_proposal = False

        if 'generate_figure' in self.doc:
            doc = self.doc['generate_figure']
        else:
            doc = "\t if True, generate python files (prefixed by 'figures_') that generate figures.\n"
            doc += "\t Those files will be saved into the 'outputDir' directory.\n"
            doc += "\t 'generate_figure' can be\n"
            doc += "\t - a boolean value: if True, all figure files are generated; if False, none of them\n"
            doc += "\t - a string: if 'all', all figure files are generated; else, only the specified\n"
            doc += "\t   figure file is generated (see below for the list)\n"
            doc += "\t - a list of strings: if 'all' is in the list, all figure files are generated;\n"
            doc += "\t   else, only the specified figure files are generated (see below for the list)\n"
            doc += "\t List of figures:\n"
        doc += "\t 'division-dendrograms'``: plot dendrograms (hierarchical clustering) of embryos\n"
        doc += "\t    each division, based on the division-to-division distance.:\n"
        self.doc['generate_figure'] = doc
        self.generate_figure = False

        #
        #
        #
        self.cells_to_be_traced = None

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# NamingPropagationParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        cparameters.BaseParameters.print_parameters(self)

        self.varprint('inputFile', self.inputFile, self.doc.get('inputFile', None))
        self.varprint('outputFile', self.outputFile, self.doc.get('outputFile', None))

        self.varprint('delay_from_division', self.delay_from_division)
        self.varprint('use_common_neighborhood', self.use_common_neighborhood)
        self.varprint('selection_method', self.selection_method, self.doc.get('selection_method', None))

        self.varprint('confidence_atlases_nmin', self.confidence_atlases_nmin)
        self.varprint('confidence_atlases_percentage', self.confidence_atlases_percentage)

        self.varprint('testFile', self.testFile, self.doc.get('testFile', None))

        self.varprint('test_diagnosis', self.test_diagnosis, self.doc.get('test_diagnosis', None))
        self.varprint('division_diagnosis', self.division_diagnosis)
        self.varprint('division_permutation_proposal', self.division_permutation_proposal)

        self.varprint('cells_to_be_traced', self.cells_to_be_traced)
        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# NamingPropagationParameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        cparameters.BaseParameters.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'inputFile', self.inputFile, self.doc.get('inputFile', None))
        self.varwrite(logfile, 'outputFile', self.outputFile, self.doc.get('outputFile', None))

        self.varwrite(logfile, 'delay_from_division', self.delay_from_division,
                      self.doc.get('delay_from_division', None))
        self.varwrite(logfile, 'use_common_neighborhood', self.use_common_neighborhood,
                      self.doc.get('use_common_neighborhood', None))
        self.varwrite(logfile, 'selection_method', self.selection_method, self.doc.get('selection_method', None))

        self.varwrite(logfile, 'confidence_atlases_nmin', self.confidence_atlases_nmin,
                      self.doc.get('confidence_atlases_nmin', None))
        self.varwrite(logfile, 'confidence_atlases_percentage', self.confidence_atlases_percentage,
                      self.doc.get('confidence_atlases_percentage', None))

        self.varwrite(logfile, 'testFile', self.testFile, self.doc.get('testFile', None))

        self.varwrite(logfile, 'test_diagnosis', self.test_diagnosis, self.doc.get('test_diagnosis', None))
        self.varwrite(logfile, 'division_diagnosis', self.division_diagnosis, self.doc.get('division_diagnosis', None))
        self.varwrite(logfile, 'division_permutation_proposal', self.division_permutation_proposal,
                      self.doc.get('division_permutation_proposal', None))

        self.varwrite(logfile, 'cells_to_be_traced', self.cells_to_be_traced, self.doc.get('cells_to_be_traced', None))

        logfile.write("\n")

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):

        cparameters.BaseParameters.update_from_parameters(self, parameters)

        self.inputFile = self.read_parameter(parameters, 'inputFile', self.inputFile)
        self.outputFile = self.read_parameter(parameters, 'outputFile', self.outputFile)

        self.delay_from_division = self.read_parameter(parameters, 'delay_from_division', self.delay_from_division)
        self.use_common_neighborhood = self.read_parameter(parameters, 'use_common_neighborhood',
                                                           self.use_common_neighborhood)
        self.selection_method = self.read_parameter(parameters, 'selection_method', self.selection_method)

        self.confidence_atlases_nmin = self.read_parameter(parameters, 'confidence_atlases_nmin',
                                                           self.confidence_atlases_nmin)
        self.confidence_atlases_percentage = self.read_parameter(parameters, 'confidence_atlases_percentage',
                                                                 self.confidence_atlases_percentage)

        self.testFile = self.read_parameter(parameters, 'testFile', self.testFile)

        self.test_diagnosis = self.read_parameter(parameters, 'test_diagnosis', self.test_diagnosis)
        self.division_diagnosis = self.read_parameter(parameters, 'division_diagnosis', self.division_diagnosis)
        self.division_permutation_proposal = self.read_parameter(parameters, 'division_permutation_proposal',
                                                                 self.division_permutation_proposal)

        self.cells_to_be_traced = self.read_parameter(parameters, 'cells_to_be_traced', self.cells_to_be_traced)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            monitoring.to_log_and_console("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)

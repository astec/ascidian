##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Lun  8 jul 2024 11:37:34 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys
import copy
import random
import statistics

import numpy as np

import astec.utils.common as common
import astec.utils.datadir as datadir
import astec.utils.ioproperties as ioproperties

import ascidian.ascidian.fate as afate
import ascidian.ascidian.name as aname
import ascidian.core.diagnosis as diagnosis
import ascidian.components.embryo as cembryo
import ascidian.components.embryoset as cembryoset
import ascidian.components.division_atlas as cdivatlas
import ascidian.figures.generate as fgenerate

import ascidian.naming_propagation.score as npscore
import ascidian.naming_propagation.parameters as npparameters


#
#
#
#
#

monitoring = common.Monitoring()

_instrumented_ = False


########################################################################################
#
#
#
########################################################################################


def _get_input_file_(f, var=None):
    proc = "_get_input_file_"
    if f is None:
        return None
    if isinstance(f, str):
        return f
    elif isinstance(f, list):
        if len(f) == 1:
            if isinstance(f[0], str):
                return f[0]
            else:
                msg = "first element of variable"
                if var is not None:
                    msg += " '" + str(var) + "'"
                msg += " is not a string"
                monitoring.to_log_and_console(proc + ": " + msg)
                return None
        else:
            msg = "variable"
            if var is not None:
                msg += " '" + str(var) + "'"
            msg += " has more than one element"
            monitoring.to_log_and_console(proc + ": " + msg)
            return None
    msg = "unhandled type (" + str(type(f)) + ") for variable"
    if var is not None:
        msg += " '" + str(var) + "'"
    monitoring.to_log_and_console(proc + ": " + msg)
    return None


########################################################################################
#
# procedures for test
#
########################################################################################


def _build_test_set(prop, n_cells=64, time_digits_for_cell_id=4):
    #
    # from an already named embryo, delete names except at one time point
    #
    proc = "_build_test_set"

    #
    # copy names
    #
    prop['ground_truth_cell_name'] = copy.deepcopy(prop['cell_name'])

    #
    # get cells to count cells per time point
    #
    lineage = prop['cell_lineage']
    cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
    cells = sorted(cells)

    #
    # see also get_times() in components/embryo.py
    #
    div = 10 ** time_digits_for_cell_id
    cells_per_time = {}
    for c in cells:
        if int(c) % div == 1 or int(c) % div == 0:
            continue
        t = int(c) // div
        cells_per_time[t] = cells_per_time.get(t, 0) + 1

    #
    # sort the cells_per_time dictionary by time
    # assume that cell number will also be in increasing order
    #
    cells_per_time = dict(sorted(cells_per_time.items(), key=lambda item: item[0]))

    ncells_per_time = np.zeros((max(cells) // div) + 1)
    for c in cells:
        t = int(c) // div
        ncells_per_time[t] += 1

    #
    # if there are times with the specified number of cells, choose among them
    #
    times = [t for t in cells_per_time if cells_per_time[t] == n_cells]
    if len(times) > 0:
        draw = random.choice(times)
    else:
        mintimes = [t for t in cells_per_time if cells_per_time[t] < n_cells]
        maxtimes = [t for t in cells_per_time if cells_per_time[t] > n_cells]
        if len(mintimes) == 0:
            draw = min(maxtimes)
        elif len(maxtimes) == 0:
            draw = max(mintimes)
        else:
            if cells_per_time[min(maxtimes)] - n_cells < n_cells - cells_per_time[max(mintimes)]:
                draw = min(maxtimes)
            else:
                draw = max(mintimes)
        msg = "no time points with " + str(n_cells) + " cells."
        msg += " Choose time " + str(draw) + " with " + str(cells_per_time[draw]) + " cells."
        monitoring.to_log_and_console(str(proc) + ": " + msg)

    #
    #
    #
    monitoring.to_log_and_console(str(proc) + ": rename from time point " + str(draw))

    cells = list(prop['cell_name'].keys())
    selected_cells = [c for c in cells if int(c) // div == draw]
    selected_name = {c: prop['cell_name'][c] for c in selected_cells}
    prop['cell_name'] = selected_name

    return prop


def _test_naming(prop):

    monitoring.to_log_and_console("")
    monitoring.to_log_and_console("----- test naming vs reference -------")

    #
    # get the cell names with error
    #
    sisters_errors = {}
    other_errors = {}
    division_errors = {}
    key_list = sorted(prop['cell_name'].keys())
    for k in key_list:
        if k not in prop['ground_truth_cell_name']:
            monitoring.to_log_and_console("\t weird, key " + str(k) + " is not in reference properties")
        elif prop['cell_name'][k] != prop['ground_truth_cell_name'][k]:
            #
            # named as its sister
            #
            if prop['ground_truth_cell_name'][k] == aname.get_sister_name(prop['cell_name'][k]):
                if prop['cell_name'][k] not in sisters_errors:
                    sisters_errors[prop['cell_name'][k]] = 1
                    msg = "cell_name[" + str(k) + "]=" + str(prop['cell_name'][k]) + " is named as its sister "
                    msg += str(prop['ground_truth_cell_name'][k])
                    monitoring.to_log_and_console("\t " + msg)
                else:
                    sisters_errors[prop['cell_name'][k]] += 1
                indexed_name = aname.get_mother_name(prop['cell_name'][k])
                division_errors[indexed_name] = division_errors.get(indexed_name, 0) + 1
            #
            # other errors
            #
            else:
                if prop['cell_name'][k] not in other_errors:
                    other_errors[prop['cell_name'][k]] = 1
                    msg = "cell_name[" + str(k) + "]=" + str(prop['cell_name'][k]) + " is not equal to reference name "
                    msg += str(prop['ground_truth_cell_name'][k])
                    msg += " for cell " + str(k)
                    monitoring.to_log_and_console("\t " + msg)
                else:
                    other_errors[prop['cell_name'][k]] += 1

    #
    # keep trace of the test result as a selection
    #
    keyleaveoneout = 'morphonet_selection_leave_one_out_errors'
    lineage = prop['cell_lineage']
    name = prop['cell_name']
    prop[keyleaveoneout] = {}
    cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))

    for c in cells:
        if c not in name:
            continue
        if name[c] in other_errors:
            prop[keyleaveoneout][c] = 255
        elif name[c] in division_errors:
            prop[keyleaveoneout][c] = 100
        elif name[c] in sisters_errors:
            prop[keyleaveoneout][c] = 200

    #
    # if an error occur, a bad choice has been made at the mother division (first_errors)
    # or at some ancestor (second_errors)
    #
    # first_errors = {}
    # second_errors = {}
    # first_error_mothers = []
    # names = division_errors.keys()
    # for n in names:
    #     if uname.get_mother_name(n) in names or uname.get_mother_name(n) in first_error_mothers:
    #         second_errors[n] = division_errors[n]
    #     else:
    #         first_errors[n] = division_errors[n]
    #         if uname.get_mother_name(n) not in first_error_mothers:
    #             first_error_mothers.append(uname.get_mother_name(n))

    name_missing = {}
    reference_name = {}
    key_list = sorted(prop['ground_truth_cell_name'].keys())
    for k in key_list:
        indexed_name = prop['ground_truth_cell_name'][k]
        reference_name[indexed_name] = reference_name.get(indexed_name, 0) + 1
        if k not in prop['cell_name']:
            if prop['ground_truth_cell_name'][k] not in name_missing:
                name_missing[prop['ground_truth_cell_name'][k]] = 1
                msg = "reference cell_name[" + str(k) + "]=" + str(prop['ground_truth_cell_name'][k]) + " is not found"
                monitoring.to_log_and_console("\t " + msg)
            else:
                name_missing[prop['ground_truth_cell_name'][k]] += 1

    division_count = 0
    for k in division_errors:
        division_count += division_errors[k]
    other_count = 0
    for k in other_errors:
        other_count += other_errors[k]
    missing_count = 0
    for k in name_missing:
        missing_count += name_missing[k]

    msg = "ground-truth cells = " + str(len(prop['ground_truth_cell_name'])) + " --- "
    msg += "ground-truth names = " + str(len(reference_name)) + " --- "
    msg += "tested cells = " + str(len(prop['cell_name'])) + " --- "
    msg += "retrieved names = " + str(len(reference_name) - len(name_missing)) + "\n"
    msg += "\t missing cell names = " + str(missing_count) + " for " + str(len(name_missing)) + " names --- \n"
    msg += "\t division errors in lineage = " + str(division_count) + " for " + str(len(division_errors)) + " names \n"
    if len(division_errors) > 0:
        msg += "\t    division errors = " + str(sorted(division_errors.keys())) + "\n"
    msg += "\t other errors in lineage = " + str(other_count) + " for " + str(len(other_errors)) + " names --- \n"
    if len(other_errors) > 0:
        msg += "\t    other errors = " + str(sorted(other_errors.keys())) + "\n"
    monitoring.to_log_and_console("summary" + ": " + msg)

    monitoring.to_log_and_console("--------------------------------------")

    return prop


def _test_unequal_divisions(prop, atlases):
    proc = "_test_unequal_divisions"

    lineage = prop['cell_lineage']
    name = prop['cell_name']

    divisions = atlases.get_division()
    atlasdivisions = atlases.get_unequal_division()
    volumes = atlases.get_volume()

    mothercells = [c for c in lineage if len(lineage[c]) == 2]
    errors = []
    tests = []

    for m in mothercells:
        if m not in name:
            continue
        if name[m] not in atlasdivisions:
            continue
        if lineage[m][0] not in name or lineage[m][1] not in name:
            continue
        #
        # here we've got an unequal named division
        #
        if lineage[m][0] not in prop['cell_volume'] or lineage[m][1] not in prop['cell_volume']:
            msg = "weird, either " + str(lineage[m][0])
            msg += " or " + str(lineage[m][1]) + " is not in 'cell_volume' dictionary"
            monitoring.to_log_and_console(proc + ": " + msg)
            continue

        tests += [m]

        if prop['cell_volume'][lineage[m][0]] > prop['cell_volume'][lineage[m][1]]:
            if atlasdivisions[name[m]][0] != name[lineage[m][0]]:
                errors += [m]
        elif prop['cell_volume'][lineage[m][1]] > prop['cell_volume'][lineage[m][0]]:
            if atlasdivisions[name[m]][0] != name[lineage[m][1]]:
                errors += [m]

    if len(errors) > 0:
        error_names = [name[m] for m in errors]
        keyunequal = 'morphonet_selection_unequal_division_errors'
        prop[keyunequal] = {}
        cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
        for c in cells:
            if c not in name:
                continue
            if name[c] in error_names:
                prop[keyunequal][c] = 255

    monitoring.to_log_and_console("")
    monitoring.to_log_and_console("----- test unequal divisions -------")
    msg = "      test " + str(len(tests)) + " unequal divisions"
    msg += " over " + str(len(atlasdivisions)) + " found in atlases "
    monitoring.to_log_and_console(msg)
    msg = "      found " + str(len(errors)) + " potential errors"
    monitoring.to_log_and_console(msg)
    if len(errors) > 0:
        for m in errors:
            msg = "    - division of cell " + str(m) + " (" + str(name[m]) + ") "
            monitoring.to_log_and_console(msg)
            if name[lineage[m][0]] == atlasdivisions[name[m]][0]:
                msg = str(name[lineage[m][0]]) + " / " + str(name[lineage[m][1]]) + " = "
                msg += str(prop['cell_volume'][lineage[m][0]]) + " / " + str(prop['cell_volume'][lineage[m][1]])
                msg += " = {:.4f}".format(prop['cell_volume'][lineage[m][0]] / prop['cell_volume'][lineage[m][1]])
            else:
                msg = str(name[lineage[m][1]]) + " / " + str(name[lineage[m][0]]) + " = "
                msg += str(prop['cell_volume'][lineage[m][1]]) + " / " + str(prop['cell_volume'][lineage[m][0]])
                msg += " = {:.4f}".format(prop['cell_volume'][lineage[m][1]] / prop['cell_volume'][lineage[m][0]])
            monitoring.to_log_and_console("      " + msg)
            for r in divisions[name[m]]:
                msg = str(r) + ": " + str(atlasdivisions[name[m]][0]) + " / " + str(atlasdivisions[name[m]][1]) + " = "
                msg += str(volumes[atlasdivisions[name[m]][0]][r]) + " / " + str(volumes[atlasdivisions[name[m]][1]][r])
                msg += " = {:.4f}".format(volumes[atlasdivisions[name[m]][0]][r] / volumes[atlasdivisions[name[m]][1]][r])
                monitoring.to_log_and_console("        - " + msg)
    monitoring.to_log_and_console("---------------------------------------")

    return prop


########################################################################################
#
#
#
########################################################################################


def _naming_diagnosis(prop, filename, parameters, time_digits_for_cell_id=4):
    diagnosis.monitoring.copy(monitoring)
    monitoring.to_log_and_console("============================================================")
    monitoring.to_log_and_console("===== diagnosis on '" + str(filename) + "'")
    diagnosis.diagnosis(prop, features=['name'], parameters=parameters, time_digits_for_cell_id=time_digits_for_cell_id)
    monitoring.to_log_and_console("============================================================")


########################################################################################
#
#
#
########################################################################################


def _get_ordered_means(distance):
    mean0 = []
    mean1 = []
    for i, v in enumerate(sorted(distance[0].values())):
        if i == 0:
            mean0 += [v]
        else:
            mean0 += [mean0[i-1] + v]
    for i, v in enumerate(sorted(distance[1].values())):
        if i == 0:
            mean1 += [v]
        else:
            mean1 += [mean1[i - 1] + v]
    for i in range(len(mean1)):
        mean0[i] /= i + 1
        mean1[i] /= i + 1
    return mean0, mean1


########################################################################################
#
#
#
########################################################################################


def _get_name(daughters, daughter_names, distance, parameters, debug=False):
    """

    Parameters
    ----------
    daughters: cell ids of the daughter cells
    daughter_names: the 2 possible names
    distance: a dictionary of distances (in [0,1]) indexed by [d][reference_name] where
        - d:
          d = 0: test (contacts[0], contacts[1]) <-> (daughter_names[0], daughter_names[1])
          d = 1: test (contacts[1], contacts[0]) <-> (daughter_names[0], daughter_names[1])
          where contacts[0] = contact vector for daughters[0]
                contacts[1] = contact vector for daughters[1]
    parameters
    debug

    Returns
    -------
    a dictionary indexed by the cell ids of the daughter cells giving their names.

    """

    proc = "_get_name"

    if len(distance[0]) != len(distance[1]):
        monitoring.to_log_and_console(str(proc) + ": weird, atlases number are different")

    # distance is a dictionary of dictionary
    # distance[d in 0,1][ref name]
    # d = 0: test (contacts[0], contacts[1]) <-> (daughter_names[0], daughter_names[1])
    # d = 1: test (contacts[1], contacts[0]) <-> (daughter_names[0], daughter_names[1])
    # ref name in embryos

    name = {}

    #
    # check whether the same decision will be made whatever the number of atlases
    #
    mean0, mean1 = _get_ordered_means(distance)
    count0less1 = sum([1 for i in zip(mean0, mean1) if i[0] <= i[1]])
    count1less0 = len(mean1) - count0less1

    if debug:
        msg = "\t - " + str(count0less1) + " naming choices (" + str(daughters[0]) + ", " + str(daughters[1]) + ") = ("
        msg += daughter_names[0] + ", " + daughter_names[1] + ") = ["
        for i, v in enumerate(mean0):
            msg += "{:1.4f}".format(v)
            if i < len(mean0)-1:
                msg += ", "
        msg += "]"
        msg += " - sum = {:.4f}".format(np.sum(mean0))
        monitoring.to_log_and_console(msg)
        msg = "\t - " + str(count1less0) + " naming choices (" + str(daughters[1]) + ", " + str(daughters[0]) + ") = ("
        msg += daughter_names[0] + ", " + daughter_names[1] + ") = ["
        for i, v in enumerate(mean1):
            msg += "{:1.4f}".format(v)
            if i < len(mean1)-1:
                msg += ", "
        msg += "]"
        msg += " - sum = {:.4f}".format(np.sum(mean1))
        monitoring.to_log_and_console(msg)

    if parameters.selection_method.lower() == 'minimum' or parameters.selection_method.lower() == 'min':
        #
        # renvoie le tuple (ref, value) avec la valeur minimale
        #
        min0 = min(distance[0].items(), key=lambda x: x[1])
        min1 = min(distance[1].items(), key=lambda x: x[1])
        if min0[1] <= min1[1]:
            name[daughters[0]] = daughter_names[0]
            name[daughters[1]] = daughter_names[1]
            natlas = count0less1
        else:
            name[daughters[1]] = daughter_names[0]
            name[daughters[0]] = daughter_names[1]
            natlas = count1less0
        return name, natlas

    if parameters.selection_method.lower() == 'sum' or parameters.selection_method.lower() == 'mean' \
            or parameters.selection_method.lower() == 'average':
        mean0 = statistics.mean([distance[0][a] for a in distance[0]])
        mean1 = statistics.mean([distance[1][a] for a in distance[1]])
        if mean0 <= mean1:
            name[daughters[0]] = daughter_names[0]
            name[daughters[1]] = daughter_names[1]
            natlas = count0less1
        else:
            name[daughters[1]] = daughter_names[0]
            name[daughters[0]] = daughter_names[1]
            natlas = count1less0
        return name, natlas

    if parameters.selection_method.lower() == 'majority':
        if count0less1 > count1less0 or (count0less1 == count1less0 and np.sum(mean0) <= np.sum(mean1)):
            name[daughters[0]] = daughter_names[0]
            name[daughters[1]] = daughter_names[1]
            natlas = count0less1
        else:
            name[daughters[1]] = daughter_names[0]
            name[daughters[0]] = daughter_names[1]
            natlas = count1less0
        return name, natlas

    monitoring.to_log_and_console(str(proc) + ": selection method '" + str(parameters.selection_method) +
                                  "' not handled yet")
    return name, 0


########################################################################################
#
# evaluation procedure
#
########################################################################################


def _get_evaluation(given_names, daughter_names, distance, parameters):
    proc = "_get_evaluation"

    # distance is a dictionary of dictionary
    # distance[d in 0,1][ref name]
    # d = 0: test (contacts[0], contacts[1]) <-> (daughter_names[0], daughter_names[1])
    # d = 1: test (contacts[1], contacts[0]) <-> (daughter_names[0], daughter_names[1])
    # ref name in embryos

    if given_names[0] == daughter_names[0] and given_names[1] == daughter_names[1]:
        dist = [(distance[0][a], distance[1][a]) for a in distance[0]]
    elif given_names[0] == daughter_names[1] and given_names[1] == daughter_names[0]:
        dist = [(distance[1][a], distance[0][a]) for a in distance[0]]
    else:
        msg = ": weird, names are not switched or non-switched ?!"
        monitoring.to_log_and_console(str(proc) + msg)
        return None, None

    sorted_dist = sorted(dist, key=lambda v: v[0])

    natlas = int(round(parameters.confidence_atlases_percentage * len(sorted_dist) / 100.0))
    if parameters.confidence_atlases_nmin > 0:
        natlas = max(parameters.confidence_atlases_nmin, natlas)
    if natlas <= 0 or len(sorted_dist) < natlas:
        return None, None

    mean0 = 0.0
    mean1 = 0.0
    for i in range(natlas):
        mean0 += sorted_dist[i][0]
        mean1 += sorted_dist[i][1]
    mean0 /= natlas
    mean1 /= natlas
    return mean0, mean1 - mean0


def _evaluate_naming(prop, embryo, atlases, parameters):
    proc = "_evaluate_naming"

    if isinstance(embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryos' variable: " + str(type(embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if isinstance(atlases, cdivatlas.DivisionAtlases) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'atlases' variable: " + str(type(atlases)))
        sys.exit(1)

    if not isinstance(parameters, npparameters.NamingPropagationParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    cell_lineage = embryo.cell_lineage
    if cell_lineage is None:
        monitoring.to_log_and_console(str(proc) + ": embryo to be named has no 'cell_lineage'")
        return None

    cell_contact_surface = embryo.rectified_cell_contact_surface
    if cell_contact_surface is None:
        monitoring.to_log_and_console(str(proc) + ": embryo to be named has no 'cell_contact_surface'")
        return None

    cell_name = embryo.cell_name
    if cell_name is None:
        monitoring.to_log_and_console(str(proc) + ": embryo to be named has no 'cell_name'")
        return None

    #
    # initialize 'morphonet_float_name_choice_certainty'
    #
    keymindistance = 'morphonet_float_name_choice_min_distance'
    prop[keymindistance] = {}

    #
    #
    #
    reverse_lineage = {v: k for k, values in cell_lineage.items() for v in values}
    cells = list(set(cell_lineage.keys()).union(set([v for values in list(cell_lineage.values()) for v in values])))

    #
    # forward propagation
    #
    monitoring.to_log_and_console(str(proc) + ": forward propagation")

    cells = sorted(cells)
    div = 10 ** embryo.time_digits_for_cell_id
    cells_per_time = {}
    for c in cells:
        t = int(c) // div
        #
        # get cells and cell names at each time point
        #
        if t not in cells_per_time:
            cells_per_time[t] = [c]
        else:
            cells_per_time[t].append(c)
    timepoints = sorted(cells_per_time.keys())

    processed_mothers = []

    for t in timepoints:
        for c in cells_per_time[t]:
            if c not in cell_name:
                continue
            if c not in reverse_lineage:
                continue

            # get its mother
            mother = reverse_lineage[c]
            if mother in processed_mothers:
                continue
            processed_mothers += [mother]

            # mother not in lineage
            # to account for lineage errors
            if mother not in cell_lineage:
                monitoring.to_log_and_console(str(proc) + ": weird, cell " + str(mother) + " is not in lineage")
                continue

            if len(cell_lineage[mother]) == 1:
                if mother not in prop[keymindistance]:
                    continue
                prop[keymindistance][c] = prop[keymindistance][mother]
                continue

            if len(cell_lineage[mother]) > 2:
                msg = ": weird, cell " + str(mother) + " divides in " + str(len(cell_lineage[mother])) + " cells "
                monitoring.to_log_and_console(str(proc) + msg)
                continue

            # here len(lineage[mother]) == 2, this is a division
            daughters = cell_lineage[mother]
            if daughters[0] not in cell_name or daughters[1] not in cell_name:
                msg = ": weird, cells " + str(daughters) + " are not both named "
                monitoring.to_log_and_console(str(proc) + msg)
                continue

            #
            # distance is a dictionary of dictionary
            # distance[d in 0,1][ref name]
            # d = 0: test (contacts[0], contacts[1]) <-> (daughter_names[0], daughter_names[1])
            # d = 1: test (contacts[1], contacts[0]) <-> (daughter_names[0], daughter_names[1])
            # ref name in embryos
            #
            # certainty are probabilities (in [0, 1])
            #
            debug = isinstance(parameters.cells_to_be_traced, list) and \
                    prop['cell_name'][mother] in parameters.cells_to_be_traced

            daughter_names = aname.get_daughter_names(prop['cell_name'][mother])
            given_names = [cell_name[daughters[0]], cell_name[daughters[1]]]

            if debug:
                msg = "===== un-homogenized neighborhood for division of " + str(cell_name[mother])
                monitoring.to_log_and_console(msg)
            #
            # distance[i][ref] are division distances
            # - between the embryo to assess and each of the atlas 'ref' at hand
            # - i=0: test (contacts[0], contacts[1]) <-> (daughter_names[0], daughter_names[1])
            # - i=1: test (contacts[1], contacts[0]) <-> (daughter_names[0], daughter_names[1])
            #
            distance = npscore.naming_scores(mother, daughters, embryo, atlases, parameters,
                                             delay_from_division=parameters.delay_from_division, debug=debug)
            if distance is None:
                continue
            #
            # mindist is the average division distance from the embryo to assess to all the atlases at hand
            #         when the names are in the same order
            # difference = maxdist - mindist
            #         ie the difference between the average division distances of the two choices
            #
            mindist, difference = _get_evaluation(given_names, daughter_names, distance, parameters)

            #
            # mean'i' is the list of average of the 'i' first sorted distances from distance[i]
            # allows to check whether the same naming choice will be done whatever the number of chosen atlases
            #
            mean0, mean1 = _get_ordered_means(distance)
            count0less1 = sum([1 for i in zip(mean0, mean1) if i[0] <= i[1]])
            if 0 < count0less1 < len(distance[0]):
                if given_names[0] == daughter_names[0]:
                    natlas = count0less1
                else:
                    natlas = len(distance[0]) - count0less1
                msg = "partial agreement to evaluate division of cell " + str(prop['cell_name'][mother])
                monitoring.to_log_and_console(str(proc) + ": " + msg)
                msg = "\t " + str(natlas) + " over " + str(len(distance[0])) + " means of ordered distances"
                msg += " (with un-homogenized neighborhood)"
                monitoring.to_log_and_console(msg)

            if difference is None:
                continue
            for d in daughters:
                prop[keymindistance][d] = mindist
            if debug:
                msg = "----- naming assessment with un-homogenized neighborhood for division of "
                msg += str(prop['cell_name'][mother])
                monitoring.to_log_and_console(msg)
                msg = "      difference = " + str(difference)
                monitoring.to_log_and_console(msg)
                msg = "      distance   = " + str(mindist)
                monitoring.to_log_and_console(msg)
    return prop


########################################################################################
#
#
#
########################################################################################


def _naming_propagation(prop, embryo, atlases, parameters):
    proc = "_propagate_naming"

    if isinstance(embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryos' variable: " + str(type(embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if isinstance(atlases, cdivatlas.DivisionAtlases) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'atlases' variable: " + str(type(atlases)))
        sys.exit(1)

    if not isinstance(parameters, npparameters.NamingPropagationParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    cell_lineage = embryo.cell_lineage
    if cell_lineage is None:
        monitoring.to_log_and_console(str(proc) + ": embryo to be named has no 'cell_lineage'")
        return None

    cell_contact_surface = embryo.rectified_cell_contact_surface
    if cell_contact_surface is None:
        monitoring.to_log_and_console(str(proc) + ": embryo to be named has no 'cell_contact_surface'")
        return None

    cell_name = embryo.cell_name
    if cell_name is None:
        monitoring.to_log_and_console(str(proc) + ": embryo to be named has no 'cell_name'")
        return None

    #
    #
    #
    keyagreement = 'morphonet_float_name_choice_agreement'
    prop[keyagreement] = {}

    #
    # remove empty names
    # leading or trailing spaces
    #
    cells = list(cell_name.keys())
    for c in cells:
        if cell_name[c] == '':
            del cell_name[c]
            continue
        cell_name[c] = cell_name[c].strip()

    #
    #
    #
    reverse_lineage = {v: k for k, values in cell_lineage.items() for v in values}
    cells = list(set(cell_lineage.keys()).union(set([v for values in list(cell_lineage.values()) for v in values])))

    #
    # backward propagation
    #
    monitoring.to_log_and_console(str(proc) + ": backward propagation")
    cells = sorted(cells, reverse=True)
    for c in cells:
        if c not in cell_name:
            continue
        if c not in reverse_lineage:
            continue
        mother = reverse_lineage[c]
        if len(cell_lineage[mother]) == 1:
            if mother in cell_name:
                if cell_name[mother] != cell_name[c]:
                    msg = ": weird, cell " + str(mother) + " is named " + str(cell_name[mother])
                    msg += ", but should be named " + str(cell_name[c])
                    msg += " as its single daughter"
                    monitoring.to_log_and_console(str(proc) + msg)
            else:
                cell_name[mother] = cell_name[c]
        elif len(cell_lineage[mother]) == 2:
            mother_name = aname.get_mother_name(cell_name[c])
            if mother in cell_name:
                if cell_name[mother] != mother_name:
                    msg = ": weird, cell " + str(mother) + " is named " + str(cell_name[mother])
                    msg += ", but should be named " + str(mother_name)
                    msg += " since one of its daughter is named " + str(cell_name[c])
                    monitoring.to_log_and_console(str(proc) + msg)
            else:
                cell_name[mother] = mother_name
        else:
            msg = ": weird, cell " + str(mother) + " has " + str(len(cell_lineage[mother])) + "daughter(s)"
            monitoring.to_log_and_console(str(proc) + msg)

    #
    # forward propagation
    #
    monitoring.to_log_and_console(str(proc) + ": forward propagation")

    cells = sorted(cells)
    div = 10 ** embryo.time_digits_for_cell_id
    cells_per_time = {}
    missing_name = {}
    for c in cells:
        t = int(c) // div
        #
        # get cells and cell names at each time point
        #
        if t not in cells_per_time:
            cells_per_time[t] = [c]
        else:
            cells_per_time[t].append(c)
        if c not in cell_name:
            if t not in missing_name:
                missing_name[t] = [c]
            else:
                missing_name[t].append(c)

    timepoints = sorted(missing_name.keys())
    cell_not_named = []

    for t in timepoints:
        division_to_be_named = {}
        for c in missing_name[t]:

            # already named
            if c in cell_name:
                continue
            # root of a tree, can not be named
            if c not in reverse_lineage:
                if c not in cell_not_named:
                    cell_not_named.append(c)
                monitoring.to_log_and_console(str(proc) + ": weird, cell " + str(c) + " is root of a subtree")
                continue

            # get its mother
            mother = reverse_lineage[c]
            # mother not in lineage
            # to account for lineage errors
            if mother not in cell_lineage:
                monitoring.to_log_and_console(str(proc) + ": weird, cell " + str(mother) + " is not in lineage")
                continue
            # mother not named, can name the cell either
            if mother in cell_not_named:
                if c not in cell_not_named:
                    cell_not_named.append(c)
                continue
            # mother not named, can name the cell either
            if mother not in cell_name:
                if mother not in cell_not_named:
                    cell_not_named.append(mother)
                if c not in cell_not_named:
                    cell_not_named.append(c)
                msg = "mother cell " + str(mother) + " is not named."
                msg += " Can not name cell " + str(c) + " either."
                monitoring.to_log_and_console(str(proc) + ": " + msg, 5)
                continue
            #
            # easy case
            # give name to cells that are only daughter and propagate along branch
            #
            if len(cell_lineage[mother]) == 1:
                cell_name[c] = cell_name[mother]
                if mother in prop[keyagreement]:
                    prop[keyagreement][c] = prop[keyagreement][mother]
            #
            # in case of division:
            # 1. give name if the sister cell is named
            # 2. keep divisions to be solved
            #
            elif len(cell_lineage[mother]) == 2:
                daughters = copy.deepcopy(cell_lineage[mother])
                daughters.remove(c)
                daughter_names = aname.get_daughter_names(cell_name[mother])
                #
                # daughter cell is already named
                #
                if daughters[0] in cell_name:
                    if cell_name[daughters[0]] in daughter_names:
                        daughter_names.remove(cell_name[daughters[0]])
                        cell_name[c] = daughter_names[0]
                    else:
                        msg = ": weird, cell " + str(daughters[0]) + " is named " + str(cell_name[daughters[0]])
                        msg += ", but should be named in " + str(daughter_names) + " since its mother cell "
                        msg += str(mother) + " is named " + str(cell_name[mother])
                        monitoring.to_log_and_console(str(proc) + msg)
                    continue
                #
                # both daughters are not named
                #
                division_to_be_named[mother] = [daughters[0], c]
            else:
                if mother not in cell_not_named:
                    cell_not_named.append(mother)
                cell_not_named.append(c)
                msg = ": weird, cell " + str(mother) + " has " + str(len(cell_lineage[mother])) + " daughter(s)."
                msg += " Its offspring '" + str(cell_lineage[mother]) + "' will not be named."
                monitoring.to_log_and_console(str(proc) + msg)

        if division_to_be_named == {}:
            continue

        #
        # update embryo cell names
        #
        embryo.cell_name = cell_name

        #
        # here we have a dictionary of divisions to be named (key = mother id, value = array of sister ids)
        #
        for mother, daughters in division_to_be_named.items():
            debug = False
            if parameters.cells_to_be_traced is not None and cell_name[mother] in parameters.cells_to_be_traced:
                debug = True
            #
            # distance is a dictionary of dictionary
            # distance[d in 0,1][ref name]
            # d = 0: test (contacts[0], contacts[1]) <-> (daughter_names[0], daughter_names[1])
            # d = 1: test (contacts[1], contacts[0]) <-> (daughter_names[0], daughter_names[1])
            # ref name in embryos
            #
            scores = npscore.naming_scores(mother, daughters, embryo, atlases, parameters,
                                           delay_from_division=parameters.delay_from_division, debug=debug)
            if debug:
                monitoring.to_log_and_console("Naming distance = " + str(scores))
            if scores is None:
                for c in daughters:
                    if c not in cell_not_named:
                        cell_not_named.append(c)
                # msg = "\t error when building scores for daughters " + str(daughters) + " of cell " + str(mother)
                # monitoring.to_log_and_console(msg)
                msg = " Can not name cells " + str(daughters) + " and their offsprings."
                monitoring.to_log_and_console(str(proc) + ": " + msg)
                continue

            daughter_names = aname.get_daughter_names(cell_name[mother])
            name, natlas = _get_name(daughters, daughter_names, scores, parameters, debug=debug)

            for c in name:
                if name[c] is not None:
                    cell_name[c] = name[c]
            embryo.cell_name = cell_name
            if 0 < natlas < len(scores[0]):
                for c in name:
                    prop[keyagreement][c] = natlas/len(scores[0])
                msg = "partial agreement to name division of cell " + str(cell_name[mother])
                monitoring.to_log_and_console(str(proc) + ": " + msg)
                msg = "\t " + str(natlas) + " over " + str(len(scores[0])) + " means of ordered distances"
                monitoring.to_log_and_console(msg)

    if len(prop[keyagreement]) == 0:
        del prop[keyagreement]
    else:
        embryo.set_property(prop[keyagreement], keyagreement)
    prop['cell_name'] = cell_name
    #
    # add to add names
    #
    return prop


########################################################################################
#
#
#
########################################################################################


def naming_propagation_process(experiment, parameters):
    proc = "naming_propagation_process"

    #
    # parameter type checking
    #

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(parameters, npparameters.NamingPropagationParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    time_digits_for_cell_id = experiment.get_time_digits_for_cell_id()

    #
    # parameter is required here to set the reference atlas (if given)
    #   time warping is done wrt to this reference atlas
    #   properties are copied into each atlas (without normalisation)
    #   volume normalisation is also computed
    #
    cembryo.monitoring.copy(monitoring)
    cembryoset.monitoring.copy(monitoring)

    embryos = cembryoset.EmbryoSet(parameters=parameters)
    embryos.add_embryos(parameters.atlasEmbryos, parameters,
                        time_digits_for_cell_id=experiment.get_time_digits_for_cell_id())

    if embryos.n_embryos() == 0:
        monitoring.to_log_and_console(str(proc) + ": no atlases have been read?!")
        sys.exit(1)

    #
    # add symmetrical atlases if required
    #
    if parameters.add_symmetric_embryo is not False:
        embryos.add_symmetrical_embryos()

    cdivatlas.monitoring.copy(monitoring)
    atlases = cdivatlas.DivisionAtlases(embryoset=embryos, parameters=parameters)
    #
    #
    #

    if parameters.generate_figure is True or isinstance(parameters.generate_figure, str) is True or \
            isinstance(parameters.generate_figure, list) is True:
        test_embryos = None
        if len(parameters.testEmbryos) > 0:
            test_embryos = cembryoset.EmbryoSet(parameters=parameters)
            test_embryos.add_embryos(parameters.testEmbryos, parameters,
                                     time_digits_for_cell_id=experiment.get_time_digits_for_cell_id())
        fgenerate.monitoring.copy(monitoring)
        fgenerate.generate_figure(embryos, parameters, test_embryos=test_embryos)
        fgenerate.generate_figure(atlases, parameters, test_embryos=test_embryos)
        return None

    #
    # read input properties to be named
    #
    inputfile = None
    prop = {}

    #
    # test case: read a property file with names
    #
    testfile = _get_input_file_(parameters.testFile)
    if testfile is not None:
        prop = ioproperties.read_dictionary(testfile, inputpropertiesdict={}, verbose=(monitoring.debug > 0))
        if 'cell_name' not in prop:
            monitoring.to_log_and_console(str(proc) + ": 'cell_name' is not in '" + str(testfile) + "'")
            sys.exit(1)
        if parameters.test_diagnosis:
            _naming_diagnosis(prop, testfile, parameters, time_digits_for_cell_id=time_digits_for_cell_id)
        inputfile = testfile
        #
        # copy prop['cell_name'] into prop['ground_truth_cell_name']
        # clean prop['cell_name']
        #
        prop = _build_test_set(prop, n_cells=64, time_digits_for_cell_id=time_digits_for_cell_id)

    #
    # normal case
    #
    elif parameters.inputFile is not None:
        inputfile = _get_input_file_(parameters.inputFile)
        if inputfile is not None:
            prop = ioproperties.read_dictionary(parameters.inputFile, inputpropertiesdict={},
                                                verbose=(monitoring.debug > 0))
            if 'cell_name' not in prop:
                monitoring.to_log_and_console(str(proc) + ": 'cell_name' is not in '" + str(inputfile) + "'")
                sys.exit(1)
            if parameters.test_diagnosis:
                _naming_diagnosis(prop, inputfile, parameters, time_digits_for_cell_id=time_digits_for_cell_id)

    if inputfile is None:
        monitoring.to_log_and_console(str(proc) + ": no input nor test file?!")
        return prop

    if prop == {}:
        monitoring.to_log_and_console(str(proc) + ": no properties?!")
        sys.exit(1)

    #
    # processing
    #
    name = inputfile.split(os.path.sep)[-1]
    if name.endswith(".xml") or name.endswith(".pkl"):
        name = name[:-4]

    #
    # clean from empty names
    # Use historically for skewed property file
    #
    cells = list(prop['cell_name'].keys())
    for c in cells:
        if prop['cell_name'][c] == '':
            del prop['cell_name'][c]

    #
    # build an atlas from the embryo to be named,
    # temporally align it with the reference embryo
    #
    embryo_tobenamed = cembryo.Embryo(prop, name=name, time_digits_for_cell_id=time_digits_for_cell_id)

    #
    # temporally align the test/read embryo with the reference embryo
    #
    embryo_tobenamed.temporally_align_with(embryos.get_reference_embryo())
    msg = "   ... "
    msg += "linear time warping of '" + str(name) + "' wrt '" + str(embryos.get_reference_embryo_name()) + "' is "
    msg += "({:.3f}, {:.3f})".format(embryo_tobenamed.temporal_alignment[0], embryo_tobenamed.temporal_alignment[1])
    monitoring.to_log_and_console(msg, 1)

    #
    # naming propagation
    # _naming_propagation() fills prop['morphonet_float_name_choice_agreement']
    # _evaluate_naming() fills prop['morphonet_float_name_choice_min_distance']
    #
    prop = _naming_propagation(prop, embryo_tobenamed, atlases, parameters)
    prop = _evaluate_naming(prop, embryo_tobenamed, atlases, parameters)

    #
    #
    #
    prop = _test_unequal_divisions(prop, atlases)

    #
    # set fate
    #
    prop = afate.set_fate_from_names(prop)
    prop = afate.set_color_from_fate(prop)
    if parameters.testFile is not None:
        prop = _test_naming(prop)
        del prop['ground_truth_cell_name']

    if isinstance(parameters.outputFile, str):
        ioproperties.write_dictionary(parameters.outputFile, prop)

    return prop

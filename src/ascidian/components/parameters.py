##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Ven 21 jui 2024 11:27:20 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

import astec.utils.common as common

import ascidian.core.diagnosis as diagnosis
import ascidian.symmetry.parameters as sparameters

monitoring = common.Monitoring()


##############################################################
#
# figures
#
##############################################################

class FigureParameters(common.PrefixedParameter):

    def __init__(self, prefix=None):

        if "doc" not in self.__dict__:
            self.doc = {}

        common.PrefixedParameter.__init__(self, prefix=prefix)
        #
        # atlas general parameters
        #

        doc = "\t Output directory where to write atlas-individualized output files,\n"
        doc += "\t ie morphonet selection files or figure files."
        self.doc['outputDir'] = doc
        self.outputDir = "."

        doc = "\t suffix used to named the above python files as well as the generated figures."
        self.doc['figurefile_suffix'] = doc
        self.figurefile_suffix = ""

        doc = "\t if True, generate python files (prefixed by 'figure_') that generate figures.\n"
        doc += "\t Those files will be saved into the 'outputDir' directory.\n"
        doc += "\t 'generate_figure' can be\n"
        doc += "\t - a boolean value: if True, all figure files are generated; if False, none of them\n"
        doc += "\t - a string: if 'all', all figure files are generated; else, only the specified\n"
        doc += "\t   figure file is generated (see below for the list)\n"
        doc += "\t - a list of strings: if 'all' is in the list, all figure files are generated;\n"
        doc += "\t   else, only the specified figure files are generated (see below for the list)\n"
        doc += "\t"
        doc += "\t List of figures:\n"
        doc += "\t 'cell-composition': plot the number of names wrt time point (normalized time\n"
        doc += "\t    ie after temporal registration), and separate between names present in all \n"
        doc += "\t    embryos and others. It offers a means to visually assess the heterechronocity\n"
        doc += "\t    of divisions.\n"
        doc += "\t 'cell-to-cell-distance-along-branch': plot the cell-to-cell distance between \n"
        doc += "\t    successive along a branch (a cell without division) wrt the distance to the\n"
        doc += "\t    first cell. Cell neighborhoods are expressed with the neighbors of the first\n"
        doc += "\t    cell of the branch (thus it ignores divisions occurring in the cell \n"
        doc += "\t    neighborhood during the cell life).\n"
        doc += "\t 'cell-to-cell-distance-histogram': plot the cell-to-cell distance between\n"
        doc += "\t    the same cell (cell of same name) between embryos, or between a cell and its\n"
        doc += "\t    cells (mostly inter-embryo, but one measure is within the same embryo), or\n"
        doc += "\t    between a cell and other (than the same and the sister) cells.\n"
        doc += "\t 'cell-neighbor-count': plot the cell number in the cell neighborhood wrt\n"
        doc += "\t    the total cell number in the embryo.\n"
        doc += "\t 'division-to-division-distance-histogram': plot the division-to-division\n"
        doc += "\t    for both right and wrong pairings of the daughter cells.\n"
        doc += "\t 'division-time-couple-misordering': assess the heterochrony of divisions\n"
        doc += "\t    in a set of embryos for a couple of divisions. The list of couples\n"
        doc += "\t    to be studied is to be edited in the 'figure_' python file.\n"
        doc += "\t 'division-time-difference': assess the synchrony of division\n"
        doc += "\t    for bilateral cells (ie the pair of corresponding right and left cells)\n"
        doc += "\t    with their difference in division times for a given embryo.\n"
        doc += "\t 'division-time-global-misordering': assess the heterochrony of divisions\n"
        doc += "\t    in a set of embryos, by plotting the probablity that a couple of divisions\n"
        doc += "\t    does not follow the 'natural' order (given by ordering the average times\n"
        doc += "\t    of division).\n"
        doc += "\t 'symmetrical-cell-percentage': assess the recognition of symmetrical cells\n"
        doc += "\t    when computing the symmetry axis of an embryo, by plotting the percentage\n"
        doc += "\t    of well recognized symmetrical cells (the symmetry axis will be computed\n"
        doc += "\t    the direction distribution issued from pair of symmetrical cells).\n"
        doc += "\t    Warning: computational time may be long.\n"
        doc += "\t 'symmetry-axis-assessment': assess the computation of the symmetry axis.\n"
        doc += "\t    The symmetry axis is computed as a maximum of a direction distribution:\n"
        doc += "\t    it is assessed by the rank of the (ordered) maximum closest to the\n"
        doc += "\t    true symmetry axis, and by the angle between this maximum and the true\n"
        doc += "\t    symmetry axis.\n"
        doc += "\t    Warning: computational time may be long.\n"
        doc += "\t 'temporal-alignment': for comparison, embryos may be required to be\n"
        doc += "\t    temporally aligned. This is achieved by a time linear warping\n"
        doc += "\t    on the (total) cell counts (it is assumed that cell counts reflects the\n"
        doc += "\t    'developmental' time). In a linear warping, 'a*t+b', 'a' corrects \n"
        doc += "\t    for the development speed, that depends on the temperature, and 'b'' corrects\n"
        doc += "\t    for the initial acquisition time, that differs from one embryo to the other.\n"
        doc += "\t 'temporal-alignment-epidermis': for perturbed development (eg when adding\n"
        doc += "\t    the MEK inhibitor U0126, the total cell count does not reflect the\n"
        doc += "\t    'developmental' time anymore. Since U0126 does not perturb the epidermis cells,\n"
        doc += "\t    it is then possible to compute the linear warping on the epidermis cell counts.\n"
        doc += "\t 'volume-fitting': plot the embryo volume (in voxel) without and with temporal \n"
        doc += "\t    registration (computed from cell number). The decreasing volume of embryos\n"
        doc += "\t    can be visually assessed, well as the linear regression (for intra- and\n"
        doc += "\t    inter-embryo volume normalization). This linear regression is used to compare\n"
        doc += "\t    embryos (eg surface contact values) at any times by normalizing\n"
        doc += "\t    their volumes to a common value.\n"
        self.doc['generate_figure'] = doc
        self.generate_figure = False

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# FigureParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        self.varprint('outputDir', self.outputDir)
        self.varprint('figurefile_suffix', self.figurefile_suffix)
        self.varprint('generate_figure', self.generate_figure)

        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# FigureParameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'outputDir', self.outputDir, self.doc.get('outputDir', None))
        self.varwrite(logfile, 'figurefile_suffix', self.figurefile_suffix, self.doc.get('figurefile_suffix', None))
        self.varwrite(logfile, 'generate_figure', self.generate_figure, self.doc.get('generate_figure', None))

        logfile.write("\n")
        return

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):

        self.outputDir = self.read_parameter(parameters, 'outputDir', self.outputDir)
        self.figurefile_suffix = self.read_parameter(parameters, 'figurefile_suffix', self.figurefile_suffix)
        self.generate_figure = self.read_parameter(parameters, 'generate_figure', self.generate_figure)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            print("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)


class BaseParameters(diagnosis.DiagnosisParameters, sparameters.SymmetryParameters, FigureParameters):

    ############################################################
    #
    # initialisation
    #
    ############################################################

    def __init__(self, prefix=None):

        if "doc" not in self.__dict__:
            self.doc = {}

        diagnosis.monitoring.copy(monitoring)
        sparameters.monitoring.copy(monitoring)

        diagnosis.DiagnosisParameters.__init__(self, prefix=prefix)
        sparameters.SymmetryParameters.__init__(self, prefix=prefix)
        FigureParameters.__init__(self, prefix=prefix)

        #
        # atlas general parameters
        #

        doc = "\t List of atlas embryo files. An atlas file is a property file that contains lineage,\n"
        doc += "\t names, and contact surfaces for an embryo. They will be used as reference for naming\n"
        doc += "\t or figures may be built from their content.\n"
        self.doc['atlasEmbryos'] = doc
        self.atlasEmbryos = []

        doc = "\t Reference embryo. Use for time alignment. If not provided, the first atlas of\n"
        doc += "\t 'atlasEmbryos' is used as reference. Warning, the reference atlas has to be in\n"
        doc += "\t 'atlasEmbryos' list also."
        self.doc['referenceEmbryo'] = doc
        self.referenceEmbryo = None

        doc = "\t List of atlas embryo files. When figures are built by comparing one or several\n"
        doc += "\t embryos against a set of atlas embryos, it can be done on a leave-one-out basis,\n"
        doc += "\t or by comparing test embryos against atlas embryos.\n"
        self.doc['testEmbryos'] = doc
        self.testEmbryos = []

        doc = "\t None, True or False. If True, add symmetrical embryos to the atlas embryos.\n"
        doc += "\t If None, adopt a contextual default behavior.\n"
        self.doc['add_symmetric_embryo'] = doc
        self.add_symmetric_embryo = None

        #
        #
        #
        doc = "\t True or False. Performs some diagnosis when reading an additional property file \n"
        doc += "\t into the atlases. Incrementing the verboseness ('-v' in the command line) may give\n"
        doc += "\t more details."
        self.doc['atlas_diagnosis'] = doc
        self.atlas_diagnosis = False

        #
        #
        #
        doc = "\t To put embryos in the same time reference, acquisition times (image index)\n"
        doc += "\t are linearly registered against the reference embryo.\n"
        doc += "\t Registration is done by comparing cell counts in the given interval.\n"
        doc += "\t None means that interval (lower or upper) limit is calculated from data.\n"
        doc += "\t Note: this was added for test purposes. No benefit has been shown so far\n"
        doc += "\t       to set this value.\n"
        self.doc['temporal_alignment_min_cell_count'] = doc
        self.temporal_alignment_min_cell_count = None
        self.doc['temporal_alignment_max_cell_count'] = doc
        self.temporal_alignment_max_cell_count = None

        doc = "\t To put embryos in the same time reference, acquisition times (image index)\n"
        doc += "\t are linearly registered against the reference embryo.\n"
        doc += "\t Registration is done by comparing cell counts of given fate.\n"
        doc += "\t None means that all cells are used.\n"
        doc += "\t 'Epidermis' should be used when U0126 embryos are in the set.\n"
        self.doc['temporal_alignment_fate'] = doc
        self.temporal_alignment_fate = None

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# BaseParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        diagnosis.DiagnosisParameters.print_parameters(self)
        sparameters.SymmetryParameters.print_parameters(self)
        FigureParameters.print_parameters(self)

        self.varprint('atlasEmbryos', self.atlasEmbryos)
        self.varprint('referenceEmbryo', self.referenceEmbryo)
        self.varprint('testEmbryos', self.testEmbryos)

        self.varprint('add_symmetric_embryo', self.add_symmetric_embryo)

        self.varprint('atlas_diagnosis', self.atlas_diagnosis)

        self.varprint('temporal_alignment_min_cell_count', self.temporal_alignment_min_cell_count)
        self.varprint('temporal_alignment_max_cell_count', self.temporal_alignment_max_cell_count)
        self.varprint('temporal_alignment_fate', self.temporal_alignment_fate)
        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# BaseParameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        diagnosis.DiagnosisParameters.write_parameters_in_file(self, logfile)
        sparameters.SymmetryParameters.write_parameters_in_file(self, logfile)
        FigureParameters.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'atlasEmbryos', self.atlasEmbryos, self.doc.get('atlasEmbryos', None))
        self.varwrite(logfile, 'referenceEmbryo', self.referenceEmbryo, self.doc.get('referenceEmbryo', None))
        self.varwrite(logfile, 'testEmbryos', self.testEmbryos, self.doc.get('testEmbryos', None))

        self.varwrite(logfile, 'add_symmetric_embryo', self.add_symmetric_embryo,
                      self.doc.get('add_symmetric_embryo', None))

        self.varwrite(logfile, 'atlas_diagnosis', self.atlas_diagnosis, self.doc.get('atlas_diagnosis', None))

        self.varwrite(logfile, 'temporal_alignment_min_cell_count', self.temporal_alignment_min_cell_count,
                      self.doc.get('temporal_alignment_min_cell_count', None))
        self.varwrite(logfile, 'temporal_alignment_max_cell_count', self.temporal_alignment_max_cell_count,
                      self.doc.get('temporal_alignment_max_cell_count', None))
        self.varwrite(logfile, 'temporal_alignment_fate', self.temporal_alignment_fate,
                      self.doc.get('temporal_alignment_fate', None))
        logfile.write("\n")
        return

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):

        diagnosis.DiagnosisParameters.update_from_parameters(self, parameters)
        sparameters.SymmetryParameters.update_from_parameters(self, parameters)
        FigureParameters.update_from_parameters(self, parameters)

        self.atlasEmbryos = self.read_parameter(parameters, 'atlasFiles', self.atlasEmbryos)
        self.atlasEmbryos = self.read_parameter(parameters, 'atlasEmbryos', self.atlasEmbryos)
        self.referenceEmbryo = self.read_parameter(parameters, 'referenceEmbryo', self.referenceEmbryo)
        self.testEmbryos = self.read_parameter(parameters, 'testEmbryos', self.testEmbryos)

        self.add_symmetric_embryo = self.read_parameter(parameters, 'add_symmetric_embryo', self.add_symmetric_embryo)

        self.atlas_diagnosis = self.read_parameter(parameters, 'atlas_diagnosis', self.atlas_diagnosis)

        self.temporal_alignment_min_cell_count = self.read_parameter(parameters, 'temporal_alignment_min_cell_count',
                                                                     self.temporal_alignment_min_cell_count)
        self.temporal_alignment_max_cell_count = self.read_parameter(parameters, 'temporal_alignment_max_cell_count',
                                                                     self.temporal_alignment_max_cell_count)
        self.temporal_alignment_fate = self.read_parameter(parameters, 'temporal_alignment_fate',
                                                           self.temporal_alignment_fate)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            print("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)

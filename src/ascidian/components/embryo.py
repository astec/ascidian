##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Ven 21 jui 2024 11:27:20 CEST
#
##############################################################
#
#
#
##############################################################

import copy
import os
import sys

from operator import itemgetter

import numpy as np
import sklearn.linear_model as sklm

import astec.utils.common as common
import astec.utils.ioproperties as ioproperties

import ascidian.ascidian.fate as afate
import ascidian.ascidian.name as aname
import ascidian.core.temporal_alignment as talign
import ascidian.symmetry.embryo as sembryo
import ascidian.symmetry.parameters as sparameters

monitoring = common.Monitoring()


############################################################
#
# Embryo = one property file
#
############################################################

class Embryo(object):
    def __init__(self, properties=None, filename=None, name=None, time_digits_for_cell_id=4, add_all_properties=False,
                 verbose=False):
        proc = "Embryo.init"

        #
        # embryo based properties
        #
        # - time alignment
        #   developpmental time = embryo_temporal_alignment[k][0] * timepoint + embryo_temporal_alignment[k][1]
        #   embryo_temporal_alignment is a dictionary whose keys are in ['default', fate]
        #   eg fate = 'Epidermis'; 'default' means there is no fate selection
        #
        # - volume correction
        #   volume linear estimation(timepoint) = embryo_volume_fit[0] * timepoint + embryo_volume_fit[1]
        #   # np.cbrt: cubic root
        #   scaling(timepoint) = np.cbrt( embryo_volume_target / volume linear estimation(timepoint) )
        #   # point coordinates to be mutiplied by scaling(timepoint)
        #   # surfaces have     to be mutiplied by scaling(timepoint) * scaling(timepoint)
        #   # volumes have      to be mutiplied by scaling(timepoint) * scaling(timepoint) * scaling(timepoint)

        self.time_digits_for_cell_id = time_digits_for_cell_id
        self._properties = {'embryo_temporal_alignment': {'default': (1.0, 0.0)}, 'embryo_temporal_reference': None,
                            'embryo_temporal_fate': None,
                            'embryo_volume_fit': (0.0, 1.0), 'embryo_volume_target': 6000000}

        if name is not None:
            self.name = name
        if isinstance(filename, str):
            local_properties = ioproperties.read_dictionary(filename, inputpropertiesdict={},
                                                            verbose=(monitoring.debug > 0))
            if name is None:
                basename = filename.split(os.path.sep)[-1]
                if basename.endswith(".xml") or basename.endswith(".pkl"):
                    self.name = basename[:-4]
                else:
                    self.name = filename
        elif isinstance(properties, dict):
            local_properties = properties
        else:
            if verbose:
                monitoring.to_log_and_console(str(proc) + ": do not know how to fill the class")
            return

        #
        # cell based properties
        #
        if isinstance(local_properties, dict):
            # cell lineage
            if 'cell_lineage' in local_properties:
                self.cell_lineage = local_properties['cell_lineage']
            elif verbose:
                monitoring.to_log_and_console(str(proc) + ": 'cell_lineage' was not in dictionary")
            # cell name
            if 'cell_name' in local_properties:
                self.cell_name = local_properties['cell_name']
            elif verbose:
                monitoring.to_log_and_console(str(proc) + ": 'cell_name' was not in dictionary")
            #
            # volume linear fit has to be at the beginning to get the voxel size correction
            #
            # cell volume
            if 'cell_volume' in local_properties:
                self.cell_volume = local_properties['cell_volume']
                self._volume_fitting()
                self.rectified_cell_volume = local_properties['cell_volume']
            elif verbose:
                monitoring.to_log_and_console(str(proc) + ": 'cell_volume' was not in dictionary")
            # cell contact surfaces
            if 'cell_contact_surface' in local_properties:
                self.cell_contact_surface = local_properties['cell_contact_surface']
                self.rectified_cell_contact_surface = local_properties['cell_contact_surface']
            elif verbose:
                monitoring.to_log_and_console(str(proc) + ": 'cell_contact_surface' was not in dictionary")
            # cell barycenter
            if 'cell_barycenter' in local_properties:
                self.cell_barycenter = local_properties['cell_barycenter']
                self.rectified_cell_barycenter = local_properties['cell_barycenter']
            elif verbose:
                monitoring.to_log_and_console(str(proc) + ": 'cell_barycenter' was not in dictionary")
            # cell fate
            if 'cell_fate' in local_properties:
                self.cell_fate = local_properties['cell_fate']

            #
            # other properties
            #
            if add_all_properties is False:
                return
            registered_properties = self.property_list()
            for k in local_properties:
                if k in registered_properties:
                    continue
                # monitoring.to_log_and_console(str(proc) + ": add property '" + str(k) + "'")
                self.set_property(local_properties[k], k)
            return

    ############################################################
    #
    # Property management
    #
    ############################################################

    def property_list(self):
        return self._properties.keys()

    def get_properties(self):
        return self._properties

    def get_property(self, property_name):
        if property_name not in self._properties:
            return None
        return self._properties[property_name]

    def set_property(self, property, property_name):
        self._properties[property_name] = property

    def _del_one_property(self, property_name):
        if not isinstance(property_name, str):
            return
        if property_name in self._properties:
            del self._properties[property_name]
        return

    def del_property(self, property_name):
        if isinstance(property_name, str):
            self._del_one_property(property_name)
        elif isinstance(property_name, list):
            for n in property_name:
                self._del_one_property(n)
        return

    ############################################################
    #
    # @property
    # From property file
    # - cell_lineage
    # - cell_name
    # - cell_volume
    # - cell_contact_surface
    # - cell_barycenter
    # Computed ones
    # - embryo_temporal_alignment: tuple (a, b). Time (at+b) of the embryo corresponds to
    #   the time t of a reference embryo
    # - embryo_temporal_reference:
    # - embryo_volume_fit: tuple (a, b). Embryo volume along time is fitted by (at+b)
    #   allows to compute a time-varying scaling factor to get a constant embryo volume
    #   along time (see self.get_voxelsize_correction())
    # - embryo_volume_target: this is the targeted contant volume to get a constant embryo volume
    #   along time (see self.get_voxelsize_correction())
    # - timepoints
    #
    ############################################################

    @property
    def name(self):
        """
        The cell lineage, as in the property file
        Returns
        -------

        """
        if 'embryo_name' in self._properties:
            return self._properties['embryo_name']
        return None

    @name.setter
    def name(self, p):
        if p is None:
            return
        if isinstance(p, str) is False:
            return
        local_name = p.split(os.path.sep)[-1]
        if local_name.endswith(".xml") or local_name.endswith(".pkl"):
            local_name = local_name[:-4]
        self._properties['embryo_name'] = local_name
        return

    @property
    def cell_lineage(self):
        """
        The cell lineage, as in the property file
        Returns
        -------

        """
        if 'cell_lineage' in self._properties:
            return self._properties['cell_lineage']
        return None

    @cell_lineage.setter
    def cell_lineage(self, p):
        self._properties['cell_lineage'] = copy.deepcopy(p)
        return

    @property
    def cell_name(self):
        """
        The cell names, as in the property file
        Returns
        -------

        """
        if 'cell_name' in self._properties:
            return self._properties['cell_name']
        return None

    @cell_name.setter
    def cell_name(self, p):
        self._properties['cell_name'] = copy.deepcopy(p)
        return

    @property
    def cell_volume(self):
        """
        The cell volumes, as in the property file
        Returns
        -------

        """
        if 'cell_volume' in self._properties:
            return self._properties['cell_volume']
        return None

    @cell_volume.setter
    def cell_volume(self, p):
        self._properties['cell_volume'] = copy.deepcopy(p)
        return

    @property
    def rectified_cell_volume(self):
        """
        The cell volumes, as in the property file
        Returns
        -------

        """
        if 'rectified_cell_volume' in self._properties:
            return self._properties['rectified_cell_volume']
        return None

    @rectified_cell_volume.setter
    def rectified_cell_volume(self, p):
        cells_per_time = self.get_cells_per_time(propertyname='cell_volume', count=False)
        vols = {}
        for t in cells_per_time:
            voxelsize = self.get_voxelsize_correction(t)
            m = voxelsize * voxelsize * voxelsize
            for c in cells_per_time[t]:
                vols[c] = p[c] * m
        self._properties['rectified_cell_volume'] = vols
        return

    @property
    def cell_contact_surface(self):
        """
        The cell contact surfaces, as in the property file
        Returns
        -------

        """
        if 'cell_contact_surface' in self._properties:
            return self._properties['cell_contact_surface']
        return None

    @cell_contact_surface.setter
    def cell_contact_surface(self, p):
        self._properties['cell_contact_surface'] = copy.deepcopy(p)
        return

    @property
    def rectified_cell_contact_surface(self):
        """
        The cell contact surfaces, as in the property file
        Returns
        -------

        """
        if 'rectified_cell_contact_surface' in self._properties:
            return self._properties['rectified_cell_contact_surface']
        return None

    @rectified_cell_contact_surface.setter
    def rectified_cell_contact_surface(self, p):
        cells_per_time = self.get_cells_per_time(propertyname='cell_contact_surface', count=False)
        surfs = {}
        for t in cells_per_time:
            voxelsize = self.get_voxelsize_correction(t)
            m = voxelsize * voxelsize
            for c in cells_per_time[t]:
                surfs[c] = {}
                for d in p[c]:
                    surfs[c][d] = p[c][d] * m
        self._properties['rectified_cell_contact_surface'] = surfs
        return

    @property
    def cell_barycenter(self):
        """
        The cell contact surfaces, as in the property file
        Returns
        -------

        """
        if 'cell_barycenter' in self._properties:
            return self._properties['cell_barycenter']
        return None

    @cell_barycenter.setter
    def cell_barycenter(self, p):
        self._properties['cell_barycenter'] = copy.deepcopy(p)
        return

    @property
    def rectified_cell_barycenter(self):
        if 'rectified_cell_barycenter' in self._properties:
            return self._properties['rectified_cell_barycenter']
        return None

    @rectified_cell_barycenter.setter
    def rectified_cell_barycenter(self, p):
        cells_per_time = self.get_cells_per_time(propertyname='cell_barycenter', count=False)
        bars = {}
        for t in cells_per_time:
            m = self.get_voxelsize_correction(t)
            for c in cells_per_time[t]:
                bars[c] = m * p[c]
        self._properties['rectified_cell_barycenter'] = bars
        return

    @property
    def cell_fate(self):
        if 'cell_fate' in self._properties:
            return self._properties['cell_fate']
        if 'cell_name' in self._properties:
            self._properties = afate.set_fate_from_names(self._properties)
            if 'cell_fate' in self._properties:
                return self._properties['cell_fate']
        return None

    @cell_fate.setter
    def cell_fate(self, p):
        self._properties['cell_fate'] = copy.deepcopy(p)
        return

    #
    # other properties
    #
    @property
    def temporal_alignment(self):
        if 'embryo_temporal_alignment' in self._properties:
            if self._properties['embryo_temporal_fate'] is not None:
                if self._properties['embryo_temporal_fate'] in self._properties['embryo_temporal_alignment']:
                    return self._properties['embryo_temporal_alignment'][self._properties['embryo_temporal_fate']]
        return None

    def set_temporal_alignment(self, v, key='default'):
        if 'embryo_temporal_alignment' not in self._properties:
            self._properties['embryo_temporal_alignment'] = {}
        self._properties['embryo_temporal_alignment'][key] = v
        if self._properties['embryo_temporal_fate'] is None:
            self._properties['embryo_temporal_fate'] = key

    def get_temporal_alignment(self, key='default'):
        if 'embryo_temporal_alignment' not in self._properties:
            return None
        if key is None and 'default' in self._properties['embryo_temporal_alignment']:
            return self._properties['embryo_temporal_alignment']['default']
        if key not in self._properties['embryo_temporal_alignment']:
            return None
        return self._properties['embryo_temporal_alignment'][key]

    def set_all_temporal_alignment(self, d):
        if isinstance(d, dict) and len(d) > 0:
            for k in d:
                self.set_temporal_alignment(d[k], key=k)
        return

    def get_all_temporal_alignment(self):
        return self._properties['embryo_temporal_alignment']

    @property
    def embryo_volume_fit(self):
        if 'embryo_volume_fit' in self._properties:
            return self._properties['embryo_volume_fit']
        return None

    @embryo_volume_fit.setter
    def embryo_volume_fit(self, coefficients):
        self._properties['embryo_volume_fit'] = coefficients
        return

    @property
    def target_volume(self):
        if 'target_volume' in self._properties:
            return self._properties['target_volume']
        return None

    @target_volume.setter
    def target_volume(self, volume):
        self._properties['target_volume'] = volume
        return

    @property
    def direction_distribution_maxima(self):
        if 'direction_distribution_maxima' not in self._properties:
            self._properties['direction_distribution_maxima'] = {}
        return self._properties['direction_distribution_maxima']

    ############################################################
    #
    # Properties (computed)
    #
    ############################################################

    def compute_cell_contact_vector(self, times=None, normalize=True, rectified=True, force=False, verbose=False):
        proc = "Embryo.compute_cell_contact_vector"
        #
        # unit contact vector between adjacent cells
        # - adjacent cells are identified from the contact surfaces (background is excluded)
        # - contact vector is defined as the vector joining the cell barycenters
        #   pointing outside of the cell
        #   vector[c][d] = bary[d] - bary[c] for d in contact[c]
        # it is computed for all time points (thus the parameter times is not useful)
        #

        #
        # is there some computation to be done?
        #
        if 'cell_contact_vector' in self._properties:
            cells_per_time = self.get_cells_per_time(propertyname='cell_contact_vector')
            if times is not None:
                if isinstance(times, int):
                    if times in cells_per_time:
                        if verbose:
                            msg = " time '" + str(times) + "' already in keys()"
                            monitoring.to_log_and_console(str(proc) + ": " + msg)
                        return
                elif isinstance(times, list):
                    inlist = [t for t in times if t in cells_per_time]
                    outlist = set(cells_per_time).difference(inlist)
                    if len(outlist) == 0:
                        if verbose:
                            msg = " times '" + str(inlist) + "' already in keys()"
                            monitoring.to_log_and_console(str(proc) + ": " + msg)
                        return
                else:
                    msg = "unexpected type for times: '" + str(type(times))
                    monitoring.to_log_and_console(str(proc) + ": " + msg)
                    return
            else:
                if len(cells_per_time) > 0:
                    msg = "no time point is given, but 'cell_contact_vector' are already computed"
                    monitoring.to_log_and_console(str(proc) + ": " + msg)
                    return

        #
        # check whether we have everything we need
        #
        contact_surface = self.cell_contact_surface
        if contact_surface is None:
            monitoring.to_log_and_console(str(proc) + ": no contact surfaces")
            return
        if rectified is True:
            barycenter = self.rectified_cell_barycenter
        else:
            barycenter = self.cell_barycenter
        if barycenter is None:
            monitoring.to_log_and_console(str(proc) + ": no barycenters")
            return

        #
        # compute contact vectors
        #
        contact_vector = {}
        div = 10 ** self.time_digits_for_cell_id

        cells_per_time = self.get_cells_per_time(propertyname='cell_contact_surface', count=False)
        for t in cells_per_time:
            #
            if times is not None:
                if isinstance(times, int) and t != times:
                    continue
                if isinstance(times, list) and t not in times:
                    continue
            #
            if verbose:
                msg = "compute contact vector for time '" + str(t) + "'"
                monitoring.to_log_and_console(str(proc) + ": " + msg)
            #
            for c in cells_per_time[t]:
                contact_vector[c] = {}
                for d in contact_surface[c]:
                    if int(d) % div == 1:
                        contact_vector[c][d] = np.zeros(3)
                    else:
                        v = barycenter[d] - barycenter[c]
                        # v /= np.linalg.norm(v)
                        contact_vector[c][d] = v

        if normalize is True:
            for c in contact_vector:
                for d in contact_vector[c]:
                    n = np.linalg.norm(contact_vector[c][d])
                    if n > 1.0:
                        contact_vector[c][d] /= n

        self._properties['cell_contact_vector'] = contact_vector
        return

    @property
    def cell_contact_vector(self):

        if 'cell_contact_vector' in self._properties:
            return self._properties['cell_contact_vector']

        self.compute_cell_contact_vector()

        if 'cell_contact_vector' in self._properties:
            return self._properties['cell_contact_vector']

        return None

    ############################################################
    #
    # Properties (end)
    #
    ############################################################

    def _volume_fitting(self):
        div = 10 ** self.time_digits_for_cell_id
        volume = self.cell_volume
        volume_along_time = {}
        #
        # compute embryo volume for each timepoint 't'
        #
        for c in volume:
            t = int(c) // div
            volume_along_time[t] = volume_along_time.get(t, 0) + volume[c]

        #
        # get array of time point (x) and array of volumes (y)
        #
        x = list(volume_along_time.keys())
        x = sorted(x)
        y = [volume_along_time[i] for i in x]

        #
        # robust regression via ransac
        #
        xnp = np.array(x)[:, np.newaxis]
        ynp = np.array(y)[:, np.newaxis]
        ransac = sklm.RANSACRegressor()
        ransac.fit(xnp, ynp)
        self._properties['embryo_volume_fit'] = (ransac.estimator_.coef_[0][0], ransac.estimator_.intercept_[0])
        v_coefficients = self.embryo_volume_fit
        formula = "{:.3f} * acquisition time + {:.3f}".format(v_coefficients[0], v_coefficients[1])
        msg = "   ... volume fitting = " + formula + "\n"
        msg += "       scaling correction = cubic root( target volume / (" + formula + "))"
        monitoring.to_log_and_console(msg)

    def get_voxelsize_correction(self, timepoint, target_volume=60000000):
        """
        Give the scale to be applied to simulate an embryo of volume 'target_volume'
        - volumes have to be multiplied by scale power 3
        - surfaces have to be multiplied by scale power 2
        Parameters
        ----------
        timepoint
        target_volume

        Returns
        -------

        """
        if 'embryo_volume_fit' not in self._properties:
            self._volume_fitting()
        if target_volume != self.target_volume:
            self.target_volume = target_volume
        v_coefficients = self.embryo_volume_fit
        t_volume = v_coefficients[0] * timepoint + v_coefficients[1]
        return np.cbrt(self.target_volume / t_volume)

    #
    # temporal alignment of an atlas/embryo with the reference atlas/embryo
    # it consists at finding the time lineage warping based on the cell number
    # so that the cell number at (a * t + b) of the atlas is equal at the one
    # of the reference number at (t)
    #
    def temporally_align_with(self, reference, mincells=None, maxcells=None, fate=None):
        """

        :param reference:
            reference atlas to be temporally aligned with.
        :param mincells:
        :param maxcells:
        :param fate:
        :return:
            Tuple (a, b). Time point t of the atlas at hand is equivalent to the time
            point (at+b) of the reference Embryo.
        """
        proc = "Embryo.temporally_align_with"
        if not isinstance(reference, Embryo):
            monitoring.to_log_and_console(str(proc) + ": 'reference' should be of 'Atlas' class")
            return

        if self.cell_lineage is None:
            monitoring.to_log_and_console(str(proc) + ": no lineage?! Exiting.")
            sys.exit(1)

        if fate is None:
            a, b = talign.temporal_alignment(reference.cell_lineage, self.cell_lineage, mincells=mincells,
                                             maxcells=maxcells,
                                             ref_time_digits_for_cell_id=reference.time_digits_for_cell_id,
                                             time_digits_for_cell_id=self.time_digits_for_cell_id)
            self.set_temporal_alignment((a, b))
            return

        #
        #
        #
        ref_cellcount = reference.get_cells_per_time(fate=fate)
        cellcount = self.get_cells_per_time(fate=fate)
        a, b = talign.cellcounts_temporal_alignment(ref_cellcount, cellcount, mincells=mincells, maxcells=maxcells)
        self.set_temporal_alignment((a, b), key=fate)
        return

    ############################################################
    #
    #
    #
    ############################################################

    def get_times(self, n_cells=64, mode='floor'):
        """

        :param n_cells:
        :param mode:
        :return:  time array and a number of cells
          The number of cells is the targeted one, if it exists in the embryo development
          else it is the closest one (with the interpolation mode)
          The time array contains the time points with this cell number
        """
        #
        # get the time range with the specified number of cells
        # if it does not exist, get the time range with the immediate upper ('ceil') or lower ('floor') number of cells
        #
        proc = "Embryo.get_times"
        #
        # count the cells per time
        #
        prop = self.cell_volume
        if prop is None:
            prop = self.cell_contact_surface
        if prop is None:
            msg = "   " + str(proc) + ": " + " no properties?!"
            monitoring.to_log_and_console(msg)
            return None, None

        cells = list(set(prop.keys()))
        cells = sorted(cells)
        cells_per_time = {}
        div = 10 ** self.time_digits_for_cell_id
        for c in cells:
            if int(c) % div == 1 or int(c) % div == 0:
                continue
            t = int(c) // div
            cells_per_time[t] = cells_per_time.get(t, 0) + 1

        #
        # sort the cells_per_time dictionary by time
        # assume that cell number will also be in increasing order
        #
        cells_per_time = dict(sorted(cells_per_time.items(), key=lambda item: item[0]))

        #
        # if there are times with the specified number of cells, return them
        #
        times = [t for t in cells_per_time if cells_per_time[t] == n_cells]
        if len(times) > 0:
            return times, n_cells

        #
        # if not, find times with immediate lower and upper number of cells
        #

        mincells = [cells_per_time[t] for t in cells_per_time if cells_per_time[t] < n_cells]
        maxcells = [cells_per_time[t] for t in cells_per_time if cells_per_time[t] > n_cells]

        if mode == 'floor':
            if len(mincells) > 0:
                n_cells = max(mincells)
                times = [t for t in cells_per_time if cells_per_time[t] == n_cells]
                return times, n_cells
            if len(maxcells) > 0:
                n_cells = min(maxcells)
                times = [t for t in cells_per_time if cells_per_time[t] == n_cells]
                return times, n_cells
        elif mode == 'ceil':
            if len(maxcells) > 0:
                n_cells = min(maxcells)
                times = [t for t in cells_per_time if cells_per_time[t] == n_cells]
                return times, n_cells
            if len(mincells) > 0:
                n_cells = max(mincells)
                times = [t for t in cells_per_time if cells_per_time[t] == n_cells]
                return times, n_cells
        msg = "   " + str(proc) + ": " + " unknown estimation mode '" + str(mode) + "'"
        monitoring.to_log_and_console(msg)
        return None, None

    def get_timerange(self, cell_number, change_cell_number=False):
        """

        Parameters
        ----------
        cell_number: requested cell number
        change_cell_number: Boolean, if False, the requested cell number has
            either to be the requested cell number (if it exists)
            or to be in the interval [lowerncells, upperncells], meaning that there
            are stages will less and more cells

        Returns
        -------
            retained_cell_count: the count of cells in the returned interval (or None)
            timerange: the time interval where this count of cells is reached
            ininterval: True if retained_cell_count is in  [lowerncells, upperncells]

        """

        #
        # get the time range of the floating embryo with the specified number of cells
        #
        lowertimerange, lowerncells = self.get_times(cell_number, mode='floor')
        uppertimerange, upperncells = self.get_times(cell_number, mode='ceil')

        if lowerncells <= cell_number <= upperncells:
            if cell_number - lowerncells <= upperncells - cell_number:
                retained_cell_number = lowerncells
                timerange = lowertimerange
            else:
                retained_cell_number = upperncells
                timerange = uppertimerange
            return retained_cell_number, timerange, True

        if change_cell_number is False:
            return None, None, False

        if lowerncells == upperncells:
            retained_cell_number = lowerncells
            timerange = lowertimerange
            return retained_cell_number, timerange, False

        return None, None, False

    ############################################################
    #
    ############################################################

    def get_cells_per_time(self, propertyname=None, fate=None, nonfate=None, count=True):
        proc = "Embryo.get_cells_per_time"
        div = 10 ** self.time_digits_for_cell_id
        cells_per_time = {}

        if fate is None and nonfate is None:
            if propertyname == 'cell_name':
                cells = list(set(self.cell_name.keys()))
            elif propertyname == 'cell_volume':
                cells = list(set(self.cell_volume.keys()))
            elif propertyname == 'cell_contact_surface':
                cells = list(set(self.cell_contact_surface.keys()))
            elif propertyname == 'cell_contact_vector':
                cells = list(set(self.cell_contact_vector.keys()))
            elif propertyname == 'cell_barycenter':
                cells = list(set(self.cell_barycenter.keys()))
            else:
                lineage = self.cell_lineage
                cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
            cells = sorted(cells)
            if count is True:
                for c in cells:
                    t = int(c) // div
                    cells_per_time[t] = cells_per_time.get(t, 0) + 1
            else:
                for c in cells:
                    t = int(c) // div
                    cells_per_time[t] = cells_per_time.get(t, []) + [c]
            return cells_per_time

        cell_fate = self.cell_fate
        if cell_fate is None:
            msg = "no cell fate for embryo, unable to compute cells per time"
            monitoring.to_log_and_console(proc + ": " + msg)
            return None

        if count is True:
            for c in cell_fate:
                if afate.has_fate(cell_fate[c], fate, nonfate):
                    t = int(c) // div
                    cells_per_time[t] = cells_per_time.get(t, 0) + 1
        else:
            for c in cell_fate:
                if afate.has_fate(cell_fate[c], fate, nonfate):
                    t = int(c) // div
                    cells_per_time[t] = cells_per_time.get(t, []) + [c]
        return cells_per_time

    ############################################################
    #
    #
    #
    ############################################################

    #
    #
    #
    def get_cells(self, timepoint, propertyname=None, sortcriterium='cell_id', background=False):
        if propertyname == 'cell_name':
            cells = list(set(self.cell_name.keys()))
        elif propertyname == 'cell_volume':
            cells = list(set(self.cell_volume.keys()))
        elif propertyname == 'cell_contact_surface':
            if background is False:
                cells = list(set(self.cell_contact_surface.keys()))
            else:
                cells = list(set([n for c in self.cell_contact_surface for n in self.cell_contact_surface[c]]))
        elif propertyname == 'cell_contact_vector':
            cells = list(set(self.cell_contact_surface.keys()))
        elif propertyname == 'cell_barycenter':
            cells = list(set(self.cell_barycenter.keys()))
        elif propertyname in self.property_list():
            cells = list(set((self.get_property(propertyname)).keys()))
        else:
            lineage = self.cell_lineage
            cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
        div = 10 ** self.time_digits_for_cell_id

        if background is False:
            cell_list = [c for c in cells if (int(c) // div == timepoint) and int(c) % div != 1]
        else:
            cell_list = [c for c in cells if (int(c) // div == timepoint)]

        if sortcriterium == 'cell_name':
            if 'cell_name' in self.property_list():
                return _sort_by_name(cell_list, self.get_property('cell_name'))
            elif 'ground_truth_cell_name' in self.property_list():
                return _sort_by_name(cell_list, self.get_property('ground_truth_cell_name'))
        # if no names, sort on cell ids
        if sortcriterium == 'cell_name' or sortcriterium == 'cell-id':
            return sorted(cell_list)

        return cell_list

    #
    #
    #
    def get_embryo_volume(self, timepoint):
        s = 0.0
        volumes = self.cell_volume
        div = 10 ** self.time_digits_for_cell_id
        for c in volumes:
            if int(c) // div != timepoint:
                continue
            if int(c) % div == 1 or int(c) % div == 0:
                continue
            s += volumes[c]
        return s

    #
    #
    #

    def get_embryo_barycenter(self, timepoint, rectified=False):
        proc = "Embryo.get_embryo_barycenter"
        if rectified is True:
            key = 'rectified_embryo_barycenter'
        else:
            key = 'embryo_barycenter'
        if key in self._properties:
            if timepoint in self._properties[key]:
                return self._properties[key][timepoint]
        else:
            self._properties[key] = {}

        if rectified is True:
            volumes = self.rectified_cell_volume
            barycenters = self.rectified_cell_barycenter
        else:
            volumes = self.cell_volume
            barycenters = self.cell_barycenter
        div = 10 ** self.time_digits_for_cell_id

        if volumes is None:
            msg = "no cell volumes for embryo, unable to compute embryo center of mass"
            monitoring.to_log_and_console(proc + ": " + msg)
            return None
        if barycenters is None:
            msg = "no cell centers of mass for embryo, unable to compute embryo center of mass"
            monitoring.to_log_and_console(proc + ": " + msg)
            return None

        b = np.zeros(3)
        s = 0.0
        for c in volumes:
            if int(c) // div != timepoint:
                continue
            if int(c) % div == 1 or int(c) % div == 0:
                continue
            if c not in barycenters:
                continue
            b += volumes[c] * barycenters[c]
            s += volumes[c]
        self._properties[key][timepoint] = b / s
        return self._properties[key][timepoint]

    #
    #
    #
    def get_embryo_covariance(self, timepoint):
        if 'embryo_covariance' in self._properties:
            if timepoint in self._properties['embryo_covariance']:
                return self._properties['embryo_covariance'][timepoint]
        else:
            self._properties['embryo_covariance'] = {}

        volumes = self.cell_volume
        barycenters = self.cell_barycenter
        div = 10 ** self.time_digits_for_cell_id

        b = np.zeros(3)
        m = np.zeros((3, 3))
        s = 0.0
        for c in volumes:
            if int(c) // div != timepoint:
                continue
            if int(c) % div == 1 or int(c) % div == 0:
                continue
            if c not in barycenters:
                continue
            s += volumes[c]
            b += volumes[c] * barycenters[c]
            t = barycenters[c].reshape((1, 3))
            m += volumes[c] * t * t.T
        # end of computation
        b /= s
        m /= s
        t = b.reshape((1, 3))
        m -= t * t.T

        #
        # val, vec = np.linalg.eig(m)
        # norma_mat = np.matmul(vec, np.matmul(np.linalg.inv(np.diag(np.sqrt(val))), vec.T))
        #
        # (re)computing the covariance matrix with the np.matmul(norma_mat, barycenters[c])
        # yields a covariance matrix with all variances equal to 1
        # (somehow makes the embryo spherical)

        self._properties['embryo_covariance'][timepoint] = m
        return self._properties['embryo_covariance'][timepoint]

    #
    #
    #
    def get_symmetry_axis_from_names(self, timepoint):
        """

        Parameters
        ----------
        timepoint

        Returns
        -------
        Left-to-right unit vector

        """
        key = 'symmetry_axis_from_names'

        if key in self._properties:
            if timepoint in self._properties[key]:
                return self._properties[key][timepoint]
        else:
            self._properties[key] = {}

        volumes = self.cell_volume
        barycenters = self.cell_barycenter
        names = self.cell_name

        if names is None:
            return np.zeros(3)
        #
        leftb = np.zeros(3)
        lefts = 0.0
        rightb = np.zeros(3)
        rights = 0.0
        div = 10 ** self.time_digits_for_cell_id
        #
        for c in volumes:
            if int(c) // div != timepoint:
                continue
            if int(c) % div == 1 or int(c) % div == 0:
                continue
            if c not in barycenters or c not in names:
                continue
            if names[c].split('.')[1][4] == '_':
                leftb += volumes[c] * barycenters[c]
                lefts += volumes[c]
            elif names[c].split('.')[1][4] == '*':
                rightb += volumes[c] * barycenters[c]
                rights += volumes[c]
        if lefts == 0.0 or rights == 0.0:
            return np.zeros(3)
        symdir = rightb/rights - leftb/lefts
        self._properties[key][timepoint] = symdir / np.linalg.norm(symdir)
        return self._properties[key][timepoint]

    #
    #
    #
    def get_direction_distribution_candidates(self, timepoint, parameters=None, verbose=True):
        proc = "get_direction_distribution_candidates"
        if parameters is not None:
            if not isinstance(parameters, sparameters.SymmetryParameters):
                msg = "parameters expected type is 'SymmetryParameters' but was '"
                msg += type(parameters) + "' instead"
                monitoring.to_log_and_console(proc + ": " + msg)
                return None
            local_parameters = parameters
        else:
            local_parameters = sparameters.SymmetryParameters()

        #
        # maxima is an array of dictionaries
        # 'voxel': (i, j, k) voxel coordinates in a sxsxs matrix with s computed by
        #           ir = math.ceil(parameters.distribution_sphere_radius)
        #           s = int(2 * ir + 1 + 2)
        # 'vector': a normalized vector v / np.linalg.norm(v)  with v = 'voxel' - center
        #           center coordinates are (c,c,c) with c = np.double(ir + 1)
        # 'value': the distribution values
        #          the maximal value is 1
        #

        if 'direction_distribution_maxima' not in self._properties:
            self._properties['direction_distribution_maxima'] = {}
        if timepoint not in self.direction_distribution_maxima:
            sembryo.monitoring.copy(monitoring)
            #
            # v: numpy array of shape (N, 3), N being the number of candidates
            # w: numpy array of shape (N), associated distribution value
            #
            v, w = sembryo.leftright_symmetry_candidates(self.cell_contact_surface, self.cell_barycenter, timepoint,
                                                         local_parameters,
                                                         time_digits_for_cell_id=self.time_digits_for_cell_id,
                                                         verbose=verbose)
            #
            # keep only the vectors (already sorted)
            #
            self.direction_distribution_maxima[timepoint] = v

        return self.direction_distribution_maxima[timepoint]

    ############################################################
    #
    #
    #
    ############################################################

    def symmetrical_embryo(self, name=None):
        proc = "symmetrical_embryo"
        #
        # build properties
        #
        prop = {}
        if self.cell_lineage is not None:
            prop['cell_lineage'] = copy.deepcopy(self.cell_lineage)
        if self.cell_name is not None:
            prop['cell_volume'] = copy.deepcopy(self.cell_volume)
        if self.cell_name is not None:
            prop['cell_name'] = {}
            for c in self.cell_name:
                name = self.cell_name[c]
                symname = name[:-1]
                if name[-1] == '*':
                    symname += '_'
                elif name[-1] == '_':
                    symname += '*'
                prop['cell_name'][c] = symname
        if self.cell_contact_surface is not None:
            prop['cell_contact_surface'] = copy.deepcopy(self.cell_contact_surface)
        if self.cell_fate is not None:
            prop['cell_fate'] = copy.deepcopy(self.cell_fate)

        if self.cell_barycenter is not None:
            prop['cell_barycenter'] = {}
            cells_per_time = self.get_cells_per_time('cell_barycenter', count=False)
            for t in cells_per_time:
                barycenter = self.get_embryo_barycenter(t)
                if self.cell_name is not None:
                    sym_axis = self.get_symmetry_axis_from_names(t)
                else:
                    vectors = self.get_direction_distribution_candidates(t)
                    sym_axis = vectors[0]
                for c in cells_per_time[t]:
                    m = self.cell_barycenter[c]
                    mg = m - barycenter
                    prop['cell_barycenter'][c] = m - 2 * np.dot(mg, sym_axis) * sym_axis

        diff_properties = set(self.property_list()) - set(prop.keys())
        if len(diff_properties) > 0:
            msg = ""
            if name is not None:
                msg += "embryo '" + str(self.name) + "': "
            msg += " properties not symmetrized: " + str(diff_properties)
            monitoring.to_log_and_console(proc + ": " + msg)
        #
        # atlas creation from properties
        #
        symatlas = Embryo(prop, time_digits_for_cell_id=self.time_digits_for_cell_id)

        #
        # add temporal registration(s)
        # skip volume estimation
        #     (it has been recomputed at the atlas creation step)
        #
        symatlas.set_all_temporal_alignment(self.get_all_temporal_alignment())
        # symatlas.volume_local_estimation = self.volume_local_estimation

        return symatlas

    ############################################################
    #
    #
    #
    ############################################################

    def write(self, filename, verbose=True):
        proc = "Embryo.write"
        if filename.endswith("pkl") is True or filename.endswith("xml") is True:
            ioproperties.write_dictionary(filename, self._properties, verbose=verbose)
            return
        else:
            msg = "such file extension '" + str(os.path.splitext(filename)[-1]) + "'is not handled"
            monitoring.to_log_and_console(proc + ": " + msg)
        return

    def write_as_graph(self, time=None, atlasname=None):
        #
        # write embryo as a graph at time time
        #
        div = 10 ** self.time_digits_for_cell_id
        contact = copy.deepcopy(self.cell_contact_surface)
        cells = [c for c in contact if c // div == time]

        #
        # add background to the (copied) contact
        #
        ibackground = time * div + 1
        contact = copy.deepcopy(self.cell_contact_surface)
        contact[ibackground] = {}
        for c in cells:
            if ibackground not in contact[c]:
                continue
            contact[ibackground][c] = contact[c][ibackground]

        cells = sorted(cells + [ibackground])
        icells = {}
        for i, c in enumerate(cells):
            icells[c] = i + 1

        voxelsize = self.get_voxelsize_correction(time)
        correction = voxelsize * voxelsize
        filename = "graph-" + str(atlasname) + ".txt"
        with open(filename, 'w') as file:
            for c in icells:
                for d in contact[c]:
                    line = "{:2d} {:2d} {:9.3f}".format(icells[c], icells[d], contact[c][d] * correction)
                    file.write(line + '\n')

        filename = "groundtruth-" + str(atlasname) + ".txt"
        with open(filename, 'w') as file:
            for c in icells:
                if c == ibackground:
                    line = "{:2d} background".format(icells[ibackground])
                else:
                    line = "{:2d} {:s}".format(icells[c], self.cell_name[c])
                file.write(line + '\n')
        return


##############################################################
#
#
#
##############################################################


def _cmp_names(n1, n2):
    # right cells, then left cells
    # cells are then sorted
    #   - 'a' first then 'b'
    #   - id ie 'q' in [a,b]p.q
    # left/right
    #  '*' < '_' is True
    lr1 = n1.split('.')[1][4]
    lr2 = n2.split('.')[1][4]
    if lr1 < lr2:
        return -1
    if lr1 > lr2:
        return 1
    # compare a and b
    ab1 = n1.split('.')[0][0]
    ab2 = n2.split('.')[0][0]
    if ab1 < ab2:
        return -1
    if ab1 > ab2:
        return 1
    # generation
    g1 = int(n1.split('.')[0][1:])
    g2 = int(n2.split('.')[0][1:])
    if g1 < g2:
        anc2 = aname.get_ancestor_name(n2, g1)
        p1 = n1.split('.')[1][0:4]
        p2 = anc2.split('.')[1][0:4]
        if p1 < p2:
            return -1
        if p1 > p2:
            return 1
        # if the ancestor is compared to descendant, prefer the ancestor
        return -1
    elif g1 > g2:
        anc1 = aname.get_ancestor_name(n1, g2)
        p1 = anc1.split('.')[1][0:4]
        p2 = n2.split('.')[1][0:4]
        if p1 < p2:
            return -1
        if p1 > p2:
            return 1
        return 1
    else:
        p1 = n1.split('.')[1][0:4]
        p2 = n2.split('.')[1][0:4]
        if p1 < p2:
            return -1
        if p1 > p2:
            return 1
    return 0


def _sort_by_name(cells, names):
    namedcells = [c for c in cells if c in names]
    if len(namedcells) < len(cells):
        unnamedcells = sorted(list(set(cells).difference(namedcells)))
    localnames = [names[c] for c in namedcells]
    namecell = {names[c]: c for c in namedcells}
    sortednames = aname.sort_by_name(localnames)
    sortedcells = [namecell[n] for n in sortednames]
    if len(namedcells) < len(cells):
        return unnamedcells + sortedcells
    return sortedcells

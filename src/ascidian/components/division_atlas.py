##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2023
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Ven  5 jul 2024 14:49:37 CEST
#
##############################################################
#
#
#
##############################################################

import copy
import sys
import operator

import numpy as np
import scipy as sp
import scipy.cluster.hierarchy as sch

import astec.utils.common as common

import ascidian.ascidian.name as aname
import ascidian.components.embryo as cembryo
import ascidian.components.embryoset as cembryoset
import ascidian.core.contact_surface_distance as ccsurfdist
import ascidian.naming_propagation.parameters as npparameters

monitoring = common.Monitoring()

##############################################################
#
#
#
##############################################################


class DivisionAtlases(object):
    def __init__(self, embryoset=None, parameters=None):
        proc = "DivisionAtlases.__init__"
        if not isinstance(parameters, npparameters.NamingPropagationParameters):
            monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                          + str(type(parameters)))
            sys.exit(1)

        self._default_delay = None
        if parameters.delay_from_division is not None:
            self._default_delay = parameters.delay_from_division
        #
        # data issued from property files
        #

        # nested dictionary of neighborhoods, where the keys are
        # [delay_from_division]['cell name(s)']['reference name']
        # - delay_from_division: allows to have different neighborhoods for the same reference
        # - 'cell name(s)': a tuple of the cell names (Conklin) of the interior of the structure
        #    one name correspond to a single cell
        #    names of two sister cells correspond to a division
        # - 'reference name' is the file name,
        # a neighborhood is a dictionary of contact surfaces indexed by cell names
        self._cell_neighborhood = {}
        #
        # if same neighborhoods have been built, there is no more need to homogenize
        # contact surfaces
        #
        self._use_common_neighborhood = False

        # nested dictionary of neighborhoods, where the keys are
        # [delay_from_division]['cell name(s)']['reference name']
        # give list of the cell name(s) volumes
        self._volume = {}

        self._cellid = {}

        # dictionary indexed by 'cell name' where 'cell name' is a mother cell giving the list
        # of references/atlases available for the two daughters
        self._divisions = {}

        # dictionary indexed by [delay_from_division]['cell name']
        # values are the pair of daughter cells (the largest first)
        # this is the set of divisions where the same daughter is always larger than the other
        # (and there are at least 5 atlases)
        self._unequal_divisions = {}

        # dictionary index by atlas name
        # to keep trace of some output (kept as morphonet selection)
        self._output_selections = {}

        if embryoset is not None:
            _update_division_atlas_from_embryo_set(self, embryoset, delay_from_division=self.default_delay)
            self.build_division_atlases(embryoset=embryoset, parameters=parameters)

    ############################################################
    #
    # getters / setters
    #
    ############################################################

    @property
    def default_delay(self):
        return self._default_delay

    @default_delay.setter
    def default_delay(self, d):
        self._default_delay = d
        return

    def get_cell_neighborhood(self, delay_from_division=None):
        if delay_from_division is None:
            delay = self.default_delay
        else:
            delay = delay_from_division
        if delay not in self._cell_neighborhood:
            self._cell_neighborhood[delay] = {}
        return self._cell_neighborhood[delay]

    def set_cell_neighborhood(self, neighborhood, delay_from_division=None):
        if delay_from_division is None:
            delay = self.default_delay
        else:
            delay = delay_from_division
            self.default_delay = delay_from_division
        self._cell_neighborhood[delay] = neighborhood

    @property
    def use_common_neighborhood(self):
        return self._use_common_neighborhood

    def get_volume(self, delay_from_division=None):
        if delay_from_division is None:
            delay = self.default_delay
        else:
            delay = delay_from_division
        if delay not in self._volume:
            self._volume[delay] = {}
        return self._volume[delay]

    def set_volume(self, volume, delay_from_division=None):
        if delay_from_division is None:
            delay = self.default_delay
        else:
            delay = delay_from_division
            self.default_delay = delay_from_division
        self._volume[delay] = volume

    def get_cellid(self, delay_from_division=None):
        if delay_from_division is None:
            delay = self.default_delay
        else:
            delay = delay_from_division
        if delay not in self._cellid:
            self._cellid[delay] = {}
        return self._cellid[delay]

    def set_cellid(self, cellid, delay_from_division=None):
        if delay_from_division is None:
            delay = self.default_delay
        else:
            delay = delay_from_division
            self.default_delay = delay_from_division
        self._cellid[delay] = cellid

    def get_division(self):
        return self._divisions

    def get_unequal_division(self):
        return self._unequal_divisions

    ##########################################################
    #
    #
    #
    ##########################################################

    def _build_divisions(self, delay_from_division=None):
        """
        Build a dictionary index by mother cell name. Each entry contains reference names
        for which the both daughters exist
        Returns
        -------

        """
        proc = "_build_divisions"

        if self._divisions is not None:
            del self._divisions
            self._divisions = {}
        else:
            self._divisions = {}

        #
        # get all references per division/mother cells
        #
        neighborhoods = self.get_cell_neighborhood(delay_from_division=delay_from_division)
        cell_names = sorted(list(neighborhoods.keys()))
        references = {}
        for cell_name in cell_names:
            mother_name = aname.get_mother_name(cell_name)
            references[mother_name] = references.get(mother_name, set()).union(set(neighborhoods[cell_name].keys()))

        #
        # remove references that does not exist for one daughter
        #
        mother_names = sorted(references.keys())
        for n in mother_names:
            daughters = aname.get_daughter_names(n)
            #
            # check whether each reference has the two daughters
            #
            refs = list(references[n])
            for r in refs:
                if daughters[0] in neighborhoods and r in neighborhoods[daughters[0]] and \
                        daughters[1] in neighborhoods and r in neighborhoods[daughters[1]]:
                    self._divisions[n] = self._divisions.get(n, []) + [r]
                else:
                    msg = "    " + str(proc) + ": remove atlas '" + str(r) + "' for division '" + str(n) + "'"
                    monitoring.to_log_and_console(msg)

    #
    #
    #

    def _build_unequal_divisions(self, delay_from_division=None, minimal_references=5):
        """
        Build a dictionary index by mother cell name. Each entry contains reference names
        for which the both daughters exist
        Returns
        -------

        """

        local_minimal_references = minimal_references

        delay = delay_from_division
        if delay is None:
            delay = self.default_delay

        if self._unequal_divisions is None:
            self._unequal_divisions = {}
        if delay in self._unequal_divisions:
            del self._unequal_divisions[delay]
        self._unequal_divisions[delay] = {}

        divisions = self.get_division()

        volumes = self.get_volume(delay_from_division=delay)

        for mother in divisions:
            d = aname.get_daughter_names(mother)
            vol01 = 0
            vol10 = 0
            for r in divisions[mother]:
                if volumes[d[0]][r] > volumes[d[1]][r]:
                    vol01 += 1
                elif volumes[d[0]][r] < volumes[d[1]][r]:
                    vol10 += 1
            if vol01 > local_minimal_references and vol10 == 0:
                self._unequal_divisions[delay][mother] = [d[0], d[1]]
            elif vol10 > local_minimal_references and vol01 == 0:
                self._unequal_divisions[delay][mother] = [d[1], d[0]]

        msg = "found " + str(len(self._unequal_divisions[delay])) + " unequal divisions "
        msg += "at delay = " + str(delay) + " "
        msg += "(more than " + str(local_minimal_references) + " atlases) in " + str(len(divisions)) + " divisions"
        monitoring.to_log_and_console("\t" + msg)
        return

    ############################################################
    #
    #
    #
    ############################################################

    def build_division_atlases(self, embryoset, parameters):
        proc = "DivisionAtlases.build_division_atlases()"

        if isinstance(embryoset, cembryoset.EmbryoSet) is False:
            msg = ": unexpected type for 'embryos' variable: " + str(type(embryoset))
            monitoring.to_log_and_console(str(proc) + msg)
            sys.exit(1)

        if not isinstance(parameters, npparameters.NamingPropagationParameters):
            monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                          + str(type(parameters)))
            sys.exit(1)

        delay = None
        if parameters.delay_from_division is None:
            delay = self.default_delay
        elif self.default_delay is None:
            self.default_delay = parameters.delay_from_division
            delay = parameters.delay_from_division

        #
        # build common neighborhood reference (for both daughter cells) if required
        #
        if parameters.use_common_neighborhood:
            monitoring.to_log_and_console("... build common neighborhoods", 1)
            neighborhoods = self.get_cell_neighborhood(delay_from_division=delay)
            self.set_cell_neighborhood(_build_common_neighborhoods(neighborhoods), delay_from_division=delay)
            self._use_common_neighborhood = True
            monitoring.to_log_and_console("    done", 1)

        #
        # dictionary indexed by mother cell names,
        # give list of references for which both daughter cells exist
        #
        monitoring.to_log_and_console("... build division list", 1)
        self._build_divisions(delay_from_division=delay)
        monitoring.to_log_and_console("    done", 1)

        #
        # dictionary indexed by mother cell names,
        # give list of daughter cells, the largest one being the first one
        #
        monitoring.to_log_and_console("... build unequal division list", 1)
        self._build_unequal_divisions(delay_from_division=delay)
        monitoring.to_log_and_console("    done", 1)

        if parameters.division_diagnosis:
            monitoring.to_log_and_console("")
            monitoring.to_log_and_console("============================================================")
            monitoring.to_log_and_console("===== diagnosis: atlases pairwise disagreements")
            _diagnosis_pairwise_switches(self, parameters)
            monitoring.to_log_and_console("===== diagnosis: dendrogram/linkage diagnosis")
            _diagnosis_linkage(self, parameters)
            monitoring.to_log_and_console("============================================================")
            monitoring.to_log_and_console("")

        #
        # look for daughter that may improve a global score
        # report it in the console/log file
        # as well as in morphonet selection file
        #
        if parameters.division_permutation_proposal:
            division_permutation_proposal(self, parameters)
    ############################################################
    #
    #
    #
    ############################################################

    def extract_division_neighborhoods(self, mother_name, delay_from_division=None):
        """
        Extract a sub-dictionary built from the daughter cell neighborhoods only for the references where
        the division of the targeted mother cell exists

        :param mother_name: cell name for which the divisions are searched;
        :param delay_from_division: neighborhoods are extracted after some delay from the division
        :return:
            A dictionary of cell neighborhoods indexed by [reference][0|1] where 'reference' is the atlas name
            (where the division occurs) and 0|1 corresponds to daughter names issued from uname.get_daughter_names()
        """

        delay = delay_from_division
        if delay is None:

            delay = self.default_delay

        divisions = self.get_division()
        neighborhoods = self.get_cell_neighborhood(delay_from_division=delay)
        daughters = aname.get_daughter_names(mother_name)

        config = {}
        for r in divisions[mother_name]:
            config[r] = {}
            config[r][0] = copy.deepcopy(neighborhoods[daughters[0]][r])
            config[r][1] = copy.deepcopy(neighborhoods[daughters[1]][r])
        return config


########################################################################################
#
#
#
########################################################################################


def _division_distance(daughter00, daughter01, daughter10, daughter11, innersurfaces=[], debug=False):
    """
    Compute distance between two contact surface vectors. Do not compute 'common' neighborhood.
    Parameters
    ----------
    daughter00
    daughter01
    daughter10
    daughter11
    innersurfaces

    Returns
    -------

    """

    #
    # get the sum of difference, as well as the sums of contact surfaces
    # cells in 'innersurfaces' are excluded
    #
    nm0, n00, n10 = ccsurfdist.contact_surface_distance_elements(daughter00, daughter10, innersurfaces=innersurfaces)
    nm1, n01, n11 = ccsurfdist.contact_surface_distance_elements(daughter01, daughter11, innersurfaces=innersurfaces)
    score = (nm0 + nm1) / (n00 + n10 + n01 + n11)
    return score


def division_distance(daughter00, daughter01, daughter10, daughter11, change_contact_surfaces=True, innersurfaces=[],
                      debug=False):
    """
    Compute the distance between two divisions (a division is a couple of daughter cells) with paired
        daughter cells (daughter00 is paired with daughter10 and daughter01 is paired with daughter11).
        The distance between two cells (ie two neighborhood) is defined by the ratio between the
        sum of absolute differences of (eligible) contact surfaces over the two sums of (eligible)
        contact surfaces for each neighborhood.
        The distance between two divisions is defined by the ratio between the sums of absolute differences
        of (eligible) contact surfaces for each couple of paired cells over the four sums of (eligible)
        contact surfaces for each neighborhood.
    Parameters
    ----------
    daughter00: dictionary depicting the neighborhood of daughter #0 of ref #0.
        Each key is a named neighbor, and the associated dictionary value give the contact surface.
    daughter01: dictionary depicting the neighborhood of daughter daughter #1 of ref #0
    daughter10: dictionary depicting the neighborhood of daughter daughter #0 of ref #1
    daughter11: dictionary depicting the neighborhood of daughter daughter #1 of ref #1
    change_contact_surfaces: True or False
    innersurfaces: name of the cells that define the innersurface; typically, they are the two names
        of the daughter cells when computing the distance between two divisions, which is built
        upon the distances between the neighborhood of each daughter. It comes to exclude the
        contact surface with the sister cell.
    debug

    Returns
    -------

    """

    if change_contact_surfaces:
        tmp = {'foo': {0: daughter00, 1: daughter10}}
        v0 = ccsurfdist.build_same_contact_surfaces(tmp, ['foo'], debug=debug)
        tmp = {'foo': {0: daughter01, 1: daughter11}}
        v1 = ccsurfdist.build_same_contact_surfaces(tmp, ['foo'], debug=debug)
        score = _division_distance(v0['foo'][0], v1['foo'][0], v0['foo'][1], v1['foo'][1], innersurfaces=innersurfaces)
    else:
        score = _division_distance(daughter00, daughter01, daughter10, daughter11, innersurfaces=innersurfaces)

    return score


########################################################################################
#
#
#
########################################################################################


def _division_signature(daughter00, daughter01, daughter10, daughter11, innersurfaces=[]):
    neighbors = set(daughter00.keys()).union(set(daughter01.keys()), set(daughter10.keys()), set(daughter11.keys()))
    den = 0.0
    num = 0.0
    for k in neighbors:
        if k in innersurfaces:
            continue
        if k in daughter00 and k in daughter10:
            num += abs(daughter00[k] - daughter10[k])
        elif k in daughter00 and k not in daughter10:
            num += abs(daughter00[k])
        elif k not in daughter00 and k in daughter10:
            num += abs(daughter10[k])
        if k in daughter01 and k in daughter11:
            num += abs(daughter01[k] - daughter11[k])
        elif k in daughter01 and k not in daughter11:
            num += abs(daughter01[k])
        elif k not in daughter01 and k in daughter11:
            num += abs(daughter11[k])
        mnum0 = 0.0
        if k in daughter00:
            mnum0 += daughter00[k]
        if k in daughter01:
            mnum0 += daughter01[k]
        mnum1 = 0.0
        if k in daughter10:
            mnum1 += daughter10[k]
        if k in daughter11:
            mnum1 += daughter11[k]
        num -= abs(mnum0 - mnum1)
        den += mnum0 + mnum1
    score = num / den
    if score < 0.0:
        return 0.0
    return score


def division_signature(daughter00, daughter01, daughter10, daughter11, change_contact_surfaces=True, innersurfaces=[],
                       debug=False):
    """
    Compute the distance increment between two divisions (a division is a couple of daughter cells) with paired
        daughter cells (daughter00 is paired with daughter10 and daughter01 is paired with daughter11).
        The distance between two cells (ie two neighborhood) is defined by the ratio between the
        sum of absolute differences of (eligible) contact surfaces over the two sums of (eligible)
        contact surfaces for each neighborhood.
        The distance increment between two divisions is defined as the division distance (between the
        neighborhoods of the daughter cells) minus the mother cell distance (between the
        neighborhoods of the mother cells). It is computed by the ratio between the sums of absolute
        differences of (eligible) contact surfaces for each couple of paired cells *minus* the sum of absolute
        differences of (eligible) contact surfaces for the mother cell (it comes to fuse daughter00 and daughter01
        on the one hand, and daughter10 and daughter11 on the other hand) over the four sums of (eligible)
        contact surfaces for each neighborhood.

    Parameters
    ----------
    daughter00: dictionary depicting the neighborhood of daughter #0 of ref #0.
        Each key is a named neighbor, and the associated dictionary value give the contact surface.
    daughter01: dictionary depicting the neighborhood of daughter daughter #1 of ref #0
    daughter10: dictionary depicting the neighborhood of daughter daughter #0 of ref #1
    daughter11: dictionary depicting the neighborhood of daughter daughter #1 of ref #1
    change_contact_surfaces: True or False
    innersurfaces: name of the cells that define the innersurface; typically, they are the two names
        of the daughter cells when computing the distance between two divisions, which is built
        upon the distances between the neighborhood of each daughter. It comes to exclude the
        contact surface with the sister cell.
    debug

    Returns
    -------

    """

    if change_contact_surfaces:
        tmp = {'foo': {0: daughter00, 1: daughter10}}
        v0 = ccsurfdist.build_same_contact_surfaces(tmp, ['foo'], debug=debug)
        tmp = {'foo': {0: daughter01, 1: daughter11}}
        v1 = ccsurfdist.build_same_contact_surfaces(tmp, ['foo'], debug=debug)
        score = _division_signature(v0['foo'][0], v1['foo'][0], v0['foo'][1], v1['foo'][1], innersurfaces=innersurfaces)
    else:
        score = _division_signature(daughter00, daughter01, daughter10, daughter11, innersurfaces=innersurfaces)

    return score


########################################################################################
#
#
#
########################################################################################


def _write_list(listtobeprinted, firstheader="", otherheader="", maxlength=112, verboseness=0):
    txt = ""
    n = 0
    for i, item in enumerate(listtobeprinted):
        if i == 0:
            txt = firstheader
            n = 0
        if len(txt) + len(str(item)) <= maxlength:
            if n >= 1:
                txt += ","
            txt += " " + str(item)
            n += 1
        else:
            monitoring.to_log_and_console(txt, verboseness=verboseness)
            txt = otherheader + " " + str(item)
            n = 1
        if i == len(listtobeprinted) - 1:
            monitoring.to_log_and_console(txt, verboseness=verboseness)


def _write_summary_pairwise_switches(atlases, summary):
    proc = "_write_summary_pairwise_switches"

    if isinstance(atlases, DivisionAtlases) is False:
        msg = ": unexpected type for 'division_atlases' variable: " + str(type(atlases))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    divisions = atlases.get_division()
    percents = []
    mother_names = list(summary.keys())

    for n in mother_names:
        percents.append(100.0 * float(len(summary[n]['disagreement'])) / float(summary[n]['tested_couples']))
    [sorted_percents, sorted_mothers] = list(zip(*sorted(zip(percents, mother_names), reverse=True)))

    majority = {}
    equality = {}
    for n in sorted_mothers:
        majority[n] = {}
        equality[n] = {}
        if len(summary[n]['disagreement']) == 0:
            continue
        msg = " - " + str(n) + " cell division into "
        msg += str(aname.get_daughter_names(n)) + " has " + str(len(summary[n]['disagreement']))
        if len(summary[n]['disagreement']) > 1:
            msg += " disagreements"
        else:
            msg += " disagreement"
        percent = 100.0 * float(len(summary[n]['disagreement'])) / float(summary[n]['tested_couples'])
        msg += " (" + "{:2.2f}%".format(percent) + ")"
        monitoring.to_log_and_console(msg)
        msg = "\t over " + str(summary[n]['tested_couples']) + " tested configurations "
        msg += "and over " + str(len(divisions[n]))
        msg += " references: "
        monitoring.to_log_and_console(msg)
        #
        # print references
        #
        _write_list(sorted(divisions[n]), firstheader="\t     ", otherheader="\t     ", maxlength=112, verboseness=0)
        #
        # count the cases where one atlas disagrees
        #
        for a in divisions[n]:
            s = 0
            for pair in summary[n]['disagreement']:
                if a in pair:
                    s += 1
            if 2 * s > len(divisions[n]):
                majority[n][a] = s
            elif 2 * s == len(divisions[n]):
                equality[n][a] = s
        #
        # print detailed disagreements
        #
        _write_list(sorted(summary[n]['disagreement']), firstheader="\t - disagreement list:", otherheader="\t     ",
                    maxlength=112, verboseness=3)

    nitems = 0
    for n in sorted_mothers:
        nitems += len(majority[n]) + len(equality[n])
    if nitems == 0:
        return
    monitoring.to_log_and_console("")
    monitoring.to_log_and_console(" --- atlases pairwise disagreements: summary ---")
    for n in sorted(mother_names):
        if len(majority[n]) > 0:
            msg = " - " + str(n) + " division, atlas that mostly disagrees:"
            akeys = sorted(list(majority[n].keys()))
            for i, a in enumerate(akeys):
                msg += " " + str(a) + " (" + str(majority[n][a]) + "/" + str(len(divisions[n])) + ")"
                if i < len(akeys) - 1:
                    msg += ","
            monitoring.to_log_and_console(msg)
    for n in sorted(mother_names):
        if len(equality[n]) > 0:
            msg = " - " + str(n) + " division, atlas that equally disagrees:"
            akeys = sorted(list(equality[n].keys()))
            for i, a in enumerate(akeys):
                msg += " " + str(a) + " (" + str(equality[n][a]) + "/" + str(len(divisions[n])) + ")"
                if i < len(akeys) - 1:
                    msg += ","
            monitoring.to_log_and_console(msg)


def _diagnosis_pairwise_switches(atlases, parameters):
    proc = "_diagnosis_pairwise_switches"

    if isinstance(atlases, DivisionAtlases) is False:
        msg = ": unexpected type for 'division_atlases' variable: " + str(type(atlases))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if not isinstance(parameters, npparameters.NamingPropagationParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    divisions = atlases.get_division()
    ccs = not atlases.use_common_neighborhood
    neighborhoods = atlases.get_cell_neighborhood(delay_from_division=parameters.delay_from_division)

    summary = {}
    innersurfaces = []

    for n in divisions:
        #
        # only one reference/atlas for mother cell 'n': nothing to do
        #
        if len(divisions[n]) <= 1:
            continue
        summary[n] = {}
        summary[n]['tested_couples'] = 0
        summary[n]['disagreement'] = []

        d = aname.get_daughter_names(n)
        #
        # this was to exclude the contact surface between daughters from computation
        # it does not matter, in fact
        #
        # if parameters.exclude_inner_surfaces:
        #    innersurfaces = [d[0], d[1]]

        for r1 in divisions[n]:
            for r2 in divisions[n]:
                if r2 <= r1:
                    continue
                summary[n]['tested_couples'] += 1
                #
                # test reference r1 versus r2
                # it is assumed that (r1, switched(r2)) is similar to (switched(r1), r2)
                # so only (r1, switched(r2)) is tested
                #
                switch_neighs = {d[0]: copy.deepcopy(neighborhoods[d[1]][r2]),
                                 d[1]: copy.deepcopy(neighborhoods[d[0]][r2])}
                if d[0] in switch_neighs[d[0]]:
                    switch_neighs[d[0]][d[1]] = switch_neighs[d[0]][d[0]]
                    del switch_neighs[d[0]][d[0]]
                if d[1] in switch_neighs[d[1]]:
                    switch_neighs[d[1]][d[0]] = switch_neighs[d[1]][d[1]]
                    del switch_neighs[d[1]][d[1]]
                #
                # same
                #
                same_dist = division_distance(neighborhoods[d[0]][r1], neighborhoods[d[1]][r1],
                                              neighborhoods[d[0]][r2], neighborhoods[d[1]][r2],
                                              change_contact_surfaces=ccs, innersurfaces=innersurfaces)
                swit_dist = division_distance(neighborhoods[d[0]][r1], neighborhoods[d[1]][r1],
                                              switch_neighs[d[0]], switch_neighs[d[1]],
                                              change_contact_surfaces=ccs, innersurfaces=innersurfaces)
                if same_dist < swit_dist:
                    continue
                summary[n]['disagreement'] += [(r1, r2)]

    divisions_with_disagreement = [n for n in summary if len(summary[n]['disagreement']) > 0]

    msg = "tested divisions = " + str(len(summary))
    monitoring.to_log_and_console(str(proc) + ": " + msg)
    msg = "divisions with pairwise disagreement =  " + str(len(divisions_with_disagreement))
    monitoring.to_log_and_console("\t " + msg)
    msg = "  A disagreement means that a division from a reference is closer to the\n"
    msg += "  switched division of an other reference than the division itself."
    monitoring.to_log_and_console(msg)
    monitoring.to_log_and_console("")
    if len(divisions_with_disagreement) > 0:
        _write_summary_pairwise_switches(atlases, summary)
    monitoring.to_log_and_console("")

    return summary


###########################################################
#
#
#
############################################################


def division_scipy_linkage(config, cluster_distance='single', change_contact_surfaces=True, innersurfaces=[],
                           distance='distance'):
    """

    Parameters
    ----------
    config: dictionary of dictionary of neighborhoods indexed by [reference] then by [0,1],
        where 0 stands for one daughter, and 1 for the other
    cluster_distance
    change_contact_surfaces
    innersurfaces
    distance

    Returns
    -------
    conddist: the squareform vector built from the distance matrice
       (see https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.squareform.html)
    z: the hierarchical clustering encoded as a linkage matrix
       (see https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html)
    labels: the list of atlas names

    """

    labels = []
    #
    # build a square matrix of distances
    #
    dist = np.zeros((len(config), len(config)))
    for i, r in enumerate(config):
        labels += [r]
        for j, s in enumerate(config):
            if r == s:
                dist[i][i] = 0.0
                continue
            if r > s:
                continue
            # if r == 'switched-' + str(s) or s == 'switched-' + str(r):
            #    continue
            dist[i][j] = 0.0
            if distance == 'signature':
                dist[i][j] = 100.0 * division_signature(config[r][0], config[r][1], config[s][0], config[s][1],
                                                        change_contact_surfaces=change_contact_surfaces,
                                                        innersurfaces=innersurfaces)
            else:
                dist[i][j] = 100.0 * division_distance(config[r][0], config[r][1], config[s][0], config[s][1],
                                                       change_contact_surfaces=change_contact_surfaces,
                                                       innersurfaces=innersurfaces)
            dist[j][i] = dist[i][j]

    conddist = sp.spatial.distance.squareform(dist)
    z = sch.linkage(conddist, method=cluster_distance)

    return conddist, z, labels


def switched_division_neighborhoods(config, mother_name):
    #
    # copy neighborhoods from atlases and add switched neighborhoods
    #
    daughters = aname.get_daughter_names(mother_name)
    swconfig = {}
    for r in config:
        swconfig[r] = {}
        swconfig[r][0] = copy.deepcopy(config[r][0])
        swconfig[r][1] = copy.deepcopy(config[r][1])
        sr = 'switched-' + str(r)
        swconfig[sr] = {}
        swconfig[sr][0] = copy.deepcopy(config[r][1])
        swconfig[sr][1] = copy.deepcopy(config[r][0])
        if daughters[1] in swconfig[sr][0] and swconfig[sr][0][daughters[1]] > 0:
            msg = "  weird, " + str(daughters[1]) + " was found in its neighborhood for reference " + str(r)
            monitoring.to_log_and_console("      " + msg)
        if daughters[0] in swconfig[sr][0]:
            swconfig[sr][0][daughters[1]] = swconfig[sr][0][daughters[0]]
            del swconfig[sr][0][daughters[0]]
        if daughters[0] in swconfig[sr][1] and swconfig[sr][1][daughters[0]] > 0:
            msg = "  weird, " + str(daughters[0]) + " was found in its neighborhood for reference " + str(r)
            monitoring.to_log_and_console("      " + msg)
        if daughters[1] in swconfig[sr][1]:
            swconfig[sr][1][daughters[0]] = swconfig[sr][1][daughters[1]]
            del swconfig[sr][1][daughters[1]]
    return swconfig


def _diagnosis_linkage(atlases, parameters):
    proc = "_diagnosis_linkage"

    if isinstance(atlases, DivisionAtlases) is False:
        msg = ": unexpected type for 'division_atlases' variable: " + str(type(atlases))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if not isinstance(parameters, npparameters.NamingPropagationParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    divisions = atlases.get_division()
    ccs = not parameters.use_common_neighborhood

    merge_values = {}
    lastmerge_values = {}

    swmerge_values = {}
    swlastmerge_values = {}

    division_lastmerge_values = {}

    innersurfaces = []

    #
    # morphonet selection
    # output_selections = atlases.get_output_selections()
    # for below, we will need embryoset
    # ref_atlases = atlases.get_atlases()
    #

    for n in divisions:
        stage = n.split('.')[0][1:]
        if len(divisions[n]) <= 2:
            continue

        # d = aname.get_daughter_names(n)
        # if parameters.exclude_inner_surfaces:
        #     innersurfaces = [d[0], d[1]]

        #
        # config is a dictionary indexed by [reference][0|1]
        # -> [reference][i] gives the neighborhood of daughter #i of division/mother n
        # swconfig contains the same neighborhoods than config plus the "switched" neighborhoods
        #
        config = atlases.extract_division_neighborhoods(n, delay_from_division=parameters.delay_from_division)
        swconfig = switched_division_neighborhoods(config, n)

        #
        # distance array for couples of atlases/references
        #
        conddist, z, labels = division_scipy_linkage(config, change_contact_surfaces=ccs, innersurfaces=innersurfaces)

        merge_values[stage] = merge_values.get(stage, []) + list(z[:, 2])
        lastmerge_value = z[:, 2][-1]
        lastmerge_values[stage] = lastmerge_values.get(stage, []) + [lastmerge_value]

        #
        # set the lastmerge_value in morphonet selection
        #
        # for r in divisions[n]:
        #     if r not in ref_atlases:
        #         if not (r[:4] == 'sym-' and r[4:] in ref_atlases):
        #             monitoring.to_log_and_console(proc + ": weird, '" + str(r) + "' is not in reference atlases.", 4)
        #         continue
        #     keyselection = "morphonet_float_" + str(r) + "_last_dendrogram_value"
        #     output_selections[keyselection] = output_selections.get(keyselection, {})
        #     lineage = ref_atlases[r].cell_lineage
        #     name = ref_atlases[r].cell_name
        #     cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
        #     for c in cells:
        #         if c not in name:
        #             continue
        #         if name[c] == n:
        #             output_selections[keyselection][c] = round(lastmerge_value)

        #
        # distance array for couples of atlases/references plus the switched ones
        #
        swconddist, swz, swlabels = division_scipy_linkage(swconfig, change_contact_surfaces=ccs,
                                                           innersurfaces=innersurfaces)

        swmerge_values[stage] = swmerge_values.get(stage, []) + list(swz[:, 2])
        swlastmerge_value = swz[:, 2][-1]
        swlastmerge_values[stage] = swlastmerge_values.get(stage, []) + [swlastmerge_value]

        division_lastmerge_values[n] = [lastmerge_value, swlastmerge_value]

    nochanges = {n: v for n, v in division_lastmerge_values.items() if v[1] <= v[0]}
    mother_with_nochanges = [n for n, v in division_lastmerge_values.items() if v[1] <= v[0]]
    #
    # set the lastmerge_value in morphonet selection
    #
    # for n in mother_with_nochanges:
    #     for r in divisions[n]:
    #         if r not in ref_atlases:
    #             if not (r[:4] == 'sym-' and r[4:] in ref_atlases):
    #                 monitoring.to_log_and_console(proc + ": weird, '" + str(r) + "' is not in reference atlases.", 4)
    #             continue
    #         keyselection = "morphonet_selection_" + str(r) + "_dendrogram_warning"
    #         output_selections[keyselection] = output_selections.get(keyselection, {})
    #         lineage = ref_atlases[r].cell_lineage
    #         name = ref_atlases[r].cell_name
    #         cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
    #         for c in cells:
    #             if c not in name:
    #                 continue
    #             if name[c] == n:
    #                 output_selections[keyselection][c] = 100

    monitoring.to_log_and_console("------ division with same dendrogram last values (without and with switch)")
    msg = str(len(nochanges)) + "/" + str(len(division_lastmerge_values)) + " divisions"
    monitoring.to_log_and_console("\t " + msg)

    monitoring.to_log_and_console("------ cell-based view")
    division_by_generation = {}
    for m in mother_with_nochanges:
        g = m.split('.')[0][1:]
        division_by_generation[g] = division_by_generation.get(g, []) + [m]
    for g in division_by_generation:
        monitoring.to_log_and_console("  - generation " + str(g))
        mothers = list(division_by_generation[g])
        mothers.sort()
        for m in mothers:
            msg = "    - division of '" + str(m) + "': " + str(division_lastmerge_values[m])
            monitoring.to_log_and_console(msg)

    monitoring.to_log_and_console("------ dendrogram last-value view")
    sorted_nochanges = sorted(nochanges.items(), key=lambda v: v[1][0], reverse=True)
    for s in sorted_nochanges:
        msg = "    - division of '" + str(s[0]) + "': " + str(s[1])
        monitoring.to_log_and_console(msg)
    monitoring.to_log_and_console("")


#######################################################################################
#
#
#
########################################################################################

def _dpp_switch_contact_surfaces(neighbors, reference, daughters):
    """
    Switch contact surfaces for the two daughters and atlas 'reference'.
    Parameters
    ----------
    neighbors
    reference
    daughters

    Returns
    -------

    """
    #
    # contact surfaces of daughters[0] for atlas 'reference'
    # replace contact surface with daughters[1] with a contact surface with daughters[0]
    #
    neighs = {0: copy.deepcopy(neighbors[1][reference]), 1: copy.deepcopy(neighbors[0][reference])}
    if daughters[0] in neighs[0]:
        neighs[0][daughters[1]] = neighs[0][daughters[0]]
        del neighs[0][daughters[0]]
    if daughters[1] in neighs[1]:
        neighs[1][daughters[0]] = neighs[1][daughters[1]]
        del neighs[1][daughters[1]]

    neighbors[0][reference] = neighs[0]
    neighbors[1][reference] = neighs[1]
    return neighbors


def _dpp_global_generic_distance(neighbors, references, parameters, debug=False):
    """
    Compute a global score. The global score is the average of local similarities over all
    couples of references/atlases.

    Parameters
    ----------
    neighbors: neighborhoods for the two daughters (dictionary indexed by [0,1] then by the references
    references: set of references
    debug

    Returns
    -------

    """

    innersurfaces = []
    # d = aname.get_daughter_names(mother)
    # if parameters.exclude_inner_surfaces:
    #     innersurfaces = [d[0], d[1]]

    ccs = not parameters.use_common_neighborhood
    score = 0
    n = 0
    distances = {}
    for r1 in references:
        if debug:
            distances[r1] = {}
        for r2 in references:
            if r2 <= r1:
                continue
            dist = division_distance(neighbors[0][r1], neighbors[1][r1], neighbors[0][r2], neighbors[1][r2],
                                     change_contact_surfaces=ccs, innersurfaces=innersurfaces)
            if debug:
                distances[r1][r2] = dist
            score += dist
            n += 1
    if debug:
        print("---- _dpp_global_generic_distance")
        refs1 = distances.keys()
        refs1 = sorted(refs1)
        for r1 in refs1:
            refs2 = distances[r1].keys()
            refs2 = sorted(refs2)
            for r2 in refs2:
                print("   - dist[" + str(r1) + ", " + str(r2) + "] = " + str(distances[r1][r2]))
    return score / n


def _dpp_test_one_division(atlases, mother, parameters):
    """
    Test whether any daughter switch (for a given reference) improve a global score
    Parameters
    ----------
    atlases
    mother
    parameters

    Returns
    -------

    """
    divisions = atlases.get_division()
    if len(divisions[mother]) <= 1:
        return {}, []

    daughters = aname.get_daughter_names(mother)
    neighborhoods = atlases.get_cell_neighborhood(delay_from_division=parameters.delay_from_division)
    neighbors = {0: copy.deepcopy(neighborhoods[daughters[0]]), 1: copy.deepcopy(neighborhoods[daughters[1]])}

    # score before any changes
    debug = False
    if debug:
        print("")
        print("===== test division " + str(mother) + " : " + str(divisions[mother]))
    score = _dpp_global_generic_distance(neighbors, divisions[mother], parameters, debug=debug)

    returned_scores = [(None, score)]
    corrections = []
    i = 1
    while True:
        newscore = {}
        for r in sorted(divisions[mother]):
            #
            # switch contact surfaces for the daughters in atlas 'r'
            #
            tmp = copy.deepcopy(neighbors)
            tmp = _dpp_switch_contact_surfaces(tmp, r, daughters)
            if debug:
                print("===== test switch " + str(mother) + " / " + str(r))
            # compute a new score, keep it if it better than the one before any changes
            newscore[r] = _dpp_global_generic_distance(tmp, divisions[mother], parameters, debug=debug)
            if debug:
                print("     new score = " + str(newscore[r]) + " - original score = " + str(score))
            if newscore[r] > score:
                del newscore[r]
        # no found correction at this iteration
        if len(newscore) == 0:
            return corrections, returned_scores
        # found several correction
        # 1. pick the only one (if only one is found)
        # 2. or pick the one with maximal score change
        elif len(newscore) == 1:
            ref = list(newscore.keys())[0]
        else:
            ref = min(newscore, key=lambda key: newscore[key])
        # first iteration, keep the value of the global score decrease
        if i == 1:
            for r in newscore:
                returned_scores += [(r, score - newscore[r])]
        corrections += [(ref, score - newscore[ref])]
        i += 1
        # if one correction has been found, apply it
        # and look for an other additional correction
        tmp = copy.deepcopy(neighbors)
        tmp = _dpp_switch_contact_surfaces(tmp, ref, daughters)
        neighbors[0] = copy.deepcopy(tmp[0])
        neighbors[1] = copy.deepcopy(tmp[1])
        score = newscore[ref]


def division_permutation_proposal(atlases, parameters):

    # neighborhoods is a dictionary of dictionaries
    # ['cell name']['reference name']
    # first key is a cell name (daughter cell)
    # second key is the reference from which the neighborhood has been extracted

    #
    # mother cell name dictionary indexed by stage
    # stage 6: 32 cells
    # stage 7: 64 cells
    #

    divisions = atlases.get_division()
    mothers = {}
    for n in divisions:
        stage = n.split('.')[0][1:]
        mothers[stage] = mothers.get(stage, []) + [n]
    for s in mothers:
        mothers[s] = sorted(mothers[s])

    stages = list(mothers.keys())
    stages.sort()
    corrections = {}
    selection = {}

    for s in stages:
        corrections[s] = {}
        # if int(s) != 7:
        #    continue
        for m in mothers[s]:
            # if m != 'a7.0002_':
            #     continue
            correction, returned_score = _dpp_test_one_division(atlases, m, parameters)
            #
            # correction is a dictionary indexed by the iteration index
            # each value is a tuple ('atlas name', score increment)
            #
            if len(correction) > 0:
                corrections[s][m] = correction
                selection[m] = returned_score

    #
    # build output selections
    #
    # ref_atlases = atlases.get_atlases()
    # output_selections = atlases.get_output_selections()
    #
    # for m in selection:
    #     if len(selection[m]) <= 1:
    #         continue
    #     (a, score) = selection[m][0]
    #
    #     for i, (ref, ds) in enumerate(selection[m]):
    #
    #         if i == 0:
    #             continue
    #
    #         # check if the reference is registered
    #         # discard symmetrical neighborhood for warning
    #         if ref not in ref_atlases:
    #             if not (ref[:4] == 'sym-' and ref[4:] in ref_atlases):
    #                 monitoring.to_log_and_console(proc + ": weird, '" + str(ref) + "' is not in reference atlases.",4)
    #             continue
    #
    #         keyscore = "morphonet_float_" + str(ref) + "_distance_average_before_permutation_proposal"
    #         keydecre = "morphonet_float_" + str(ref) + "_distance_decrement_percentage_after_permutation_proposal"
    #         output_selections[keyscore] = output_selections.get(keyscore, {})
    #         output_selections[keydecre] = output_selections.get(keydecre, {})
    #
    #         lineage = ref_atlases[ref].cell_lineage
    #         name = ref_atlases[ref].cell_name
    #         cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
    #
    #         for c in cells:
    #             if c not in name:
    #                 continue
    #             if name[c] == m:
    #                 output_selections[keyscore][c] = score
    #                 output_selections[keydecre][c] = ds / score

    #
    # reporting
    #
    monitoring.to_log_and_console("====== division permutation proposal =====")
    monitoring.to_log_and_console("------ cell-based view")
    corrections_by_atlas = {}
    for s in stages:
        if len(corrections[s]) == 0:
            continue
        msg = "  - generation " + str(s)
        monitoring.to_log_and_console(msg)
        mothers = list(corrections[s].keys())
        mothers.sort()
        for m in mothers:
            if len(corrections[s][m]) == 0:
                continue
            tmp = [c[0] for c in corrections[s][m]]
            tmp.sort()
            for a in tmp:
                corrections_by_atlas[a] = corrections_by_atlas.get(a, []) + [m]
            msg = "    - division of '" + str(m) + "': "
            for i, r in enumerate(tmp):
                msg += str(r)
                if i < len(tmp)-1:
                    msg += ", "
            monitoring.to_log_and_console(msg)
    if len(corrections_by_atlas) > 0:
        monitoring.to_log_and_console("------ atlas-based view")
        refs = list(corrections_by_atlas.keys())
        refs.sort()
        for r in refs:
            msg = "  - reference '" + str(r) + "': " + str(corrections_by_atlas[r])
            monitoring.to_log_and_console(msg)

    if len(selection) > 0:
        quadruplet = []
        for m in selection:
            (a, score) = selection[m][0]
            if len(selection[m]) <= 1:
                continue
            for i, (ref, ds) in enumerate(selection[m]):
                if i == 0:
                    continue
                quadruplet += [(m, ref, score, 100.0 * ds / score)]
        quadruplet = sorted(quadruplet, key=operator.itemgetter(1))
        quadruplet = sorted(quadruplet, key=operator.itemgetter(3), reverse=True)
        monitoring.to_log_and_console("------ average distance percentage decrease view")
        for q in quadruplet:
            msg = "  - division of '" + str(q[0]) + "' in '" + str(q[1]) + "': "
            msg += "{:2.2f}% decrease of {:1.2f} average distance".format(q[3], q[2])
            monitoring.to_log_and_console(msg)

    monitoring.to_log_and_console("==========================================")


###########################################################
#
#
#
############################################################


def _build_common_neighborhoods(neighborhoods):
    """
    Build a new neighborhood dictionary where both a cell and its sister have
    the same neighbors.

    :param neighborhoods: dictionary of dictionaries
        ['cell name']['reference name']['neighboring cell']
        first key is a cell name;
        second key is a reference name (ie an atlas):  neighborhoods['cell name']['reference name'] is then
        the neighborhood of 'cell name' extracted from 'reference name';
        the neighborhood itself is a dictionary, indexed by the neighboring cell names, whose values are
        contact surfaces.
    :return:
        The neighborhood dictionary where both a cell and its sister have the same neighbors.
    """

    proc = "_build_common_neighborhoods"

    common_neighborhoods = {}

    for cell in neighborhoods:
        if cell in common_neighborhoods:
            continue
        sister = aname.get_sister_name(cell)
        if sister in common_neighborhoods:
            msg = "weird, '" + str(sister) + "' is in neighborhoods while '" + str(cell) + "' is not"
            monitoring.to_log_and_console(proc + ": " + msg)
        new_neighborhoods = ccsurfdist.build_same_contact_surfaces(neighborhoods, [cell, sister])
        for n in new_neighborhoods:
            common_neighborhoods[n] = copy.deepcopy(new_neighborhoods[n])
    return common_neighborhoods


##############################################################
#
#
#
##############################################################


def _get_branch_length(cell, lineage):
    length = 0
    c = cell
    while c in lineage and len(lineage[c]) == 1:
        length += 1
        c = lineage[c][0]
    return length


def _update_division_atlas_from_embryo(division_atlases, embryo, embryo_name, delay_from_division=3):
    proc = "_update_division_atlas_from_embryo"

    if isinstance(division_atlases, DivisionAtlases) is False:
        msg = ": unexpected type for 'division_atlases' variable: " + str(type(division_atlases))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if isinstance(embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryos' variable: " + str(type(embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if embryo.cell_name is None:
        msg = "atlas '" + str(embryo_name) + "' has no name entry, will not be added"
        monitoring.to_log_and_console(str(proc) + ": " + msg)
        return
    if embryo.cell_volume is None:
        msg = "atlas '" + str(embryo_name) + "' has no volume entry, will not be added"
        monitoring.to_log_and_console(str(proc) + ": " + msg)
        return
    if embryo.cell_lineage is None:
        msg = "atlas '" + str(embryo_name) + "' has no lineage entry, will not be added"
        monitoring.to_log_and_console(str(proc) + ": " + msg)
        return
    if embryo.rectified_cell_contact_surface is None:
        msg = "atlas '" + str(embryo_name) + "' has no contact surface entry, will not be added"
        monitoring.to_log_and_console(str(proc) + ": " + msg)
        return

    #
    # build a nested dictionary of neighborhood, where the keys are
    # ['cell name']['reference name']
    # where 'reference name' is the name of the reference lineage
    # and the neighborhood a dictionary of contact surfaces indexed by cell names
    #

    neighborhoods = division_atlases.get_cell_neighborhood(delay_from_division=delay_from_division)
    volumes = division_atlases.get_volume(delay_from_division=delay_from_division)

    #
    # clean cell names:
    # - remove empty names
    # - leading or trailing spaces
    # was necessary for original property files (science ones stored on fileshare)
    #
    cell_name = embryo.cell_name
    cells = list(cell_name.keys())
    for c in cells:
        if cell_name[c] == '':
            del cell_name[c]
            continue
        cell_name[c] = cell_name[c].strip()

    #
    # volumes and contact surfaces are normalized
    #
    cell_volume = embryo.rectified_cell_volume
    cell_lineage = embryo.cell_lineage
    cell_contact = embryo.rectified_cell_contact_surface

    div = 10 ** embryo.time_digits_for_cell_id

    #
    # get the daughter cells just after division
    #
    reverse_lineage = {v: k for k, values in cell_lineage.items() for v in values}
    daughters = [cell_lineage[c][0] for c in cell_lineage if len(cell_lineage[c]) == 2]
    daughters += [cell_lineage[c][1] for c in cell_lineage if len(cell_lineage[c]) == 2]

    ancestor_name = []
    missing_name = []
    missing_contact = []
    missing_neighbors = []

    #
    # parse the cells
    #
    for daugh in daughters:
        #
        # check whether the cell is in dictionaries
        #
        if daugh not in cell_name:
            if daugh not in missing_name:
                missing_name.append(daugh)
                monitoring.to_log_and_console("\t" + str(proc) + ": daughter cell #" + str(daugh)
                                              + " was not found in 'cell_name' dictionary. Skip it", 6)
            continue
        if daugh not in cell_contact:
            if daugh not in missing_contact:
                missing_contact.append(daugh)
                monitoring.to_log_and_console("\t" + str(proc) + ": daughter cell #" + str(daugh)
                                              + " was not found in 'cell_contact_surface' dictionary. Skip it")
            continue

        #
        # mother cell should be named
        #
        if reverse_lineage[daugh] not in cell_name:
            msg = "weird, mother cell #" + str(reverse_lineage[daugh]) + " of  cell #" + str(daugh) + " is not named."
            msg += " Skip it."
            monitoring.to_log_and_console("\t" + str(proc) + ": " + msg, 6)
            continue

        #
        # check whether the mother name is the right one
        #
        if cell_name[reverse_lineage[daugh]] != aname.get_mother_name(cell_name[daugh]):
            msg = "weird, name of daughter cell #" + str(daugh) + " is " + str(cell_name[daugh])
            msg += " while its mother #" + str(reverse_lineage[daugh]) + " is named "
            msg += str(cell_name[reverse_lineage[daugh]]) + ". Skip it"
            monitoring.to_log_and_console("\t" + str(proc) + ": " + msg)
            continue

        #
        # get the daughter cell after some additional delay
        # positive delay: count from the division
        # negative delay: count from the end of the shortest branch for the two sisters
        #   to get the same delay for the two sisters
        #   kept for historical reason (and also for comparison purpose)
        #
        d = daugh
        local_delay_from_division = 0
        if delay_from_division >= 0:
            local_delay_from_division = delay_from_division
        elif delay_from_division < 0:
            length0 = _get_branch_length(d, cell_lineage)
            sisters = copy.deepcopy(cell_lineage[reverse_lineage[d]])
            sisters.remove(d)
            length1 = _get_branch_length(sisters[0], cell_lineage)
            local_delay_from_division = min(length0, length1) + delay_from_division
            if local_delay_from_division < 0:
                local_delay_from_division = 0

        for i in range(local_delay_from_division):
            if d not in cell_lineage:
                break
            if len(cell_lineage[d]) > 1:
                break
            nextd = cell_lineage[d][0]
            if nextd not in cell_name or nextd not in cell_contact:
                break
            d = nextd

        #
        # build the neighborhood of cell of id 'd'
        #
        neighbor = {}
        neighbor_is_complete = True
        for c in cell_contact[d]:
            n = int(c) % div
            #
            # historically, some '0' were remaining
            #
            if n == 1 or n == 0:
                neighbor['background'] = neighbor.get('background', 0) + cell_contact[d][c]
                continue
            #
            # general case
            #
            cname = c
            #
            # if c is not named, get a named ancestor
            #
            if cname not in cell_name:
                while cname in reverse_lineage and cname not in cell_name:
                    cname = reverse_lineage[cname]
            if cname not in cell_name:
                neighbor_is_complete = False
                if c not in missing_neighbors:
                    missing_neighbors.append(c)
                    msg = "cell #" + str(c) + " (nor any ancestor) was not found in 'cell_name' dictionary."
                    monitoring.to_log_and_console("\t" + proc + ": " + msg)
                continue
            #
            # cname is either the cell id or the id of a named ancestor
            #
            this_cell_name = cell_name[cname]

            #
            # ancestor_name is just used to keep trace of 'ancestor' replacement
            # not really useful
            #
            if cname != c:
                if c not in ancestor_name:
                    ancestor_name += [c]
                    msg = "cell #" + str(c) + " (neighbor of " + cell_name[d] + " of '" + str(embryo_name)
                    msg += "') is not named.\n"
                    msg += "\t\tUse name '" + str(this_cell_name) + "' of its ancestor cell #" + str(cname)
                    monitoring.to_log_and_console("\t" + proc + ": " + msg)
            #
            # add the contact surface contribution to 'this_cell_name'
            #
            neighbor[this_cell_name] = neighbor.get(this_cell_name, 0) + cell_contact[d][c]

        #
        # check whether all neighboring cells have a name
        #
        if not neighbor_is_complete:
            msg = ": neighborhood of " + str(cell_name[d]) + " is not complete. Skip it"
            monitoring.to_log_and_console("\t" + str(proc) + msg)
            continue

        #
        # add cell neighborhood and volume
        #
        key_cell_name = cell_name[d]

        if key_cell_name not in neighborhoods:
            neighborhoods[key_cell_name] = {}
        if embryo_name in neighborhoods[key_cell_name]:
            msg = "weird, " + str(embryo_name) + " was already indexed for neighbors of cell " + \
                  str(cell_name[d])
            monitoring.to_log_and_console("\t" + str(proc) + ": " + msg)
        neighborhoods[key_cell_name][embryo_name] = neighbor

        #
        # add cell volume
        #
        if key_cell_name not in volumes:
            volumes[key_cell_name] = {}
        if embryo_name in volumes[key_cell_name]:
            msg = "weird, " + str(embryo_name) + " was already indexed for volume of cell " + \
                  str(cell_name[d])
            monitoring.to_log_and_console("\t" + str(proc) + ": " + msg)
        if d not in cell_volume:
            msg = "\t cell #" + str(d) + " was not found in 'cell_volume' dictionary."
            monitoring.to_log_and_console("\t" + str(proc) + ": " + msg)
        else:
            volumes[key_cell_name][embryo_name] = cell_volume[d]

    division_atlases.set_cell_neighborhood(neighborhoods, delay_from_division=delay_from_division)
    division_atlases.set_volume(volumes, delay_from_division=delay_from_division)

    if len(missing_name) > 0:
        msg = ": daughter cells without names = " + str(len(missing_name)) + "/" + str(len(daughters))
        msg += " in '" + str(embryo_name) + "'"
        monitoring.to_log_and_console(str(proc) + msg)

    if len(missing_contact) > 0:
        msg = ": daughter cells without contact surfaces  = " + str(len(missing_contact))
        msg += "/" + str(len(daughters)) + " in '" + str(embryo_name) + "'"
        monitoring.to_log_and_console(str(proc) + msg)

    if len(missing_neighbors) > 0:
        msg = ": neighboring cells without names  = " + str(len(missing_neighbors))
        msg += " in '" + str(embryo_name) + "'"
        monitoring.to_log_and_console(str(proc) + msg)

    # monitoring.to_log_and_console("")

    return


##############################################################
#
#
#
##############################################################


def _update_division_atlas_from_embryo_set(division_atlases, imported_embryos, delay_from_division=3):
    proc = "_update_division_atlas_from_embryo_set"

    local_delay_from_division = 3
    if delay_from_division is not None:
        if isinstance(delay_from_division, int):
            local_delay_from_division = delay_from_division

    if isinstance(division_atlases, DivisionAtlases) is False:
        msg = ": unexpected type for 'division_atlases' variable: " + str(type(division_atlases))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if isinstance(imported_embryos, cembryoset.EmbryoSet) is False:
        msg = ": unexpected type for 'embryos' variable: " + str(type(imported_embryos))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    monitoring.to_log_and_console("... create atlas of neighborhood")
    all_embryos = imported_embryos.get_embryos()
    for e in all_embryos:
        monitoring.to_log_and_console("   ... add embryo '" +  str(e) + "'")
        _update_division_atlas_from_embryo(division_atlases, all_embryos[e], str(e),
                                           delay_from_division=local_delay_from_division)
    return

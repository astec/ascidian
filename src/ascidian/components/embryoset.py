##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Ven 21 jui 2024 11:27:20 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys
import statistics

from operator import itemgetter

import astec.utils.common as common
import astec.utils.ioproperties as ioproperties

import ascidian.components.embryo as cembryo
import ascidian.core.diagnosis as diagnosis
import ascidian.components.parameters as cparameters

monitoring = common.Monitoring()


############################################################
#
#
#
############################################################

def get_division_time_by_cell_atlas(embryos):
    """

    Parameters
    ----------
    embryos

    Returns
    -------
    A dictionary indexed by cell names. Each entry is a also a dictionary indexed by atlas names,
    whose value is the cell division developmental time for this embryo,
    meaning that the temporal alignment has been performed.
    Eg
    timepoints = get_division_time_by_cell_atlas(embryos)
    timepoints['a7.0002*']['Pm1'] is the developmental time of division for 'a7.0002*' in 'Pm1'
    """
    proc = "get_division_time_by_cell_atlas"

    if isinstance(embryos, EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        sys.exit(1)

    timepoints = {}
    ref_embryos = embryos.get_embryos()

    for aname in ref_embryos:
        div = 10 ** ref_embryos[aname].time_digits_for_cell_id
        cell_name = ref_embryos[aname].cell_name
        if cell_name is None or len(cell_name) == 0:
            continue
        cell_lineage = ref_embryos[aname].cell_lineage
        temporal_coefficients = ref_embryos[aname].temporal_alignment
        divisions = [c for c in cell_lineage if len(cell_lineage[c]) == 2]
        for d in divisions:
            if d not in cell_name:
                continue
            #
            # pool together '_' and '*'
            #
            cname = cell_name[d]
            if cname not in timepoints:
                timepoints[cname] = {}
            if aname in timepoints[cname]:
                msg = "weird, '" + str(aname) + "' is already in division of cell '" + str(cname) + "'"
                monitoring.to_log_and_console("    " + proc + ": " + msg)
            else:
                acq_time = int(d) // div
                t = temporal_coefficients[0] * acq_time + temporal_coefficients[1]
                timepoints[cname][aname] = t
    return timepoints


def _diagnosis_division_timepoint(embryos, parameters):
    #
    # timepoints is a dictionary indexed by cell names (without '_' or '*'). Each entry is a list of tuples
    # (time of division, atlas name + [_ or *]) sorted by increasing time
    # timepoints is a dictionary indexed by cell names (with '_' or '*'). Each entry is a dictionary
    # indexed by atlas name, and values is a list of times of division
    #
    proc = "_diagnosis_division_timepoint"

    if isinstance(embryos, EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        sys.exit(1)
    if isinstance(parameters, diagnosis.DiagnosisParameters) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: " + str(type(embryos)))
        sys.exit(1)

    timepoints = get_division_time_by_cell_atlas(embryos)
    #
    # intra-embryo largest (normalized) time interval
    #
    intra_timepoints = {}
    for d in timepoints:
        # d left and right
        dlr = d[:-1]
        if dlr not in intra_timepoints:
            intra_timepoints[dlr] = {}
        for a in timepoints[d]:
            intra_timepoints[dlr][a] = intra_timepoints[dlr].get(a, []) + [timepoints[d][a]]

    #
    # intra_timepoints[dlr][a] : dictionary (cell name without * or _) of dictionary (atlas names) of time divisions
    #
    intra_division_time_interval = []
    for d in intra_timepoints:
        for a in intra_timepoints[d]:
            if len(intra_timepoints[d][a]) != 2:
                continue
            intra_division_time_interval += [(d, a, abs(intra_timepoints[d][a][0] - intra_timepoints[d][a][1]))]
    intra_division_time_interval = sorted(intra_division_time_interval, key=itemgetter(2), reverse=True)

    #
    # inter-embryo largest (normalized) time interval
    #
    inter_timepoints = {}
    for d in timepoints:
        dlr = d[:-1]
        if dlr not in inter_timepoints:
            inter_timepoints[dlr] = []
        for a in timepoints[d]:
            inter_timepoints[dlr] += [timepoints[d][a]]

    inter_division_time_interval = []
    for d in inter_timepoints:
        inter_division_time_interval += [(d, max(inter_timepoints[d]) - min(inter_timepoints[d]))]
    inter_division_time_interval = sorted(inter_division_time_interval, key=itemgetter(1), reverse=True)

    monitoring.to_log_and_console("")
    monitoring.to_log_and_console("  === division (normalized) time interval diagnosis === ")

    monitoring.to_log_and_console("  - intra-embryo largest (normalized) time interval")
    n = parameters.items
    if n > len(intra_division_time_interval):
        n = len(intra_division_time_interval)
    for i in range(n):
        msg = "    cell " + str(intra_division_time_interval[i][0])
        msg += " '" + str(intra_division_time_interval[i][1]) + "'"
        msg += " time interval = {:.2f}".format(intra_division_time_interval[i][2])
        monitoring.to_log_and_console(msg)
        msg = "       "
        msg += str(intra_division_time_interval[i][1]) + ": "
        for j, e in enumerate(['_', '*']):
            d = str(intra_division_time_interval[i][0]) + e
            msg += str(d) + ": {:.2f}".format(timepoints[d][intra_division_time_interval[i][1]])
            if j == 0:
                msg += ", "

        monitoring.to_log_and_console(msg)

    monitoring.to_log_and_console("  - inter-embryo largest (normalized) time interval")
    n = parameters.items
    if n > len(inter_division_time_interval):
        n = len(inter_division_time_interval)
    for i in range(n):
        msg = "    cell " + str(inter_division_time_interval[i][0])
        msg += " time interval = {:.2f}".format(inter_division_time_interval[i][1])
        monitoring.to_log_and_console(msg)
        msg = "       "
        j = 0
        lj = 0
        for e in ['_', '*']:
            d = str(inter_division_time_interval[i][0]) + e
            if d in timepoints:
                lj += len(timepoints[d])
        for e in ['_', '*']:
            d = str(inter_division_time_interval[i][0]) + e
            if d in timepoints:
                for a in timepoints[d]:
                    msg += str(a) + "(" + str(e) + ") at " + "{:.2f}".format(timepoints[d][a])
                    if j < lj-1:
                        msg += ", "
                        if j > 0 and (j+1) % 3 == 0:
                            msg += "\n"
                            msg += "       "
                    j += 1
        monitoring.to_log_and_console(msg)
    monitoring.to_log_and_console("  === end of diagnosis === ")
    monitoring.to_log_and_console("")


############################################################
#
# EmbryoSet: set of many embryos
#
############################################################

class EmbryoSet(object):
    """
    The EmbryoSet object contains a collection of embryos, each of them being an
    'Embryo' object. They can be retrieved by the method 'get_atlases()'
    that returns a dictionary whose keys are the atlas' names, and whose
    values are the 'Embryo' object.

    'Embryo' objects have properties
    - copied (if existing) from the original property file (retrieved by atlas.'property name')
        - cell_lineage
        - cell_name
        - cell_volume
        - cell_contact_surface
        - cell_barycenter
    - computed
        - temporal_alignment: tuple (a, b). Time (at+b) of the embryo corresponds to
          the time t of a reference embryo
        - volume_local_estimation: tuple (a, b). Embryo volume along time is fitted by (at+b)
          allows to compute a time-varying scaling factor to get a constant embryo volume
    - rectified ones: the scaling factor issued from the volume_local_estimation is applied
        so that all embryos (at any time point) have the same volume (and are then comparable)
        - rectified_cell_volume
        - rectified_cell_contact_surface
        - rectified_cell_barycenter
    
    """
    def __init__(self, ref_embryo_name=None, parameters=None):
        #
        self._symprefix = "sym-"
        # atlases
        self._embryos = {}
        # reference atlas for time alignment
        self._ref_embryo_name = None

        if ref_embryo_name is not None and isinstance(ref_embryo_name, str):
            self.set_reference_embryo_name(ref_embryo_name)
        elif parameters is not None:
            self.update_from_parameters(parameters)

    ############################################################
    #
    # getters / setters
    #
    ############################################################

    @property
    def symprefix(self):
        return self._symprefix

    def n_embryos(self):
        return len(self._embryos)

    def get_embryos(self):
        return self._embryos

    def set_embryos(self, embryos):
        self._embryos = embryos

    def get_reference_embryo_name(self):
        proc = "EmbryoSet.get_reference_embryo_name"
        if self._ref_embryo_name is None:
            embryos = self.get_embryos()
            if len(embryos) == 0:
                monitoring.to_log_and_console(proc + ": no reference embryo nor registered embryos")
                return None
            names = sorted(list(embryos.keys()))
            monitoring.to_log_and_console("   ... set reference to '" + str(names[0]) + "'")
            self.set_reference_embryo_name(names[0])
        return self._ref_embryo_name

    def get_reference_embryo(self):
        reference_embryo_name = self.get_reference_embryo_name()
        if reference_embryo_name is None:
            return None
        embryos = self.get_embryos()
        if reference_embryo_name not in embryos:
            return None
        return embryos[reference_embryo_name]

    def set_reference_embryo_name(self, reference_embryo_name):
        self._ref_embryo_name = reference_embryo_name

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):
        proc = "EmbryoSet.update_from_parameters"
        if not isinstance(parameters, cparameters.BaseParameters):
            monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                          + str(type(parameters)))
            return
        if parameters.referenceEmbryo is not None:
            name = parameters.referenceEmbryo.split(os.path.sep)[-1]
            if name.endswith(".xml") or name.endswith(".pkl"):
                name = name[:-4]
            self.set_reference_embryo_name(name)

    ############################################################
    #
    #
    #
    ############################################################

    def compute_temporal_alignment(self, mincells=None, maxcells=None, fate=None):
        proc = "compute_temporal_alignment"
        ref_name = self.get_reference_embryo_name()
        if ref_name is None:
            monitoring.to_log_and_console(proc + ": no reference embryo, can not perform temporal alignment")
            return
        embryos = self.get_embryos()
        if ref_name not in embryos:
            msg = "'" + str(ref_name) + "' is not in registered atlases, can not perform temporal alignment"
            monitoring.to_log_and_console(proc + ": " + msg)
            return

        #
        # compute temporal alignment
        #
        for a in embryos:
            if a == ref_name or a == self._symprefix + ref_name:
                if fate is not None:
                    embryos[ref_name].set_temporal_alignment((1.0, 0.0), key=fate)
                else:
                    embryos[ref_name].set_temporal_alignment((1.0, 0.0))
                continue
            if fate is None:
                embryos[a].temporally_align_with(embryos[ref_name], mincells=mincells, maxcells=maxcells)
            else:
                embryos[a].temporally_align_with(embryos[ref_name], mincells=mincells, maxcells=maxcells, fate=fate)

        for n in embryos:
            temporal_alignment = embryos[n].get_temporal_alignment(key=fate)
            msg = "   ... "
            msg += "linear time warping of '" + str(n) + "' wrt '" + str(ref_name) + "' is "
            msg += "({:.3f}, {:.3f})".format(temporal_alignment[0], temporal_alignment[1])
            if fate is None:
                msg += " [all cells]"
            else:
                msg += " [" + str(fate) + " cells]"
            monitoring.to_log_and_console(msg, 1)

        return

    ############################################################
    #
    #
    #
    ############################################################

    def _add_embryo(self, prop, name, time_digits_for_cell_id=4):
        embryos = self.get_embryos()
        embryos[name] = cembryo.Embryo(prop, name=name, time_digits_for_cell_id=time_digits_for_cell_id)
        self.set_embryos(embryos)

    def add_embryos(self, embryofiles, parameters, time_digits_for_cell_id=4):
        """
        Method of class EmbryoSet that read property files to build atlases, ie copy properties of interest
        ('cell_lineage', 'cell_name',  'cell_contact_surface', 'cell_barycenter' and 'cell_volume').
        It may also perform some diagnosis (on 'name' and 'contact' properties) if required.
        It also temporally register the read atlases wrt to a reference one (the one one is reference
        is given in the parameters).

        :param embryofiles: a file name or a list of file names (pkl or xml files containing embryo properties)
        :param parameters: an instance of class AtlasParameters
        :param time_digits_for_cell_id: number of digits used to represent cell label in images; an unique
            cell id is made by concatening 'time' + 'cell label'
        :return: no return. It updates an instance variable
        """
        proc = "add_embryos"

        if not isinstance(parameters, cparameters.BaseParameters):
            monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                          + str(type(parameters)))
            sys.exit(1)

        if isinstance(embryofiles, str):
            prop = ioproperties.read_dictionary(embryofiles, inputpropertiesdict={}, verbose=(monitoring.debug > 0))
            if len(prop) > 0:
                name = embryofiles.split(os.path.sep)[-1]
                if name.endswith(".xml") or name.endswith(".pkl"):
                    name = name[:-4]
                if parameters.atlas_diagnosis is True:
                    diagnosis.diagnosis(prop, ['name', 'contact'], parameters,
                                        time_digits_for_cell_id=time_digits_for_cell_id)
                self._add_embryo(prop, name, time_digits_for_cell_id=time_digits_for_cell_id)
            del prop
        elif isinstance(embryofiles, list):
            if len(embryofiles) == 0:
                monitoring.to_log_and_console(str(proc) + ": empty embryo file list ?!")
                sys.exit(1)
            for f in embryofiles:
                prop = ioproperties.read_dictionary(f, inputpropertiesdict={}, verbose=(monitoring.debug > 0))
                if len(prop) > 0:
                    name = f.split(os.path.sep)[-1]
                    if name.endswith(".xml") or name.endswith(".pkl"):
                        name = name[:-4]
                    if parameters.atlas_diagnosis is True:
                        diagnosis.diagnosis(prop, ['name', 'contact'], parameters,
                                            time_digits_for_cell_id=time_digits_for_cell_id)
                    self._add_embryo(prop, name, time_digits_for_cell_id=time_digits_for_cell_id)
                del prop

        #
        # temporal alignment (done from the cell number)
        #
        if self.n_embryos() > 0:
            monitoring.to_log_and_console("... temporal alignment of lineages", 1)
            self.compute_temporal_alignment(mincells=parameters.temporal_alignment_min_cell_count,
                                            maxcells=parameters.temporal_alignment_max_cell_count,
                                            fate=parameters.temporal_alignment_fate)
        else:
            msg = proc + ": no read embryos ?!"
            monitoring.to_log_and_console(msg, 1)

        if parameters.atlas_diagnosis:
            _diagnosis_division_timepoint(self, parameters)

    ############################################################
    #
    #
    #
    ############################################################

    def add_symmetrical_embryos(self):
        proc = "add_symmetrical_embryos"
        embryos = self.get_embryos()
        embryo_names = list(embryos.keys())

        for a in embryo_names:
            if str(a)[0:len(self._symprefix)] == self._symprefix:
                msg = "embryo '" + str(a) + "' is already a symmetrized embryo. Skip it."
                monitoring.to_log_and_console(proc + ": " + msg)
                continue
            sym_name = self._symprefix + str(a)
            if sym_name in embryos:
                msg = "symmetrized embryo '" + str(sym_name) + "' is already in embryos. Skip it."
                monitoring.to_log_and_console(proc + ": " + msg)
                continue
            msg = "... create symmetrized embryo '" + str(sym_name) + " from " + str(a)
            monitoring.to_log_and_console(msg, 1)
            embryos[sym_name] = embryos[a].symmetrical_embryo(a)

        self.set_embryos(embryos)

    ############################################################
    #
    #
    #
    ############################################################

    def write_as_graph(self, cell_number=64):
        proc = "write_as_graph"
        embryos = self.get_embryos()
        for n in embryos:
            ncells, timerange, ininterval = embryos[n].get_timerange(cell_number, change_cell_number=True)
            if ncells is None:
                msg = "weird, requested cell number was " + str(cell_number) + ". "
                msg += "No cell range was found."
                monitoring.to_log_and_console(proc + ": " + msg)
                continue
            if ncells != cell_number:
                msg = "will produce graph at " + str(ncells) + " instead of " + str(cell_number) + " cells"
                monitoring.to_log_and_console(proc + ": " + msg)
            time = statistics.median_low(timerange)
            embryos[n].write_as_graph(time=time, atlasname=str(n))

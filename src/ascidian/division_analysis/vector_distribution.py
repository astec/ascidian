##############################################################
#
#       ASTEC package
#
#       Copyright INRIA 2021-2023
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Mar  7 nov 2023 14:14:21 CET
#
##############################################################
#
#
#
##############################################################

"""
Computation of (unit) vector distribution
"""

import os
import math
import copy

import numpy as np
import sklearn.preprocessing as skpreproc

from collections import deque

import astec.utils.common as common

monitoring = common.Monitoring()


########################################################################################
#
# sphere discretization
#
########################################################################################

def _size_center_from_radius(r):
    ir = math.ceil(r)
    s = int(2 * ir + 1 + 2)
    return s, ir + 1


def _sphere_matrix(r=3):
    """
    Draw an hollow discrete sphere in a 3D numpy array. Sphere points will have the 2 value.

    Parameters
    ----------
    r : float, default=3
        sphere radius
        radius =  2.0:    26 vectors, angle between neighboring vectors in [36.26, 60.0] degrees
        radius =  2.3:    38 vectors, angle between neighboring vectors in [26.57, 54.74] degrees
        radius =  2.5:    54 vectors, angle between neighboring vectors in [24.09, 43.09] degrees
        radius =  2.9:    66 vectors, angle between neighboring vectors in [18.43, 43.09] degrees
        radius =  3.0:    90 vectors, angle between neighboring vectors in [17.72, 43.09] degrees
        radius =  3.5:    98 vectors, angle between neighboring vectors in [15.79, 32.51] degrees
        radius =  3.7:   110 vectors, angle between neighboring vectors in [15.26, 32.51] degrees
        radius =  3.8:   134 vectors, angle between neighboring vectors in [14.76, 29.50] degrees
        radius =  4.0:   222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
        radius =  5.0:   222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
        radius = 10.0:   978 vectors, angle between neighboring vectors in [4.40, 10.58] degrees
        radius = 15.0:  2262 vectors, angle between neighboring vectors in [2.98, 6.93] degrees
        radius = 20.0:  4026 vectors, angle between neighboring vectors in [2.25, 5.16] degrees
        radius = 25.0:  6366 vectors, angle between neighboring vectors in [1.73, 4.01] degrees
        radius = 30.0:  9194 vectors, angle between neighboring vectors in [1.46, 3.40] degrees
        radius = 35.0: 12542 vectors, angle between neighboring vectors in [1.26, 2.90] degrees
        radius = 40.0: 16418 vectors, angle between neighboring vectors in [1.08, 2.53] degrees

    Returns
    -------
    a 3D numpy ndarray where outer sphere points have non-zero value, and the center coordinates

    """

    #
    # r is radius
    # matrix size will be 2*r + 1 (center) + 2 (margins)
    # center is r+1
    #
    s, c = _size_center_from_radius(r)
    m = np.zeros((s, s, s), dtype=np.int8)

    # fill the sphere
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            for k in range(m.shape[2]):
                di = float(i) - float(c)
                dj = float(j) - float(c)
                dk = float(k) - float(c)
                if math.sqrt(di * di + dj * dj + dk * dk) <= r:
                    m[i][j][k] = 1
    # set the border voxel to 2
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            for k in range(m.shape[2]):
                if m[i][j][k] < 1:
                    continue
                if i > 0 and m[i - 1][j][k] < 1:
                    m[i][j][k] = 2
                    continue
                if i < m.shape[0] - 1 and m[i + 1][j][k] < 1:
                    m[i][j][k] = 2
                    continue
                if j > 0 and m[i][j - 1][k] < 1:
                    m[i][j][k] = 2
                    continue
                if j < m.shape[1] - 1 and m[i][j + 1][k] < 1:
                    m[i][j][k] = 2
                    continue
                if k > 0 and m[i][j][k - 1] < 1:
                    m[i][j][k] = 2
                    continue
                if k < m.shape[2] - 1 and m[i][j][k + 1] < 1:
                    m[i][j][k] = 2
                    continue
    # erase the inner sphere
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            for k in range(m.shape[2]):
                if m[i][j][k] == 1:
                    m[i][j][k] = 0

    return m, (c, c, c)


def sphere_vector(r=3):
    """
    Returns the vectors pointing towards voxels of a discretized sphere.

    Parameters
    ----------
    ``r`` : sphere radius, default=3
        * radius =  2.0:    26 vectors, angle between neighboring vectors in [36.26, 60.0] degrees
        * radius =  2.3:    38 vectors, angle between neighboring vectors in [26.57, 54.74] degrees
        * radius =  2.5:    54 vectors, angle between neighboring vectors in [24.09, 43.09] degrees
        * radius =  2.9:    66 vectors, angle between neighboring vectors in [18.43, 43.09] degrees
        * radius =  3.0:    90 vectors, angle between neighboring vectors in [17.72, 43.09] degrees
        * radius =  3.5:    98 vectors, angle between neighboring vectors in [15.79, 32.51] degrees
        * radius =  3.7:   110 vectors, angle between neighboring vectors in [15.26, 32.51] degrees
        * radius =  3.8:   134 vectors, angle between neighboring vectors in [14.76, 29.50] degrees
        * radius =  4.0:   222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
        * radius =  5.0:   222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
        * radius = 10.0:   978 vectors, angle between neighboring vectors in [4.40, 10.58] degrees
        * radius = 15.0:  2262 vectors, angle between neighboring vectors in [2.98, 6.93] degrees
        * radius = 20.0:  4026 vectors, angle between neighboring vectors in [2.25, 5.16] degrees
        * radius = 25.0:  6366 vectors, angle between neighboring vectors in [1.73, 4.01] degrees
        * radius = 30.0:  9194 vectors, angle between neighboring vectors in [1.46, 3.40] degrees
        * radius = 35.0: 12542 vectors, angle between neighboring vectors in [1.26, 2.90] degrees
        * radius = 40.0: 16418 vectors, angle between neighboring vectors in [1.08, 2.53] degrees

    Returns
    -------
    A numpy array of shape (N, 3) of unit vectors

    """
    # m will have non-zero value at sphere border
    m, c = _sphere_matrix(r=r)
    v = []
    angles = []
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            for k in range(m.shape[2]):
                if m[i][j][k] == 0:
                    continue
                di = float(i) - float(c[0])
                dj = float(j) - float(c[1])
                dk = float(k) - float(c[2])
                norm = math.sqrt(di * di + dj * dj + dk * dk)
                u = np.array([di / norm, dj / norm, dk / norm])
                v += [u]
                for di in range(-1, 2):
                    for dj in range(-1, 2):
                        for dk in range(-1, 2):
                            if di == 0 and dj == 0 and dk == 0:
                                continue
                            if m[i + di][j + dj][k + dk] == 0:
                                continue
                            v = np.array([(i + di - c[0]), (j + dj - c[1]), (k + dk - c[2])])
                            angles += [math.acos(np.dot(u, v / np.linalg.norm(v)))]

    v = np.array(v)

    monitoring.to_log_and_console("      ... direction distribution build with r = " + str(r), 2)
    monitoring.to_log_and_console("          vectors = " + str(v.shape[0]), 2)
    min_angles = min(angles) * 180.0 / np.pi
    max_angles = max(angles) * 180.0 / np.pi
    msg = "          angles between adjacent vectors in [{:.2f}, {:.2f}]".format(min_angles, max_angles)
    monitoring.to_log_and_console(msg, 2)

    return v


def _sphere_distribution_support(sphere_radius, WithColor=None, verbose=True):
    """

    :param sphere_radius:
    :return:
    a array of dictionaries, each dictionary having the following keys
    - 'voxel': the 3D point in the 3D array correponding to the vector
    - 'vector': the unit vector (joigning the 'voxel' and the array center)
    - 'value': the distribution value

    """

    m, c = _sphere_matrix(sphere_radius)

    #
    # points of the outer sphere have a value of 2, all other points are at 0
    #

    direction_distribution = []

    angles = []
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            for k in range(m.shape[2]):
                if m[i][j][k] == 0:
                    continue
                p = {'voxel': (i, j, k)}
                v = np.array([(i - c[0]), (j - c[1]), (k - c[2])])
                p['vector'] = v / np.linalg.norm(v)
                p['value'] = 0.0
                if WithColor:
                    p['color'] = 0
                direction_distribution += [p]
                #
                # error evaluation: angles between neighboring vectors
                #
                for di in range(-1, 2):
                    for dj in range(-1, 2):
                        for dk in range(-1, 2):
                            if di == 0 and dj == 0 and dk == 0:
                                continue
                            if m[i + di][j + dj][k + dk] == 0:
                                continue
                            v = np.array([(i + di - c[0]), (j + dj - c[1]), (k + dk - c[2])])
                            angles += [math.acos(np.dot(p['vector'], v / np.linalg.norm(v)))]

    if verbose:
        msg = "      ... direction distribution support build with r = " + str(sphere_radius)
        monitoring.to_log_and_console(msg, 2)
        monitoring.to_log_and_console("          vectors = " + str(len(direction_distribution)), 2)
        min_angles = min(angles) * 180.0 / np.pi
        max_angles = max(angles) * 180.0 / np.pi
        msg = "          angles between adjacent vectors in [{:.2f}, {:.2f}]".format(min_angles, max_angles)
        monitoring.to_log_and_console(msg, 2)

    return direction_distribution


def _fill_distribution(distribution, vectors, weights, kernel_sigma=0.15, verbose=True):
    # print("filling distribution...")
    #
    # distribution support: it is an array of dictionaries, each dictionary having the following keys
    # - 'voxel': the 3D point in the 3D array corresponding to the vector
    # - 'vector': the unit vector (joigning the 'voxel' and the array center)
    # - 'value': the distribution value
    #
    for i in range(len(distribution)):
        distribution[i]['value'] = 0.0

    contribution_list = np.zeros(len(distribution))

    #
    # initialisation
    #
    div = 2 * kernel_sigma * kernel_sigma
    # do not compute contribution for angles > 3 * sigma
    angle_threshold = 3.0 * kernel_sigma

    if verbose:
        msg = "      ... build direction distribution from {:d} vectors".format(len(vectors))
        monitoring.to_log_and_console(msg, 2)

    for k, v in enumerate(vectors):
        #
        # get contributions for vector
        #
        plist = []
        sum_positive_contributions = 0.0

        for i, p in enumerate(distribution):
            sp = p['vector'][0] * v[0] + p['vector'][1] * v[1] + p['vector'][2] * v[2]
            sp = max(min(sp, 1.0), -1.0)
            #
            # positive contribution
            # math.acos(x) return the arc cosine of x, in radians. The result is between 0 and pi.
            #
            angle = math.acos(sp)
            if angle < angle_threshold:
                contrib = math.exp(- angle * angle / div)
                sum_positive_contributions += contrib
                plist += [(i, contrib)]

        #
        # make sure the sum of contributions of vectors[k] is weights[k]
        #
        for (i, contrib) in plist:
            contribution_list[i] += weights[k] * contrib / sum_positive_contributions

    #
    # set distribution values on distribution support
    # set the maximum of distribution to 1.0
    #
    max_contribution = max(contribution_list)
    for i, v in enumerate(contribution_list):
        distribution[i]['value'] = v / max_contribution

    return distribution


def _array_values_from_distribution(distribution, sphere_radius):
    s, c = _size_center_from_radius(sphere_radius)
    values = np.zeros((s, s, s))
    for p in distribution:
        values[p['voxel'][0]][p['voxel'][1]][p['voxel'][2]] = p['value']
    return values


def _array_color_from_distribution(distribution, sphere_radius):
    s, c = _size_center_from_radius(sphere_radius)
    values = np.ones((s, s, s), dtype=int) * -1
    for p in distribution:
        values[p['voxel'][0]][p['voxel'][1]][p['voxel'][2]] = p['color']
    return values


########################################################################################
#
#
#
########################################################################################


def _print_distribution(d):
    for i, p in enumerate(d):
        msg = "#{:4d}".format(i)
        msg += " - "
        msg += "({:5.2f}, {:5.2f}, {:5.2f})".format(p['vector'][0], p['vector'][1], p['vector'][2])
        msg += " - "
        msg += "{:5.3f}".format(p['value'])
        print(msg)


########################################################################################
#
#
#
########################################################################################


def _write_3d_array(f, varname, a, isInt=False, length=2, file_suffix=None):
    form = "{:1." + str(length) + "f}"
    if isInt:
        form = "{:d}"
    f.write(str(varname))
    # if file_suffix is not None:
    #    f.write(str(file_suffix))
    f.write(" = np.array(")
    f.write("[")
    for z in range(a.shape[0]):
        f.write("[")
        for y in range(a.shape[1]):
            f.write("[")
            for x in range(a.shape[2]):
                f.write(form.format(a[z][y][x]))
                if x < a.shape[2] - 1:
                    f.write(", ")
            f.write("]")
            if y < a.shape[1] - 1:
                f.write(", \n")
        f.write("]")
        if z < a.shape[0] - 1:
            f.write(", \n")
    f.write("])\n")


########################################################################################
#
#
#
########################################################################################
def propagate_color(i, j, k, _array, _color_array):
    """
    Takes voxel indexesn array of values and array of color values.
    Fill it recursively by going in all directions
    Discards when value becomes increasing.(so theoratically stops at local minima); points already filled; points with value=0

    #self._array, self._color_array
    """
    # for a local maxima in self._distribution (array of dictionnaries) :
    #  propagate color in all directions on sphere till a voxel colored is met or the value of voxel in this direction is increasing
    list_voxels_to_add_to_queue = []
    for di in range(-1, 2):
        for dj in range(-1, 2):
            for dk in range(-1, 2):
                # print("visiting neighbors")
                if di == 0 and dj == 0 and dk == 0:
                    continue
                ni, nj, nk = i + di, j + dj, k + dk

                if ni < 0 or nj < 0 or nk < 0 or ni >= _array.shape[0] or nj >= _array.shape[1] or nk >= _array.shape[
                    2]:
                    # print("ignoring not part of array")
                    continue

                if _color_array[ni, nj, nk] == -1:
                    # print("ignoring not part of sphere:",(ni,nj,nk))
                    continue

                if _color_array[ni, nj, nk] > 0:
                    # print("ignoring coloring border: ",(ni,nj,nk),_array_color[ni,nj,nk],"exchg color:",_array_color[i,j,k] )
                    continue
                _color_array[ni, nj, nk] = _color_array[i, j, k]
                # print("voxel to propagate:", ni,nj,nk, "color:",_array_color[ni,nj,nk])
                list_voxels_to_add_to_queue.append((ni, nj, nk))
                # print("propagated to neighbors of:",(i,j,k), (ni,nj,nk),"color:",_color_array[i][j][k])
    return list_voxels_to_add_to_queue, _color_array


def distance_matrix_3d_vectors(vectors):
    vectors = np.array(vectors)
    num_vectors = len(vectors)
    distance_matrix = np.zeros((num_vectors, num_vectors))
    max_distance = 0.0

    for i in range(num_vectors):
        for j in range(num_vectors):
            if i == j:
                distance_matrix[i][j] = 0.0
                continue
            if i > j:
                continue

            dot_product = np.dot(vectors[i], vectors[j])
            magnitudes = np.linalg.norm(vectors[i]) * np.linalg.norm(vectors[j])
            cosine_similarity = dot_product / magnitudes

            # Using arccos to calculate the angle in radians
            angle_radians = np.arccos(cosine_similarity)

            # Convert the angle to a distance metric
            distance_matrix[i][j] = angle_radians
            distance_matrix[j][i] = angle_radians
            max_distance = max(max_distance, angle_radians)

    return distance_matrix, max_distance


########################################################################################
#
# sphere discretization
#
########################################################################################


class VectorDistribution(object):
    def __init__(self, vectors=None, weights=None, vector_labels=None, sphere_radius=20, kernel_sigma=0.15,
                 add_opposite=False, WithColor=False,
                 verbose=True):
        """
        Distribution of vectors on a sphere, estimated by kernel density estimation with a gaussian kernel

        Parameters
        ----------
        vectors: vectors, list of list or np.ndarray of shape (N, 3) where N is the number of sample vectors
        weights: list of weights or np.ndarray of shape (N)
            if None, equal weights (1) are used
        sphere_radius: default=20. Size of the distribution support
            - radius = 3:     90 vectors, angle between neighboring vectors in [17.72, 43.09] degrees
            - radius = 5:    222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
            - radius = 10:   978 vectors, angle between neighboring vectors in [4.40, 10.58] degrees
            - radius = 15:  2262 vectors, angle between neighboring vectors in [2.98, 6.93] degrees
            - radius = 20:  4026 vectors, angle between neighboring vectors in [2.25, 5.16] degrees
            - radius = 25:  6366 vectors, angle between neighboring vectors in [1.73, 4.01] degrees
            - radius = 30:  9194 vectors, angle between neighboring vectors in [1.46, 3.40] degrees
            - radius = 35: 12542 vectors, angle between neighboring vectors in [1.26, 2.90] degrees
            - radius = 40: 16418 vectors, angle between neighboring vectors in [1.08, 2.53] degrees
        kernel_sigma: default=0.15
            Standard deviation of the gaussian kernel (in radians)
        add_opposite: if ``True``, add opposite vectors to the distribution.
            It comes to compute a distribution of directions. As a consequence,
            the number of distribution maxima will be even, and each successive pair of maxima
            will be a couple of opposite vectors.

        Returns
        -------
        self: instance of the distribution

        Examples
        --------
        >>> import ascidian.core.vector_distribution as vdist
        >>> import numpy as np
        >>> import scipy
        >>> f = scipy.stats.multivariate_normal.rvs([2,0,2], [[1,0,0],[0,1,0],[0,0,1]], 500, random_state=0)
        >>> g = scipy.stats.multivariate_normal.rvs([-1,-3,1], [[1,0,0],[0,1,0],[0,0,1]], 400, random_state=0)
        >>> h = np.concatenate((f,g))
        >>> d = vdist.VectorDistribution(vectors=h,sphere_radius=10)
        >>> d.maxima()

        References
        ----------
        .. [Kernel_density_estimation] https://en.wikipedia.org/wiki/Kernel_density_estimation
        """
        #
        # class values
        #
        # inputs
        self._sample_vectors = None
        self._sample_weights = None
        self._vector_labels = None
        # parameters
        self._sphere_radius = 20
        self._kernel_sigma = 0.15

        # distribution
        self._distribution = None
        self._array = None
        self._color_array = None
        # Colored dir dict
        self._colored_direction_dict = {}

        # max distance between modes
        self.d_max = 0

        #
        # analyse samples
        #
        local_vectors = None
        if vectors is not None:
            if isinstance(vectors, np.ndarray):
                local_vectors = vectors
            elif isinstance(vectors, list):
                local_vectors = np.array(vectors)
            else:
                msg = "VectorDistribution: vectors type '" + str(type(vectors)) + "' not handled yet"
                monitoring.to_log_and_console(msg)
                return
            if local_vectors.shape[1] != 3:
                msg = "VectorDistribution: vectors shape[1] = " + str(local_vectors.shape[1])
                msg += ", should be 3"
                monitoring.to_log_and_console(msg)
                return
        local_weights = None
        if weights is not None:
            if vectors is None:
                msg = "VectorDistribution: there weights but no vectors ?!"
                monitoring.to_log_and_console(msg)
                return
            if isinstance(weights, np.ndarray):
                local_weights = weights
            elif isinstance(vectors, list):
                local_weights = np.array(weights)
            else:
                msg = "VectorDistribution: weights type '" + str(type(weights)) + "' not handled yet"
                monitoring.to_log_and_console(msg, 2)
                return
            if len(local_weights.shape) != 1 and local_weights.shape[1] > 1:
                msg = "VectorDistribution: weights shape[1] = " + str(local_weights.shape[1])
                msg += ", should be 1"
                monitoring.to_log_and_console(msg)
                return
            if local_weights.shape[0] != local_vectors.shape[0]:
                msg = "VectorDistribution: weights shape[0] = " + str(local_weights.shape[0])
                msg += ", should be equal to vectors shape[0] = " + str(local_vectors.shape[0])
                monitoring.to_log_and_console(msg)
                return

        if vector_labels is not None:
            self._vector_labels = copy.deepcopy(vector_labels)
        else:
            msg = "Vector labels is None "
            monitoring.to_log_and_console(msg)
        #
        # build the distribution =
        # a array of dictionaries, each dictionary having the following keys
        #     - 'voxel': the 3D point in the 3D array corresponding to the vector
        #     - 'vector': the unit vector (joining the 'voxel' and the array center)
        #     - 'value': the distribution value
        # print("distribution initiation")
        self._distribution = _sphere_distribution_support(sphere_radius=sphere_radius, WithColor=WithColor,
                                                          verbose=verbose)
        self._sphere_radius = sphere_radius

        #
        # nothing to do
        #
        if local_vectors is None:
            return

        #
        # keep samples, in case we want
        # - to add vectors in a dedicated method (not done yet)
        # - change parameters (sphere radius, kernel sigma)
        #
        if add_opposite is False:
            # normalize vectors to get unit vectors
            self._sample_vectors = skpreproc.normalize(local_vectors)
            if local_weights is not None:
                self._sample_weights = copy.deepcopy(local_weights)
            else:
                self._sample_weights = np.ones(local_vectors.shape[0])
        else:
            # normalize vectors to get unit vectors
            self._sample_vectors = skpreproc.normalize(np.concatenate((local_vectors, -local_vectors)))
            if local_weights is not None:
                self._sample_weights = copy.deepcopy(np.concatenate((local_weights, local_weights)))
            else:
                self._sample_weights = np.ones(2 * local_vectors.shape[0])

        #
        # compute distribution
        #
        self._distribution = _fill_distribution(self._distribution, self._sample_vectors, self._sample_weights,
                                                kernel_sigma=kernel_sigma, verbose=verbose)
        return

    def maxima(self, maxima_threshold=0.5, maxima_number=None, verbose=True):
        """
        Get maxima of the distribution

        Parameters
        ----------
        maxima_threshold: only select maxima with values above the threshold.
            Distribution has been normalized so that its maximal value is 1.0.
        maxima_number: maximal number of maxima to be returned.
            If None, all maxima (above the threshold) are returned
        verbose: ``True`` or ``False``

        Returns
        -------
        vectors: a numpy array of shape (N, 3) of vectors with local maximal distribution value
            sorted in distribution value decreasing order.
            If `add_opposite` have been set to `True` when building the distribution, each pair
            of successive vectors are opposite vectors.
        values: a numpy array of shape (N) of local maximal distribution values associated to the vectors
        """

        self._array = _array_values_from_distribution(self._distribution, self._sphere_radius)
        distribution_maxima = []  # list with all local maximas, will be sorted later
        for p in self._distribution:
            #
            # hard threshold on maxima
            # recall the maximum of the distribution has to set to 1.0
            #
            if p['value'] <= maxima_threshold:
                continue
            i = p['voxel'][0]
            j = p['voxel'][1]
            k = p['voxel'][2]
            if self._array[i][j][k] == 0.0:
                continue
            if self._array[i][j][k] < self._array[i - 1][j - 1][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i][j - 1][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j - 1][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i][j][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j + 1][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i][j + 1][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j + 1][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j - 1][k]:
                continue
            if self._array[i][j][k] < self._array[i][j - 1][k]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j - 1][k]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j][k]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j][k]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j + 1][k]:
                continue
            if self._array[i][j][k] < self._array[i][j + 1][k]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j + 1][k]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j - 1][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i][j - 1][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j - 1][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i][j][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j + 1][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i][j + 1][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j + 1][k + 1]:
                continue
            distribution_maxima += [p]

        #
        #
        #
        if len(distribution_maxima) == 0:
            return np.array([]), np.array([])

        #
        # sort maxima in descending  order
        #
        distribution_maxima = sorted(distribution_maxima, reverse=True, key=lambda x: x['value'])

        if verbose:
            msg = "      ... found {:d} distribution maxima".format(len(distribution_maxima))
            monitoring.to_log_and_console(msg, 2)

        selected_maxima = len(distribution_maxima)
        if maxima_number is not None and isinstance(maxima_number, int):
            if 0 < maxima_number < len(distribution_maxima):
                selected_maxima = maxima_number

        vectors_maxima = []
        values_maxima = []
        voxels_maxima = []
        for i, p in enumerate(distribution_maxima[:selected_maxima]):
            voxels_maxima += [p['voxel']]
            vectors_maxima += [p['vector']]
            values_maxima += [p['value']]
            if 'color' in p:
                # _distribution is an array of dictionnaries, in each element you have: 'voxel','vector', 'value' and 'color'
                # Update 'color' in self._distribution for the corresponding voxel
                for d in self._distribution:
                    if d['voxel'] == p['voxel']:
                        d['color'] = int(i + 1)
        # create _array_color, initialize with local maximas values
        self._color_array = _array_color_from_distribution(self._distribution, self._sphere_radius)

        # compute distance matrix between modes ()local maximas
        dmatrix, d_max = distance_matrix_3d_vectors(vectors_maxima)
        self.d_max = d_max

        return np.array(vectors_maxima), np.array(values_maxima), np.array(voxels_maxima)

    def figure(self, filename='figures_vector_distribution', file_suffix=None, output_dir=None, cmap='autumn'):
        """

        Parameters
        ----------
        filename:
        file_suffix:
        output_dir:
        cmap:

        Returns
        -------

        Example
        -------
        >>> import ascidian.core.vector_distribution as vdist
        >>> import scipy
        >>> f = scipy.stats.multivariate_normal.rvs([2,-2,2], [[1,0,0],[0,1,0],[0,0,1]], 100)
        >>> d = vdist.VectorDistribution(vectors=f,sphere_radius=4)
        >>> d.figure(cmap='autumn')
        """

        proc = "VectorDistribution.figure"

        self._array = _array_values_from_distribution(self._distribution, self._sphere_radius)

        local_filename = filename

        localfile_suffix = None
        if file_suffix is not None and isinstance(file_suffix, str) and len(file_suffix) > 0:
            localfile_suffix = '_' + file_suffix
        if localfile_suffix is not None:
            local_filename += localfile_suffix
        local_filename += '.py'

        if output_dir is not None and isinstance(output_dir, str):
            if not os.path.isdir(output_dir):
                if not os.path.exists(output_dir):
                    os.makedirs(output_dir)
                else:
                    msg = "not a directory"
                    monitoring.to_log_and_console(proc + ": '" + str(output_dir) + "' is not a directory ?!")
            if os.path.isdir(output_dir):
                local_filename = os.path.join(output_dir, local_filename)

        sphere, center = _sphere_matrix(self._sphere_radius)

        f = open(local_filename, "w")

        f.write("import numpy as np\n")
        f.write("import matplotlib.pyplot as plt\n")

        f.write("\n")
        f.write("savefig = True\n")
        f.write("\n")
        _write_3d_array(f, "sphere", sphere, file_suffix=localfile_suffix)
        f.write("\n")
        _write_3d_array(f, "values", self._array, file_suffix=localfile_suffix)
        f.write("\n")
        f.write("cmap = plt.get_cmap('" + str(cmap) + "')\n")
        f.write("colors = cmap(values)\n")
        f.write("\n")

        # and plot everything
        f.write("ax = plt.figure().add_subplot(projection='3d')\n")
        f.write("ax.voxels(sphere, facecolors=colors, edgecolors='k', linewidth=0.5)\n")
        f.write("ax.set(xlabel='x', ylabel='y', zlabel='z')\n")
        f.write("ax.set_aspect('equal')\n")

        f.write("\n")
        f.write("if savefig:\n")
        f.write("    plt.savefig('vector_distribution")
        if file_suffix is not None:
            f.write(file_suffix)
        f.write("'" + " + '.png')\n")
        f.write("    plt.show()\n")
        f.write("    plt.close()\n")

        f.write("else:\n")
        f.write("    plt.show()\n")
        f.write("    plt.close()\n")
        f.close()

    def color_propagation(self, maxima_threshold=0.5, maxima_number=None, verbose=True):
        """
        Propagates colors to neighboring voxels starting from local maxima.

        Args:
            maxima_threshold (float): Threshold for identifying local maxima.
            maxima_number (int): Maximum number of local maxima to consider.
            verbose (bool): Whether to print verbose output.

        Returns:
        """

        # Get the local maxima
        # The maxima() will initialise local maximas "color" in _distribution
        vectors_maxima, values_maxima, voxels_maxima = self.maxima(maxima_threshold, maxima_number, verbose)

        queue = deque(voxels_maxima)

        while queue:
            # print("queue:", queue)

            list_voxels_to_add_to_queue = []

            FO_voxel = queue.popleft()  # the first out so highest level voxels
            i = FO_voxel[0]
            j = FO_voxel[1]
            k = FO_voxel[2]

            # needs to be the highest !!!!
            flood_level = self._array[i][j][k]

            # print("array shape:", self._array[i][j][k])
            # print("flood level",flood_level)

            # I need to check all queu elements higher than flood level
            while self._array[i][j][k] >= flood_level:

                last_voxels, self._color_array = propagate_color(i, j, k, self._array, self._color_array)
                if len(last_voxels) != 0:
                    list_voxels_to_add_to_queue.extend(last_voxels)

                if not queue:
                    # queue is empty
                    break
                # print("voxel:",FO_voxel)
                FO_voxel = queue.popleft()
                i = FO_voxel[0]
                j = FO_voxel[1]
                k = FO_voxel[2]

            ##
            # print("one iteration done,appending to queue")
            # add to queue after poping if not propagated
            if self._array[i][j][k] < flood_level:
                queue.appendleft(FO_voxel)
            #
            #  Add the propagated voxels to the queue : ?if list is empty?
            queue.extendleft(list_voxels_to_add_to_queue)
            # sort queue
            queue = deque(sorted(queue, key=lambda voxel: self._array[voxel[0]][voxel[1]][voxel[2]], reverse=True))
            # print("queue:", queue)

        # Fill p['color']: distribution support
        for p in self._distribution:
            voxel = p['voxel']
            p['color'] = self._color_array[voxel[0]][voxel[1]][voxel[2]]

        unique_colors, nb_vectors_per_maxima = np.unique(self._color_array, return_counts=True)
        # -1 is not part of sphere

        # In verbose add error message if this method is called without WithColor option when object created

        return vectors_maxima, values_maxima, nb_vectors_per_maxima[1:]

    def color_figure(self, filename='figures_color_distribution', file_suffix=None, output_dir=None, cmap='autumn'):
        """

        Parameters
        ----------
        filename:
        file_suffix:
        output_dir:
        cmap:

        Returns
        -------

        Example
        -------
        >>> import ascidian.core.vector_distribution as vdist
        >>> import scipy
        >>> f = scipy.stats.multivariate_normal.rvs([2,-2,2], [[1,0,0],[0,1,0],[0,0,1]], 100)
        >>> d = vdist.VectorDistribution(vectors=f,sphere_radius=4, WithColor=True)
        >>> d.color_propagation(vectors=f,sphere_radius=4, WithColor=True)
        >>> d.color_figure(cmap='autumn')
        """

        proc = "ColorDistribution.figure"

        self._array = _array_color_from_distribution(self._distribution, self._sphere_radius)

        local_filename = filename

        localfile_suffix = None
        if file_suffix is not None and isinstance(file_suffix, str) and len(file_suffix) > 0:
            localfile_suffix = '_' + file_suffix
        if localfile_suffix is not None:
            local_filename += localfile_suffix
        local_filename += '.py'

        if output_dir is not None and isinstance(output_dir, str):
            if not os.path.isdir(output_dir):
                if not os.path.exists(output_dir):
                    os.makedirs(output_dir)
                else:
                    msg = "not a directory"
                    monitoring.to_log_and_console(proc + ": '" + str(output_dir) + "' is not a directory ?!")
            if os.path.isdir(output_dir):
                local_filename = os.path.join(output_dir, local_filename)

        sphere, center = _sphere_matrix(self._sphere_radius)

        f = open(local_filename, "w")

        f.write("import numpy as np\n")
        f.write("import matplotlib.pyplot as plt\n")

        f.write("\n")
        f.write("savefig = True\n")
        f.write("\n")
        _write_3d_array(f, "sphere", sphere, file_suffix=localfile_suffix)
        f.write("\n")
        _write_3d_array(f, "values", self._color_array, isInt=True, file_suffix=localfile_suffix)
        f.write("\n")
        f.write("cmap = plt.get_cmap('" + str(cmap) + "')\n")
        f.write("colors = cmap(values)\n")
        f.write("\n")

        # and plot everything
        f.write("ax = plt.figure().add_subplot(projection='3d')\n")
        f.write("ax.voxels(sphere, facecolors=colors, edgecolors='k', linewidth=0.5)\n")
        f.write("ax.set(xlabel='x', ylabel='y', zlabel='z')\n")
        f.write("ax.set_aspect('equal')\n")

        f.write("\n")
        f.write("if savefig:\n")
        f.write("    plt.savefig('color_propagation")
        if file_suffix is not None:
            f.write(file_suffix)
        f.write("'" + " + '.png')\n")
        f.write("    plt.show()\n")
        f.write("    plt.close()\n")

        f.write("else:\n")
        f.write("    plt.show()\n")
        f.write("    plt.close()\n")
        f.close()

    def get_color_per_maxima(self):
        """
        for each of the orientations, assign to each a color, get using np.unique frequencies
        of ordered maximas
        """
        vector_colors = []

        for vector, v_label in zip(self._sample_vectors, self._vector_labels):
            max_cos = -1  # Initialize maximum cosine similarity
            vector_color = None  # Initialize color for the vector

            # Iterate through each vector in the distribution support
            for p in self._distribution:
                dist_vector = p['vector']

                # Calculate cosine similarity between the current vector and the distribution vector
                cos_similarity = np.dot(vector, dist_vector) / (np.linalg.norm(vector) * np.linalg.norm(dist_vector))

                # Check if the current cosine similarity is greater than the maximum so far
                if cos_similarity > max_cos:
                    max_cos = cos_similarity
                    vector_color = p['color']
            vector_colors.append(vector_color)
        unique_colors, nb_vectors_per_maxima = np.unique(vector_colors, return_counts=True)
        return nb_vectors_per_maxima

    def color_vectors(self):
        """
        Assigns colors to vectors based on the closest vector found in the distribution support.

        Fills the 'color' field in the distribution support dictionary using the color propagation method.
        Finds the closest vector in the distribution support to each vector in self._sample_vectors and assigns its color.

        Returns:
            direction: A dictionary containing vectors and their corresponding assigned colors.
        """
        for vector, v_label in zip(self._sample_vectors, self._vector_labels):
            max_cos = -1  # Initialize maximum cosine similarity
            vector_color = None  # Initialize color for the vector

            # Iterate through each vector in the distribution support
            for p in self._distribution:
                dist_vector = p['vector']

                # Calculate cosine similarity between the current vector and the distribution vector
                cos_similarity = np.dot(vector, dist_vector) / (np.linalg.norm(vector) * np.linalg.norm(dist_vector))

                # Check if the current cosine similarity is greater than the maximum so far
                if cos_similarity > max_cos:
                    max_cos = cos_similarity
                    vector_color = p['color']  # Assign the color of the closest vector

                # Assign the color of the closest vector to the current vector: array of dictionnaries/dictionnary with cell name as key
                self._colored_direction_dict[v_label] = {'vector': vector, 'color': vector_color}

        return self._colored_direction_dict

    def figure_colored_directions(self, filename='figures_color_distribution', file_suffix=None, output_dir=None,
                                  cell_name=None, distance_between_modes=None, kernel_size=None,
                                  cmap='autumn'):

        proc = "ColorDirection.figure"
        local_filename = filename

        localfile_suffix = None
        if file_suffix is not None and isinstance(file_suffix, str) and len(file_suffix) > 0:
            localfile_suffix = '_' + file_suffix
        if localfile_suffix is not None:
            local_filename += localfile_suffix
        local_filename += '.py'

        if output_dir is not None and isinstance(output_dir, str):
            if not os.path.isdir(output_dir):
                if not os.path.exists(output_dir):
                    os.makedirs(output_dir)
                else:
                    msg = "not a directory"
                    monitoring.to_log_and_console(proc + ": '" + str(output_dir) + "' is not a directory ?!")
            if os.path.isdir(output_dir):
                local_filename = os.path.join(output_dir, local_filename)

        f = open(local_filename, "w")

        f.write("import numpy as np\n")
        f.write("import matplotlib.pyplot as plt\n")

        f.write("\n")
        f.write("# Define the data\n")
        f.write("data_labels = []\n")
        f.write("data_vectors = []\n")
        f.write("data_color_idx = []\n")

        vector = []
        color = []
        labels = []
        for individual, data in self._colored_direction_dict.items():
            vector.append(data['vector'].tolist())
            color.append(data['color'])
            labels.append(individual)

        f.write(f"data_labels={labels}\n")
        f.write(f"data_vectors={vector}\n")
        f.write(f"data_color_idx={color}\n")

        f.write("# Plotting\n")
        f.write("fig = plt.figure()\n")
        f.write("ax = fig.add_subplot(111, projection='3d')\n\n")

        f.write("colors = ['red', 'blue', 'green', 'orange', 'purple', 'cyan', 'magenta', 'yellow', 'brown', 'pink']\n")
        f.write("color_mapped =  [colors[i % len(colors)] for i in data_color_idx]\n\n")

        f.write("for i, vector in enumerate(data_vectors):\n")
        f.write("    x, y, z = vector\n")
        f.write("    ax.quiver(0, 0, 0, x, y, z, color=color_mapped[i])\n")
        f.write("    ax.text(x, y, z, str(data_labels[i]), fontsize=12, color=color_mapped[i])\n\n")

        f.write("ax.set_xlabel('X')\n")
        f.write("ax.set_ylabel('Y')\n")
        f.write("ax.set_zlabel('Z')\n\n")

        f.write("max_val = max(max(vector) for vector in data_vectors)\n")
        f.write("ax.set_xlim([-max_val, max_val])\n")
        f.write("ax.set_ylim([-max_val, max_val])\n")
        f.write("ax.set_zlim([-max_val, max_val])\n")
        f.write(f"plt.title('Cell {cell_name} Clustered Division Directions') "
                f"#Distance Between Modes = {distance_between_modes}\n"
                f"#Kernel Size = {kernel_size}')\n")
        f.write("plt.show()\n")
        f.write("plt.savefig('color_direction")
        if file_suffix is not None:
            f.write(file_suffix)
        f.write(".png')\n")
        f.close()

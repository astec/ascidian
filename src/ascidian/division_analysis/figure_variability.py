# There will be plots to analyse division direction variability and heterechronies
import numpy as np
import pickle as pkl
import os
import scipy as sp
import itertools
from scipy.stats import spearmanr, pearsonr
import math

# local import
from ascidian.division_analysis.division_analysis_tools import (compute_std_t, compute_avg_division_time, fate_division_color,
                                                   compute_std_distance, get_id_at_top_lineage, get_ids_at_division,
                                                   align_time, angle_vector_to_direction, sort_mother_cells,
                                                   determine_fate_group, angle_btw_vectors, average_t_for_cells,
                                                   compare_division_times,
                                                   surface_volume, sphercity, elongation,
                                                   returns_merged_daughters_residues)
import ascidian.ascidian.name as uname
import ascidian.core.temporal_alignment as utimes

# fates groups
epid_group = ['Midline Tail Epidermis', 'Head Epidermis', 'Midline Tail Epidermis', 'Medio-Lateral Tail Epidermis',
              'Lateral Tail Epidermis']
endo_group = ['Anterior Head Endoderm', 'Posterior Head Endoderm', '2nd Endodermal Lineage', '1st Endodermal Lineage']
ns_group = ['Posterior Lateral Neural Plate', 'Anterior Dorsal Neural Plate', 'Posterior Ventral Neural Plate',
            'Anterior Ventral Neural Plate', 'Posterior Dorsal Neural Plate']
meso_group = ['2nd Lineage, Notochord', '1st Lineage, Tail Muscle', 'Trunk Lateral Cell', '2nd Lineage, Tail Muscle',
              '1st Lineage, Notochord', 'Mesenchyme', 'Trunk Ventral Cell']
fate_group_names = ['Epid.', 'End.', 'NS', 'Mesoderm']
fate_groups = [epid_group, endo_group, ns_group, meso_group]

colors = {'Epid.': 'red', 'NS': 'blue', 'Mesoderm': 'green', 'End.': 'purple'}



############
#### dispersion of cell vs dispersion mother

def dispersion_of_cell_vs_mother(divisions_properties, generations, output_folder=''):
    """

    Parameters
    ----------
    divisions_properties
    generations
    output_folder

    Returns
    -------

    """
    for gen in generations:
        list_std_a_cell = []
        list_std_a_mother = []
        cell_names = []
        fates = []
        # filter gen
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == gen}
        gen_divisions = dict(sorted(gen_divisions.items()))

        for cell in gen_divisions.keys():
            if (uname.get_mother_name(cell+'_')[:-1] in divisions_properties and
                divisions_properties[cell]['nb_of_measurements']>5 and
                 divisions_properties[uname.get_mother_name(cell+'_')[:-1]]['nb_of_measurements']>5):
                list_std_a_cell.append(divisions_properties[cell]['std_from_mean_directions'])
                list_std_a_mother.append(divisions_properties[uname.get_mother_name(cell+'_')[:-1]]['std_from_mean_directions'])
                cell_names.append(cell)
                f = determine_fate_group(divisions_properties[cell].get('cell_fate', None), True)
                if isinstance(f, list):
                    f = f[0]
                fates.append(f)
        # call fucntion to write script to plot
        write_plot_script_cell_mother_dispersion_dir(output_folder, gen, list_std_a_cell, list_std_a_mother,
                                                     cell_names, fates)
def write_plot_script_cell_mother_dispersion_dir(output_folder, gen, list_std_a_cell, list_std_a_mother, cell_names, fates):
    script_content = f"""
import os
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import matplotlib.patches as mpatches
from scipy.stats import pearsonr

list_std_a_mother = {list_std_a_mother}
list_std_a_cell = {list_std_a_cell}
cell_names = {cell_names}
fates = {fates}

# Define colors for each fate
colors = {{'Epid.': 'red', 'NS': 'blue', 'Mesoderm': 'green', 'End.': 'purple'}}
fate_colors = [colors[fate] for fate in fates]

corr_coef, p_value = pearsonr(list_std_a_mother, list_std_a_cell)

plt.figure(figsize=(10, 6))
plt.scatter(list_std_a_mother, list_std_a_cell, c=fate_colors,cmap='viridis', alpha=0.5)
plt.xlabel('Mothers division direction variability')
plt.ylabel('Cell division direction variability')
plt.title(f'Division direction variability inter-generations - Generation {gen}\\nPearson r: {{corr_coef:.2f}}, p-value: {{p_value:.2e}}')

for i, cell_name in enumerate(cell_names):
    plt.annotate(cell_name, (list_std_a_mother[i], list_std_a_cell[i]), fontsize=8, alpha=0.7)
plt.grid(True)

# Create custom legend
legend_elements = [mpatches.Patch(color=color, label=fate) for fate, color in colors.items()]
plt.legend(handles=legend_elements, title='Fate')


plt.tight_layout()
plt.savefig(f'{output_folder}/gen_{gen}_std_a_scatterplot.png')
plt.show()
"""
    output_folder = os.path.join(output_folder, 'plot_std_a_to_mothers')
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    script_path = os.path.join(output_folder, f'plot_std_a_to_mothers_gen_{gen}.py')
    with open(script_path, 'w') as script_file:
        script_file.write(script_content)
    print(f"Script for generation {gen} saved to {script_path}")
#############
#### How are cells from different patterns different: div times, cells volumes, shape, surface of contact
def comparision_time_vol_btw_clusters(divisions_properties, time_alignment_coefficients, output_folder=''):
    """
    For each cell, it has tw different cluster
    compare time of div/volume of cells from different clusters

    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
               Example: {'mother_name': 'color': {'left/right': {'embryo name':int}}, # same value=. cells in same pattern,
                                        'cell_volume_scaled': {'left/right': {'embryo name': cell_volume}},
                                        'cell_surface_scaled': {'left/right': {'embryo name': cell_surface}},
                                        'nb_of_measurements': int,
                                         'division_aq_time': {'left/right': {'embryo name': value_to_adjusted}},
                                         'gen': int }
    time_alignment_coefficients (dict): {'embryo name': (a,b)}
    output_folder

    Returns
    -------

    """

    for mother_name, props in divisions_properties.items():

        division_times = []
        cells_volumes = []
        cells_clusters = []
        cells_lifetimes = []

        if 'color' not in props:
            continue
        for dr, prop_dir in props['color'].items():
            for individual_name, color in prop_dir.items():
                # for individuals with same color store shape descriptors in list
                t =props['division_aq_time'][dr][individual_name]
                a,b = time_alignment_coefficients[individual_name]
                t = t*a + b
                division_times.append(t)
                cells_volumes.append(props['cell_volume_scaled'][dr][individual_name])
                cells_clusters.append(color)
                # Check if the lifetime data exists before appending
                if dr in props['division_mothers_lifetimes'] and props['division_mothers_lifetimes'][dr][
                        individual_name]:
                        cells_lifetimes.append(props['division_mothers_lifetimes'][dr][individual_name] * a)
                else:
                    cells_lifetimes.append(None)

                    # call fct to write a python script that plots for cell shape descriptors values of each cluster(same color value)
        script_plot_cell_cluster_time_volume_comparison(division_times, cells_volumes, cells_lifetimes, cells_clusters,
                                                   mother_name, output_folder)


def script_plot_cell_cluster_time_volume_comparison(division_times, cells_volumes, cells_lifetimes, cells_clusters, mother_name, output_folder):
    """
    Generates a Python script to plot division times, cell volumes, and cell lifetimes for different clusters.

    Parameters
    ----------
    division_times (list): List of division times for different clusters.
    cells_volumes (list): List of cell volumes for different clusters.
    cells_lifetimes (list): List of cell lifetimes for different clusters.
    cells_clusters (list): List of cluster identifiers for each cell.
    mother_name (str): Name of the mother cell.
    output_folder (str): Path to the folder where the script will be saved.

    Returns
    -------
    None
    """
    script_content = f""" 
import os
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

# Division times, cell volumes, and cell lifetimes
division_times = {division_times}
cells_volumes = {cells_volumes}
cells_lifetimes = {cells_lifetimes}
cells_clusters = {cells_clusters}

# Prepare data for plotting
data = []
for i in range(len(cells_clusters)):
    data.append([cells_clusters[i], division_times[i], cells_volumes[i], cells_lifetimes[i]])

data_dict = {{
    'Cluster': [x[0] for x in data],
    'Division Time': [x[1] for x in data],
    'Cell Volume': [x[2] for x in data],
    'Cell Lifetime': [x[3] for x in data]
}}

# Create DataFrame
df = pd.DataFrame(data_dict)

# Create plots for division times, cell volumes, and cell lifetimes
fig, axes = plt.subplots(3, 1, figsize=(10, 10))

# Division Time
sns.boxplot(x="Cluster", y="Division Time", data=df, ax=axes[0], boxprops=dict(alpha=.3))
sns.stripplot(x="Cluster", y="Division Time", data=df, color='green', jitter=True, dodge=True, ax=axes[0])
axes[0].set_title('Division Time by Cluster')

# Cell Volume
sns.boxplot(x="Cluster", y="Cell Volume", data=df, ax=axes[1], boxprops=dict(alpha=.3))
sns.stripplot(x="Cluster", y="Cell Volume", data=df, color='green', jitter=True, dodge=True, ax=axes[1])
axes[1].set_title('Cell Volume by Cluster')

# Cell Lifetime
sns.boxplot(x="Cluster", y="Cell Lifetime", data=df, ax=axes[2], boxprops=dict(alpha=.3))
sns.stripplot(x="Cluster", y="Cell Lifetime", data=df, color='green', jitter=True, dodge=True, ax=axes[2])
axes[2].set_title('Cell Lifetime by Cluster')

plt.tight_layout()
plt.show()

#output_path = os.path.join(r'{output_folder}', '{mother_name}_time_volume_lifetime_comparison.png')
#plt.savefig(output_path)
#plt.close()
#print(f"Time, volume, and lifetime comparison plot saved to {{output_path}}")
"""
    output_folder = os.path.join(output_folder, 'division_time_vol_lifetime_clusters_analysis')
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    script_path = os.path.join(output_folder, f'{mother_name}_plot_time_volume_lifetime_comparison.py')
    with open(script_path, 'w') as script_file:
        script_file.write(script_content)

    print(f"Plotting script saved to {script_path}")


##################
#### How are cells from different patterns different: volume/embryo volume,
#  ‘‘Entropy,’’(object compactness, (0,100: for sphere) )
#  ‘‘Elongation,’’ ‘‘Flatness,’’ (from bounding box) we dont have
# ‘‘Sphericity,’’( Silva  et al. from volume and surface)
# ‘‘Surface over volume’’ (or ‘‘S/V’’) ratio,
def comparision_geom_btw_clusters(divisions_properties, output_folder=''):
    """
    For each cell, it has tw different cluster
    compare shape of cells from different clusters using geom featurs (compactness, elongation, sphercity)

    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
               Example: {'mother_name': 'division_directions_after_symmetry':{'left/right': {'embryo name': np.array()}},
                                                        # directions before registration
                                        'color': {'left/right': {'embryo name':int}}, # same value=. cells in same pattern,
                                        'cell_principal_values'{'left/right': {'embryo name': igenvalues}},
                                        'cell_volume_scaled': {'left/right': {'embryo name': cell_volume}},
                                        'cell_surface_scaled': {'left/right': {'embryo name': cell_surface}},
                                        'nb_of_measurements': int,
                                         'division_aq_time': {'left/right': {'embryo name': value_to_adjusted}},
                                         'gen': int }
    output_folder

    Returns
    -------

    """

    for mother_name, props in divisions_properties.items():

        elongation_values = []
        sphericity_values = []
        sv_ratio_values = []
        cells_clusters = []

        if 'color' not in props:
            continue
        for dr, prop_dir in props['color'].items():
            for individual_name, color in prop_dir.items():
                # for individuals with same color store shape descriptors in list
                # fct to call : surface_volume(surface, volume), sphercity(eigenvalues), elongation(eigenvalues)
                eigenvalues = props['cell_principal_values'][dr][individual_name]
                sv_ratio_values.append(surface_volume(props['cell_surface_scaled'][dr][individual_name],
                                                      props['cell_volume_scaled'][dr][individual_name]))
                sphericity_values.append(sphercity(eigenvalues))
                elongation_values.append(elongation(eigenvalues))
                cells_clusters.append(color)

        # call fct to write a python script that plots for cell shape descriptors values of each cluster(same color value)
        script_plot_cell_cluster_shape_comparision(sv_ratio_values, sphericity_values, elongation_values,
                                                   cells_clusters,
                                                   mother_name, output_folder)


def script_plot_cell_cluster_shape_comparision(sv_ratio_values, sphericity_values, elongation_values, cells_clusters,
                                                 cell_name, output_folder):

    output_folder = os.path.join(output_folder, 'shape_clusters_analysis')
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    script_content = f"""
import pandas as pd 
import os
import seaborn as sns
import matplotlib.pyplot as plt

# Shape descriptor values
sv_ratio_values = {sv_ratio_values}
sphericity_values = {sphericity_values}
elongation_values = {elongation_values}
cells_clusters = {cells_clusters}

# Prepare data for boxplots
data = []
for i in range(len(cells_clusters)):
    data.append([cells_clusters[i], sv_ratio_values[i], sphericity_values[i], elongation_values[i]])

data_dict = {{'Cluster': [x[0] for x in data],
              'S/V Ratio': [x[1] for x in data],
              'Sphericity': [x[2] for x in data],
              'Elongation': [x[3] for x in data]}}

# Create DataFrame
df = pd.DataFrame(data_dict)

# Create boxplots for each shape descriptor
fig, axes = plt.subplots(3, 1, figsize=(10, 10))

# Surface-to-Volume Ratio
sns.boxplot(x="Cluster", y="S/V Ratio", data=df, ax=axes[0], boxprops=dict(alpha=.3))
sns.stripplot(x="Cluster", y="S/V Ratio", data=df, color='green', jitter=True, dodge=True, ax=axes[0])
axes[0].set_title('Surface^3-to-Volume^2 Ratio, Higher => less compact')

# Sphericity
sns.boxplot(x="Cluster", y="Sphericity", data=df, ax=axes[1], boxprops=dict(alpha=.3))
sns.stripplot(x="Cluster", y="Sphericity", data=df, color='green', jitter=True, dodge=True, ax=axes[1])
axes[1].set_title('Sphericity, 1 => more like a sphere')

# Elongation
sns.boxplot(x="Cluster", y="Elongation", data=df, ax=axes[2], boxprops=dict(alpha=.3))
sns.stripplot(x="Cluster", y="Elongation", data=df, color='green', jitter=True, dodge=True, ax=axes[2])
axes[2].set_title('Elongation, if 0 => more elongated')

plt.tight_layout()
plt.show()
#output_path = os.path.join(r'{output_folder}', '{cell_name}_shape_descriptors_comparison.png')
#plt.savefig(output_path)
#plt.close()
#print(f"Shape descriptors comparison plot saved to {{output_path}}")
"""
    script_path = os.path.join(output_folder, f'{cell_name}_plot_shape_clusters_comparision.py')
    with open(script_path, 'w') as script_file:
        script_file.write(script_content)

    print(f"Plotting script saved to {script_path}")

##################
#######
def deviation_left_right_to_other_individuals_and_registration_local_residue_in_same_pattern(divisions_properties,
                                                        time_alignment_coefficients,mothers_to_consider, gen=[7, 8, 9],
                                                                                             output_folder=''):
    """
    We want to compare cells division directions between left and right to other individuals from same pattern

    Deviation between left and right is not sensible to registration (but to symetry) compared to other individuals
    compare cells division direction angle to difference in division time and average division time

    Calls a functions that writes a script that does the plot
    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
               Example: {'mother_name': 'division_directions_after_symmetry':{'left/right': {'embryo name': np.array()}},
                                                        # directions before registration
                                        'color': {'left/right': {'embryo name':int}}, # same value=. cells in same pattern,
                                        'two_modes_kernel_size': float,
                                        'nb_of_measurements': int,
                                         'division_aq_time': {'left/right': {'embryo name': value_to_adjusted}},
                                         'gen': int }
    time_alignment_coefficients (dict): {'embryo name': (a,b)}, to adjust time: t = t*a+b
    mothers (list): list of unimodal cells
                     a7.0008 b7.0012 a7.0014 a7.0010 a7.0011 b7.0014 b7.0001 a7.0013 b7.0009 a7.0004 b7.0003 b7.0010
                     a7.0012 a8.0026 a8.0028 a8.0008 a8.0024 b8.0020 a8.0027 a8.0017 b8.0008 b8.0007 b8.0018 b8.0019
                     b8.0021 a8.0007 a8.0021 a8.0022 a8.0011 a8.0010 a8.0018 b8.0022 a8.0030
                     b9.0042 a9.0016 a9.0046 a9.0040 a9.0036 a9.0041 b9.0062 b9.0058 a9.0047 a9.0039 b9.0040 b9.0049
                     a9.0014 a9.0045 a9.0048 a9.0035 b9.0063 a9.0042 b9.0036 a9.0063 a9.0059 b9.0039 b9.0045 b9.0050
                     a9.0056 a9.0055 a9.0013 b9.0053
    gen
    output_folder

    Returns
    -------

    """

    output_folder = create_output_folder(output_folder, 'deviation_left_right_to_other_individuals')

    left_right_in_same_pattern_angles_all = []
    other_individuals_in_same_pattern_angles_all = []
    dif_left_right_all = []
    avg_div_time_lr_all = []
    avg_div_time_o_all = []

    residues_lr_all = []
    residues_o_all = []

    for g in gen:

        left_right_in_same_pattern_angles = []
        other_individuals_in_same_pattern_angles = []
        dif_left_right = []
        avg_div_time_lr = []
        avg_div_time_o = []

        residues_lr = []
        residues_o = []

        for mother_name, props in divisions_properties.items():

            # if cell is precised as multimodal ignore
            #
            if mother_name not in mothers_to_consider:
                continue

            if props['gen'] != g:
                continue
            if 'division_barycenter_from_global_registration' not in props or 'division_volume_ratio' not in props:
                continue

            # compute avg div time
            avg_div_time = compute_avg_division_time(props.get('division_aq_time', {}),
                                                     time_alignment_coefficients)

            # compute for cell its dispersion of cell barycenter
            merged_barycenters = returns_merged_daughters_residues(props['division_barycenter_from_global_registration'],
                                                    props['division_volume_ratio'])
            # merged_barycenters is dict {'left/right':{'ind': barycenter of merged daughters}}

            for dr, prop_dir in props['division_directions_after_symmetry'].items():
                for individual_name, direction in prop_dir.items():
                    # if individual not ref skip
                    if not (props['residues'][dr][individual_name] == -1):
                        continue

                    if individual_name not in props['division_directions_after_symmetry']['left'] or \
                            individual_name not in props['division_directions_after_symmetry']['right']:
                        continue

                    left_direction = props['division_directions_after_symmetry']['left'][individual_name]
                    right_direction = props['division_directions_after_symmetry']['right'][individual_name]
                    # Compute the angle between left and right directions
                    angle_between_lr = np.arccos(np.clip(np.dot(left_direction, right_direction) /
                                                         (np.linalg.norm(left_direction) * np.linalg.norm(
                                                             right_direction)), -1.0, 1.0))

                    left_right_in_same_pattern_angles.append(np.degrees(angle_between_lr))

                    if np.degrees(angle_between_lr)>70:
                        print('in left of ref', mother_name, np.degrees(angle_between_lr))

                    # get div times and store their differences with avg div time
                    avg_div_time_lr.append(avg_div_time)

                    # store residues: get bary in ref, bary in other dir
                    v = props['cell_volume_scaled'][dr][individual_name]
                    bary_ref = merged_barycenters[dr][individual_name]
                    if dr == 'left':
                        v2 =props['cell_volume_scaled']['right'][individual_name]
                        v = v + v2
                        r = 3 * v / (4 * np.pi) ** (1 / 3)
                        residues_lr.append(np.linalg.norm(bary_ref - merged_barycenters['right'][individual_name]))
                    else:
                        v2 = props['cell_volume_scaled']['left'][individual_name]
                        v = v+v2
                        r = 3 * v / (4 * np.pi) ** (1 / 3)
                        r = np.linalg.norm(bary_ref - merged_barycenters['left'][individual_name])
                        residues_lr.append(r)

                        # Compare with other individuals in the same pattern
                        for lr, prop_dir_2 in props['division_directions_after_symmetry'].items():
                            for other_individual, other_direction in prop_dir_2.items():
                                if other_individual == individual_name:
                                    continue

                                angle_with_other = np.arccos(np.clip(np.dot(left_direction, other_direction) /
                                                                     (np.linalg.norm(left_direction) * np.linalg.norm(
                                                                         other_direction)), -1.0, 1.0))
                                other_individuals_in_same_pattern_angles.append(np.degrees(angle_with_other))

                                dif_left_right.append(np.abs(np.degrees(angle_with_other) - np.degrees(angle_between_lr)))
                                if np.abs(np.degrees(angle_with_other) - np.degrees(angle_between_lr)) > 50:
                                    print('dif angles', np.abs(np.degrees(angle_with_other) - np.degrees(angle_between_lr)))

                                if np.degrees(angle_with_other)>70:
                                    print(other_individual, lr, mother_name, np.degrees(angle_with_other))

                                # get division times and store their diff
                                avg_div_time_o.append(avg_div_time)

                                # store residues
                                v2 = props['cell_volume_scaled'][lr][other_individual]
                                v = (v+v2)/2  # from v get r
                                r = 3 * v / (4 * np.pi) ** (1 / 3)
                                r = np.linalg.norm(bary_ref - merged_barycenters[lr][other_individual])
                                residues_o.append(r)

        # a plot per gen
        # plot boxplots of strqightforward comparisions
        script_path = os.path.join(output_folder, f'plot_angle_comparison_lr_local_residues_{g}.py')
        script_plot_deviation_angles_btw_lr_indiv_to_lres(left_right_in_same_pattern_angles,
                                                        other_individuals_in_same_pattern_angles,
                                                        residues_lr, residues_o,
                                                        avg_div_time_lr, avg_div_time_o, script_path)

        script_path = os.path.join(output_folder, f'plot_angle_comparison_lr_local_residues_distribution_{g}.py')
        script_plot_deviation_angles_distribution_btw_lr_indiv_to_lres(left_right_in_same_pattern_angles,
                                                          other_individuals_in_same_pattern_angles,
                                                          residues_lr, residues_o,
                                                          avg_div_time_lr, avg_div_time_o, script_path)
        left_right_in_same_pattern_angles_all.extend(left_right_in_same_pattern_angles)
        other_individuals_in_same_pattern_angles_all.extend(other_individuals_in_same_pattern_angles)
        residues_lr_all.extend(residues_lr)
        residues_o_all.extend(residues_o)
        avg_div_time_lr_all.extend(avg_div_time_lr)
        avg_div_time_o_all.extend(avg_div_time_o)
        dif_left_right_all.extend(dif_left_right)

    # a plot per gen
    # plot boxplots of strqightforward comparisions
    script_path = os.path.join(output_folder, f'plot_angle_comparison_lr_local_residues_.py')
    script_plot_deviation_angles_btw_lr_indiv_to_lres(left_right_in_same_pattern_angles_all,
                                                                   other_individuals_in_same_pattern_angles_all,
                                                                   residues_lr_all, residues_o_all,
                                                                   avg_div_time_lr_all, avg_div_time_o_all, script_path)

    script_path = os.path.join(output_folder, f'plot_angle_comparison_lr_local_residues_distribution_.py')
    script_plot_deviation_angles_distribution_btw_lr_indiv_to_lres(left_right_in_same_pattern_angles_all,
                                                                   other_individuals_in_same_pattern_angles_all,
                                                                   residues_lr_all, residues_o_all,
                                                                   avg_div_time_lr_all, avg_div_time_o_all, script_path)
    # todo adjust plot, title, x and y axis...
    script_path = os.path.join(output_folder, f'plot_angle_dif_comparison_lr_local_residues_.py')
    script_plot_deviation_angles_btw_lr_indiv_to_lres(left_right_in_same_pattern_angles_all,
                                                      dif_left_right_all,
                                                      residues_lr_all, residues_o_all,
                                                      avg_div_time_lr_all, avg_div_time_o_all, script_path)



def script_plot_deviation_angles_distribution_btw_lr_indiv_to_lres(left_right_in_same_pattern_angles, other_individuals_in_same_pattern_angles,
                                                   left_right_in_same_pattern_dif_div_times, other_individuals_in_same_pattern_dif_div_times,
                                                   avg_div_time_lr, avg_div_time_o, script_path):
    """
    Generates a Python script to plot angles and division times for intra-individual (left-right) and inter-individual comparisons.
    to shape difference between embryos(registration residues)

    Parameters
    ----------
    left_right_in_same_pattern_angles (list): List of angles between left and right divisions for the same individual.
    other_individuals_in_same_pattern_angles (list): List of angles between divisions of different individuals in the same pattern.
    left_right_in_same_pattern_dif_div_times (list): List of division time differences between left and right divisions for the same individual.
    other_individuals_in_same_pattern_dif_div_times (list): List of division time differences between divisions of different individuals in the same pattern.
    avg_div_time_lr (list): List of average division times for left-right comparisons.
    avg_div_time_o (list): List of average division times for inter-individual comparisons.
    script_path (str): Path to save the generated Python script.

    Returns
    -------
    None
    """
    script_content = f""" 
import os
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

# Data preparation
data_lr = {{
    'Angle': {left_right_in_same_pattern_angles},
    'residues': {left_right_in_same_pattern_dif_div_times},
    't_avg': {avg_div_time_lr},
    'Comparison': ['Left-Right' for _ in range(len({left_right_in_same_pattern_angles}))]
}}

data_other = {{
    'Angle': {other_individuals_in_same_pattern_angles},
    'residues': {other_individuals_in_same_pattern_dif_div_times},
    't_avg': {avg_div_time_o},
    'Comparison': ['Inter-Individual' for _ in range(len({other_individuals_in_same_pattern_angles}))]
}}

# DataFrames
df_lr = pd.DataFrame(data_lr)
df_other = pd.DataFrame(data_other)

# Create subplots
fig, axes = plt.subplots(2, 1, figsize=(12, 8), gridspec_kw={{'height_ratios': [1, 1], 'hspace': 0.3}})

# 2D KDE plot for left-right angles
sns.kdeplot(ax=axes[0], x='residues', y='Angle', data=df_lr, cmap="viridis", fill=True, bw_adjust=0.3)
axes[0].set_title('Left-Right: Angle vs registration residue')
axes[0].set_xlabel('Cells mother registration residue')
axes[0].set_ylabel('Angle (degrees)')

# 2D KDE plot for inter-individual angles
sns.kdeplot(ax=axes[1], x='residues', y='Angle', data=df_other, cmap="viridis", fill=True, bw_adjust=0.3)
axes[1].set_title('Inter-Individual: Angle vs registration residues')
axes[1].set_xlabel('Cells mother registration residue')
axes[1].set_ylabel('Angle (degrees)')

# Ensure the x-axes are the same length
x_min = min(df_lr['residues'].min(), df_other['residues'].min())
x_max = max(df_lr['residues'].max(), df_other['residues'].max())
axes[0].set_xlim(x_min, x_max)
axes[1].set_xlim(x_min, x_max)

# Adjust layout to make space for colorbar
fig.subplots_adjust(right=0.85)

# Add colorbar outside the subplots
cbar_ax = fig.add_axes([0.9, 0.15, 0.03, 0.7])  # [left, bottom, width, height]
norm = plt.Normalize(vmin=0, vmax=1)
sm = plt.cm.ScalarMappable(cmap="viridis", norm=norm)
sm.set_array([])
cbar = fig.colorbar(sm, cax=cbar_ax)
cbar.set_label('Density')

# Save the figure
plt.tight_layout(rect=[0, 0, 0.85, 1])  # Adjust rect to make room for the colorbar
#plt.savefig(script_path)
plt.show()
#plt.close()
        """
    with open(script_path, 'w') as script_file:
        script_file.write(script_content)

    print(f"Plot script saved to {script_path}")


def script_plot_deviation_angles_btw_lr_indiv_to_lres(left_right_in_same_pattern_angles, other_individuals_in_same_pattern_angles,
                                                   left_right_in_same_pattern_dif_div_times, other_individuals_in_same_pattern_dif_div_times,
                                                   avg_div_time_lr, avg_div_time_o, script_path):
    """
    Generates a Python script to plot angles and division times for intra-individual (left-right) and inter-individual comparisons.
    to shape difference between embryos(registration residues)
    Parameters
    ----------
    left_right_in_same_pattern_angles (list): List of angles between left and right divisions for the same individual.
    other_individuals_in_same_pattern_angles (list): List of angles between divisions of different individuals in the same pattern.
    left_right_in_same_pattern_dif_div_times (list): List of division time differences between left and right divisions for the same individual.
    other_individuals_in_same_pattern_dif_div_times (list): List of division time differences between divisions of different individuals in the same pattern.
    avg_div_time_lr (list): List of average division times for left-right comparisons.
    avg_div_time_o (list): List of average division times for inter-individual comparisons.
    output_folder (str): Path to the folder where the script will be saved.

    Returns
    -------
    None
    """
    script_content = f""" 
import os
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

# Data preparation
data_lr = {{
    'Angle': {left_right_in_same_pattern_angles},
    'residues': {left_right_in_same_pattern_dif_div_times},
    't_avg': {avg_div_time_lr},
    'Comparison': ['Left-Right' for _ in range(len({left_right_in_same_pattern_angles}))]
}}

data_other = {{
    'Angle': {other_individuals_in_same_pattern_angles},
    'residues': {other_individuals_in_same_pattern_dif_div_times},
    't_avg': {avg_div_time_o},
    'Comparison': ['Inter-Individual' for _ in range(len({other_individuals_in_same_pattern_angles}))]
}}

# DataFrames
df_lr = pd.DataFrame(data_lr)
df_other = pd.DataFrame(data_other)

# Create subplots
fig, axes = plt.subplots(2, 1, figsize=(14, 10))

# Scatter plot for left-right angles
sns.scatterplot(ax=axes[0], x='residues', y='Angle', size='t_avg', data=df_lr, sizes=(20, 200), alpha=0.7, palette="viridis")
axes[0].set_title('Left-Right: Angle vs registration residue')
axes[0].set_xlabel(' Cells mother registration residue')
axes[0].set_ylabel('Angle (degrees)')
axes[0].legend(title='Avg div time', loc='upper right', bbox_to_anchor=(1.15, 1))

# Scatter plot for inter-individual angles
sns.scatterplot(ax=axes[1], x='residues', y='Angle', size='t_avg', data=df_other, sizes=(20, 200), alpha=0.7, palette="viridis")
axes[1].set_title('Inter-Individual: Angle vs registration residues')
axes[1].set_xlabel(' Cells mother registration residue')
axes[1].set_ylabel('Angle (degrees)')
axes[1].legend(title='Avg div time', loc='upper right', bbox_to_anchor=(1.15, 1))

x_min = min(df_lr['residues'].min(), df_other['residues'].min())
x_max = max(df_lr['residues'].max(), df_other['residues'].max())
axes[0].set_xlim(x_min, x_max)
axes[1].set_xlim(x_min, x_max)


# Save the figure
#output_path = os.path.join('{script_path}', 'angle_comparison_lr_residues.png')
plt.tight_layout()
#plt.savefig(output_path)
plt.show()
#plt.close()

print(f"Plots saved to {{output_path}}")
    """
    with open(script_path, 'w') as script_file:
        script_file.write(script_content)

    print(f"Plot script saved to {script_path}")



def deviation_left_right_to_other_individuals_in_same_pattern(divisions_properties,time_alignment_coefficients,
                                                              mothers_to_consider,
                                                              gen=[7, 8, 9], output_folder=''):
    """
    We want to compare cells division directions between left and right to other individuals from same pattern

    Deviation between left and right is not sensible to registration (but to symetry) compared to other individuals
    compare cells division direction angle to difference in division time and average division time

    Calls a functions that writes a script that does the plot
    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
               Example: {'mother_name': 'division_directions_after_symmetry':{'left/right': {'embryo name': np.array()}},
                                                        # directions before registration
                                        'color': {'left/right': {'embryo name':int}}, # same value=. cells in same pattern,
                                        'two_modes_kernel_size': float,
                                        'nb_of_measurements': int,
                                         'division_aq_time': {'left/right': {'embryo name': value_to_adjusted}},
                                         'gen': int }
    time_alignment_coefficients (dict): {'embryo name': (a,b)}, to adjust time: t = t*a+b
    mothers (list): list of unimodal cells
                     a7.0008 b7.0012 a7.0014 a7.0010 a7.0011 b7.0014 b7.0001 a7.0013 b7.0009 a7.0004 b7.0003 b7.0010
                     a7.0012 a8.0026 a8.0028 a8.0008 a8.0024 b8.0020 a8.0027 a8.0017 b8.0008 b8.0007 b8.0018 b8.0019
                     b8.0021 a8.0007 a8.0021 a8.0022 a8.0011 a8.0010 a8.0018 b8.0022
    gen
    output_folder

    Returns
    -------

    """

    output_folder = create_output_folder(output_folder, 'deviation_left_right_to_other_individuals')

    ##compared_individuals = [] # list of tuples (cell name, individual_dir_1, individual_dir_2 )
    left_right_in_same_pattern_angles = []
    other_individuals_in_same_pattern_angles = []
    dif_left_right = []
    left_right_in_same_pattern_dif_div_times = []  # tl - tr (division time difference)
    other_individuals_in_same_pattern_dif_div_times = []  # t1 - t2
    avg_div_time_lr = []
    avg_div_time_o = []

    avg_residues_lr = []
    avg_residues_o = []

    for mother_name, props in divisions_properties.items():

        # if cell is precised as multimodal ignore
        #
        if mother_name not in mothers_to_consider:
            continue

        if props['gen'] not in gen:
            continue
        if 'residues' not in props:
            continue
        # compute avg div time
        avg_div_time = compute_avg_division_time(props.get('division_aq_time', {}),
                                                 time_alignment_coefficients)

        for dr, prop_dir in props['division_directions_after_symmetry'].items():
            for individual_name, direction in prop_dir.items():

                # if individual not ref skip
                if not (props['residues'][dr][individual_name] == -1):
                    continue

                if individual_name not in props['division_directions_after_symmetry']['left'] or \
                        individual_name not in props['division_directions_after_symmetry']['right']:
                    continue

                left_direction = props['division_directions_after_symmetry']['left'][individual_name]
                right_direction = props['division_directions_after_symmetry']['right'][individual_name]
                # Compute the angle between left and right directions
                angle_between_lr = np.arccos(np.clip(np.dot(left_direction, right_direction) /
                                                     (np.linalg.norm(left_direction) * np.linalg.norm(
                                                         right_direction)), -1.0, 1.0))
                left_right_in_same_pattern_angles.append(np.degrees(angle_between_lr))

                # get div times and store their differences with avg div time
                a,b = time_alignment_coefficients[individual_name]
                t1 = props['division_aq_time']['left'][individual_name]
                t2 = props['division_aq_time']['right'][individual_name]
                delta_t = np.abs(a* (t1-t2))
                left_right_in_same_pattern_dif_div_times.append(delta_t)
                avg_div_time_lr.append(avg_div_time)

                # store residues
                if dr == 'left':
                    avg_residues_lr.append(props['residues']['right'][individual_name])
                else:
                    avg_residues_lr.append(props['residues']['left'][individual_name])
                ##

                # Compare with other individuals in the same pattern
                for lr, prop_dir_2 in props['division_directions_after_symmetry'].items():
                    for other_individual, other_direction in prop_dir_2.items():
                        if other_individual == individual_name:
                            continue

                        angle_with_other = np.arccos(np.clip(np.dot(left_direction, other_direction) /
                                                             (np.linalg.norm(left_direction) * np.linalg.norm(
                                                                 other_direction)), -1.0, 1.0))
                        other_individuals_in_same_pattern_angles.append(np.degrees(angle_with_other))

                        dif_left_right.append(np.abs(np.degrees(angle_with_other) - np.degrees(angle_between_lr)))

                        # get division times and store their diff
                        a, b = time_alignment_coefficients[individual_name]
                        t1 = props['division_aq_time'][dr][individual_name]
                        t1 = t1*a+b
                        a, b = time_alignment_coefficients[other_individual]
                        t2 = props['division_aq_time'][lr][other_individual]
                        t2 = a*t2+b
                        delta_t = np.abs((t1 - t2))
                        other_individuals_in_same_pattern_dif_div_times.append(delta_t)
                        avg_div_time_o.append(avg_div_time)

                        # store residues
                        avg_residues_o.append(props['residues'][lr][other_individual])

    # make residues instead of delta_t, and for all cells included
    # fct to include in plot delta_t and t avg info in the plot
    # seperate plots for inter and intra(btw left and right of same individual) deviations
    script_plot_cell_same_cluster_disp_lr_inter_ind_shape_dif(left_right_in_same_pattern_angles,
                                                    other_individuals_in_same_pattern_angles,
                                                    avg_residues_lr, avg_residues_o,
                                                    avg_div_time_lr, avg_div_time_o, output_folder)

    # plot boxplots of strqightforward comparisions
    script_plot_deviation_angles_btw_lr_indiv(left_right_in_same_pattern_angles,
                                              other_individuals_in_same_pattern_angles, dif_left_right, gen, output_folder)


def script_plot_cell_same_cluster_disp_lr_inter_ind_shape_dif(left_right_in_same_pattern_angles, other_individuals_in_same_pattern_angles,
                                                   left_right_in_same_pattern_dif_div_times, other_individuals_in_same_pattern_dif_div_times,
                                                   avg_div_time_lr, avg_div_time_o, output_folder):
    """
    Generates a Python script to plot angles and division times for intra-individual (left-right) and inter-individual comparisons.
    to shape difference between embryos(registration residues)
    Parameters
    ----------
    left_right_in_same_pattern_angles (list): List of angles between left and right divisions for the same individual.
    other_individuals_in_same_pattern_angles (list): List of angles between divisions of different individuals in the same pattern.
    left_right_in_same_pattern_dif_div_times (list): List of division time differences between left and right divisions for the same individual.
    other_individuals_in_same_pattern_dif_div_times (list): List of division time differences between divisions of different individuals in the same pattern.
    avg_div_time_lr (list): List of average division times for left-right comparisons.
    avg_div_time_o (list): List of average division times for inter-individual comparisons.
    output_folder (str): Path to the folder where the script will be saved.

    Returns
    -------
    None
    """
    script_content = f""" 
import os
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

# Data preparation
data_lr = {{
    'Angle': {left_right_in_same_pattern_angles},
    'residues': {left_right_in_same_pattern_dif_div_times},
    't_avg': {avg_div_time_lr},
    'Comparison': ['Left-Right' for _ in range(len({left_right_in_same_pattern_angles}))]
}}

data_other = {{
    'Angle': {other_individuals_in_same_pattern_angles},
    'residues': {other_individuals_in_same_pattern_dif_div_times},
    't_avg': {avg_div_time_o},
    'Comparison': ['Inter-Individual' for _ in range(len({other_individuals_in_same_pattern_angles}))]
}}

# DataFrames
df_lr = pd.DataFrame(data_lr)
df_other = pd.DataFrame(data_other)

# Create subplots
fig, axes = plt.subplots(2, 2, figsize=(14, 10), gridspec_kw={{'width_ratios': [3, 1]}})

# Scatter plot for left-right angles
sns.scatterplot(ax=axes[0, 0], x='residues', y='Angle', size='t_avg', data=df_lr, sizes=(20, 200), alpha=0.7, palette="viridis")
axes[0, 0].set_title('Left-Right: Angle vs registration residue')
axes[0, 0].set_xlabel('Registration avg residues')
axes[0, 0].set_ylabel('Angle (degrees)')
axes[0, 0].legend(title='Avg div time', loc='upper right', bbox_to_anchor=(1.15, 1))

# KDE plot for left-right angles
sns.kdeplot(ax=axes[0, 1], data=df_lr, y='Angle', fill=True, alpha=0.3, color="blue", bw_adjust=0.5)
axes[0, 1].set_title('Density of Angles (Left-Right) kernel chosen using scotts method')
axes[0, 1].set_xlabel('Density')
axes[0, 1].set_ylabel('')  # No ylabel to keep it clean
axes[0, 1].yaxis.set_label_position("right")
axes[0, 1].yaxis.tick_right()

# Scatter plot for inter-individual angles
sns.scatterplot(ax=axes[1, 0], x='residues', y='Angle', size='t_avg', data=df_other, sizes=(20, 200), alpha=0.7, palette="viridis")
axes[1, 0].set_title('Inter-Individual: Angle vs registration residues')
axes[1, 0].set_xlabel('Registration avg residues')
axes[1, 0].set_ylabel('Angle (degrees)')
axes[1, 0].legend(title='Avg div time', loc='upper right', bbox_to_anchor=(1.15, 1))

# KDE plot for inter-individual angles
sns.kdeplot(ax=axes[1, 1], data=df_other, y='Angle', fill=True, alpha=0.3, color="blue", bw_adjust=0.5)
axes[1, 1].set_title('Density of Angles (Inter-Individual) kernel chosen using scotts method')
axes[1, 1].set_xlabel('Density')
axes[1, 1].set_ylabel('')  # No ylabel to keep it clean
axes[1, 1].yaxis.set_label_position("right")
axes[1, 1].yaxis.tick_right()

# Save the figure
output_path = os.path.join('{output_folder}', 'angle_comparison_lr_residues.png')
plt.tight_layout()
plt.savefig(output_path)
plt.show()
plt.close()

print(f"Plots saved to {{output_path}}")
    """
    script_path = os.path.join(output_folder, f'plot_angle_comparison_lr_residues.py')
    with open(script_path, 'w') as script_file:
        script_file.write(script_content)

    print(f"Plot script saved to {script_path}")


def script_plot_deviation_angles_btw_lr_indiv(left_right_angles, other_angles, dif_left_right, gen, output_folder):
    script_content = f""" 
import os
import matplotlib.pyplot as plt

left_right_angles = {left_right_angles}
other_angles = {other_angles}
dif_left_right = {dif_left_right}

plt.figure(figsize=(10, 5))

# Create boxplot
plt.boxplot([left_right_angles, other_angles, dif_left_right], labels=['Left-Right Angles', 'Other Individuals Angles', 'Difference between angles of LR and other individuals angles'])
plt.ylabel('Deviation Angle (degrees)')
plt.title('Deviation Angles Between Left-Right and Other Individuals')
#plt.ylim(0,180)
plt.yticks([0,5,10,15,20,30,40])
plt.tight_layout()
output_path = os.path.join(r'{output_folder}', 'deviation_angles_boxplot.png')
plt.savefig(output_path)
plt.show()
plt.close()
print(f"Deviation angles boxplot saved to {{output_path}}")
"""

    script_path = os.path.join(output_folder, f'plot_deviation_angles_boxplot_gen{gen}.py')
    with open(script_path, 'w') as script_file:
        script_file.write(script_content)

    print(f"Plotting script saved to {script_path}")

########
##### # variability vs spatial position

def div_dir_var_vs_spatial_post(divisions_properties, gen=[7, 8, 9], avg=True, output_folder=''):
    """
    Do we have more variability at the peripheries of the embryo?
    Parameters
    ----------
    divisions_properties
    gen
    avg
    output_folder

    Returns
    -------

    """

    output_folder = os.path.join(output_folder, 'div_dir_var_vs_spatial_post')
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for g in gen:
        # Filter divisions properties for the current generation
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == g}
        gen_divisions = dict(sorted(gen_divisions.items()))

        mother_names = []
        mothers_spatial_pos = []
        mothers_div_dispersion = []
        list_fate_group = []

        for mother_name, properties in gen_divisions.items():
            if properties.get('nb_of_measurements') < 5:
                continue
            coordinates = []
            std_a = properties['std_from_mean_directions']
            for lr, ind_prop in properties['cylindrical_coords'].items():
                for embryo_name, coords in ind_prop.items():
                    if coords is not None:
                        coordinates.append(coords)
            # maybe avg coords instead
            if coordinates:
                fate_group = determine_fate_group(properties.get('cell_fate', None), True)
                if isinstance(fate_group, list):
                    fate_group = fate_group[0]
                if avg:
                    coordinates_array = np.array(coordinates)
                    avg_coords = coordinates_array.mean(axis=0).tolist()
                    #print(coordinates, avg_coords)
                    mothers_spatial_pos.append(avg_coords)
                    mothers_div_dispersion.append(std_a)
                    mother_names.append(mother_name)
                    list_fate_group.append(fate_group)
                else:
                    mothers_spatial_pos.append(coordinates)
                    mothers_div_dispersion.append(std_a)
                    mother_names.append(mother_name)
                    list_fate_group.append(fate_group)

        output_file = os.path.join(output_folder, f'gen{g}.py')
        write_div_dir_var_vs_spatial_pos_script(g, mother_names, mothers_spatial_pos, mothers_div_dispersion, list_fate_group, output_file)

def write_div_dir_var_vs_spatial_pos_script(gen, mother_names, mothers_spatial_pos, mothers_div_dispersion,
                                            list_fate_group, output_file):
    print('plot in', output_file)
    script_content = f"""
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

mother_names = {mother_names}
mothers_spatial_pos = {mothers_spatial_pos}
mothers_div_dispersion = {mothers_div_dispersion}
list_fate_group = {list_fate_group}

colors = {{'Epid.': 'red', 'NS': 'blue', 'Mesoderm': 'green', 'End.': 'purple'}}

fig, ax = plt.subplots()
for i, mother in enumerate(mother_names):
    r, z = np.array(mothers_spatial_pos[i])
    dispersion = mothers_div_dispersion[i]
    
    ax.scatter(r*0.3, np.abs(z)*0.3, s=dispersion * 50, alpha=0.5, color=colors.get(list_fate_group[i], 'black'), label=list_fate_group[i], linewidth=0.5)
    ax.text(r*0.3, np.abs(z)*0.3, mother)

ax.set_xlabel('r coordinate(in um)')
ax.set_ylabel('z coordinate(in um)')

# Add legend for dispersion
legend_title = 'Dispersion of cell division direction'
sizes_legend = [10, 15, 30]  # Adjust these sizes as needed
labels_legend = ['5', '20', '30']

legend_elements = [
    plt.Line2D([0], [0], marker='o', color='w', markerfacecolor='grey', markersize=size, alpha=0.1, label=label)
    for size, label in zip(sizes_legend, labels_legend)
]
legend1 = plt.legend(handles=legend_elements, title=legend_title, loc='upper right')
ax.add_artist(legend1)

# Add legend for fate groups
legend_handles = [mpatches.Patch(color=color, label=label) for label, color in colors.items()]
plt.legend(handles=legend_handles, title='Fate Groups', loc='upper left')


plt.tight_layout()
ax.set_title(f'Division Direction Variability vs Spatial Position (Generation {gen})')
plt.savefig(os.path.join('{os.path.dirname(output_file)}', f'gen{gen}.png'))
plt.show()
"""
    with open(output_file, 'w') as script_file:
        script_file.write(script_content)

#####################
##########
###

def plot_mothers_lifetime_to_daughters_lifetimes(divisions_properties, time_alignment_coefficients, output_folder=''):
    """
    For each cell compare its lifetime to its daughter lifetime in each individual

    cell's daughter name = uname.get_daughters(name)

    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
               Example: {'mother_name': { 'division_mothers_lifetimes':
                                        {'left': {'individual name': lifetime (None if not valid)},
                                        'right': {'individual name': lifetime (None if not valid)} }
                                                    },
                                        'nb_of_measurements': int,
                                         'division_aq_time': {'left/right': {'embryo name': value_to_adjusted}},
                                         'gen': int }
    time_alignment_coefficients (dict): {'embryo name': (a,b)}
    gen (list): mothers generations to be considered
    output_folder

    Returns
    -------

    """
    output_folder = os.path.join(output_folder, 'cells_lifetime_variability', 'daughters_vs_mothers_lifetimes')
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    mother_names = []
    daughter_names = []
    lifetimes_per_mother = []
    lifetimes_per_daughters = []
    for mother_name, properties in divisions_properties.items():
        if properties.get('nb_of_measurements') < 5:
            continue

        d1, d2 = uname.get_daughters(mother_name)
        if divisions_properties[d1].get('nb_of_measurements')<5 and divisions_properties[d2].get('nb_of_measurements')<5:
            continue

        # get mother and daughter lifetimes
        m_lifetimes = []
        d_lifetimes = []
        # todo correct
        for lr, ind_prop in properties['division_mothers_lifetimes'].items():
            for embryo_name, lifetime in ind_prop.items():
                if lifetime is not None:

                    for d in [d1,d2]:
                        d_lifetime = divisions_properties[d]['division_mothers_lifetimes'][lr].get(embryo_name,None)
                        if d_lifetime:
                            # Get the alignment coefficients for the embryo
                            a, b = time_alignment_coefficients[embryo_name]
                            # Adjust lifetime using the alignment coefficients
                            adjusted_lifetime = a * d_lifetime
                            # Append adjusted lifetime to the list
                            d_lifetimes.append(adjusted_lifetime)

                            adjusted_lifetime = a * lifetime
                            # Append adjusted lifetime to the list
                            m_lifetimes.append(adjusted_lifetime)

                            mother_names.append(mother_name)
                            daughter_names.append(d)

        lifetimes_per_mother.append(m_lifetimes)
        lifetimes_per_daughters.append(d_lifetimes)
    output_file = os.path.join(output_folder, f'gen{g}.py')


def variability_cells_lifetime(divisions_properties, time_alignment_coefficients, gen=[7, 8, 9], output_folder=''):
    """
    Write a file that for each generation plots mother's boxplot of cells lifetimes
    Parameters
    ----------
    divisions_properties  (dict): A dictionary containing division properties.
               Example: {'mother_name': { 'division_mothers_lifetimes':
                                        {'left': {'individual name': lifetime (None if not valid)},
                                        'right': {'individual name': lifetime (None if not valid)} }
                                                    },
                                        'nb_of_measurements': int,
                                         {'division_aq_time': {'left/right': 'embryo name': value_to_adjusted},
                                         'gen': int }}
    time_alignment_coefficients (dict): {'embryo name': (a,b)}
    gen (list): mothers generations to be considered
    output_folder

    Returns
    -------

    """
    output_folder = os.path.join(output_folder, 'cells_lifetime_variability')
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for g in gen:
        # Filter divisions properties for the current generation
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == g}
        gen_divisions = dict(sorted(gen_divisions.items()))

        mother_names = []
        lifetimes_per_mother = []
        for mother_name, properties in gen_divisions.items():
            if properties.get('nb_of_measurements') < 5:
                continue
            mother_names.append(mother_name)
            lifetimes = []
            for lr, ind_prop in properties['division_mothers_lifetimes'].items():
                for embryo_name, lifetime in ind_prop.items():
                    if lifetime is not None:
                        # Get the alignment coefficients for the embryo
                        a, b = time_alignment_coefficients[embryo_name]
                        # Adjust lifetime using the alignment coefficients
                        adjusted_lifetime = a * lifetime
                        # Append adjusted lifetime to the list
                        lifetimes.append(adjusted_lifetime)
            lifetimes_per_mother.append(lifetimes)
        output_file = os.path.join(output_folder, f'gen{g}.py')
        write_lifetime_boxplot_script(g, mother_names, lifetimes_per_mother, output_file)


def write_lifetime_boxplot_script(gen, mother_names, lifetimes_per_mother, output_file):
    with open(output_file, "w") as f:
        f.write("import numpy as np\n")
        f.write("import matplotlib.pyplot as plt\n\n")

        f.write(f"gen = {gen}\n")
        f.write(f"mother_names = {mother_names}\n")
        f.write(f"lifetimes_per_mother = {lifetimes_per_mother}\n\n")

        f.write("# Plotting the boxplot\n")
        f.write("plt.figure(figsize=(10, 8))\n")
        f.write("plt.boxplot(lifetimes_per_mother, vert=True, patch_artist=True, labels=mother_names)\n")
        f.write(f"plt.title('Cell Lifetimes for Generation {gen}, developmental speeds compensated')\n")
        f.write("plt.xlabel('Mother Cells')\n")
        f.write("plt.ylabel('Lifetimes (2*min)')\n")
        f.write("plt.ylim(0, 100)\n")
        f.write("plt.xticks(rotation=90)\n")
        f.write("plt.tight_layout()\n")

        f.write("for i, lifetimes in enumerate(lifetimes_per_mother):\n")
        f.write(
            "    plt.text(i + 1, max(lifetimes) if lifetimes else 0, f'n={len(lifetimes)}', ha='center', va='bottom')\n")

        f.write(f"plt.savefig('{output_file.replace('.py', '.png')}', dpi=300)\n")
        f.write("plt.show()\n")

def adjust_time(time, alignment_coefficients):
        return time * alignment_coefficients[0] + alignment_coefficients[1]

def correlation_appearance_to_division_time(divisions_properties,generations, time_alignment_coefficients, output_folder=''):
    """
    Is the tapparance related to cell diviiosn time? (is cell lifetime related to cell appearance?)
    Supposing tappearance and tdivision are gaugssian processes
    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
               Example: {'mother_name': { 'division_mothers_lifetimes':
                                        {'left': {'individual name': lifetime (None if not valid)},
                                        'right': {'individual name': lifetime (None if not valid)} }
                                                    },
                                        'nb_of_measurements': int,
                                         {'division_aq_time': {'left/right': 'embryo name': value_to_adjusted},
                                         'gen': int }}
    time_alignment_coefficients (dict): {'embryo name': (a,b)}
    generations (list)
    output_folder

    Returns
    -------

    """

    output_folder = create_output_folder(output_folder, 'cells_lifetime_variability')
    output_folder = create_output_folder(output_folder, 'correlation_cells_appearance_to_division_time')



    for gen in generations:
        tappearance_all = []
        tdivision_all = []
        lifetimes_all = []

        cell_names = []
        nb_of_samples = []
        fates = []

        pvalues = []


        # Filter divisions properties for the current generation
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == gen}
        gen_divisions = dict(sorted(gen_divisions.items()))

        for cell_name, cell_data in gen_divisions.items():
            if cell_data['nb_of_measurements']<5:
                continue
            tappearance = []
            tdivision = []
            labels = []
            lifetimes = []

            nb_of_samples.append(cell_data['nb_of_measurements'])
            fates.append(determine_fate_group(cell_data['cell_fate']))
            print(cell_name, cell_data['cell_fate'], determine_fate_group(cell_data['cell_fate']))
            cell_names.append(cell_name)

            for dir in ['left', 'right']:
                for individual_name, lifetime in cell_data['division_mothers_lifetimes'][dir].items():
                    if lifetime is not None:
                        t_div = cell_data['division_aq_time'][dir][individual_name]
                        t_app = t_div - lifetime
                        t_app_adjusted = adjust_time(t_app, time_alignment_coefficients[individual_name])
                        t_div = adjust_time(t_div, time_alignment_coefficients[individual_name])
                        lifetime = lifetime * time_alignment_coefficients[individual_name][0]

                        tappearance.append(t_app_adjusted)
                        tdivision.append(t_div)
                        labels.append(individual_name+'_'+dir)
                        lifetimes.append(lifetime)

            # Normalize tappearance and tdivision
            tappearance_avg = np.mean(tappearance)
            tappearance_std = np.std(tappearance)
            tlifetime_avg = np.mean(lifetimes)
            tlifetime_std = np.std(lifetimes)
            tdivision_avg = np.mean(tdivision)
            tdivision_std = np.std(tdivision)

            tappearance_norm = [(t - tappearance_avg) for t in tappearance]
            tdivision_norm = [(t - tdivision_avg)  for t in tdivision]
            lifetimes_norm = [(t - tlifetime_avg)  for t in lifetimes]

            tappearance_norm = tappearance
            tdivision_norm = tdivision
            lifetimes_norm = lifetimes

            # Compute correlation coefficient and p-value
            correlation_coefficient, p_value = pearsonr(tappearance_norm, tdivision_norm, alternative='greater')
            title = f"pearsonr: {p_value}, gen{gen}"
            output_file = os.path.join(output_folder, f'{cell_name}.py')
            script_plot_correlation_cell_appearance_time_to_division_time(cell_name, tappearance_norm, tdivision_norm, title,
                                                                          output_file, labels)

            tappearance_all.extend(tappearance_norm)
            tdivision_all.extend(tdivision_norm)
            lifetimes_all.extend(lifetimes_norm)

            pvalues.append(p_value)

        # plot cels pvalues
        output_file = os.path.join(output_folder, f'plot_pvalues_gen{gen}.py')
        write_pvalue_plot_script(cell_names, pvalues, fates, gen, output_file)

        # not really useful just analysis at the cell level of each cell in different individuals

        # plot for all cells
        #correlation_coefficient, p_value = pearsonr(tappearance_all, tdivision_all)
        #title = f"Correlation coefficient for all cells: {correlation_coefficient}, pearsonr: {p_value}"
        #title = f" pearsonr: {p_value}"
        #output_file = os.path.join(output_folder, f'all_cells.py')
        #script_plot_correlation_cell_appearance_time_to_division_time(cell_name, tappearance_all, tdivision_all, title,
        #                                                              output_file)

        # plot lifetime vs tappearance
        #correlation_coefficient, p_value = pearsonr(tdivision_all, lifetimes_all)
        #title = f"Correlation coefficient(normalised lifetimes vs normalised tdiv) for all cells: {correlation_coefficient}, pearsonr: {p_value}"
        #output_file = os.path.join(output_folder, f'lifetimes_vs_tdiv_all_cells.py')
        #script_plot_correlation_cell_appearance_time_to_division_time(cell_name, tdivision_all, lifetimes_all, title,
        #                                                              output_file)


def write_pvalue_plot_script(cell_names, p_values, cell_fates, gen, output_folder):
    """
    Generates a Python script to plot p-values for each cell name.

    Parameters
    ----------
    cell_names (list): List of cell names.
    p_values (list): List of p-values corresponding to the cell names.
    fates (list): List of cells fates
    output_folder (str): Path to the folder where the script will be saved.

    Returns
    -------
    None
    """
    script_content = f"""
import os
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

cell_names = {cell_names}
p_values = {p_values}
cell_fates = {cell_fates}

legend_handles = []
box_colors = {{'Epid.': 'red', 'NS': 'blue', 'Mesoderm': 'green', 'End.': 'purple'}}

plt.figure(figsize=(12, 6))

# Plot each point with color according to its fate
for i, cell_name in enumerate(cell_names):
    fate = cell_fates[i]
    plt.plot(cell_name, p_values[i], 'o', markersize=5, color=box_colors.get(fate, 'black'))

plt.axhline(y=0.05, color='r', linestyle='--', label='Significance Level (0.05)')
plt.xlabel('Cell Names')
plt.ylabel('P-values')
plt.title('P-values for Each Cell Name (at least 5 samples for each cell), for gen{gen}')
plt.xticks(rotation=90)
plt.ylim(0, 1)
plt.yticks(list(plt.yticks()[0]) + [0.05, 0.10])
plt.grid(True)

# Create legend
for fate, color in box_colors.items():
    legend_handles.append(mpatches.Patch(color=color, label=fate))

plt.legend(handles=legend_handles, title='Cell Fates')
plt.tight_layout()

#output_path = os.path.join(r'{output_folder}', 'pvalue_plot.png')
#plt.savefig(output_path)
plt.show()
#plt.close()
#print(f"P-value plot saved to {{output_path}}")
"""
    with open(output_folder, 'w') as script_file:
        script_file.write(script_content)

    print(f"Python script for plotting p-values saved to {output_folder}")


def script_plot_correlation_cell_appearance_time_to_division_time(cell_name, tapp, tdiv, title, output_file, labels=[]):
    # title has cell name, correlation_coefficient, pvalue
    with open(output_file, "w") as file:
        file.write(f"""
# put plot in file: 
import matplotlib.pyplot as plt

tapp ={tapp}
tdiv = {tdiv}

plt.figure(figsize=(6, 4))
plt.scatter({tapp}, {tdiv}, color='b')

labels = {labels}
if labels:
    for i, label in enumerate(labels):
        plt.annotate(label, (tapp[i], tdiv[i]), textcoords="offset points", xytext=(5,-5), ha='center')


plt.xlabel('Tappearance (dev time)')
plt.ylabel('Tdivision (dev time)')
plt.title(f'{title} for cell {cell_name}')
plt.grid(True)
plt.show()""")

###################
################################################
##############

def distance_matrices_based_on_cell_div_order(divisions_properties, time_alignment_coefficients, cells_per_time_in_ref,
                                              gen=[7, 8, 9], cumulatif=False, output_folder=''):
    """
    I want to add a function that computes the order of division between the individuals:

    for each individual
    ei = (c1,c2,,,,cn) vector of cells division times in ei
    d(ei,ej) = sum of deltat_{n,k} for all combinations of n,k
    deltat_{n,k} = abs ( (t^ei_cj - t^ei_ck ) - (t^ej_cj - t^ej_ck ) )
    t^ei_cj : time of division of cell cj in ei

    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
           Example: {'mother_name': 'nb_of_measurements': int,
                                     'division_aq_time': {'left/right': 'embryo name': value_to_adjusted},
                                     'gen': int }
    time_alignment_coefficients { 'individua name': (a,b) } times in this indiivduals are to be adjusted: tadjusted = t*a+b
    cells_per_time_in_ref
    gen
    cumulatif
    output_folder

    Returns
    -------

    """
    folder_suffix = 'divisions_order'
    output_folder = create_output_folder(output_folder, folder_suffix)
    dev_stage_mapping = get_dev_stage_mapping(cells_per_time_in_ref)
    dev_times = list(dev_stage_mapping.keys())

    deviations = {}
    for g in gen:
        gen_divisions = get_gen_divisions(divisions_properties, g)
        gen_divisions = dict(sorted(gen_divisions.items()))
        key = 'division_aq_time'
        individuals = extract_individuals(gen_divisions, key)
        generation_data = (
        gen_divisions, individuals, dev_times, deviations, time_alignment_coefficients, dev_stage_mapping, cumulatif,
        output_folder, f"gen_{g}")

        n = len(individuals)
        for i, dev_time in enumerate(dev_times):
            if i == 0 or i == len(dev_times) - 1:
                continue

            same_dimension = True if i != 1 else False
            # mothers in common in considered dev time range
            mothers_in_common = get_mothers_in_common(individuals, gen_divisions, dev_time, dev_times,
                                                      time_alignment_coefficients, cumulatif, key, same_dimension)

            # Compute the deviation matrix at this developmental time
            n = len(individuals)
            deviation_matrix = np.zeros((n, n))
            for ind1, ind2 in itertools.combinations_with_replacement(individuals, 2):
                differences = []
                for mother_name in mothers_in_common:
                    # div_times1 has times of each mother in individual ind1
                    div_times1 = [adjust_time(div_time, time_alignment_coefficients[ind1.split('_')[0]])
                                  for div_time in gen_divisions[mother_name][key][ind1.split('_')[1]].get(ind1.split('_')[0], None)]
                    # div_times2 has times of each mother in individual ind2
                    div_times2 = [adjust_time(div_time, time_alignment_coefficients[ind2.split('_')[0]])
                                  for div_time in gen_divisions[mother_name][key][ind2.split('_')[1]].get(ind2.split('_')[0], None)]

                    combinations = compare_division_times(div_times1, div_times2)

                    for (t1_i, t1_j), (t2_i, t2_j) in combinations:
                        deltat_nk = np.abs((t1_i - t1_j) - (t2_i - t2_j))
                        differences.append(deltat_nk)

                if differences:
                    total_difference = np.sum(differences)
                    deviation_matrix[individuals.index(ind1), individuals.index(ind2)] = total_difference
                    deviation_matrix[individuals.index(ind2), individuals.index(ind1)] = total_difference

            # plot dev matrix at t
            deviations[dev_time] = deviation_matrix
            title = f"'Distance Matrix of Deviations Between Individuals, for division of wave after {dev_stage_mapping.get(dev_time)} cell stage (generation_{gen})'"
            output_file = f"{output_folder}/distance_matrix_after_{dev_time}_generation_{gen}.py"
            print('plot in', output_file)
            script_distance_matrix_individuals_deviations_at_t(deviation_matrix, individuals, title,
                                                               len(mothers_in_common),
                                                               output_file)

    individuals = extract_individuals(divisions_properties, key)
    generation_data = (
    divisions_properties, individuals, dev_times, deviations, time_alignment_coefficients, dev_stage_mapping, cumulatif,
    output_folder, "all_generations")

###################
#########
####

def create_output_folder(base_folder, sub_folder):
    output_folder = os.path.join(base_folder, sub_folder)
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    return output_folder


def get_dev_stage_mapping(cells_per_time_in_ref):
    same_cell_stages_dev_times = average_t_for_cells(cells_per_time_in_ref, [64, 76, 112, 218])
    return {
        same_cell_stages_dev_times[0]: 'start_of_aq',
        same_cell_stages_dev_times[1]: 64,
        same_cell_stages_dev_times[2]: 76,
        same_cell_stages_dev_times[3]: 112,
        same_cell_stages_dev_times[4]: 218,
        same_cell_stages_dev_times[5]: 'end_of_aquisition'
    }


def extract_individuals(properties, key):
    individuals = set()
    for mother_props in properties.values():
        individuals.update(
            [f"{ind}_left" for ind in mother_props[key]['left'].keys()])
        individuals.update(
            [f"{ind}_right" for ind in mother_props[key]['right'].keys()])
    return sorted(list(individuals))


def get_gen_divisions(divisions_properties, generation):
    return {mother_name: properties for mother_name, properties in divisions_properties.items() if
            properties.get('gen') == generation}


def compute_deviation_matrix(individuals, divisions_properties, dev_time, dev_times, time_alignment_coefficients,
                             mothers_in_common, cumulatif, key, compute_difference):
    n = len(individuals)
    deviation_matrix = np.zeros((n, n))
    for ind1, ind2 in itertools.combinations_with_replacement(individuals, 2):
        #print(ind1,ind2)
        differences = []
        for mother_name, mother_props in divisions_properties.items():
            avg_div_time = compute_avg_division_time(mother_props.get('division_aq_time', {}),
                                                     time_alignment_coefficients)
            if mother_name not in mothers_in_common:
                continue
            if cumulatif:
                if avg_div_time > dev_times[dev_times.index(dev_time) + 1]:
                    continue
            else:
                if dev_time > avg_div_time or avg_div_time > dev_times[dev_times.index(dev_time) + 1]:
                    continue

            value1 = mother_props[key][ind1.split('_')[1]].get(ind1.split('_')[0], None)
            value2 = mother_props[key][ind2.split('_')[1]].get(ind2.split('_')[0], None)

            if value1 is not None and value2 is not None:
                # adjust times
                if key == 'division_aq_time':
                    value1 = adjust_time(value1, time_alignment_coefficients[ind1.split('_')[0]])
                    value2 = adjust_time(value2, time_alignment_coefficients[ind2.split('_')[0]])
                #if( ind1 == 'Asaspe1_right' or ind2== 'Asaspe1_right') and (
                #        ind1 == 'louis_right' or ind2 == 'louis_right'):
                #    print('distance', mother_name, int(compute_difference(value1, value2)))
                differences.append(compute_difference(value1, value2))

        if differences:
            deviation_matrix[individuals.index(ind1), individuals.index(ind2)] = np.sum(differences)
            deviation_matrix[individuals.index(ind2), individuals.index(ind1)] = np.sum(differences)

    return deviation_matrix


def get_mothers_in_common(individuals, divisions_properties, dev_time, dev_times, time_alignment_coefficients,
                          cumulatif, key, same_dimension=True):
    mothers_list_of_sets = []
    for ind1, ind2 in itertools.combinations_with_replacement(individuals, 2):
        mothers = set()
        for mother_name, mother_props in divisions_properties.items():
            avg_div_time = compute_avg_division_time(mother_props.get('division_aq_time', {}),
                                                     time_alignment_coefficients)
            if cumulatif:
                if avg_div_time > dev_times[dev_times.index(dev_time) + 1]:
                    # take all previous waves divisions
                    continue
            else:
                if dev_time > avg_div_time or avg_div_time > dev_times[dev_times.index(dev_time) + 1]:
                    # take only the considered waves divisions
                    continue

            value1 = mother_props[key][ind1.split('_')[1]].get(ind1.split('_')[0], None)
            value2 = mother_props[key][ind2.split('_')[1]].get(ind2.split('_')[0], None)

            if value1 is not None and value2 is not None:
                mothers.update([mother_name])
        mothers_list_of_sets.append(mothers)

    if same_dimension:
        return set.intersection(*mothers_list_of_sets)
    else:
        return set.union(*mothers_list_of_sets)

def compute_deviation_matrix_for_gen(generation_data, key, compute_difference):
    gen_divisions, individuals, dev_times, deviations, time_alignment_coefficients, dev_stage_mapping, cumulatif, output_folder, generation_label = generation_data
    n = len(individuals)
    for i, dev_time in enumerate(dev_times):
        if i == 0 or i == len(dev_times) - 1:
            continue

        same_dimension = True if i != 1 else False
        same_dimension = False
        mothers_in_common = get_mothers_in_common(individuals, gen_divisions, dev_time, dev_times,
                                                  time_alignment_coefficients, cumulatif, key, same_dimension )

        # lenght(mothers_in_common) is the emberyo dimension
        print('dimension at', dev_time, ':', len(mothers_in_common))

        deviation_matrix = compute_deviation_matrix(individuals, gen_divisions, dev_time, dev_times,
                                                    time_alignment_coefficients, mothers_in_common, cumulatif, key,
                                                    compute_difference)

        deviations[dev_time] = deviation_matrix
        title = f"'Distance Matrix of Deviations Between Individuals, for division of wave after {dev_stage_mapping.get(dev_time)} cell stage (gen {generation_label}){'_cumulative' if cumulatif else ''}'"
        output_file = f"{output_folder}/distance_matrix_after_{dev_time}_{generation_label}{'_cumulative' if cumulatif else ''}.py"
        print('plot in', output_file)
        script_distance_matrix_individuals_deviations_at_t(deviation_matrix, individuals, title, len(mothers_in_common),
                                                           output_file)
        # plot distance matrix mds
        title = f"'Distance Matrix MDS of Deviations Between Individuals, for division of wave after {dev_stage_mapping.get(dev_time)} cell stage (gen {generation_label}){'_cumulative' if cumulatif else ''}'"
        output_file = f"{output_folder}/mds_after_{dev_time}_{generation_label}{'_cumulative' if cumulatif else ''}.py"
        script_distance_matrix_individuals_mds(deviation_matrix, individuals, title, len(mothers_in_common),
                                               output_file)


def compute_dmatrix(divisions_properties, time_alignment_coefficients, cells_per_time_in_ref, gen, cumulatif, output_folder, key, compute_difference):
    folder_suffix = (
        'deviation_btw_individuals_overtime' if key == 'division_directions_after_symmetry' else
        'dmatrix_divisions_time' if key == 'division_aq_time' else
        'dmatrix_lifetimes' if key == 'division_mothers_lifetimes' else
        'default_suffix'
    )
    output_folder = create_output_folder(output_folder, folder_suffix)
    dev_stage_mapping = get_dev_stage_mapping(cells_per_time_in_ref)
    dev_times = list(dev_stage_mapping.keys())
    print('dev times', dev_stage_mapping )

    deviations = {}
    for g in gen:
        gen_divisions = get_gen_divisions(divisions_properties, g)
        gen_divisions = dict(sorted(gen_divisions.items()))
        individuals = extract_individuals(gen_divisions, key)
        generation_data = (gen_divisions, individuals, dev_times, deviations, time_alignment_coefficients, dev_stage_mapping, cumulatif, output_folder, f"gen_{g}")
        compute_deviation_matrix_for_gen(generation_data, key, compute_difference)

    individuals = extract_individuals(divisions_properties, key)
    generation_data = (divisions_properties, individuals, dev_times, deviations, time_alignment_coefficients, dev_stage_mapping, cumulatif, output_folder, "all_generations")
    compute_deviation_matrix_for_gen(generation_data, key, compute_difference)


def compute_dmatrix_based_on_cells_division_time_overtime(divisions_properties, time_alignment_coefficients, cells_per_time_in_ref, gen=[7, 8, 9], cumulatif=False, output_folder=''):
    def division_time_difference(time1, time2):
        return np.abs(time2 - time1)

    compute_dmatrix(divisions_properties, time_alignment_coefficients, cells_per_time_in_ref, gen, cumulatif, output_folder, 'division_aq_time', division_time_difference)

def compute_dmatrix_based_on_cells_lifetimes_overtime(divisions_properties, time_alignment_coefficients, cells_per_time_in_ref, gen=[7, 8, 9], cumulatif=False, output_folder=''):
    def lifetime_difference(time1, time2):
        return np.abs(time1 - time2)

    compute_dmatrix(divisions_properties, time_alignment_coefficients, cells_per_time_in_ref, gen, cumulatif, output_folder, 'division_mothers_lifetimes', lifetime_difference)

def compute_dmatrix_based_on_division_deviation_overtime(divisions_properties, time_alignment_coefficients,
                                            cells_per_time_in_ref, gen=[7, 8, 9], cumulatif=False, output_folder=''):
    """
   Writes a file that plot the deviation of division directions between different individuals
   the distance is at each time, its the average of deviation values between both individuals
   we expect left right of te same embryo to be closest to each others
   The considered generation can be specified
   Parameters:
   divisions_properties (dict): A dictionary containing division properties.
           Example: {'mother_name': { 'division_directions_after_symmetry':
                                    {'left': {'individual name': direction vector},
                                    'right': {'individual name': direction vector} }
                                                },
                                    'nb_of_measurements': int,
                                     {'division_aq_time': {'left/right': 'embryo name': value_to_adjusted},
                                     'gen': int }}
   time_alignment_coefficients
   same_cell_stages_dev_times (list of int): defines the dev times of the same cell stages
                                            0: 64, 15: 76, 35: 112, 120: to design all divisions after
   cumulatif : (bool) is set to true, it will accumulate measures from
   output_folder
   Returns
   -------

    """
    def angle_difference(vec1, vec2):
        vec1 = np.array(vec1) / np.linalg.norm(vec1)
        vec2 = np.array(vec2) / np.linalg.norm(vec2)
        return angle_btw_vectors(vec1, vec2)

    compute_dmatrix(divisions_properties, time_alignment_coefficients, cells_per_time_in_ref, gen, cumulatif, output_folder, 'division_directions_after_symmetry', angle_difference)

###################

###########################################
#####################
def script_boxplot_individuals_deviations(angles, ind1_name, ind2_name, dev_time, output_folder):
    """
    Write python script that plot boxplot of values between individuals at a given dev time
    It will tell us to which extent the two individuals have similar divisions at a certain dev time(after a division wave)
    Parameters
    ----------
    angles
    ind1_name
    ind2_name
    dev_time: (int) cell avg div time
    output_folder

    Returns
    -------

    """
    output_folder = f"{output_folder}/deviation_btw_individuals_overtime/"
    os.makedirs(output_folder, exist_ok=True)
    output_file = f"{output_folder}/{ind1_name}_{ind2_name}_{dev_time}_boxplot.py"
    with open(output_file, "w") as file:
        file.write(f"""
import matplotlib.pyplot as plt

# Boxplot of deviation values between individuals {ind1_name} and {ind2_name} at time {dev_time}
plt.figure()
plt.boxplot({angles})
plt.title("Boxplot of Deviation Between {ind1_name} and {ind2_name} at Dev Time {dev_time}")
plt.xlabel("Individual Pair")
plt.ylabel("Deviation Angle (degrees)")
plt.ylim(0,180)
plt.show()
""")


def script_distance_matrix_individuals_mds(deviations_matrix_at_t, individuals, title, embryo_dimension, output_file):
    """
    Write python file that plots distance matrix at a dev_time using MDS
    It will tell us which individuals have more (on average) closer deviations
    Parameters
    ----------
    deviations_matrix_at_t : ndarray
        n*n of avg angular values
    individuals : list
    title : str
    embryo_dimension : int
    output_file : str

    Returns
    -------
    None
    """

    deviations_matrix_list = [
        [deviations_matrix_at_t[individuals.index(ind1)][individuals.index(ind2)] for ind2 in individuals] for ind1 in
        individuals]

    with open(output_file, "w") as file:
        file.write(f"""
import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import MDS


individuals = {individuals}
# Assign colors to embryos
embryos = list(set([ind.split('_')[0] for ind in individuals]))
colors = plt.cm.get_cmap('tab10', len(embryos))
embryo_color_map = {{embryo: colors(i) for i, embryo in enumerate(embryos)}}

# Prepare the deviation matrix
global_avg_distances_matrix = np.array({deviations_matrix_list})
normalized_distance_matrix = global_avg_distances_matrix / {embryo_dimension}
np.fill_diagonal(normalized_distance_matrix, 0)

# Perform MDS
mds = MDS(n_components=2, dissimilarity="precomputed", random_state=42)
mds_coords = mds.fit_transform(normalized_distance_matrix)

# Assign colors to embryos
embryo_color_map = embryo_color_map

# Plotting the MDS result
plt.figure(figsize=(12, 12))
for i, (x, y) in enumerate(mds_coords):
    embryo = individuals[i].split('_')[0]
    color = embryo_color_map[embryo]
    plt.scatter(x, y, color=color, label=embryo if i == 0 or individuals[i-1].split('_')[0] != embryo else "")
    plt.text(x, y, individuals[i], fontsize=12, ha='right', color=color)

# Create a legend
handles, labels = plt.gca().get_legend_handles_labels()
by_label = dict(zip(labels, handles))
plt.legend(by_label.values(), by_label.keys(), loc='best', fontsize=12)

plt.title({title})
plt.xlabel("MDS Dimension 1")
plt.ylabel("MDS Dimension 2")
plt.grid(True)

plt.tight_layout()
plt.savefig("{output_file[:-3]}_mds.png", dpi=300)
plt.show()
""")


# make output file instead of folder
def script_distance_matrix_individuals_deviations_at_t(deviations_matrix_at_t, individuals, title, embryo_dimension, output_file):
    """
    Write python file that plots distance matrix at a dev_time
    It will tell us which individuals have more (on average) closer deviations
    Parameters
    ----------
    deviations_matrix_at_t ndarray n*n of avg angular values
    individuals (list)
    dev_time
    output_file

    Returns
    -------

    """

    deviations_matrix_list = [
        [deviations_matrix_at_t[individuals.index(ind1)][individuals.index(ind2)] for ind2 in individuals] for ind1 in
        individuals]

    with open(output_file, "w") as file:
        file.write(f"""
import numpy as np
import matplotlib.pyplot as plt
import scipy.cluster.hierarchy as sch
from scipy.spatial.distance import squareform

global_avg_distances_matrix = np.array({deviations_matrix_list})
normalized_distance_matrix = global_avg_distances_matrix / {embryo_dimension}
np.fill_diagonal(normalized_distance_matrix, 0)
individual_names = {individuals}


condensedD = squareform(global_avg_distances_matrix)
fig = plt.figure(figsize=(12, 12))
ax1 = fig.add_axes([0.09, 0.1, 0.2, 0.8])
Y1 = sch.linkage(condensedD, method='complete')
Y1 = sch.optimal_leaf_ordering(Y1, condensedD)
Z1 = sch.dendrogram(Y1, orientation='left')

ax1.set_xticks([])
ax1.set_yticks([])
ax1.set_yticklabels([])
idx1 = Z1['leaves']
D = normalized_distance_matrix[idx1, :]
D = D[:, idx1]
min_val = np.min(D[np.nonzero(D)])
max_val = np.max(D)
axmatrix = fig.add_axes([0.3, 0.1, 0.6, 0.8])
im = axmatrix.matshow(D, aspect='auto', origin='lower', cmap='viridis', vmin=min_val, vmax=max_val)
axmatrix.xaxis.set_ticks_position('bottom')
axmatrix.xaxis.set_label_position('bottom')
axmatrix.set_xticks(range(len(idx1)))
axmatrix.set_xticklabels([individual_names[i] for i in idx1], rotation=90, fontsize=14)
axmatrix.set_yticks(range(len(idx1)))
axmatrix.set_yticklabels([individual_names[i] for i in idx1], fontsize=14)
axcolor = fig.add_axes([0.91, 0.1, 0.02, 0.8])
cbar = plt.colorbar(im, cax=axcolor, label='Average Distance')
cbar.set_ticks([min_val, max_val])

axmatrix.set_title({title}, pad=20, fontsize=14)
axmatrix.title.set_position([0.5, 1.05])

plt.tight_layout()
plt.savefig("{output_file[:-3]}.png", dpi=300)
plt.show()     
""")

"""
import matplotlib.pyplot as plt
import numpy as np

deviations_matrix_at_t = np.array({deviations_matrix_list})
deviations_matrix_at_t = deviations_matrix_at_t/ {embryo_dimension}
individuals = {individuals}

plt.figure()
plt.imshow(deviations_matrix_at_t, cmap='viridis', origin='upper', interpolation='nearest', vmin=0, vmax=np.max(deviations_matrix_at_t))
plt.colorbar(label='Avg Deviation Angle (degrees)')

for i in range(len(individuals)):
    for j in range(len(individuals)):
        plt.text(j, i, f'{{deviations_matrix_at_t[i][j]:.2f}}', ha='center', va='center', color='white', size=10)

plt.title({title})
plt.xticks(np.arange(len(individuals)), individuals, rotation=90)
plt.yticks(np.arange(len(individuals)), individuals)
plt.xlabel("Individual")
plt.ylabel("Individual")
plt.tight_layout()
plt.savefig("{output_file[:-3]}.png", dpi=300)
plt.show()     
"""


####################
################
########
###

def plot_sister_plane_deviation_to_barycenter_axis(divisions_properties, generations, output_folder):
    """
    Compare approximated divison direction (axis between cells barycenters) to vector normal to plane between daughters
    (approximated using 3 point junctions between daughters)
    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
                                   Example: {
                                    'mother_name': {
                                    'division_directions_after_symmetry':
                                        {'left': {'individual name': direction vector},
                                        'right': {'individual name': direction vector} }
                                                    },
                                    'cell_vector_normal_to_sister_interface' : {'left': {'individual name': direction vector},
                                        'right': {'individual name': direction vector} }
                                                    }
                                           }
    generations (list): List of generations to include in the plot.
    output_folder (str): where the python script that does the plot will be placed

    Returns
    -------

    """
    for gen in generations:
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == gen}

        mothers_name_list = []
        angles = []
        divisions_volume_ratio = []
        avg_div_ratios = []

        for division_name, mother_props in gen_divisions.items():
            mother_angles = []
            div_volume_ratio = []
            for lr, dir_div_props in mother_props['division_directions_before_registration'].items():
                for ind, div_dir in dir_div_props.items():
                    # get vector normal to daughters plane
                    if mother_props['sisters_interface_vectors'] is not None:
                        lr_dict = mother_props['sisters_interface_vectors']
                        if lr in lr_dict and lr_dict[lr] is not None:
                            ind_dict = lr_dict[lr]
                            if ind in ind_dict and ind_dict[ind] is not None:
                                n_dir = ind_dict[ind][2]
                    # vector of division: dir div_dir
                    # get angle between
                    n_dir = np.array(n_dir)
                    n_dir = n_dir.reshape(3, )
                    angle = angle_vector_to_direction(n_dir, div_dir)
                    mother_angles.append(angle)
                    div_volume_ratio.append(mother_props['division_volume_ratio'][lr][ind])

                    # if division_name == 'a7.0002':
                    #    print(ind,lr,angle,mother_props['division_volume_ratio'][lr][ind] )

                    # todo write high values in log file
                    if angle > 20:
                        print(division_name, ind, lr, angle)
            angles.append(mother_angles)
            # get mother name
            mothers_name_list.append(division_name)
            divisions_volume_ratio.append(div_volume_ratio)
            avg_div_ratios.append(np.nanmean(np.array(div_volume_ratio)))

        # plot boxplot script
        write_script_plot_sister_plane_deviation_to_barycenter_axis(mothers_name_list, angles, gen, output_folder)
        write_script_plot_sister_plane_deviation_to_barycenter_axis_with_volumes_ratios(mothers_name_list, angles,
                                                                                        divisions_volume_ratio,
                                                                                        avg_div_ratios, gen,
                                                                                        output_folder)


def write_script_plot_sister_plane_deviation_to_barycenter_axis_with_volumes_ratios(mothers_name_list, angles,
                                                                                    divisions_volume_ratio,
                                                                                    avg_div_ratios,
                                                                                    gen, output_folder):
    output_folder = os.path.join(output_folder, "sister_plane_deviation_to_barycenter_axis")
    os.makedirs(output_folder, exist_ok=True)
    output_file = os.path.join(output_folder, f"gen_{gen}_with_volume_ratios.py")
    not_sorted_mothers = mothers_name_list
    mothers_name_list, angles = sort_mother_cells(mothers_name_list, angles)
    mothers_name_list, divisions_volume_ratio = sort_mother_cells(not_sorted_mothers, divisions_volume_ratio)
    mothers_name_list, avg_div_ratios = sort_mother_cells(not_sorted_mothers, avg_div_ratios)
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt

angles = {angles}
mothers_name_list = {mothers_name_list}
divisions_volume_ratio = {divisions_volume_ratio}
avg_div_ratios = {avg_div_ratios}

# Create subplots with two rows
fig, axs = plt.subplots(2, 1, figsize=(10, 12))

# Plot boxplot of deviation angles
axs[0].boxplot(angles)
axs[0].set_xlabel('Mother Name')
axs[0].set_ylabel('Deviation Angle (degrees)')
axs[0].set_title('Boxplot of Deviation Angle between Approximated Division Direction and Vector Normal to Daughters Plane at division time')
axs[0].set_xticks(range(1, len(mothers_name_list) + 1))
axs[0].set_xticklabels(mothers_name_list, rotation=45, ha='right')
axs[0].set_ylim(0, 90)

axs_avg_div_ratios = axs[0].twinx()
axs_avg_div_ratios.plot(range(1, len(mothers_name_list) + 1), avg_div_ratios, color='r', marker='o')
axs_avg_div_ratios.set_ylabel('Average Division Ratios', color='r')

# Plot boxplot of division volume ratio
axs[1].boxplot(divisions_volume_ratio)
axs[1].set_xlabel('Mother Name')
axs[1].set_ylabel('Division Volume Ratio')
axs[1].set_title('Boxplot of Division Volume Ratio')
axs[1].set_xticks(range(1, len(mothers_name_list) + 1))
axs[1].set_xticklabels(mothers_name_list, rotation=45, ha='right')

plt.tight_layout()
plt.show()
""")


def write_script_plot_sister_plane_deviation_to_barycenter_axis(mothers_name_list, angles, gen, output_folder):
    output_folder = os.path.join(output_folder, "sister_plane_deviation_to_barycenter_axis")
    os.makedirs(output_folder, exist_ok=True)
    output_file = os.path.join(output_folder, f"gen_{gen}.py")
    mothers_name_list, angles = sort_mother_cells(mothers_name_list, angles)
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt

angles = {angles}
mothers_name_list = {mothers_name_list}

# Plot boxplot
plt.figure(figsize=(10, 6))
plt.boxplot(angles)
plt.xlabel('Mother Name')
plt.ylabel('Deviation Angle (degrees)')
plt.title('Boxplot of Deviation Angle between Approximated Division Direction and Vector Normal to Daughters Plane at division time')
plt.xticks(ticks=range(1, len(mothers_name_list) + 1), labels=mothers_name_list, rotation=45, ha='right')
plt.tight_layout()
plt.ylim(0,90)
plt.show()
""")


def plot_deviation_of_division_direction_from_mean_to_division_order(divisions_properties, time_alignment_coefficients,
                                                                     mother, output_folder):
    """
    Does order of divisions influence deviation from mean of division direction
    Division have average division time (in terms of developmental time)

    When a cell deviation from avg developmental time (standardized by std_t)
        => it hasn't divided in the same order as its homologous cells
    Returns
    -------

    """
    individuals_list = []
    t_deviation_from_mean = []
    d_deviation_from_mean = []

    mother_props = divisions_properties[mother]

    # cell avg div time (developmental time)
    avg_div_time = compute_avg_division_time(mother_props.get('division_aq_time', {}), time_alignment_coefficients)
    # cell std division time (developmental time)
    std_t = compute_std_t(mother_props.get('division_aq_time', {}), time_alignment_coefficients)
    # cell divison time (in terms of dev time) : in every individual
    for lr, ind_prop in mother_props.get('division_aq_time', {}).items():
        for embryo_name, aq_time in ind_prop.items():
            t_dev = align_time(aq_time, time_alignment_coefficients, embryo_name)
            t_dev_from_mean = (t_dev - avg_div_time) / std_t

            direction_vector = mother_props['division_directions_after_symmetry'][lr][embryo_name]
            deviation_from_mean = compute_std_distance(direction_vector,
                                                       mother_props['division_mean_direction'],
                                                       mother_props['std_from_mean_directions'])

            t_deviation_from_mean.append(t_dev_from_mean)
            d_deviation_from_mean.append(deviation_from_mean)
            individuals_list.append(embryo_name + lr)

    output_folder = os.path.join(output_folder, "mother_deviation_to_division_order")
    os.makedirs(output_folder, exist_ok=True)
    # write script to do the mother plot
    write_mother_direction_deviation_to_order_dif_plot_script(individuals_list, t_deviation_from_mean,
                                                              d_deviation_from_mean, mother, output_folder)


def write_mother_direction_deviation_to_order_dif_plot_script(individuals_list, t_deviation_from_mean,
                                                              d_deviation_from_mean, mother, output_folder):
    """
    Writes a Python script to plot the deviation of division direction from mean to division order for a mother cell.
    """

    output_file = os.path.join(output_folder, f"{mother}.py")

    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt

individuals_list = {individuals_list}
t_deviation_from_mean = {t_deviation_from_mean}
d_deviation_from_mean = {d_deviation_from_mean}

plt.figure(figsize=(10, 6))
plt.scatter(t_deviation_from_mean, d_deviation_from_mean, s=100)
for i, individual in enumerate(individuals_list):
    plt.text(t_deviation_from_mean[i], d_deviation_from_mean[i], individual,fontsize=8)
plt.xlabel('Divison Time Deviation from Mean (standarized, in terms of developmental time)')
plt.ylabel('Division Direction Deviation from Mean(standarized)')
plt.title('Deviation of Division {mother} Direction from Mean to Division Order')
plt.xticks(rotation=45, ha='right')
plt.tight_layout()
plt.ylim(0,5)
plt.xlim(-2,2)
plt.show()
""")


#####################################################################################################################
# todo this block needs adjustments, delete what aint needed
#####################################################################################################################
##########################################################################################

def classify_divison(individual_div_time, neighbor_div_time):
    """
    Currntley (at cell division time in individual) is the neighbor in mitosis or interphase
    """
    time_diff_a = 1 if (neighbor_div_time - individual_div_time) > 8 else 0  # neighbor in interphase
    time_diff_b = 1 if individual_div_time - 2 < neighbor_div_time < individual_div_time + 8 else 0  # neighbors in mitose
    return time_diff_a, time_diff_b


def calculate_weighted_division_sum(division_time_diffs, sum_contact_surfaces):
    division_time_diffs = np.array(division_time_diffs)
    return sum(division_time_diffs) / sum_contact_surfaces if sum_contact_surfaces > 0 else 0


def extract_individual_div_time(neighbor_name, individual, divisions_properties):
    """
    Pulls cells divison time from division properties
    """
    if neighbor_name[:-1] in divisions_properties:
        if neighbor_name[-1] == '_':
            d = 'left'
        elif neighbor_name[-1] == '*':
            d = 'right'
        neighbor_div_time = divisions_properties[neighbor_name[:-1]].get('division_aq_time').get(d).get(individual)
        return neighbor_div_time
    return None


def plot_direction_deviation_vs_neighbors_life_cycle(divisions_properties, mother, output_folder=''):
    """
    To compare cell deviation of division direction from mean direction vs nb of neighboring cells in interphase state
    ( they will divide in the future, range and window to be adjusted)
    vs nb of cells dividing at the same time
    vs nb of cells that already divided (should have a correlation but allows to show if we're doing a random analysis)
    the nb of neighbors will be weighted by neighbor;'s surface of contact
        Parameters
        ----------
        divisions_properties  (dict): A dictionary containing division properties.
                                   Example: {
                                    'mother_name': {
                                    'division_directions_after_symmetry':
                                        {'left': {'individual name': direction vector},
                                        'right': {'individual name': direction vector} }
                                                    },
                                    'gen': int,
                                    'std_from_mean_directions': std_a,
                                    'division_mean_direction': mean_direction,
                                    'neighbors':{'direction':{'individual': {'neighbor_name':{'surface_of_contact':value }}} ,
                                        } }
        mother (str): list of divisions mother names to be considered
        output_folder (str): where the python script that does the plot will be placed
        window (float): Window of time to look for cells that divided before or after:
                        cells in time window=cells divided at same time of considered cell
        interphasic_range (float): Range of time considered for interphase state after window/2.

        Returns
        -------

        """
    mother_props = divisions_properties[mother]

    delta_a = []
    delta_b = []
    direction_deviations = []
    individual_name = []

    for direction, indiv_props in mother_props['division_directions_after_symmetry'].items():
        for individual, direction_vector in indiv_props.items():
            division_time_dif_a = []
            division_time_dif_b = []
            sum_contact_surfaces = 0

            individual_div_time = mother_props.get('division_aq_time', {}).get(direction).get(individual)
            deviation_from_mean = compute_std_distance(direction_vector,
                                                       mother_props['division_mean_direction'],
                                                       mother_props['std_from_mean_directions'])

            for neighbor_name, surface_of_contact in mother_props['neighbors'][direction][individual].items():
                if "Unknown Neighbor" in neighbor_name:
                    continue

                sum_contact_surfaces += surface_of_contact

                neighbor_div_time = extract_individual_div_time(neighbor_name, individual, divisions_properties)

                if neighbor_div_time is not None:
                    time_diff_a, time_diff_b = classify_divison(  # is it in mitosis, interphase, devided before
                        individual_div_time, neighbor_div_time)
                    division_time_dif_a.append(time_diff_a * surface_of_contact)
                    division_time_dif_b.append(time_diff_b * surface_of_contact)

            delta_a.append(calculate_weighted_division_sum(division_time_dif_a, sum_contact_surfaces))
            delta_b.append(calculate_weighted_division_sum(division_time_dif_b, sum_contact_surfaces))
            direction_deviations.append(deviation_from_mean)
            individual_name.append(individual + '_' + direction)

    write_script_to_plot_direction_deviation_vs_div_times(mother, delta_a, delta_b, direction_deviations,
                                                          individual_name, output_folder, "interphasic")


##########################################################################################
def plot_direction_deviation_vs_neighbors_div_times(divisions_properties, mother, window, output_folder):
    """
    To compare cell deviation of division direction from mean direction vs nb of neighboring cells dividing in similar
     time range : np.exp(-np.abs(neighbor_div_time - individual_div_time) ** 2 / window)

    The impact of cells dividing before is expected to be different from those that will divide after
    Parameters
    ----------
    divisions_properties  (dict): A dictionary containing division properties.
                               Example: {
                                'mother_name': {
                                'division_directions_after_symmetry':
                                    {'left': {'individual name': direction vector},
                                    'right': {'individual name': direction vector} }
                                                },
                                'gen': int,
                                'std_from_mean_directions': std_a,
                                'division_mean_direction': mean_direction,
                                'neighbors':{'direction':{'individual': {'neighbor_name':{'surface_of_contact':value }}} ,
                                    } }
    mother (str): list of divisions mother names to be considered
    output_folder (str): where the python script that does the plot will be placed
    window (int): neighbors division time window (sigma of gaussian centered at cell division time)
    Returns
    -------

    """
    # get mother props
    mother_props = divisions_properties[mother]

    # weighted sums per indiv
    delta_a = []
    delta_b = []

    direction_deviations = []
    # neighbor_name_list = []
    individual_name = []
    # contact_surfaces_with_neighbor = []
    # deviation_of_neighbors = []

    for direction, indiv_props in mother_props['division_directions_after_symmetry'].items():
        # Iterate through individual properties
        for individual, direction_vector in indiv_props.items():
            division_time_dif_a = []
            division_time_dif_b = []
            sum_contact_surfaces = 0
            contact_surfaces = []

            # Get division time of the individual
            individual_div_time = mother_props.get('division_aq_time', {}).get(direction).get(individual)
            # Get deviation from mean of the individual
            deviation_from_mean = compute_std_distance(direction_vector,
                                                       mother_props['division_mean_direction'],
                                                       mother_props['std_from_mean_directions'])
            cell_surface = mother_props['cell_surface'][direction][individual]

            # Iterate through neighbors
            for neighbor_name, surface_of_contact in mother_props['neighbors'][direction][individual].items():

                if "Unknown Neighbor" in neighbor_name:
                    continue  # neighbor not named

                contact_surfaces.append(surface_of_contact)
                sum_contact_surfaces += surface_of_contact

                # compute division_time_dif_b
                neighbor_mother_name = uname.get_mother_name(neighbor_name)
                if neighbor_mother_name[:-1] in divisions_properties:  # we have a record of its division
                    # Get division time of the neighbor cell
                    if neighbor_mother_name[-1] == '_':
                        d = 'left'
                    elif neighbor_mother_name[-1] == '*':
                        d = 'right'
                    neighbor_div_time = divisions_properties[neighbor_mother_name[:-1]].get('division_aq_time').get(
                        d).get(
                        individual)
                    if neighbor_div_time is not None:
                        time_diff = np.exp(
                            -np.abs(neighbor_div_time - individual_div_time) ** 2 / window) * surface_of_contact
                        division_time_dif_b.append(time_diff)
                    else:
                        # division_time_dif_a append default value : neighbor not gonna divide in this individual
                        division_time_dif_b.append(0)
                else:
                    # division_time_dif_a append default value : neighbor not gonna divide in all individuals
                    division_time_dif_b.append(0)

                # compute division_time_dif_a
                if neighbor_name[:-1] in divisions_properties:  # we have a record of its division
                    # Get division time of the neighbor cell
                    if neighbor_name[-1] == '_':
                        d = 'left'
                    elif neighbor_name[-1] == '*':
                        d = 'right'
                    neighbor_div_time = divisions_properties[neighbor_name[:-1]].get('division_aq_time').get(d).get(
                        individual)
                    if neighbor_div_time is not None:
                        time_diff = np.exp(
                            -np.abs(neighbor_div_time - individual_div_time) ** 2 / window) * surface_of_contact
                        division_time_dif_a.append(time_diff)

                        # neighbor's division deviation :
                        # neighbor_direction = divisions_properties[neighbor_name[:-1]]['division_directions_after_symmetry'][direction][individual]
                        # neighbor_mean = divisions_properties[neighbor_name[:-1]]['division_mean_direction']
                        # neighbor_std = divisions_properties[neighbor_name[:-1]]['std_from_mean_directions']
                        # neighbor_deviation = compute_std_distance(neighbor_direction, neighbor_mean, neighbor_std)
                        # deviation_of_neighbors.append(neighbor_deviation)
                    else:
                        # division_time_dif_a append default value : neighbor not gonna divide in this individual
                        division_time_dif_a.append(0)
                else:
                    # division_time_dif_a append default value : neighbor not gonna divide in all individuals
                    division_time_dif_a.append(0)

            # compute weighted value of number of divisions
            division_time_dif_a = np.array(division_time_dif_a)

            delta_a.append(sum(division_time_dif_a) / sum_contact_surfaces)

            # compute weighted value of number of divisions
            division_time_dif_b = np.array(division_time_dif_b)
            delta_b.append(sum(division_time_dif_b) / sum_contact_surfaces)

            direction_deviations.append(deviation_from_mean)
            individual_name.append(individual + '_' + direction)

    # call function to write file that does the plot
    write_script_to_plot_direction_deviation_vs_div_times(mother, delta_a, delta_b, direction_deviations,
                                                          individual_name, window, output_folder)


def write_script_to_plot_direction_deviation_vs_div_times(mother, delta_a, delta_b, direction_deviations,
                                                          individual_name, output_folder, available_cases=None):
    """
    Write a Python script to plot deviation of division direction from mean vs. division times difference with neighbors.

    Parameters
    ----------
    mother (str): mother name
    direction_deviations : list
        List of deviations from mean direction.
    division_times_diff : list
        List of division times differences with neighboring divisions.
    neighbor_name_list : list
    individual_name : list
    output_folder : str
        Path to the folder where the plot script will be saved.

    Returns
    -------

    """
    # give two folders options: interphasic neighbors or cells devided before (y axis label should be adjusted)
    # adjust y labels : either # of neighbors in interphase weighted by surface of contact
    # either # of neighbors that divided in the same window as the considered cell
    if "interphasic" in available_cases:
        output_folder = output_folder + '/neighbors_life_cycle_vs_deviation_from_mean'
        y_label = "Fraction of Surface Contact with Neighbors in Mitosis"
        x_label = "Fraction of Surface Contact with Neighbors in Interphase"
        title = (f"Deviation of Division Direction from Mean for {mother} vs. Number of Neighbors in Interphase and "
                 f"Mitosis")

    # todo delete
    elif "divisions_before" in available_cases:
        output_folder = output_folder + '/neighbors_dividing_before_div_time'
        y_label = "Number of Neighbors that Divided Before (Weighted by Surface of Contact)"
        x_label = "Number of Neighbors in dividing at the same time (Weighted by Surface of Contact)"
        title = (f"Deviation of Division Direction from Mean for {mother} vs. Number of Neighbors Divided Before, "
                 f"time window={window}")
    else:
        y_label = "Nb of neighbors dividing before the cell, weighted by surface of contact"
        x_label = "Nb of neighbors dividing after the cell, weighted by surface of contact"
        title = (f"Deviation of Division Direction from Mean for {mother} vs. Number of Neighbors Divided Before & "
                 f"after,"
                 f"time window={window}")

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    output_file = os.path.join(output_folder, f'{mother}.py')
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt

# x axis, interphase surface of contact
delta_a = {delta_a} 
# y axis, mitosis surface of contact
delta_b = {delta_b} 
individual_name = {individual_name} # annotation
direction_deviations = {direction_deviations} # scatter size

y_label = "{str(y_label)}"
x_label = "{str(x_label)}"
title = "{str(title)}"

plt.figure(figsize=(10, 6))

for i, txt in enumerate(individual_name):
    plt.scatter(delta_a[i], delta_b[i], s=100*direction_deviations[i]+1, alpha=0.4)
    plt.annotate(txt, (delta_a[i], delta_b[i]))

plt.xlabel(x_label)
plt.ylabel(y_label)
plt.title(title)

sizes = [0, 1, 5]  # Example sizes for the legend
size_labels = ['0', '1', '5']  # Example size labels
scatter_sizes = [plt.scatter([], [], s=100*size+1, color='grey', alpha=0.2, label=label) for size, label in zip(sizes, size_labels)]
legend = plt.legend(handles=scatter_sizes, loc='upper right', title="  Division Deviation")

plt.xlim(-0.1,1)
plt.ylim(-0.1,1)

# Show plot
plt.grid(True)
plt.show()
""")


#####################################################################################################################
#####################################################################################################################
#####################################################################################################################
def dendogramme_surface_of_contact(divisions_properties, generations, output_folder=''):
    """
    for given cells compute their dendogramme of surface of contact
    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
            Example: {'mother_name': {'neighbors': {'left/right'{'ind': vector of surface of contact} },
                                      'nb_of_measurements': int,
                                      'gen': int }}
    generations (list): List of generations to include in the plot.
    output_folder (str): The path to the output folder where the plot files will be saved.

    Returns
    -------

    """
    output_folder = output_folder + '/dendrogram_cells_vector_surface_of_contact'

    # Iterate over each specified generation
    for gen in generations:
        # Filter divisions properties for the current generation
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == gen}

        # Collect division directions and cell fates
        for mother_name, properties in gen_divisions.items():
            vectors_of_surface_of_contact = []
            individual_name_list = []
            division_directions = properties.get('neighbors', {})
            for lr, direction in division_directions.items():
                for ind, dir in direction.items():
                    vectors_of_surface_of_contact.append(dir)
                    individual_name_list.append(ind +'_'+ lr)
            cond_dist_matrix, d_max = compute_distance_matrix_btw_surf_of_cont_vectors(vectors_of_surface_of_contact)
            write_dendrogram_plot(dmatrix=cond_dist_matrix, list_of_ind_names=individual_name_list,
                                  mother_name=mother_name, output_folder=output_folder, title='surface of contact')

def compute_distance_matrix_btw_surf_of_cont_vectors(vectors_of_surface_of_contact):
    vector_dicts = []

    # Convert list of vectors to dictionaries with neighbor names as keys
    for vector in vectors_of_surface_of_contact:
        vector_dicts.append(vector)

    # Compute the distance matrix
    num_vectors = len(vector_dicts)
    dist_matrix = np.zeros((num_vectors, num_vectors))

    for i in range(num_vectors):
        for j in range(i + 1, num_vectors):
            keys_union = set(vector_dicts[i].keys()).union(vector_dicts[j].keys())
            distance = 0
            for key in keys_union:
                val_i = vector_dicts[i].get(key, 0)
                val_j = vector_dicts[j].get(key, 0)
                distance += abs(val_i - val_j)
            dist_matrix[i, j] = distance
            dist_matrix[j, i] = distance

    conddist = sp.spatial.distance.squareform(dist_matrix)
    return conddist, np.max(dist_matrix)


def plot_dendrogram_directions(divisions_properties, generations, output_folder):
    """
    Writes a file that plots the dendrogram of cell division directions dispersion.
    A figure is generated for each cell
    Parameters:
    divisions_properties (dict): A dictionary containing division properties.
            Example: {'mother_name': {'division_directions': {'left/right'{'ind': 3d direction} },
                                      'nb_of_measurements': int,
                                      'gen': int }}
    generations (list): List of generations to include in the plot.
    output_folder (str): The path to the output folder where the plot files will be saved.
    show_fate (bool): Whether to show cell fates in the plot. Default is False.

    Returns:
    None
    """
    output_folder = output_folder + '/dendrogram_div_directions'
    # Iterate over each specified generation
    for gen in generations:
        # Filter divisions properties for the current generation
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == gen}

        # Collect division directions and cell fates
        for mother_name, properties in gen_divisions.items():
            div_directions_ = []
            individual_name_list = []
            division_directions = properties.get('division_directions_after_symmetry', {})
            for lr, direction in division_directions.items():
                for ind, dir in direction.items():  # Iterate over directions and append angles
                    div_directions_.append(dir)
                    individual_name_list.append(ind +'_'+ lr)
            cond_dist_matrix, d_max = compute_distance_matrix(div_directions_)
            write_dendrogram_plot(dmatrix=cond_dist_matrix, list_of_ind_names=individual_name_list,
                                  mother_name=mother_name, output_folder=output_folder)


def write_dendrogram_plot(dmatrix, list_of_ind_names, mother_name, output_folder, title='division direction'):
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    output_file = os.path.join(output_folder, f'{mother_name}.py')

    with open(output_file, 'w') as f:
        f.write("import matplotlib.pyplot as plt\n")
        f.write("from scipy.cluster.hierarchy import dendrogram, linkage\n")
        f.write("import numpy as np\n\n")

        f.write(f"list_of_ind_names = {list_of_ind_names}\n")
        f.write(f"dmatrix = np.array({dmatrix.tolist()})\n")

        f.write("linkage_matrix = linkage(dmatrix, 'single', optimal_ordering = True)\n\n")

        f.write("plt.figure(figsize=(10, 6))\n")
        f.write("dendrogram(linkage_matrix, labels=list_of_ind_names, leaf_rotation=90)\n")
        f.write("plt.xlabel('Individuals')\n")
        if title == 'division direction':
            f.write("plt.ylabel('Distance (in radian)')\n")
            f.write("plt.ylim(0,5)\n")
        else:
            f.write("plt.ylabel('Distance between surface of contact')\n")
            f.write("#plt.ylim(0,5)\n")
        f.write(f"plt.title('Dendrogram for Cell {title}, Mother Cell Name: {mother_name}, single linkage')\n")
        f.write("plt.tight_layout()\n")
        f.write("plt.show()\n")
        f.close()


def compute_distance_matrix(vectors_list):
    labels = []

    dist = np.zeros((len(vectors_list), len(vectors_list)))
    for i, r in enumerate(vectors_list):
        r = np.array(r)

        labels += [r]
        for j, s in enumerate(vectors_list):
            s = np.array(s)
            if i == j:
                dist[i][i] = 0.0
                continue
            if i > j:
                continue

            dist[i][j] = 0.0
            dist[i][j] = np.arccos(np.dot(r, s))  # radinas difference between two directions
            dist[j][i] = dist[i][j]

    conddist = sp.spatial.distance.squareform(dist)
    # z = sch.linkage(conddist, method=cluster_distance)

    return conddist, np.max(dist)


def plot_division_direction_deviation_vs_surface_of_contacts(divisions_properties, mother, cell_name_to_fate,
                                                             output_folder):
    """
    we will check the cells deviation from mean direction vs neighbors percentage of contact relative to cell surface

    Parameters
    ----------
    divisions_properties  (dict): A dictionary containing division properties.
                               Example: {
                                'mother_name': {
                                'division_directions_after_symmetry':
                                    {'left': {'individual name': direction vector},
                                    'right': {'individual name': direction vector} }
                                                },
                                'gen': int,
                                'std_from_mean_directions': std_a,
                                'division_mean_direction': mean_direction,
                                'neighbors':{'direction':{'individual': {'neighbor_name':{'surface_of_contact':value }}} ,
                                    } }
    cell_name_to_fate (dict): {'cell_name': cell_fate}
    mother (list): list of divisions mother names to be considered
    output_folder (str): where the python script that does the plot will be placed

    Returns
    -------

    """
    deviations = {}
    mother_props = divisions_properties[mother]
    for direction, direction_data in mother_props['division_directions_after_symmetry'].items():
        for individual, individual_data in direction_data.items():
            deviation = compute_std_distance(individual_data, mother_props['division_mean_direction'],
                                             mother_props['std_from_mean_directions'])
            mother_key = mother
            if mother_key not in deviations:
                deviations[mother_key] = {}
            if individual not in deviations[mother_key]:
                deviations[mother_key][individual] = {'left': {}, 'right': {}}
            deviations[mother_key][individual][direction]['cell_deviation'] = deviation
            deviations[mother_key][individual][direction]['cell_surface'] = \
                mother_props['cell_surface'][direction][individual]

            for neighbor_name, neighbor_data in mother_props['neighbors'][direction][individual].items():
                if "Unknown Neighbor" in neighbor_name:
                    continue
                if 'neighbors' not in deviations[mother_key][individual][direction]:
                    deviations[mother_key][individual][direction]['neighbors'] = {}
                if neighbor_name not in deviations[mother_key][individual][direction]['neighbors']:
                    # Add fate and contact surface
                    deviations[mother_key][individual][direction]['neighbors'][neighbor_name] = {}
                    deviations[mother_key][individual][direction]['neighbors'][neighbor_name][
                        'cell_fate'] = cell_name_to_fate.get(neighbor_name, 'unknown')
                    deviations[mother_key][individual][direction]['neighbors'][neighbor_name][
                        'contact_surface'] = neighbor_data

    write_script_to_plot_deviation_of_div_vs_neighbors_contact(deviations, output_folder)
    write_script_deviation_of_div_vs_fates_contact(deviations, output_folder)


def write_script_to_plot_deviation_of_div_vs_neighbors_contact(deviations, output_folder):
    """
    Plots the following, In the y axis the deviation of the cell in each individual
    in the x axis for each neighbor in the individual the percentage of the neighbors surface of contact relative to cell surface
    text on scatter is neighbor name
    Parameters
    ----------
    deviations(dict): {'cell_name': {'individual':{'left': {'cell_deviation':deviation, 'neighbors_contact':
                                        {'neighbor name': {'contact_surface':surface of contact, 'cell_fate':cell fate}}
                                                    , 'cell_surface':surface of cell }
                                                    'right': same as in left   }}   }
    output_folder

    Returns
    -------

    """
    output_folder = output_folder + '/deviation_vs_neighbors_contact'
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    x_values = []
    y_values = []
    individuals = []
    labels = []
    fates = []
    for mother, mother_data in deviations.items():
        for individual, individual_data in mother_data.items():
            for direction in ['left', 'right']:

                deviation = individual_data[direction]['cell_deviation']
                cell_surface = individual_data[direction]['cell_surface']
                for neighbor_name, neighbor_data in individual_data[direction]['neighbors'].items():
                    surface_of_contact = neighbor_data['contact_surface']
                    percentage_contact = 100 * (surface_of_contact / cell_surface)
                    neighbors_fate = neighbor_data['cell_fate']
                    if isinstance(neighbors_fate, list):
                        # If the cell has multiple fates, we consider each one separately
                        for fate in neighbors_fate:
                            fates.append(fate)
                            x_values.append(percentage_contact)
                            y_values.append(deviation)
                            individuals.append(individual + '_' + direction)
                            labels.append(neighbor_name)
                    else:
                        fates.append(neighbors_fate)
                        x_values.append(percentage_contact)
                        y_values.append(deviation)
                        individuals.append(individual + '_' + direction)
                        labels.append(neighbor_name)

    output_file = os.path.join(output_folder, f'{mother}.py')
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

x_values = {x_values}
y_values = {y_values}
neighbor_names = {labels}
individuals = {individuals}
fates = {fates}
fate_colors = {{}}

fig, ax = plt.subplots(figsize=(10, 6))
for y_val in set(y_values):
    idx =  y_values.index(y_val)
    ax.axhline(y_val, color='black', linestyle='-.', linewidth=0.5)
    ax.text(1, y_val, individuals[idx], va='center')

legend_handles = []

for fate in set(fates):
    if isinstance(fate, list):
        # For cells with multiple fates, generate a unique color identifier
        fate_key = hashlib.md5(''.join(sorted(fate)).encode()).hexdigest()
        if fate_key not in fate_colors:
            fate_colors[fate_key] = len(fate_colors)
        color_index = fate_colors[fate_key]
    else:
        # For single fate, use the fate as color identifier
        if fate not in fate_colors:
            fate_colors[fate] = len(fate_colors)
        color_index = fate_colors[fate]

    color = plt.cm.tab10(color_index)
    legend_handles.append(Line2D([0], [0], marker='o', color=color, markersize=5, label=fate))

         
                                
for x, y, neighbor_name, individual, fate in zip(x_values, y_values, neighbor_names, individuals, fates):
    if isinstance(fate, list):
        # For cells with multiple fates, generate a unique color identifier
        fate_key = hashlib.md5(''.join(sorted(fate)).encode()).hexdigest()
        color_index = fate_colors[fate_key]
    else:
        # For single fate, use the fate as color identifier
        color_index = fate_colors[fate]
    
    color = plt.cm.tab10(color_index)
    ax.scatter(x, y, c=color, s=100)
    ax.text(x,y,neighbor_name, rotation=45)

# Add labels and title
ax.set_xlabel("Neighbor Surface of Contact relative to Cell Surface (Percentage)")
ax.set_ylabel("Cell Division Deviation from Mean (Standardized)")
ax.set_title("Deviation of {mother} Division Direction from Mean vs Neighbor Surface of Contact relative to Cell Surface")

# Add legend
ax.legend(handles=legend_handles)

# Show plot
ax.set_ylim(0,5)
ax.set_xlim(0,50)
ax.grid(True)
plt.show()        
""")


def write_script_deviation_of_div_vs_fates_contact(deviations, output_folder):
    """
    Plots the deviation of a cell vs percentage of contact with cells from every fate

    Parameters
    ----------
    deviations (dict): Dictionary containing deviation data.
    output_folder (str): Output folder path.

    Returns
    -------
    None
    """
    output_folder = os.path.join(output_folder, 'deviation_vs_neighbors_fates_contact')
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    y_values = []
    individuals = []
    size_values = []
    fates = []
    cell_neighbor_percentage = []

    for mother, mother_data in deviations.items():
        for individual, individual_data in mother_data.items():
            for direction in ['left', 'right']:
                deviation = individual_data[direction]['cell_deviation']
                cell_surface = individual_data[direction]['cell_surface']
                for neighbor_name, neighbor_data in individual_data[direction]['neighbors'].items():
                    surface_of_contact = neighbor_data['contact_surface']
                    percentage_contact = 100 * (surface_of_contact / cell_surface)
                    neighbors_fate = neighbor_data['cell_fate']
                    if isinstance(neighbors_fate, list):
                        # If the cell has multiple fates, we consider each one separately
                        for fate in neighbors_fate:
                            fates.append(fate)
                            cell_neighbor_percentage.append(percentage_contact)
                            y_values.append(deviation)
                            individuals.append(individual + '_' + direction)
                    else:
                        fates.append(neighbors_fate)
                        cell_neighbor_percentage.append(percentage_contact)
                        y_values.append(deviation)
                        individuals.append(individual + '_' + direction)
    fates_set = set(fates)
    individuals_set = set(individuals)
    deviations_values = []
    individuals_ = []
    fates_ = []
    for fate in fates_set:
        for ind in individuals_set:
            # in size values list put sum of cells that have this fate
            percentage = 0
            for i, neighbor_value in enumerate(cell_neighbor_percentage):
                if fates[i] == fate and individuals[i] == ind:
                    percentage += neighbor_value
            size_values.append(percentage)
            idx = individuals.index(ind)
            deviations_values.append(y_values[idx])
            individuals_.append(ind)
            fates_.append(fate)
    output_file = os.path.join(output_folder, f'{mother}.py')
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt

fates = {fates_}
set_fates = list(set(fates))
individuals = {individuals_}
deviations_values = {deviations_values}
sizes = {size_values}

fig, ax = plt.subplots(figsize=(10, 6))  
 
for y_val, individual in zip(deviations_values, individuals):
    ax.axhline(y_val, color='black', linestyle='-.', linewidth=0.5)
    ax.text(-1, y_val, individual, va='center')

for f, y, size in zip(fates, deviations_values, sizes):
    x = set_fates.index(f)
    ax.scatter(x, y, s=size*50, alpha=0.5, color='grey')
    
ax.set_xticks(range(len(set_fates)))
ax.set_xticklabels(set_fates, rotation=45)
ax.set_xlabel("Neighboring Cell Fates")
ax.set_ylabel("Cell Division Deviation from Mean (Standardized)")
ax.set_title("Deviation of {mother} Cell Division Direction from Mean vs Neighbors Surface of Contact")

sizes = [5, 20]  # Example sizes for the legend
size_labels = ['5%', '20%']  # Example size labels
scatter_sizes = [ax.scatter([], [], s=50*size, color='grey', alpha=0.2, label=label) for size, label in zip(sizes, size_labels)]
legend = ax.legend(handles=scatter_sizes, loc='upper right', title="% of Cell Surface in Contact with Neighbors of the considered Fate out of the Cell’s Total Surface")

ax.add_artist(legend)

ax.set_ylim(0,10)
ax.set_xlim(-1,15)
ax.grid(True)

plt.tight_layout()
plt.show()
""")


def plot_scatter_div_and_neighbors_deviation_from_mean(divisions_properties, mother, threshold, output_folder):
    """
    will compute for a given division its deviation from mean with its neighbors deviation from mean
    Goal is to check :
        Does high deviation from mean in a neighboring division causes high deviation from mean in the considered division
    Parameters
    ----------
    divisions_properties  (dict): A dictionary containing division properties.
                               Example: {
                                'mother_name': {
                                'division_directions_after_symmetry':
                                    {'left': {'individual name': direction vector},
                                    'right': {'individual name': direction vector} }
                                                },
                                'gen': int,
                                'std_from_mean_directions': std_a,
                                'division_mean_direction': mean_direction,
                                'neighbors':{'left':{'individual': {'neighbor_name':{'surface_of_contact':value }  } } ,
                 'right': {'individual': {'neighbor_name':{'surface_of_contact':value, 'gen':value }  } }
                 }
    mother (list): list of divisions mother names to be considered
    threshold(int): When considering the surface of contact of neighbors, we use a threshold based on the percentage
                        of cell surface. Only neighbors with a contact surface exceeding this percentage are considered.
    output_folder (str): where the python script that does the plot will be placed

    Returns
    -------

    """
    deviations = {}  # {' mother+'*'/'_' ': 'individual':{'neighbor_cell_name':neighbor_deviation } }
    # mother = mother_name,
    mother_props = divisions_properties[mother]

    plot_boxplot_div_and_neighbors_deviation_from_mean(divisions_properties, mother, threshold, output_folder)

    for direction, direction_data in mother_props['division_directions_after_symmetry'].items():
        # direction is either left or right
        for individual, individual_data in direction_data.items():
            # individual_data : direction in this individual
            deviation = compute_std_distance(individual_data, mother_props['division_mean_direction'],
                                             mother_props['std_from_mean_directions'])
            mother_key = mother
            if mother_key not in deviations:
                deviations[mother_key] = {}
            if individual not in deviations[mother_key]:
                deviations[mother_key][individual] = {}
            deviations[mother_key][individual]['mother_deviation'] = deviation
            deviations[mother_key][individual]['mother_surface_in_individual'] = \
                mother_props['cell_surface'][direction][individual]
            for neighbor_name, neighbor_contact_surface in mother_props['neighbors'][direction][individual].items():
                # some cells arent named
                if "Unknown Neighbor" in neighbor_name:
                    continue
                #  eliminate when neighbor is one of the cells daughters
                if uname.get_mother_name(neighbor_name)[:-1] == mother:
                    if uname.get_mother_name(neighbor_name)[-1] == '*' and direction == 'right':
                        continue
                    if uname.get_mother_name(neighbor_name)[-1] == '_' and direction == 'left':
                        continue
                # neighbor_data is the neighbor surface_of_contact with the mother cell considered
                # todo update using division time of cell and its mother
                if int(neighbor_name.split('.')[0][1:]) > int(mother.split('.')[0][1:]):
                    # we need the division properties of the mother
                    neighbor_mother_name = uname.get_mother_name(neighbor_name)  # mother neighbor name
                    neighbor_props = divisions_properties[neighbor_mother_name[:-1]]

                    if neighbor_name[-1] == '*':
                        if individual in neighbor_props['division_directions_after_symmetry']['right']:
                            neighbor_direction = neighbor_props['division_directions_after_symmetry']['right'][
                                individual]
                    else:
                        if individual in neighbor_props['division_directions_after_symmetry']['left']:
                            neighbor_direction = neighbor_props['division_directions_after_symmetry']['left'][
                                individual]

                    neighbor_mean = neighbor_props['division_mean_direction']
                    neighbor_std = neighbor_props['std_from_mean_directions']
                    neighbor_deviation = compute_std_distance(neighbor_direction, neighbor_mean, neighbor_std)

                    # Store needed data for the plot
                    if 'cell_neighbors' not in deviations[mother_key][individual]:
                        deviations[mother_key][individual]['cell_neighbors'] = {}
                    if neighbor_mother_name not in deviations[mother_key][individual]['cell_neighbors']:
                        deviations[mother_key][individual]['cell_neighbors'][neighbor_mother_name] = {
                            'division_deviation': neighbor_deviation, 'daughters_contact_surface': {}}
                    deviations[mother_key][individual]['cell_neighbors'][neighbor_mother_name][
                        'daughters_contact_surface'][neighbor_name] = neighbor_contact_surface

        # Call function to write a script that does the plot
        if threshold is None:
            threshold = 10
        write_script_to_plot_deviation_of_div_and_neighbors(deviations, output_folder, threshold)
        deviations = {}


def write_script_to_plot_deviation_of_div_and_neighbors(deviations_dict, output_folder, threshold=10):
    """
    Plots for a given division its division direction deviation from mean in each individual on y axis
    on the x axis the neighbors division deviation from mean
    give colors of scatters according to the individual name
    put on each scatter the neighbor name
    Parameters
    ----------
    deviations_dict (dict): {' mother': 'individual':
                                               {'cell_neighbors': {'neighbor_cell_name':
                                                                    {'division_deviation': ,
                                                                    'daughters_contact_surface':
                                                                        {'neighbor_name':  neighbor_contact_surface
                                        # Usually you have two neighbor daughters d1 name will end with a pair number
                                        # One of the daughters may not have a contact thus may not be present}
                                                                                    },
                                                'mother_deviation':deviation,
                                                      'mother_surface_in_individual'              }}

                            }
    threshold(int): by default =10%
    When considering the surface of contact of neighbors, we use a threshold based on the percentage
                        of cell surface. Only neighbors with a contact surface exceeding this percentage are considered
    output_folder(str): where to place the script that when run will do the plot !

    Returns
    -------

    """
    # assign shapes : o if only d1 is in contact, square if only d2 is in contact, other : percentage ration : d1/d2
    output_folder = output_folder + '/neighbors_deviation_impact_on_direction_deviation'
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    x_values = []
    y_values = []
    individuals = []
    labels = []
    percentage_of_contact_surfaces = []
    color_map = {}
    color_index = 0

    for mother, mother_data in deviations_dict.items():
        for individual, individual_data in mother_data.items():
            if individual not in color_map:
                color_map[individual] = color_index
                color_index += 1

            if 'cell_neighbors' in individual_data:
                neighbors_deviation = individual_data['cell_neighbors']
            else:
                continue
            mother_deviation = individual_data['mother_deviation']
            for neighbor_dividing_mother_name, neighbor_data in neighbors_deviation.items():
                neighbor_deviation = neighbor_data['division_deviation']

                daughters_contact_surface = neighbor_data.get('daughters_contact_surface', {})
                # Check if the total surface is greater than or equal to the minimum threshold
                total_surface = sum(daughters_contact_surface.values())
                # adjust to a chosen threshold
                if total_surface < individual_data['mother_surface_in_individual'] * threshold / 100:
                    continue

                x_values.append(neighbor_deviation)
                y_values.append(mother_deviation)
                individuals.append(individual)
                labels.append(neighbor_dividing_mother_name)

                # daughters_contact_surface have two/one keys which are the daughters names
                if len(daughters_contact_surface) == 1:
                    d_name = list(daughters_contact_surface.keys())
                    d_name = d_name[0]
                    if int(d_name.split('.')[1][:-1]) % 2 == 0:
                        percentage_of_contact_surfaces.append(1)
                    else:
                        percentage_of_contact_surfaces.append(0)
                elif len(daughters_contact_surface) == 2:
                    # d1 has impair name
                    # compute percentage : s(d1) / s(d1)+s(d2) : values can be 0 or infinity
                    d_names = list(daughters_contact_surface.keys())
                    d1_name = d_names[0]
                    d2_name = d_names[1]
                    # check if there's a need to inverse names
                    if int(d1_name.split('.')[1][:-1]) % 2 == 0:
                        d1_name, d2_name = d2_name, d1_name
                    total_surface = daughters_contact_surface[d1_name] + daughters_contact_surface[d2_name]
                    percentage_of_contact_surfaces.append(daughters_contact_surface[d1_name] / total_surface)

    output_file = os.path.join(output_folder,
                               f'{list(deviations_dict.keys())[0]}.py')
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

x_values = {x_values}
y_values = {y_values}
neighbor_names = {labels}
individuals = {individuals}
color_map = {{individual: i for i, individual in enumerate(sorted(set(individuals)))}}
percentages_of_daughters_contacts = {percentage_of_contact_surfaces}

fig, ax = plt.subplots(figsize=(10, 6))

for y_val in set(y_values):
    ax.axhline(y_val, color='black', linestyle='-.', linewidth=0.5)

legend_handles = []
for individual in set(individuals):
    legend_handles.append(Line2D([0], [0], marker='o', color=plt.cm.tab10(color_map[individual]), markersize=5, markerfacecolor=plt.cm.tab10(color_map[individual]), label=individual))
for x, y, neighbor_name, individual, percentage in zip(x_values, y_values, neighbor_names, individuals, percentages_of_daughters_contacts):
    if percentage is None:
        # Handle None values (0 total surface)
        marker = '^'  # Triangular marker
        size = 50
    elif percentage == 0:
        marker = '^'  # Triangular marker
        size = 50
    elif percentage == 1:
        marker = 's'  # Rectangular marker
        size = 50
    else:
        marker = 'o'  # Circular marker
        size = 200 * percentage 
    ax.scatter(x, y, c=[plt.cm.tab10(color_map[individual])], label=individual, marker=marker, s=size)
    ax.text(x,y,neighbor_name, rotation=45)

# Add labels and title
ax.set_xlabel("Neighbor Division Deviation from Mean")
ax.set_ylabel("Cell Division Deviation from Mean (Standardized)")
ax.set_title("Deviation of {mother} Division Direction from Mean vs Deviation of Neighboring Division Direction from Mean. Only neighbors with a surface contact of at least {threshold}% of cell surface are considered")

# Add legend
legend1 = ax.legend(handles=legend_handles, title='Embryo', loc='upper left')

# Second legend for scatter point sizes
sizes = [0.3, 0.5, 0.8]  # Example sizes for the legend
size_labels = ['30%', '50%', '80%']  # Example size labels
scatter_sizes = [ax.scatter([], [], s=200*size, color='grey', alpha=0.2, label=label) for size, label in zip(sizes, size_labels)]
legend2 = ax.legend(handles=scatter_sizes, loc='upper right', title="Percentage of Daughter d1's Surface of Contact")

legend3 = ax.legend(handles=[Line2D([], [], marker='o', color='black', markersize=10, label='Both Daughters d1 and d2'),
                             Line2D([], [], marker='^', color='black', markersize=10, label='Only Daughter d1'),
                             Line2D([], [], marker='s', color='black', markersize=10, label='Only Daughter d2')],
                    loc='lower right', title='Daughters Configuration')

ax.add_artist(legend1)
ax.add_artist(legend2)
ax.add_artist(legend3)

# Show plot
ax.set_ylim(0,5)
ax.set_xlim(0,5)
ax.grid(True)
plt.show()
""")


def plot_boxplot_div_and_neighbors_deviation_from_mean(divisions_properties, mother, threshold, output_folder):
    """
    will compute for a given division its deviation from mean with its neighbors deviation from mean
    Goal is to check :
        Does high deviation from mean in a neighboring division causes high deviation from mean in the considered division
    Parameters
    ----------
    divisions_properties  (dict): A dictionary containing division properties.
                               Example: {
                                'mother_name': {
                                'division_directions_after_symmetry':
                                    {'left': {'individual name': direction vector},
                                    'right': {'individual name': direction vector} }
                                                },
                                'gen': int,
                                'std_from_mean_directions': std_a,
                                'division_mean_direction': mean_direction,
                                'neighbors':{'left':{'individual': {'neighbor_name':{'surface_of_contact':value }  } } ,
                 'right': {'individual': {'neighbor_name':{'surface_of_contact':value, 'gen':value }  } }
                 }
    mother (list): list of divisions mother names to be considered
    threshold(int): When considering the surface of contact of neighbors, we use a threshold based on the percentage
                        of cell surface. Only neighbors with a contact surface exceeding this percentage are considered.
    output_folder (str): where the python script that does the plot will be placed

    Returns
    -------

    """
    deviations = {}  # {' mother+'*'/'_' ': 'individual':{'neighbor_cell_name':neighbor_deviation } }
    # mother = mother_name,
    mother_props = divisions_properties[mother]
    for direction, direction_data in mother_props['division_directions_after_symmetry'].items():
        # direction is either left or right
        for individual, individual_data in direction_data.items():
            # individual_data : direction in this individual
            deviation = compute_std_distance(individual_data, mother_props['division_mean_direction'],
                                             mother_props['std_from_mean_directions'])
            mother_key = mother
            individual_key = individual + '_' + direction
            if mother_key not in deviations:
                deviations[mother_key] = {}
            if individual_key not in deviations[mother_key]:
                deviations[mother_key][individual_key] = {}
            deviations[mother_key][individual_key]['mother_deviation'] = deviation
            deviations[mother_key][individual_key]['mother_surface_in_individual'] = \
                mother_props['cell_surface'][direction][individual]
            for neighbor_name, neighbor_contact_surface in mother_props['neighbors'][direction][individual].items():
                # some cells arent named
                if "Unknown Neighbor" in neighbor_name:
                    continue
                #  eliminate when neighbor is one of the cells daughters
                if uname.get_mother_name(neighbor_name)[:-1] == mother:
                    if uname.get_mother_name(neighbor_name)[-1] == '*' and direction == 'right':
                        continue
                    if uname.get_mother_name(neighbor_name)[-1] == '_' and direction == 'left':
                        continue
                # neighbor_data is the neighbor surface_of_contact with the mother cell considered
                # todo update using division time of cell and its mother
                if int(neighbor_name.split('.')[0][1:]) > int(mother.split('.')[0][1:]):
                    # we need the division properties of the mother
                    neighbor_mother_name = uname.get_mother_name(neighbor_name)  # mother neighbor name
                    neighbor_props = divisions_properties[neighbor_mother_name[:-1]]

                    if neighbor_name[-1] == '*':
                        if individual in neighbor_props['division_directions_after_symmetry']['right']:
                            neighbor_direction = neighbor_props['division_directions_after_symmetry']['right'][
                                individual]
                    else:
                        if individual in neighbor_props['division_directions_after_symmetry']['left']:
                            neighbor_direction = neighbor_props['division_directions_after_symmetry']['left'][
                                individual]

                    neighbor_mean = neighbor_props['division_mean_direction']
                    neighbor_std = neighbor_props['std_from_mean_directions']
                    neighbor_deviation = compute_std_distance(neighbor_direction, neighbor_mean, neighbor_std)

                    # Store needed data for the plot
                    if 'cell_neighbors' not in deviations[mother_key][individual_key]:
                        deviations[mother_key][individual_key]['cell_neighbors'] = {}
                    if neighbor_mother_name not in deviations[mother_key][individual_key]['cell_neighbors']:
                        deviations[mother_key][individual_key]['cell_neighbors'][neighbor_mother_name] = {
                            'division_deviation': neighbor_deviation, 'daughters_contact_surface': {}}
                    deviations[mother_key][individual_key]['cell_neighbors'][neighbor_mother_name][
                        'daughters_contact_surface'][neighbor_name] = neighbor_contact_surface

    # Call function to write a script that does the plot
    if threshold is None:
        threshold = 10

    write_script_to_plot_deviation_of_div_and_neighbors_deviation_boxplot(deviations, output_folder,
                                                                          threshold=10)
    deviations = {}


def write_script_to_plot_deviation_of_div_and_neighbors_deviation_boxplot(deviations_dict, output_folder, threshold=10):
    """
    Plots for a given division its division direction deviation from mean in each individual on y axis
    on the x axis the neighbors division deviation from mean boxplot
    Parameters
    ----------
    deviations_dict (dict): {' mother': 'individual':
                                               {'cell_neighbors': {'neighbor_cell_name':
                                                                    {'division_deviation': ,
                                                                    'daughters_contact_surface':
                                                                        {'neighbor_name':  neighbor_contact_surface
                                        # Usually you have two neighbor daughters d1 name will end with a pair number
                                        # One of the daughters may not have a contact thus may not be present}
                                                                                    },
                                                'mother_deviation':deviation,
                                                      'mother_surface_in_individual'              }}

                            }
    threshold(int): by default =10%
    When considering the surface of contact of neighbors, we use a threshold based on the percentage
                        of cell surface. Only neighbors with a contact surface exceeding this percentage are considered
    output_folder(str): where to place the script that when run will do the plot !

    Returns
    -------

    """
    # assign shapes : o if only d1 is in contact, square if only d2 is in contact, other : percentage ration : d1/d2
    output_folder = output_folder + '/neighbors_deviation_impact_on_direction_deviation_boxplot'
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    x_values = []
    y_values = []
    individuals = []
    labels = []
    percentage_of_contact_surfaces = []
    color_map = {}
    color_index = 0

    for mother, mother_data in deviations_dict.items():
        for individual, individual_data in mother_data.items():
            if individual not in color_map:
                color_map[individual] = color_index
                color_index += 1

            if 'cell_neighbors' in individual_data:
                neighbors_deviation = individual_data['cell_neighbors']
            else:
                continue
            mother_deviation = individual_data['mother_deviation']
            for neighbor_dividing_mother_name, neighbor_data in neighbors_deviation.items():
                neighbor_deviation = neighbor_data['division_deviation']

                daughters_contact_surface = neighbor_data.get('daughters_contact_surface', {})
                # Check if the total surface is greater than or equal to the minimum threshold
                total_surface = sum(daughters_contact_surface.values())
                # adjust to a chosen threshold
                if total_surface < individual_data['mother_surface_in_individual'] * threshold / 100:
                    continue

                x_values.append(round(neighbor_deviation, 2))
                y_values.append(round(mother_deviation, 2))
                individuals.append(individual)
                labels.append(neighbor_dividing_mother_name)

                # daughters_contact_surface have two/one keys which are the daughters names
                if len(daughters_contact_surface) == 1:
                    d_name = list(daughters_contact_surface.keys())
                    d_name = d_name[0]
                    if int(d_name.split('.')[1][:-1]) % 2 == 0:
                        percentage_of_contact_surfaces.append(1)
                    else:
                        percentage_of_contact_surfaces.append(0)
                elif len(daughters_contact_surface) == 2:
                    # d1 has impair name
                    # compute percentage : s(d1) / s(d1)+s(d2) : values can be 0 or infinity
                    d_names = list(daughters_contact_surface.keys())
                    d1_name = d_names[0]
                    d2_name = d_names[1]
                    # check if there's a need to inverse names
                    if int(d1_name.split('.')[1][:-1]) % 2 == 0:
                        d1_name, d2_name = d2_name, d1_name
                    total_surface = daughters_contact_surface[d1_name] + daughters_contact_surface[d2_name]
                    percentage_of_contact_surfaces.append(daughters_contact_surface[d1_name] / total_surface)

    output_file = os.path.join(output_folder,
                               f'{list(deviations_dict.keys())[0]}.py')
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

x_values = {x_values}
y_values = {y_values}
neighbor_names = {labels}
individuals = {individuals}
percentages_of_daughters_contacts = {percentage_of_contact_surfaces}

fig, ax = plt.subplots(figsize=(10, 6))

for y_val in set(y_values):
    ax.axhline(y_val, color='black', linestyle='-.', linewidth=0.5)
    individual = individuals[y_values.index(y_val)]
    ax.text(-1, y_val, individual, va='center')

# for each y value a boxplot (color corresponding to individual)
for ind_name in set(individuals):
    # get y value individual name and corresponding color
    y_val = y_values[individuals.index(ind_name)]
    # get the x values (same indexes as in y values)
    x_vals_for_y = [round(x_values[i],2) for i, ind in enumerate(individuals) if ind == ind_name ]
    # draw boxplot with corresponding color
    ax.boxplot(x_vals_for_y, positions=[y_val] , vert=False, widths=0.15, patch_artist=True, showfliers=True)

# Add labels and title
ax.set_xlabel("Neighbor Division Deviation Boxplot from Mean")
ax.set_ylabel("Cell Division Deviation from Mean (Standardized)")
ax.set_title("Deviation of {mother} Division Direction from Mean vs Deviation of Neighboring Division Direction from Mean.")

ax.set_yticks(range(11))
ax.set_yticklabels(range(11)) 

# Show plot
ax.set_ylim(0,10)
ax.set_xlim(-2,10)
ax.grid(True)
plt.show()
""")


def plot_boxplot_of_cells_rltv_dist_per_individual(divisions_properties, generations, mother, output_folder):
    """

    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
                               Example: {
                               'mother_name': {
                                'division_directions_after_symmetry':
                                    {'left': {'individual name': direction vector},
                                    'right': {'individual name': direction vector} }
                                                },
                                'gen': int,
                                'std_from_mean_directions': std_a,
                                'division_mean_direction'
                                        }
    generations (list): cell generation to be considered
    mother (list): cell names to be ignored
    output_folder (str): where the python script that does the plot will be placed

    Returns
    -------

    """
    # filter cells by generation
    filtered_divisions = {mother_name: props for mother_name, props in divisions_properties.items() if
                          props.get('gen') in generations}
    # remove cell in mother
    filtered_divisions = {mother_name: props for mother_name, props in filtered_divisions.items() if
                          mother_name not in mother}
    # list_embryos_values
    embryo_values = []

    # Grab all embryo individual names
    embryo_individuals = set()
    # update it from division properties actually : less computations
    for props in filtered_divisions.values():
        for side_props in props['cell_ids'].values():
            for individual_name in side_props.keys():
                embryo_individuals.add(individual_name)
                # todo give option to choose indivduals or embryos

    # For each individual
    for individual_name in embryo_individuals:
        individual_values = []
        for props in filtered_divisions.values():
            # get std a and mean
            std_a = props['std_from_mean_directions']
            mean_direction = props['division_mean_direction']
            for side_props in props['division_directions_after_symmetry'].values():
                for ind, direction in side_props.items():
                    if ind == individual_name:
                        # compute direction distance
                        deviation = compute_std_distance(direction, mean_direction, std_a)
                        individual_values.append(deviation)
        embryo_values.append(individual_values)

    # call a function that generates script that plot the boxplot of values in each individual
    write_boxplot_script_individual(embryo_values, embryo_individuals, output_folder)


def write_boxplot_script_individual(embryo_values, embryo_individuals, output_folder):
    """
       Writes a Python script to create boxplots of values for each individual.

       Parameters:
           embryo_values (list): List of lists containing values for each individual.
           embryo_individuals (set): Set of individual names.
           output_folder (str): The path to the output folder where the plot script will be saved.

       Returns:
           None
       """
    output_folder = output_folder + '/boxplot_cells_relative_deviation_in_dif_embryos/'
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

        # Create the output file path
    output_file = os.path.join(output_folder, 'boxplot_embryos.py')

    embryo_individuals = list(embryo_individuals)
    sorted_indices = sorted(range(len(embryo_values)), key=lambda i: np.std(embryo_values[i]))
    sorted_embryo_values = [embryo_values[i] for i in sorted_indices]
    sorted_individuals = [embryo_individuals[i] for i in sorted_indices]

    # Write the script to generate boxplots
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt

# Embryo values and individual names
embryo_values = {sorted_embryo_values}
embryo_individuals = {sorted_individuals}

# Plot boxplots
plt.figure(figsize=(10, 6))
plt.boxplot(embryo_values, labels=embryo_individuals)
plt.xlabel('Individual')
plt.ylabel('Value')
plt.title('Boxplot of Values for Each Individual')
plt.xticks(rotation=45, ha='right')
plt.tight_layout()
plt.show()
    """)


def generate_heatmap_of_cell_variability_per_individual(divisions_properties, generations, data_folder, colorFlag,
                                                        output_folder):
    """
    For each individual, for each cell, computes (std percentage deviation from mean)
    Goal is to check two things :
        1- Are there embryos that are more bizarre or variability of division direction can occur in any embryo
        2- We know that division direction variability is not spatially distributed in the embryo, Is the division
            direction variability of one cell induces a high variability of neighboring cells ?
    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
                               Example: {
                               'mother_name': {
                                'division_directions_after_symmetry':
                                    {'left': {'individual name': direction vector},
                                    'right': {'individual name': direction vector} }
                                                },
                                'gen': int,
                                'cell_ids':{'left': {'individual name': id },
                                             'right': {'individual name': id } } ,
                                'std_from_mean_directions': std_a,
                                'division_mean_direction'
                                        }
    generations (list): cell generation to be considered
    data_folder (str):  The path to folder containing individuals pkl files (properties files)
    output_folder(str): The path to the output folder where the plot files will be saved.

    Returns
    -------
    .pkl (s) : updates embryo properties pkl file for eah embryo. Creates a dictionary key named
                'morphonet_selection_cell_deviation' to be recognized by

    """
    # filter by generation
    filtered_divisions = {mother: props for mother, props in divisions_properties.items() if
                          props.get('gen') in generations}

    for individual_name in os.listdir(data_folder):
        if individual_name.endswith('.pkl'):
            # Load individual properties
            individual_file = os.path.join(data_folder, individual_name)
            with open(individual_file, 'rb') as f:
                individual_properties = pkl.load(f)
            dict_cell_names = individual_properties['cell_name']
            dict_cells_lineage = individual_properties['cell_lineage']
            individual_name = individual_name.split('-')[1]
            # Create or update the dictionary for deviation
            deviation_dict = {}
            for mother, props in filtered_divisions.items():
                std_a = props['std_from_mean_directions']
                mean_direction = props['division_mean_direction']

                for side in ['left', 'right']:
                    # cell_id = props['cell_ids'][side].get(individual_name)
                    if colorFlag:
                        if side == 'left':
                            cell_ids = get_id_at_top_lineage(mother + '_', dict_cell_names)
                        elif side == 'right':
                            cell_ids = get_id_at_top_lineage(mother + '*', dict_cell_names)
                    else:
                        if side == 'left':
                            cell_ids = get_ids_at_division(mother + '_', dict_cell_names, dict_cells_lineage)
                        elif side == 'right':
                            cell_ids = get_ids_at_division(mother + '*', dict_cell_names, dict_cells_lineage)
                    direction = props['division_directions_after_symmetry'][side].get(individual_name)
                    # print(individual_name,mother, direction, mean_direction, std_a)
                    if direction is not None:
                        deviation = compute_std_distance(direction, mean_direction, std_a)
                        for cell_id in cell_ids:
                            deviation_dict[cell_id] = deviation

            # Update individual properties with deviation dictionary
            if colorFlag:
                individual_properties['morphonet_selection_cell_deviation'] = deviation_dict
            else:
                individual_properties['morphonet_selection_cell_deviation_starting_cell_division'] = deviation_dict
            # Save updated individual properties
            with open(individual_file, 'wb') as f:
                pkl.dump(individual_properties, f)
    return


#
####
def angle_between_vectors(v1, v2):
    """
    Calculate the angle between two vectors.
    """
    v1_u = v1 / np.linalg.norm(v1)
    v2_u = v2 / np.linalg.norm(v2)
    dot_product = np.dot(v1_u, v2_u)
    return np.arccos(np.clip(dot_product, -1.0, 1.0))


def compute_correlation_cell_deviation_of_div_direction_to_deviation_of_div_time(std_a, mean_di, div_dir,
                                                                                 std_t, mean_t, adjusted_div_times):
    """
    Given mean division direction, list of division directions, computes angle of each division direction to the mean.
    Given also the corresponding adjusted_div_times, computes covariance matrix between both variables adjusted_div_times
    and angles, returns eigen_values ratio.

    Parameters
    ----------
    mean_di : ndarray
        The mean division direction vector.
    div_dir : list of ndarray
        List of division direction vectors.
    mean_t : ndarray
        The mean division time.
    adjusted_div_times : list of float
        Corresponding adjusted division times.

    Returns
    -------
    eigen_values_ratio : float
        Ratio of the eigenvalues of the covariance matrix.
    """
    # standardize by std
    # Compute the angles between each division direction and the mean division direction
    angles = np.array([angle_between_vectors(mean_di, direction) / std_a for direction in div_dir])
    adjusted_div_times = [(t - mean_t) / std_t for t in adjusted_div_times]

    if len(angles) != len(adjusted_div_times):
        raise ValueError("The length of angles and adjusted_div_times must be the same.")

        # Compute Spearman correlation
    spearman_corr, _ = spearmanr(angles, adjusted_div_times)

    return spearman_corr

##########
#### Is variability not occuring at tissues border
##########
def plot_variability_vs_neighbours_tissuetype(divisions_properties, time_alignment_coefficients, generations,
                                              cell_name_to_fate, TissueGroupResolution, output_folder):
    """
    Check if division direction variability occuring only at tissues borders.
    Tissues resolution is specified.
    Generates a script that does the plot (x axis: avg division time, y axis: std_a, scatter color(red or blue):
    if neighbours have different fates), A scatter for each cell

    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
                               Example: {
                               'mother_name': {
                                'division_directions_after_symmetry':
                                    {'left': {'individual name': direction vector},
                                    'right': {'individual name': direction vector} }
                                                },
                                'gen': int,
                                'neighbors':{'left/right': {'individual name': {neighbour cell_name: surface of contact
                                 }}},
                                'cell_fate': cell fate
                                'std_from_mean_directions': std_a,
                                        }
    time_alignment_coefficients
    generations
    cell_name_to_fate : (dict) {cell name: cell fate}
    TissueGroupResolution
    output_folder

    Returns
    -------

    """
    tissue_border = [] # list of bool
    avg_div_times = []
    cell_names = []
    div_dir_var = []

    # Filter divisions properties for the current generation
    gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                     properties.get('gen') in generations}

    for mother, mother_data in gen_divisions.items():
        dif_fate = 0
        d = dif_fate
        div_dir_variability = mother_data['std_from_mean_directions']
        avg_div_time = compute_avg_division_time(mother_data.get('division_aq_time', {}),
                                                 time_alignment_coefficients)
        cell_fate = determine_fate_group(mother_data.get('cell_fate'))

        #print("######################")
        print('for cell', mother)
        for direction, dir_data in mother_data['neighbors'].items():
            for individual, individual_data in dir_data.items():
                if d == dif_fate:
                    print('no add contact in this individual')
                print(individual, direction)
                d=dif_fate
                for neighbor_name, neighbor_data in individual_data.items():
                    if neighbor_name == 'Background':
                        continue
                    neighbors_fate = determine_fate_group(cell_name_to_fate[neighbor_name])
                    print(neighbor_name, neighbors_fate)
                    if neighbors_fate != cell_fate:
                        #print('surface of contact percentage:', neighbor_data/mother_data['cell_surface'][direction][individual] )
                        #print('cell fate', cell_fate, 'neighbours fate(first dif encountered):', neighbors_fate)
                        dif_fate += 1
                        break
        #print(dif_fate/mother_data.get('nb_of_measurements',0), mother_data.get('nb_of_measurements',0))
        print(dif_fate, mother_data.get('nb_of_measurements',0), dif_fate!= mother_data.get('nb_of_measurements',0) )
        print("######################")
        tissue_border.append(dif_fate > 0)
        div_dir_var.append(div_dir_variability)
        cell_names.append(mother)
        avg_div_times.append(avg_div_time)
    # call plot
    script_plot_variability_vs_neighbours_tissuetype(tissue_border, div_dir_var, cell_names, avg_div_times,
                                                     output_folder)


def script_plot_variability_vs_neighbours_tissuetype(tissue_border, div_dir_var, cell_names, avg_div_times, output_folder):
    """
    Generates a script that creates a plot with avg division time on x-axis, std_a on y-axis, and scatter color indicating
    if neighbors have different fates. A scatter plot for each cell.

    Parameters
    ----------
    tissue_border (list): List indicating if neighbours have different fates.
    div_dir_var (list): List of division direction variabilities.
    cell_names (list): List of cell names.
    avg_div_times (list): List of average division times.
    output_folder (str): Path to the output folder to save the script.

    Returns
    -------
    None
    """
    script_content = f"""
import os
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

# Prepare DataFrame
data_dict = {{
    'Tissue Border': {tissue_border},
    'Division Direction Variability': {div_dir_var},
    'Cell Name': {cell_names},
    'Average Division Time': {avg_div_times}
}}
df = pd.DataFrame(data_dict)

# Plotting
plt.figure(figsize=(12, 8))
sns.scatterplot(
    x='Average Division Time',
    y='Division Direction Variability',
    hue='Tissue Border',
    palette={{True: 'red', False: 'blue'}},
    data=df,
    alpha=0.6
)

plt.title('Division Direction Variability vs. Neighbours Tissue Type')
plt.xlabel('Average Division Time')
plt.ylabel('Division Direction Variability')
plt.legend(title='Different Fates', loc='upper right')
plt.show()
# Save the plot
#output_path = os.path.join(r'{output_folder}', 'variability_vs_neighbours_tissuetype.png')
#plt.savefig(output_path)
#plt.close()
#print(f"Plot saved to {{output_path}}")
"""
    output_folder_path = os.path.join(output_folder, 'plot_variability_vs_neighbours_tissuetype')
    if not os.path.exists(output_folder_path):
        os.makedirs(output_folder_path)
    script_path = os.path.join(output_folder_path, 'plot_variability_vs_neighbours_tissuetype.py')
    with open(script_path, 'w') as script_file:
        script_file.write(script_content)

    print(f"Plotting script saved to {script_path}")



#############
####
#############

def plot_variability_vs_fate_precision(divisions_properties, time_alignment_coefficients, generations,
                                       TissueGroupResolution, output_folder):
    """
    Parameters
    ----------
    divisions_properties(dict): A dictionary containing division properties.
    time_alignment_coefficients
    generations
    output_folder(str): The path to the output folder where the plot files will be saved.
    Returns
    -------
    """

    def process_and_plot(selected_divisions, output_folder, generation_label="All Generations"):
        avg_div_times = []
        std_ts = []
        std_as = []
        state_colors = []
        mother_names = []
        correlation_t_vs_a = []

        for mother, props in selected_divisions:
            avg_div_time = compute_avg_division_time(props.get('division_aq_time', {}), time_alignment_coefficients)
            std_t, adjusted_div_times = compute_std_t(props.get('division_aq_time', {}), time_alignment_coefficients,
                                                      True)
            std_a = props.get('std_from_mean_directions', None)
            mother_fate = props.get('cell_fate')
            daughter1_fate = props.get('daughters_fate', {}).get(
                'first_daughter')  # it will be [] or None if no fate given
            daughter2_fate = props.get('daughters_fate', {}).get('second_daughter')
            mother_name = mother

            # Compute cell division variance ratio (correlation of division direction from mean to deviation of cell division time)
            directions = list(props.get('division_directions_after_symmetry', {}).get('left', {}).values())
            r_directions = list(props.get('division_directions_after_symmetry', {}).get('right', {}).values())
            directions.extend(r_directions)
            mean_a = props.get('division_mean_direction')

            # print(mother_name, mean_a,avg_div_time, std_a, std_t)
            a = compute_correlation_cell_deviation_of_div_direction_to_deviation_of_div_time(
                std_a, mean_a, directions, std_t, avg_div_time, adjusted_div_times)
            correlation_t_vs_a.append(a)

            # If TissueGroupResolution=True, fates of mother and daughters need to be transformed to tissue group
            if TissueGroupResolution:
                mother_fate = determine_fate_group(mother_fate, check_multiple=True)
                daughter1_fate = determine_fate_group(daughter1_fate, check_multiple=True)
                daughter2_fate = determine_fate_group(daughter2_fate, check_multiple=True)

            state_color = fate_division_color(mother_fate, daughter1_fate, daughter2_fate)

            avg_div_times.append(avg_div_time)
            std_ts.append(std_t)
            std_as.append(std_a)
            state_colors.append(state_color)
            mother_names.append(mother_name)

        generation_output_folder = os.path.join(output_folder,'variability_vs_fate_determination',
                                                generation_label.replace(" ", "_"))
        if not os.path.exists(generation_output_folder):
            os.makedirs(generation_output_folder)

        write_plot_variability_vs_fate_relation(avg_div_times, std_ts, std_as, state_colors, mother_names, generations,
                                                TissueGroupResolution, generation_output_folder)
        write_plot_dir_deviation_vs_time_deviation(correlation_t_vs_a, mother_names, generation_output_folder)

    # Process and plot for all generations
    selected_divisions = [(mother, props) for mother, props in divisions_properties.items() if
                          props.get('gen') in generations and props.get('nb_of_measurements') > 5]
    process_and_plot(selected_divisions, output_folder, "All Generations")

    # Process and plot for each generation separately
    for gen in generations:
        print('for:', gen)
        selected_divisions = [(mother, props) for mother, props in divisions_properties.items() if
                              props.get('gen') == gen and props.get('nb_of_measurements') > 5]
        process_and_plot(selected_divisions, output_folder, f"generation_{gen}")


def write_plot_dir_deviation_vs_time_deviation(correlation_t_vs_a, mother_names, output_folder):
    """
    Writes a python script that plots for each mother name the corresponding correlation value
    Parameters
    ----------
    correlation_t_vs_a
    mother_names
    output_folder

    Returns
    -------

    """
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    output_file = os.path.join(output_folder, 'correlation_plot_t_vs_div_dir.py')
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt

# Data
correlation_t_vs_a = {correlation_t_vs_a}
mother_names = {mother_names}

# Plot
plt.figure(figsize=(10, 6))
plt.bar(mother_names, correlation_t_vs_a, color='skyblue')
plt.xlabel('Mother Names')
plt.ylabel('Spearman_corr')
plt.title('Correlation between division directions deviation and division times deviation for Each Cell')
plt.xticks(rotation=90)
plt.tight_layout()
plt.grid(True)
plt.ylim(0,1)
plt.show()
""")


def write_plot_variability_vs_fate_relation(avg_div_times, std_ts, std_as, state_colors, mother_names, generations,
                                            resolution, output_folder):
    """
    Writes a Python script to create a scatter plot of std_t vs avg_t, the size of the scatter is related to std_a
    the color of the scatter is related to the state_color given by fate_division_color color function.
    Parameters:
        avg_div_times (list): List of average division times.
        std_ts (list): List of standard deviations of division times.
        std_as (list): List of standard deviations from mean directions.
        state_colors (list): List of colors representing the state of each division.
        mother_names (list): List of mother names.
        resolution(bool): boolean if true, tissue group is considered
        output_folder (str): The path to the output folder where the plot file will be saved.

    Returns:
        None
    """
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    if generations is None:
        generations_str = "all_generations"
    else:
        generations_str = "_".join(str(gen) for gen in generations)

    fate_resolution = {False: 'Cell_fate', True: 'Tissue_group'}

    output_file = os.path.join(output_folder,
                               f'variability_vs_fate_precision_script_gen_{generations_str}_resolution{fate_resolution[resolution]}.py')

    color_mapping = {
        "Mother has unique fate": 'blue',
        "Fate yet to be decided": 'green',
        'Either daughter cells will have its fate determined upon division': 'red',
        'Unknown': 'grey'
    }
    # todo adjust resolution in title
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

avg_div_times = {avg_div_times}
std_ts =  {std_ts}
std_as = {std_as}
state_colors = {state_colors}      
mother_names = {mother_names}
resolution = "{fate_resolution[resolution]}"
color_mapping = {color_mapping}

# Plot
fig, ax = plt.subplots(figsize=(12, 6))
legend_handles = []
for state_color in set(state_colors):
    legend_handles.append(mpatches.Patch(color=color_mapping[state_color], label=state_color))
for avg_t, std_t, std_a, state_color, mother_name in zip(avg_div_times, std_ts, std_as, state_colors, mother_names):
    ax.scatter(avg_t, std_t, s=20*std_a, c=color_mapping[state_color], label=state_color, alpha=0.5)
    ax.text(avg_t, std_t, mother_name)
ax.set_xlabel('Average Division Time')
ax.set_ylabel('Standard Deviation of Division Time')
ax.set_title("Variability vs. Fate Precision \%n Fate resolution = {resolution}")
legend1 = ax.legend(handles=legend_handles, loc='upper left')

# Second legend for scatter point sizes
sizes = [3, 15, 30]  # Example sizes for the legend
size_labels = ['3°', '15°', '30°']  # Example size labels
scatter_sizes = [ax.scatter([], [], s=20*size,color='grey', alpha=0.2, label=label) for size, label in zip(sizes, size_labels)]
legend2 = ax.legend(handles=scatter_sizes, loc='upper right', title='Dispersion of Division Directions')
ax.add_artist(legend2)
ax.add_artist(legend1)
plt.grid(True)
plt.show()

""")


def plot_boxplot_of_cell_division_directions(divisions_properties, generations, output_folder, show_fate=False):
    """
    Writes a file that plots the boxplot of cell division directions dispersion from mean direction.
    A figure is generated for each generation.
    Parameters:
    divisions_properties (dict): A dictionary containing division properties.
            Example: {'mother_name': {'division_directions': {'left/right':  value },
                                      'nb_of_measurements': int,
                                      'gen': int,
                                      'cell_fate': string or list }}
    generations (list): List of generations to include in the plot.
    output_folder (str): The path to the output folder where the plot files will be saved.
    show_fate (bool): Whether to show cell fates in the plot. Default is False.

    Returns:
    None
    """
    # Create the output folder if it doesn't exist
    output_folder = os.path.join(output_folder, 'mothers_division_directions_boxplot')
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # Iterate over each specified generation
    for gen in generations:
        # Filter divisions properties for the current generation
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == gen}

        # Collect division directions and cell fates
        directions_angle_to_mean = []
        mother_name_list = []
        fate_groups = []
        for mother_name, properties in gen_divisions.items():

            # if nb of mes is less than 5, ignore
            if properties.get('nb_of_measurements',0) < 5:
                continue

            division_directions = properties.get('division_directions_after_symmetry', {})
            mean_direction = properties.get('division_mean_direction', {})
            angles = []
            for lr, direction in division_directions.items():
                for ind, dir in direction.items():  # Iterate over directions and append angles
                    angles.append(angle_btw_vectors(dir, mean_direction))

                    if angle_btw_vectors(dir, mean_direction) > 20:
                        print(mother_name, ind+lr, angle_btw_vectors(dir, mean_direction))

            # If cell has multiple group fates : append into lists multiple times
            if isinstance(determine_fate_group(properties.get('cell_fate'), True), list):
                cell_fate_group = []
                for fate in determine_fate_group(properties.get('cell_fate'), True):
                    if fate not in cell_fate_group:
                        cell_fate_group.append(fate)
                        directions_angle_to_mean.append(angles)
                        mother_name_list.append(mother_name)
                        fate_groups.append(fate)
            else:
                directions_angle_to_mean.append(angles)
                mother_name_list.append(mother_name)
                fate_groups.append([determine_fate_group(properties.get('cell_fate')), True])

        # Write script to generate the boxplot
        write_boxplot_script_directions(gen, mother_name_list, directions_angle_to_mean, fate_groups, output_folder,
                                        show_fate)


def write_boxplot_script_directions(gen, mother_names, directions_angle_to_mean, fate_group, output_folder,
                                    show_fate=False):
    """
    Writes a Python script to generate a boxplot for the dispersion of cell division directions from the mean direction.

    Parameters:
    gen (int): Generation number.
    mother_names (list): List of mother cell names.
    directions_angle_to_mean (list): List of lists containing angles between division directions and mean direction for
     each mother.
    fate_group (list): List of fate groups.
    output_folder (str): Path to the output folder where the Python script will be saved.
    show_fate (bool): Whether to include cell fates in the plot. Default is False.

    Returns:
    None
    """
    # Create the output folder if it doesn't exist
    output_file = os.path.join(output_folder, f'boxplot_generation_{gen}.py')

    # Group mother names by fate group
    grouped_mothers = {}
    for i, mother in enumerate(mother_names):
        fate = fate_group[i]
        if type(fate) is list:
            fate = fate[0]
        if fate not in grouped_mothers:
            grouped_mothers[fate] = []
        grouped_mothers[fate].append((mother, directions_angle_to_mean[i]))

    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches

# Group mother names by fate group
grouped_mothers = {grouped_mothers}
grouped_mothers = dict(sorted(grouped_mothers.items()))
mothers = []
legend_handles = []

# Plot boxplots
plt.figure(figsize=(10, 6))
box_colors = {{'Epid.': 'red', 'NS': 'blue', 'Mesoderm': 'green', 'End.': 'purple'}}  
positions = 0
for fate, mothers_data in grouped_mothers.items():
    # pass in the loop according to lexicographic order of mother names 
    sorted_mothers_data = sorted(mothers_data, key=lambda x: x[0])
    for mother, data in sorted_mothers_data:
        plt.boxplot(data, positions=[positions], showfliers=True, patch_artist=True, 
                    boxprops=dict(facecolor=box_colors[fate]))
        mothers.append(mother)
        positions += 1
for fate, color in box_colors.items():
        legend_handles.append(mpatches.Patch(color=color, label=fate))
plt.xlabel('Mother name')
plt.ylabel('Dispersion from Mean Direction')
plt.title(f'Boxplot of Cell Division Direction Dispersion for Generation {gen}')
plt.xticks(np.arange(0, len(mothers)),  mothers, rotation=90)
plt.legend(handles=legend_handles)
plt.ylim(0,180)
plt.tight_layout()
plt.savefig('{output_file.replace('.py', '.png')}', dpi=300)

plt.show()

""")


#########################

def plot_scatter_division_time_avg_vs_division_registration_dispersion(divisions_properties,
                                                                       time_alignment_coefficients,
                                                                       cells_per_time_in_ref,
                                                                       generations,
                                                                       output_folder,
                                                                       show_fate=False):
    """
    Writes a file that plots the scatter plot of average adjusted division time vs registration dispersion.
    The considered generation can be specified.

    Parameters:
        divisions_properties (dict): A dictionary containing division properties.
            Example: {'mother_name': {'division_daughters_registration_dispersion': {'left/right':  value },
                                      'nb_of_measurements': int,
                                      {'division_aq_time': {'left/right': 'individual name': value_to_adjusted},
                                      'gen': int,
                                      'cell_fate': string }}
        time_alignment_coefficients (dict): A dictionary containing temporal alignment coefficients.
        cells_per_time_in_ref (dict): {'timePoint': nb_of_cells}
        generations (list): List of generations to consider.
        output_folder (str): The path to the output folder where the plot file will be saved.
        show_fate (bool): Flag to determine whether to show cell fate information in the plot. Default is False.

    Returns:
        None
    """
    list_avg_division_time = []
    list_divisions_reg_dispersion = []
    list_mother_names = []
    list_fate_group = []

    if generations is None:
        selected_divisions = divisions_properties.items()
    else:
        selected_divisions = [(mother, props) for mother, props in divisions_properties.items() if
                              props.get('gen') in generations]

    for mother, props in selected_divisions:
        avg_div_time = compute_avg_division_time(props.get('division_aq_time', {}), time_alignment_coefficients)
        reg_disp = max(props.get('division_daughters_registration_dispersion', {}).values())
        fate_group = determine_fate_group(props.get('cell_fate', None))

        if avg_div_time is not None and reg_disp is not None:
            list_avg_division_time.append(avg_div_time)
            list_divisions_reg_dispersion.append(reg_disp)
            list_mother_names.append(mother)
            list_fate_group.append(fate_group)

    # Write script that creates the plot
    write_scatter_plot_avg_div_time_vs_reg_disp_script(output_folder, list_avg_division_time,
                                                       list_divisions_reg_dispersion, cells_per_time_in_ref,
                                                       list_mother_names, generations, list_fate_group, show_fate)


def plot_scatter_division_time_std_vs_division_registration_dispersion(divisions_properties,
                                                                       time_alignment_coefficients,
                                                                       generations,
                                                                       output_folder,
                                                                       show_fate):
    """
    Writes a file that plot the scatter plot of std_t vs registration dispersion.
    The considered generation can be specified
    Parameters:
    divisions_properties (dict): A dictionary containing division properties.
            Example: {'mother_name': {'division_daughters_registration_dispersion': {'left/right':  value },
                                      'nb_of_measurements': int,
                                      {'division_aq_time': {'left/right': 'individual name': value_to_adjusted},
                                      'gen': int,
                                      'cell_fate': string }}
    time_alignment_coefficients
    generations (list)
    output_folder
    showFates (bool)

    Returns
    -------

    """
    list_cells_std_t = []
    list_divisions_reg_dispersion = []
    list_mother_names = []
    list_fate_group = []

    if generations is None:
        selected_divisions = divisions_properties.items()
    else:
        selected_divisions = [(mother, props) for mother, props in divisions_properties.items() if
                              props.get('gen') in generations]

    for mother, props in selected_divisions:
        std_t = compute_std_t(props.get('division_aq_time', {}), time_alignment_coefficients)
        reg_disp = max(props.get('division_daughters_registration_dispersion', {}).values())
        fate_group = determine_fate_group(props.get('cell_fate', None))

        if std_t is not None and reg_disp is not None:
            list_cells_std_t.append(std_t)
            list_divisions_reg_dispersion.append(reg_disp)
            list_mother_names.append(mother)
            list_fate_group.append(fate_group)
    write_scatter_plot_std_t_vs_reg_disp_script(output_folder, list_cells_std_t, list_divisions_reg_dispersion,
                                                list_mother_names, generations, list_fate_group, show_fate)


def write_scatter_plot_std_t_vs_reg_disp_script(output_folder, list_cells_std_t, list_divisions_reg_dispersion,
                                                list_mother_names, generations, list_fate_group=None, show_fate=False):
    """
    Writes a Python script to create a scatter plot of std_t vs registration dispersion.
    Parameters:
        output_folder (str): The path to the output folder where the plot file will be saved.
        list_cells_std_t (list): List of standard deviations of time (std_t).
        list_divisions_reg_dispersion (list): List of registration dispersion values.
        list_mother_names (list): List of mother cell names.
        generations (list or None): List of generations to include in the filename,
                                        or None if all generations are included.
        show_fate (bool)
        list_fate_group (list)
    Returns:
        None
    """
    output_folder = os.path.join(output_folder, "std_t_vs_registration_dispersion")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    if generations is None:
        generations_str = "all_generations"
    else:
        generations_str = "_".join(str(gen) for gen in generations)

    output_file = os.path.join(output_folder, f'scatter_plot_{generations_str}.py')
    with open(output_file, 'w') as f:
        f.write("# Script to create scatter plot of std_t vs registration dispersion\n")
        f.write("import matplotlib.pyplot as plt\n")
        f.write("import matplotlib.patches as mpatches\n")
        f.write("import os\n")
        f.write("\n")
        f.write("# Data\n")
        f.write(f"list_cells_std_t = {list_cells_std_t}\n")
        f.write(f"list_divisions_reg_dispersion = {list_divisions_reg_dispersion}\n")
        f.write(f"list_mother_names = {list_mother_names}\n")
        f.write("\n")
        f.write("# Create scatter plot\n")
        f.write("plt.figure(figsize=(8, 6))\n")
        if show_fate and list_fate_group:
            for i in range(len(list_cells_std_t)):
                f.write(f"plt.scatter({list_cells_std_t[i]},{list_divisions_reg_dispersion[i]},"
                        f" color='{colors.get(list_fate_group[i], 'black')}', label='{list_fate_group[i]}' "
                        f", alpha=0.5)\n")
                f.write(f"plt.text({list_cells_std_t[i]}, {list_divisions_reg_dispersion[i]}, '{list_mother_names[i]}',"
                        " fontsize=8)\n")
        else:
            f.write("plt.scatter(list_cells_std_t,list_divisions_reg_dispersion, color='blue', alpha=0.5)\n")
            for i, name in enumerate(list_mother_names):
                f.write(f"plt.text( {list_cells_std_t[i]},{list_divisions_reg_dispersion[i]}, '{name}', fontsize=8)\n")
        f.write("plt.ylabel('Registration Dispersion')\n")
        f.write("plt.xlabel('Standard Deviation of Division Time')\n")
        f.write("plt.title('Scatter Plot of Standard Deviation of Division Time vs. Registration Dispersion')\n")
        f.write("plt.grid(True)\n")
        f.write(f"legend_handles = [mpatches.Patch(color=color, label=label) for label, color in {colors}.items()]\n")
        f.write("plt.legend(handles=legend_handles)\n")
        f.write("plt.tight_layout()\n")
        f.write("\n")
        f.write("plt.show()\n")
        f.write("# Save plot\n")
        f.write(f"output_file = 'scatter_plot_{generations_str}.png'\n")
        f.write(f"plt.savefig(os.path.join('{output_folder}', output_file))\n")
        f.write("plt.close()\n")
        f.write("\n")


def write_scatter_plot_avg_div_time_vs_reg_disp_script(output_folder, list_avg_division_time,
                                                       list_divisions_reg_dispersion, cells_per_time_in_ref,
                                                       list_mother_names, generations, list_fate_group=None,
                                                       show_fate=False):
    """
    Writes a Python script to create a scatter plot of average adjusted division time vs registration dispersion.
    Parameters:
        output_folder (str): The path to the output folder where the plot file will be saved.
        list_avg_division_time (list): List of average adjusted division times.
        list_divisions_reg_dispersion (list): List of registration dispersion values.
        list_mother_names (list): List of mother cell names.
        cells_per_time_in_ref (dict): {'timePoint': nb_of_cells}
        generations (list or None): List of generations to include in the filename,
                                        or None if all generations are included.
        show_fate (bool)
        list_fate_group (list)
    Returns:
        None
    """
    output_folder = os.path.join(output_folder, "avg_div_time_vs_registration_dispersion")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    if generations is None:
        generations_str = "all_generations"
    else:
        generations_str = "_".join(str(gen) for gen in generations)

    output_file = os.path.join(output_folder, f'scatter_plot_{generations_str}.py')
    with open(output_file, 'w') as f:
        f.write("# Script to create scatter plot of average adjusted division time vs registration dispersion\n")
        f.write("import matplotlib.pyplot as plt\n")
        f.write("import matplotlib.patches as mpatches\n")
        f.write("import os\n")
        f.write("\n")
        f.write("# Data\n")
        f.write(f"list_avg_division_time = {list_avg_division_time}\n")
        f.write(f"list_divisions_reg_dispersion = {list_divisions_reg_dispersion}\n")
        f.write(f"list_mother_names = {list_mother_names}\n")
        f.write(f"cells_per_time_in_ref = {cells_per_time_in_ref}\n")
        f.write("\n")
        f.write("# Create scatter plot\n")
        f.write("fig, ax1 = plt.subplots(figsize=(8, 6))\n")
        if show_fate and list_fate_group:
            for i in range(len(list_avg_division_time)):
                f.write(f"ax1.scatter({list_avg_division_time[i]},{list_divisions_reg_dispersion[i]},"
                        f" color='{colors.get(list_fate_group[i], 'black')}', label='{list_fate_group[i]}' "
                        f", alpha=0.5)\n")
                f.write(
                    f"ax1.text( {list_avg_division_time[i]},{list_divisions_reg_dispersion[i]}, '{list_mother_names[i]}',"
                    " fontsize=8)\n")
        else:
            f.write("ax1.scatter(list_avg_division_time,list_divisions_reg_dispersion, color='blue', alpha=0.5)\n")
            for i, name in enumerate(list_mother_names):
                f.write(
                    f"ax1.text({list_divisions_reg_dispersion[i]}, {list_avg_division_time[i]}, '{name}', fontsize=8)\n")
        f.write("ax1.set_ylabel('Registration Dispersion')\n")
        f.write("ax1.set_xlabel('Average Adjusted Division Time')\n")
        f.write("ax1.set_title('Scatter Plot of Average Adjusted Division Time vs. Registration Dispersion')\n")
        f.write("ax1.grid(True)\n")

        # Creating a twin y-axis (right) for max_vectors_per_maxima
        f.write("# comment out to show nb of cells in ref embryo\n")
        f.write("#ax2 = ax1.twinx()\n")
        f.write("#ax2.plot(cells_per_time_in_ref.keys(), cells_per_time_in_ref.values(), color='red',"
                " marker='o', linestyle='-')\n")
        f.write("#ax2.set_ylabel('Number of Cells in Ref Embryo at Each Timestep', color='red')\n")
        f.write("#ax2.tick_params(axis='y', labelcolor='red')\n")
        f.write("#ax2.legend(loc='upper right')\n")
        f.write("#ax1.set_xlim(0,70)\n")
        f.write("#ax2.set_ylim(0,300)\n")

        f.write(f"legend_handles = [mpatches.Patch(color=color, label=label) for label, color in {colors}.items()]\n")
        f.write("plt.legend(handles=legend_handles)\n")
        f.write("plt.tight_layout()\n")
        f.write("\n")
        f.write("plt.show()\n")
        f.write("# Save plot\n")
        f.write(f"output_file = 'scatter_plot_{generations_str}.png'\n")
        f.write(f"plt.savefig(os.path.join('{output_folder}', output_file))\n")
        f.write("plt.close()\n")
        f.write("\n")


###########################################################

def plot_scatter_division_direction_std_vs_division_registration_dispersion(divisions_properties, generations,
                                                                            output_folder, show_fate=False):
    """
    Writes a file that plot the scatter plot of std_a vs registration dispersion.
    The considered generation can be specified
    Parameters:
    divisions_properties (dict): A dictionary containing division properties.
            Example: {'mother_name': {'division_daughters_registration_dispersion': {'left/right':  value },
                                      'std_from_mean_directions': std_a,
                                      'nb_of_measurements': int,
                                      'gen': int
                                      'cell_fate': string
                                       }}
    generations (list)
    output_folder
    show_fate (bool)

    Returns
    -------

    """
    list_cells_std_a = []
    list_divisions_reg_dispersion = []
    list_mother_names = []
    list_fate_group = []
    if generations is None:
        selected_divisions = divisions_properties.items()
    else:
        selected_divisions = [(mothers, properties) for mothers, properties in divisions_properties.items() if
                              properties.get('gen') in generations]
    for mothers, mother_props in selected_divisions:
        std_a = mother_props.get('std_from_mean_directions', None)
        reg_disp = max(mother_props.get('division_daughters_registration_dispersion', None).values())

        fate_group = determine_fate_group(mother_props.get('cell_fate', None))
        if std_a is not None and reg_disp is not None:
            list_cells_std_a.append(std_a)
            list_divisions_reg_dispersion.append(reg_disp)
            list_mother_names.append(mothers)
            list_fate_group.append(fate_group)

    # Write script that does the plot
    write_scatter_plot_reg_disp_vs_std_a_script(output_folder, list_divisions_reg_dispersion, list_cells_std_a,
                                                list_mother_names, generations, list_fate_group, show_fate)


def write_scatter_plot_reg_disp_vs_std_a_script(output_folder, list_divisions_reg_dispersion, list_cells_std_a,
                                                list_mother_names, generations, list_fate_group=None, show_fate=False):
    """
    Writes a Python script to create a scatter plot of std_a vs registration dispersion.
    Parameters:
        output_folder (str): The path to the output folder where the plot file will be saved.
        list_divisions_reg_dispersion (list): List of registration dispersion values.
        list_cells_std_a (list): List of standard deviations of direction (std_a).
        list_mother_names (list): List of mother cell names.
        generations (list or None): List of generations to include in the filename,
                                        or None if all generations are included.
        show_fate (bool)
        list_fate_group (list)
    Returns:
        None
    """
    output_folder = os.path.join(output_folder, "std_a_vs_registration_dispersion")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    if generations is None:
        generations_str = "all_generations"
    else:
        generations_str = "_".join(str(gen) for gen in generations)

    output_file = os.path.join(output_folder, f'scatter_plot_{generations_str}.py')
    with open(output_file, 'w') as f:
        f.write("# Script to create scatter plot of std_a vs registration dispersion\n")
        f.write("import matplotlib.pyplot as plt\n")
        f.write("import matplotlib.patches as mpatches\n")
        f.write("import os\n")
        f.write("\n")
        f.write("# Data\n")
        f.write(f"list_divisions_reg_dispersion = {list_divisions_reg_dispersion}\n")
        f.write(f"list_cells_std_a = {list_cells_std_a}\n")
        f.write(f"list_mother_names = {list_mother_names}\n")
        f.write("\n")
        f.write("# Create scatter plot\n")
        f.write("plt.figure(figsize=(8, 6))\n")
        if show_fate and list_fate_group:
            for i in range(len(list_divisions_reg_dispersion)):
                f.write(f"plt.scatter({list_cells_std_a[i]},{list_divisions_reg_dispersion[i]},"
                        f" color='{colors.get(list_fate_group[i], 'black')}', label='{list_fate_group[i]}' "
                        f", alpha=0.5)\n")
                f.write(f"plt.text( {list_cells_std_a[i]}, {list_divisions_reg_dispersion[i]},'{list_mother_names[i]}',"
                        " fontsize=8)\n")
        else:
            f.write("plt.scatter(list_cells_std_a,list_divisions_reg_dispersion, color='blue', alpha=0.5)\n")
            for i, name in enumerate(list_mother_names):
                f.write(f"plt.text({list_divisions_reg_dispersion[i]}, {list_cells_std_a[i]}, '{name}', fontsize=8)\n")
        f.write("plt.ylabel('Registration Dispersion')\n")
        f.write("plt.xlabel('Standard Deviation of Direction (std_a)')\n")
        f.write("plt.title('Scatter Plot of Standard Deviation of Division Direction vs. Registration Dispersion')\n")
        f.write("plt.grid(True)\n")
        f.write(f"legend_handles = [mpatches.Patch(color=color, label=label) for label, color in {colors}.items()]\n")
        f.write("plt.legend(handles=legend_handles)\n")
        # f.write(f"plt.legend({list(colors.values())}, {list(colors.keys())})\n")
        f.write("plt.tight_layout()\n")
        f.write("\n")
        f.write("plt.show()\n")
        f.write("# Save plot\n")
        f.write(f"output_file = 'scatter_plot_{generations_str}.png'\n")
        f.write(f"plt.savefig(os.path.join('{output_folder}', output_file))\n")
        f.write("plt.close()\n")
        f.write("\n")


def plot_scatter_std_division_direction_vs_division_time(divisions_properties, time_alignment_coefficients,
                                                         generations, output_folder, show_fate):
    """
    Writes a file that plot the scatter plot the std_a  vs std_t. The considered generations can be specified
    Parameters:
        divisions_properties (dict): A dictionary containing division properties.
            Example: {'mother_name': {'division_aq_time': {'left/right': 'individual name': value_to_adjusted},
                                      'std_from_mean_directions': std_a,
                                      'nb_of_measurements': None,
                                      'gen': int}}
        time_alignment_coefficients
        generations (list)
        output_folder
        showFates (bool)

    Returns
    -------

    """
    list_cells_std_a = []
    list_cells_std_t = []
    list_cells_avg_t = []
    list_mother_names = []
    list_fate_group = []

    if generations is None:
        selected_divisions = divisions_properties.items()
    else:
        selected_divisions = [(mothers, properties) for mothers, properties in divisions_properties.items() if
                              properties.get('gen') in generations and properties.get('nb_of_measurements',0)>5]

    for mothers, mother_props in selected_divisions:
        if mother_props.get('nb_of_measurements',0) < 5:
            continue

        std_a = mother_props.get('std_from_mean_directions', None)
        std_t = compute_std_t(mother_props['division_aq_time'], time_alignment_coefficients)
        avg_t = compute_avg_division_time(mother_props['division_aq_time'], time_alignment_coefficients)
        fate_group = determine_fate_group(mother_props.get('cell_fate', None), True)

        if isinstance(fate_group, list):
            fate_group = fate_group[0]

        if std_a is not None and std_t is not None:
            list_cells_std_a.append(std_a)
            list_cells_std_t.append(std_t)
            list_cells_avg_t.append(avg_t)
            list_mother_names.append(mothers)
            list_fate_group.append(fate_group)

    # Write script that does the plot
    write_std_t_vs_avg_t_plot_script(output_folder, list_cells_std_t, list_cells_std_a, list_mother_names, generations
                                     , list_fate_group, show_fate)
    write_std_t_vs_avg_t_plot_script(output_folder, list_cells_avg_t, list_cells_std_a, list_mother_names, generations
                                     , list_fate_group, show_fate, True)


def write_std_t_vs_avg_t_plot_script(output_folder, list_cells_std_t, list_cells_std_a, list_mother_names, generations,
                                     list_fate_group, show_fate, plot_avg_t=False):
    """
    Writes a Python script to create a scatter plot of std_a vs std_t or avg_t.
    Parameters:
        output_folder (str): The path to the output folder where the plot file will be saved.
        list_cells_std_t (list): List of standard deviations of time (std_t). or
            list_cells_avg_t (list): List of average times (avg_t).

        list_cells_std_a (list): List of standard deviations of direction (std_a).
        list_mother_names (list): List of mother cell names.
        generations (list or None): List of generations to include in the filename, or None if all generations are included.
        show_fate (bool)
        list_fate_group (list)
        plot_avg_t (bool): True if plotting avg_t vs std_a, False if plotting std_t vs std_a.
    Returns:
        None
    """
    output_folder = os.path.join(output_folder, "std_a_vs_std_t_or_avg_t")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    if generations is None:
        generations_str = "all_generations"
    else:
        generations_str = "_".join(str(gen) for gen in generations)

    if plot_avg_t:
        plot_type = "avg_t"
        list_x = list_cells_std_t
    else:
        plot_type = "std_t"
        list_x = list_cells_std_t

    output_file = os.path.join(output_folder, f'{plot_type}_vs_std_a_{generations_str}.py')
    print('plot in', output_file)
    with open(output_file, 'w') as f:
        f.write(f"# Script to create scatter plot of std_a vs {plot_type}\n")
        f.write("import matplotlib.pyplot as plt\n")
        f.write("import matplotlib.patches as mpatches\n")
        f.write("import os\n")
        f.write("\n")
        f.write("# Data\n")
        f.write(f"list_cells_std_a = {list_cells_std_a}\n")
        f.write(f"list_mother_names = {list_mother_names}\n")

        f.write("\n")
        f.write("# Create scatter plot\n")
        f.write("plt.figure(figsize=(8, 6))\n")

        if show_fate and list_fate_group:
            for i in range(len(list_x)):
                f.write(f"plt.scatter({list_x[i]}, {list_cells_std_a[i]}, "
                        f"color='{colors.get(list_fate_group[i], 'black')}', label='{list_fate_group[i]}' "
                        f", alpha=0.5, s={list_cells_std_a[i]}*25)\n")
                f.write(f"plt.text({list_x[i]}, {list_cells_std_a[i]}, '{list_mother_names[i]}',"
                        " fontsize=8)\n")
        else:
            f.write(f"plt.scatter(list_x, list_cells_std_a, color='blue', alpha=0.5)\n")
            for i, name in enumerate(list_mother_names):
                f.write(f"plt.text(list_x[i], {list_cells_std_a[i]}, '{name}', fontsize=8)\n")

        f.write(f"plt.xlabel('Average division time (developmental time)' if {plot_avg_t} else 'Standard Deviation of division time (developmental time)')\n")
        f.write("plt.ylabel('Dispersion of cell division direction')\n")
        f.write("plt.title('Division direction dispersion vs. Division time')\n")

        f.write(f"legend_handles = [mpatches.Patch(color=color, label=label) for label, color in {colors}.items()]\n")
        f.write("plt.legend(handles=legend_handles)\n")

        f.write("plt.grid(True)\n")
        f.write("plt.tight_layout()\n")
        f.write("\n")
        f.write("# Save plot\n")
        f.write(f"output_file = 'scatter_plot_{plot_type}_{generations_str}.png'\n")
        f.write(f"plt.savefig(os.path.join('{output_folder}', output_file))\n")
        f.write(f"plt.ylim(0,30)\n")
        f.write("plt.show()\n")
        f.write("\n")

def plot_mother_division_directions(divisions_properties, mother, output_folder):
    """
    Writes a file that plots for a given mother name the different division direction in a unit sphere.
    Puts label on each direction as the individual name wile specifying if its left or right
    Parameters:
        divisions_properties (dict): A dictionary containing division properties.
        Example: {'mother_name': {'division_directions_after_symmetry':
                                {'left': {'individual name': direction vector},
                                'right': {'individual name': direction vector}}}}
        mother (str): The name of the mother cell.
        output_folder

    Returns:
        None
    """
    output_folder = os.path.join(output_folder, "mother_division_directions")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    if mother not in divisions_properties:
        print(f"Mother cell {mother} not found in division properties.")
        return

    mother_division_props = divisions_properties[mother]
    left_directions = mother_division_props.get('division_directions_after_symmetry', {}).get('left', {})
    right_directions = mother_division_props.get('division_directions_after_symmetry', {}).get('right', {})
    mean = mother_division_props.get('division_mean_direction')
    write_directions_script(left_directions, right_directions, mean, mother, output_folder)


def write_directions_script(left_directions, right_directions, mean, mother, output_folder):
    """
    Writes a Python script to create a plot of division directions for a given mother cell.
    Parameters:
        left_directions (dict): Dictionary of left division directions.
        right_directions (dict): Dictionary of right division directions.
        mother (str): The name of the mother cell.
        output_folder (str): The path to the output folder where the plot file will be saved.
    Returns:
        None
    """
    # maybe add unit sphere
    output_file = os.path.join(output_folder, f'{mother}.py')
    output_folder = os.path.join(output_folder, 'figures')
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    fig_file = os.path.join(output_folder, f'{mother}.png')

    with open(output_file, 'w') as script_file:
        script_file.write(f"\n"
                          f"import matplotlib.pyplot as plt\n"
                          f"import numpy as np\n"
                          f"\n"
                          f"fig = plt.figure()\n"
                          f"ax = fig.add_subplot(111, projection='3d')\n"
                          f"\n"
                          f"# Plot left division directions\n"
                          f"left_directions = {[direction.tolist() for direction in left_directions.values()]}\n"
                          f"right_directions = {[direction.tolist() for direction in right_directions.values()]}\n"
                          f"mean = {mean.tolist()} \n"
                          f"ax.quiver(0, 0, 0, mean[0],  mean[1],  mean[2],color='red', label='mean')\n"
                          f"ax.text( mean[0],  mean[1],  mean[2], 'mean', color='red')\n"
                          f"left_individuals = {list(left_directions.keys())}\n"
                          f"right_individuals = {list(right_directions.keys())}\n"
                          f"for name, direction in zip(right_individuals,right_directions):\n"
                          f"    x, y, z = direction\n"
                          f"    ax.quiver(0, 0, 0, x, y, z, label=name+'_left')\n"
                          f"    ax.text(x, y, z, name+'_right', color='blue')\n"
                          f"\n"

                          f"# Plot right division directions\n"
                          f"for name, direction in zip(left_individuals,left_directions):\n"
                          f"    x, y, z = direction\n"
                          f"    ax.quiver(0, 0, 0, x, y, z, label=name+'_right')\n"
                          f"    ax.text(x, y, z, name+'_left', color='blue')\n"
                          f"\n"
                          f"ax.set_xlim([-1, 1])\n"
                          f"ax.set_ylim([-1, 1])\n"
                          f"ax.set_zlim([-1, 1])\n"
                          f"ax.set_xlabel('X')\n"
                          f"ax.set_ylabel('Y')\n"
                          f"ax.set_zlabel('Z')\n"
                          f"plt.title(f'Division Directions of Mother Cell {mother}')\n"
                          f"\n"
                          f"\n"
                          f"# Save the plot to a file\n"
                          f"plt.savefig('{fig_file}')\n"
                          f"plt.show()\n")


def plot_mother_division_times(divisions_properties, time_alignment_coefficients, mother, output_folder):
    """
    Writes a file that plots for a given mother name the division time as vline in different individuals,
    the label of each vline is the individual name and direction (left or right).

    Parameters:
        divisions_properties (dict): A dictionary containing division properties.
            Example: {'mother_name': {'division_aq_time': {'left/right': 'individual name': value_to_adjusted},
                                      'cell_fate': None,
                                      'nb_of_measurements': None,
                                      'gen': int}}
        time_alignment_coefficients (dict): A dictionary containing temporal alignment coefficients.
            Example: {'embryo name': [a, b]}
        mother (str): The name of the mother cell.
        output_folder (str): The path to the output folder where the plot file will be saved.

    Returns:
        None
    """
    output_folder = os.path.join(output_folder, "mother_division_times_across_dif_individuals")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # Check if mother is in divisions_properties, get its aq_time properties
    if mother not in divisions_properties:
        print(f"Mother cell {mother} not found in division properties.")
        return
    mother_division_props = divisions_properties[mother]
    individual_names = []
    adjusted_division_times = []
    for lr in mother_division_props['all_div_aq_times']:
        for individual_name, value_to_adjusted in mother_division_props['all_div_aq_times'][lr].items():
            # make the individual name
            individual_names.append(f"{individual_name}_{lr}")
            # get adjusted time and put in list
            a, b = time_alignment_coefficients.get(individual_name)
            adjusted_time = int(a * value_to_adjusted + b)
            adjusted_division_times.append(adjusted_time)

    # write a file that plots for each individual the adjusted time as a small vertical line in a plot
    write_division_time_vline_script(adjusted_division_times, individual_names, mother, output_folder)


def write_division_time_vline_script(adjusted_division_times, individual_names, mother, output_folder):
    """

    Parameters
    ----------
    adjusted_division_times
    individual_names
    mother_names
    output_file

    Returns
    -------

    """
    # TODO make y axis to for individuals assignment
    output_file = os.path.join(output_folder, f'{mother}.py')
    output_folder = output_folder + '/figs'
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    fig_file = os.path.join(output_folder, f'{mother}.png')

    with open(output_file, 'w') as script_file:
        script_file.write(f"\n"
                          f"import matplotlib.pyplot as plt\n"
                          f"\n"
                          f"plt.figure(figsize=(10, 6))\n"
                          f"plt.vlines({adjusted_division_times}, ymin=0, ymax=1, color='blue', alpha=0.5, linewidth=2)\n"
                          f"label_y_position = 0.5\n"

                          f"for i, (time, individual_name) in enumerate(zip({adjusted_division_times}, {individual_names})):\n"
                          f"    label_y_position = 0.2 + (i * 0.05)\n"
                          f"    plt.text(time, label_y_position, individual_name, rotation=45, ha='right', fontsize=8)\n"
                          f"plt.xticks({adjusted_division_times}, {adjusted_division_times}, rotation=45, ha='right')\n"
                          f"plt.xlabel('Adjusted Division Time')\n"
                          f"plt.title(f'Division Times of Mother Cell {mother} across different individuals')\n"
                          f"plt.gca().yaxis.set_visible(False)\n"
                          f"plt.tight_layout()\n"
                          f"\n"
                          f"# Save the plot to a file\n"
                          f"plt.savefig('{fig_file}')\n"
                          f"plt.show()\n")


def plot_boxplot_of_cell_division_time(divisions_properties, time_alignment, output_folder):
    """
    Writes a file that does the plot of boxplot for each mother cell of the acquisition time.
    A figure per gen.
    The acquisition time is adjusted by alignment coefficients in dictionary : adjusted_t = a*t +b
    Parameters:
    divisions_properties (dict): A dictionary containing division properties.
         Example: {'mother_name': {'division_aq_time': {'left/right': 'individual name': value_to_adjusted},
                                      'cell_fate': None,
                                      'nb_of_measurements': None,
                                      'gen': int}}
    time_alignment (dict) : temporal_alignment_coefficient_dict : {'embryo name': [a,b] }

    Returns
    -------
    """
    # TODO order by avg division time
    output_folder = os.path.join(output_folder, 'mothers_division_time_boxplot')
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # Create a figure for each generation
    for gen in set([properties['gen'] for properties in divisions_properties.values()]):
        # print('creating figure (cell division time boxplot) for generation:', gen)
        # Filter divisions properties for the current generation
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == gen}

        mother_names = []
        adjusted_times_per_mother = []
        for mother_name, properties in gen_divisions.items():
            mother_names.append(mother_name)
            adjusted_times = []
            for lr, ind_prop in properties['all_div_aq_times'].items():
                for embryo_name, aq_time in ind_prop.items():
                    # Get the alignment coefficients for the embryo
                    a, b = time_alignment[embryo_name]

                    # Adjust acquisition time using the alignment coefficients
                    adjusted_time = a * aq_time + b

                    # Append adjusted time to the list
                    adjusted_times.append(adjusted_time)
            adjusted_times_per_mother.append(adjusted_times)
        # In output_path writes a file that plots the boxplot of all cells for the current generation
        output_file = os.path.join(output_folder, f'gen{gen}.py')
        write_boxplot_script(gen, mother_names, adjusted_times_per_mother, output_file)


def write_boxplot_script(gen, mother_names, adjusted_times_per_mother, output_file):
    """
    Writes a Python script to generate a boxplot for a generation.

    Parameters:
    gen (int): Generation number.
    mother_names (list): List of mother cell names.
    adjusted_times_per_mother (list): List of lists containing adjusted acquisition times for each mother.
    output_file (str): Path to the output Python script.

    Returns
    -------
    None
    """
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt

# Adjusted acquisition times for generation {gen}
adjusted_times_per_mother = {adjusted_times_per_mother}
mothers = {mother_names}
# Plot boxplot
plt.figure()
plt.boxplot(adjusted_times_per_mother)
plt.xlabel('Mother Name')
plt.ylabel('Developmental Time')
plt.title(f'Boxplot of Cell Division Time for Generation {gen}')
plt.xticks(range(1, len(mothers) + 1), mothers, rotation=45, ha='right')
plt.show()
""")


def plot_ridgeline_of_cell_division_time(divisions_properties, time_alignment, output_folder):
    output_folder = os.path.join(output_folder, 'mothers_division_time_ridgeline')
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for gen in set([properties['gen'] for properties in divisions_properties.values()]):
        # Filter divisions properties for the current generation
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == gen}

        # Sort mothers by average division time
        gen_divisions_sorted = {mother_name: properties for mother_name, properties in
                                sorted(gen_divisions.items(), key=lambda x: np.nanmean(
                                    [np.nanmean(list(values.values())) for values in x[1]['all_div_aq_times'].values()
                                     if values]), reverse=False)}

        # Get all mother names and adjusted times
        mother_names = list(gen_divisions_sorted.keys())
        adjusted_times_per_mother = []
        mother_lists_per_chunk = []

        # Iterate over chunks of 10 mothers at a time
        for mothers_chunk in chunk_list(mother_names, 10):
            chunk_mother_names = []
            chunk_adjusted_times = []
            for mother_name in mothers_chunk:
                # Get adjusted times for the current mother
                adjusted_times = []
                for lr, ind_prop in gen_divisions[mother_name]['all_div_aq_times'].items():
                    for embryo_name, aq_time in ind_prop.items():
                        # Get the alignment coefficients for the embryo
                        a, b = time_alignment[embryo_name]

                        # Adjust acquisition time using the alignment coefficients
                        adjusted_time = a * aq_time + b

                        # Append adjusted time to the list
                        adjusted_times.append(adjusted_time)
                chunk_mother_names.append(mother_name)
                chunk_adjusted_times.append(adjusted_times)
            adjusted_times_per_mother.append(chunk_adjusted_times)
            mother_lists_per_chunk.append(chunk_mother_names)

        # Write a file for each chunk of mothers
        for i, (adjusted_times_chunk, mother_in_chunk) in enumerate(
                zip(adjusted_times_per_mother, mother_lists_per_chunk), start=1):
            output_file = os.path.join(output_folder, f'gen{gen}_part{i}.py')
            write_ridgeline_script(gen, mother_in_chunk, adjusted_times_chunk, output_file)


def chunk_list(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def write_ridgeline_script(gen, mother_names, adjusted_times_per_mother, output_file):
    """
    Writes a Python script to generate a ridgeline plot for a generation.

    Parameters
    ----------
    gen (int): Generation number.
    mother_names (list): List of mother cell names.
    adjusted_times_per_mother (list): List of lists containing adjusted acquisition times for each mother.
    output_file (str): Path to the output Python script.

    Returns
    -------
    None
    """
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
from joypy import joyplot
import numpy as np

# Adjusted acquisition times for generation {gen}
adjusted_times_per_mother = {adjusted_times_per_mother}
mothers = {mother_names}
# Create ridgeline plot
fig, axes = joyplot(adjusted_times_per_mother, labels=mothers)
plt.xlabel('Developmental Time')
plt.title(f'Ridgeline Plot of Cell Division Time for Generation {gen}')
plt.show()
""")


# add compactness of clusters
def write_variability_plot(list_mothers, list_d_trends, list_kernel_windows,
                           list_vectors_per_maxima, list_kernel_widths=[], filename='', file_suffix=None, output_dir=""):
    """
    input: mother_names,max_d_btw_max, kernel_windows, vectors_per_maxima,
    """

    proc = "VarDirection.figure"
    local_filename = filename

    if file_suffix is not None and isinstance(file_suffix, str) and len(file_suffix) > 0:
        localfile_suffix = '_' + file_suffix
        local_filename += localfile_suffix

    local_filename += '.py'

    if output_dir is not None and isinstance(output_dir, str):
        if not os.path.isdir(output_dir):
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            else:
                msg = "not a directory"
        if os.path.isdir(output_dir):
            local_filename = os.path.join(output_dir, local_filename)

    code_to_write = f"""
import matplotlib.pyplot as plt
import numpy as np

list_of_ind_names = {list_mothers}
max_d_btw_max = np.array({list_d_trends})
list_kernel_windows = {list_kernel_windows}
list_kernel_widths = {list_kernel_widths}

max_d_btw_max = np.rad2deg(max_d_btw_max)
#list_kernel_windows= np.rad2deg(list_kernel_windows)

max_vectors_per_maxima = np.array({list_vectors_per_maxima})

fig, ax1 = plt.subplots(figsize=(10, 6))
ax1.plot(list_of_ind_names, max_d_btw_max, color='blue', label='Max Distance Between Maximas')
ax1.plot(list_of_ind_names, np.array(list_kernel_windows), color='red', marker='o', label='Kernel width interval to get two modes')
ax1.plot(list_of_ind_names, np.array(list_kernel_widths), color='red', marker='x', label='Kernel width to get two modes')
ax1.set_xlabel('Division Mother Names')
ax1.set_ylabel('Degrees Values (Max Distance and Kernel Size Interval)', color='black')
ax1.tick_params(axis='y', labelcolor='black')
ax1.tick_params(axis='x', labelrotation=90)
ax1.legend(loc='upper left')

# Creating a twin y-axis (right) for max_vectors_per_maxima
ax2 = ax1.twinx()
ax2.plot(list_of_ind_names, np.array(max_vectors_per_maxima), color='green', marker='s', label='Max Portion of Vectors per Maxima')
ax2.set_ylabel('Max Portion of Vectors per Maxima', color='green')
ax2.tick_params(axis='y', labelcolor='green')
ax2.legend(loc='upper right')

plt.title('Variability of Different Cells Division Directions')
plt.xticks(rotation=180)
plt.tight_layout()
#plt.savefig('Ordered_div_var.png')
plt.show()
"""

    with open(local_filename, 'w') as file:
        file.write(code_to_write)

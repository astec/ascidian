import astec.utils.common as common


class DivisionAnalysisParameters:
    """
    This class will now manage parameters from python file.
    init: generates doc(to be fixed)
          loads parameter from file (returns object: params.param=value)
    """

    def __init__(self, param_file=None):
        if "doc" not in self.__dict__:
            self.doc = {}

            # Initialize all parameters with default values
            self.folder = None
            self.nb_of_modes = 2
            self.generations = []
            self.sphere_radius = 20
            self.initial_sigma = 0.01
            self.sigma_step = 0.01

            # Descriptions (docs) for each parameter
            self.doc['folder'] = ("Path to the folder containing the pickle file division_properties."
                                  "\nFolder to save the plots generated from the analysis.")
            self.doc['nb_of_modes'] = "The number of modes or groups to cluster observations (default: 2)."
            self.doc['generations'] = ("List of generations to consider for analysis. "
                                       "If not provided, all generations are considered.")
            self.doc['sphere_radius'] = "Radius of the sphere used for analysis (default: 20)."
            self.doc['initial_sigma'] = "Initial value of sigma for calculations (default: 0.01)."
            self.doc['sigma_step'] = "Step size for sigma during analysis (default: 0.01)."

            # Load parameters from file if provided
            if param_file:
                self.load_parameters_from_file(param_file)

    def print_parameters(self):
        """
        Print all parameters for verification.
        """
        print("Division Analysis Parameters:")
        for param, value in vars(self).items():
            if param != "doc":
                print(f"{param}: {value}")

    def load_parameters_from_file(self, param_file):
        """
        Load parameters from a Python file.
        """
        parameters = {}
        with open(param_file, 'r') as file:
            exec(file.read(), {}, parameters)

        self.update_from_parameters(parameters)

    def update_from_parameters(self, parameters):
        """
        Update class attributes with parameters loaded from a file.
        """
        self.folder = parameters.get('folder', self.folder)
        self.nb_of_modes = parameters.get('nb_of_modes', self.nb_of_modes)
        self.generations = parameters.get('generations', self.generations)
        self.sphere_radius = parameters.get('sphere_radius', self.sphere_radius)
        self.initial_sigma = parameters.get('initial_sigma', self.initial_sigma)
        self.sigma_step = parameters.get('sigma_step', self.sigma_step)

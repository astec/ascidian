import astec.utils.common as common
import ascidian.components.parameters as cparameters

monitoring = common.Monitoring()

class DivisionAnalysisParameters:
    """
    This class will now manage parameters from python file.
    init: generates doc(to be fixed)
          loads parameter from file (returns object: params.param=value)
    """

    def __init__(self, param_file=None):
        if "doc" not in self.__dict__:
            self.doc = {}

        # Initialize all parameters with default values
        self.folder = None
        self.data_folder = None
        self.approximate_apical_direction = False
        self.figure = None
        self.TissueGroupResolution = True
        self.colorFlag = True
        self.mother = None
        self.generations = [7]
        self.cumulutatif = True
        self.threshold = None
        self.showFate = True

        # Descriptions (docs) for each parameter
        self.doc['folder'] = 'Path to the folder containing pickle file division_properties'
        self.doc['data_folder'] = 'Folder with embryos properties'
        self.doc['approximate_apical_direction'] = 'Approximate apical longest axis, updates division properties pkl'
        self.doc['figure'] = (
            'Type of plot to generate, choose from various figure options. Available options include:\n'
            '- **division_time_boxplot**: Boxplot of cell division times.\n'
            '- **division_mother_lifetimes_dispersion**: Boxplot of directions and variability in cell lifetimes.\n'
            '- **division_direction_dispersion_boxplot**: Boxplot of division directions with or without '
            'fate options.\n'
            '- **division_time_ridgeline**: Ridgeline plot of cell division times.\n'
            '- **mother_division_time_vline**: Vertical lines for division times of specified mother cells.\n'
            '- **directions_var_vs_division_time_variability**: Scatter plot of direction variability versus division'
            ' time variability.\n'
            '- **directions_var_vs_spatial_pos**: Plot of direction variability versus spatial position.\n'
            '- **division_registration_dispersion_vs_sigma_a**: Scatter plot of division direction standard deviation'
            ' versus registration dispersion.\n'
            '- **division_registration_dispersion_vs_sigma_t**: Scatter plot of division time standard deviation '
            'versus registration dispersion.\n'
            '- **division_registration_dispersion_vs_avg_t**: Scatter plot of average division time versus'
            ' registration dispersion.\n'
            '- **3d_directions**: 3D plot of division directions for specified mother cells.\n'
            '- **var_vs_fate_precision**: Plot of variability versus fate precision.\n'
            '- **var_tissues**: Plot of variability versus tissue type contacts.\n'
            '- **generate_heatmap_of_cell_variability_per_individual**: Heatmap of cell variability per'
            ' individual, with optional color flag.\n'
            '- **boxplot_of_cells_relative_distance_from_mean_per_individual**: Boxplot of relative distances'
            ' from the mean per individual.\n'
            '- **division_direction_and_neighbors_deviation_from_mean**: Scatter plot of division direction'
            ' and neighbors deviation from mean.\n'
            '- **division_direction_deviation_vs_surface_of_contacts**: Plot of division direction deviation'
            ' versus surface of contacts.\n'
            '- **mother_div_dir_deviation_to_division_order**: Plot of deviation of division direction from mean to '
            'division order.\n'
            '- **dendrogram_div_directions_surface_of_contact**: Dendrogram plots of division directions and surface of'
            ' contact.\n'
            '- **neighbors_division_time_vs_division_dir_deviation**: Plot of division time versus direction'
            ' deviation for neighbors.\n'
            '- **boxplot_deviation_div_direction_apical_surface_longest_axis**: Boxplot of deviation in '
            'division direction versus apical surface longest axis.\n'
            '- **mother_cell_apical_surface_directions**: Plot of apical surface directions for specified'
            ' mother cells.\n'
            '- **boxplot_deviation_div_direction_to_longest_axis**: Boxplot of deviation of division direction to '
            'the longest axis.\n'
            '- **mother_cell_pca_directions**: PCA directions for mother cells.\n'
            '- **sister_plane_deviation_to_barycenter_axis**: Plot of sister plane deviation to barycenter axis.\n'
            '- **inter_individuals_div_deviations**: Compute division deviations between individuals over time.\n'
            '- **inter_individuals_dmatrix_div_order**: Compute distance matrix based on cell division '
            'order and time.\n'
            '- **inter_individuals_dmatrix_cells_lifetimes**: Compute distance matrix based on cell lifetimes.\n'
            '- **div_dir_deviation_lr_to_other_indiv**: Compare left-right deviation to other individuals within the'
            ' same pattern.\n'
            '- **shape_analysis_between_different_clusters_of_same_cells**: Shape analysis between different clusters '
            'of the same cells.\n'
            '- **time_vol_analysis_between_different_clusters_of_same_cells**: Time and volume analysis between '
            'different clusters of the same cells.\n'
            '- **inter_gen_div_dir_var**: Analysis of division direction variability between generations.\n'
        )
        self.doc['TissueGroupResolution'] = 'Fate resolution for tissue analysis, defaults to tissue group'
        self.doc['colorFlag'] = 'Flag to generate a heatmap with values starting from division time, default is True'
        self.doc['mother'] = 'Mother cell name for specific plots'
        self.doc['generations'] = 'List of generations to consider in the scatter plot, default is 7'
        self.doc['cumulutatif'] = 'Whether to consider cumulative data or data between division waves, default is True'
        self.doc['threshold'] = 'Percentage threshold for surface of contact between neighboring cells'
        self.doc['showFate'] = 'Flag to consider fate as a color in scatter plots, default is True'

        if param_file:
            self.load_parameters_from_file(param_file)

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# Analysis Parameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        cparameters.BaseParameters.write_parameters_in_file(self, logfile)

        # Writing each parameter with its value and description
        self.varwrite(logfile, 'folder', self.folder, self.doc.get('folder', None))
        self.varwrite(logfile, 'data_folder', self.data_folder, self.doc.get('data_folder', None))
        self.varwrite(logfile, 'approximate_apical_direction', self.approximate_apical_direction,
                      self.doc.get('approximate_apical_direction', None))
        self.varwrite(logfile, 'figure', self.figure, self.doc.get('figure', None))
        self.varwrite(logfile, 'TissueGroupResolution', self.TissueGroupResolution,
                      self.doc.get('TissueGroupResolution', None))
        self.varwrite(logfile, 'colorFlag', self.colorFlag, self.doc.get('colorFlag', None))
        self.varwrite(logfile, 'mother', self.mother, self.doc.get('mother', None))
        self.varwrite(logfile, 'generations', self.generations, self.doc.get('generations', None))
        self.varwrite(logfile, 'cumulutatif', self.cumulutatif, self.doc.get('cumulutatif', None))
        self.varwrite(logfile, 'threshold', self.threshold, self.doc.get('threshold', None))
        self.varwrite(logfile, 'showFate', self.showFate, self.doc.get('showFate', None))

        logfile.write("\n")

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    def print_parameters(self):
        """
        Print out all parameters for verification
        """
        print("Division Analysis Parameters:")
        for param, value in vars(self).items():
            if param != "doc":
                print(f"{param}: {value}")

    def load_parameters_from_file(self, param_file):
        """
        Dynamically load parameters from a Python file.
        """
        parameters = {}
        with open(param_file, 'r') as file:
            exec(file.read(), {}, parameters)

        # parameters = common.load_source(param_file) # dunno what it does

        self.update_from_parameters(parameters)

    def update_from_parameters(self, parameters):
        """
        Update class attributes based on the loaded parameters.
        """
        self.folder = parameters.get('folder', self.folder)
        self.data_folder = parameters.get('data_folder', self.data_folder)
        self.approximate_apical_direction = parameters.get('approximate_apical_direction',
                                                           self.approximate_apical_direction)
        self.figure = parameters.get('figure', self.figure)
        self.TissueGroupResolution = parameters.get('TissueGroupResolution', self.TissueGroupResolution)
        self.colorFlag = parameters.get('colorFlag', self.colorFlag)
        self.mother = parameters.get('mother', self.mother)
        self.generations = parameters.get('generations', self.generations)
        self.cumulutatif = parameters.get('cumulutatif', self.cumulutatif)
        self.threshold = parameters.get('threshold', self.threshold)
        self.showFate = parameters.get('showFate', self.showFate)

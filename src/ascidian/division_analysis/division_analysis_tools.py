# function to compute mean division direction
# function to compute nb_of_measurements

# function to compute std
# function to compute registration dispersion for a cell

import numpy as np
from scipy import stats
import os
from sklearn.decomposition import PCA
import pickle as pkl
import statistics


#######
# fates groups
epid_group = ['Midline Tail Epidermis', 'Head Epidermis', 'Midline Tail Epidermis', 'Medio-Lateral Tail Epidermis',
              'Lateral Tail Epidermis']
endo_group = ['Anterior Head Endoderm', 'Posterior Head Endoderm', '2nd Endodermal Lineage', '1st Endodermal Lineage']
ns_group = ['Posterior Lateral Neural Plate', 'Anterior Dorsal Neural Plate', 'Posterior Ventral Neural Plate',
            'Anterior Ventral Neural Plate', 'Posterior Dorsal Neural Plate']
meso_group = ['2nd Lineage, Notochord', '1st Lineage, Tail Muscle', 'Trunk Lateral Cell', '2nd Lineage, Tail Muscle',
              '1st Lineage, Notochord', 'Mesenchyme', 'Trunk Ventral Cell']
fate_group_names = ['Epid.', 'End.', 'NS', 'Mesoderm']
fate_groups = [epid_group, endo_group, ns_group, meso_group]

colors = {'Epid.': 'red', 'NS': 'blue', 'Mesoderm': 'green', 'End.': 'purple'}

##############
##############
### registered daughters barycenters dispersion is related to cell's division direction
# registered daughters merged barycenters is a better criteria (local) than global residue
def compute_merged_barycenter(barycenter1, barycenter2, volume_ratio):
    """
    Compute the barycenter of two merged cells given their individual barycenters and the volume ratio.

    Parameters:
    barycenter1 (np.array): Barycenter of the first cell.
    barycenter2 (np.array): Barycenter of the second cell.
    volume_ratio (float): Ratio of the volume of the second cell to the total volume (v2 / (v1 + v2)).

    Returns:
    np.array: Barycenter of the merged cells.
    """
    return (1 - volume_ratio) * np.array(barycenter1) + volume_ratio * np.array(barycenter2)


def compute_local_merged_daughters_residues(division_barycenters_registration, divisions_volume_ratios):
    """
    for a given division we have its daughters barycenters registered and after symetry and its volume ratio
    Computes the merged daughters dispersion of points
    For each division we have two daughters first and last in each left/right of each indvidual

    Parameters
    ----------
    division_barycenters_registration (dict)  {'left/'right':{ 'first/second': 'individual name': {barycenter_3d_coord}}}
    divisions_volume_ratios (dict) {'left/'right': {'individual name': (v1/v1+v2)} }

    Returns
    -------

    """
    # compute list of registerd merged daughters
    merged_barycenters = []
    for dir, prop in division_barycenters_registration.items():
        for d, d_prop in prop.items():
            for ind, i_prop in d_prop.items():
                barycenter1 = division_barycenters_registration[dir]['first'][ind]
                barycenter2 = division_barycenters_registration[dir]['second'][ind]
                volume_ratio = divisions_volume_ratios[dir][ind]

                merged_barycenter = compute_merged_barycenter(barycenter1, barycenter2, volume_ratio)
                merged_barycenters.append(merged_barycenter)

    # compute dispersion of points
    dispersion = compute_geom_mean(merged_barycenters) # geometric mean of eigenvalues sqrt(ellipsoid radius)
    return dispersion

def returns_merged_daughters_residues(division_barycenters_registration, divisions_volume_ratios):
    """
    for a given division we have its daughters barycenters registered and after symetry and its volume ratio
    Computes the merged daughters
    For each division we have two daughters first and last in each left/right of each indvidual

    Parameters
    ----------
    division_barycenters_registration (dict)  {'left/'right':{ 'first/second': 'individual name': {barycenter_3d_coord}}}
    divisions_volume_ratios (dict) {'left/'right': {'individual name': (v1/v1+v2)} }

    Returns
    -------

    """
    # compute list of registerd merged daughters
    merged_barycenters = {}
    for dir, prop in division_barycenters_registration.items():
            for ind, i_prop in prop['first'].items():
                barycenter1 = division_barycenters_registration[dir]['first'][ind]
                barycenter2 = division_barycenters_registration[dir]['second'][ind]
                volume_ratio = divisions_volume_ratios[dir][ind]

                merged_barycenter = compute_merged_barycenter(barycenter1, barycenter2, volume_ratio)
                if dir not in merged_barycenters:
                    merged_barycenters[dir] = {}
                if ind not in merged_barycenters[dir]:
                    merged_barycenters[dir][ind] = {}
                merged_barycenters[dir][ind] = merged_barycenter

    return merged_barycenters

##########

####################
### Shape related comparisions: computed from surace, volume or eigenvectors and eigenvalues
#  ‘‘Entropy,’’(object compactness, (0,100: for sphere) )
#  ‘‘Elongation,’’ ‘‘Flatness,’’ (from bounding box) we dont have
# ‘‘Sphericity,’’( Silva  et al. from volume and surface)
# ‘‘Surface over volume’’ (or ‘‘S/V’’) ratio,

def surface_volume(surface, volume):
    """
    Computes Surface/volume ratio
    for better compactness measure maybe change this to entropy measure
    maybe make surface_volume ration dimensioneless
    Higher => less compact
    Parameters
    ----------
    eigenvalues (array): Eigenvalues list

    Returns
    -------
    surface_volume_ratio (float):
    """
    surface_volume_ratio = surface**3 / volume**2
    return surface_volume_ratio


def sphercity(eigenvalues):
    """
    Computes the sphercity based on eigenvalues.
    if 1 => more like a sphere

    Parameters
    ----------
    eigenvalues (array): Eigenvalues list

    Returns
    -------
    sphercity (float): Elongation value.
    """
    sphercity = np.max(eigenvalues) / np.min(eigenvalues)
    return sphercity


def elongation(eigenvalues):
    """
    Computes the elongation based on eigenvalues.
    if 0 => more elongated

    Parameters
    ----------
    eigenvalues (array): Eigenvalues list

    Returns
    -------
    elongation (float): Elongation value.
    """
    elongation_value = eigenvalues[1] / np.max(eigenvalues)
    return elongation_value


#####################

def compare_division_times(div_times1, div_times2):
    # Ensure both lists are the same length
    assert len(div_times1) == len(div_times2)

    # Get combinations of pairs within each list
    combinations1 = list(itertools.combinations(div_times1, 2))
    combinations2 = list(itertools.combinations(div_times2, 2))

    # Compare corresponding pairs from the two lists
    paired_comparisons = list(zip(combinations1, combinations2))

    return paired_comparisons



def filter_and_sort_divisions_properties_by_generation(divisions_properties, generation):
    """
    Filters and sorts division properties for the specified generation.

    Parameters
    ----------
    divisions_properties : dict
        A dictionary containing division properties.
    generation : int
        The generation to filter by.

    Returns
    -------
    dict
        A dictionary containing filtered and sorted division properties for the specified generation.
    """
    # Filter divisions properties for the specified generation
    gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                     properties.get('gen') == generation}
    # Sort the filtered dictionary by mother name
    gen_divisions = dict(sorted(gen_divisions.items()))

    return gen_divisions



def average_t_for_cells(nb_of_cells_at_t, target_cells):
    """
        Calculate the average time values for which the number of cells is equal to specified target values.

        Parameters:
        nb_of_cells_at_t (dict): A dictionary where keys are time points (t) and values are the number of cells at that
         time.
        target_cells (list): A list of target cell counts for which to calculate the average time values.

        Returns:
        dict: A dictionary where keys are the target cell counts and values are the average time points for which
        the number of cells matches the target counts. If no matching time points are found for a target cell count,
        the value will be None. The first element is the first aq time, the last is the last aq time

        Example:
        >>> nb_of_cells_at_t = {0: 50, 1: 64, 2: 76, 3: 64, 4: 112, 5: 76, 6: 64}
        >>> target_cells = [64, 76, 112]
        >>> average_t_for_cells(nb_of_cells_at_t, target_cells)
        {0, 3.3333333333333335,  3.5, 4.0, 4}
    """
    result = []

    smallest_nb_of_cells = min(nb_of_cells_at_t.values())
    largest_nb_of_cells = max(nb_of_cells_at_t.values())
    smallest_times = [t for t, nb_cells in nb_of_cells_at_t.items() if nb_cells == smallest_nb_of_cells]
    largest_times = [t for t, nb_cells in nb_of_cells_at_t.items() if nb_cells == largest_nb_of_cells]

    if isinstance(smallest_times, list):
        result.append(smallest_times[0])
    else:
        result.append(smallest_times)

    for target in target_cells:
        matching_times = [t for t, nb_cells in nb_of_cells_at_t.items() if nb_cells == target]
        if matching_times:
            sorted_times = sorted(matching_times)
            # Calculate the median
            median_index = len(sorted_times) // 2
            median_value = sorted_times[median_index]
            # Find the closest value to the median
            closest_value = min(sorted_times, key=lambda x: abs(x - median_value))

            result.append(closest_value)
        else:
            result[target] = None

    if isinstance(largest_times, list):
        result.append(largest_times[-1])
    else:
        result.append(largest_times)

    return result


def determine_fate_group(cell_fate, check_multiple=False):
    """
    Determines the fate group of a cell's fate.
    Parameters:
        cell_fate (str): The fate of the cell.
        check_multiple(bool): checks multiple fate groups
    Returns:
        str: The fate group of the cell's fate.

    """
    # take into consideration multiple fate groups
    if cell_fate is None:
        return 'Unknown'
    # multiple for boxplot
    if check_multiple:
        cell_tissue_groups = []
        for idx, group in enumerate(fate_groups):
            # cell fate might be a list
            if isinstance(cell_fate, list):
                for fate in cell_fate:
                    if fate in group:
                        if fate_group_names[idx] not in cell_tissue_groups:
                            cell_tissue_groups.append(fate_group_names[idx])

            elif cell_fate in group:
                cell_tissue_groups.append(fate_group_names[idx])
        return cell_tissue_groups
    # if not multiple (for scatters)
    for idx, group in enumerate(fate_groups):
        if isinstance(cell_fate, list):
            for fate in cell_fate:
                if fate in group:
                    return fate_group_names[idx]
        else:
            if cell_fate in group:
                return fate_group_names[idx]
    return 'Unknown'


def sort_mother_cells(mother_names_list, list_values):
    """
    Sort both list values according to lexicographic order of values in mother_names_list
    Parameters
    ----------
    mother_names_list
    list_values

    Returns
    -------

    """
    # Combine mother names list and list values
    combined = list(zip(mother_names_list, list_values))

    # Sort the combined list based on the lexicographic order of mother names
    combined.sort(key=lambda x: x[0])

    # Extract sorted lists
    sorted_names_list, sorted_values = zip(*combined)

    return sorted_names_list, sorted_values

def angle_vector_to_direction(apical, div):
    angle = np.arccos(np.dot(apical, div) / (np.linalg.norm(apical) * np.linalg.norm(div))) * (180 / np.pi)
    inverted_angle = np.arccos(np.dot(-apical, div) / (np.linalg.norm(apical) * np.linalg.norm(div))) * (
            180 / np.pi)
    min_angle = min(angle, inverted_angle)
    return min_angle


def compute_std_distance(direction, mean_direction, std_a):
    """
    Give deviation of direction as percentage of std_a from mean direction
    Parameters
    ----------
    direction
    mean_direction
    std_a

    Returns
    -------
    distance
    """
    # Calculate the angle between the direction and the mean direction
    angle = np.arccos(np.dot(direction, mean_direction) / (np.linalg.norm(direction) * np.linalg.norm(mean_direction)))

    # Convert angle to degrees
    angle_deg = np.degrees(angle)

    # Calculate the distance as percentage of std_a
    distance = angle_deg / std_a

    return distance


def get_id_at_top_lineage(cell_name, cell_name_dict):
    """
    Get the cell ID associated with the provided cell name starting the top lineage till division.

    Parameters:
        cell_name (str): The name of the cell to find.
        cell_name_dict (dict): A dictionary mapping cell IDs to cell names.

    Returns:
        str or None: The cell ID associated with the provided cell name in the top lineage, or None if not found.
    """
    cell_ids = []
    for cell_id, name in cell_name_dict.items():
        if name == cell_name:
            cell_ids.append(cell_id)
    return cell_ids


def get_ids_at_division(cell_name, cell_name_dict, cells_lineage):
    """
        Get the cell IDs when the provided cell divides and the IDs of its descendants.

        Parameters:
            cell_name (str): The name of the cell to find.
            cell_name_dict (dict): A dictionary mapping cell IDs to cell names.
            cells_lineage (dict): A dictionary mapping cell IDs to their lineage information.

        Returns:
            tuple: A tuple containing two lists:
                - The first list contains the cell IDs when the provided cell divides.
                - The second list contains the IDs of the descendants of the provided cell.
        """
    cell_ids = get_id_at_top_lineage(cell_name, cell_name_dict)
    division_ids = []
    for cell_id in cell_ids:
        lineage = cells_lineage.get(cell_id)
        if lineage and len(lineage) == 2:
            division_ids.append(cell_id)
            division_ids.extend(lineage)
    return division_ids


def fate_division_color(mother_fate, daughter1_fate, daughter_2_fate):
    """
    Give division's fate precision state
    if |mother_fate|=1 => fate decided previously => expected to have higher variability (red)
    if |mother_fate|== |daughter1_fate| = = |daughter2_fate| => Fate yet to be decided (yet) => expected to have smaller
                                                                                                       variability(green)
    if |mother_fate| > |daughter1_fate| or  > |daughter2_fate| => Fate decision has been made => expected to have
                                                                                            smaller variability(orange)
    Parameters
    ----------
    mother_fate (list of strings or string)
    daughter1_fate (list of strings or string)
    daughter_2_fate (list of strings or string)

    Returns
    -------
    state_color
    """
    if mother_fate is None:
        return 'Unknown'

    # Calculate the lengths of the fate lists
    mother_len = len(mother_fate) if isinstance(mother_fate, list) else 1
    daughter1_len = len(daughter1_fate) if isinstance(daughter1_fate, list) else 1
    daughter2_len = len(daughter_2_fate) if isinstance(daughter_2_fate, list) else 1

    if mother_len == 1:
        state_color = "Mother has unique fate"
    elif daughter1_fate is None or daughter_2_fate is None:
        return 'Unknown'
    elif mother_len == daughter1_len == daughter2_len:
        state_color = 'Fate yet to be decided'
    else:
        state_color = 'Either daughter cells will have its fate determined upon division'

    return state_color


def angle_btw_vectors(direction, mean_direction):
    """
    Calculate the angle between a direction vector and the mean direction vector.
    Parameters:
        direction (tuple or list): The direction vector.
        mean_direction (tuple or list): The mean direction vector.
    Returns:
        float: The angle between the two vectors in degrees.
    """
    # Convert direction vectors to numpy arrays
    direction = np.array(direction)
    mean_direction = np.array(mean_direction)

    # Calculate dot product and magnitudes
    dot_product = np.dot(direction, mean_direction)
    magnitude_direction = np.linalg.norm(direction)
    magnitude_mean_direction = np.linalg.norm(mean_direction)

    # Calculate cosine of the angle
    cosine_angle = dot_product / (magnitude_direction * magnitude_mean_direction)

    # Calculate angle in radians
    angle_rad = np.arccos(np.clip(cosine_angle, -1.0, 1.0))

    # Convert angle to degrees
    angle_deg = np.degrees(angle_rad)

    return angle_deg


def align_time(aq_time, time_alignment_coefficients, embryo_name):
    """
    Get time_alignment coefficients and transform aq time to dev time
    Parameters
    ----------
    aq_time
    time_alignment_coefficients
    embryo_name

    Returns
    -------

    """
    a, b = time_alignment_coefficients.get(embryo_name,
                                           (1, 0))
    adjusted_time = a * aq_time + b
    return adjusted_time


def compute_avg_division_time(division_aq_time, time_alignment_coefficients):
    """
    Compute the average adjusted division time for a given cell.
    In terms of developmental time
    """
    # Initialize variables to calculate average time
    total_time = 0
    num_measurements = 0

    # Iterate through division_aq_time dictionary
    for lr, ind_prop in division_aq_time.items():
        for embryo_name, aq_time in ind_prop.items():
            # Get the alignment coefficients for the embryo
            a, b = time_alignment_coefficients.get(embryo_name,
                                                   (1, 0))  # Default to identity transformation if not found

            # Adjust acquisition time using the alignment coefficients
            adjusted_time = a * aq_time + b

            # Add adjusted time to total and increment measurement count
            total_time += adjusted_time
            num_measurements += 1

    # Return average time if there are measurements, otherwise return None
    return total_time / num_measurements if num_measurements > 0 else None


# todo make robust for uni-modal cells, or make new version
def compute_std_t(division_aq_time, time_alignment_coefficients, give_atimes=False):
    """
    Compute the standard deviation of time (std_t) for a given set of division acquisition times.
    Parameters:
        division_aq_time (dict): A dictionary containing division acquisition times for left/right individuals.
        time_alignment_coefficients (dict): A dictionary containing temporal alignment coefficients.
        give_atimes (bool): list of adjusted division times
    Returns:
        float: The standard deviation of time (std_t).
    """
    adjusted_times = []
    for lr, ind_prop in division_aq_time.items():
        for individual_name, value_to_adjusted in ind_prop.items():
            a, b = time_alignment_coefficients.get(individual_name, [1, 0])  # Default to no adjustment if not found
            adjusted_time = a * value_to_adjusted + b
            adjusted_times.append(adjusted_time)
    if len(adjusted_times) > 1:
        if give_atimes == True:
            return np.std(adjusted_times), adjusted_times
        return np.std(adjusted_times)
    else:
        return None


def compute_divisions_mean_direction(divisions_properties):
    """
    Parameters
    ----------
    divisions_properties: dict
        Dictionary containing division properties.

    Returns
    -------
    divisions_properties: dict
        Updated dictionary with the mean direction property added to each division.
    """
    for mother_name, properties in divisions_properties.items():
        list_of_vectors = []

        # Iterate over division_directions_after_symmetry (assuming it's a nested dictionary)
        for lr, directions_dict in properties.get('division_directions_after_symmetry', {}).items():
            for direction in directions_dict.values():
                list_of_vectors.append(direction)

        if list_of_vectors:
            # Calculate mean direction
            mean_dir = mean_direction(list_of_vectors, max_iterations=10, min_angle_threshold=1e-1)

            # Update divisions_properties with mean direction
            divisions_properties[mother_name]['division_mean_direction'] = mean_dir

    return divisions_properties


def compute_division_nb_of_measurements(divisions_properties):
    """

    Parameters
    ----------
    divisions_properties

    Returns
    -------
    divisions_properties with nb of measurements for each division updated

    """
    for mother_name, properties in divisions_properties.items():
        nb_of_mes = 0

        # Iterate over division_directions_after_symmetry (assuming it's a nested dictionary)
        for lr, directions_dict in properties.get('division_directions_after_symmetry', {}).items():
            for direction in directions_dict.values():
                nb_of_mes += 1

        divisions_properties[mother_name]['nb_of_measurements'] = nb_of_mes

    return divisions_properties


def compute_std_from_mean(divisions_properties):
    """
    Parameters
    ----------
    divisions_properties: dict
        Dictionary containing division properties.

    Returns
    -------
    divisions_properties: dict
        Updated dictionary with std from mean direction computed for each division direction.
    """

    for mother_name, properties in divisions_properties.items():
        list_of_angles = []
        mean_dir = properties['division_mean_direction']

        # Iterate over division_directions_after_symmetry (assuming it's a nested dictionary)
        for lr, directions_dict in properties.get('division_directions_after_symmetry', {}).items():
            for direction in directions_dict.values():
                # Calculate the angle between mean vector and direction
                angle = calculate_angle(mean_dir, direction)
                list_of_angles.append(angle)

        if list_of_angles:
            std_from_mean_directions = np.std(list_of_angles)
            # Update divisions_properties with std from mean direction
            divisions_properties[mother_name]['std_from_mean_directions'] = np.degrees(std_from_mean_directions)

    return divisions_properties


def compute_div_registration_dispersion_of_registered_barycenters(divisions_properties):
    """

    Parameters
    ----------
    divisions_properties

    Returns -------
    divisions_properties : with the registration dispersion of the division. It's defined as the
    maximal dispersion between two daughters' registered barycenters. the registration dispersion of barycenters is
    defined as the geometric mean of the axis ellipsoid eng-lobing the registered barycenters

    """

    # Only grab barycenters from individuals that exist in both 'first' and 'last'
    for mother_name, properties in divisions_properties.items():
        # get only registered barycenters properties (after symetry)
        directions_dict = properties.get('division_barycenter_from_global_registration', {})
        for lr, daughters_barycenters_dict in directions_dict.items():
            list_barycenters_1 = []
            list_barycenters_2 = []

            individuals_in_first = set()
            individuals_in_last = set()

            # left and right divisions separately
            # get individuals names
            for fs, individuals_dict in daughters_barycenters_dict.items():
                if fs == 'first':
                    individuals_in_first.update(individuals_dict.keys())
                elif fs == 'second':
                    individuals_in_last.update(individuals_dict.keys())

            # print(individuals_in_first)
            # print(individuals_in_last)

            # get the barycenters
            for fs, individuals_dict in daughters_barycenters_dict.items():
                if fs == 'first':
                    list_barycenters_1.extend(
                        individuals_dict[ind] for ind in individuals_dict if ind in individuals_in_last)
                elif fs == 'second':
                    list_barycenters_2.extend(
                        individuals_dict[ind] for ind in individuals_dict if ind in individuals_in_first)

            # compute dispersion
            if len(list_barycenters_1) <= 1 and len(list_barycenters_2) <= 1:
                # print("Got a single barycenter")
                continue
            if len(list_barycenters_1) <= 1 or len(list_barycenters_2) <= 1:
                if len(list_barycenters_1) <= 1:
                    geom_mean = compute_geom_mean(list_barycenters_2)
                else:
                    geom_mean = compute_geom_mean(list_barycenters_1)
            else:
                geom_mean_1 = compute_geom_mean(list_barycenters_1)
                geom_mean_2 = compute_geom_mean(list_barycenters_2)

                geom_mean = max(geom_mean_1, geom_mean_2)

            # normalize by average distance between daughters barycenters
            average_barycenters_distances = get_division_barycenters_distances(list_barycenters_1,
                                                                               list_barycenters_2)
            avg_bary_distance = cell_truncated_mean(average_barycenters_distances)
            a_geom_mean = geom_mean / avg_bary_distance

            if 'division_daughters_registration_dispersion' not in divisions_properties[mother_name]:
                # If the key is not found, create it with an empty dictionary
                divisions_properties[mother_name]['division_daughters_registration_dispersion'] = {}

            divisions_properties[mother_name]['division_daughters_registration_dispersion'][lr] = a_geom_mean

    return divisions_properties


def get_division_barycenters_distances(list_barycenters_1, list_barycenters_2):
    """
    Calculate distances between corresponding barycenters in two lists.

    Parameters:
    - list_barycenters_1: List of barycenters for the first set of divisions.
    - list_barycenters_2: List of barycenters for the second set of divisions.

    Returns:
    - list_distances: List of distances between corresponding barycenters.
    """
    # Ensure both lists have the same length
    if len(list_barycenters_1) != len(list_barycenters_2):
        raise ValueError("Both lists must have the same length.")

    # Convert lists to NumPy arrays for efficient computation
    array_barycenters_1 = np.array(list_barycenters_1)
    array_barycenters_2 = np.array(list_barycenters_2)

    # Calculate distances using Euclidean distance formula
    list_distances = np.linalg.norm(array_barycenters_1 - array_barycenters_2)
    return list_distances


def cell_truncated_mean(list_cell_volume, percentage=10):
    """
    Compute the truncated mean (moyenne tamisée) of cell volumes using scipy.stats.tmean().

    Parameters:
        list_cell_volume (list): List of cell volumes.
        percentage (float): Percentage of data to keep for computing the truncated mean.

    Returns:
        float: Truncated mean of cell volumes.
    """
    # Check if the percentage is within the valid range
    if not (0 < percentage < 100):
        raise ValueError("Percentage should be between 0 and 100.")

    # Calculate the lower and upper percentile values
    lower_percentile = percentage
    upper_percentile = 100 - percentage
    median_value = np.median(list_cell_volume)

    # print("array values:", list_cell_volume)
    # print("percentiles:", lower_percentile, upper_percentile)

    lower_limit = np.percentile(list_cell_volume, lower_percentile)
    upper_limit = np.percentile(list_cell_volume, upper_percentile)

    # print("range:", median_value-lower_limit, median_value+upper_limit)

    # Compute the truncated mean
    truncated_mean = stats.tmean(list_cell_volume, limits=(median_value - lower_limit, median_value + upper_limit))
    return truncated_mean


def compute_geom_mean(barycenters_list):
    """
    Compute the geometric mean of an array of 3D coordinates.
    the eigenvalues of the covariance matrix is the ellipsoid axes
    Parameters:
        barycenters_list (list or numpy array): Array of 3D coordinates (Nx3).

    Returns:
        numpy array: Geometric mean of the coordinates.
    """
    # Ensure div_barycenters_array is a NumPy array
    div_barycenters_array = np.array(barycenters_list)

    # Compute the covariance matrix
    cov_matrix = np.cov(div_barycenters_array, rowvar=False)

    # Compute eigenvalues of the covariance matrix
    eigenvalues = np.linalg.eigvals(cov_matrix)
    # Compute the geometric mean
    geom_mean = np.cbrt(
        np.sqrt(np.abs(eigenvalues[0])) * np.sqrt(np.abs(eigenvalues[1])) * np.sqrt(np.abs(eigenvalues[2])))

    return geom_mean


def calculate_angle(vector1, vector2):
    """
    Calculate the angle between two vectors.

    Parameters
    ----------
    vector1, vector2: numpy arrays
        Vectors for which the angle needs to be calculated.

    Returns
    -------
    angle: float
        Angle between the two vectors in radians.
    """
    # Ensure vectors are normalized
    vector1 = vector1 / np.linalg.norm(vector1)
    vector2 = vector2 / np.linalg.norm(vector2)

    # Calculate the dot product
    dot_product = np.dot(vector1, vector2)

    # Calculate the angle in radians
    angle = np.arccos(np.clip(dot_product, -1.0, 1.0))

    return angle


#######

def mean_direction(list_of_vectors, max_iterations=10, min_angle_threshold=1e-2):
    """
    Gets list of direcition vectors(which are also coordinates of points on the unit sphere),

    initialize n(the mean vector) as the unitary mean vectorof vectors
    the distance between two points on the sphere surface is the arc=angle in radians, d(ni,uk)=arcos(ni,uk), uk are the vectors from the list, ni is the estimation of n at iteration i
    the plane tangent to the spere at point n is defined as, P(n):
    Projection of uk to P(ni) is the point located at distance d(ni,uk) from ni in the direction uk-(ni.uk)ni (which is the non colinear component of uk to ni):
        pk=ni + d(ni,uk)* uk-(ni.uk)ni / |uk-(ni.uk)ni |
    the barycenter estimation at iteration i+1 q(i+1):
        q(i+1) = ni + 1/K sum(d(ni,uk)* uk-(ni.uk)ni / |uk-(ni.uk)ni |) where K is the total number of vectors
    Then qi+1 coordinates need to be projected from the plan tangent to ni to the sphere
    The next estimation of the average (ie at iteration i + 1) is the point on the sphere that projects onto qi+1.
        The distance d(ni, ni+1)=||q(i+1) - ni|| = || 1/K sum(d(ni,uk)* uk-(ni.uk)ni / |uk-(ni.uk)ni |) ||
        n(i+1) is obtained by rotation of angle alpha_i and axis r_i co_linear to n_ixq(i+1) :
                r_i = n_ixq(i+1)  / ||n_i x q(i+1) ||
        According to Rodrigues's formula:
            n(i+1) = ni + sin(alpha_i)(r_i x ni) + cos(alpha_i)(rix(r_i x ni))  , (r_i x ni): vector cross product
    the iterations stop if number of iterations is too big or the distance (ie l'angle) between 2 successive estimations comes too small

    Returns:
        n: mean vector
    """
    n = np.mean(list_of_vectors, axis=0)
    n = n / np.linalg.norm(n)  # initialize n(the mean vector) as the avg unitary direction
    # print("list of vectors:",list_of_vectors)
    # print("initial n:", n)
    total_vectors = len(list_of_vectors)

    for iteration in range(max_iterations):
        sum_direction = np.zeros(3)

        for vector in list_of_vectors:
            ni = n
            uk = vector
            dot_product = np.clip(np.dot(ni, uk), -1, 1)

            # The distance between ni and uk on the sphere's surface: d(ni,uk)=arcos(ni,uk)
            arc = np.arccos(dot_product)
            # Calculate the non-collinear component of uk to ni
            non_collinear_component = uk - dot_product * ni
            non_collinear_component_norm = np.linalg.norm(non_collinear_component)
            # Calculate pk : Projection of uk to P(ni)
            pk = ni + arc * non_collinear_component / non_collinear_component_norm  # pk=ni + d(ni,uk)* uk-(ni.uk)ni / |uk-(ni.uk)ni |
            sum_direction += pk
            # print("for iteration:",iteration,"dot product:",dot_product,"distance of ni & uk:",arc ,"projection of pts coordinate:",pk)

        # Calculate the new estimation q
        q = sum_direction / total_vectors  # q(i+1) = ni + 1/K sum(d(ni,uk)* uk-(ni.uk)ni / |uk-(ni.uk)ni |)
        # print("Projected coordinate of new estimate:",q,"current estimation:",n)

        # The distance between q and ni
        distance = np.linalg.norm(
            q - n)  # d(ni, ni+1)=||q(i+1) - ni|| = || 1/K sum(d(ni,uk)* uk-(ni.uk)ni / |uk-(ni.uk)ni |) ||

        # Check if the angle has converged
        if distance < min_angle_threshold:
            break

        # Rotate ni to ni+1
        r = np.cross(n, q)
        r_norm = np.linalg.norm(r)

        if r_norm != 0:
            alpha = distance
            n = n + np.sin(alpha) * (r / r_norm) + np.cos(alpha) * np.cross(r, np.cross(r, n))
            # n(i+1) = ni + sin(alpha_i)(r_i x ni) + cos(alpha_i)(rix(r_i x ni))  , (r_i x ni)
            # print("new estimation coordinate on unit sphere:", n)

    #if np.ceil(np.linalg.norm(n)) != 1:
    #    print("not unitary mean:", np.linalg.norm(n))
    #else:
    #    print("done, mean norm:", np.linalg.norm(n))
    n /= np.linalg.norm(n)
    return n

import os
import numpy as np

from ascidian.core.icp import lts_transformation
from ascidian.division_analysis.division_analysis_init_tools import (apply_transformation, transform_to4d_points)


def get_rotation_angle(similitude_matrix):
    # Extract rotation and scaling matrix
    rotation_scaling_matrix = similitude_matrix[:3, :3]

    U, S, Vt = np.linalg.svd(rotation_scaling_matrix)
    rotation_matrix = np.dot(U, Vt)    # may not be a unique solution

    # Ensure the determinant of the rotation matrix is 1
    assert np.isclose(np.linalg.det(rotation_matrix), 1), "Invalid rotation matrix"

    # Compute rotation angle using trace formula
    trace = np.trace(rotation_matrix)
    rotation_angle = np.arccos((trace - 1) / 2)
    rotation_angle_degrees = np.degrees(rotation_angle)

    # Compute rotation axis
    w, v = np.linalg.eig(rotation_matrix)
    rotation_axis = v[:, np.isclose(w, 1)].flatten().real
    rotation_axis /= np.linalg.norm(rotation_axis)

    return rotation_angle_degrees, rotation_axis


def get_rotation_angles_dif(similitude_matrix_l, similitude_matrix_g):
    """
    Gives differences in angle between both similitude rotations.

    Parameters
    ----------
    similitude_matrix_l : array-like
        The first similitude matrix.
    similitude_matrix_g : array-like
        The second similitude matrix.

    Returns
    -------
    angle_difference : float
        The difference in rotation angles in radians.
    """

    def extract_rotation_matrix(similitude_matrix):
        rotation_scaling_matrix = similitude_matrix[:3, :3]
        U, _, Vt = np.linalg.svd(rotation_scaling_matrix)
        rotation_matrix = np.dot(U, Vt)

        # Ensure the determinant of the rotation matrix is 1
        assert np.isclose(np.linalg.det(rotation_matrix), 1), "Invalid rotation matrix"

        return rotation_matrix

    # Extract rotation matrices from the similitude matrices
    rotation_matrix_l = extract_rotation_matrix(similitude_matrix_l)
    rotation_matrix_g = extract_rotation_matrix(similitude_matrix_g)

    # Compute the relative rotation matrix
    relative_rotation_matrix = np.dot(rotation_matrix_l.T, rotation_matrix_g)

    # Compute the angle of rotation from the relative rotation matrix
    angle_difference = np.arccos((np.trace(relative_rotation_matrix) - 1) / 2)

    return np.degrees(angle_difference)

def give_neighbours_ids(pmx_properties, daughter1_id_pmx, daughter2_id_pmx, mode):
    if mode == 'neighbours':
        # get neighboorhoods of daughters of pmx
        neighbouring_ids_pmx = list(pmx_properties['cell_contact_surface'][daughter1_id_pmx].keys())
        neighbouring_ids_pmx.extend(list(pmx_properties['cell_contact_surface'][daughter2_id_pmx].keys()))
        neighbouring_ids_pmx = [x for x in neighbouring_ids_pmx if
                                x not in [daughter1_id_pmx, daughter2_id_pmx]]

        return neighbouring_ids_pmx
    if mode == 'neighbours_neighbours':
        # get neighboorhoods of daughters of pmx
        neighbouring_ids_pmx = list(pmx_properties['cell_contact_surface'][daughter1_id_pmx].keys())
        neighbouring_ids_pmx.extend(list(pmx_properties['cell_contact_surface'][daughter2_id_pmx].keys()))
        additional_ids = []
        for i in neighbouring_ids_pmx:
            if i % 1000 != 1:  # not background
                additional_ids.extend(
                    list(pmx_properties['cell_contact_surface'][i].keys()))
        neighbouring_ids_pmx.extend(additional_ids)
        neighbouring_ids_pmx = [x for x in neighbouring_ids_pmx if
                                x not in [daughter1_id_pmx, daughter2_id_pmx]]

        return neighbouring_ids_pmx
    if mode == 'half_embryo':
        neighbouring_ids_pmx = list(pmx_properties['cell_contact_surface'][daughter1_id_pmx].keys())
        neighbouring_ids_pmx.extend(list(pmx_properties['cell_contact_surface'][daughter2_id_pmx].keys()))
        neighbouring_ids_pmx = [x for x in neighbouring_ids_pmx if x % 1000 != 1]  # Filter out background

        segment_id = int(daughter1_id_pmx // 10000)
        all_ids_in_segment = [k for k in pmx_properties['cell_contact_surface'].keys() if int(k // 10000) == segment_id]
        total_ids = len(all_ids_in_segment)

        half_ids = total_ids // 2
        additional_ids = []
        while len(neighbouring_ids_pmx) < half_ids:
            for i in neighbouring_ids_pmx:
                if i % 1000 != 1:  # not background
                    additional_ids.extend(
                        list(pmx_properties['cell_contact_surface'][i].keys()))
            neighbouring_ids_pmx.extend(additional_ids)
            neighbouring_ids_pmx = [x for x in neighbouring_ids_pmx if
                                    x not in [daughter1_id_pmx, daughter2_id_pmx]]

        return neighbouring_ids_pmx


def get_neighborhood_dicts(cell_id, dict_names, dict_volumes, dict_barycenters, embryo_props):
    """
    Given cell id and properties at t, return dictionnary of cell neighborhood's properties
    Parameters
    ----------
    cell_id : str
        The ID of the cell for which neighborhood properties are to be retrieved.
    dict_names : dict
        A dictionary mapping cell IDs to cell names.
    dict_volumes : dict
        A dictionary mapping cell IDs to cell volumes.
    dict_barycenters : dict
        A dictionary mapping cell IDs to cell barycenters.
    embryo_props : dict
        A dictionary containing properties of the embryo.

    Returns
    -------
    neighborhood_names : dict
        A dictionary mapping neighboring cell IDs to their names.
    neighborhood_volumes : dict
        A dictionary mapping neighboring cell IDs to their volumes.
    neighborhood_barycenters : dict
        A dictionary mapping neighboring cell IDs to their barycenters.
    """

    # Get neighboring cell IDs
    neighbouring_ids = list(embryo_props['cell_contact_surface'][cell_id].keys())
    neighbouring_ids.append(cell_id)
    neighborhood_names_t1 = {id: name for id, name in dict_names.items() if id in neighbouring_ids}
    neighborhood_volumes_t1 = {id: v for id, v in dict_volumes.items() if id in neighbouring_ids}
    neighborhood_barycenters_t1 = {id: bary for id, bary in dict_barycenters.items() if id in neighbouring_ids}

    return neighborhood_names_t1, neighborhood_volumes_t1, neighborhood_barycenters_t1


def local_register(barycenters_t1, cell_name_t1, barycenters_t2, cell_name_t2):
    """

    Parameters
    ----------
    barycenters_t1 (list)
    cell_name_t1 (list)
    barycenters_t2 (list)
    cell_name_t2 (list)
    cell_name (str): the cell we're locally registering
    residues_dict (dict): {'cell name': residue}
    Returns
    -------
    residues_dict (dict): {'cell name': residue}
    """
    # ignore non common cells
    common_cells = set(cell_name_t1).intersection(cell_name_t2)

    # order coord according to cell names lexicographic order
    barycenters_t1_filtered = [barycenters_t1[i] for i, name in enumerate(cell_name_t1) if name in common_cells]
    barycenters_t2_filtered = [barycenters_t2[i] for i, name in enumerate(cell_name_t2) if name in common_cells]
    cell_name_filtered_t1 = [name for name in cell_name_t1 if name in common_cells]
    cell_name_filtered_t2 = [name for name in cell_name_t2 if name in common_cells]
    # Order coordinates according to cell names lexicographic order
    sorted_indices_t1 = np.argsort(cell_name_filtered_t1)
    sorted_indices_t2 = np.argsort(cell_name_filtered_t2)

    sorted_cell_name = [cell_name_filtered_t2[i] for i in sorted_indices_t2]

    sorted_barycenters_t1 = [barycenters_t1_filtered[i] for i in sorted_indices_t1]
    sorted_barycenters_t2 = [barycenters_t2_filtered[i] for i in sorted_indices_t2]

    # Compute similitude, ref and floating have shape (3,N)
    transform, ref, floating = compute_similitude(sorted_barycenters_t1, sorted_barycenters_t2,
                                                  'similitude', 1)
    return transform, ref, floating, sorted_cell_name


def update_residues_dict(sorted_cell_name, cell_name, transformed_floating, ref, residues_dict):
    # store residues
    idx = sorted_cell_name.index(cell_name)
    residue = np.linalg.norm(ref[:, idx] - transformed_floating[:, idx])
    # return residues dict
    residues_dict[cell_name] = residue
    return residues_dict


def compute_similitude(ref, flo, transformation_type='similitude', retained_fraction=0.75):
    """

    Parameters
    ----------
    ref (list) : list of barycenters of 3d coords
    flo (list)
    transformation_type (str): 'rigid' 'similitude' or 'affine'
    Returns
    -------
    transform
    """
    ref = np.array(ref)
    ref = ref.T

    floating = np.array(flo)
    floating = floating.T
    homologous_ref, homologous_floating = transform_to4d_points(ref, floating)
    # retained fraction by default 25%
    # print('registration homologuous coords shape',floating.shape, homologous_floating.shape)
    transform = lts_transformation(homologous_ref, homologous_floating, transformation_type="similitude",
                                   retained_fraction=retained_fraction)
    return transform, ref, floating


def write_registration_residues_script(cell_names, residues, fates, embryo_name, t1, t2, output_file):
    """
    Scatter plot of cell registration residues, color according to fate
    Expecting higher values for endoderm, lower for NP and epidermis
    Parameters
    ----------
    cell_names
    residues
    fates
    output_file

    Returns
    -------

    """
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt

box_colors = {{'Epid.': 'red', 'NS': 'blue', 'Mesoderm': 'green', 'End.': 'purple'}} 
# Cell names and corresponding residues
cell_names = {cell_names}
residues = {residues}
fates = {fates}

n_cell_names = []
n_residues = []
n_fates = []
for name, residue, fate_list in zip(cell_names, residues, fates):
    if isinstance(fate_list, list):
        for fate in fate_list:
            n_cell_names.append(name)
            n_residues.append(residue)
            n_fates.append(fate)
    else:
        n_cell_names.append(name)
        n_residues.append(residue)
        n_fates.append(fate_list)

# Create lists to store sorted data
sorted_cell_names = []
sorted_residues = []
sorted_fates = []

# Sort cell names and residues based on fate
for fate, color in box_colors.items():
    for i, name in enumerate(n_cell_names):
        if n_fates[i] == fate:
            sorted_cell_names.append(name)
            sorted_residues.append(n_residues[i])
            sorted_fates.append(n_fates[i])

legend_handles = []

# Plot boxplot
plt.figure(figsize=(10, 6))

for fate, color in box_colors.items():
    legend_handles.append(plt.scatter([], [], color=color, label=fate))  # Add scatter plot with no data points

for i, (name, residue, fate) in enumerate(zip(sorted_cell_names, sorted_residues, sorted_fates)):
    color = box_colors.get(fate, 'black')
    plt.scatter([i+1], residue, color=color)
plt.xlabel('Cell Name')
plt.ylabel('Global Registration Residue')
plt.title(f'Residues of Global Similitude for Each Cell, embryo :{embryo_name}, times:{t1}&{t2}')
plt.xticks(range(1, len(sorted_cell_names) + 1), sorted_cell_names, rotation=45, ha='right')
plt.legend(handles=legend_handles)
plt.tight_layout()
plt.show()
""")


def compute_residual_dictionary(transformed_floating, ref_vector, sorted_names):
    """
    Calculate the Euclidean distances between corresponding pairs of 3D coordinates in two sets,
    and construct a dictionary where keys are cell names and values are the distances between
    coordinates in the transformed set and coordinates in the reduced set.

    Parameters:
    transformed_floating (list): List of (3,N) arrays of 3D coordinates in the transformed set.
    reduced_cell_name_t2 (list): List of cell names corresponding to the transformed coordinates.
    reduced_barycenters_t1 (list): List of (3,N) arrays of 3D coordinates in the reduced set.

    Returns:
    dict: A dictionary where keys are cell names and values are the distances between corresponding coordinates.
    """
    # Initialize the dictionary
    distance_dict = {}

    # Iterate over the transformed coordinates and calculate distances
    for i in range(len(sorted_names)):
        cell_name_t2 = sorted_names[i]
        coordinate_t2 = transformed_floating[:, i]
        coordinate_t1 = ref_vector[:, i]

        # Calculate Euclidean distance
        distance = np.linalg.norm(coordinate_t2 - coordinate_t1)

        # Associate the distance with the cell name
        distance_dict[cell_name_t2] = distance

    return distance_dict

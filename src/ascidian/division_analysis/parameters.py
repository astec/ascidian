import astec.utils.common as common


class DivisionAnalysisParameters:
    """
    This class will now manage parameters from python file.
    init: generates doc(to be fixed)
          loads parameter from file (returns object: params.param=value)
    """

    def __init__(self, param_file=None):
        if "doc" not in self.__dict__:
            self.doc = {}

        # Initialize all the parameters with default values
        self.atlasFiles = [""]
        self.atlasRefFile = [""]
        self.output_folder = None
        self.delay = 4
        self.local_reg = False
        self.global_reg = True
        self.filter_3_way_junctions = False
        self.stability_of_sisters_approx = False
        self.registration_assessment = False
        self.deviation_btw_sisters_interface = False
        self.deviation_apical_to_div_dir = False
        self.coord_ref_sys_assess = False
        self.daughter_mother_div_dir = False
        self.stability_tissues_surface_of_contact = False
        self.tissue_res = False

        self.geom_barycenters_distance = False
        self.rotations_assessment = False
        self.distance_matrix = False
        self.topological_distance_matrix = False
        self.temporal_geometric_ref = False
        self.common_cells_subspace = False

        self.tissues_nb_of_cells = False

        # Descriptions (docs) for each parameter
        self.doc['atlasFiles'] = "Path to the files of the atlas population"
        self.doc['atlasRefFile'] = "Path to the reference atlas"
        self.doc['output_folder'] = "Folder to register the pickle with registered divisions barycenter/characteristics"
        self.doc['delay'] = "Delay since division time (default: 4) to get the division vector"
        self.doc['local_reg'] = "True if you want local registration"
        self.doc['global_reg'] = "True if you want Global registration (default True)"
        self.doc['filter_3_way_junctions'] = "Filter 3-way junctions between sisters"
        self.doc['stability_of_sisters_approx'] = ("Check stability of your axis (barycenters or interface "
                                                   "between sisters)")
        self.doc['registration_assessment'] = "Compare local and global registration (rotation angles and residues)"
        self.doc['deviation_btw_sisters_interface'] = ("Check deviation between the two approximations of the plane "
                                                       "normal to the sisters")
        self.doc['deviation_apical_to_div_dir'] = "Test geometrical relation to division direction"
        self.doc['coord_ref_sys_assess'] = "Test stability of coordinate reference system"
        self.doc['daughter_mother_div_dir'] = "Compare division direction with the mother's division"
        self.doc['stability_tissues_surface_of_contact'] = "Compare stability of tissue contact surface"
        self.doc['tissue_res'] = "Which tissue types resolution to use, False for germ line"

        self.doc['rotations_assessment'] = "True if rotations assessment should be computed."
        self.doc['geom_barycenters_distance'] = "True if geometrical barycenters distance calculation is needed."
        self.doc['distance_matrix'] = "True if embryos distance matrix calculation is needed."
        self.doc['topological_distance_matrix'] = "True if embryos topological distance matrix calculation is needed."
        self.doc['temporal_geometric_ref'] = "True if temporal geometric reference should be computed."
        self.doc['common_cells_subspace'] = "True if the common cells subspace function should be executed."
        self.doc['tissues_nb_of_cells'] = ("if True, plots for each tissue type/fate nb of cells at each dev time "
                                           "in each individuals")

        if param_file:
            self.load_parameters_from_file(param_file)

    def print_parameters(self):
        """
        Print out all parameters for verification
        """
        print("Experiment Parameters:")
        for param, value in vars(self).items():
            if param != "doc":
                print(f"{param}: {value}")

    def load_parameters_from_file(self, param_file):
        """
        Dynamically load parameters from the given Python file.
        """
        parameters = {}
        with open(param_file, 'r') as file:
            exec(file.read(), {}, parameters)

        # parameters = common.load_source(param_file) # dunno what it does

        self.update_from_parameters(parameters)

    def update_from_parameters(self, parameters):
        """
        Update class attributes based on the loaded parameters
        """
        self.atlasFiles = parameters.get('atlasFiles', self.atlasFiles)
        self.atlasRefFile = parameters.get('atlasRefFile', self.atlasRefFile)
        self.output_folder = parameters.get('output_folder', self.output_folder)
        self.delay = parameters.get('delay', self.delay)
        self.local_reg = parameters.get('local_reg', self.local_reg)
        self.filter_3_way_junctions = parameters.get('filter_3_way_junctions', self.filter_3_way_junctions)
        self.stability_of_sisters_approx = parameters.get('stability_of_sisters_approx',
                                                          self.stability_of_sisters_approx)
        self.registration_assessment = parameters.get('registration_assessment', self.registration_assessment)
        self.deviation_btw_sisters_interface = parameters.get('deviation_btw_sisters_interface',
                                                              self.deviation_btw_sisters_interface)
        self.deviation_apical_to_div_dir = parameters.get('deviation_apical_to_div_dir',
                                                          self.deviation_apical_to_div_dir)
        self.coord_ref_sys_assess = parameters.get('coord_ref_sys_assess', self.coord_ref_sys_assess)
        self.daughter_mother_div_dir = parameters.get('daughter_mother_div_dir', self.daughter_mother_div_dir)
        self.stability_tissues_surface_of_contact = parameters.get('stability_tissues_surface_of_contact',
                                                                   self.stability_tissues_surface_of_contact)
        self.tissue_res = parameters.get('tissue_res', self.tissue_res)

        self.registration_assessment = parameters.get('registration_assessment', self.registration_assessment)
        self.rotations_assessment = parameters.get('rotations_assessment', self.rotations_assessment)
        self.geom_barycenters_distance = parameters.get('geom_barycenters_distance', self.geom_barycenters_distance)
        self.distance_matrix = parameters.get('distance_matrix', self.distance_matrix)
        self.topological_distance_matrix = parameters.get('topological_distance_matrix',
                                                          self.topological_distance_matrix)
        self.temporal_geometric_ref = parameters.get('temporal_geometric_ref', self.temporal_geometric_ref)
        self.common_cells_subspace = parameters.get('common_cells_subspace', self.common_cells_subspace)
        self.tissues_nb_of_cells = parameters.get('tissues_nb_of_cells', self.tissues_nb_of_cells)


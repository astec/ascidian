# Has functions to : detect a division, grab a division, grab an embryo at t, get mean directio
# Get property for cell/ fate , get cells mother, return to most common ancestry by pairs
# register two frames of embryos by similitude, compare division in two embryos

# Functions:
#   get_first_last_timestamps
#   get_embryo_at_t
#   get_divided_cells_ids_at_t
#   filter_cells_with_short_life_daughters
#   get_cells_name
#   get_cell_ids
#   get_cell_id_at_div
#   get_daughters_ids_at_tn
#   get_cell_name_parts
#   merge_daughters
#   reduce_to_closest_ancestry
#   look_for_daughters
#   merge_sister_cells
#   nb_of_cells
#   register_frames
#   transform_to4d_points
#   get_vectors_ordered
#   estimate_sym_vector
#   barycenter_after_symmetry
#   post_process_reduction
#   compare_divisions_in_embryo_and_reference

import numpy as np
from sklearn.linear_model import RANSACRegressor

from ascidian.core.icp import lts_transformation
import ascidian.ascidian.name as uname


def volume_fitting_and_voxelsize_correction(pm_volume_prop, target_volume=60000000):
    """
    Fitting parameters of embryo volume overtime and calculate voxel size correction
    to simulate an embryo of a given target volume.

    to be multiplied by its square for surfaces, cube for volumes adjustments

    Parameters
    ----------
    pm_volume_prop : dict
        Dictionary where keys are cell IDs and values are their corresponding volumes.
    target_volume : int, optional
        The desired target volume to scale to, default is 60,000,000.

    Returns
    -------
    scaling_factors: dict of float
        Embryo scaling factoras at each timepoint. {'t': scaling_factor}
    """
    # Step 1: Fit the volume data over time
    volume_along_time = {}

    scaling_factors = {}

    # Compute embryo volume for each timepoint 't'
    for c in pm_volume_prop:
        t = int(c) // 10000
        volume_along_time[t] = volume_along_time.get(t, 0) + pm_volume_prop[c]

    # Get array of time points (x) and array of volumes (y)
    x = sorted(volume_along_time.keys())
    y = [volume_along_time[i] for i in x]

    # Robust regression via RANSAC
    xnp = np.array(x)[:, np.newaxis]
    ynp = np.array(y)[:, np.newaxis]
    ransac = RANSACRegressor()
    ransac.fit(xnp, ynp)

    # Extracting coefficients
    (a, b) = (ransac.estimator_.coef_[0][0], ransac.estimator_.intercept_[0])

    # Step 2: Calculate the voxel size correction
    for timepoint in x:
        t_volume = a * timepoint + b
        scaling_factors[timepoint] = np.cbrt(target_volume / t_volume)
    return scaling_factors


def get_division_mother_lifetime(dict_lineage, mother_name, dict_names):
    """
    Computes the lifetimes of a mother cell and its daughters based on lineage and name dictionaries.

    Parameters
    ----------
    dict_lineage : dict
    mother_name : str
        The name of the mother cell.
    dict_names : dict

    Returns
    -------
    lifetime
        mother lifetime duration in terms of acquisition time, None if we can't compute the lifetime of this cell
    """

    def get_first_id(cell_name, dict_names, dict_lineage):
        for cell_id, name in dict_names.items():
            if name == cell_name:
                # Check if cell_id is a daughter in any lineage
                if any(isinstance(v, list) and len(v) == 2 and cell_id in v for v in dict_lineage.values()):
                    return cell_id
        return None

    def get_lifetime(cell_id, dict_lineage):
        first_appearance = cell_id
        current_id = cell_id
        while current_id in dict_lineage and not (
                isinstance(dict_lineage[current_id], list) and len(dict_lineage[current_id]) == 2):
            current_id = dict_lineage[current_id][0]
        last_appearance = current_id if current_id in dict_lineage else None
        if last_appearance is None:
            return None
        return int(last_appearance // 10000) - int(first_appearance // 10000)

    # Get the first ID of the mother
    mother_id = get_first_id(mother_name, dict_names, dict_lineage)
    if mother_id is None:
        return None
    # Get the mother's lifetime
    mother_lifetime = get_lifetime(mother_id, dict_lineage)

    return mother_lifetime


def get_divisions_acquisition_times(embryo_properties, reference_properties, divisions_properties):
    """
    For each division in reference, store the acquisition time of each matching cell found in the other embryo
    You'll be filling divisions_properties['mother']['all_aq_times']['Left or right'][individual_name] = acquisition_time
    Parameters
    ----------
    divisions_properties

    Returns
    -------

    """
    # Get embryos names, times, different properties
    ref_name, ref_properties = next(iter(reference_properties.items()))
    pmx_name, pmx_properties = next(iter(embryo_properties.items()))
    dict_barycenter_pm1 = ref_properties['cell_barycenter']
    first_pm1_time, last_pm1_time = get_first_last_timestamps(dict_barycenter_pm1)

    # for each time in ref:
    for t in range(first_pm1_time, last_pm1_time):
        # get divided cells ids at t, filter(delay after division), get cells names
        dict_cell_lineage_ref_at_t = get_embryo_at_t(ref_properties['cell_lineage'], t)
        list_of_divided_cells_ids_at_t = get_divided_cells_ids_at_t(dict_cell_lineage_ref_at_t)

        #############################
        # add to properties all cells division acquisition times without delays
        list_ids_pm1_at_aq = list_of_divided_cells_ids_at_t
        list_names_pm1_at_aq = get_cells_name(ref_properties['cell_name'],
                                              list_ids_pm1_at_aq)
        # fill cells in dictionary
        # get mother_name and direction for each id in pm1
        for name, cell_id in zip(list_names_pm1_at_aq, list_ids_pm1_at_aq):
            if name[:-1] not in list(divisions_properties.keys()):
                continue
            # get mother name unless its 'Unknown'
            if name != 'Unknown':
                mother_name = uname.get_mother_name(name)
                if mother_name[-1] == '*':
                    direction = 'right'
                else:
                    direction = 'left'
                # fill in:

                divisions_properties[name[:-1]]['all_div_aq_times'][direction][ref_name] = int(cell_id / 10000)

        # get div ids in pmx at div without delay (get names in pm1 before, use names to get ids)
        # get ids in pmx at division
        list_ids_pmx_at_aq, list_names_pmx_at_aq = get_cell_ids_at_div(pmx_properties['cell_name'],
                                                                       pmx_properties['cell_lineage'],
                                                                       list_names_pm1_at_aq)
        # fill in dict
        if not list_ids_pmx_at_aq:
            continue
        if not list_names_pmx_at_aq:
            continue
        for name, cell_id in zip(list_names_pmx_at_aq, list_ids_pmx_at_aq):
            if name[:-1] not in divisions_properties:
                continue
            # get mother name unless its 'Unknown'
            if name != 'Unknown':
                mother_name = uname.get_mother_name(name)
                if mother_name[-1] == '*':
                    direction = 'right'
                else:
                    direction = 'left'
                # fill in:
                divisions_properties[name[:-1]]['all_div_aq_times'][direction][pmx_name] = int(cell_id / 10000)


def get_cell_ids_at_div(dict_cell_names, dict_cell_lineage, list_cell_names):
    """
    Should work for other property dictionaries
    Given dict_cell_names={'cell_id': cell_name'} and a list of cell names,
    returns a list of corresponding cell IDs. For each cell name, consider the first ID (or first key).
    """
    cell_ids = []
    cell_names = []
    if not list_cell_names:
        return cell_ids, cell_names
    for cell_name in list_cell_names:
        if cell_name not in list(dict_cell_names.values()):
            # print("Name not found in embryo:", cell_name)
            continue
        for cell_id, name in dict_cell_names.items():
            if name == cell_name:
                # Check if the cell ID is in lineage and if its lineage value is a list with two elements
                if cell_id in dict_cell_lineage and isinstance(dict_cell_lineage[cell_id], list) and len(
                        dict_cell_lineage[cell_id]) == 2:
                    cell_ids.append(cell_id)
                    cell_names.append(name)
                    break  # Stop searching after the first match

    return cell_ids, cell_names


def compare_divisions_in_embryo_and_reference(embryo_properties, reference_properties, divisions_properties, delay,
                                              direction_type='interface'):
    # direction_type can be 'bary' or 'interface'

    # Get embryos names, times, different properties
    ref_name, ref_properties = next(iter(reference_properties.items()))
    pmx_name, pmx_properties = next(iter(embryo_properties.items()))
    dict_barycenter_pm1 = ref_properties['cell_barycenter']
    first_pm1_time, last_pm1_time = get_first_last_timestamps(dict_barycenter_pm1)

    volume_adjustement_factors_ref = volume_fitting_and_voxelsize_correction(ref_properties['cell_volume'],
                                                                             target_volume=60000000)
    volume_adjustement_factors_pmx = volume_fitting_and_voxelsize_correction(pmx_properties['cell_volume'],
                                                                             target_volume=60000000)

    print('In embryo :', pmx_name)

    # for each time in ref:
    for t in range(first_pm1_time, last_pm1_time):
        # get divided cells ids at t, filter(delay after division), get cells names
        dict_cell_lineage_ref_at_t = get_embryo_at_t(ref_properties['cell_lineage'], t)
        list_of_divided_cells_ids_at_t = get_divided_cells_ids_at_t(dict_cell_lineage_ref_at_t)

        # filter cells with short life daughters
        list_of_divided_cells_ids_at_t_pm1 = filter_cells_with_short_life_daughters(ref_properties['cell_lineage'],
                                                                                    list_of_divided_cells_ids_at_t,
                                                                                    delay)
        # get list of the cell's names
        list_of_divided_cells_names_at_t_pm1 = get_cells_name(ref_properties['cell_name'],
                                                              list_of_divided_cells_ids_at_t_pm1)

        #  only consider right cells from: list_of_divided_cells_names_at_t_pm1,list_of_divided_cells_ids_at_t_pm1
        list_of_divided_cells_ids_at_t_pm1 = [cell_id for cell_id, cell_name in
                                              zip(list_of_divided_cells_ids_at_t_pm1,
                                                  list_of_divided_cells_names_at_t_pm1) if
                                              cell_name.endswith('*')]
        list_of_divided_cells_names_at_t_pm1 = [cell_name for cell_name in list_of_divided_cells_names_at_t_pm1 if
                                                cell_name.endswith('*')]

        # get cell ids(any) in the other embryo
        # take left cells and right cells :
        if pmx_name == ref_name:
            (list_of_cell_divided_ids_pmx, list_of_cell_divided_names_pmx,
             list_of_divided_cells_ids_at_t_pm1, list_of_divided_cells_names_at_t_pm1) = (
                get_dividing_names_ids_(pmx_properties['cell_name'], list_of_divided_cells_names_at_t_pm1,
                                        list_of_divided_cells_ids_at_t_pm1, ['_']))
        else:
            (list_of_cell_divided_ids_pmx, list_of_cell_divided_names_pmx,
             list_of_divided_cells_ids_at_t_pm1, list_of_divided_cells_names_at_t_pm1) = (
                get_dividing_names_ids_(pmx_properties['cell_name'], list_of_divided_cells_names_at_t_pm1,
                                        list_of_divided_cells_ids_at_t_pm1, ['_', '*']))

        ##
        # Iterate over divided cell pairs
        ##
        for divided_cell_id_pmx, divided_cell_id_pm1 in zip(list_of_cell_divided_ids_pmx,
                                                            list_of_divided_cells_ids_at_t_pm1):

            # Make divided_cell_id_pmx: divided_cell_id_pmx at division time
            divided_cell_id_pmx = get_cell_id_at_div(pmx_properties['cell_lineage'], divided_cell_id_pmx)

            if divided_cell_id_pmx is None:
                # Lineage of cell in pmx not continuous
                continue

            # Get embryos at delayed time (t + delay)
            # ref
            dict_barycenters_pm1_at_t_delayed = get_embryo_at_t(ref_properties['cell_barycenter'], t + delay)
            dict_names_pm1_at_t_delayed = get_embryo_at_t(ref_properties['cell_name'], t + delay)
            # pmx
            division_time_pmx = int(divided_cell_id_pmx / 10000)
            dict_barycenters_pmx_at_t_delayed = get_embryo_at_t(pmx_properties['cell_barycenter'],
                                                                division_time_pmx + delay)
            dict_names_pmx_at_t_delayed = get_embryo_at_t(pmx_properties['cell_name'], division_time_pmx + delay)

            # Get daughters' IDs at t + delay
            daughter1_id_pmx, daughter2_id_pmx = get_daughters_ids_at_tn(
                pmx_properties['cell_lineage'], divided_cell_id_pmx, delay
            )

            daughter1_id_pm1, daughter2_id_pm1 = get_daughters_ids_at_tn(
                ref_properties['cell_lineage'], divided_cell_id_pm1, delay
            )

            if daughter1_id_pmx is not None and daughter2_id_pmx is not None and daughter1_id_pm1 is not None and daughter2_id_pm1 is not None:

                if isinstance(daughter1_id_pmx, list):
                    daughter1_id_pmx = int(daughter1_id_pmx[0])
                if isinstance(daughter2_id_pmx, list):
                    daughter2_id_pmx = int(daughter2_id_pmx[0])
                if isinstance(daughter1_id_pm1, list):
                    daughter1_id_pm1 = int(daughter1_id_pm1[0])
                if isinstance(daughter2_id_pm1, list):
                    daughter2_id_pm1 = int(daughter2_id_pm1[0])

                # Get names of daughters at t + delay
                name_daughter_pmx_1 = dict_names_pmx_at_t_delayed.get(daughter1_id_pmx)
                name_daughter_pmx_2 = dict_names_pmx_at_t_delayed.get(daughter2_id_pmx)

                name_daughter_pm1_1 = dict_names_pm1_at_t_delayed.get(daughter1_id_pm1)
                name_daughter_pm1_2 = dict_names_pm1_at_t_delayed.get(daughter2_id_pm1)

                # Check if names of daughters in both embryos are the same
                if (name_daughter_pmx_1 and name_daughter_pmx_2 and name_daughter_pm1_1 and name_daughter_pm1_2 and
                        ((
                                 name_daughter_pmx_1[:-1] == name_daughter_pm1_1[:-1] and
                                 name_daughter_pmx_2[:-1] == name_daughter_pm1_2[:-1]) or
                         (
                                 name_daughter_pmx_1[:-1] == name_daughter_pm1_2[:-1]
                                 and name_daughter_pmx_2[:-1] == name_daughter_pm1_1[:-1]))):

                    # Daughter names match in both embryos
                    # Process the matching daughters
                    # print('embryo:', pmx_name)
                    # print('cell id in pmx and pm1', daughter1_id_pmx, daughter1_id_pm1)
                    # print('name of daughter is:', name_daughter_pmx_1)
                    # print('name of daughter is:', name_daughter_pm1_1)
                    # print(daughter1_id_pmx, daughter2_id_pmx, name_daughter_pm1_1, name_daughter_pm1_2)
                    _, _, q1, _ = get_cell_name_parts(name_daughter_pmx_1)

                    if q1 % 2 == 0:
                        # Swap the names and IDs
                        name_daughter_pmx_1, name_daughter_pmx_2 = name_daughter_pmx_2, name_daughter_pmx_1
                        daughter1_id_pmx, daughter2_id_pmx = daughter2_id_pmx, daughter1_id_pmx

                    _, gen, q1, _ = get_cell_name_parts(name_daughter_pm1_1)

                    if q1 % 2 == 0:
                        # Swap the names and IDs
                        name_daughter_pm1_1, name_daughter_pm1_2 = name_daughter_pm1_2, name_daughter_pm1_1
                        daughter1_id_pm1, daughter2_id_pm1 = daughter2_id_pm1, daughter1_id_pm1
                    ##
                    ###
                    #
                    # return to most common ancestry
                    reduced_barycenters_pm1, reduced_barycenters_pmx, \
                        reduced_cell_name_pm1, reduced_cell_name_pmx = reduce_to_closest_ancestry(
                        dict_barycenters_pm1_at_t_delayed, dict_barycenters_pmx_at_t_delayed,
                        ref_properties['cell_volume'], pmx_properties['cell_volume'],
                        dict_names_pm1_at_t_delayed, dict_names_pmx_at_t_delayed
                    )

                    # Check the number of cells in each frame before and after reduction
                    nb_cells_pm1_before = len(dict_barycenters_pm1_at_t_delayed)
                    nb_cells_pmx_before = len(dict_barycenters_pmx_at_t_delayed)
                    nb_cells_pm1_after = nb_of_cells(reduced_barycenters_pm1)
                    nb_cells_pmx_after = nb_of_cells(reduced_barycenters_pmx)

                    if nb_cells_pm1_after == nb_cells_pmx_after:
                        # Global registration
                        transform, floating, ref_pts, _ = global_similitude(reduced_barycenters_pm1,
                                                                            reduced_cell_name_pm1,
                                                                            reduced_barycenters_pmx,
                                                                            reduced_cell_name_pmx,
                                                                            ref_name=ref_name,
                                                                            registered_ind=pmx_name,
                                                                            t_in_ref=t + delay,
                                                                            t_in_ind=division_time_pmx + delay)

                        transformed_floating = apply_transformation(transform, floating)

                        # compute avg robust residues
                        residues = robust_average_l2_distance(ref_pts, transformed_floating)

                        # register barycenters for pmx
                        registered_barycenters_pmx_at_t4 = register_barycenters(transform,
                                                                                dict_barycenters_pmx_at_t_delayed)

                        barycenter_daughter1_pmx = registered_barycenters_pmx_at_t4[daughter1_id_pmx]
                        barycenter_daughter2_pmx = registered_barycenters_pmx_at_t4[daughter2_id_pmx]

                        # barycenters in ref (right)
                        barycenter_daughter1_pm1 = dict_barycenters_pm1_at_t_delayed[daughter1_id_pm1]
                        barycenter_daughter2_pm1 = dict_barycenters_pm1_at_t_delayed[daughter2_id_pm1]

                        #######
                        #########
                        #########
                        ####
                        # get pm1 at barycenter
                        pm1_barycenter = estimate_embryo_barycenter(reduced_barycenters_pm1,
                                                                    reduced_cell_name_pm1,
                                                                    ref_properties['cell_volume'])
                        # get pmx at t registered barycenter
                        pmx_barycenter = estimate_embryo_barycenter(registered_barycenters_pmx_at_t4,
                                                                    dict_names_pmx_at_t_delayed,
                                                                    pmx_properties['cell_volume'])

                        # get registered barycenter without scaling
                        # compute for pm1 and pmx the scaling factor at t
                        scale = volume_adjustement_factors_ref[int(daughter1_id_pm1 / 10000)]
                        scale_ref = scale
                        pm1_barycenter = pm1_barycenter * scale
                        bary_d1_pm1_scaled = barycenter_daughter1_pm1 * scale
                        bary_d2_pm1_scaled = barycenter_daughter2_pm1 * scale
                        bary_m_pm1_scaled = ref_properties['cell_barycenter'][divided_cell_id_pm1] * scale

                        mother_volume_ref = ref_properties['cell_volume'][divided_cell_id_pm1] * scale * scale * scale
                        mother_surface_ref = ref_properties['cell_surface'][divided_cell_id_pm1] * scale * scale

                        scale = volume_adjustement_factors_pmx[int(daughter1_id_pmx / 10000)]
                        scale_pmx = scale
                        pmx_barycenter = pmx_barycenter * scale
                        bary_d1_pmx_scaled = rescale_barycenter(barycenter_daughter1_pmx, transform, scale)
                        bary_d2_pmx_scaled = rescale_barycenter(barycenter_daughter2_pmx, transform, scale)

                        bary_m_pmx_scaled = rescale_barycenter(pmx_properties['cell_barycenter'][divided_cell_id_pmx],
                                                               transform, scale)

                        # compute cylindrical coords
                        sym_vector_ref = estimate_sym_vector(reduced_barycenters_pm1,
                                                             reduced_cell_name_pm1,
                                                             ref_properties['cell_volume'])
                        sym_vector_pmx = estimate_sym_vector(registered_barycenters_pmx_at_t4,
                                                             dict_names_pmx_at_t_delayed,
                                                             pmx_properties['cell_volume'])

                        r_pmx, z_pmx = compute_cell_coordinates_relative_to_symmetry(pmx_barycenter, sym_vector_pmx,
                                                                                     bary_m_pmx_scaled)

                        r_ref, z_ref = compute_cell_coordinates_relative_to_symmetry(pm1_barycenter, sym_vector_ref,
                                                                                     bary_m_pm1_scaled)

                        # cells volume and surface scaled
                        mother_volume_pmx = pmx_properties['cell_volume'][divided_cell_id_pmx] * scale * scale * scale
                        mother_surface_pmx = pmx_properties['cell_surface'][divided_cell_id_pmx] * scale * scale
                        #############

                        # Update divisions info
                        # Fill divisions_properties

                        # get mother name
                        mother_name = uname.get_mother_name(name_daughter_pmx_1)
                        cell_fate = pmx_properties['cell_fate'][divided_cell_id_pmx]
                        # division_time_pmx div time in pm1: t

                        # cell radius : mass is equal to volume (supposing cell is spherical), adjust radius computation
                        radius_pmx = np.cbrt(3 * pmx_properties['cell_volume'][divided_cell_id_pmx] / (4 * np.pi))
                        radius_pm1 = np.cbrt(3 * ref_properties['cell_volume'][divided_cell_id_pm1] / (4 * np.pi))

                        # normalised division direction
                        division_direction_pmx = barycenter_daughter2_pmx - barycenter_daughter1_pmx
                        normalized_division_direction_pmx = division_direction_pmx / np.linalg.norm(
                            division_direction_pmx)
                        division_direction_pm1 = barycenter_daughter2_pm1 - barycenter_daughter1_pm1
                        normalized_division_direction_pm1 = division_direction_pm1 / np.linalg.norm(
                            division_direction_pm1)
                        # if left cell apply symetry transform
                        is_left = mother_name[-1] == '_'

                        if mother_name[:-1] not in divisions_properties:
                            divisions_properties[mother_name[:-1]] = {
                                'division_directions_after_symmetry': {'left': {}, 'right': {}},
                                'division_directions_before_registration': {'left': {}, 'right': {}},
                                'division_barycenter_from_global_registration': {'left': {'first': {}, 'second': {}},
                                                                                 'right': {'first': {}, 'second': {}}
                                                                                 },


                                'division_volume_ratio': {'left': {}, 'right': {}},
                                'division_aq_time': {'left': {}, 'right': {}},
                                'all_div_aq_times': {'left': {}, 'right': {}},
                                'cell_radius': {'left': {}, 'right': {}},
                                'embryo_symmetry_plane_at_t': {},  # for each individual
                                'cell_fate': None,
                                'ids_in_ref': {},
                                'gen': None,
                                'daughters_fate': {'first_daughter': [], 'second_daughter': []},
                                # daughter1_id_pmx, daughter2_id_pmx

                                # ids in each individual
                                'cell_ids': {'left': {}, 'right': {}},
                                #  divided_cell_id_pmx, divided_cell_id_pm1

                                # division neighbors data: extracted from cell_contact_surface property
                                'neighbors': {'left': {}, 'right': {}},

                                # get cell_surface too
                                'cell_surface': {'left': {}, 'right': {}},

                                # cell principal values
                                'cell_principal_values': {'left': {}, 'right': {}},
                                # cell principal vectors
                                'cell_principal_vectors': {'left': {}, 'right': {}},

                                # sisters interfaces
                                'sisters_interface_vectors': {'left': {}, 'right': {}},
                                'sisters_surface_principal_values': {'left': {}, 'right': {}},

                                'apical_surface_principal_vectors': {'left': {}, 'right': {}},
                                'apical_surface_principal_values': {'left': {}, 'right': {}},

                                'sisters_interface_edges_vectors': {'left': {}, 'right': {}},
                                'sisters_surface_edges_principal_values': {'left': {}, 'right': {}},

                                'apical_surface_edges_principal_vectors': {'left': {}, 'right': {}},
                                'apical_surface_edges_principal_values': {'left': {}, 'right': {}},

                                'division_mothers_lifetimes': {'left': {}, 'right': {}},

                                'cylindrical_coords': {'left': {}, 'right': {}},

                                'cell_volume_scaled': {'left': {}, 'right': {}},
                                'cell_surface_scaled': {'left': {}, 'right': {}},

                                'residues': {'left': {}, 'right': {}},  # in ref store -1 value,
                                # for registered ind put avg residues value
                                # in ref value=-1

                            }
                        # fill data
                        #########
                        ####
                        #########

                        # compute mother lifetime in ref and flo
                        ref_lifetime = get_division_mother_lifetime(ref_properties['cell_lineage'], mother_name,
                                                                    ref_properties['cell_name'])
                        flo_lifetime = get_division_mother_lifetime(pmx_properties['cell_lineage'], mother_name,
                                                                    pmx_properties['cell_name'])
                        # print('lifetimes', ref_lifetime, flo_lifetime, 'for', mother_name, is_left)

                        # add vectors : interface between sisters, apical longest axis

                        sisters_interface_vectors_pmx = pmx_properties.get('sisters_interface_vectors', {}).get(
                            daughter1_id_pmx, None)
                        sisters_surface_principal_values_pmx = pmx_properties.get('sisters_surface_principal_values',
                                                                                  {}).get(daughter1_id_pmx, None)
                        sisters_interface_edges_vectors_pmx = pmx_properties.get('sisters_interface_edges_vectors',
                                                                                 {}).get(daughter1_id_pmx, None)
                        sisters_surface_edges_principal_values_pmx = pmx_properties.get(
                            'sisters_surface_edges_principal_values', {}).get(daughter1_id_pmx, None)

                        apical_surface_principal_vectors_pmx = pmx_properties.get('apical_surface_principal_vectors',
                                                                                  {}).get(divided_cell_id_pmx, None)
                        apical_surface_principal_values_pmx = pmx_properties.get('apical_surface_principal_values',
                                                                                 {}).get(divided_cell_id_pmx, None)
                        apical_surface_edges_principal_vectors_pmx = pmx_properties.get(
                            'apical_surface_edges_principal_vectors', {}).get(divided_cell_id_pmx, None)
                        apical_surface_edges_principal_values_pmx = pmx_properties.get(
                            'apical_surface_edges_principal_values', {}).get(divided_cell_id_pmx, None)

                        sisters_interface_vectors_ref = ref_properties.get('sisters_interface_vectors', {}).get(
                            daughter1_id_pm1, None)
                        sisters_surface_principal_values_ref = ref_properties.get('sisters_surface_principal_values',
                                                                                  {}).get(daughter1_id_pm1, None)
                        sisters_interface_edges_vectors_ref = ref_properties.get('sisters_interface_edges_vectors',
                                                                                 {}).get(daughter1_id_pm1, None)
                        sisters_surface_edges_principal_values_ref = ref_properties.get(
                            'sisters_surface_edges_principal_values', {}).get(daughter1_id_pm1, None)

                        apical_surface_principal_vectors_ref = ref_properties.get('apical_surface_principal_vectors',
                                                                                  {}).get(divided_cell_id_pm1, None)
                        apical_surface_principal_values_ref = ref_properties.get('apical_surface_principal_values',
                                                                                 {}).get(divided_cell_id_pm1, None)
                        apical_surface_edges_principal_vectors_ref = ref_properties.get(
                            'apical_surface_edges_principal_vectors', {}).get(divided_cell_id_pm1, None)
                        apical_surface_edges_principal_values_ref = ref_properties.get(
                            'apical_surface_edges_principal_values', {}).get(divided_cell_id_pm1, None)

                        ###########
                        ####
                        ###########

                        # register cell pca vectors in pmx, shape needs to be (3,N)
                        registered_pcas = np.array(pmx_properties['cell_principal_vectors'][divided_cell_id_pmx])
                        registered_pcas = registered_pcas.T  # reshape to have 3,N shape
                        # apply registration
                        registered_pcas = apply_transformation(transform, registered_pcas, translate=False)
                        registered_pcas = registered_pcas.T  # shape to N,3

                        # add fates
                        divisions_properties[mother_name[:-1]]['daughters_fate']['first_daughter'] = \
                            pmx_properties['cell_fate'][daughter1_id_pmx]
                        divisions_properties[mother_name[:-1]]['daughters_fate']['second_daughter'] = \
                            pmx_properties['cell_fate'][daughter2_id_pmx]

                        divisions_properties[mother_name[:-1]]['cell_fate'] = cell_fate
                        divisions_properties[mother_name[:-1]]['gen'] = gen - 1

                        # following has dict :{neighbor_id: contact_surface} => {neighbor_name: contact_surface}
                        cell_neighbor_dict_in_pmx = pmx_properties['cell_contact_surface'][divided_cell_id_pmx]
                        cell_neighbor_dict_in_ref = ref_properties['cell_contact_surface'][divided_cell_id_pm1]

                        #########################################

                        neighbor_name_surface_ref = {}
                        neighbor_name_surface_pmx = {}

                        cell_surface_ref = ref_properties['cell_surface'][divided_cell_id_pm1]
                        # Iterate over the neighbor IDs and contact surfaces in the ref/pmx properties
                        for neighbor_id, contact_surface in cell_neighbor_dict_in_ref.items():
                            if contact_surface >= cell_surface_ref * 5 / 100:
                                neighbor_name = ref_properties['cell_name'].get(neighbor_id,
                                                                                f"Background")
                                neighbor_name_surface_ref[neighbor_name] = contact_surface * scale_ref * scale_ref

                        cell_surface_pmx = pmx_properties['cell_surface'][divided_cell_id_pmx]
                        for neighbor_id, contact_surface in cell_neighbor_dict_in_pmx.items():
                            if contact_surface >= cell_surface_pmx * 5 / 100:
                                neighbor_name = pmx_properties['cell_name'].get(neighbor_id,
                                                                                f"Background")
                                neighbor_name_surface_pmx[neighbor_name] = contact_surface * scale_pmx * scale_pmx

                        # store right or left pmx data
                        if is_left:
                            #
                            ####
                            ### add if and adapt to dir type
                            # get axis of symmetry estimation at frame

                            sym_vector_pmx = estimate_sym_vector(registered_barycenters_pmx_at_t4,
                                                                 reduced_cell_name_pmx,
                                                                 pmx_properties['cell_volume'])

                            # Do the symmetry transform of left cells
                            barycenter_daughter2_pmx = barycenter_after_symmetry(pmx_barycenter,
                                                                                 barycenter_daughter2_pmx,
                                                                                 sym_vector_pmx)
                            # get daughter 1 symetry % embryo's barycenters
                            barycenter_daughter1_pmx = barycenter_after_symmetry(pmx_barycenter,
                                                                                 barycenter_daughter1_pmx,
                                                                                 sym_vector_pmx)

                            division_direction_pmx = barycenter_daughter2_pmx - barycenter_daughter1_pmx
                            if direction_type != 'bary':
                                # if no interface between sisters: continue
                                if sisters_interface_vectors_pmx is None or sisters_interface_vectors_ref is None:
                                    continue

                                # we know how to register a vecto
                                # sisters_interface_vectors_ref
                                ###
                                registered_v3_pmx = np.array(sisters_interface_vectors_pmx)
                                registered_v3_pmx = registered_v3_pmx.T  # reshape to have 3,N shape
                                registered_v3_pmx = apply_transformation(transform, registered_v3_pmx, translate=False)
                                registered_v3_pmx = registered_v3_pmx.T  # shape to N,3

                                division_direction_pmx_i = apply_symetry_on_vector(sym_vector_pmx, registered_v3_pmx[2])
                                dot_product = np.dot(division_direction_pmx_i, division_direction_pmx)
                                if dot_product < 0:
                                    division_direction_pmx = -division_direction_pmx_i
                                else:
                                    division_direction_pmx = division_direction_pmx_i

                            normalized_division_direction_pmx = division_direction_pmx / np.linalg.norm(
                                division_direction_pmx)
                            divisions_properties[mother_name[:-1]]['division_directions_after_symmetry']['left'][
                                pmx_name] = normalized_division_direction_pmx

                            #########
                            ###
                            bary_d2_pmx_scaled = barycenter_after_symmetry(pmx_barycenter,
                                                                                 bary_d2_pmx_scaled,
                                                                                 sym_vector_pmx)
                            # get daughter 1 symetry % embryo's barycenters
                            bary_d1_pmx_scaled = barycenter_after_symmetry(pmx_barycenter,
                                                                                 bary_d1_pmx_scaled,
                                                                                 sym_vector_pmx)

                            divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                                'left']['first'][
                                pmx_name] = bary_d1_pmx_scaled
                            divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                                'left']['second'][
                                pmx_name] = bary_d2_pmx_scaled


                            #########
                            ###

                            ########
                            ####
                            divisions_properties[mother_name[:-1]]['cell_volume_scaled']['left'][pmx_name] =  mother_volume_pmx

                            divisions_properties[mother_name[:-1]]['cell_surface_scaled']['left'][
                                pmx_name] = mother_surface_pmx

                            #######
                            ###
                            # put cylindrical coords
                            divisions_properties[mother_name[:-1]]['cylindrical_coords']['left'][pmx_name] = (
                            z_pmx, r_pmx)

                            ########
                            ####
                            # Put residues
                            divisions_properties[mother_name[:-1]]['residues']['left'][
                                pmx_name] = residues
                            #######
                            #####

                            divisions_properties[mother_name[:-1]]['division_volume_ratio']['left'][
                                pmx_name] = pmx_properties['cell_volume'][daughter2_id_pmx] / \
                                            (pmx_properties['cell_volume'][daughter2_id_pmx] + pmx_properties['cell_volume'][daughter1_id_pmx])

                            divisions_properties[mother_name[:-1]]['division_aq_time']['left'][
                                pmx_name] = division_time_pmx

                            # add division's mother lifetime, division_mothers_lifetimes
                            divisions_properties[mother_name[:-1]]['division_mothers_lifetimes']['left'][
                                pmx_name] = flo_lifetime


                            divisions_properties[mother_name[:-1]]['cell_radius']['left'][pmx_name] = radius_pmx

                            divisions_properties[mother_name[:-1]]['ids_in_ref']['left'] = divided_cell_id_pm1

                            divisions_properties[mother_name[:-1]]['embryo_symmetry_plane_at_t'][
                                pmx_name] = sym_vector_pmx
                            # add id in ref and other embryodivisions_properties[mother_name[:-1]]['cell_ids']['left'][pmx_name] = divided_cell_id_pmx

                            # add neighbors data in each individual
                            divisions_properties[mother_name[:-1]]['neighbors']['left'][pmx_name] = \
                                neighbor_name_surface_pmx

                            divisions_properties[mother_name[:-1]]['cell_surface']['left'][pmx_name] = \
                                cell_surface_pmx

                            # add cell principal value and vectors
                            # apply symmetry
                            ###
                            # before we registered eigenvectors cz we wanted to verify Hertwig, now we do it in  a
                            # seperate code in divisions init
                            #v1 = registered_pcas
                            #registered_pcas_pmx = apply_symetry_on_vector(sym_vector_pmx, v1)
                            #v2 = np.array(ref_properties['cell_principal_vectors'][divided_cell_id_pm1])
                            #registered_pcas_ref = apply_symetry_on_vector(sym_vector_ref, v2)
                            ####
                            ###
                            #
                            divisions_properties[mother_name[:-1]]['cell_principal_vectors']['left'][
                                pmx_name] = np.array(pmx_properties['cell_principal_vectors'][divided_cell_id_pmx])

                            divisions_properties[mother_name[:-1]]['cell_principal_values']['left'][
                                pmx_name] = pmx_properties['cell_principal_values'][divided_cell_id_pmx]

                            # add 'cell_vector_normal_to_sister_interface: : without registration nor symetry
                            divisions_properties[mother_name[:-1]]['sisters_interface_vectors']['left'][
                                pmx_name] = sisters_interface_vectors_pmx

                            divisions_properties[mother_name[:-1]]['sisters_surface_principal_values']['left'][
                                pmx_name] = sisters_surface_principal_values_pmx

                            divisions_properties[mother_name[:-1]]['sisters_interface_edges_vectors']['left'][
                                pmx_name] = sisters_interface_edges_vectors_pmx

                            divisions_properties[mother_name[:-1]]['sisters_surface_edges_principal_values']['left'][
                                pmx_name] = sisters_surface_edges_principal_values_pmx

                            # add cell apical surface vector : without registration nor symetry
                            divisions_properties[mother_name[:-1]]['apical_surface_principal_vectors']['left'][
                                pmx_name] = apical_surface_principal_vectors_pmx

                            divisions_properties[mother_name[:-1]]['apical_surface_principal_values']['left'][
                                pmx_name] = apical_surface_principal_values_pmx

                            divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_vectors']['left'][
                                pmx_name] = apical_surface_edges_principal_vectors_pmx

                            divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_values']['left'][
                                pmx_name] = apical_surface_edges_principal_values_pmx

                            # add div direction without registration nor symetry
                            divisions_properties[mother_name[:-1]]['division_directions_before_registration']['left'][
                                pmx_name] = \
                                pmx_properties['cell_barycenter'][daughter2_id_pmx] - pmx_properties['cell_barycenter'][
                                    daughter1_id_pmx]

                        else:
                            #######
                            ###
                            ####
                            # store right ref data also
                            if direction_type != 'bary':
                                # if no interface between sisters: continue
                                if sisters_interface_vectors_pmx is None or sisters_interface_vectors_ref is None:
                                    continue

                                # we know how to register a vecto
                                # sisters_interface_vectors_ref
                                ###
                                registered_v3_pmx = np.array(sisters_interface_vectors_pmx)
                                registered_v3_pmx = registered_v3_pmx.T  # reshape to have 3,N shape
                                registered_v3_pmx = apply_transformation(transform, registered_v3_pmx, translate=False)
                                registered_v3_pmx = registered_v3_pmx.T  # shape to N,3

                                division_direction_pm1_i = np.array(sisters_interface_vectors_ref[2])
                                dot_product = np.dot(division_direction_pm1_i, division_direction_pm1)
                                if dot_product < 0:
                                    division_direction_pm1 = -division_direction_pm1_i
                                else:
                                    division_direction_pm1 = division_direction_pm1_i

                                division_direction_pmx_i = np.array(registered_v3_pmx[2])
                                dot_product = np.dot(division_direction_pmx_i, division_direction_pmx)
                                if dot_product < 0:
                                    division_direction_pmx = -division_direction_pmx_i
                                else:
                                    division_direction_pmx = division_direction_pmx_i

                            normalized_division_direction_pmx = division_direction_pmx / np.linalg.norm(
                                division_direction_pmx)
                            normalized_division_direction_pm1 = division_direction_pm1 / np.linalg.norm(
                                division_direction_pm1)

                            ####
                            #######
                            ####

                            divisions_properties[mother_name[:-1]]['division_directions_after_symmetry']['right'][
                                pmx_name] = normalized_division_direction_pmx
                            divisions_properties[mother_name[:-1]]['division_directions_after_symmetry']['right'][
                                ref_name] = normalized_division_direction_pm1

                            ########
                            ####
                            ########
                            ####
                            # Put residues
                            divisions_properties[mother_name[:-1]]['residues']['right'][
                                ref_name] = -1
                            divisions_properties[mother_name[:-1]]['residues']['right'][
                                pmx_name] = residues
                            #######
                            ####
                            divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                                'right']['first'][
                                pmx_name] = bary_d1_pmx_scaled
                            divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                                'right']['second'][
                                pmx_name] = bary_d2_pmx_scaled

                            divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                                'right']['first'][
                                ref_name] = bary_d1_pm1_scaled
                            divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                                'right']['second'][
                                ref_name] = bary_d2_pm1_scaled

                            ########
                            ####
                            ########
                            ####
                            divisions_properties[mother_name[:-1]]['cell_volume_scaled']['right'][
                                ref_name] = mother_volume_ref
                            divisions_properties[mother_name[:-1]]['cell_volume_scaled']['right'][
                                pmx_name] = mother_volume_pmx

                            divisions_properties[mother_name[:-1]]['cell_surface_scaled']['right'][
                                ref_name] = mother_surface_ref
                            divisions_properties[mother_name[:-1]]['cell_surface_scaled']['right'][
                                pmx_name] = mother_surface_pmx

                            #######
                            ###
                            # put cylindrical coords
                            divisions_properties[mother_name[:-1]]['cylindrical_coords']['right'][ref_name] = (
                                z_ref, r_ref)
                            divisions_properties[mother_name[:-1]]['cylindrical_coords']['right'][pmx_name] = (
                                z_pmx, r_pmx)

                            ########
                            ####

                            divisions_properties[mother_name[:-1]]['division_volume_ratio']['right'][
                                pmx_name] = pmx_properties['cell_volume'][daughter2_id_pmx] / \
                                            (pmx_properties['cell_volume'][daughter2_id_pmx]+ pmx_properties['cell_volume'][daughter1_id_pmx])
                            divisions_properties[mother_name[:-1]]['division_volume_ratio']['right'][ref_name] = (
                                    ref_properties['cell_volume'][daughter2_id_pm1] /
                                    (ref_properties['cell_volume'][daughter2_id_pm1]+ref_properties['cell_volume'][daughter1_id_pm1]))

                            divisions_properties[mother_name[:-1]]['division_aq_time']['right'][
                                pmx_name] = division_time_pmx
                            divisions_properties[mother_name[:-1]]['division_aq_time']['right'][ref_name] = t

                            # add division's mother lifetime
                            divisions_properties[mother_name[:-1]]['division_mothers_lifetimes']['right'][
                                pmx_name] = flo_lifetime
                            divisions_properties[mother_name[:-1]]['division_mothers_lifetimes']['right'][
                                ref_name] = ref_lifetime

                            divisions_properties[mother_name[:-1]]['ids_in_ref']['right'] = divided_cell_id_pm1

                            divisions_properties[mother_name[:-1]]['cell_radius']['right'][pmx_name] = radius_pmx
                            divisions_properties[mother_name[:-1]]['cell_radius']['right'][ref_name] = radius_pm1

                            # add id in ref and other embryo
                            divisions_properties[mother_name[:-1]]['cell_ids']['right'][ref_name] = divided_cell_id_pm1
                            divisions_properties[mother_name[:-1]]['cell_ids']['right'][pmx_name] = divided_cell_id_pmx

                            # add neighbors data in each individual
                            divisions_properties[mother_name[:-1]]['neighbors']['right'][ref_name] = \
                                neighbor_name_surface_ref
                            divisions_properties[mother_name[:-1]]['neighbors']['right'][pmx_name] = \
                                neighbor_name_surface_pmx

                            divisions_properties[mother_name[:-1]]['cell_surface']['right'][ref_name] = \
                                cell_surface_ref
                            divisions_properties[mother_name[:-1]]['cell_surface']['right'][pmx_name] = \
                                cell_surface_pmx

                            # add cell principal value and vectors

                            divisions_properties[mother_name[:-1]]['cell_principal_vectors']['right'][ref_name] \
                                = np.array(ref_properties['cell_principal_vectors'][divided_cell_id_pm1])
                            divisions_properties[mother_name[:-1]]['cell_principal_vectors']['right'][
                                pmx_name] = np.array(pmx_properties['cell_principal_vectors'][divided_cell_id_pmx])

                            divisions_properties[mother_name[:-1]]['cell_principal_values']['right'][ref_name] \
                                = ref_properties['cell_principal_values'][divided_cell_id_pm1]
                            divisions_properties[mother_name[:-1]]['cell_principal_values']['right'][
                                pmx_name] = pmx_properties['cell_principal_values'][divided_cell_id_pmx]

                            ##
                            # add 'cell_vector_normal_to_sister_interface: : without registration nor symetry
                            divisions_properties[mother_name[:-1]]['sisters_interface_vectors']['right'][
                                ref_name] = sisters_interface_vectors_ref
                            divisions_properties[mother_name[:-1]]['sisters_interface_vectors']['right'][
                                pmx_name] = sisters_interface_vectors_pmx

                            divisions_properties[mother_name[:-1]]['sisters_surface_principal_values']['right'][
                                ref_name] = sisters_surface_principal_values_ref
                            divisions_properties[mother_name[:-1]]['sisters_surface_principal_values']['right'][
                                pmx_name] = sisters_surface_principal_values_pmx

                            divisions_properties[mother_name[:-1]]['sisters_interface_edges_vectors']['right'][
                                ref_name] = sisters_interface_edges_vectors_ref
                            divisions_properties[mother_name[:-1]]['sisters_interface_edges_vectors']['right'][
                                pmx_name] = sisters_interface_edges_vectors_pmx

                            divisions_properties[mother_name[:-1]]['sisters_surface_edges_principal_values']['right'][
                                ref_name] = sisters_surface_edges_principal_values_ref
                            divisions_properties[mother_name[:-1]]['sisters_surface_edges_principal_values']['right'][
                                pmx_name] = sisters_surface_edges_principal_values_pmx

                            # add cell apical surface vector : without registration nor symetry
                            divisions_properties[mother_name[:-1]]['apical_surface_principal_vectors']['right'][
                                ref_name] = apical_surface_principal_vectors_ref
                            divisions_properties[mother_name[:-1]]['apical_surface_principal_vectors']['right'][
                                pmx_name] = apical_surface_principal_vectors_pmx

                            divisions_properties[mother_name[:-1]]['apical_surface_principal_values']['right'][
                                ref_name] = apical_surface_principal_values_ref
                            divisions_properties[mother_name[:-1]]['apical_surface_principal_values']['right'][
                                pmx_name] = apical_surface_principal_values_pmx

                            divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_vectors']['right'][
                                ref_name] = apical_surface_edges_principal_vectors_ref
                            divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_vectors']['right'][
                                pmx_name] = apical_surface_edges_principal_vectors_pmx

                            divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_values']['right'][
                                ref_name] = apical_surface_edges_principal_values_ref
                            divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_values']['right'][
                                pmx_name] = apical_surface_edges_principal_values_pmx

                            # add division direction before registration
                            divisions_properties[mother_name[:-1]]['division_directions_before_registration']['right'][
                                ref_name] = ref_properties['cell_barycenter'][
                                                ref_properties['cell_lineage'][divided_cell_id_pm1][1]] - \
                                            ref_properties['cell_barycenter'][
                                                ref_properties['cell_lineage'][divided_cell_id_pm1][0]]
                            divisions_properties[mother_name[:-1]]['division_directions_before_registration']['right'][
                                pmx_name] = pmx_properties['cell_barycenter'][
                                                pmx_properties['cell_lineage'][divided_cell_id_pmx][1]] - \
                                            pmx_properties['cell_barycenter'][
                                                pmx_properties['cell_lineage'][divided_cell_id_pmx][0]]
                        ##########
                        ####
                        #######
                        ### Add right ref info
                        ####
                        # store right ref data also
                        if direction_type != 'bary':
                            # if no interface between sisters: continue
                            if sisters_interface_vectors_pmx is None or sisters_interface_vectors_ref is None:
                                continue

                            # we know how to register a vecto
                            # sisters_interface_vectors_ref
                            ###

                            division_direction_pm1_i = np.array(sisters_interface_vectors_ref[2])
                            dot_product = np.dot(division_direction_pm1_i, division_direction_pm1)
                            if dot_product < 0:
                                division_direction_pm1 = -division_direction_pm1_i
                            else:
                                division_direction_pm1 = division_direction_pm1_i

                        normalized_division_direction_pm1 = division_direction_pm1 / np.linalg.norm(
                            division_direction_pm1)

                        ####
                        #######
                        ####
                        divisions_properties[mother_name[:-1]]['division_directions_after_symmetry']['right'][
                            ref_name] = normalized_division_direction_pm1

                        ########
                        ####
                        ########
                        ####
                        # Put residues
                        divisions_properties[mother_name[:-1]]['residues']['right'][
                            ref_name] = -1
                        #######
                        ####

                        divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                            'right']['first'][
                            ref_name] = bary_d1_pm1_scaled
                        divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                            'right']['second'][
                            ref_name] = bary_d2_pm1_scaled

                        ########
                        ####
                        ########
                        ####
                        divisions_properties[mother_name[:-1]]['cell_volume_scaled']['right'][
                            ref_name] = mother_volume_ref

                        divisions_properties[mother_name[:-1]]['cell_surface_scaled']['right'][
                            ref_name] = mother_surface_ref

                        #######
                        ###
                        # put cylindrical coords
                        divisions_properties[mother_name[:-1]]['cylindrical_coords']['right'][ref_name] = (
                            z_ref, r_ref)

                        ########
                        ####

                        divisions_properties[mother_name[:-1]]['division_volume_ratio']['right'][ref_name] = (
                                ref_properties['cell_volume'][daughter2_id_pm1] /
                                (ref_properties['cell_volume'][daughter2_id_pm1]+ ref_properties['cell_volume'][daughter1_id_pm1]))

                        divisions_properties[mother_name[:-1]]['division_aq_time']['right'][ref_name] = t

                        # add division's mother lifetime
                        divisions_properties[mother_name[:-1]]['division_mothers_lifetimes']['right'][
                            ref_name] = ref_lifetime

                        divisions_properties[mother_name[:-1]]['ids_in_ref']['right'] = divided_cell_id_pm1

                        divisions_properties[mother_name[:-1]]['cell_radius']['right'][ref_name] = radius_pm1

                        # add id in ref and other embryo
                        divisions_properties[mother_name[:-1]]['cell_ids']['right'][ref_name] = divided_cell_id_pm1

                        # add neighbors data in each individual
                        divisions_properties[mother_name[:-1]]['neighbors']['right'][ref_name] = \
                            neighbor_name_surface_ref

                        divisions_properties[mother_name[:-1]]['cell_surface']['right'][ref_name] = \
                            cell_surface_ref

                        # add cell principal value and vectors

                        divisions_properties[mother_name[:-1]]['cell_principal_vectors']['right'][ref_name] \
                            = np.array(ref_properties['cell_principal_vectors'][divided_cell_id_pm1])

                        divisions_properties[mother_name[:-1]]['cell_principal_values']['right'][ref_name] \
                            = ref_properties['cell_principal_values'][divided_cell_id_pm1]

                        ##
                        # add 'cell_vector_normal_to_sister_interface: : without registration nor symetry
                        divisions_properties[mother_name[:-1]]['sisters_interface_vectors']['right'][
                            ref_name] = sisters_interface_vectors_ref

                        divisions_properties[mother_name[:-1]]['sisters_surface_principal_values']['right'][
                            ref_name] = sisters_surface_principal_values_ref

                        divisions_properties[mother_name[:-1]]['sisters_interface_edges_vectors']['right'][
                            ref_name] = sisters_interface_edges_vectors_ref

                        divisions_properties[mother_name[:-1]]['sisters_surface_edges_principal_values']['right'][
                            ref_name] = sisters_surface_edges_principal_values_ref

                        # add cell apical surface vector : without registration nor symetry
                        divisions_properties[mother_name[:-1]]['apical_surface_principal_vectors']['right'][
                            ref_name] = apical_surface_principal_vectors_ref

                        divisions_properties[mother_name[:-1]]['apical_surface_principal_values']['right'][
                            ref_name] = apical_surface_principal_values_ref

                        divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_vectors']['right'][
                            ref_name] = apical_surface_edges_principal_vectors_ref

                        divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_values']['right'][
                            ref_name] = apical_surface_edges_principal_values_ref

                        # add division direction before registration
                        divisions_properties[mother_name[:-1]]['division_directions_before_registration']['right'][
                            ref_name] = ref_properties['cell_barycenter'][
                                            ref_properties['cell_lineage'][divided_cell_id_pm1][1]] - \
                                        ref_properties['cell_barycenter'][
                                            ref_properties['cell_lineage'][divided_cell_id_pm1][0]]

            else:
                print(f"Error with cell {pmx_properties['cell_name'][divided_cell_id_pmx]} from {ref_name} "
                      f"or {pmx_name}. Skipping this iteration.")


def translate_barycenters(bary_d1_pmx_scaled, bary_d2_pmx_scaled, sym_vector_pmx):
    """
    Translate two sets of barycenter coordinates by a given translation vector.

    Parameters:
    bary_d1_pmx_scaled (np.array): Barycenter coordinates of the first daughter cell.
    bary_d2_pmx_scaled (np.array): Barycenter coordinates of the second daughter cell.
    sym_vector_pmx (np.array): Translation vector.

    Returns:
    tuple: Translated barycenter coordinates for both daughter cells.
    """
    bary_d1_pmx_translated = bary_d1_pmx_scaled + sym_vector_pmx
    bary_d2_pmx_translated = bary_d2_pmx_scaled + sym_vector_pmx
    return bary_d1_pmx_translated, bary_d2_pmx_translated


def register_barycenters(transform, flo_dict_barycenters):
    """
    Applies a transformation to a dictionary of barycenters and returns the registered barycenters.

    Parameters:
    transform (numpy.ndarray): A 4x4 homogeneous transformation matrix.
    flo_dict_barycenters (dict): A dictionary where keys are cell IDs and values are barycenter 3D coordinates.

    Returns:
    dict: A dictionary with registered barycenters.
    """
    registered_barycenters = {}

    for cell_id, barycenter in flo_dict_barycenters.items():
        # Convert the barycenter to homogeneous coordinates (4D)
        homogeneous_barycenter = np.append(barycenter, 1)

        # Apply the transformation
        transformed_barycenter = np.dot(transform, homogeneous_barycenter)

        # Convert back to 3D coordinates
        registered_barycenters[cell_id] = transformed_barycenter[:3]

    return registered_barycenters


def get_dividing_names_ids_(dict_cell_names_pmx, list_cell_names_ref, list_cell_names_ids, dir=['_','*']):
    """
    Given dict_cell_names={'cell_id': cell_name'} and a list of cell names,
    returns a list of corresponding cell IDs in pmx and ids in pm1.
     For each cell name, consider the first ID (or first key).
    """
    cell_ids_pmx = []
    cell_names_pmx = []

    cell_ids_ref = []
    cell_names_ref = []

    for cell_name_ref, cell_id_ref in zip(list_cell_names_ref,list_cell_names_ids):
        n1 = cell_name_ref[:-1] + '_'
        if cell_name_ref not in list(dict_cell_names_pmx.values()) and n1 not in list(dict_cell_names_pmx.values()):
            print("Name not found in embryo:", cell_name_ref)
            continue
        for d in dir:
            for cell_id, name in dict_cell_names_pmx.items():
                    if name[-1] != d:
                        continue
                    if name[:-1] == cell_name_ref[:-1]:
                            cell_ids_pmx.append(cell_id)
                            cell_names_pmx.append(name)

                            cell_ids_ref.append(cell_id_ref)
                            cell_names_ref.append(cell_name_ref)
                            break  # Stop searching after the first match
    return cell_ids_pmx, cell_names_pmx, cell_ids_ref, cell_names_ref



def get_cell_ids(dict_cell_names, list_cell_names):
    """
    Should work for other property dictionaries
    Given dict_cell_names={'cell_id': cell_name'} and a list of cell names,
    returns a list of corresponding cell IDs. For each cell name, consider the first ID (or first key).
    """
    cell_ids = []
    cell_names = []
    for cell_name in list_cell_names:
        if cell_name not in list(dict_cell_names.values()):
            # print("Name not found in embryo:", cell_name)
            continue
        for cell_id, name in dict_cell_names.items():
            if name == cell_name:
                cell_ids.append(cell_id)
                cell_names.append(name)
                break  # Stop searching after the first match
    return cell_ids, cell_names


def robust_average_l2_distance(points1, points2):
    """
    Computes the robust average L2 distance between two sets of points in 3D.

    Parameters:
    points1 (np.ndarray): First set of points of shape (3, N).
    points2 (np.ndarray): Second set of points of shape (3, N).

    Returns:
    float: The robust average L2 distance between the two sets of points.
    """
    if points1.shape != points2.shape or points1.shape[0] != 3:
        raise ValueError("Both input arrays must be of shape (3, N).")

    # Compute the pairwise L2 distances
    distances = np.linalg.norm(points1 - points2, axis=0)

    # Sort distances and exclude the highest 10%
    sorted_distances = np.sort(distances)
    cutoff_index = int(len(sorted_distances) * 0.9)
    filtered_distances = sorted_distances[:cutoff_index]

    # Compute the average of the remaining distances
    robust_avg_distance = np.mean(filtered_distances)

    return robust_avg_distance


def rescale_barycenter(registered_barycenter, similitude_transform, new_scale):
    """
    Rescales a registered barycenter using a new scaling factor.

    Parameters
    ----------
    registered_barycenter : np.array
        The registered barycenter coordinates (3x1 or 3xN).
    similitude_transform : np.array
        The similitude transformation matrix (4x4), including scaling, rotation, and translation.
    new_scale : float
        The new scaling factor to apply.

    Returns
    -------
    rescaled_barycenter : np.array
        The barycenter coordinates rescaled using the new scaling factor.
    """
    # Extract the scaling factor from the similitude transform
    scaling_factor = np.cbrt(np.linalg.det(similitude_transform[:3, :3]))
    # Remove the existing scaling
    barycenter_without_scaling = registered_barycenter / scaling_factor
    # Apply the new scaling factor
    rescaled_barycenter = barycenter_without_scaling * new_scale
    return rescaled_barycenter


def barycenter_after_symmetry(daughter1_barycenter_pmx_left, daughter2_barycenter_pmx_left, sym_vector):
    """
    Rotates the position of the barycenter 2 in the pmx frame to create a reflection of the division axis
    with respect to the symmetry plane. give vector normal to symmetry plane.

    Parameters:
    daughter1_barycenter_pmx_left (tuple): 3D coordinates of the barycenter of the left daughter cell in the pmx frame.
    daughter2_barycenter_pmx_left : 3D coordinates of the barycenter of the left daughter cell in the pmx frame.
    sym_vector

    Returns:
    tuple: New 3D coordinates of the barycenter 2 in the pmx frame.

    What does it do:
    1 Calculate the normal vector of the symmetry plane (n) as the normalized vector from daughter1_barycenter_pmx_left to daughter1_barycenter_pmx_right.
    2. Calculate the left division direction as a vector from daughter1_barycenter_pmx_left to daughter2_barycenter_pmx_left.
    3. Calculate the non-orthogonal component of left_division_direction with respect to the symmetry plane as left_division_direction_non_orthogonal_component = dot_product(left_division_direction, n) * n.
    4. Calculate the new position of barycenter 2 in the pmx frame as new_barycenter_2_pmx_left = daughter1_barycenter_pmx_left + left_division_direction - 2 * left_division_direction_non_orthogonal_component.

    """
    # Calculate the normal vector (n)
    n = sym_vector
    n /= np.linalg.norm(n)

    # Calculate the left division direction vector
    left_division_vector = np.array(daughter2_barycenter_pmx_left) - np.array(daughter1_barycenter_pmx_left)

    # Calculate the component of left_division_vector parallel to n
    left_division_direction_parallel = np.dot(left_division_vector, n) * n

    # Calculate the new position of barycenter 2 by reflecting it with respect to the plane
    new_barycenter_2_pmx_left = np.array(daughter2_barycenter_pmx_left) - 2 * left_division_direction_parallel

    return new_barycenter_2_pmx_left


def estimate_embryo_barycenter(registered_barycenters_pmx_at_t4, cell_names, cell_volumes):
    """
    Estimates the barycenter of the embryo given the registered barycenters, cell names, and cell volumes.

    Parameters:
    ----------
    registered_barycenters_pmx_at_t4 : dict
        Dictionary with cell IDs as keys and barycenter coordinates as values.
    cell_names : dict
        Dictionary with cell IDs as keys and cell names as values.
    cell_volumes : dict
        Dictionary with cell IDs as keys and cell volumes as values.

    Returns:
    -------
    embryo_barycenter : np.ndarray
        The estimated barycenter of the embryo.
    """

    total_barycenter = np.zeros(3)  # Initialize the total barycenter as (0, 0, 0)
    total_volume = 0  # Initialize the total cell volume

    for cell_id, cell_name in cell_names.items():
        barycenter = registered_barycenters_pmx_at_t4.get(cell_id)
        cell_volume = cell_volumes.get(cell_id)

        if barycenter is not None and cell_volume is not None:
            total_barycenter += np.array(barycenter) * cell_volume
            total_volume += cell_volume

    if total_volume > 0:
        embryo_barycenter = total_barycenter / total_volume
    else:
        embryo_barycenter = np.zeros(3)  # Default to zero vector if no valid cells

    return embryo_barycenter


def compute_cell_coordinates_relative_to_symmetry(embryo_center_of_mass, sym_vector, cell_coords):
    """
    Compute the z and r coordinates for a given cell with respect to the symmetry plane of the embryo.

    Parameters:
    ----------
    embryo_center_of_mass : np.ndarray
        The center of mass of the embryo (3D coordinates).
    sym_vector : np.ndarray
        The vector normal to the symmetry plane (3D coordinates).
    cell_coords : np.ndarray
        The coordinates of the cell (3D coordinates).

    Returns:
    -------
    z : float
        The distance of the cell to the symmetry plane.
    r : float
        The distance between the projection of the cell onto the symmetry plane and the embryo center of mass.
    """
    # Normalize the symmetry vector
    sym_vector_normalized = sym_vector / np.linalg.norm(sym_vector)

    # Compute the vector from the cell to the embryo center of mass
    vector_to_center = cell_coords - embryo_center_of_mass

    # Compute the z coordinate as the projection of the vector_to_center onto the sym_vector
    z = np.dot(vector_to_center, sym_vector_normalized)

    # Compute the projection of the cell onto the symmetry plane
    cell_projection = cell_coords - z * sym_vector_normalized

    # Compute the r coordinate as the distance from the projection to the embryo center of mass
    r = np.linalg.norm(cell_projection - embryo_center_of_mass)

    return z, r


def estimate_sym_vector(dict_barycenters_at_t4, dict_names_at_t4, dict_cell_mass):
    """
    Gives estimation of vector normal to symmetry of embryo

    Given dictionary at timestamp t4 of barycenters and names
    Left cells : names ending with '_'
    Right cells : names ending with '*'
    Parameters:
        dict_barycenters_at_t4 = { cell_id: [barycenter] }
        dict_names_at_t4 = {cell_id: cell_name }
        dict_cell_mass = {cell_id: cell_mass}
    Returns:
        sym_vector : The vector joining the barycenter of right cells with barycenter of left cells
    """
    left_barycenter = np.zeros(3)  # Initialize the left barycenter as (0, 0, 0)
    left_mass = 0  # Initialize the total left cell mass

    right_barycenter = np.zeros(3)  # Initialize the right barycenter as (0, 0, 0)
    right_mass = 0  # Initialize the total right cell mass

    for cell_id, cell_name in dict_names_at_t4.items():
        barycenter = dict_barycenters_at_t4.get(cell_id)
        cell_mass = dict_cell_mass.get(cell_id)

        if barycenter is not None and cell_mass is not None:
            if cell_name.endswith('_'):  # Left cell
                left_barycenter += np.array(barycenter) * cell_mass
                left_mass += cell_mass
            elif cell_name.endswith('*'):  # Right cell
                right_barycenter += np.array(barycenter) * cell_mass
                right_mass += cell_mass

    if left_mass > 0:
        left_barycenter /= left_mass

    if right_mass > 0:
        right_barycenter /= right_mass

    # Calculate the symmetry vector as the vector from right barycenter to left barycenter
    sym_vector = right_barycenter - left_barycenter
    return sym_vector


def apply_symetry_on_vector(sym_vector, vectors):
    """
    Applies symmetry transformation to a given array of vectors.
    Parameters
    ----------
    sym_vecto (ndarray): Vector representing the symmetry transformation.
    v_n_plane_pmx (array_like): Array of vector/s to which the symmetry transformation is applied.

    Returns
    -------
    array_like: Transformed vectors after applying the symmetry transformation
    """
    sym_vector = np.array(sym_vector)
    sym_vector = np.array(sym_vector) / np.linalg.norm(sym_vector)
    # Check if vectors is a single vector, an array of vectors, or an array of arrays of vectors
    if isinstance(vectors, (np.ndarray, list)):
        if isinstance(vectors[0], (np.ndarray, list)):
            # If vectors is an array of an array of arrays (multiple vectors), apply transformation
            # to each array of vectors
            transformed_vectors = []
            for vec_arr in vectors:
                transformed_vec_arr = apply_symetry_on_vector(sym_vector, vec_arr)
                transformed_vectors.append(transformed_vec_arr)
            return np.array(transformed_vectors)
        else:
            # If vectors is a single vector, apply transformation to the single vector
            v_n_plane_pmx = np.array(vectors).reshape((3,))
            dot_product = np.dot(sym_vector, v_n_plane_pmx)
            # transformed_vector = 2 * dot_product * sym_vector - v_n_plane_pmx
            transformed_vector = v_n_plane_pmx - 2 * dot_product * sym_vector
            return transformed_vector


def get_vectors_ordered(dict_pm1_name, dict_bary_pm1, dict_pmx_name, dict_bary_pmx):
    """
    This function serves to return the ref and flo vectors before registration by lts/lt
    so that each point at each index in both vector corresponds to the same cell name
    Parameters :
        dict_pm1_name:
        dict_bary_pm1,dict_pmx_name:
        dict_bary_pmx:
    Return :
        ref: reference vector
        flo: floating vector
        sorted_keys_ref : cell names in each vector
    """
    # Create a dictionary with cell names as keys and corresponding barycenter as values
    ref_dict = {dict_pm1_name[cell_id]: dict_bary_pm1[cell_id] for cell_id in dict_pm1_name}
    flo_dict = {dict_pmx_name[cell_id]: dict_bary_pmx[cell_id] for cell_id in dict_pmx_name}

    sorted_keys_ref = list(ref_dict.keys())
    sorted_keys_ref.sort()
    sorted_keys_flo = list(flo_dict.keys())
    sorted_keys_flo.sort()

    # Check if keys are the same
    if sorted_keys_ref != sorted_keys_flo:
        print("Keys are not the same or not in the same order.")

    # Sort the dictionaries by cell name
    sorted_ref_dict = {k: ref_dict[k] for k in sorted_keys_ref}
    sorted_flo_dict = {k: flo_dict[k] for k in sorted_keys_flo}

    # Extract the vectors from the sorted dictionaries
    ref = list(sorted_ref_dict.values())
    flo = list(sorted_flo_dict.values())

    return ref, flo, sorted_keys_ref


def transform_to4d_points(ref, flo):
    """
    Transform to homologous coordinates before applying ls or lts transforms..
    """
    mat = np.identity(4)

    # transform to 4D points
    lref = np.ones((4, ref.shape[1]))
    lref[:3, :] = ref
    lflo = np.ones((4, flo.shape[1]))
    lflo[:3, :] = flo
    return lref, lflo


def global_similitude(ref_dict_barycenters, ref_dict_name, flo_dict_barycenters, flo_dict_name, ref_name="",
                      registered_ind="",
                      t_in_ref=0, t_in_ind=0, retained_fraction=0.25):
    """
    Gets dictionary of barycenters in pm1 and pmx. Compute the transformation, apply it, returns the new dict of
    barycenter positions
    """
    ref, flo, sorted_names = get_vectors_ordered(ref_dict_name, ref_dict_barycenters, flo_dict_name,
                                                 flo_dict_barycenters)

    dict_registered_barycenters = {}

    ref = np.array(ref)
    ref = ref.T

    floating = np.array(flo)
    floating = floating.T

    homologous_ref, homologous_floating = transform_to4d_points(ref, floating)
    # print("ref shape", homologous_ref.shape)
    # retained fraction by default 25%
    transform = lts_transformation(homologous_ref, homologous_floating, transformation_type="similitude",
                                   retained_fraction=retained_fraction)

    # save transform numpy array in a folder, name of file reflect: transform type/ reference individual /
    # transformed individual / timestamp in reference frame / timestamp in registered frame
    transform_type = "similitude"
    reference_individual = ref_name
    transformed_individual = registered_ind
    timestamp_reference = t_in_ref
    timestamp_registered = t_in_ind
    # Construct the file name
    # file_name = f"{transform_type}_{reference_individual}_{transformed_individual}_{timestamp_reference}_{timestamp_registered}.npy"
    # Save the transform array to a file
    # folder_path = "transformations_folder"  # Update this with your folder path
    # file_path = f"{folder_path}/{file_name}"
    # np.save(file_path, transform)

    return transform, floating, ref, sorted_names


def apply_transformation(transform, floating, translate=True):
    """
    Applies the transformation matrix to the floating points.
    transform : homogeneous coordinates
    """
    transformed_floating = np.empty_like(floating)
    if translate:
        for i in range(floating.shape[1]):
            point = floating[:, i]  # Extract one 3D point at a time
            transformed_point = np.dot(transform[:3, :3], point) + transform[:3, 3]
            transformed_floating[:, i] = transformed_point
    else:
        for i in range(floating.shape[1]):
            point = floating[:, i]  # Extract one 3D point at a time
            transformed_point = np.dot(transform[:3, :3], point)
            transformed_floating[:, i] = transformed_point
    return transformed_floating


def update_barycenters(transformed_floating, floating, flo_dict_barycenters):
    """
    Updates the barycenters with the transformed floating points.
    """
    dict_registered_barycenters = {}
    for cell_id, value_before_transform in flo_dict_barycenters.items():
        mask = np.all(floating == value_before_transform[:, np.newaxis], axis=0)
        index = np.where(mask)
        value = transformed_floating[:, index].squeeze()
        dict_registered_barycenters[cell_id] = value
    return dict_registered_barycenters


def post_process_reduction(reduced_dict_barycenters_pm1, reduced_dict_cell_name_pm1, reduced_dict_barycenters_pmx,
                           reduced_dict_cell_name_pmx):
    """
    Sometimes certain individuals have cells that dissapear due to mistakes in segmentation
    They introduce error in the reduction to closest ancester: the frames arent equal,
    some daughters dont have a paretn in the homologue embryo or vice versa..
    ...just remove them by removing cells that are unique in each embryo
    """
    set_names_pm1 = set(reduced_dict_cell_name_pm1.values())
    set_names_pmx = set(reduced_dict_cell_name_pmx.values())
    unique_names_pm1 = set_names_pm1 - set_names_pmx
    unique_names_pmx = set_names_pmx - set_names_pm1

    list_names_unique_pm1 = list(unique_names_pm1)
    list_names_unique_pmx = list(unique_names_pmx)
    # if len(list_names_unique_pm1)!=0:
    #    print("cells to be ignored from pm1 embryo(werent found in the second embryo):",list_names_unique_pm1)
    # if len(list_names_unique_pmx)!=0:
    #    print("cells to be ignored from pmx embryo(werent found in the second embryo):",list_names_unique_pmx)
    # ids of cells with unique in pm1 and pmx
    cell_ids_to_remove_pm1 = [cell_id for cell_id, cell_name in reduced_dict_cell_name_pm1.items() if
                              cell_name in list_names_unique_pm1]
    cell_ids_to_remove_pmx = [cell_id for cell_id, cell_name in reduced_dict_cell_name_pmx.items() if
                              cell_name in list_names_unique_pmx]

    # remove them from dict
    for cell_id in cell_ids_to_remove_pm1:
        del reduced_dict_cell_name_pm1[cell_id]
    for cell_id in cell_ids_to_remove_pm1:
        del reduced_dict_barycenters_pm1[cell_id]

    for cell_id in cell_ids_to_remove_pmx:
        del reduced_dict_cell_name_pmx[cell_id]
    for cell_id in cell_ids_to_remove_pmx:
        del reduced_dict_barycenters_pmx[cell_id]


def nb_of_cells(dict_of_a_property):
    """
    For any list or dictionnary: gives nb of elements
    """
    return len(dict_of_a_property)


def merge_sister_cells(dict_of_barycenters, dict_mass_of_cells, cell_id1, cell_id2):
    """
    Imagine sister cells are merged, the barycenter will be computed of this merge as the:
      sum(position_points_cell1) + sum(position_points_cell2) / nb_of_points_in_both_cells
    parameter:
        : daughter cell ids
    return:
        new_position_barycenter
    """
    new_barycenter_pos = (dict_mass_of_cells[cell_id1] * dict_of_barycenters[cell_id1] + dict_mass_of_cells[cell_id2] *
                          dict_of_barycenters[cell_id2]) / (dict_mass_of_cells[cell_id1] + dict_mass_of_cells[cell_id2])
    return new_barycenter_pos


def merge_daughters(dict_cell_mass_pmx, dict_barycenters_pmx, dict_cell_name_pmx, list_of_dict_merge_info_pmx,
                    nb_of_merges):
    """
    Given a list containing pairs of daughters' names and IDs (pairs of IDs of daughters) and pairs of cell names,
    merge sisters and return a new dictionary of the embryo at t with merged daughters.
    Daughters that have been merged have concatenated IDs, and the parent's name is assigned.
    """
    reduced_dict_barycenters = dict_barycenters_pmx.copy()
    reduced_dict_names = dict_cell_name_pmx.copy()
    count = nb_of_merges
    for merge in list_of_dict_merge_info_pmx:
        if count == 0:
            break
        merge_name = merge['mother_id']
        # daughter1_name, daughter2_name = merge['daughter_names']
        daughter1_id, daughter2_id = merge['daughter_ids']
        # Merge sisters to create a new barycenter position
        merge_barycenter = merge_sister_cells(reduced_dict_barycenters, dict_cell_mass_pmx, daughter1_id, daughter2_id)
        merge_id = f"{daughter1_id}_{daughter2_id}"
        reduced_dict_barycenters[merge_id] = merge_barycenter  # Add the merged cell
        # Remove daughters from the dictionary
        if daughter1_id in reduced_dict_barycenters:
            del reduced_dict_barycenters[daughter1_id]
        if daughter2_id in reduced_dict_barycenters:
            del reduced_dict_barycenters[daughter2_id]
        # Assign the parent's name to the merged cell
        # dict_cell_name_pmx[merge_id] = merge_name
        reduced_dict_names[merge_id] = merge_name  # Add the merged cell
        # delete daughters from name dictionnary
        if daughter1_id in reduced_dict_names:
            del reduced_dict_names[daughter1_id]
        if daughter2_id in reduced_dict_names:
            del reduced_dict_names[daughter2_id]
        count = count - 1
    return reduced_dict_barycenters, reduced_dict_names


def look_for_daughters(dict_names_at_t_pm1, dict_names_at_y_pmx):
    """
    Given embryo names at two different developmental times, searches for daughters of forst inside the second As for
    now the daughter cells that divides are not considered to be merged so if there's a pair record it, if only one
    daughter just show an error message(we're not gonna look for grandparents for now) cell name synthax: 'ap.q_' In
    the cell names there's no A or B 'a' and 'b' prefixes are inherited, '_' and '*' suffixes are inherited daughters
    will have p incremented a cell has two daughters with different numbers: 2q or 2q-1 return:
    list_of_dict_of_merge_info: list of dictionnary with cell name,  pair of daughters id and names(that can be
    merged to make a parent prresent in the forst individual) merge_info = { 'mother_id': mother_name,
    'daughter_names': [pair_of_daughters_names], 'daughter_ids': [pair_of_daughters_ids], }
    """
    list_of_dict_of_merge_info = []
    # print("cell names at t:",dict_names_at_t_pm1.values() )
    for mother_name in dict_names_at_t_pm1.values():
        mother_name_splited = mother_name.split('.')
        ant_post = mother_name_splited[0][0]
        mother_gen = int(mother_name_splited[0][1:])
        mother_q = mother_name_splited[1].split('*')
        if len(mother_q) == 1:
            # The name ends with '_': it's a cell from the right symmetry
            mother_q = mother_name_splited[1].split('_')
        mother_q = int(mother_q[0])
        # print("mother info:",ant_post,mother_gen,mother_q,mother_name[-1] )
        # Search for daughters in dict_names_at_y_pmx
        potential_daughters = []
        for daughter_candidate_id, daughter_candidate_name in dict_names_at_y_pmx.items():
            # Daughter needs to have an identical ant_post prefix as the mother, generation number of the mother incremented by one,
            daughter_name_splitted = daughter_candidate_name.split('.')
            if (
                    daughter_name_splitted[0][0] != ant_post
                    or int(daughter_name_splitted[0][1:]) != mother_gen + 1
                    or daughter_candidate_name[-1] != mother_name[-1]
            ):
                continue
            # and daughter's q value is 2*mother_q or 2*mother_q-1
            daughter_q_value = daughter_name_splitted[1].split('*')
            if len(daughter_q_value) == 1:
                # it means that the name do not end with *
                daughter_q_value = daughter_name_splitted[1].split('_')
            if (
                    int(daughter_q_value[0]) == 2 * mother_q
            ) or (
                    int(daughter_q_value[0]) == 2 * mother_q - 1
            ):
                # Valid daughter found
                # print("found a daughter")
                potential_daughters.append((daughter_candidate_name, daughter_candidate_id))
        # If there are two daughters found in pmx for one parent in pm1
        if len(potential_daughters) == 2:
            merge_info = {
                'mother_id': mother_name,
                'daughter_names': [d[0] for d in potential_daughters],
                'daughter_ids': [d[1] for d in potential_daughters],
            }
            list_of_dict_of_merge_info.append(merge_info)
    return list_of_dict_of_merge_info


def reduce_to_closest_ancestry(dict_barycenters_pm1, dict_barycenters_pmx, dict_mass_of_cell_pm1, dict_mass_of_cell_pmx,
                               dict_cell_name_pm1_at_t4, dict_cell_name_pmx_at_t4):
    """
    Adjust number of cells in different frames of different embryos by merging daughters from each embryo.
    This is mainly due to heterochronies where cells at different individuals would divide at different times.
    To be able to compare two frames of different embryos at different developmental times.

    Prameters:
        dict_barycenters_pm1: at timestamp t+4 {'cell_id': barycenter}
        dict_barycenters_pmx: at timestamp t'+4 {'cell_id': barycenter}
        dict_mass_of_cell_pm1 : used for the merge of daughters at timestamp t'+4 {'cell_id': barycenter}
        dict_mass_of_cell_pmx: same as above
        dict_cell_name_pm1 : used to detect daughters in the other individuals at timestamp t'+4 {'cell_id': barycenter}
        dict_cell_name_pmx : same as above
    Returns:
        dict_barycenters_pm1: embryo pm1(at timestamp t+4) after merging the daughter cells(of cells from pmx) that exist inside it
        dict_barycenters_pmx : the second embryo(at timestamp t'+4) after merging the daughter cells(of cells from pm1) inside it
    """
    # I have two different embryo frames at different developmental times
    # reduce embryo frames to most commun ancesters
    # search for daughters : make sure dict are in t !!!

    list_of_dict_of_merge_info_in_pmx = look_for_daughters(dict_cell_name_pm1_at_t4, dict_cell_name_pmx_at_t4)

    list_of_dict_of_merge_info_in_pm1 = look_for_daughters(dict_cell_name_pmx_at_t4, dict_cell_name_pm1_at_t4)

    nb_of_merges = len(list_of_dict_of_merge_info_in_pm1)
    reduced_dict_barycenters_pm1, reduced_dict_cell_name_pm1 = merge_daughters(dict_mass_of_cell_pm1,
                                                                               dict_barycenters_pm1,
                                                                               dict_cell_name_pm1_at_t4,
                                                                               list_of_dict_of_merge_info_in_pm1,
                                                                               nb_of_merges)
    nb_of_merges = len(list_of_dict_of_merge_info_in_pmx)
    reduced_dict_barycenters_pmx, reduced_dict_cell_name_pmx = merge_daughters(dict_mass_of_cell_pmx,
                                                                               dict_barycenters_pmx,
                                                                               dict_cell_name_pmx_at_t4,
                                                                               list_of_dict_of_merge_info_in_pmx,
                                                                               nb_of_merges)

    post_process_reduction(reduced_dict_barycenters_pm1, reduced_dict_cell_name_pm1, reduced_dict_barycenters_pmx,
                           reduced_dict_cell_name_pmx)

    return reduced_dict_barycenters_pm1, reduced_dict_barycenters_pmx, reduced_dict_cell_name_pm1, reduced_dict_cell_name_pmx


def get_cell_name_parts(cell_name):
    """
    Returns part of the cell name
    """
    cell_name_splitted = cell_name.split('.')
    ant_post = cell_name_splitted[0][0]
    cell_gen = int(cell_name_splitted[0][1:])
    cell_q = cell_name_splitted[1].split('*')
    cell_dir = '*'
    if len(cell_q) == 1:
        # The name ends with '_': it's a cell from the right symmetry
        cell_dir = '*'
        cell_q = cell_name_splitted[1].split('_')
    cell_q = int(cell_q[0])
    return ant_post, cell_gen, cell_q, cell_dir


def get_daughters_ids_at_tn(dict_cell_lineage, divided_cell_id, time):
    """
    Given a lineage dictionary, the divided cell ID, and the timestamp after division,
    returns the two daughters' IDs at t+4 since the division.
    """
    daughter1_id = [dict_cell_lineage[divided_cell_id][0]]
    daughter2_id = [dict_cell_lineage[divided_cell_id][1]]
    for _ in range(0, time - 1):
        if len(daughter1_id) == 2 or len(daughter2_id) == 2:
            # Return None as a flag to indicate one daughter divided
            # print("A daughter has divided:", daughter1_id,daughter2_id)
            return None, None
        if isinstance(daughter1_id, list):
            daughter1_id = int(daughter1_id[0])
        if isinstance(daughter2_id, list):
            daughter2_id = int(daughter2_id[0])
        try:
            daughter1_id = dict_cell_lineage[daughter1_id]
            daughter2_id = dict_cell_lineage[daughter2_id]
        except KeyError:
            # print(f"Cell IDs:{daughter1_id} & {daughter2_id} not found in dictionary. Skipping this iteration.")
            return None, None
    return daughter1_id, daughter2_id


def get_cell_id_at_div(dic_of_lineage, cell_id):
    """
    Iterate through lineage staring with cell id till you find a division
    """
    if isinstance(cell_id, list):
        cell_id = int(cell_id[0])
    if cell_id not in dic_of_lineage:
        # print("cell id not in lineage dict:",cell_id )
        return None
    while len(dic_of_lineage[cell_id]) != 2:
        cell_id = dic_of_lineage[cell_id]
        if isinstance(cell_id, list):
            cell_id = int(cell_id[0])
        if not cell_id in dic_of_lineage:
            # print("cell id not in lineage dict:",cell_id )
            return None
    return cell_id


def get_cells_name(dict_of_cell_names, cell_id):
    """
    Fetches the cell name from its ID. Supports a single ID or a list of IDs.
    """
    if isinstance(cell_id, list):
        # If cell_id is a list of IDs, return a list of cell names
        cell_names = []
        for id in cell_id:
            cell_name = dict_of_cell_names.get(id, "Unknown")
            cell_names.append(cell_name)
        return cell_names
    else:
        # If cell_id is a single ID, return the cell name
        cell_name = dict_of_cell_names.get(cell_id, "Unknown")
        return cell_name


def filter_cells_with_short_life_daughters(dict_cell_lineage_pmx, list_of_divided_cells_ids, delay):
    """
    For each cell in list_of_divided_cellsids,
        get both daughter ids
        for each daughter ensure cell_lineage is ==1 for the next 4 generation
    """
    filtered_list_of_divided_cells_ids = []
    for cell_id in list_of_divided_cells_ids:
        daughter1_id, daughter2_id = dict_cell_lineage_pmx[cell_id]
        # Check if daughters haven't divided in the next 4 timestamps
        daughters_divided = False
        # Check the next 4 generations
        for _ in range(delay):
            if (
                    isinstance(daughter1_id, list) and len(daughter1_id) == 2
            ) or (
                    isinstance(daughter2_id, list) and len(daughter2_id) == 2
            ):
                # print("daughters with short lifetime, d1, d2 ids:",daughter1_id,daughter2_id)
                # Daughters have divided in the next generation, exit loop
                daughters_divided = True
                break
            # Update daughter IDs for the next generation
            if isinstance(daughter1_id, list) and len(daughter1_id) == 1:
                daughter1_id = daughter1_id[0]
            if isinstance(daughter2_id, list) and len(daughter2_id) == 1:
                daughter2_id = daughter2_id[0]
            # if None => not in dictionnary
            if dict_cell_lineage_pmx.get(daughter1_id, None) is None or dict_cell_lineage_pmx.get(daughter2_id,
                                                                                                  None) is None:
                # print("Lineage not continuous,d1, d2 ids:",daughter1_id,daughter2_id )
                break
            daughter1_id = dict_cell_lineage_pmx.get(daughter1_id, None)
            daughter2_id = dict_cell_lineage_pmx.get(daughter2_id, None)
        if not daughters_divided:
            # Daughters haven't divided for the next 4 generations, add cell_id to the filtered list
            filtered_list_of_divided_cells_ids.append(cell_id)
    return filtered_list_of_divided_cells_ids


def get_divided_cells_ids_at_t(dict_cell_lineage_at_t):
    """
    For cells whose dict of lineage at t indicates to two cells: they have divided at t
    returns list of keys(cell id) whose value is a list of size ==2
    """
    list_of_divided_cells = []
    for cell_id, cell_lineage in dict_cell_lineage_at_t.items():
        if isinstance(cell_lineage, list) and len(cell_lineage) == 2:
            list_of_divided_cells.append(cell_id)
    return list_of_divided_cells


def get_embryo_at_t(dict_property, timestamp):
    """
    Gets the dictionnary of embryo property at a particular snapshot
    prameters:

    returns:
    """
    # Extract frame t=t0: dict with all elts whose cell_ids/1000 == first_timestamp
    dict_property_at_t = {cell_id: pos for cell_id, pos in dict_property.items() if int(cell_id / 10000) == timestamp}
    return dict_property_at_t


def get_first_last_timestamps(dict_of_property):
    """
    Gives first and last timestamps of an embryo
    """
    cell_ids = list(dict_of_property.keys())
    # from first cell id get the first timestamp
    first_timestamp = int(cell_ids[0] / 10000)
    # from last cell id get the last timestamp
    last_timestamp = int(cell_ids[-1] / 10000)

    return first_timestamp, last_timestamp

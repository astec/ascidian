import numpy as np

from ascidian.core.icp import lts_transformation
import ascidian.ascidian.name as uname
import ascidian.division_analysis.division_analysis_init_tools
from ascidian.division_analysis.registration_assessment_tools import (local_register, get_neighborhood_dicts)
from ascidian.division_analysis.division_analysis_init_tools import (get_first_last_timestamps, get_divided_cells_ids_at_t,
                                                        filter_cells_with_short_life_daughters, get_embryo_at_t,
                                                        get_cells_name, get_cell_ids, get_cell_id_at_div,
                                                        get_daughters_ids_at_tn, get_cell_name_parts,
                                                        reduce_to_closest_ancestry, apply_transformation,
                                                        update_barycenters, estimate_sym_vector,
                                                        apply_symetry_on_vector,
                                                        barycenter_after_symmetry)


def compare_divisions_in_embryo_and_reference_local_reg(embryo_properties, reference_properties, divisions_properties,
                                                        delay,
                                                        direction_type='interface'):
    # direction_type can be 'bary' or 'interface'

    # Get embryos names, times, different properties
    ref_name, ref_properties = next(iter(reference_properties.items()))
    pmx_name, pmx_properties = next(iter(embryo_properties.items()))
    dict_barycenter_pm1 = ref_properties['cell_barycenter']
    dict_barycenter_pmx = pmx_properties['cell_barycenter']
    first_pm1_time, last_pm1_time = get_first_last_timestamps(dict_barycenter_pm1)

    print('In embryo :', pmx_name)

    # for each time in ref:
    for t in range(first_pm1_time, last_pm1_time):
        # get divided cells ids at t, filter(delay after division), get cells names
        dict_cell_lineage_ref_at_t = get_embryo_at_t(ref_properties['cell_lineage'], t)
        list_of_divided_cells_ids_at_t = get_divided_cells_ids_at_t(dict_cell_lineage_ref_at_t)

        # filter cells with short life daughters
        list_of_divided_cells_ids_at_t_pm1 = filter_cells_with_short_life_daughters(ref_properties['cell_lineage'],
                                                                                    list_of_divided_cells_ids_at_t,
                                                                                    delay)
        # get list of the cell's names
        list_of_divided_cells_names_at_t_pm1 = get_cells_name(ref_properties['cell_name'],
                                                              list_of_divided_cells_ids_at_t_pm1)

        # get cell ids in the other embryo
        # from cell names, get cell ids
        list_of_cell_divided_ids_pmx, list_of_cell_divided_names_pmx = get_cell_ids(pmx_properties['cell_name'],
                                                                                    list_of_divided_cells_names_at_t_pm1)

        if len(list_of_cell_divided_names_pmx) != len(list_of_divided_cells_names_at_t_pm1):
            # keep only the names and ids that exist in pm1 and pmx
            set_of_names_pmx = set(list_of_cell_divided_names_pmx)

            # Synchronize cell names and IDs in pm1 and pmx
            synced_list_of_cell_divided_ids_pmx = []
            synced_list_of_cell_divided_names_pmx = []

            for id_pm1, name_pm1 in zip(list_of_divided_cells_ids_at_t_pm1, list_of_divided_cells_names_at_t_pm1):
                # Check if the name exists in pm1
                if name_pm1 in set_of_names_pmx:
                    synced_list_of_cell_divided_ids_pmx.append(id_pm1)
                    synced_list_of_cell_divided_names_pmx.append(name_pm1)

            # Update the lists
            list_of_divided_cells_ids_at_t_pm1 = synced_list_of_cell_divided_ids_pmx
            list_of_divided_cells_names_at_t_pm1 = synced_list_of_cell_divided_names_pmx

        ##
        # Iterate over divided cell pairs
        ##
        for divided_cell_id_pmx, divided_cell_id_pm1 in zip(list_of_cell_divided_ids_pmx,
                                                            list_of_divided_cells_ids_at_t_pm1):
            # Make divided_cell_id_pmx: divided_cell_id_pmx at division time
            divided_cell_id_pmx = get_cell_id_at_div(pmx_properties['cell_lineage'], divided_cell_id_pmx)

            if divided_cell_id_pmx is None:
                # Lineage of cell in pmx not continuous
                continue

            # Get embryos at delayed time (t + delay)
            # ref
            dict_barycenters_pm1_at_t_delayed = get_embryo_at_t(ref_properties['cell_barycenter'], t + delay)
            dict_names_pm1_at_t_delayed = get_embryo_at_t(ref_properties['cell_name'], t + delay)
            # pmx
            division_time_pmx = int(divided_cell_id_pmx / 10000)
            dict_barycenters_pmx_at_t_delayed = get_embryo_at_t(pmx_properties['cell_barycenter'],
                                                                division_time_pmx + delay)
            dict_names_pmx_at_t_delayed = get_embryo_at_t(pmx_properties['cell_name'], division_time_pmx + delay)

            # Get daughters' IDs at t + delay
            daughter1_id_pmx, daughter2_id_pmx = get_daughters_ids_at_tn(
                pmx_properties['cell_lineage'], divided_cell_id_pmx, delay
            )

            daughter1_id_pm1, daughter2_id_pm1 = get_daughters_ids_at_tn(
                ref_properties['cell_lineage'], divided_cell_id_pm1, delay
            )

            if daughter1_id_pmx is not None and daughter2_id_pmx is not None and daughter1_id_pm1 is not None and daughter2_id_pm1 is not None:
                if isinstance(daughter1_id_pmx, list):
                    daughter1_id_pmx = int(daughter1_id_pmx[0])
                if isinstance(daughter2_id_pmx, list):
                    daughter2_id_pmx = int(daughter2_id_pmx[0])
                if isinstance(daughter1_id_pm1, list):
                    daughter1_id_pm1 = int(daughter1_id_pm1[0])
                if isinstance(daughter2_id_pm1, list):
                    daughter2_id_pm1 = int(daughter2_id_pm1[0])

                # Get names of daughters at t + delay
                name_daughter_pmx_1 = dict_names_pmx_at_t_delayed.get(daughter1_id_pmx)
                name_daughter_pmx_2 = dict_names_pmx_at_t_delayed.get(daughter2_id_pmx)

                name_daughter_pm1_1 = dict_names_pm1_at_t_delayed.get(daughter1_id_pm1)
                name_daughter_pm1_2 = dict_names_pm1_at_t_delayed.get(daughter2_id_pm1)

                # Check if names of daughters in both embryos are the same
                if (name_daughter_pmx_1 == name_daughter_pm1_1 and name_daughter_pmx_2 == name_daughter_pm1_2) or \
                        (name_daughter_pmx_1 == name_daughter_pm1_2 and name_daughter_pmx_2 == name_daughter_pm1_1):
                    # Daughter names match in both embryos
                    # Process the matching daughters
                    _, _, q1, _ = get_cell_name_parts(name_daughter_pmx_1)

                    if q1 % 2 == 0:
                        # Swap the names and IDs
                        name_daughter_pmx_1, name_daughter_pmx_2 = name_daughter_pmx_2, name_daughter_pmx_1
                        daughter1_id_pmx, daughter2_id_pmx = daughter2_id_pmx, daughter1_id_pmx

                    _, gen, q1, _ = get_cell_name_parts(name_daughter_pm1_1)

                    if q1 % 2 == 0:
                        # Swap the names and IDs
                        name_daughter_pm1_1, name_daughter_pm1_2 = name_daughter_pm1_2, name_daughter_pm1_1
                        daughter1_id_pm1, daughter2_id_pm1 = daughter2_id_pm1, daughter1_id_pm1

                    ##
                    ### Apply local reg: to register pms daughter's barycenters
                    #
                    # get neighboorhoods of daughters of pmx
                    neighbouring_ids_pmx = list(pmx_properties['cell_contact_surface'][daughter1_id_pmx].keys())
                    neighbouring_ids_pmx.extend(list(pmx_properties['cell_contact_surface'][daughter2_id_pmx].keys()))
                    additional_ids = []
                    for i in neighbouring_ids_pmx:
                        if i%1000 != 1:  # not background
                            additional_ids.extend(
                                list(pmx_properties['cell_contact_surface'][i].keys()))
                    neighbouring_ids_pmx.extend(additional_ids)
                    neighbouring_ids_pmx = [x for x in neighbouring_ids_pmx if
                                            x not in [daughter1_id_pmx, daughter2_id_pmx]]

                    neighborhood_volumes_pmx = {i: v for i, v in pmx_properties['cell_volume'].items() if
                                                i in neighbouring_ids_pmx}
                    neighborhood_barycenters_pmx = {i: bary for i, bary in pmx_properties['cell_barycenter'].items() if
                                                    i in neighbouring_ids_pmx}
                    neighborhood_names_pmx = {i: name for i, name in pmx_properties['cell_name'].items() if
                                              i in neighbouring_ids_pmx}

                    # get neighbourhoods of daughters of pm1
                    neighbouring_ids_ref = list(ref_properties['cell_contact_surface'][daughter1_id_pm1].keys())
                    neighbouring_ids_ref.extend(list(ref_properties['cell_contact_surface'][daughter2_id_pm1].keys()))
                    additional_ids = []
                    for i in neighbouring_ids_ref:
                        if i%1000 != 1:  # not background
                            additional_ids.extend(
                                list(ref_properties['cell_contact_surface'][i].keys()))
                    neighbouring_ids_ref.extend(additional_ids)
                    neighbouring_ids_ref.append(daughter1_id_pm1)
                    neighbouring_ids_ref.append(daughter2_id_pm1)
                    neighbouring_ids_pmx = [x for x in neighbouring_ids_pmx if
                                            x not in [daughter1_id_pm1, daughter2_id_pm1]]

                    neighbouring_ids_ref = [i for i in neighbouring_ids_ref if i % 1000 != 1]
                    neighbouring_ids_pmx = [i for i in neighbouring_ids_pmx if i % 1000 != 1]
                    neighbouring_ids_ref = list(set(neighbouring_ids_ref))
                    neighbouring_ids_pmx = list(set(neighbouring_ids_pmx))

                    neighborhood_volumes_ref = {i: v for i, v in ref_properties['cell_volume'].items() if
                                                i in neighbouring_ids_ref}
                    neighborhood_barycenters_ref = {i: bary for i, bary in ref_properties['cell_barycenter'].items() if
                                                    i in neighbouring_ids_ref}
                    neighborhood_names_ref = {i: name for i, name in ref_properties['cell_name'].items() if
                                              i in neighbouring_ids_ref}

                    # most common ancestry:
                    reduced_barycenters_ref, reduced_barycenters_pmx, \
                        reduced_cell_name_ref, reduced_cell_name_pmx = reduce_to_closest_ancestry(
                        neighborhood_barycenters_ref, neighborhood_barycenters_pmx, neighborhood_volumes_ref,
                        neighborhood_volumes_pmx, neighborhood_names_ref, neighborhood_names_pmx)

                    # compute transform
                    #print(name_daughter_pmx_2)
                    transform, ref, floating, sorted_cell_name = local_register(list(reduced_barycenters_ref.values()),
                                                                                list(reduced_cell_name_ref.values()),
                                                                                list(reduced_barycenters_pmx.values()),
                                                                                list(reduced_cell_name_pmx.values())
                                                                                )
                    # add to floating the daughters in pmx: daughter2_id_pmx
                    floating = np.concatenate((floating, np.array(pmx_properties['cell_barycenter'][daughter1_id_pmx])[:, np.newaxis]), axis=1)
                    floating = np.concatenate((floating, np.array(pmx_properties['cell_barycenter'][daughter2_id_pmx])[:, np.newaxis]), axis=1)


                    # add to reduced_barycenters_pmx: name: barycenter
                    reduced_barycenters_pmx[daughter2_id_pmx] = pmx_properties['cell_barycenter'][daughter2_id_pmx]
                    reduced_barycenters_pmx[daughter1_id_pmx] = pmx_properties['cell_barycenter'][daughter1_id_pmx]

                    # apply transform on barycenters, ,
                    transformed_floating = apply_transformation(transform, floating)
                    registered_barycenters_pmx_at_t4 = update_barycenters(transformed_floating, floating,
                                                                          reduced_barycenters_pmx)

                    barycenter_daughter1_pmx = registered_barycenters_pmx_at_t4[daughter1_id_pmx]
                    barycenter_daughter2_pmx = registered_barycenters_pmx_at_t4[daughter2_id_pmx]
                    barycenter_daughter1_pm1 = ref_properties['cell_barycenter'][daughter1_id_pm1]
                    barycenter_daughter2_pm1 = ref_properties['cell_barycenter'][daughter2_id_pm1]

                    ##
                    ### store needed info
                    #
                    # Update divisions info
                    # Fill divisions_properties

                    # get mother name
                    mother_name = uname.get_mother_name(name_daughter_pmx_1)
                    cell_fate = pmx_properties['cell_fate'][divided_cell_id_pmx]
                    # division_time_pmx div time in pm1: t

                    # cell radius : mass is equal to volume (supposing cell is spherical), adjust radius computation
                    radius_pmx = np.cbrt(3 * pmx_properties['cell_volume'][divided_cell_id_pmx] / (4 * np.pi))
                    radius_pm1 = np.cbrt(3 * ref_properties['cell_volume'][divided_cell_id_pm1] / (4 * np.pi))

                    # normalised division direction
                    division_direction_pmx = barycenter_daughter2_pmx - barycenter_daughter1_pmx
                    normalized_division_direction_pmx = division_direction_pmx / np.linalg.norm(
                        division_direction_pmx)
                    division_direction_pm1 = barycenter_daughter2_pm1 - barycenter_daughter1_pm1
                    normalized_division_direction_pm1 = division_direction_pm1 / np.linalg.norm(
                        division_direction_pm1)

                    # if left cell apply symetry transform
                    is_left = mother_name[-1] == '_'

                    if mother_name[:-1] not in divisions_properties:
                        divisions_properties[mother_name[:-1]] = {
                            'division_directions_after_symmetry': {'left': {}, 'right': {}},
                            'division_directions_before_registration': {'left': {}, 'right': {}},
                            'division_barycenter_from_global_registration': {'left': {'first': {}, 'second': {}},
                                                                             'right': {'first': {}, 'second': {}}
                                                                             },
                            'division_volume_ratio': {'left': {}, 'right': {}},
                            'division_aq_time': {'left': {}, 'right': {}},
                            'all_div_aq_times': {'left': {}, 'right': {}},
                            'cell_radius': {'left': {}, 'right': {}},
                            'embryo_symmetry_plane_at_t': {},  # for each individual
                            'cell_fate': None,
                            'ids_in_ref': {},
                            'gen': None,
                            'daughters_fate': {'first_daughter': [], 'second_daughter': []},
                            # daughter1_id_pmx, daughter2_id_pmx

                            # ids in each individual
                            'cell_ids': {'left': {}, 'right': {}},
                            #  divided_cell_id_pmx, divided_cell_id_pm1

                            # division neighbors data: extracted from cell_contact_surface property
                            'neighbors': {'left': {}, 'right': {}},

                            # get cell_surface too
                            'cell_surface': {'left': {}, 'right': {}},

                            # cell principal values
                            'cell_principal_values': {'left': {}, 'right': {}},
                            # cell principal vectors
                            'cell_principal_vectors': {'left': {}, 'right': {}},

                            # sisters interfaces
                            'sisters_interface_vectors': {'left': {}, 'right': {}},
                            'sisters_surface_principal_values': {'left': {}, 'right': {}},

                            'apical_surface_principal_vectors': {'left': {}, 'right': {}},
                            'apical_surface_principal_values': {'left': {}, 'right': {}},

                            'sisters_interface_edges_vectors': {'left': {}, 'right': {}},
                            'sisters_surface_edges_principal_values': {'left': {}, 'right': {}},

                            'apical_surface_edges_principal_vectors': {'left': {}, 'right': {}},
                            'apical_surface_edges_principal_values': {'left': {}, 'right': {}},

                        }
                    # fill data
                    #########
                    ####
                    #########
                    # add vectors : interface between sisters, apical longest axis

                    sisters_interface_vectors_pmx = pmx_properties.get('sisters_interface_vectors', {}).get(
                        daughter1_id_pmx, None)
                    sisters_surface_principal_values_pmx = pmx_properties.get('sisters_surface_principal_values',
                                                                              {}).get(daughter1_id_pmx, None)
                    sisters_interface_edges_vectors_pmx = pmx_properties.get('sisters_interface_edges_vectors',
                                                                             {}).get(daughter1_id_pmx, None)
                    sisters_surface_edges_principal_values_pmx = pmx_properties.get(
                        'sisters_surface_edges_principal_values', {}).get(daughter1_id_pmx, None)

                    apical_surface_principal_vectors_pmx = pmx_properties.get('apical_surface_principal_vectors',
                                                                              {}).get(divided_cell_id_pmx, None)
                    apical_surface_principal_values_pmx = pmx_properties.get('apical_surface_principal_values',
                                                                             {}).get(divided_cell_id_pmx, None)
                    apical_surface_edges_principal_vectors_pmx = pmx_properties.get(
                        'apical_surface_edges_principal_vectors', {}).get(divided_cell_id_pmx, None)
                    apical_surface_edges_principal_values_pmx = pmx_properties.get(
                        'apical_surface_edges_principal_values', {}).get(divided_cell_id_pmx, None)

                    sisters_interface_vectors_ref = ref_properties.get('sisters_interface_vectors', {}).get(
                        daughter1_id_pm1, None)
                    sisters_surface_principal_values_ref = ref_properties.get('sisters_surface_principal_values',
                                                                              {}).get(daughter1_id_pm1, None)
                    sisters_interface_edges_vectors_ref = ref_properties.get('sisters_interface_edges_vectors',
                                                                             {}).get(daughter1_id_pm1, None)
                    sisters_surface_edges_principal_values_ref = ref_properties.get(
                        'sisters_surface_edges_principal_values', {}).get(daughter1_id_pm1, None)

                    apical_surface_principal_vectors_ref = ref_properties.get('apical_surface_principal_vectors',
                                                                              {}).get(divided_cell_id_pm1, None)
                    apical_surface_principal_values_ref = ref_properties.get('apical_surface_principal_values',
                                                                             {}).get(divided_cell_id_pm1, None)
                    apical_surface_edges_principal_vectors_ref = ref_properties.get(
                        'apical_surface_edges_principal_vectors', {}).get(divided_cell_id_pm1, None)
                    apical_surface_edges_principal_values_ref = ref_properties.get(
                        'apical_surface_edges_principal_values', {}).get(divided_cell_id_pm1, None)

                    ###########
                    ####
                    ###########

                    # register cell pca vectors in pmx, shape needs to be (3,N)
                    registered_pcas = np.array(pmx_properties['cell_principal_vectors'][divided_cell_id_pmx])
                    registered_pcas = registered_pcas.T  # reshape to have 3,N shape
                    # apply registration
                    registered_pcas = apply_transformation(transform, registered_pcas, scale=False)
                    registered_pcas = registered_pcas.T  # shape to N,3

                    # add fates
                    divisions_properties[mother_name[:-1]]['daughters_fate']['first_daughter'] = \
                        pmx_properties['cell_fate'][daughter1_id_pmx]
                    divisions_properties[mother_name[:-1]]['daughters_fate']['second_daughter'] = \
                        pmx_properties['cell_fate'][daughter2_id_pmx]

                    divisions_properties[mother_name[:-1]]['cell_fate'] = cell_fate
                    divisions_properties[mother_name[:-1]]['gen'] = gen - 1

                    # following has dict :{neighbor_id: contact_surface} => {neighbor_name: contact_surface}
                    cell_neighbor_dict_in_pmx = pmx_properties['cell_contact_surface'][divided_cell_id_pmx]
                    cell_neighbor_dict_in_ref = ref_properties['cell_contact_surface'][divided_cell_id_pm1]

                    #########################################

                    neighbor_name_surface_ref = {}
                    neighbor_name_surface_pmx = {}

                    cell_surface_ref = ref_properties['cell_surface'][divided_cell_id_pm1]
                    # Iterate over the neighbor IDs and contact surfaces in the ref/pmx properties
                    for neighbor_id, contact_surface in cell_neighbor_dict_in_ref.items():
                        if contact_surface >= cell_surface_ref * 5 / 100:
                            neighbor_name = ref_properties['cell_name'].get(neighbor_id,
                                                                            f"Unknown Neighbor {neighbor_id}")
                            neighbor_name_surface_ref[neighbor_name] = contact_surface

                    cell_surface_pmx = pmx_properties['cell_surface'][divided_cell_id_pmx]
                    for neighbor_id, contact_surface in cell_neighbor_dict_in_pmx.items():
                        if contact_surface >= cell_surface_pmx * 5 / 100:
                            neighbor_name = pmx_properties['cell_name'].get(neighbor_id,
                                                                            f"Unknown Neighbor {neighbor_id}")
                            neighbor_name_surface_pmx[neighbor_name] = contact_surface

                    if is_left:
                        #
                        ####
                        ### add if and adapt to dir type
                        # get axis of symmetry estimation at frame

                        sym_vector_ref = estimate_sym_vector(dict_barycenters_pm1_at_t_delayed,
                                                             dict_names_pm1_at_t_delayed,
                                                             ref_properties['cell_volume'])
                        # Do the symmetry transform of left cells
                        barycenter_daughter2_pm1 = barycenter_after_symmetry(barycenter_daughter1_pm1,
                                                                             barycenter_daughter2_pm1,
                                                                             sym_vector_ref)

                        sym_vector_pmx = estimate_sym_vector(dict_barycenters_pmx_at_t_delayed,
                                                             dict_names_pmx_at_t_delayed,
                                                             pmx_properties['cell_volume'])
                        # register sym vector (only rotation)
                        sym_vector_pmx = np.array(sym_vector_pmx).T  # reshape to have 3,N shape
                        sym_vector_pmx = np.reshape(sym_vector_pmx, (3,1))
                        # apply registration
                        sym_vector_pmx = apply_transformation(transform, sym_vector_pmx, scale=False)
                        sym_vector_pmx = sym_vector_pmx.T  # shape to N,3
                        sym_vector_pmx = sym_vector_pmx[0]
                        # Do the symmetry transform of left cells
                        barycenter_daughter2_pmx = barycenter_after_symmetry(barycenter_daughter1_pmx,
                                                                             barycenter_daughter2_pmx,
                                                                             sym_vector_pmx)

                        division_direction_pmx = barycenter_daughter2_pmx - barycenter_daughter1_pmx
                        division_direction_pm1 = barycenter_daughter2_pm1 - barycenter_daughter1_pm1
                        if direction_type != 'bary':
                            # if no interface between sisters: continue
                            if sisters_interface_vectors_pmx is None or sisters_interface_vectors_ref is None:
                                continue

                            # we know how to register a vecto
                            # sisters_interface_vectors_ref
                            ###
                            registered_v3_pmx = np.array(sisters_interface_vectors_pmx)
                            registered_v3_pmx = registered_v3_pmx.T  # reshape to have 3,N shape
                            registered_v3_pmx = apply_transformation(transform, registered_v3_pmx, scale=False)
                            registered_v3_pmx = registered_v3_pmx.T  # shape to N,3

                            division_direction_pm1_i = apply_symetry_on_vector(sym_vector_ref,
                                                                               sisters_interface_vectors_ref[2])

                            dot_product = np.dot(division_direction_pm1_i, division_direction_pm1)
                            if dot_product < 0:
                                division_direction_pm1 = -division_direction_pm1_i
                            else:
                                division_direction_pm1 = division_direction_pm1_i

                            division_direction_pmx_i = apply_symetry_on_vector(sym_vector_pmx, registered_v3_pmx[2])
                            dot_product = np.dot(division_direction_pmx_i, division_direction_pmx)
                            if dot_product < 0:
                                division_direction_pmx = -division_direction_pmx_i
                            else:
                                division_direction_pmx = division_direction_pmx_i

                        normalized_division_direction_pmx = division_direction_pmx / np.linalg.norm(
                            division_direction_pmx)
                        normalized_division_direction_pm1 = division_direction_pm1 / np.linalg.norm(
                            division_direction_pm1)

                        divisions_properties[mother_name[:-1]]['division_directions_after_symmetry']['left'][
                            pmx_name] = normalized_division_direction_pmx
                        divisions_properties[mother_name[:-1]]['division_directions_after_symmetry']['left'][
                            ref_name] = normalized_division_direction_pm1

                        divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                            'left']['first'][
                            pmx_name] = barycenter_daughter1_pmx
                        divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                            'left']['second'][
                            pmx_name] = barycenter_daughter2_pmx

                        divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                            'left']['first'][
                            ref_name] = barycenter_daughter1_pm1
                        divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                            'left']['second'][
                            ref_name] = barycenter_daughter2_pm1

                        divisions_properties[mother_name[:-1]]['division_volume_ratio']['left'][
                            pmx_name] = pmx_properties['cell_volume'][daughter2_id_pmx] / \
                                        pmx_properties['cell_volume'][divided_cell_id_pmx]
                        divisions_properties[mother_name[:-1]]['division_volume_ratio']['left'][ref_name] = (
                                ref_properties['cell_volume'][daughter2_id_pm1] / ref_properties['cell_volume'][
                            divided_cell_id_pm1])

                        divisions_properties[mother_name[:-1]]['division_aq_time']['left'][
                            pmx_name] = division_time_pmx
                        divisions_properties[mother_name[:-1]]['division_aq_time']['left'][ref_name] = t

                        divisions_properties[mother_name[:-1]]['cell_radius']['left'][pmx_name] = radius_pmx
                        divisions_properties[mother_name[:-1]]['cell_radius']['left'][ref_name] = radius_pm1

                        divisions_properties[mother_name[:-1]]['ids_in_ref']['left'] = divided_cell_id_pm1

                        divisions_properties[mother_name[:-1]]['embryo_symmetry_plane_at_t'][
                            ref_name] = sym_vector_ref
                        divisions_properties[mother_name[:-1]]['embryo_symmetry_plane_at_t'][
                            pmx_name] = sym_vector_pmx
                        # add id in ref and other embryo
                        divisions_properties[mother_name[:-1]]['cell_ids']['left'][ref_name] = divided_cell_id_pm1
                        divisions_properties[mother_name[:-1]]['cell_ids']['left'][pmx_name] = divided_cell_id_pmx

                        # add neighbors data in each individual
                        divisions_properties[mother_name[:-1]]['neighbors']['left'][ref_name] = \
                            neighbor_name_surface_ref
                        divisions_properties[mother_name[:-1]]['neighbors']['left'][pmx_name] = \
                            neighbor_name_surface_pmx

                        divisions_properties[mother_name[:-1]]['cell_surface']['left'][ref_name] = \
                            cell_surface_ref
                        divisions_properties[mother_name[:-1]]['cell_surface']['left'][pmx_name] = \
                            cell_surface_pmx

                        # add cell principal value and vectors
                        # apply symmetry
                        ###
                        v1 = registered_pcas
                        registered_pcas_pmx = apply_symetry_on_vector(sym_vector_pmx, v1)
                        v2 = np.array(ref_properties['cell_principal_vectors'][divided_cell_id_pm1])
                        registered_pcas_ref = apply_symetry_on_vector(sym_vector_ref, v2)
                        ####
                        ###
                        #
                        divisions_properties[mother_name[:-1]]['cell_principal_vectors']['left'][ref_name] \
                            = registered_pcas_ref
                        divisions_properties[mother_name[:-1]]['cell_principal_vectors']['left'][
                            pmx_name] = registered_pcas_pmx

                        divisions_properties[mother_name[:-1]]['cell_principal_values']['left'][ref_name] \
                            = ref_properties['cell_principal_values'][divided_cell_id_pm1]
                        divisions_properties[mother_name[:-1]]['cell_principal_values']['left'][
                            pmx_name] = pmx_properties['cell_principal_values'][divided_cell_id_pmx]

                        # add 'cell_vector_normal_to_sister_interface: : without registration nor symetry
                        divisions_properties[mother_name[:-1]]['sisters_interface_vectors']['left'][
                            ref_name] = sisters_interface_vectors_ref
                        divisions_properties[mother_name[:-1]]['sisters_interface_vectors']['left'][
                            pmx_name] = sisters_interface_vectors_pmx

                        divisions_properties[mother_name[:-1]]['sisters_surface_principal_values']['left'][
                            ref_name] = sisters_surface_principal_values_ref
                        divisions_properties[mother_name[:-1]]['sisters_surface_principal_values']['left'][
                            pmx_name] = sisters_surface_principal_values_pmx

                        divisions_properties[mother_name[:-1]]['sisters_interface_edges_vectors']['left'][
                            ref_name] = sisters_interface_edges_vectors_ref
                        divisions_properties[mother_name[:-1]]['sisters_interface_edges_vectors']['left'][
                            pmx_name] = sisters_interface_edges_vectors_pmx

                        divisions_properties[mother_name[:-1]]['sisters_surface_edges_principal_values']['left'][
                            ref_name] = sisters_surface_edges_principal_values_ref
                        divisions_properties[mother_name[:-1]]['sisters_surface_edges_principal_values']['left'][
                            pmx_name] = sisters_surface_edges_principal_values_pmx

                        # add cell apical surface vector : without registration nor symetry
                        divisions_properties[mother_name[:-1]]['apical_surface_principal_vectors']['left'][
                            ref_name] = apical_surface_principal_vectors_ref
                        divisions_properties[mother_name[:-1]]['apical_surface_principal_vectors']['left'][
                            pmx_name] = apical_surface_principal_vectors_pmx

                        divisions_properties[mother_name[:-1]]['apical_surface_principal_values']['left'][
                            ref_name] = apical_surface_principal_values_ref
                        divisions_properties[mother_name[:-1]]['apical_surface_principal_values']['left'][
                            pmx_name] = apical_surface_principal_values_pmx

                        divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_vectors']['left'][
                            ref_name] = apical_surface_edges_principal_vectors_ref
                        divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_vectors']['left'][
                            pmx_name] = apical_surface_edges_principal_vectors_pmx

                        divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_values']['left'][
                            ref_name] = apical_surface_edges_principal_values_ref
                        divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_values']['left'][
                            pmx_name] = apical_surface_edges_principal_values_pmx

                        # add div direction without registration nor symetry
                        divisions_properties[mother_name[:-1]]['division_directions_before_registration']['left'][
                            ref_name] = \
                            ref_properties['cell_barycenter'][daughter2_id_pm1] - ref_properties['cell_barycenter'][
                                daughter1_id_pm1]
                        divisions_properties[mother_name[:-1]]['division_directions_before_registration']['left'][
                            pmx_name] = \
                            pmx_properties['cell_barycenter'][daughter2_id_pmx] - pmx_properties['cell_barycenter'][
                                daughter1_id_pmx]

                    else:
                        #######
                        ###
                        ####
                        if direction_type != 'bary':
                            # if no interface between sisters: continue
                            if sisters_interface_vectors_pmx is None or sisters_interface_vectors_ref is None:
                                continue

                            # we know how to register a vecto
                            # sisters_interface_vectors_ref
                            ###
                            registered_v3_pmx = np.array(sisters_interface_vectors_pmx)
                            registered_v3_pmx = registered_v3_pmx.T  # reshape to have 3,N shape
                            registered_v3_pmx = apply_transformation(transform, registered_v3_pmx, scale=False)
                            registered_v3_pmx = registered_v3_pmx.T  # shape to N,3

                            division_direction_pm1_i = np.array(sisters_interface_vectors_ref[2])
                            dot_product = np.dot(division_direction_pm1_i, division_direction_pm1)
                            if dot_product < 0:
                                division_direction_pm1 = -division_direction_pm1_i
                            else:
                                division_direction_pm1 = division_direction_pm1_i

                            division_direction_pmx_i = np.array(registered_v3_pmx[2])
                            dot_product = np.dot(division_direction_pmx_i, division_direction_pmx)
                            if dot_product < 0:
                                division_direction_pmx = -division_direction_pmx_i
                            else:
                                division_direction_pmx = division_direction_pmx_i

                        normalized_division_direction_pmx = division_direction_pmx / np.linalg.norm(
                            division_direction_pmx)
                        normalized_division_direction_pm1 = division_direction_pm1 / np.linalg.norm(
                            division_direction_pm1)

                        ####
                        #######
                        ####

                        divisions_properties[mother_name[:-1]]['division_directions_after_symmetry']['right'][
                            pmx_name] = normalized_division_direction_pmx
                        divisions_properties[mother_name[:-1]]['division_directions_after_symmetry']['right'][
                            ref_name] = normalized_division_direction_pm1

                        divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                            'right']['first'][
                            pmx_name] = barycenter_daughter1_pmx
                        divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                            'right']['second'][
                            pmx_name] = barycenter_daughter2_pmx

                        divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                            'right']['first'][
                            ref_name] = barycenter_daughter1_pm1
                        divisions_properties[mother_name[:-1]]['division_barycenter_from_global_registration'][
                            'right']['second'][
                            ref_name] = barycenter_daughter2_pm1

                        divisions_properties[mother_name[:-1]]['division_volume_ratio']['right'][
                            pmx_name] = pmx_properties['cell_volume'][daughter2_id_pmx] / \
                                        pmx_properties['cell_volume'][divided_cell_id_pmx]
                        divisions_properties[mother_name[:-1]]['division_volume_ratio']['right'][ref_name] = (
                                ref_properties['cell_volume'][daughter2_id_pm1] / ref_properties['cell_volume'][
                            divided_cell_id_pm1])

                        divisions_properties[mother_name[:-1]]['division_aq_time']['right'][
                            pmx_name] = division_time_pmx
                        divisions_properties[mother_name[:-1]]['division_aq_time']['right'][ref_name] = t

                        divisions_properties[mother_name[:-1]]['ids_in_ref']['right'] = divided_cell_id_pm1

                        divisions_properties[mother_name[:-1]]['cell_radius']['right'][pmx_name] = radius_pmx
                        divisions_properties[mother_name[:-1]]['cell_radius']['right'][ref_name] = radius_pm1

                        # add id in ref and other embryo
                        divisions_properties[mother_name[:-1]]['cell_ids']['right'][ref_name] = divided_cell_id_pm1
                        divisions_properties[mother_name[:-1]]['cell_ids']['right'][pmx_name] = divided_cell_id_pmx

                        # add neighbors data in each individual
                        divisions_properties[mother_name[:-1]]['neighbors']['right'][ref_name] = \
                            neighbor_name_surface_ref
                        divisions_properties[mother_name[:-1]]['neighbors']['right'][pmx_name] = \
                            neighbor_name_surface_pmx

                        divisions_properties[mother_name[:-1]]['cell_surface']['right'][ref_name] = \
                            cell_surface_ref
                        divisions_properties[mother_name[:-1]]['cell_surface']['right'][pmx_name] = \
                            cell_surface_pmx

                        # add cell principal value and vectors

                        divisions_properties[mother_name[:-1]]['cell_principal_vectors']['right'][ref_name] \
                            = np.array(ref_properties['cell_principal_vectors'][divided_cell_id_pm1])
                        divisions_properties[mother_name[:-1]]['cell_principal_vectors']['right'][
                            pmx_name] = registered_pcas

                        divisions_properties[mother_name[:-1]]['cell_principal_values']['right'][ref_name] \
                            = ref_properties['cell_principal_values'][divided_cell_id_pm1]
                        divisions_properties[mother_name[:-1]]['cell_principal_values']['right'][
                            pmx_name] = pmx_properties['cell_principal_values'][divided_cell_id_pmx]

                        ##
                        # add 'cell_vector_normal_to_sister_interface: : without registration nor symetry
                        divisions_properties[mother_name[:-1]]['sisters_interface_vectors']['right'][
                            ref_name] = sisters_interface_vectors_ref
                        divisions_properties[mother_name[:-1]]['sisters_interface_vectors']['right'][
                            pmx_name] = sisters_interface_vectors_pmx

                        divisions_properties[mother_name[:-1]]['sisters_surface_principal_values']['right'][
                            ref_name] = sisters_surface_principal_values_ref
                        divisions_properties[mother_name[:-1]]['sisters_surface_principal_values']['right'][
                            pmx_name] = sisters_surface_principal_values_pmx

                        divisions_properties[mother_name[:-1]]['sisters_interface_edges_vectors']['right'][
                            ref_name] = sisters_interface_edges_vectors_ref
                        divisions_properties[mother_name[:-1]]['sisters_interface_edges_vectors']['right'][
                            pmx_name] = sisters_interface_edges_vectors_pmx

                        divisions_properties[mother_name[:-1]]['sisters_surface_edges_principal_values']['right'][
                            ref_name] = sisters_surface_edges_principal_values_ref
                        divisions_properties[mother_name[:-1]]['sisters_surface_edges_principal_values']['right'][
                            pmx_name] = sisters_surface_edges_principal_values_pmx

                        # add cell apical surface vector : without registration nor symetry
                        divisions_properties[mother_name[:-1]]['apical_surface_principal_vectors']['right'][
                            ref_name] = apical_surface_principal_vectors_ref
                        divisions_properties[mother_name[:-1]]['apical_surface_principal_vectors']['right'][
                            pmx_name] = apical_surface_principal_vectors_pmx

                        divisions_properties[mother_name[:-1]]['apical_surface_principal_values']['right'][
                            ref_name] = apical_surface_principal_values_ref
                        divisions_properties[mother_name[:-1]]['apical_surface_principal_values']['right'][
                            pmx_name] = apical_surface_principal_values_pmx

                        divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_vectors']['right'][
                            ref_name] = apical_surface_edges_principal_vectors_ref
                        divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_vectors']['right'][
                            pmx_name] = apical_surface_edges_principal_vectors_pmx

                        divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_values']['right'][
                            ref_name] = apical_surface_edges_principal_values_ref
                        divisions_properties[mother_name[:-1]]['apical_surface_edges_principal_values']['right'][
                            pmx_name] = apical_surface_edges_principal_values_pmx

                        # add division direction before registration
                        divisions_properties[mother_name[:-1]]['division_directions_before_registration']['right'][
                            ref_name] = ref_properties['cell_barycenter'][
                                            ref_properties['cell_lineage'][divided_cell_id_pm1][1]] - \
                                        ref_properties['cell_barycenter'][
                                            ref_properties['cell_lineage'][divided_cell_id_pm1][0]]
                        divisions_properties[mother_name[:-1]]['division_directions_before_registration']['right'][
                            pmx_name] = pmx_properties['cell_barycenter'][
                                            pmx_properties['cell_lineage'][divided_cell_id_pmx][1]] - \
                                        pmx_properties['cell_barycenter'][
                                            pmx_properties['cell_lineage'][divided_cell_id_pmx][0]]


                else:

                    print("Error with cells from pmx/pm1. Skipping this iteration.")

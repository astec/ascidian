import pickle as pkl
import os
import numpy as np
from sklearn.linear_model import RANSACRegressor
import random

from ascidian.algorithms.division_analysis_init import get_temporal_alignment_coefficients_of_embryos
import ascidian.core.temporal_alignment as utimes
from ascidian.division_analysis.division_analysis_init_tools import get_embryo_at_t, apply_transformation
import ascidian.ascidian.name as uname
from ascidian.division_analysis.registration_assessment_tools import compute_similitude
from ascidian.division_analysis.division_analysis_tools import determine_fate_group

#########################
######################
def write_tissues_eigenspace_stability_consecutive_timepoints_overtime(tissue_variances, N, output_folder):
    """
    Writes Python scripts that generate plots of eigenspace stability over consecutive timepoints for each tissue.
    The plot includes:
    - Angles between the first N eigenvectors of consecutive timepoints for each tissue, all on the same plot.
    - Variance (eigenvalue) associated with each eigenvector at each timepoint, plotted on the second y-axis.

    Parameters
    ----------
    tissue_variances : dict
        Dictionary where each tissue contains eigenvalues, eigenvectors, and cell counts for different timepoints.
    N : int
        Number of eigenvectors to consider for comparison (default 18).
    output_folder : str
        Directory to save the output script.

    Returns
    -------
    None
    """
    output_dir = os.path.join(output_folder, "tissue_eigenspace_stability")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for tissue, time_data in tissue_variances.items():
        local_filename = os.path.join(output_dir, f"plot_eigenspace_stability_{tissue}.py")

        with open(local_filename, "w") as f:
            # Write imports
            f.write("import numpy as np\n")
            f.write("import matplotlib.pyplot as plt\n\n")

            # Define number of eigenvectors
            f.write(f"N = {N}\n")

            # Write the tissue-specific data
            # Ensure that the dictionary is serialized properly
            # Using repr to get a string representation of the dictionary
            f.write(f"tissue_variances = {{'{tissue}': {repr(time_data)}}}\n\n")

            # Define a list of colors for eigenvectors
            # Extend this list if N > 10 by repeating or adding more colors
            colors = [
                'tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
                'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan',
                'tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
                'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan'
            ]
            # Write the color list to the script
            f.write(f"colors = {colors}\n\n")

            # Function to calculate the angle between two vectors
            f.write("""
def calculate_angle_between_vectors(v1, v2):
        cos_theta = np.abs(np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2)))
        return np.arccos(np.clip(cos_theta, -1.0, 1.0)) * (180.0 / np.pi)
    \n""")

            # Initialize figure for the tissue with two y-axes
            f.write(f"fig, ax1 = plt.subplots(figsize=(12, 8))\n")
            f.write(f"plt.title('Eigenspace Stability and Eigenvalue Variance over Time for Tissue: {tissue}')\n")
            f.write("ax1.set_xlabel('Developmental Time')\n")
            f.write("ax1.set_ylabel('Angle (Degrees)', color='tab:blue')\n")
            f.write("ax1.tick_params(axis='y', labelcolor='tab:blue')\n\n")

            # Create second y-axis for the eigenvalues
            f.write("ax2 = ax1.twinx()\n")
            f.write("ax2.set_ylabel('Eigenvalue', color='tab:red')\n")
            f.write("ax2.tick_params(axis='y', labelcolor='tab:red')\n\n")

            # Extract sorted timepoints
            f.write("timepoints = sorted(tissue_variances['" + tissue + "'].keys())\n\n")

            # Start plotting for each eigenvector
            f.write("for i in range(N):\n")
            f.write("    color = colors[i % len(colors)]  # Assign color cyclically if N > len(colors)\n")
            f.write("    angles = []\n")
            f.write("    eigenvalues = []\n")
            f.write("    consecutive_timepoints = []\n\n")

            # Calculate angles and collect eigenvalues
            f.write("    for t1, t2 in zip(timepoints[:-1], timepoints[1:]):\n")
            f.write("        evectors_t1 = tissue_variances['" + tissue + "'][t1]['eigenvectors']\n")
            f.write("        evectors_t2 = tissue_variances['" + tissue + "'][t2]['eigenvectors']\n")
            f.write("        eigenvalue_t1 = tissue_variances['" + tissue + "'][t1]['eigenvalues']\n\n")

            f.write("        evectors_t1 = np.array(evectors_t1)\n")
            f.write("        evectors_t2 = np.array(evectors_t2)\n")
            f.write("        if evectors_t1.shape[1] != evectors_t2.shape[1]:\n")
            f.write("            continue  # Skip if the number of eigenvectors does not match\n\n")

            f.write("        if len(evectors_t1) > i and len(evectors_t2) > i:\n")
            f.write("            angle = calculate_angle_between_vectors(evectors_t1[:, i], evectors_t2[:, i])\n")
            f.write("            angles.append(angle)\n")
            f.write("            eigenvalues.append(eigenvalue_t1[i])  # Eigenvalue of t1 for eigenvector i\n")
            f.write("            consecutive_timepoints.append(t1)\n\n")

            # Plot angles on ax1
            f.write("    ax1.plot(consecutive_timepoints, angles, marker='o', linestyle='-', "
                    "label=f'Eigenvector {i + 1} Angle', color=color)\n")

            # Plot eigenvalues on ax2
            f.write("    ax2.plot(consecutive_timepoints, eigenvalues, marker='x', linestyle='--', "
                    "label=f'Eigenvector {i + 1} Eigenvalue', color=color, alpha=0.7)\n\n")

            # Add legends
            f.write("lines_labels = [ax.get_legend_handles_labels() for ax in [ax1, ax2]]\n")
            f.write("lines, labels = [sum(lol, []) for lol in zip(*lines_labels)]\n")
            f.write("unique_labels = dict(zip(labels, lines))\n")
            f.write(
                "ax1.legend(unique_labels.values(), unique_labels.keys(), loc='upper left', bbox_to_anchor=(1.05, 1))\n\n")

            # Adjust layout and save the plot
            f.write("fig.tight_layout()\n")
            f.write(f"# plt.savefig('eigenspace_stability_and_variance_{tissue}.png', dpi=300)\n")
            f.write("plt.show()\n")
            f.write("plt.close()\n")

        print(f"Plot script written to: {local_filename}")

def write_variance_plot_script(eigenvalues_dict, dimension_sizes, outputfolder, suffix=None):
    """
    Writes a Python script that calculates and plots:
    1. Total eigenvalue variance (sum) at each timepoint.
    2. The variance normalized by the number of dimensions.
    3. The number of dimensions at each timepoint.

    Parameters
    ----------
    eigenvalues_dict : dict
        Dictionary where keys are timepoints and values are arrays of eigenvalues at each timepoint.
    dimension_sizes : list of int
        List of the number of dimensions to consider in the analysis for each timepoint.
    outputfolder : str
        Directory to save the output script.
    title : str, optional
        Title of the plot.

    Returns
    -------
    None
    """

    # Create output directory if it doesn't exist
    output_dir = os.path.join(outputfolder, "variance_plot")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Script file name
    if not suffix:
        local_filename = os.path.join(output_dir, "plot_variance.py")
    else:
        local_filename = os.path.join(output_dir, f"plot_variance_{suffix}.py")

    with open(local_filename, "w") as f:
        # Write imports
        f.write("import numpy as np\n")
        f.write("import matplotlib.pyplot as plt\n\n")

        # Write eigenvalues dictionary and dimension sizes
        f.write(f"eigenvalues_dict = {eigenvalues_dict}\n\n")
        f.write(f"dimension_sizes = {dimension_sizes}\n\n")

        # Compute sum of eigenvalues (total variance) for each timepoint
        f.write("total_variances = {timepoint: np.sum(eigenvalues) for timepoint, eigenvalues in eigenvalues_dict.items()}\n")
        f.write("timepoints = list(total_variances.keys())\n")
        f.write("total_variance_values = list(total_variances.values())\n\n")

        # Compute variance normalized by dimension size
        f.write("normalized_variances = [var / dimension_sizes[i] for i, var in enumerate(total_variance_values)]\n\n")

        # Plot setup
        f.write("# Plot total variance, normalized variance over timepoints, and dimension size\n")
        f.write("fig, ax1 = plt.subplots(figsize=(12, 8))\n")

        # Primary y-axis for total variance and normalized variance
        f.write("color = 'tab:blue'\n")
        f.write("ax1.set_xlabel('Timepoint')\n")
        f.write("ax1.set_ylabel('Variance', color=color)\n")
        f.write("ax1.plot(timepoints, total_variance_values, marker='o', linestyle='-', color='tab:blue', label='Total Variance')\n")
        f.write("ax1.plot(timepoints, normalized_variances, marker='x', linestyle='--', color='tab:green', label='Normalized Variance')\n")
        f.write("ax1.tick_params(axis='y', labelcolor=color)\n")
        f.write("ax1.legend(loc='upper left')\n")

        # Secondary y-axis for number of dimensions
        f.write("ax2 = ax1.twinx()\n")
        f.write("color = 'tab:red'\n")
        f.write("ax2.set_ylabel('Number of Dimensions', color=color)\n")
        f.write("ax2.plot(timepoints, dimension_sizes, marker='s', linestyle='-', color=color, label='Number of Dimensions')\n")
        f.write("ax2.tick_params(axis='y', labelcolor=color)\n")
        f.write("ax2.legend(loc='upper right')\n")

        # Set the title
        f.write("plt.title('Variance and Number of Dimensions Over Timepoints')\n")

        f.write("fig.tight_layout()\n")

        # Save and show the plot
        file = os.path.join(output_dir, "VariancePlot.png")
        f.write(f"#plt.savefig('{file}', dpi=300)\n")
        f.write("plt.show()\n")

    print(f"Plot script written to: {local_filename}")

def write_variance_plot_script_at_t(eigenvalues, outputfolder, time, suffix=None):
    """
    Writes a Python script that plots the percentage of variance explained by each
    eigenvalue as a function of the number of dimensions.

    Parameters
    ----------
    eigenvalues : np.ndarray
        Array of eigenvalues.
    outputfolder : str
        Directory to save the output script.
    nb_cells : int
        Number of cells in the analysis.
    timepoint : str
        Developmental timepoint to include in the plot title.

    Returns
    -------
    None
    """

    # Create output directory if it doesn't exist
    output_dir = os.path.join(outputfolder, "variance_explained_plot")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Script file name
    if suffix is None:
        local_filename = os.path.join(output_dir, f"variance_explained_plot_at_{time}.py")
    else:
        local_filename = os.path.join(output_dir, f"variance_explained_plot_at_{time}_{suffix}.py")

    with open(local_filename, "w") as f:
        # Write imports
        f.write("import numpy as np\n")
        f.write("import matplotlib.pyplot as plt\n\n")

        # Write eigenvalues and compute variance ratios
        f.write(f"eigenvalues = np.array({eigenvalues.tolist()})\n\n")
        f.write("total_variance = np.sum(eigenvalues)\n")
        f.write("variance_ratios = eigenvalues / total_variance\n")
        f.write("cumulative_variance = np.cumsum(variance_ratios)\n\n")

        # Plot setup
        f.write("# Plot percentage of variance explained by each dimension\n")
        f.write("plt.figure(figsize=(10, 6))\n")
        f.write(
            "plt.plot(np.arange(1, len(variance_ratios) + 1), variance_ratios * 100, marker='o', label='Variance per dimension')\n")
        f.write(
            "plt.plot(np.arange(1, len(variance_ratios) + 1), cumulative_variance * 100, marker='x', linestyle='--', label='Cumulative variance')\n")

        # Plot labels and title
        f.write("plt.xlabel('Number of Dimensions')\n")
        f.write("plt.ylabel('Percentage of Variance Explained (%)')\n")
        f.write(f"plt.title('Variance Explained by Eigenvalues at {time}')\n")
        f.write("plt.grid(True)\n")
        f.write("plt.legend()\n")
        f.write("plt.tight_layout()\n")

        # Save and show the plot
        file = os.path.join(output_dir, f"VarianceExplained_{time}.png")
        f.write(f"#plt.savefig('{file}', dpi=300)\n")
        f.write("plt.show()\n")


def write_pca_projection_script(projections, variance_ratios, developmental_time, embryo_res, outputfolder):
    """
    Writes a Python script that plots the projection of embryos onto the subspace defined by
    the first two principal components.

    Parameters
    ----------
    projections : dict
        A dictionary containing the projections of each embryo onto the plane defined by the first two
        principal components (eigenvectors). The keys are the embryo names.
    variance_ratios : np.ndarray
        A 2-element array containing the percentage of variance explained by each of the first two eigenvectors.
    embryo_names : list
        List of embryo names corresponding to the projections.
    developmental_time : int or str
        The developmental time point of the analysis to include in the plot title.
    outputfolder : str
        Directory where the script will be saved.

    Returns
    -------
    None
    """
    output_dir = os.path.join(outputfolder, f"pca_projection_btw_embryos")
    if embryo_res == 'halfs':
        output_dir = os.path.join(output_dir, f"pca_projection_btw_halfs_embryos")
    local_filename = os.path.join(output_dir, f"pca_projection_at_{developmental_time}.py")

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    projections_as_lists = {k: v.tolist() if isinstance(v, np.ndarray) else v for k, v in projections.items()}

    with open(local_filename, "w") as f:
        f.write("import numpy as np\n")
        f.write("import matplotlib.pyplot as plt\n")
        f.write("import matplotlib.colors as mcolors\n\n")

        f.write(f"# Projections of embryos on the first two principal components\n")
        f.write(f"projections = {projections_as_lists}\n\n")

        f.write(f"# Variance ratios of the first two principal components\n")
        f.write(f"variance_ratios = {variance_ratios.tolist()}\n\n")

        f.write(f"# Embryo names\n")
        f.write(f"embryo_names = list(projections.keys())")

        # Assign colors to symmetrical counterparts
        f.write("# Assign colors to symmetrical counterparts\n")
        f.write("colors = {}\n")
        f.write("base_colors = list(mcolors.TABLEAU_COLORS.values())\n")
        f.write("for name in embryo_names:\n")
        f.write("    base_name = name.replace('_left', '').replace('_right', '').replace('_random','')\n")
        f.write("    if base_name not in colors:\n")
        f.write("        colors[base_name] = base_colors[len(colors) % len(base_colors)]\n\n")

        f.write("# Plot the projections\n")
        f.write("fig, ax = plt.subplots(figsize=(10, 8))\n\n")

        f.write("# Plot each embryo\n")
        f.write("for name in embryo_names:\n")
        f.write("    proj = projections[name]\n")
        f.write("    base_name = name.replace('_left', '').replace('_right', '')\n")
        f.write("    ax.scatter(proj[0], proj[1], s=100, color=colors[base_name], label=base_name if base_name not in ax.get_legend_handles_labels()[1] else '')\n")
        f.write("    ax.annotate(name, (proj[0], proj[1]), fontsize=12, ha='right')\n\n")

        f.write("ax.set_xlabel(f'PC1 (Variance: {variance_ratios[0] * 100:.2f}%)')\n")
        f.write("ax.set_ylabel(f'PC2 (Variance: {variance_ratios[1] * 100:.2f}%)')\n")

        f.write(f"ax.set_title('PCA Projection at Developmental Time {developmental_time}\\n"
                f"Variance Explained by PC1 and PC2: {variance_ratios[0] * 100:.2f}%, "
                f"{variance_ratios[1] * 100:.2f}%')\n\n")

        f.write("plt.grid(True)\n")
        f.write("plt.legend()\n")
        f.write("plt.tight_layout()\n")

        file = f"pca_projection_time_{developmental_time}.png"
        file = os.path.join(outputfolder, file)
        f.write(f"#plt.savefig('{file}', dpi=300)\n")
        f.write("plt.show()\n")

    print(f"Script written to {local_filename}")
def compute_projections_and_variance(eigenvectors, eigenvalues, dict_embryos_in_same_ref_at_t, avg_embryo):
    """
    Computes the projections of the average embryo and all individuals onto the plane
    defined by the first two eigenvectors. Also computes the variance ratios explained
    by the first two eigenvalues.

    Parameters
    ----------
    eigenvectors : np.ndarray
        Matrix of eigenvectors (shape: [n_cells * 3, n_cells * 3]).
    eigenvalues : np.ndarray
        Array of eigenvalues (shape: [n_cells * 3]).
    avg_embryo_vector : np.ndarray
        Flattened vector representation of the average embryo (shape: [n_cells * 3]).
    dict_embryos_in_same_ref_at_t : dict
        Dictionary with individual names as keys and cell coordinates as values.

    Returns
    -------
    projections : dict
        Dictionary with projections onto the plane defined by the first two eigenvectors.
        Keys are individual names and values are the projected coordinates.
    variance_ratios : np.ndarray
        The percentage of variance explained by the first two eigenvalues.
    """
    # Get the list of cells (assuming all individuals have the same cells)
    cell_names = list(avg_embryo.keys())

    # Number of cells and individuals
    num_cells = len(cell_names)
    num_individuals = len(dict_embryos_in_same_ref_at_t)

    # Flatten each embryo into a vector of dimension (number_of_cells * 3)
    embryos_vectors = []
    for individual, cells in dict_embryos_in_same_ref_at_t.items():
        flattened_vector = np.concatenate([cells[cell] for cell in cell_names])
        embryos_vectors.append(flattened_vector)

    # Convert to numpy array for matrix operations
    embryos_matrix = np.array(embryos_vectors)

    # Also flatten the average embryo
    avg_embryo_vector = np.concatenate([avg_embryo[cell] for cell in cell_names])

    # Select the first two eigenvectors
    first_two_eigenvectors = eigenvectors[:, :2]

    # Compute the percentage of variance explained by each of the first two eigenvalues
    total_variance = np.sum(eigenvalues)
    variance_ratios = eigenvalues[:2] / total_variance

    # Project the average embryo and all individuals onto the plane defined by the first two eigenvectors
    projections = {}
    projections['avg_embryo'] = (avg_embryo_vector @ first_two_eigenvectors).tolist()

    for individual, flattened_vector in zip(dict_embryos_in_same_ref_at_t.keys(), embryos_matrix):
        projections[individual] = (flattened_vector @ first_two_eigenvectors).tolist()

    return projections, variance_ratios


def compute_pca_projections(dict_embryos_in_same_ref_at_t, avg_embryo):
    """
    Computes the PCA (Principal Component Analysis) projections of the embryos onto the plane defined by
    the first two eigenvectors of the covariance matrix of the embryo data.

    Parameters
    ----------
    dict_embryos_in_same_ref_at_t : dict
        A dictionary where keys are individual names and values are dictionaries with
        cell names as keys and their barycenters (positions) as values.
        Example structure:
        {
            'individual_1': {
                'cell_1': np.array([x1, y1, z1]),
                'cell_2': np.array([x2, y2, z2]),
                ...
            },
            'individual_2': {
                'cell_1': np.array([x3, y3, z3]),
                'cell_2': np.array([x4, y4, z4]),
                ...
            },
            ...
        }
    avg_embryo : dict
        A dictionary representing the average embryo at time `t`, where each key is a cell name and
        the value is the average position (barycenter) of that cell across all individuals.

    Returns
    -------
    projections : dict
        A dictionary containing the projections of each embryo, including the average embryo,
        onto the plane defined by the first two principal components (eigenvectors).
    variance_ratios : np.ndarray
        A 2-element array containing the percentage of variance explained by each of the first two eigenvectors.
    """
    # Get the list of cells (assuming all individuals have the same cells)
    cell_names = sorted(list(avg_embryo.keys()))

    # Number of cells and individuals
    num_cells = len(cell_names)
    num_individuals = len(dict_embryos_in_same_ref_at_t)

    # Flatten each embryo into a vector of dimension (number_of_cells * 3)
    embryos_vectors = []
    for individual, cells in dict_embryos_in_same_ref_at_t.items():
        flattened_vector = np.concatenate([cells[cell] for cell in cell_names])
        embryos_vectors.append(flattened_vector)

    # Convert to numpy array for matrix operations
    embryos_matrix = np.array(embryos_vectors)

    # Also flatten the average embryo
    avg_embryo_vector = np.concatenate([avg_embryo[cell] for cell in cell_names])

    # Compute the covariance matrix
    covariance_matrix = np.cov(embryos_matrix.T)

    # Compute the eigenvalues and eigenvectors
    eigenvalues, eigenvectors = np.linalg.eigh(covariance_matrix)
    print('shape of eigenvectors before', eigenvectors.shape)
    # Sort eigenvalues and eigenvectors in descending order
    idx = np.argsort(eigenvalues)[::-1]
    eigenvalues = eigenvalues[idx]
    eigenvectors = eigenvectors[:, idx]
    print('shape of eigenvectors here', eigenvectors.shape)
    return eigenvalues, eigenvectors


def compute_avg_barycenters_at_t(dict_embryos_in_same_ref_at_t):
    """
    Computes the average embryo at a given time point `t` by calculating the barycenter
    of each cell across all individuals.

    Parameters
    ----------
    dict_embryos_in_same_ref_at_t : dict
        A dictionary where keys are individual names and values are dictionaries with
        cell names as keys and their barycenters (positions) as values.
        Example structure:
        {
            'individual_1': {
                'cell_1': np.array([x1, y1, z1]),
                'cell_2': np.array([x2, y2, z2]),
                ...
            },
            'individual_2': {
                'cell_1': np.array([x3, y3, z3]),
                'cell_2': np.array([x4, y4, z4]),
                ...
            },
            ...
        }

    Returns
    -------
    dict
        A dictionary representing the average embryo at time `t`, where each key is a cell name and
        the value is the average position (barycenter) of that cell across all individuals.
        Example structure:
        {
            'cell_1': np.array([avg_x1, avg_y1, avg_z1]),
            'cell_2': np.array([avg_x2, avg_y2, avg_z2]),
            ...
        }
    """
    # Initialize the dictionary to store the average embryo
    avg_embryo = {}
    # Extract the list of individuals
    individuals = list(dict_embryos_in_same_ref_at_t.keys())
    # Iterate over each cell in the first individual's dictionary
    for cell_name in dict_embryos_in_same_ref_at_t[individuals[0]].keys():
        # Initialize a list to store the positions of the current cell across all individuals
        cell_positions = []
        # Collect the positions of the cell from all individuals
        for individual in individuals:
            if cell_name in dict_embryos_in_same_ref_at_t[individual]:
                cell_positions.append(dict_embryos_in_same_ref_at_t[individual][cell_name])
        # Compute the average position of the cell
        if cell_positions:
            avg_position = np.mean(cell_positions, axis=0)
            avg_embryo[cell_name] = avg_position
    return avg_embryo

def get_fate_map(cell_names_common, cell_name_dict, cell_fate_dict, tissue_res):
    """
    Get the fate map for the provided cell names based on cell name and fate dictionaries.

    Parameters
    ----------
    cell_names_common : list
        List of cell names in common at a specific timepoint.
    cell_name_dict : dict
        {cell_id: cell_name}
    cell_fate_dict : dict
        {cell_id: cell_fate}

    Returns
    -------
    dict
        A dictionary mapping cell names to their fates.
    """
    fate_map = {}
    # Iterate through the common cell names
    for cell_name in cell_names_common:
        # Find the cell ID corresponding to the cell name in cell_name_dict
        cell_id = None
        for id_, name in cell_name_dict.items():
            if name == cell_name:
                cell_id = id_
                break

        # If the cell ID was found, retrieve its fate
        if cell_id is not None and cell_id in cell_fate_dict:
            fate = cell_fate_dict[cell_id]
            if tissue_res == False:
                fate_map[cell_name] = fate
            else:
                fate_map[cell_name] = determine_fate_group(fate)

    return fate_map

def compute_variance_per_tissue_plot_script_at_t(dict_embryos_in_same_ref_at_t, fate_map, tissue_variances, t):
    """
    Compute PCA projections for each tissue type, calculate variance, and write a script to plot variance per tissue.

    Parameters
    ----------
    dict_embryos_in_same_ref_at_t : dict
        Dictionary mapping each embryo name to its cells' positions at time t.
        Format: {embryo_name: {cell_name: [x, y, z]}}.
    fate_map : dict
        Mapping of cell names to tissue types (fates).
        Format: {cell_name: tissue_type}.
    output_folder : str
        Directory where the Python script will be saved.

    Returns
    -------
    {'tissue': {t: {'eigenvalues': eigenvalues, 'eigenvectors': eigenvectors}}}
    """

    # Iterate through the tissues
    for tissue in set(fate_map.values()):
        tissue_cells = [cell for cell, fate in fate_map.items() if fate == tissue]
        # Collect cell positions for all embryos for this tissue
        tissue_embryos = {}
        for embryo, cells in dict_embryos_in_same_ref_at_t.items():
            # Filter out the cells belonging to this tissue
            embryo_tissue_cells = {cell: pos for cell, pos in cells.items() if cell in tissue_cells}
            # If there are no cells of this tissue type for this embryo, skip
            if not embryo_tissue_cells:
                continue
            tissue_embryos[embryo] = embryo_tissue_cells

        # If no embryos have cells for this tissue, skip
        if not tissue_embryos:
            continue

        # compute avg embryo at t : param dict_embryos_in_same_ref[t]: x
        avg_embryo = compute_avg_barycenters_at_t(tissue_embryos)
        # compute cov matrix eigenvalues/vectors/percentages/ projection/
        eigenvalues, eigenvectors = compute_pca_projections(tissue_embryos, avg_embryo)
        print('eigenvectors shape', eigenvectors.shape)

        if tissue not in tissue_variances:
            tissue_variances[tissue] = {}
        tissue_variances[tissue][t] = {
            'eigenvalues': eigenvalues.tolist(),
            'eigenvectors': eigenvectors.tolist(),
            'nb_of_cells': len(embryo_tissue_cells)
        }
    return tissue_variances

def write_variance_per_tissue_scripts(tissue_variances, output_folder):
    """
    write for each tissue a python script that does a plot  using
    write_variance_plot_script(eigenvalues_dict, dimension_sizes, outputfolder, suffix=None)

    Parameters
    ----------
    tissue_variances[tissue][t] = {
            'eigenvalues': eigenvalues,
            'eigenvectors': eigenvectors,
            'nb_of_cells': len(embryo_tissue_cells)
        }

    Returns
    -------

    """
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    all_tissues_total_variance = {}

    # Iterate over tissues and their respective time points
    for tissue, timepoint_data in tissue_variances.items():
        tissue_folder = os.path.join(output_folder, tissue)
        #if not os.path.exists(tissue_folder):
        #    os.makedirs(tissue_folder)

        # Prepare data for variance plot script
        eigenvalues_dict = {}
        dimension_sizes = []
        total_variance_per_time = []

        for t, data in timepoint_data.items():
            # Extract eigenvalues and number of cells (dimensions) for this tissue at time t
            eigenvalues_dict[t] = data['eigenvalues']
            dimension_sizes.append(data['nb_of_cells'])

            total_variance = sum(data['eigenvalues'] )/ data['nb_of_cells']
            total_variance_per_time.append((t, total_variance))

            # Add to the dictionary that accumulates variance for all tissues
            if tissue not in all_tissues_total_variance:
                all_tissues_total_variance[tissue] = []
            all_tissues_total_variance[tissue].append((t, total_variance))

        # Call the function to write the variance plot script for this tissue
        write_variance_plot_script(
            eigenvalues_dict=eigenvalues_dict,
            dimension_sizes=dimension_sizes,
            outputfolder=output_folder,
            suffix=tissue  # tissue name as the suffix
        )

    # call a new fct that writes a scripts that plots on same figure at each t the total variance/nb_of_cells
    write_all_tissues_normalised_variance_script(
        all_tissues_total_variance=all_tissues_total_variance,
        output_folder=output_folder
    )


def write_all_tissues_normalised_variance_script(all_tissues_total_variance, output_folder):
    """
    Writes a Python script that generates a plot of total variance normalized by the number of cells
    for all tissues on the same plot over time.

    Parameters
    ----------
    all_tissues_total_variance : dict
        Dictionary where each tissue contains a list of (timepoint, normalized_total_variance) tuples.
    output_folder : str
        Directory where the script will be saved.

    Returns
    -------
    None
    """
    script_filename = os.path.join(output_folder, "plot_nromalised_variance_btw_tissues.py")

    with open(script_filename, "w") as f:
        # Write imports
        f.write("import matplotlib.pyplot as plt\n\n")

        # Start the plotting script
        f.write("plt.figure(figsize=(10, 6))\n")
        f.write("plt.title('Total Variance Normalized by Number of Cells over Time for All Tissues')\n")
        f.write("plt.xlabel('Timepoints')\n")
        f.write("plt.ylabel('Normalized Total Variance')\n\n")

        # Colors for each tissue's line plot
        f.write("colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']\n")  # A list of 7 different colors
        f.write("color_index = 0\n\n")

        # Iterate over all tissues and their variances
        for tissue, variance_data in all_tissues_total_variance.items():
            f.write(f"timepoints = []\n")
            f.write(f"total_variances = []\n\n")

            for t, variance in variance_data:
                f.write(f"timepoints.append({t})\n")
                f.write(f"total_variances.append({variance})\n")

            # Plot each tissue's variance with its own color
            f.write(
                f"plt.plot(timepoints, total_variances, marker='o', linestyle='-', color=colors[color_index], label='{tissue}')\n")
            f.write("color_index = (color_index + 1) % len(colors)\n\n")

        # Add legend and show the plot
        f.write("plt.legend(title='Tissues')\n")
        f.write("plt.tight_layout()\n")
        f.write("plt.show()\n")

    print(f"Total variance plot for all tissues script written to: {script_filename}")

def compute_geom_barycenters_distance_at_each_dev_time_only_common_cells(files, ref_filename,
                                                                         transformation_type='similitude',
                                                                         retained_fraction=1, output_folder='',
                                                                         embryo_res='halfs',
                                                                         imaging_res=0.3, interval=[0, 20],
                                                                         dif_dev_time=15, weight_by_cell_mass=True,
                                                                         tissue_res=True):
    """
    For an interval of time chosen
        common names in interval = []
        xfor each t
        x    register ref between t
        x    common names at t = []
        x    ignore embryos not imaged at considered stage
            filter only common cells
            register common cells to ref
            store registered embryos at t
            distance matrix and mds at t of common cells

        filter only common cells in all intervals from stored embryos at each t
        umap of embryos
        distance matrix and mds


    Parameters
    ----------
    files
    ref_filename
    output_folder
    embryo_res
    imaging_res
    interval depends of reference, to be adapted

    Returns
    -------

    """

    # imaging_res is 0.3x0.3x0.3um

    list_dict_barycenters = []
    list_dict_names = []
    list_dict_mass = []

    embryo_names = []
    embryos_props = {}
    embryo_idx = {}

    atimes_list = []
    times_list = []

    list_surface_adjustment_factors = []  # list of dict [pmx_idx]{'taq': scaling_factor}

    err = ""

    dict_embryos_in_same_ref = {}  # in a tdev, embryos all in same ref barycenters

    all_cell_names = set()

    # get embryos
    with open(ref_filename, 'rb') as f:
        ref_props_data = {}
        ref_name = ref_filename.split('-')[1]
        ref_props = pkl.load(f)
        ref_props_data[ref_name] = ref_props

        # compute volume alignment parms
        volume_adjustement_factors = volume_fitting_and_voxelsize_correction(ref_props['cell_volume'],
                                                                             target_volume=60000000)

        volume_adjustement_factors = {t: factor * imaging_res for t, factor in volume_adjustement_factors.items()}

        #  handle case of embryo_resolution
        if embryo_res == 'halfs':
            left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                ref_props['cell_name'], ref_props['cell_barycenter'])
            list_dict_barycenters.append(left_prop_dict)
            list_dict_barycenters.append(right_prop_dict)

            list_dict_names.append(left_names_dict)
            list_dict_names.append(right_names_dict)

            left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                ref_props['cell_name'], ref_props['cell_volume'])
            list_dict_mass.append(left_prop_dict)
            list_dict_mass.append(right_prop_dict)

            i = len(embryo_idx)
            embryo_idx[ref_name + '_left'] = i
            embryo_idx[ref_name + '_right'] = i + 1
            embryo_names.append(ref_name + '_left')
            embryo_names.append(ref_name + '_right')

            # add
            list_surface_adjustment_factors.append(volume_adjustement_factors)
            list_surface_adjustment_factors.append(volume_adjustement_factors)

        else:
            #
            list_dict_barycenters.append(ref_props['cell_barycenter'])
            list_dict_names.append(ref_props['cell_name'])
            list_dict_mass.append(ref_props['cell_volume'])
            embryo_names.append(ref_name)
            i = len(embryo_idx)
            embryo_idx[ref_name] = i

            #  add
            list_surface_adjustment_factors.append(volume_adjustement_factors)
        ###

    for filename in files:
        if filename == ref_filename:
            continue
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)
            embryo_name = filename.split('-')[1]

            embryos_props[embryo_name] = {
                'cell_barycenter': pm_props['cell_barycenter'],
                'cell_name': pm_props['cell_name'],
                'cell_volume': pm_props['cell_volume'],
                'cell_lineage': pm_props['cell_lineage']
            }

            # compute volume alignment parms
            volume_adjustement_factors = volume_fitting_and_voxelsize_correction(pm_props['cell_volume'],
                                                                                 target_volume=60000000)
            volume_adjustement_factors = {t: factor * imaging_res for t, factor in volume_adjustement_factors.items()}

            # handle a case of embryo_resolution
            #
            if embryo_res == 'halfs':
                left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                    pm_props['cell_name'], pm_props['cell_barycenter'])
                list_dict_barycenters.append(left_prop_dict)
                list_dict_barycenters.append(right_prop_dict)

                list_dict_names.append(left_names_dict)
                list_dict_names.append(right_names_dict)

                left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                    pm_props['cell_name'], pm_props['cell_volume'])
                list_dict_mass.append(left_prop_dict)
                list_dict_mass.append(right_prop_dict)

                i = len(embryo_idx)
                embryo_idx[embryo_name + '_left'] = i
                embryo_idx[embryo_name + '_right'] = i + 1

                embryo_names.append(embryo_name + '_left')
                embryo_names.append(embryo_name + '_right')

                # add adjustement parameters of volumes
                list_surface_adjustment_factors.append(volume_adjustement_factors)
                list_surface_adjustment_factors.append(volume_adjustement_factors)

            else:
                i = len(embryo_idx)
                embryo_idx[embryo_name] = i
                # list_dict_lineage.append(embryo_prop['cell_lineage'])
                list_dict_barycenters.append(pm_props['cell_barycenter'])
                list_dict_names.append(pm_props['cell_name'])
                list_dict_mass.append(pm_props['cell_volume'])
                embryo_names.append(embryo_name)

                #  add adjustement parameters of volumes
                list_surface_adjustment_factors.append(volume_adjustement_factors)

                #

    output_file = os.path.join(output_folder, "log_file.log")
    file = open(output_file, "w")
    file.write('Hello')

    # do temporal alignment
    temporal_alignment_coefficient_dict, _ = get_temporal_alignment_coefficients_of_embryos(embryos_props,
                                                                                            ref_props_data)
    print('nb of embryos in pop:', len(embryo_names))
    print('ref embryo', ref_name)
    #
    #  delete embryos_props
    del embryos_props
    del ref_props_data

    # for each embryo compute adjusted times
    times_ref = utimes.get_embryo_times(ref_props['cell_lineage'])
    a, b = temporal_alignment_coefficient_dict[ref_name]
    a_times_ref = [a * t + b for t in times_ref]
    print('for ref:', a, b)
    # times_list.append(times_ref)
    # atimes_list.append(a_times_ref)

    for embryo, i in embryo_idx.items():
        times = utimes.get_embryo_times(list_dict_barycenters[i])
        if embryo_res == 'halfs':
            embryo = embryo.split('_')[0]
            a, b = temporal_alignment_coefficient_dict[embryo]
            a_times = [a * t + b for t in times]
            atimes_list.append(a_times)
            times_list.append(times)
        else:
            a, b = temporal_alignment_coefficient_dict[embryo]
            a_times = [a * t + b for t in times]
            atimes_list.append(a_times)
            times_list.append(times)

    if embryo_res == 'halfs':
        ref_name = ref_name + '_left'
    ##
    ##
    #

    prev_ref_co_registered = {}  # {t: reg_barycenters}
    prev_ref_names = {}  # {t: ref_cell_names}

    common_names_interval = set()
    non_common_names_interval = set()

    barycenters_in_common_ref = {}
    cells_volumes_in_common_ref = {}
    nb_of_cells_in_common = {}

    variances_dict = {}
    tissue_variances = {}

    for i, t in enumerate(range(interval[0], interval[1])):

        list_dict_mass_at_t_common = {}
        list_dict_barycenters_at_t_common = {}
        list_dict_names_at_t_common = {}

        list_cell_ids_in_pm1_at_t_common = []

        print('working for dev time in ref:', t)
        ignored_embryos = set()  # embryos not imaged at the corresponding dev time

        # t is dev time in ref we're currently putting all embryos in the same referential at this dev t

        list_dict_barycenters_at_t = []
        list_dict_names_at_t = []
        list_dict_mass_at_t = []

        t = int(t)
        # t aquisition
        if t not in a_times_ref:
            continue
        taq_1 = times_ref[a_times_ref.index(t)]
        err_1 = f"aq time in pm1:{taq_1}\n"

        # Get dictionaries at t_aq1
        names_dict = get_embryo_at_t(list_dict_names[0], taq_1)
        mass_dict = get_embryo_at_t(list_dict_mass[0], taq_1)
        barycenters_dict = get_embryo_at_t(list_dict_barycenters[0], taq_1)
        dict_at_t = barycenters_dict
        # Scaling factor
        scaling_factor = list_surface_adjustment_factors[0][taq_1]

        # Align cell data, does scaling also
        list_cell_ids_in_pm1_at_t, ref_names_at_t, ref_mass_at_t, ref_barycenters_at_t = align_cell_data(
            names_dict, mass_dict, barycenters_dict, scaling_factor
        )

        # Update the prev_ref_co_registered dictionary
        prev_ref_co_registered, prev_ref_names = update_prev_ref_co_registered(
            taq_1,
            times_ref,
            prev_ref_names,
            ref_names_at_t,
            ref_barycenters_at_t,
            transformation_type,
            retained_fraction,
            prev_ref_co_registered,
            interval[0]
        )
        print('lenght of stored ref_barycenters_at_t', len(prev_ref_co_registered[t]),
              len(get_embryo_at_t(list_dict_names[0], t)), t)
        ###########
        ####
        list_dict_barycenters_at_t.append(prev_ref_co_registered[t])
        list_dict_names_at_t.append(ref_names_at_t)
        list_dict_mass_at_t.append(ref_mass_at_t)
        common_names_t = set(ref_names_at_t)  # Start with PM1 names
        non_common_names_t = set(ref_names_at_t)
        ##############

        print('lenght in ref', len(ref_barycenters_at_t), 'in embryo', len(ref_barycenters_at_t) * 2, 'at', t)
        ##
        # Grab embryos at needed dev_t
        # for most common ancestry (putting all embryos in same ref) I need to know common cells and
        # non common cells
        ##
        individuals = [ref_name]
        for pmx_name, pmx_idx in embryo_idx.items():
            if pmx_idx == 0:
                continue
            closest_at = min(atimes_list[pmx_idx], key=lambda x: abs(x - t))

            if embryo_res == 'halfs' and pmx_name.split('_')[1] == 'right':
                # if an individual is a right, when dealing with halfs embryos, apply symetry
                #  vector = left_embryo(idx-1) barycenters

                pmx_barycenters_at_t_left, pmx_mass_at_t_left = (list_dict_barycenters_at_t[pmx_idx - 1],
                                                                 list_dict_mass_at_t[pmx_idx - 1])
                taq = times_list[pmx_idx][atimes_list[pmx_idx].index(closest_at)]
                pmx_barycenters_at_t, pmx_names_at_t, pmx_mass_at_t = (
                    get_embryo_data_at_time(list_dict_barycenters[pmx_idx], list_dict_names[pmx_idx],
                                            list_dict_mass[pmx_idx], taq))
                # estimate symetry at t
                sym_pmx_at_t = estimate_symmetry_at_t(pmx_mass_at_t, pmx_barycenters_at_t, pmx_mass_at_t_left,
                                                      pmx_barycenters_at_t_left)
                # apply symetry on t right barycenters
                pmx_barycenters_at_t = apply_sym_on_half_embryo(pmx_barycenters_at_t, sym_pmx_at_t)
            else:
                taq = times_list[pmx_idx][atimes_list[pmx_idx].index(closest_at)]
                pmx_barycenters_at_t, pmx_names_at_t, pmx_mass_at_t = (
                    get_embryo_data_at_time(list_dict_barycenters[pmx_idx], list_dict_names[pmx_idx],
                                            list_dict_mass[pmx_idx], taq))

            # print(pmx_name,len(pmx_barycenters_at_t) - len(ref_barycenters_at_t), len(pmx_barycenters_at_t))
            if abs(len(pmx_barycenters_at_t) - len(ref_barycenters_at_t)) >= dif_dev_time:
                print('lenght in pmx', len(pmx_barycenters_at_t), 'in embryo', len(pmx_barycenters_at_t) * 2, 'at', t,
                      closest_at)
                print(pmx_name, 'embryo not imaged at this developmental stage', 'nb of cells in ref:',
                      len(list(dict_at_t.keys())),
                      len(pmx_barycenters_at_t))
                ignored_embryos.add(pmx_name)

            # apply scaling on volume & barycenters at taq, pmx_idx
            scaling_factor = list_surface_adjustment_factors[pmx_idx][taq]
            pmx_mass_at_t = [nv * scaling_factor * scaling_factor * scaling_factor for nv in pmx_mass_at_t]
            pmx_barycenters_at_t = [nv * scaling_factor for nv in pmx_barycenters_at_t]

            individuals.append(pmx_name)

            list_dict_barycenters_at_t.append(pmx_barycenters_at_t)
            list_dict_names_at_t.append(pmx_names_at_t)
            list_dict_mass_at_t.append(pmx_mass_at_t)

            ###
            #
            pmx_names_at_t_sets = set(pmx_names_at_t)

            if pmx_name not in ignored_embryos:
                # get commun and non-commun cell names
                common_names_t = common_names_t.intersection(pmx_names_at_t_sets)
                # non_common_names_t.append(pmx_names_at_t_sets.difference(common_names_t))
                non_common_names_t = non_common_names_t.union(pmx_names_at_t_sets)
                err_1 += f"aq time in {pmx_name}:{taq}\n"
                err_1 += f"names before merge: {pmx_names_at_t}\n"

                ##########
                # for first timepoint use flag to get
                if i == 0:
                    common_names_interval = common_names_t
                else:
                    common_names_interval = common_names_interval.intersection(common_names_t)
                non_common_names_interval = non_common_names_interval.union(pmx_names_at_t_sets)

        if len(common_names_t) == 0:
            print('no common names btw embryos')
            continue

        ######## register, compute distance
        ##############
        ###########################
        # Take vector of common names
        for cell in common_names_t:
            # common cellsstored in embryos in same order
            for pmx_name, pmx_idx in embryo_idx.items():
                if pmx_name in ignored_embryos:
                    continue
                idx_d1 = list_dict_names_at_t[pmx_idx].index(cell)
                barycenter_1 = list_dict_barycenters_at_t[pmx_idx][idx_d1]
                mass_1 = list_dict_mass_at_t[pmx_idx][idx_d1]

                if pmx_idx not in list_dict_names_at_t_common:
                    list_dict_names_at_t_common[pmx_idx] = []
                    list_dict_barycenters_at_t_common[pmx_idx] = []
                    list_dict_mass_at_t_common[pmx_idx] = []
                list_dict_names_at_t_common[pmx_idx].append(cell)
                list_dict_barycenters_at_t_common[pmx_idx].append(barycenter_1)
                list_dict_mass_at_t_common[pmx_idx].append(mass_1)

                if pmx_name == ref_name:
                    id1 = list_cell_ids_in_pm1_at_t[idx_d1]
                    list_cell_ids_in_pm1_at_t_common.append(id1)

        print('nb of cells in common', len(common_names_t), 't', t)

        # register common names

        ##
        # take ref embryo
        ref_bary = list_dict_barycenters_at_t_common[0]
        ref_names = list_dict_names_at_t_common[0]
        ref_volumes = list_dict_mass_at_t_common[0]
        ref_names, ref_bary, ref_ids = sort_lists_according_to_cell_name(ref_names, ref_bary,
                                                                         list_cell_ids_in_pm1_at_t)
        ref_names, ref_volumes, ref_ids = sort_lists_according_to_cell_name(ref_names, ref_volumes,
                                                                            list_cell_ids_in_pm1_at_t)

        ##### Storing embryos in same ref
        # add to common ref data
        for name, bary, vol in zip(ref_names, ref_bary, ref_volumes):
            if t not in barycenters_in_common_ref:
                barycenters_in_common_ref[t] = {}
                cells_volumes_in_common_ref[t] = {}

            if name not in barycenters_in_common_ref[t]:
                barycenters_in_common_ref[t][name] = []
                cells_volumes_in_common_ref[t][name] = []
            barycenters_in_common_ref[t][name].append(bary)
            cells_volumes_in_common_ref[t][name].append(vol)
            nb_of_cells_in_common[t] = len(common_names_t)

        print('embryos size at t:', len(ref_names))
        # register each of the embryos (global registration)
        for pmx_name, pmx_idx in embryo_idx.items():
            if pmx_name == ref_name:
                continue
            if pmx_name in ignored_embryos:
                continue
            # get pmx barycenters and names
            flo_bary = list_dict_barycenters_at_t_common[pmx_idx]
            flo_names = list_dict_names_at_t_common[pmx_idx]
            flo_vol = list_dict_mass_at_t_common[pmx_idx]

            # sort appropriately
            flo_names, flo_bary = get_vectors_before_registration(ref_names, flo_bary, flo_names)
            flo_names, flo_vol = get_vectors_before_registration(ref_names, flo_vol, flo_names)
            #
            # compute transform: should we use a similitude/rigid?
            transform, ref, floating = compute_similitude(ref_bary, flo_bary, transformation_type,
                                                          retained_fraction)

            # make transform rigid, its transform homogeneous coordinates
            transform = similitude_without_scaling(transform)
            # apply transform
            transformed_floating = apply_transformation(transform, floating)

            for name, bary, vol in zip(flo_names, transformed_floating.T, flo_vol):
                if name not in barycenters_in_common_ref[t]:
                    barycenters_in_common_ref[t][name] = []
                    cells_volumes_in_common_ref[t][name] = []
                barycenters_in_common_ref[t][name].append(bary)
                cells_volumes_in_common_ref[t][name].append(vol)

            # every coordinate should be weighted by cell's volume
            # store embryos in same ref
            for i, cell_name in enumerate(flo_names):
                if t not in dict_embryos_in_same_ref:
                    dict_embryos_in_same_ref[t] = {}
                if ref_name not in dict_embryos_in_same_ref[t]:
                    dict_embryos_in_same_ref[t][ref_name] = {}
                if pmx_name not in dict_embryos_in_same_ref[t]:
                    dict_embryos_in_same_ref[t][pmx_name] = {}
                dict_embryos_in_same_ref[t][ref_name][cell_name] = ref[:, i]
                dict_embryos_in_same_ref[t][pmx_name][cell_name] = transformed_floating[:, i]

        # compute distance between embryos
        ######
        # in the fcts called ignore the ignored embryonames
        embryo_names_t = [name for name in embryo_names if name not in ignored_embryos]

        if weight_by_cell_mass:
            # distance matrix of geometrical feature(cells barycenters) at each t
            global_avg_distances_matrix, dimensions_ = distance_matrix_geom_btw_individuals_at_t(
                dict_embryos_in_same_ref[t], sorted(embryo_names_t), cells_volumes_in_common_ref[t])
        else:
            global_avg_distances_matrix, dimensions_ = distance_matrix_geom_btw_individuals_at_t(
                dict_embryos_in_same_ref[t], sorted(embryo_names_t))

        time = f'{len(dict_at_t)}_{t}'
        print('wuhu', time, embryo_names_t)
        # plot global_avg_distances_matrix at t
        write_plot_script_global_distance_btw_embryos(global_avg_distances_matrix, dimensions_, time,
                                                      sorted(embryo_names_t),
                                                      output_folder, 'geometric', embryo_res)

        write_plot_script_mds_distance_btw_embryos(global_avg_distances_matrix, dimensions_, time,
                                                   sorted(embryo_names_t),
                                                   output_folder, 'geometric', embryo_res)

        # compute avg embryo at t : param dict_embryos_in_same_ref[t]: x
        avg_embryo = compute_avg_barycenters_at_t(dict_embryos_in_same_ref[t])
        # compute cov matrix eigenvalues/vectors/percentages/ projection/
        eigenvalues, eigenvectors = compute_pca_projections(dict_embryos_in_same_ref[t], avg_embryo)
        # plot variance vs nb of dimensions
        write_variance_plot_script_at_t(eigenvalues, output_folder, time)
        # projection on first two dimensions
        projections, variance_ratios = compute_projections_and_variance(eigenvectors, eigenvalues,
                                                                        dict_embryos_in_same_ref[t], avg_embryo)
        # plot
        write_pca_projection_script(projections, variance_ratios, time, embryo_res, output_folder)
        variances_dict[t] = eigenvalues.tolist()

        # per tissue
        # get fate map
        fate_map = get_fate_map(list_dict_names_at_t_common[0], list_dict_names[0], ref_props['cell_fate'], tissue_res)
        # compute variance per tissue at t
        tissue_variances = compute_variance_per_tissue_plot_script_at_t(dict_embryos_in_same_ref[t], fate_map,
                                                                        tissue_variances, t)

    # overtime analysis
    write_variance_plot_script(variances_dict, list(nb_of_cells_in_common.values()), output_folder)
    # per tissue overtime variability analysis
    write_variance_per_tissue_scripts(tissue_variances, output_folder)

    # tissues eigenspace stability overtime
    write_tissues_eigenspace_stability_consecutive_timepoints_overtime(tissue_variances, 3, output_folder)

    dict_projection_embryos_common_cells = {}
    # filter on common names
    for t in dict_embryos_in_same_ref.keys():
        for pmx in dict_embryos_in_same_ref[t].keys():
            for cell in common_names_interval:
                if t not in dict_projection_embryos_common_cells:
                    dict_projection_embryos_common_cells[t] = {}
                if pmx not in dict_projection_embryos_common_cells[t]:
                    dict_projection_embryos_common_cells[t][pmx] = {}
                dict_projection_embryos_common_cells[t][pmx][cell] = dict_embryos_in_same_ref[t][pmx][cell]

    print('common name in interval', common_names_interval)
    # umap for: dict_embryos_in_same_ref[t][pmx_name][cell]=3d_coordinate
    generate_umap_script(dict_projection_embryos_common_cells, common_names_interval, ref_name, embryo_names,
                         output_folder)

    file.close()
    # mds on interval of embryo
    output_script_path = os.path.join(output_folder, 'mds_across_timepoints.py')
    generate_mds_script_across_timepoints(dict_projection_embryos_common_cells, cells_volumes_in_common_ref, output_script_path)


def generate_mds_script_across_timepoints(dict_projection_embryos_common_cells, cells_volumes_in_common_ref, output_script_path):
    script_content = f"""
import numpy as np
from sklearn.metrics import pairwise_distances
from sklearn.manifold import MDS
import matplotlib.pyplot as plt

dict_projection_embryos_common_cells = {dict_projection_embryos_common_cells}
cells_volumes_in_common_ref = {cells_volumes_in_common_ref} 

def compute_average_distance_matrix_across_timepoints(dict_projection_embryos_common_cells, cells_volumes_in_common_ref):
    timepoints = list(dict_projection_embryos_common_cells.keys())
    embryos = list(set([embryo for t in timepoints for embryo in dict_projection_embryos_common_cells[t].keys()]))
    num_embryos = len(embryos)
    num_timepoints = len(timepoints)

    # Initialize the distance matrix
    total_combinations = num_embryos * num_timepoints
    distance_matrix = np.zeros((total_combinations, total_combinations))
    counts_matrix = np.zeros((total_combinations, total_combinations))

    for t1_idx, t1 in enumerate(timepoints):
        for t2_idx, t2 in enumerate(timepoints):
            for i in range(num_embryos):
                for j in range(num_embryos):
                    embryo_i = embryos[i]
                    embryo_j = embryos[j]

                    if embryo_i in dict_projection_embryos_common_cells[t1] and embryo_j in dict_projection_embryos_common_cells[t2]:
                        common_cells = set(dict_projection_embryos_common_cells[t1][embryo_i].keys()).intersection(set(dict_projection_embryos_common_cells[t2][embryo_j].keys()))
                        
                        if common_cells:
                            distances = []
                            # total volume is expected to be constant due to scaling
                            total_volume_t1 = sum(cells_volumes_in_common_ref[t1][cell] for cell in common_cells)
                            total_volume_t2 = sum(cells_volumes_in_common_ref[t2][cell] for cell in common_cells)
                            total_volume = (total_volume_t1 + total_volume_t2) / 2  # Average the volumes at t1 and t2

                            for cell in common_cells:
                                volume_t1 = cells_volumes_in_common_ref[t1][cell]
                                volume_t2 = cells_volumes_in_common_ref[t2][cell]
                                volume = (volume_t1 + volume_t2) / 2  # Average the volume at t1 and t2
                                distance = np.linalg.norm(np.array(dict_projection_embryos_common_cells[t1][embryo_i][cell]) - np.array(dict_projection_embryos_common_cells[t2][embryo_j][cell]))
                                weighted_distance = volume * distance
                                distances.append(weighted_distance)

                            average_distance = np.sum(distances) / total_volume
                        else:
                            average_distance = np.inf  # or some large value to indicate no common cells

                        idx_i = i + t1_idx * num_embryos
                        idx_j = j + t2_idx * num_embryos

                        distance_matrix[idx_i, idx_j] += average_distance
                        counts_matrix[idx_i, idx_j] += 1

    # Avoid division by zero
    valid_pairs = counts_matrix > 0
    distance_matrix[valid_pairs] /= counts_matrix[valid_pairs]
    distance_matrix[~valid_pairs] = np.inf  # or some large value to indicate no common cells across all time points

    return distance_matrix, embryos, timepoints


def plot_mds(distance_matrix, embryos):
    mds = MDS(n_components=2, dissimilarity='precomputed')
    coords = mds.fit_transform(distance_matrix)

    plt.figure(figsize=(10, 8))
    plt.scatter(coords[:, 0], coords[:, 1], s=100)

    for i, embryo in enumerate(embryos):
        plt.text(coords[i, 0], coords[i, 1], embryo, fontsize=12)

    plt.title("MDS Plot of Embryos Across All Time Points")
    plt.xlabel("MDS Dimension 1")
    plt.ylabel("MDS Dimension 2")
    plt.grid()
    plt.show()

distance_matrix, embryos = compute_average_distance_matrix_across_timepoints(dict_projection_embryos_common_cells)
plot_mds(distance_matrix, embryos)

"""

    with open(output_script_path, 'w') as f:
        f.write(script_content)


def all_embryos_in_same_referential__temporal_geometric(files, ref_filename, transformation_type='similitude',
                                                        retained_fraction=1, output_folder='', d_or_mean='avg',
                                                        embryo_res='halfs', imaging_res=0.3):
    """
    Aim is to do a temporal and spatial comparision between embryos at all developmental stages
    We can use only common cells/Return to MCA to compare embryos at same dev stage (projection to a subpspace)

    To compare embryos at different timepoint we project to a hyperspace (dimension 3*nb_of_cells)
    Pb is with latter zero padding (its a traslation of coordinates that will reflect a division occcuring in all cells)
    So it do not just reflect changes in shape
        ->(we will later compare it to a subspace projection of common cells)


    Parameters
    ----------
    files
    ref_filename
    transformation_type
    retained_fraction
    output_folder
    d_or_mean
    embryo_res
    imaging_res

    Returns
    -------

    """

    # imaging_res is 0.3x0.3x0.3um

    list_dict_barycenters = []
    list_dict_names = []
    list_dict_mass = []

    embryo_names = []
    embryos_props = {}
    embryo_idx = {}

    atimes_list = []
    times_list = []

    list_surface_adjustment_factors = []  # list of dict [pmx_idx]{'taq': scaling_factor}

    err = ""

    dict_embryos_in_same_ref = {}  # in a tdev, embryos all in same ref barycenters

    all_cell_names = set()

    # get embryos
    with open(ref_filename, 'rb') as f:
        ref_props_data = {}
        ref_name = ref_filename.split('-')[1]
        ref_props = pkl.load(f)
        ref_props_data[ref_name] = ref_props

        # compute volume alignment parms
        volume_adjustement_factors = volume_fitting_and_voxelsize_correction(ref_props['cell_volume'],
                                                                             target_volume=60000000)

        volume_adjustement_factors = {t: factor * imaging_res for t, factor in volume_adjustement_factors.items()}

        #  handle case of embryo_resolution
        if embryo_res == 'halfs':
            left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                ref_props['cell_name'], ref_props['cell_barycenter'])
            list_dict_barycenters.append(left_prop_dict)
            list_dict_barycenters.append(right_prop_dict)

            list_dict_names.append(left_names_dict)
            list_dict_names.append(right_names_dict)

            left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                ref_props['cell_name'], ref_props['cell_volume'])
            list_dict_mass.append(left_prop_dict)
            list_dict_mass.append(right_prop_dict)

            i = len(embryo_idx)
            embryo_idx[ref_name + '_left'] = i
            embryo_idx[ref_name + '_right'] = i + 1
            embryo_names.append(ref_name + '_left')
            embryo_names.append(ref_name + '_right')

            # add
            list_surface_adjustment_factors.append(volume_adjustement_factors)
            list_surface_adjustment_factors.append(volume_adjustement_factors)

        else:
            #
            list_dict_barycenters.append(ref_props['cell_barycenter'])
            list_dict_names.append(ref_props['cell_name'])
            list_dict_mass.append(ref_props['cell_volume'])
            embryo_names.append(ref_name)
            i = len(embryo_idx)
            embryo_idx[ref_name] = i

            #  add
            list_surface_adjustment_factors.append(volume_adjustement_factors)

        ###

    for filename in files:
        if filename == ref_filename:
            continue
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)
            embryo_name = filename.split('-')[1]

            embryos_props[embryo_name] = {
                'cell_barycenter': pm_props['cell_barycenter'],
                'cell_name': pm_props['cell_name'],
                'cell_volume': pm_props['cell_volume'],
                'cell_lineage': pm_props['cell_lineage']
            }

            # compute volume alignment parms
            volume_adjustement_factors = volume_fitting_and_voxelsize_correction(pm_props['cell_volume'],
                                                                                 target_volume=60000000)
            volume_adjustement_factors = {t: factor * imaging_res for t, factor in volume_adjustement_factors.items()}

            # handle a case of embryo_resolution
            #
            if embryo_res == 'halfs':
                left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                    pm_props['cell_name'], pm_props['cell_barycenter'])
                list_dict_barycenters.append(left_prop_dict)
                list_dict_barycenters.append(right_prop_dict)

                list_dict_names.append(left_names_dict)
                list_dict_names.append(right_names_dict)

                left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                    pm_props['cell_name'], pm_props['cell_volume'])
                list_dict_mass.append(left_prop_dict)
                list_dict_mass.append(right_prop_dict)

                i = len(embryo_idx)
                embryo_idx[embryo_name + '_left'] = i
                embryo_idx[embryo_name + '_right'] = i + 1

                embryo_names.append(embryo_name + '_left')
                embryo_names.append(embryo_name + '_right')

                # add adjustement parameters of volumes
                list_surface_adjustment_factors.append(volume_adjustement_factors)
                list_surface_adjustment_factors.append(volume_adjustement_factors)

            else:
                i = len(embryo_idx)
                embryo_idx[embryo_name] = i
                # list_dict_lineage.append(embryo_prop['cell_lineage'])
                list_dict_barycenters.append(pm_props['cell_barycenter'])
                list_dict_names.append(pm_props['cell_name'])
                list_dict_mass.append(pm_props['cell_volume'])
                embryo_names.append(embryo_name)

                #  add adjustement parameters of volumes
                list_surface_adjustment_factors.append(volume_adjustement_factors)

                #

    output_file = os.path.join(output_folder, "log_file.log")
    file = open(output_file, "w")
    file.write('Hello')

    # do temporal alignment
    temporal_alignment_coefficient_dict, _ = get_temporal_alignment_coefficients_of_embryos(embryos_props,
                                                                                            ref_props_data)
    print('nb of embryos in pop:', len(embryo_names))
    print('ref embryo', ref_name)
    #
    #  delete embryos_props
    del embryos_props
    del ref_props_data

    # for each embryo compute adjusted times
    times_ref = utimes.get_embryo_times(ref_props['cell_lineage'])
    a, b = temporal_alignment_coefficient_dict[ref_name]
    a_times_ref = [a * t + b for t in times_ref]
    print('for ref:', a, b)
    # times_list.append(times_ref)
    # atimes_list.append(a_times_ref)

    for embryo, i in embryo_idx.items():
        times = utimes.get_embryo_times(list_dict_barycenters[i])
        if embryo_res == 'halfs':
            embryo = embryo.split('_')[0]
            a, b = temporal_alignment_coefficient_dict[embryo]
            a_times = [a * t + b for t in times]
            atimes_list.append(a_times)
            times_list.append(times)
        else:
            a, b = temporal_alignment_coefficient_dict[embryo]
            a_times = [a * t + b for t in times]
            atimes_list.append(a_times)
            times_list.append(times)

    if embryo_res == 'halfs':
        ref_name = ref_name + '_left'
    ##
    ##
    #

    prev_ref_co_registered = {}  # {t: reg_barycenters}
    prev_ref_names= {}  # {t: ref_cell_names}

    for t in a_times_ref:
        print('working for dev time in ref:', t)
        flag = 0  # skips t if cells are missing cz MCA wount be gd
        ignored_embryos = set()  # embryos not imaged at the corresponding dev time

        # t is dev time in ref we're currently putting all embryos in the same referential at this dev t

        list_dict_barycenters_at_t = []
        list_dict_names_at_t = []
        list_dict_mass_at_t = []

        t = int(t)
        # t aquisition
        taq_1 = times_ref[a_times_ref.index(t)]
        err_1 = f"aq time in pm1:{taq_1}\n"

        # Get dictionaries at t_aq1
        names_dict = get_embryo_at_t(list_dict_names[0], taq_1)
        mass_dict = get_embryo_at_t(list_dict_mass[0], taq_1)
        barycenters_dict = get_embryo_at_t(list_dict_barycenters[0], taq_1)
        dict_at_t = barycenters_dict
        # Scaling factor
        scaling_factor = list_surface_adjustment_factors[0][taq_1]

        # Align cell data, does scaling also
        list_cell_ids_in_pm1_at_t, ref_names_at_t, ref_mass_at_t, ref_barycenters_at_t = align_cell_data(
            names_dict, mass_dict, barycenters_dict, scaling_factor
        )

        #list_dict_barycenters_at_t.append(ref_barycenters_at_t)
        #list_dict_names_at_t.append(ref_names_at_t)
        #list_dict_mass_at_t.append(ref_mass_at_t)

        #print('bary at t shape:', np.array(ref_barycenters_at_t).shape)

        # Update the prev_ref_co_registered dictionary
        prev_ref_co_registered, prev_ref_names = update_prev_ref_co_registered(
            taq_1,
            times_ref,
            prev_ref_names,
            ref_names_at_t,
            ref_barycenters_at_t,
            transformation_type,
            retained_fraction,
            prev_ref_co_registered
        )
        print('lenght of stored ref_barycenters_at_t', len(prev_ref_co_registered[t]), len(get_embryo_at_t(list_dict_names[0],t)), t)
        ###########
        ####
        list_dict_barycenters_at_t.append(prev_ref_co_registered[t])
        list_dict_names_at_t.append(ref_names_at_t)
        list_dict_mass_at_t.append(ref_mass_at_t)
        common_names_all = set(ref_names_at_t)  # Start with PM1 names
        non_common_names_all = set(ref_names_at_t)
        ##############

        print('lenght in ref', len(ref_barycenters_at_t), 'in embryo', len(ref_barycenters_at_t) * 2, 'at', t)
        ##
        # Grab embryos at needed dev_t
        # for most common ancestry (putting all embryos in same ref) I need to know common cells and
        # non common cells
        ##
        individuals = [ref_name]
        for pmx_name, pmx_idx in embryo_idx.items():
            if pmx_idx == 0:
                continue
            closest_at = min(atimes_list[pmx_idx], key=lambda x: abs(x - t))

            if embryo_res == 'halfs' and pmx_name.split('_')[1] == 'right':
                # if an individual is a right, when dealing with halfs embryos, apply symetry
                #  vector = left_embryo(idx-1) barycenters

                pmx_barycenters_at_t_left, pmx_mass_at_t_left = (list_dict_barycenters_at_t[pmx_idx - 1],
                                                                 list_dict_mass_at_t[pmx_idx - 1])
                taq = times_list[pmx_idx][atimes_list[pmx_idx].index(closest_at)]
                pmx_barycenters_at_t, pmx_names_at_t, pmx_mass_at_t = (
                    get_embryo_data_at_time(list_dict_barycenters[pmx_idx], list_dict_names[pmx_idx],
                                            list_dict_mass[pmx_idx], taq))
                # estimate symetry at t
                sym_pmx_at_t = estimate_symmetry_at_t(pmx_mass_at_t, pmx_barycenters_at_t, pmx_mass_at_t_left,
                                                      pmx_barycenters_at_t_left)
                # apply symetry on t right barycenters
                pmx_barycenters_at_t = apply_sym_on_half_embryo(pmx_barycenters_at_t, sym_pmx_at_t)
            else:
                taq = times_list[pmx_idx][atimes_list[pmx_idx].index(closest_at)]
                pmx_barycenters_at_t, pmx_names_at_t, pmx_mass_at_t = (
                    get_embryo_data_at_time(list_dict_barycenters[pmx_idx], list_dict_names[pmx_idx],
                                            list_dict_mass[pmx_idx], taq))

            # print(pmx_name,len(pmx_barycenters_at_t) - len(ref_barycenters_at_t), len(pmx_barycenters_at_t))
            if abs(len(pmx_barycenters_at_t) - len(ref_barycenters_at_t)) >= 4:
                print('lenght in pmx', len(pmx_barycenters_at_t), 'in embryo', len(pmx_barycenters_at_t) * 2, 'at', t,
                      closest_at)
                print(pmx_name, 'embryo not imaged at this developmental stage', 'nb of cells in ref:',
                      len(list(dict_at_t.keys())),
                      len(pmx_barycenters_at_t))

                # if taq too far ignore pmx: if it correspond to a t_dev different much than t
                # flag = 0
                # in the case where one embryo is not imaged at this stage just remove it from analysis
                # get its name/idx and ignore it for mca
                ignored_embryos.add(pmx_name)

            # apply scaling on volume & barycenters at taq, pmx_idx
            scaling_factor = list_surface_adjustment_factors[pmx_idx][taq]
            pmx_mass_at_t = [nv * scaling_factor * scaling_factor * scaling_factor for nv in pmx_mass_at_t]
            pmx_barycenters_at_t = [nv * scaling_factor for nv in pmx_barycenters_at_t]

            individuals.append(pmx_name)

            list_dict_barycenters_at_t.append(pmx_barycenters_at_t)
            list_dict_names_at_t.append(pmx_names_at_t)
            list_dict_mass_at_t.append(pmx_mass_at_t)

            # print(pmx_name, len(list_dict_names_at_t[pmx_idx]))
            # print(list_dict_names_at_t[pmx_idx])
            ###
            #
            pmx_names_at_t_sets = set(pmx_names_at_t)
            # get commun and non-commun cell names
            common_names_all = common_names_all.intersection(pmx_names_at_t_sets)
            # non_common_names_all.append(pmx_names_at_t_sets.difference(common_names_all))
            non_common_names_all = non_common_names_all.union(pmx_names_at_t_sets)
            err_1 += f"aq time in {pmx_name}:{taq}\n"
            err_1 += f"names before merge: {pmx_names_at_t}\n"

        print('starting mca')
        # print("non common names:", non_common_names_all)
        non_common_names_all = non_common_names_all.difference(common_names_all)

        # log msg
        err_1 += f"ref time of aq:{taq_1}, dev time:{t}, "
        for i, embryo_cells in enumerate(list_dict_names_at_t, start=0):
            err_1 += f"{embryo_names[i]} nb of cells:{len(embryo_cells)}, "
        err_1 += "\n"

        ###
        ##
        # return to most common ancestry of all embryos
        ###
        for cell in non_common_names_all:
            daughter_1_name, daughter_2_name = uname.get_daughter_names(cell)
            # Include granddaughters too
            ###
            #
            # grand_daughter_1, grand_daughter_2 = get_daughters(cell)
            # grand_daughter_3, grand_daughter_4 = get_daughters(cell)

            if daughter_1_name in non_common_names_all and daughter_2_name in non_common_names_all:
                # search for daughters in every pmx and merge them
                for pmx_name, pmx_idx in embryo_idx.items():
                    if pmx_name in ignored_embryos:
                        continue

                    # either i want to find in pmx daughters or the mother or else break
                    if daughter_1_name in list_dict_names_at_t[pmx_idx] and daughter_2_name in list_dict_names_at_t[
                        pmx_idx]:
                        # get daughters barycenters and mass : theyre in the same index in the lists
                        idx_d1 = list_dict_names_at_t[pmx_idx].index(daughter_1_name)
                        idx_d2 = list_dict_names_at_t[pmx_idx].index(daughter_2_name)
                        if idx_d1 < idx_d2:
                            idx_d1, idx_d2 = idx_d2, idx_d1
                        if idx_d1 == idx_d2:
                            print("error equal indices")

                        barycenter_1 = list_dict_barycenters_at_t[pmx_idx][idx_d1]
                        barycenter_2 = list_dict_barycenters_at_t[pmx_idx][idx_d2]

                        mass_1 = list_dict_mass_at_t[pmx_idx][idx_d1]
                        mass_2 = list_dict_mass_at_t[pmx_idx][idx_d2]
                        # get new barycenter
                        merged_barycenter = barycenter_1 * mass_1 + barycenter_2 * mass_2
                        merged_barycenter = merged_barycenter / (mass_1 + mass_2)
                        # merge daughters and update the embryo pmx=
                        del list_dict_names_at_t[pmx_idx][idx_d1]
                        del list_dict_names_at_t[pmx_idx][idx_d2]
                        list_dict_names_at_t[pmx_idx].append(cell)

                        del list_dict_barycenters_at_t[pmx_idx][idx_d1]
                        del list_dict_barycenters_at_t[pmx_idx][idx_d2]
                        list_dict_barycenters_at_t[pmx_idx].append(merged_barycenter)

                        del list_dict_mass_at_t[pmx_idx][idx_d1]
                        del list_dict_mass_at_t[pmx_idx][idx_d2]
                        list_dict_mass_at_t[pmx_idx].append(mass_1 + mass_2)
                        if pmx_name == "pm1":
                            id1, id2 = list_cell_ids_in_pm1_at_t[idx_d1], list_cell_ids_in_pm1_at_t[idx_d2]
                            del list_cell_ids_in_pm1_at_t[idx_d1]
                            del list_cell_ids_in_pm1_at_t[idx_d2]
                            list_cell_ids_in_pm1_at_t.append(f"{id1}_{id2}")
                    elif daughter_1_name in list_dict_names_at_t[pmx_idx] or daughter_2_name in list_dict_names_at_t[
                        pmx_idx]:
                        err = (f"dev time: {t}, pmx:{pmx_name}, daughters:{daughter_1_name} or"
                               f" {daughter_2_name} missing \n")
                        print("a daughter do not exist in embryo pb", err)
                        file.write("A Daughter is missing in:\n")
                        file.write(err)
                        flag = 1
                    elif cell in list_dict_names_at_t[pmx_idx]:
                        pass
                        # print("cell already in embryo")
                    else:
                        file.write(
                            f"One of the ancestors did not divide yet(heterochrony or division not detected)..:{cell}"
                            f" in {pmx_name}\n")
        if flag == 1:
            flag = 0
            continue
        ##
        # some embryos should be ignored
        # check if we have same nb of cells in all embryos after the merge
        # if any(len(list_dict_names_at_t[0]) != len(names) for i, names in enumerate(list_dict_names_at_t) if
        #       embryo_names[i] not in ignored_embryos):
        # Write in output file the sizes of the embryos
        file.write("-----------------------\n")
        file.write(err_1)
        file.write("Embryo Sizes are different after merge of daughters:\n")
        # Generate the output for each embryo
        for idx, names_list in enumerate(list_dict_names_at_t):
            nb_cells = len(names_list)
            err += f"{embryo_names[idx]} nb of cells (AFTER MERGE):{nb_cells}, "
            # print(f"{embryo_names[idx]} nb of cells (AFTER MERGE):{nb_cells}")
        err += "\n"
        # Write the output to the file
        file.write(err)
        file.write("The non common names in embryos after merge (we should have the mothers from list above):\n")
        # Iterate over each embryo to identify non-common cells after merging daughters
        for idx, names_list in enumerate(list_dict_names_at_t):
            # Find non-common cells
            c_names = set(names_list).intersection(non_common_names_all)
            err += f"{embryo_names[idx]} has these non commun cells(after merge): {c_names}\n"

        # Ignore non-common cells after merging daughters: they're due to naming/segementation errors, nor cell nor its daughters exist
        c_names = set(list_dict_names_at_t[0])
        nc_names = set()
        for idx, names_list in enumerate(list_dict_names_at_t):
            if any(pmx for pmx, val in embryo_idx.items() if val == idx and pmx not in ignored_embryos):
                c_names = set(names_list).intersection(c_names)
        for idx, names_list in enumerate(list_dict_names_at_t):
            nc_names = set(names_list).union(nc_names)
        nc_names = nc_names - c_names
        err += f"Problematic cells(non commun names after merge) being ignored in the registration:{nc_names}\n"
        err += "These cells/their daughters(or one of them) do not exist in all embryos\n"
        file.write(err)
        file.write("\n ********************** \n")
        file.write("\n ********************** \n")
        file.write("\n ********************** \n")
        # Delete nc_names in each list_dict_names_at_t list, list_dict_mass_at_t, list_dict_mass_at_t, list_dict_barycenters_at_t
        print("Removing cells non existent in certain individuals")
        indices_to_delete = []
        # Iterate over each embryo
        for idx, names_list in enumerate(list_dict_names_at_t):
            # Iterate over each cell in the embryo
            for jdx in range(len(list_dict_names_at_t[idx])):
                cell = list_dict_names_at_t[idx][jdx]
                if cell in nc_names:
                    indices_to_delete.append((idx, jdx))

        # Deleting elements in reverse order to avoid index issues
        for idx, jdx in reversed(indices_to_delete):
            del list_dict_names_at_t[idx][jdx]
            del list_dict_mass_at_t[idx][jdx]
            del list_dict_barycenters_at_t[idx][jdx]
            if embryo_names[idx] == ref_name:
                del list_cell_ids_in_pm1_at_t[jdx]

        for idx, names_list in enumerate(list_dict_names_at_t):
            nb_cells = len(names_list)
            err += f"{embryo_names[idx]} nb of cells (AFTER MERGE and removing nc names):{nb_cells}, "
            # print(f"{embryo_names[idx]} nb of cells (AFTER MERGE and removing nc names):{nb_cells}")
        err += "\n"
        # Write the output to the file
        file.write(err)

        # else:
        dict_embryos_in_same_ref[t] = {}
        for embryo_name, i in embryo_idx.items():
            # ignore not imaged embryos idx/name at this stage
            if embryo_name in ignored_embryos:
                continue
            dict_embryos_in_same_ref[t][embryo_name] = {}

        ###
        ##
        # registering embryos after most common ancestry
        ##
        # take ref embryo
        ref_bary = list_dict_barycenters_at_t[0]
        ref_names = list_dict_names_at_t[0]
        ref_names, ref_bary, ref_ids = sort_lists_according_to_cell_name(ref_names, ref_bary, list_cell_ids_in_pm1_at_t)

        all_cell_names.update(ref_names)

        print('embryo size at t:', len(ref_names))
        ######
        # register each of the embryos (global registration)
        for pmx_name, pmx_idx in embryo_idx.items():
            # ignore not imaged embryos idx/name at this stage
            if pmx_name in ignored_embryos:
                continue
            if pmx_name == ref_name:
                continue
            # print('registering embryo',pmx_name, list_dict_names_at_t[pmx_idx].index('b7.0003*'))
            # get pmx barycenters and names
            flo_bary = list_dict_barycenters_at_t[pmx_idx]
            flo_names = list_dict_names_at_t[pmx_idx]

            # sort appropriately
            flo_names, flo_bary = get_vectors_before_registration(ref_names, flo_bary, flo_names)
            #
            # compute transform: should we use a similitude/rigid?
            transform, ref, floating = compute_similitude(ref_bary, flo_bary, transformation_type,
                                                          retained_fraction)

            # make transform rigid, its transform homogeneous coordinates
            transform = similitude_without_scaling(transform)
            # apply transform
            transformed_floating = apply_transformation(transform, floating)
            # save barycenters in
            for i, cell_name in enumerate(flo_names):
                dict_embryos_in_same_ref[t][ref_name][cell_name] = ref[:, i]
                dict_embryos_in_same_ref[t][pmx_name][cell_name] = transformed_floating[:, i]

        ###
        #

        print('dev time', t, 'nb of cell in ref', len(dict_at_t), 'nb of cells after mca:',
              len(ref[0]))

        # in the fcts called ignore the ignored embryonames
        embryo_names_t = [name for name in embryo_names if name not in ignored_embryos]

        # distance matrix of geometrical feature(cells barycenters) at each t
        global_avg_distances_matrix, dimensions_ = distance_matrix_geom_btw_individuals_at_t(
            dict_embryos_in_same_ref[t],
            sorted(embryo_names_t))

        time = f'{len(dict_at_t)}_{t}'
        print('wuhu', time, embryo_names_t)
        # plot global_avg_distances_matrix at t
        write_plot_script_global_distance_btw_embryos(global_avg_distances_matrix, dimensions_, time,
                                                      sorted(embryo_names_t),
                                                      output_folder, 'geometric', embryo_res)

        write_plot_script_mds_distance_btw_embryos(global_avg_distances_matrix, dimensions_, time,
                                                   sorted(embryo_names_t),
                                                   output_folder, 'geometric', embryo_res)

        if len(ref[0]) in [64, 76, 112, 184, 218]:
            write_plot_script_global_distance_btw_embryos(global_avg_distances_matrix, dimensions_, time,
                                                          sorted(embryo_names_t), output_folder, 'geometric',
                                                          embryo_res)
            # plot distance matrix mds
            write_plot_script_mds_distance_btw_embryos(global_avg_distances_matrix, dimensions_,
                                                       len(ref[0]),
                                                       sorted(embryo_names_t), output_folder, distance_type='geometric',
                                                       embryo_res='complete')

        if embryo_res == 'halfs':
            if len(dict_at_t) in [32, 38, 56, 92, 109]:
                write_plot_script_global_distance_btw_embryos(global_avg_distances_matrix, dimensions_, time,
                                                              sorted(embryo_names_t), output_folder,
                                                              'of barycenters positions',
                                                              embryo_res)
                # plot distance matrix mds
                write_plot_script_mds_distance_btw_embryos(global_avg_distances_matrix, dimensions_,
                                                           len(dict_at_t),
                                                           sorted(embryo_names_t), output_folder,
                                                           distance_type='geometric',
                                                           embryo_res='halfs')
    # update vectors
    print('all cells lenghts', len(all_cell_names))
    for t in dict_embryos_in_same_ref.keys():
        for pmx in dict_embryos_in_same_ref[t].keys():
            for cell in all_cell_names:
                if cell not in dict_embryos_in_same_ref[t][pmx]:
                    dict_embryos_in_same_ref[t][pmx][cell] = np.array([0, 0, 0])

    # umap for: dict_embryos_in_same_ref[t][pmx_name][cell]=3d_coordinate
    generate_umap_script(dict_embryos_in_same_ref, all_cell_names, ref_name, embryo_names, output_folder)

    file.close()


def update_prev_ref_co_registered(t, times_ref, prev_ref_names,
                                  ref_names_at_t, ref_barycenters_at_t, transformation_type, retained_fraction,
                                  prev_ref_co_registered, tstart=0):
    """
    Updates the prev_ref_co_registered dictionary by registering the reference embryo at time t to the reference embryo at t-4 or t=0.

    Parameters:
    t (int): Current time point.
    times_ref (list): List of reference times.
    list_dict_names (list): List of dictionaries containing cell names.
    list_dict_barycenters (list): List of dictionaries containing barycenters.
    list_surface_adjustment_factors (list): List of scaling factors for each time point.
    ref_names_at_t (list): List of reference names at time t.
    ref_barycenters_at_t (list): List of reference barycenters at time t.
    transformation_type (str): Type of transformation to apply.
    retained_fraction (float): Fraction of cells to retain during registration.
    prev_ref_co_registered (dict): Dictionary to store registered barycenters.

    Returns:
    dict: Updated prev_ref_co_registered dictionary.
    """
    if t == tstart or t == tstart+1:
        prev_ref_co_registered[t] = ref_barycenters_at_t
        prev_ref_names[t] = ref_names_at_t
        return prev_ref_co_registered, prev_ref_names
    elif t > tstart+4:
        taq_prev = times_ref[times_ref.index(t) - 4]
    else:
        taq_prev = times_ref[tstart]

    #dict_prev = get_embryo_at_t(list_dict_names[0], taq_prev)
    #ref_names_prev = list(dict_prev.values())
    ref_names_prev = prev_ref_names[taq_prev]

    # Get barycenter at taq_prev registered
    ref_barycenters_prev = prev_ref_co_registered[taq_prev]

    # Get common cell names
    common_names = set(ref_names_at_t) & set(ref_names_prev)
    common_barycenters_t = []
    common_barycenters_prev = []

    print('t previous and t current', taq_prev, t)
    print(len(ref_names_prev))
    print(len(ref_barycenters_prev))

    for cell_name in common_names:
        idx_t = ref_names_at_t.index(cell_name)
        idx_prev = ref_names_prev.index(cell_name)
        #print(idx_prev, ref_names_prev[idx_prev])
        #print(ref_barycenters_prev[idx_prev])
        common_barycenters_t.append(ref_barycenters_at_t[idx_t])
        common_barycenters_prev.append(ref_barycenters_prev[idx_prev])

    common_barycenters_t = np.array(common_barycenters_t)
    common_barycenters_prev = np.array(common_barycenters_prev)

    # Apply registration of cells barycenters in ref_at_t-4 to ref_at_t
    transform, ref, floating = compute_similitude(common_barycenters_prev, common_barycenters_t, transformation_type,
                                                  retained_fraction)
    transform = similitude_without_scaling(transform)
    transformed_floating = apply_transformation(transform, np.array(ref_barycenters_at_t).T)
    transformed_floating = transformed_floating.T

    ref_barycenters_at_t = [point for point in transformed_floating]
    # Save registered ref at t barycenters
    prev_ref_co_registered[t] = ref_barycenters_at_t
    prev_ref_names[t] = ref_names_at_t
    print('lenght in prev ref names', len(prev_ref_names), 'len in prev ref bary', len(prev_ref_co_registered))
    return prev_ref_co_registered, prev_ref_names


def align_cell_data(names_dict, mass_dict, barycenters_dict, scaling_factor):
    """
    Align cell data from different dictionaries and return lists with corresponding values.

    Parameters:
    names_dict (dict): Dictionary of cell names.
    mass_dict (dict): Dictionary of cell masses.
    barycenters_dict (dict): Dictionary of cell barycenters.
    scaling_factor (float): Scaling factor for mass and barycenters.

    Returns:
    list: Aligned cell IDs.
    list: Aligned cell names.
    list: Aligned cell masses.
    list: Aligned cell barycenters.
    """
    # Ensure dictionaries are sorted by cell ID
    names_dict = dict(sorted(names_dict.items(), key=lambda x: x[0]))
    mass_dict = dict(sorted(mass_dict.items(), key=lambda x: x[0]))
    barycenters_dict = dict(sorted(barycenters_dict.items(), key=lambda x: x[0]))

    # Apply scaling to mass and barycenters
    mass_dict = {k: v * scaling_factor ** 3 for k, v in mass_dict.items()}
    barycenters_dict = {k: v * scaling_factor for k, v in barycenters_dict.items()}

    # Get common keys
    common_keys = set(names_dict.keys()) & set(mass_dict.keys()) & set(barycenters_dict.keys())

    # Create aligned lists
    aligned_ids = []
    aligned_names = []
    aligned_masses = []
    aligned_barycenters = []

    for key in common_keys:
        aligned_ids.append(key)
        aligned_names.append(names_dict[key])
        aligned_masses.append(mass_dict[key])
        aligned_barycenters.append(barycenters_dict[key])

    return aligned_ids, aligned_names, aligned_masses, aligned_barycenters


def compute_cell_reg_dispersion(cell_barycenters, cell_volumes):
    """
    Given cell volumes and barycenter positions in different individuals,
    compute cells' geometric mean of registration dispersion normalized by the cell's average radius
    (assuming cell is spherical).

    Parameters
    ----------
    cell_barycenters (list) : list of barycenter positions in different individuals
    cell_volumes (list) : list of cell volumes in different individuals

    Returns
    -------
    float : geometric mean of registration dispersion
    """
    if len(cell_barycenters) == 0 or len(cell_volumes) == 0:
        return None

    barycenters = np.array(cell_barycenters)
    volumes = np.array(cell_volumes)

    # Compute eigenvalues of barycenters positions
    covariance_matrix = np.cov(barycenters, rowvar=False)
    eigenvalues, _ = np.linalg.eig(covariance_matrix)

    # Compute geometric mean of the 3 eigenvalues
    geom_mean = np.cbrt(np.prod(eigenvalues))

    # Calculate the average radius assuming the cell is spherical
    radii = (3 * volumes / (4 * np.pi)) ** (1 / 3)

    # Normalize by the average radius
    cell_relative_dispersion = geom_mean / radii.mean()

    return cell_relative_dispersion

def script_boxplot_at_each_t_cells_reg_dispersion(barycenters_in_common_ref, cells_volumes_in_common_ref,
                                                  nb_of_cells_in_common, output_folder):
    """
    Create boxplots of cells' relative geometric mean of eigenvalue at each time point t.

    Parameters
    ----------
    barycenters_in_common_ref : {t: {'cell name': [barycenter position in common ref]}}
    cells_volumes_in_common_ref : {t: {'cell name': [cell volume in common ref]}}
    output_folder : str : directory to save the boxplot images

    Returns
    -------
    None
    """
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    cell_dispersions_at_t = []
    times = []
    nb_cells = []
    for t in barycenters_in_common_ref.keys():
        cell_dispersion = []
        nb_cells.append(nb_of_cells_in_common[t])
        for cell_name in barycenters_in_common_ref[t].keys():
            cell_barycenters = barycenters_in_common_ref[t][cell_name]
            cell_volumes = cells_volumes_in_common_ref[t][cell_name]
            dispersion = compute_cell_reg_dispersion(cell_barycenters, cell_volumes)
            if dispersion is not None:
                cell_dispersion.append(dispersion)
        cell_dispersions_at_t.append(cell_dispersion)
        times.append(t)

    script_path = os.path.join(output_folder, 'registration_dispersion_overtime_boxplots.py')
    print('plot in', script_path)
    with open(script_path, 'w') as script_file:
        script_file.write(f"""
import os
import numpy as np
import matplotlib.pyplot as plt

cell_dispersions_at_t = {cell_dispersions_at_t}
times = {times}
nb_cells = {nb_cells}
output_folder = '{output_folder}'


fig, ax1 = plt.subplots()

color = 'tab:blue'
ax1.set_xlabel('Developmental Times')
ax1.set_ylabel('Relative Dispersion', color=color)
bp = ax1.boxplot(cell_dispersions_at_t, positions=times)
ax1.tick_params(axis='y', labelcolor=color)

for median in bp['medians']:
    median.set_color(color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:red'
ax2.set_ylabel('Number of Cells in Common', color=color)
line2, = ax2.plot(times, nb_cells, color=color, label='Number of Cells in Common')
ax2.tick_params(axis='y', labelcolor=color)


plt.xticks(rotation=90)
plt.title('Cell Registration Dispersion Over Time')

# Add legends
ax1.legend([bp["medians"][0]], ['Relative Dispersion'], loc='upper left')
ax2.legend(['Number of Cells in Common'], loc='upper right')
fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.savefig(os.path.join(output_folder, 'registration_dispersion_overtime_boxplots.png'))
plt.show()
""")


def similitude_without_scaling(similitude_matrix):
    """
    Converts a similitude transformation matrix into a rigid transformation matrix by removing the scaling component.

    Parameters:
    similitude_matrix (numpy.ndarray): A 4x4 similitude transformation matrix.

    Returns:
    numpy.ndarray: A 4x4 rigid transformation matrix.
    """
    if similitude_matrix.shape != (4, 4):
        raise ValueError("Input similitude matrix must be of shape (4, 4)")

    # Extract rotation and translation components
    rotation_scaling_matrix = similitude_matrix[:3, :3]
    translation_vector = similitude_matrix[:3, 3]

    # Remove scaling by normalizing the rotation matrix
    scale_factor = np.cbrt(np.linalg.det(rotation_scaling_matrix))
    rotation_matrix = rotation_scaling_matrix / scale_factor

    # Construct the rigid transformation matrix
    rigid_matrix = np.eye(4)
    rigid_matrix[:3, :3] = rotation_matrix
    rigid_matrix[:3, 3] = translation_vector

    return rigid_matrix


# to compare embryos surfaces of contacts at different devlopmental stages
# they need to be in the same refferential(volume and time alignement)

def compute_topological_distance_matrix_btw_embryos(embryo_idx, list_dict_names_at_t, list_dict_surfaces_at_t):
    """
    Computes the distance matrix between different embryos based on the sum of distances
    between their cells' surface contacts.

    Parameters
    ----------
    embryo_idx : dict
        Dictionary where keys are embryo names and values are their respective indices.
    list_dict_names_at_t : list of dict
        List of dictionaries where each dictionary contains 'id_in_pmx' as keys and cell names as values.
    list_dict_surfaces_at_t : list of dict
        List of dictionaries where each dictionary contains 'id_in_pmx' as keys and
        cell surface of contact dictionaries as values.

    Returns
    -------
    distance_matrix : dict
        Dictionary where keys are embryo pairs (tuple) and values are the computed distances.
    """
    global ref_surfaces_cell
    embryo_names = sorted(list(embryo_idx.keys()))

    distance_matrix = np.zeros((len(embryo_names), len(embryo_names)))

    normalising_factors = np.zeros((len(embryo_names), len(embryo_names)))

    # Compute distances between every pair of embryos
    for i, ref_name in enumerate(embryo_names):
        ref_idx = embryo_idx[ref_name]
        ref_names = list_dict_names_at_t[ref_idx]
        ref_surfaces = list_dict_surfaces_at_t[ref_idx]

        for j, flo_name in enumerate(embryo_names):
            if i >= j:
                continue  # Avoid duplicate computations

            flo_idx = embryo_idx[flo_name]
            flo_names = list_dict_names_at_t[flo_idx]
            flo_surfaces = list_dict_surfaces_at_t[flo_idx]

            total_distance = 0
            dim = 0

            # Compute distance between homologous cells based on names
            for ref_id, ref_cell_name in ref_names.items():
                if ref_cell_name in flo_names.values():
                    ref_surfaces_cell = 0
                    flo_surfaces_cell = 0

                    flo_id = next(key for key, value in flo_names.items() if value == ref_cell_name)

                    ref_contacts_values = ref_surfaces.get(ref_id, {})  # {for cell in ref: {n_ids:surface_of_contact}}
                    flo_contacts_values = flo_surfaces.get(flo_id, {})

                    ref_contacts_names = {neighbor_id: ref_names[neighbor_id] for neighbor_id in
                                          ref_contacts_values.keys() if
                                          neighbor_id in ref_names}  # neighbour names of cell in ref embryo
                    flo_contacts_names = {neighbor_id: flo_names[neighbor_id] for neighbor_id in
                                          flo_contacts_values.keys() if neighbor_id in flo_names}

                    # Compute L1 norm between corresponding cells' surface contact values
                    all_neighbors_names = set(ref_contacts_names.values()).union(flo_contacts_names.values())
                    for neighbor_name in all_neighbors_names:
                        ref_surface = next((ref_contacts_values[n_id] for n_id, n_name in ref_contacts_names.items()
                                            if n_name == neighbor_name), 0)
                        flo_surface = next((flo_contacts_values[n_id] for n_id, n_name in flo_contacts_names.items()
                                            if n_name == neighbor_name), 0)
                        ref_surfaces_cell += ref_surface
                        flo_surfaces_cell += flo_surface
                        total_distance += abs(ref_surface - flo_surface)

                    dim += 1

            # divide by cells sum of surfaces of contact
            total_distance /= (ref_surfaces_cell + flo_surfaces_cell)

            distance_matrix[i, j] = total_distance
            distance_matrix[j, i] = total_distance

            # also store len(all_neighbors_names)
            normalising_factors[i, j] = dim
            normalising_factors[j, i] = dim

    return distance_matrix, normalising_factors


def volume_fitting_and_voxelsize_correction(pm_volume_prop, target_volume=60000000):
    """
    Fitting parameters of embryo volume overtime and calculate voxel size correction
    to simulate an embryo of a given target volume.

    to be multiplied by its square for surfaces, cube for volumes adjustments

    Parameters
    ----------
    pm_volume_prop : dict
        Dictionary where keys are cell IDs and values are their corresponding volumes.
    target_volume : int, optional
        The desired target volume to scale to, default is 60,000,000.

    Returns
    -------
    scaling_factors: dict of float
        Embryo scaling factoras at each timepoint. {'t': scaling_factor}
    """
    # Step 1: Fit the volume data over time
    volume_along_time = {}

    scaling_factors = {}

    # Compute embryo volume for each timepoint 't'
    for c in pm_volume_prop:
        t = int(c) // 10000
        volume_along_time[t] = volume_along_time.get(t, 0) + pm_volume_prop[c]

    # Get array of time points (x) and array of volumes (y)
    x = sorted(volume_along_time.keys())
    y = [volume_along_time[i] for i in x]

    # Robust regression via RANSAC
    xnp = np.array(x)[:, np.newaxis]
    ynp = np.array(y)[:, np.newaxis]
    ransac = RANSACRegressor()
    ransac.fit(xnp, ynp)

    # Extracting coefficients
    (a, b) = (ransac.estimator_.coef_[0][0], ransac.estimator_.intercept_[0])

    # Step 2: Calculate the voxel size correction
    for timepoint in x:
        t_volume = a * timepoint + b
        scaling_factors[timepoint] = np.cbrt(target_volume / t_volume)
    return scaling_factors


def all_embryos_in_same_topological_referential_distance_matrix(files, ref_filename, output_folder='',
                                                                embryo_res='halfs', imaging_res=0.3):
    # imagin resolution is 0.3x0.3x0.3 um

    list_dict_surfaces = []
    list_dict_names = []
    list_dict_mass = []

    embryo_names = []
    embryos_props = {}
    embryo_idx = {}

    atimes_list = []
    times_list = []

    list_surface_adjustment_factors = []  # list of dict [pmx_idx]{'taq': scaling_factor}

    err = ""

    # get surfaces of contacts instead of barycenters
    # get embryos
    with open(ref_filename, 'rb') as f:
        ref_props_data = {}
        ref_name = ref_filename.split('-')[1]
        ref_props = pkl.load(f)
        ref_props_data[ref_name] = ref_props

        # compute volume alignment parms
        volume_adjustement_factors = volume_fitting_and_voxelsize_correction(ref_props['cell_volume'],
                                                                             target_volume=60000000)

        volume_adjustement_factors = {t: factor * imaging_res for t, factor in volume_adjustement_factors.items()}

        #  handle case of embryo_resolution
        if embryo_res == 'halfs':
            left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                ref_props['cell_name'], ref_props['cell_contact_surface'])
            list_dict_surfaces.append(left_prop_dict)
            list_dict_surfaces.append(right_prop_dict)

            list_dict_names.append(left_names_dict)
            list_dict_names.append(right_names_dict)

            left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                ref_props['cell_name'], ref_props['cell_volume'])
            list_dict_mass.append(left_prop_dict)
            list_dict_mass.append(right_prop_dict)

            i = len(embryo_idx)
            embryo_idx[ref_name + '_left'] = i
            embryo_idx[ref_name + '_right'] = i + 1
            embryo_names.append(ref_name + '_left')
            embryo_names.append(ref_name + '_right')

            # add
            list_surface_adjustment_factors.append(volume_adjustement_factors)
            list_surface_adjustment_factors.append(volume_adjustement_factors)

        else:
            list_dict_surfaces.append(ref_props['cell_contact_surface'])
            list_dict_names.append(ref_props['cell_name'])
            list_dict_mass.append(ref_props['cell_volume'])
            embryo_names.append(ref_name)
            i = len(embryo_idx)
            embryo_idx[ref_name] = i

            #  add
            list_surface_adjustment_factors.append(volume_adjustement_factors)

        ###

    # get surfaces of contacts instead of barycenters
    for filename in files:
        if filename == ref_filename:
            continue
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)
            embryo_name = filename.split('-')[1]

            embryos_props[embryo_name] = {
                'surface_of_contact': pm_props['cell_contact_surface'],
                'cell_name': pm_props['cell_name'],
                'cell_volume': pm_props['cell_volume'],
                'cell_lineage': pm_props['cell_lineage']
            }

            # compute volume alignment parms
            volume_adjustement_factors = volume_fitting_and_voxelsize_correction(pm_props['cell_volume'],
                                                                                 target_volume=60000000)
            volume_adjustement_factors = {t: factor * imaging_res for t, factor in volume_adjustement_factors.items()}

            # handle a case of embryo_resolution
            #
            if embryo_res == 'halfs':
                left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                    pm_props['cell_name'], pm_props['cell_contact_surface'])
                list_dict_surfaces.append(left_prop_dict)
                list_dict_surfaces.append(right_prop_dict)

                list_dict_names.append(left_names_dict)
                list_dict_names.append(right_names_dict)

                left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                    pm_props['cell_name'], pm_props['cell_volume'])
                list_dict_mass.append(left_prop_dict)
                list_dict_mass.append(right_prop_dict)

                i = len(embryo_idx)
                embryo_idx[embryo_name + '_left'] = i
                embryo_idx[embryo_name + '_right'] = i + 1

                embryo_names.append(embryo_name + '_left')
                embryo_names.append(embryo_name + '_right')

                # add adjustement parameters of volumes
                list_surface_adjustment_factors.append(volume_adjustement_factors)
                list_surface_adjustment_factors.append(volume_adjustement_factors)

            else:
                i = len(embryo_idx)
                embryo_idx[embryo_name] = i
                list_dict_surfaces.append(pm_props['cell_contact_surface'])
                list_dict_names.append(pm_props['cell_name'])
                list_dict_mass.append(pm_props['cell_volume'])
                embryo_names.append(embryo_name)

                #  add adjustement parameters of volumes
                list_surface_adjustment_factors.append(volume_adjustement_factors)

    output_file = os.path.join(output_folder, "log_file.log")
    file = open(output_file, "w")
    file.write('Hello')

    # do temporal alignment
    temporal_alignment_coefficient_dict, _ = get_temporal_alignment_coefficients_of_embryos(embryos_props,
                                                                                            ref_props_data)

    #
    #  delete embryos_props
    del embryos_props
    del ref_props_data

    # for each embryo compute adjusted times
    times_ref = utimes.get_embryo_times(ref_props['cell_lineage'])
    a, b = temporal_alignment_coefficient_dict[ref_name]
    a_times_ref = [a * t + b for t in times_ref]

    for i, embryo in enumerate(embryo_names):
        times = utimes.get_embryo_times(list_dict_surfaces[i])
        if embryo_res == 'halfs':
            embryo = embryo.split('_')[0]
            a, b = temporal_alignment_coefficient_dict[embryo]
            a_times = [a * t + b for t in times]
            atimes_list.append(a_times)
            times_list.append(times)
        else:
            a, b = temporal_alignment_coefficient_dict[embryo]
            a_times = [a * t + b for t in times]
            atimes_list.append(a_times)
            times_list.append(times)

    if embryo_res == 'halfs':
        ref_name = ref_name + '_left'

    ##
    ##
    #

    for t in a_times_ref:
        print('working for dev time in ref:', t)
        flag = 0  # skips t if cells are missing cz MCA wount be gd
        # t is dev time in ref we're currently putting all embryos in the same referential at this dev t
        ignored_embryos = set()
        non_ignored_embryos_dxs = {}

        list_dict_surfaces_at_t = []
        list_dict_names_at_t = []

        t = int(t)
        # t aquisition
        taq_1 = times_ref[a_times_ref.index(t)]
        err_1 = f"aq time in pm1:{taq_1}\n"
        # pm1 at t_aq1
        dict_at_t = get_embryo_at_t(list_dict_names[0], taq_1)
        dict_at_t = dict(sorted(dict_at_t.items(), key=lambda x: x[0]))
        ref_names_at_t = dict_at_t

        dict_at_t = get_embryo_at_t(list_dict_surfaces[0], taq_1)
        dict_at_t = dict(sorted(dict_at_t.items(), key=lambda x: x[0]))
        # adjust surfaces of pm1 at this aq time: taq_1
        scaling_factor = list_surface_adjustment_factors[0][taq_1]
        dict_at_t = {k: {nk: nv * scaling_factor * scaling_factor for nk, nv in v.items()} for k, v in
                     dict_at_t.items()}
        ref_surfaces_at_t = dict_at_t

        list_dict_surfaces_at_t.append(ref_surfaces_at_t)
        list_dict_names_at_t.append(ref_names_at_t)

        common_names_all = set(list(ref_names_at_t.values()))  # Start with PM1 names
        non_common_names_all = set(list(ref_names_at_t.values()))

        non_ignored_embryos_dxs[ref_name] = 0
        print('lenght in ref', len(ref_names_at_t), 'in embryo', len(ref_names_at_t) * 2, 'at', t)
        n = len(ref_names_at_t)
        print('n at t', n, t)
        ##
        # Grab embryos at needed dev_t
        # for most common ancestry (putting all embryos in same ref) I need to know common cells and non common cells
        ##
        individuals = [ref_name]
        for pmx_name, pmx_idx in embryo_idx.items():
            if pmx_idx == 0:
                continue
            closest_at = min(atimes_list[pmx_idx], key=lambda x: abs(x - t))

            taq = times_list[pmx_idx][atimes_list[pmx_idx].index(closest_at)]
            pmx_surfaces_at_t, pmx_names_at_t, pmx_mass_at_t = (
                get_embryo_dict_at_time(list_dict_surfaces[pmx_idx], list_dict_names[pmx_idx],
                                        list_dict_mass[pmx_idx], taq))

            if abs(len(pmx_surfaces_at_t) - len(ref_surfaces_at_t)) >= 4:
                print('not imaged at this stage:', pmx_name, 'lenght in pmx', len(pmx_names_at_t), 'in embryo', len(pmx_names_at_t) * 2, 'at', t,
                      closest_at)
                #print('embryo not imaged at this developmental stage', 'nb of cells in ref:',
                #      len(list(ref_names_at_t.keys())),'in embryo',2*len(list(ref_names_at_t.keys())) )
                # if taq too far ignore pmx: if it correspond to a t_dev different much than t
                # flag = 0
                # in the case where one embryo is not imaged at this stage just remove it from analysis
                # get its name/idx and ignore it for mca
                ignored_embryos.add(pmx_name)
            else:
                non_ignored_embryos_dxs[pmx_name] = pmx_idx

            # adjust pmx surfaces: taq
            scaling_factor = list_surface_adjustment_factors[pmx_idx][taq]
            pmx_surfaces_at_t = {k: {nk: nv * scaling_factor * scaling_factor for nk, nv in v.items()} for k, v in
                                 pmx_surfaces_at_t.items()}

            individuals.append(pmx_name)

            list_dict_surfaces_at_t.append(pmx_surfaces_at_t)
            list_dict_names_at_t.append(pmx_names_at_t)
            print('in', pmx_name)
            #print(pmx_names_at_t)
            ###
            #
            pmx_names_at_t_sets = set(list(pmx_names_at_t.values()))
            # get commun and non-commun cell names
            common_names_all = common_names_all.intersection(pmx_names_at_t_sets)
            # non_common_names_all.append(pmx_names_at_t_sets.difference(common_names_all))
            non_common_names_all = non_common_names_all.union(pmx_names_at_t_sets)
            err_1 += f"aq time in {pmx_name}:{taq}\n"
            err_1 += f"names before merge: {list(pmx_names_at_t.values())}\n"


        non_common_names_all = non_common_names_all.difference(common_names_all)
        print('starting mca')
        print("non common names:", len(non_common_names_all), 'nb of comon names:', len(common_names_all))
        non_common_names_all = non_common_names_all.difference(common_names_all)

        # log msg
        err_1 += f"ref time of aq:{taq_1}, dev time:{t}, "
        for i, embryo_cells in enumerate(list_dict_names_at_t, start=0):
            err_1 += f"{embryo_names[i]} nb of cells:{len(embryo_cells)}, "
        err_1 += "\n"

        ###
        ##
        # return to most common ancestry of all embryos
        ###
        # correct merging in the return to most common ancester
        for cell in non_common_names_all:
            daughter_1_name, daughter_2_name = uname.get_daughter_names(cell)
            # Include granddaughters too
            ###
            #
            # grand_daughter_1, grand_daughter_2 = get_daughters(cell)
            # grand_daughter_3, grand_daughter_4 = get_daughters(cell)

            if daughter_1_name in non_common_names_all and daughter_2_name in non_common_names_all:
                # search for daughters in every pmx and merge them
                for pmx_name, pmx_idx in embryo_idx.items():
                    if pmx_name in ignored_embryos:
                        continue
                    # either i want to find in pmx daughters or the mother or else break
                    if daughter_1_name in list_dict_names_at_t[pmx_idx].values() and daughter_2_name in list_dict_names_at_t[pmx_idx].values():
                        # both daughters found in pmx,merge them
                        # print('merging', err)
                        # get ids of daughter_1_name and daughter_2_name
                        #  in list_dict_names_at_t[pmx_idx]
                        id_d1 = None
                        id_d2 = None
                        for cell_id, name in list_dict_names_at_t[pmx_idx].items():
                            if name == daughter_1_name:
                                id_d1 = cell_id
                            elif name == daughter_2_name:
                                id_d2 = cell_id
                        if id_d1 is None or id_d2 is None:
                            print(
                                f"Could not find ids for {daughter_1_name} or {daughter_2_name} "
                                f"in list_dict_names_at_t[pmx_idx]")

                        # print('merging ids', id_d1, id_d2, 'daughters', daughter_1_name, daughter_2_name)

                        #if id_d1 > id_d2:
                        #    id_d1, id_d2 = id_d2, id_d1

                        surfaces_1 = list_dict_surfaces_at_t[pmx_idx][id_d1]  # {'n_id': surface_of_contact}
                        surfaces_2 = list_dict_surfaces_at_t[pmx_idx][id_d2]

                        # adjust merge daughters surfaces
                        surfaces_m = surfaces_1.copy()  # Start with a copy of surfaces_1
                        for n_id, surface in surfaces_2.items():
                            if n_id in surfaces_m:
                                surfaces_m[n_id] += surface
                            else:
                                surfaces_m[n_id] = surface

                        # Ensure surfaces_m does not contain ids of the daughters themselves
                        surfaces_m.pop(id_d1, None)
                        surfaces_m.pop(id_d2, None)

                        # store surfaces_m in list_dict_surfaces_at_t[pmx_idx] and remove surfaces_1 and surfaces_2
                        merged_id = f"{id_d1}_{id_d2}"
                        list_dict_surfaces_at_t[pmx_idx][merged_id] = surfaces_m
                        del list_dict_surfaces_at_t[pmx_idx][id_d1]
                        del list_dict_surfaces_at_t[pmx_idx][id_d2]

                        # update daughters neighborhood surfaces and names
                        # other cells in list_dict_surfaces_at_t[pmx_idx] if they have a surface of contact with
                        # id_1 or id_2 they should be replace by contact with the newly merged cell
                        for n_id, surfaces in list_dict_surfaces_at_t[pmx_idx].items():
                            if id_d1 in surfaces:
                                if merged_id in surfaces:
                                    surfaces[merged_id] += surfaces.pop(id_d1)
                                else:
                                    surfaces[merged_id] = surfaces.pop(id_d1)
                            if id_d2 in surfaces:
                                if merged_id in surfaces:
                                    surfaces[merged_id] += surfaces.pop(id_d2)
                                else:
                                    surfaces[merged_id] = surfaces.pop(id_d2)

                        # merge daughters and update the embryo pmx=
                        del list_dict_names_at_t[pmx_idx][id_d1]
                        del list_dict_names_at_t[pmx_idx][id_d2]
                        list_dict_names_at_t[pmx_idx][merged_id] = cell
                        #####
                        #####
                        ##
                    elif (daughter_1_name in list_dict_names_at_t[pmx_idx].values() or
                          daughter_2_name in list_dict_names_at_t[pmx_idx].values()):
                        # Find the keys corresponding to daughter_1_name and daughter_2_name
                        daughter_1_key = None
                        daughter_2_key = None
                        for cell_id, name in list_dict_names_at_t[pmx_idx].items():
                            if name == daughter_1_name:
                                daughter_1_key = cell_id
                            elif name == daughter_2_name:
                                daughter_2_key = cell_id
                        err = (f"Pb cell not found dev time: {t}, cell ids: {daughter_1_key}, {daughter_2_key}, pmx: {pmx_name},"
                               f" daughters: {daughter_1_name} or {daughter_2_name} missing \n")
                        print(f"Pb cell not found  dev time: {t}, cell ids: {daughter_1_key}, {daughter_2_key}, pmx: {pmx_name},"
                               f" daughters: {daughter_1_name} or {daughter_2_name} missing \n")
                        flag = 1
                    elif cell in list_dict_names_at_t[pmx_idx].values():
                        pass
                        # print("cell already in embryo")
                    else:
                        file.write(
                            f"One of the ancestors did not divide yet(heterochrony or"
                            f" division not detected)..:{cell} in {pmx_name}\n")
        if flag == 1:
            flag = 0
            continue
        ##
        # check if we have same nb of cells in all embryos after the merge
        # if any(len(list_dict_names_at_t[0]) != len(names) for names in list_dict_names_at_t[1:]):

        # Write in output file the sizes of the embryos
        file.write("-----------------------\n")
        file.write(err_1)
        file.write("Embryo Sizes are different after merge of daughters:\n")
        # Generate the output for each embryo
        for idx, names_dict in enumerate(list_dict_names_at_t):
            nb_cells = len(list(names_dict.values()))
            err += f"{embryo_names[idx]} nb of cells (AFTER MERGE):{nb_cells}, "
            print(f"{embryo_names[idx]} nb of cells (AFTER MERGE):{nb_cells}")
        err += "\n"
        # Write the output to the file
        file.write(err)
        file.write("The non common names in embryos after merge (we should have the mothers from list above):\n")
        # Iterate over each embryo to identify non-common cells after merging daughters
        for idx, names_dict in enumerate(list_dict_names_at_t):
            # ignore ignored embryos
            # Find non-common cells
            c_names = set(names_dict.values()).intersection(non_common_names_all)
            err += f"{embryo_names[idx]} has these non-common cells (after merge): {c_names}\n"

        # Ignore non-common cells after merging daughters: they're due to naming/segementation errors,
        # nor cell nor its daughters exist
        c_names = set(list(list_dict_names_at_t[0].values()))
        nc_names = set()
        #print("list dict",list_dict_names_at_t)
        for idx, names_list in enumerate(list_dict_names_at_t):
            if idx in list(non_ignored_embryos_dxs.values()):
                c_names = set(list(names_list.values())).intersection(c_names)
        for idx, names_list in enumerate(list_dict_names_at_t):
            nc_names = set(list(names_list.values())).union(nc_names)
        nc_names = nc_names - c_names
        err += f"Problematic cells(non commun names after merge) being ignored in the registration:{nc_names}\n"
        err += "These cells/their daughters(or one of them) do not exist in all embryos\n"
        file.write(err)
        file.write("\n ********************** \n")
        file.write("\n ********************** \n")
        file.write("\n ********************** \n")
        # Delete nc_names in each list_dict_names_at_t list, list_dict_mass_at_t, list_dict_mass_at_t,
        # list_dict_barycenters_at_t
        print("Removing cells non existent in certain individuals", len(nc_names))
        # print(nc_names)

        for idx, names_dict in enumerate(list_dict_names_at_t):
            # Identify cells to delete
            ids_to_delete = [cell_id for cell_id, name in names_dict.items() if name in nc_names]
            #print(ids_to_delete, idx)
            # Delete identified cells
            for cell_id in ids_to_delete:
                del list_dict_names_at_t[idx][cell_id]

        for idx, names_dict in enumerate(list_dict_names_at_t):
            nb_cells = len(list(names_dict.values()))
            err += f"{embryo_names[idx]} nb of cells (AFTER MERGE and removing nc names):{nb_cells},"
            print(f"{embryo_names[idx]} nb of cells (AFTER MERGE and removing nc names):{nb_cells}")
        err += "\n"

        # else:
        # in the fcts called ignore the ignored embryonames
        embryo_names_t = [name for name in embryo_names if name not in ignored_embryos]

        # compute distance between embryos
        distance_matrix, dimensions_ = compute_topological_distance_matrix_btw_embryos(non_ignored_embryos_dxs,
                                                                                       list_dict_names_at_t,
                                                                                       list_dict_surfaces_at_t)

        print('dev time', t, 'nb of cell in ref', len(list(ref_names_at_t.keys())), 'nb of cells after mca:',
              len(list(list_dict_names_at_t[0].values())))
        #print(non_ignored_embryos_dxs)
        time = f'{n}_{t}'
        # plot the distance matrix for this dev time and its mds
        write_plot_script_global_distance_btw_embryos(distance_matrix, dimensions_, time,
                                                      sorted(embryo_names_t), output_folder,
                                                      'of_surface_of_contact_vectors', embryo_res)
        write_plot_script_mds_distance_btw_embryos(distance_matrix, dimensions_, time,
                                                   sorted(embryo_names_t),
                                                   output_folder, 'topological', embryo_res)

        if len(list(list_dict_names_at_t[0].values())) in [64, 76, 112, 184, 218]:
            write_plot_script_global_distance_btw_embryos(distance_matrix, dimensions_, time,
                                                          sorted(embryo_names_t), output_folder, 'of surface_of_contact_vectors',
                                                          embryo_res)
            # distance matrix mds
            write_plot_script_mds_distance_btw_embryos(distance_matrix, dimensions_,
                                                       len(list(list_dict_names_at_t[0].values())),
                                                       sorted(embryo_names_t), output_folder,
                                                       distance_type='topological', embryo_res=embryo_res)
        if embryo_res == 'halfs':

            if len(list(list_dict_names_at_t[0].values())) in [32, 38, 56, 92, 109]:
                write_plot_script_global_distance_btw_embryos(distance_matrix, dimensions_, time,
                                                              sorted(embryo_names_t), output_folder, 'of surface_of_contact_vectors',
                                                              embryo_res)
                # distance matrix mds
                write_plot_script_mds_distance_btw_embryos(distance_matrix, dimensions_,
                                                           len(list(list_dict_names_at_t[0].values())), sorted(embryo_names_t), output_folder,
                                                           distance_type='topological', embryo_res='halfs')
        ###
        ##
        ##


###
########
#####


# we want to option, registering common cells, to verify only impact of registration residues
# adding the return to most common ancesters of all individuals will help know additional impact of merging daughters

def divide_embryo_left_right(pm_names, pm_props):
    """
    Divides the embryo names and properties into left and right groups.

    Parameters
    ----------
    pm_names : dict
        Dictionary with IDs as keys and names as values.
        Name is left if name[-1] == '_', is right if name[-1] == '*'.
    pm_props : dict
        Dictionary with IDs as keys and properties as values.

    Returns
    -------
    left_names_dict : dict
        Dictionary with IDs as keys and left names as values.
    left_prop_dict : dict
        Dictionary with IDs as keys and left properties as values.
    right_names_dict : dict
        Dictionary with IDs as keys and right names as values.
    right_prop_dict : dict
        Dictionary with IDs as keys and right properties as values.
    """

    left_names_dict = {}
    left_prop_dict = {}
    right_names_dict = {}
    right_prop_dict = {}

    for id, name in pm_names.items():
        if name[-1] == '_':
            name = name[:-1] + '*'
            left_names_dict[id] = name
            left_prop_dict[id] = pm_props.get(id)
        elif name[-1] == '*':
            right_names_dict[id] = name
            right_prop_dict[id] = pm_props.get(id)

    return left_names_dict, left_prop_dict, right_names_dict, right_prop_dict


def estimate_symmetry_at_t(mass_right, bary_right, mass_left, bary_left):
    """
    Estimates the embryo symmetry vector given right and left properties at a timepoint t.

    Parameters
    ----------
    mass_right : list
        List of cell masses on the right.
    bary_right : list
        List of tuples representing the cell barycenters (x, y, z) on the right.
    mass_left : list
        List of cell masses on the left.
    bary_left : list
        List of tuples representing the cell barycenters (x, y, z) on the left.

    Returns
    -------
    symmetry_vector : tuple
        The estimated symmetry vector (sx, sy, sz).
    """
    # Calculate left barycenter
    total_mass_left = sum(mass_left)
    left_bary = np.sum(np.array([m * np.array(b) for m, b in zip(mass_left, bary_left)]), axis=0) / total_mass_left
    # Calculate right barycenter
    total_mass_right = sum(mass_right)
    right_bary = np.sum(np.array([m * np.array(b) for m, b in zip(mass_right, bary_right)]), axis=0) / total_mass_right
    # Return symmetry vector
    symmetry_vector = left_bary - right_bary
    return tuple(symmetry_vector)


def apply_sym_on_half_embryo(right_bary, symmetry_vector):
    """
    Applies symmetry on barycenters using a given symmetry vector.

    Parameters
    ----------
    right_bary_at_t : list
        List of tuples representing the barycenters (x, y, z) at those time points.
    symmetry_vector : tuple
        A tuple representing the symmetry vector (sx, sy, sz).

    Returns
    -------
    right_bary_after_symmetry : list
        List of tuples representing the barycenters after applying symmetry.
    """
    # Normalize the symmetry vector
    n = np.array(symmetry_vector)
    n /= np.linalg.norm(n)

    right_bary_at_t = right_bary
    right_bary_after_symmetry = []

    for bary in right_bary_at_t:
        bary = np.array(bary)
        # Calculate the component of the barycenter vector parallel to n
        bary_parallel = np.dot(bary, n) * n
        # Calculate the new position by reflecting with respect to the plane
        new_bary = bary - 2 * bary_parallel
        right_bary_after_symmetry.append(new_bary)
    return right_bary_after_symmetry


def all_embryos_in_same_referential_distance_matrix(files, ref_filename, transformation_type='similitude',
                                                    retained_fraction=1, output_folder='',
                                                    embryo_res='halfs', imaging_res=0.3, random_ref_add_ind = False,
                                                    dif_dev_time=15):
    # imaging_res is 0.3x0.3x0.3um

    list_dict_barycenters = []
    list_dict_names = []
    list_dict_mass = []

    embryo_names = []
    embryos_props = {}
    embryo_idx = {}

    atimes_list = []
    times_list = []

    list_surface_adjustment_factors = []  # list of dict [pmx_idx]{'taq': scaling_factor}

    err = ""

    dict_embryos_in_same_ref = {}  # in a tdev, embryos all in same ref barycenters

    all_cell_names = set()

    # get embryos
    with open(ref_filename, 'rb') as f:
        ref_props_data = {}
        ref_name = ref_filename.split('-')[1]
        ref_props = pkl.load(f)
        ref_props_data[ref_name] = ref_props

        # compute volume alignment parms
        volume_adjustement_factors = volume_fitting_and_voxelsize_correction(ref_props['cell_volume'],
                                                                             target_volume=60000000)

        volume_adjustement_factors = {t: factor * imaging_res for t, factor in volume_adjustement_factors.items()}

        #  handle case of embryo_resolution
        if embryo_res == 'halfs':
            left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                ref_props['cell_name'], ref_props['cell_barycenter'])
            list_dict_barycenters.append(left_prop_dict)
            list_dict_barycenters.append(right_prop_dict)

            list_dict_names.append(left_names_dict)
            list_dict_names.append(right_names_dict)

            left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                ref_props['cell_name'], ref_props['cell_volume'])
            list_dict_mass.append(left_prop_dict)
            list_dict_mass.append(right_prop_dict)

            i = len(embryo_idx)
            embryo_idx[ref_name + '_left'] = i
            embryo_names.append(ref_name + '_left')
            # add to embryos idx the random ref (if parameter precise it)
            if random_ref_add_ind:
                embryo_idx[ref_name + '_random'] = i + 1
                embryo_names.append(ref_name + '_random')
                list_surface_adjustment_factors.append(volume_adjustement_factors)
                list_dict_mass.append(left_prop_dict)
                list_dict_names.append(left_names_dict)
                list_dict_barycenters.append(left_prop_dict)
                embryo_idx[ref_name + '_right'] = i + 2
            else:
                embryo_idx[ref_name + '_right'] = i + 1

            embryo_names.append(ref_name + '_right')
            # add
            list_surface_adjustment_factors.append(volume_adjustement_factors)
            list_surface_adjustment_factors.append(volume_adjustement_factors)
        else:
            #
            list_dict_barycenters.append(ref_props['cell_barycenter'])
            list_dict_names.append(ref_props['cell_name'])
            list_dict_mass.append(ref_props['cell_volume'])
            embryo_names.append(ref_name)
            i = len(embryo_idx)
            embryo_idx[ref_name] = i
            #  add
            list_surface_adjustment_factors.append(volume_adjustement_factors)

        ###

    for filename in files:
        if filename == ref_filename:
            continue
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)
            embryo_name = filename.split('-')[1]

            embryos_props[embryo_name] = {
                'cell_barycenter': pm_props['cell_barycenter'],
                'cell_name': pm_props['cell_name'],
                'cell_volume': pm_props['cell_volume'],
                'cell_lineage': pm_props['cell_lineage']
            }

            # compute volume alignment parms
            volume_adjustement_factors = volume_fitting_and_voxelsize_correction(pm_props['cell_volume'],
                                                                                 target_volume=60000000)
            volume_adjustement_factors = {t: factor * imaging_res for t, factor in volume_adjustement_factors.items()}

            # handle a case of embryo_resolution
            #
            if embryo_res == 'halfs':
                left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                    pm_props['cell_name'], pm_props['cell_barycenter'])
                list_dict_barycenters.append(left_prop_dict)
                list_dict_barycenters.append(right_prop_dict)

                list_dict_names.append(left_names_dict)
                list_dict_names.append(right_names_dict)

                left_names_dict, left_prop_dict, right_names_dict, right_prop_dict = divide_embryo_left_right(
                    pm_props['cell_name'], pm_props['cell_volume'])
                list_dict_mass.append(left_prop_dict)
                list_dict_mass.append(right_prop_dict)

                i = len(embryo_idx)
                embryo_idx[embryo_name + '_left'] = i
                embryo_idx[embryo_name + '_right'] = i + 1

                embryo_names.append(embryo_name + '_left')
                embryo_names.append(embryo_name + '_right')

                # add adjustement parameters of volumes
                list_surface_adjustment_factors.append(volume_adjustement_factors)
                list_surface_adjustment_factors.append(volume_adjustement_factors)

            else:
                i = len(embryo_idx)
                embryo_idx[embryo_name] = i
                # list_dict_lineage.append(embryo_prop['cell_lineage'])
                list_dict_barycenters.append(pm_props['cell_barycenter'])
                list_dict_names.append(pm_props['cell_name'])
                list_dict_mass.append(pm_props['cell_volume'])
                embryo_names.append(embryo_name)

                #  add adjustement parameters of volumes
                list_surface_adjustment_factors.append(volume_adjustement_factors)
                #

    output_file = os.path.join(output_folder, "log_file.log")
    file = open(output_file, "w")
    file.write('Hello, hey')

    # do temporal alignment
    temporal_alignment_coefficient_dict, _ = get_temporal_alignment_coefficients_of_embryos(embryos_props,
                                                                                            ref_props_data)
    print('ref embryo', ref_name)
    #
    #  delete embryos_props
    del embryos_props
    del ref_props_data

    # for each embryo compute adjusted times
    times_ref = utimes.get_embryo_times(ref_props['cell_lineage'])
    a, b = temporal_alignment_coefficient_dict[ref_name]
    a_times_ref = [a * t + b for t in times_ref]
    print('for ref:', a, b)
    # times_list.append(times_ref)
    # atimes_list.append(a_times_ref)

    for embryo, i in embryo_idx.items():
        if random_ref_add_ind and i==1:
            times = utimes.get_embryo_times(list_dict_barycenters[i+1])
        else:
            times = utimes.get_embryo_times(list_dict_barycenters[i])
        if embryo_res == 'halfs':
            embryo = embryo.split('_')[0]
            a, b = temporal_alignment_coefficient_dict[embryo]
            a_times = [a * t + b for t in times]
            atimes_list.append(a_times)
            times_list.append(times)
        else:
            a, b = temporal_alignment_coefficient_dict[embryo]
            a_times = [a * t + b for t in times]
            atimes_list.append(a_times)
            times_list.append(times)

    if embryo_res == 'halfs':
        ref_name = ref_name + '_left'
    ##
    ##
    #
    for t in a_times_ref:
        print('working for dev time in ref:', t)
        flag = 0   # skips t if cells are missing cz MCA wount be gd
        ignored_embryos = set() # embryos not imaged at the corresponding dev time

        # t is dev time in ref we're currently putting all embryos in the same referential at this dev t

        list_dict_barycenters_at_t = []
        list_dict_names_at_t = []
        list_dict_mass_at_t = []

        t = int(t)
        # t aquisition
        taq_1 = times_ref[a_times_ref.index(t)]
        err_1 = f"aq time in pm1:{taq_1}\n"
        # pm1 at t_aq1
        dict_at_t = get_embryo_at_t(list_dict_names[0], taq_1)
        dict_at_t = dict(sorted(dict_at_t.items(), key=lambda x: x[0]))
        list_cell_ids_in_pm1_at_t = list(dict_at_t.keys())
        ref_names_at_t = list(dict_at_t.values())

        dict_at_t = get_embryo_at_t(list_dict_mass[0], taq_1)
        dict_at_t = dict(sorted(dict_at_t.items(), key=lambda x: x[0]))
        # apply scaling on volume at taq_1, idx=0
        scaling_factor = list_surface_adjustment_factors[0][taq_1]
        dict_at_t = {nk: nv * scaling_factor * scaling_factor * scaling_factor for nk, nv in dict_at_t.items()}
        ref_mass_at_t = list(dict_at_t.values())

        dict_at_t = get_embryo_at_t(list_dict_barycenters[0], taq_1)
        dict_at_t = dict(sorted(dict_at_t.items(), key=lambda x: x[0]))
        # apply scaling on volume at taq_1, idx=0
        dict_at_t = {nk: nv * scaling_factor for nk, nv in dict_at_t.items()}

        ref_barycenters_at_t = list(dict_at_t.values())

        list_dict_barycenters_at_t.append(ref_barycenters_at_t)
        list_dict_names_at_t.append(ref_names_at_t)
        list_dict_mass_at_t.append(ref_mass_at_t)

        common_names_all = set(ref_names_at_t)  # Start with PM1 names
        non_common_names_all = set(ref_names_at_t)

        ##
        # Grab embryos at needed dev_t
        # then for most common ancestry (putting all embryos in same ref) I need to know common cells and
        # non common cells
        ##
        individuals = [ref_name]

        if random_ref_add_ind:
            # add to individuals at t, the ref with random names shuffling
            list_dict_barycenters_at_t.append(ref_barycenters_at_t)
            random.shuffle(ref_names_at_t)
            list_dict_names_at_t.append(ref_names_at_t)
            list_dict_mass_at_t.append(ref_mass_at_t)
            individuals.append(ref_name + '_random')

        for pmx_name, pmx_idx in embryo_idx.items():
            if pmx_idx == 0:
                continue

            if random_ref_add_ind and pmx_idx == 1:
                continue

            closest_at = min(atimes_list[pmx_idx], key=lambda x: abs(x - t))

            if embryo_res == 'halfs' and pmx_name.split('_')[1] == 'right':
                # if an individual is a right, when dealing with halfs embryos, apply symetry
                #  vector = left_embryo(idx-1) barycenters

                pmx_barycenters_at_t_left, pmx_mass_at_t_left = (list_dict_barycenters_at_t[pmx_idx - 1],
                                                                 list_dict_mass_at_t[pmx_idx - 1])
                taq = times_list[pmx_idx][atimes_list[pmx_idx].index(closest_at)]
                pmx_barycenters_at_t, pmx_names_at_t, pmx_mass_at_t = (
                    get_embryo_data_at_time(list_dict_barycenters[pmx_idx], list_dict_names[pmx_idx],
                                            list_dict_mass[pmx_idx], taq))
                # estimate symetry at t
                sym_pmx_at_t = estimate_symmetry_at_t(pmx_mass_at_t, pmx_barycenters_at_t, pmx_mass_at_t_left,
                                                      pmx_barycenters_at_t_left)
                # apply symetry on t right barycenters
                pmx_barycenters_at_t = apply_sym_on_half_embryo(pmx_barycenters_at_t, sym_pmx_at_t)
            else:
                taq = times_list[pmx_idx][atimes_list[pmx_idx].index(closest_at)]
                pmx_barycenters_at_t, pmx_names_at_t, pmx_mass_at_t = (
                    get_embryo_data_at_time(list_dict_barycenters[pmx_idx], list_dict_names[pmx_idx],
                                            list_dict_mass[pmx_idx], taq))

            # print(pmx_name,len(pmx_barycenters_at_t) - len(ref_barycenters_at_t), len(pmx_barycenters_at_t))
            if abs(len(pmx_barycenters_at_t) - len(ref_barycenters_at_t)) >= dif_dev_time:
                print('lenght in pmx', len(pmx_barycenters_at_t), 'in embryo', len(pmx_barycenters_at_t) * 2, 'at', t, closest_at)
                print(pmx_name, 'embryo not imaged at this developmental stage', 'nb of cells in ref:', len(list(dict_at_t.keys())),
                      len(pmx_barycenters_at_t))

                # if taq too far ignore pmx: if it correspond to a t_dev different much than t
                # flag = 0
                # in the case where one embryo is not imaged at this stage just remove it from analysis
                # get its name/idx and ignore it for mca
                ignored_embryos.add(pmx_name)


            # apply scaling on volume & barycenters at taq, pmx_idx
            scaling_factor = list_surface_adjustment_factors[pmx_idx][taq]
            pmx_mass_at_t = [nv * scaling_factor * scaling_factor * scaling_factor for nv in pmx_mass_at_t]
            pmx_barycenters_at_t = [nv * scaling_factor for nv in pmx_barycenters_at_t]

            individuals.append(pmx_name)
            list_dict_barycenters_at_t.append(pmx_barycenters_at_t)
            list_dict_names_at_t.append(pmx_names_at_t)
            list_dict_mass_at_t.append(pmx_mass_at_t)

            # print(pmx_name, len(list_dict_names_at_t[pmx_idx]))
            # print(list_dict_names_at_t[pmx_idx])
            ###
            #
            pmx_names_at_t_sets = set(pmx_names_at_t)
            # get commun and non-commun cell names
            common_names_all = common_names_all.intersection(pmx_names_at_t_sets)
            # non_common_names_all.append(pmx_names_at_t_sets.difference(common_names_all))
            non_common_names_all = non_common_names_all.union(pmx_names_at_t_sets)
            err_1 += f"aq time in {pmx_name}:{taq}\n"
            err_1 += f"names before merge: {pmx_names_at_t}\n"

        print('starting mca')
        # print("non common names:", non_common_names_all)
        non_common_names_all = non_common_names_all.difference(common_names_all)

        # log msg
        err_1 += f"ref time of aq:{taq_1}, dev time:{t}, "
        for i, embryo_cells in enumerate(list_dict_names_at_t, start=0):
            err_1 += f"{embryo_names[i]} nb of cells:{len(embryo_cells)}, "
        err_1 += "\n"

        ###
        ##
        # return to most common ancestry of all embryos
        ###
        for cell in non_common_names_all:
            daughter_1_name, daughter_2_name = uname.get_daughter_names(cell)
            # Include granddaughters too
            ###
            #
            # grand_daughter_1, grand_daughter_2 = get_daughters(cell)
            # grand_daughter_3, grand_daughter_4 = get_daughters(cell)

            if daughter_1_name in non_common_names_all and daughter_2_name in non_common_names_all:
                # search for daughters in every pmx and merge them
                for pmx_name, pmx_idx in embryo_idx.items():
                    if pmx_name in ignored_embryos:
                        continue
                    # either i want to find in pmx daughters or the mother or else break
                    if daughter_1_name in list_dict_names_at_t[pmx_idx] and daughter_2_name in list_dict_names_at_t[
                        pmx_idx]:
                        # get daughters barycenters and mass : theyre in the same index in the lists
                        idx_d1 = list_dict_names_at_t[pmx_idx].index(daughter_1_name)
                        idx_d2 = list_dict_names_at_t[pmx_idx].index(daughter_2_name)
                        if idx_d1 < idx_d2:
                            idx_d1, idx_d2 = idx_d2, idx_d1
                        if idx_d1 == idx_d2:
                            print("error equal indices")

                        barycenter_1 = list_dict_barycenters_at_t[pmx_idx][idx_d1]
                        barycenter_2 = list_dict_barycenters_at_t[pmx_idx][idx_d2]

                        mass_1 = list_dict_mass_at_t[pmx_idx][idx_d1]
                        mass_2 = list_dict_mass_at_t[pmx_idx][idx_d2]
                        # get new barycenter
                        merged_barycenter = barycenter_1 * mass_1 + barycenter_2 * mass_2
                        merged_barycenter = merged_barycenter / (mass_1 + mass_2)
                        # merge daughters and update the embryo pmx=
                        del list_dict_names_at_t[pmx_idx][idx_d1]
                        del list_dict_names_at_t[pmx_idx][idx_d2]
                        list_dict_names_at_t[pmx_idx].append(cell)

                        del list_dict_barycenters_at_t[pmx_idx][idx_d1]
                        del list_dict_barycenters_at_t[pmx_idx][idx_d2]
                        list_dict_barycenters_at_t[pmx_idx].append(merged_barycenter)

                        del list_dict_mass_at_t[pmx_idx][idx_d1]
                        del list_dict_mass_at_t[pmx_idx][idx_d2]
                        list_dict_mass_at_t[pmx_idx].append(mass_1 + mass_2)
                        if pmx_name == "pm1":
                            id1, id2 = list_cell_ids_in_pm1_at_t[idx_d1], list_cell_ids_in_pm1_at_t[idx_d2]
                            del list_cell_ids_in_pm1_at_t[idx_d1]
                            del list_cell_ids_in_pm1_at_t[idx_d2]
                            list_cell_ids_in_pm1_at_t.append(f"{id1}_{id2}")
                    elif daughter_1_name in list_dict_names_at_t[pmx_idx] or daughter_2_name in list_dict_names_at_t[
                        pmx_idx]:
                        err = (f"dev time: {t}, pmx:{pmx_name}, daughters:{daughter_1_name} or"
                               f" {daughter_2_name} missing \n")
                        print("a daughter do not exist in embryo pb", err)
                        file.write("A Daughter is missing in:\n")
                        file.write(err)
                        flag = 1
                    elif cell in list_dict_names_at_t[pmx_idx]:
                        pass
                        # print("cell already in embryo")
                    else:
                        file.write(
                            f"One of the ancestors did not divide yet(heterochrony or division not detected)..:{cell}"
                            f" in {pmx_name}\n")
        if flag == 1:
            flag = 0
            continue
        ##
        # some embryos should be ignored
        # check if we have same nb of cells in all embryos after the merge
        #if any(len(list_dict_names_at_t[0]) != len(names) for i, names in enumerate(list_dict_names_at_t) if
        #       embryo_names[i] not in ignored_embryos):
            # Write in output file the sizes of the embryos
        file.write("-----------------------\n")
        file.write(err_1)
        file.write("Embryo Sizes are different after merge of daughters:\n")
        # Generate the output for each embryo
        for idx, names_list in enumerate(list_dict_names_at_t):
            nb_cells = len(names_list)
            err += f"{embryo_names[idx]} nb of cells (AFTER MERGE):{nb_cells}, "
            #print(f"{embryo_names[idx]} nb of cells (AFTER MERGE):{nb_cells}")
        err += "\n"
        # Write the output to the file
        file.write(err)
        file.write("The non common names in embryos after merge (we should have the mothers from list above):\n")
        # Iterate over each embryo to identify non-common cells after merging daughters
        for idx, names_list in enumerate(list_dict_names_at_t):
            # Find non-common cells
            c_names = set(names_list).intersection(non_common_names_all)
            err += f"{embryo_names[idx]} has these non commun cells(after merge): {c_names}\n"

        # Ignore non-common cells after merging daughters: they're due to naming/segementation errors, nor cell nor its daughters exist
        c_names = set(list_dict_names_at_t[0])
        nc_names = set()
        for idx, names_list in enumerate(list_dict_names_at_t):
            if any(pmx for pmx, val in embryo_idx.items() if val == idx and pmx not in ignored_embryos):
                c_names = set(names_list).intersection(c_names)
        for idx, names_list in enumerate(list_dict_names_at_t):
            nc_names = set(names_list).union(nc_names)
        nc_names = nc_names - c_names
        err += f"Problematic cells(non commun names after merge) being ignored in the registration:{nc_names}\n"
        err += "These cells/their daughters(or one of them) do not exist in all embryos\n"
        file.write(err)
        file.write("\n ********************** \n")
        file.write("\n ********************** \n")
        file.write("\n ********************** \n")
        # Delete nc_names in each list_dict_names_at_t list, list_dict_mass_at_t, list_dict_mass_at_t, list_dict_barycenters_at_t
        print("Removing cells non existent in certain individuals")
        indices_to_delete = []
        # Iterate over each embryo
        for idx, names_list in enumerate(list_dict_names_at_t):
            print('lenght of list names at t', len(names_list))
            # Iterate over each cell in the embryo
            for jdx in range(len(list_dict_names_at_t[idx])):
                cell = list_dict_names_at_t[idx][jdx]
                if cell in nc_names:
                    indices_to_delete.append((idx, jdx))

        # Deleting elements in reverse order to avoid index issues
        for idx, jdx in reversed(indices_to_delete):
            del list_dict_names_at_t[idx][jdx]
            del list_dict_mass_at_t[idx][jdx]
            del list_dict_barycenters_at_t[idx][jdx]
            if embryo_names[idx] == ref_name:
                del list_cell_ids_in_pm1_at_t[jdx]

        for idx, names_list in enumerate(list_dict_names_at_t):
            nb_cells = len(names_list)
            err += f"{embryo_names[idx]} nb of cells (AFTER MERGE and removing nc names):{nb_cells}, "
            #print(f"{embryo_names[idx]} nb of cells (AFTER MERGE and removing nc names):{nb_cells}")
        err += "\n"
        # Write the output to the file
        file.write(err)

        #else:
        dict_embryos_in_same_ref[t] = {}
        for embryo_name, i in embryo_idx.items():
            # ignore not imaged embryos idx/name at this stage
            if embryo_name in ignored_embryos:
                continue
            dict_embryos_in_same_ref[t][embryo_name] = {}

        ###
        ##
        # registering embryos after most common ancestry
        ##
        # take ref embryo
        ref_bary = list_dict_barycenters_at_t[0]
        ref_names = list_dict_names_at_t[0]
        ref_names, ref_bary, ref_ids = sort_lists_according_to_cell_name(ref_names, ref_bary, list_cell_ids_in_pm1_at_t)

        all_cell_names.update(ref_names)

        print('embryo size at t:', len(ref_names))
        ######
        # register each of the embryos (global registration)
        for pmx_name, pmx_idx in embryo_idx.items():
            # ignore not imaged embryos idx/name at this stage
            if pmx_name in ignored_embryos:
                continue
            if pmx_name == ref_name:
                continue
            # print('registering embryo',pmx_name, list_dict_names_at_t[pmx_idx].index('b7.0003*'))
            # get pmx barycenters and names
            flo_bary = list_dict_barycenters_at_t[pmx_idx]
            flo_names = list_dict_names_at_t[pmx_idx]

            # sort appropriately
            flo_names, flo_bary = get_vectors_before_registration(ref_names, flo_bary, flo_names)
            #
            # compute transform: should we use a similitude/rigid?
            transform, ref, floating = compute_similitude(ref_bary, flo_bary, transformation_type,
                                                          retained_fraction)

            # make transform rigid, its transform homogeneous coordinates
            transform = similitude_without_scaling(transform)
            # apply transform
            transformed_floating = apply_transformation(transform, floating)

            # save barycenters in a
            for i, cell_name in enumerate(flo_names):
                dict_embryos_in_same_ref[t][ref_name][cell_name] = ref[:, i]
                dict_embryos_in_same_ref[t][pmx_name][cell_name] = transformed_floating[:, i]

        ###
        #

        print('dev time', t, 'nb of cell in ref', len(dict_at_t), 'nb of cells after mca:',
              len(ref[0]), 'nb of cell not in common:', len(common_names_all))

        # in the fcts called ignore the ignored embryonames
        embryo_names_t = [name for name in embryo_names if name not in ignored_embryos]

        # distance matrix of geometrical feature(cells barycenters) at each t
        global_avg_distances_matrix, dimensions_ = distance_matrix_geom_btw_individuals_at_t(
            dict_embryos_in_same_ref[t],
            sorted(embryo_names_t))

        time = f'{len(dict_at_t)}_{t}'
        print('wuhu',time, embryo_names_t)
        # plot global_avg_distances_matrix at t
        write_plot_script_global_distance_btw_embryos(global_avg_distances_matrix, dimensions_, time,
                                                      sorted(embryo_names_t),
                                                      output_folder, 'geometric', embryo_res)
        write_plot_script_mds_distance_btw_embryos(global_avg_distances_matrix, dimensions_, time,
                                                      sorted(embryo_names_t),
                                                      output_folder, 'geometric', embryo_res)

        if len(ref[0]) in [64, 76, 112, 184, 218]:
            write_plot_script_global_distance_btw_embryos(global_avg_distances_matrix, dimensions_, time,
                                                          sorted(embryo_names_t), output_folder, 'geometric',
                                                          embryo_res)
            # plot distance matrix mds
            write_plot_script_mds_distance_btw_embryos(global_avg_distances_matrix, dimensions_,
                                                       len(ref[0]),
                                                       sorted(embryo_names_t), output_folder, distance_type='geometric',
                                                       embryo_res='complete')

        if embryo_res == 'halfs':
            if len(dict_at_t) in [32, 38, 56, 92, 109]:
                write_plot_script_global_distance_btw_embryos(global_avg_distances_matrix, dimensions_, time,
                                                              sorted(embryo_names_t), output_folder, 'of barycenters positions',
                                                              embryo_res)
                # plot distance matrix mds
                write_plot_script_mds_distance_btw_embryos(global_avg_distances_matrix, dimensions_,
                                                           len(dict_at_t),
                                                           sorted(embryo_names_t), output_folder, distance_type='geometric',
                                                           embryo_res='halfs')
    # update vectors
    print('all cells lenghts', len(all_cell_names))
    for t in dict_embryos_in_same_ref.keys():
        for pmx in dict_embryos_in_same_ref[t].keys():
            for cell in all_cell_names:
                if cell not in dict_embryos_in_same_ref[t][pmx]:
                    dict_embryos_in_same_ref[t][pmx][cell] = np.array([0, 0, 0])

    # umap for: dict_embryos_in_same_ref[t][pmx_name][cell]=3d_coordinate
    # put ref name in param
    generate_umap_script(dict_embryos_in_same_ref, all_cell_names, ref_name, embryo_names, output_folder)

    file.close()


#write script of umap of embryo geometrical charach at all t's
######
def generate_umap_script(dict_embryos_in_same_ref, all_cells, ref_name, all_embryos, output_folder):
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    dict_embryos_in_same_ref = {t: {
        ref_name: {cell: coord.tolist() if isinstance(coord, np.ndarray) else coord for cell, coord in cells.items()}
        for ref_name, cells in refs.items()}
                                for t, refs in dict_embryos_in_same_ref.items()}

    script_content = f"""
import numpy as np
import umap
import matplotlib.pyplot as plt
import os
import matplotlib.cm as cm

all_cells = sorted(list({all_cells}))

# Data Preparation
dict_embryos_in_same_ref = {dict_embryos_in_same_ref}
all_embeddings = []
all_labels = []
timepoints_labels = []
time_cell_counts = {{}}

all_embryos = {all_embryos}
all_embryos_in_embedding = False

for t, embryos in dict_embryos_in_same_ref.items():
    flag = False
    #if t > 10:
    #    break
    if all_embryos_in_embedding:
        for pm in all_embryos:
            if pm not in embryos:
                flag = True
    if flag:
        continue
    for pmx_name in embryos:
        non_zero_cells = 0
        embryo_coords = []
        for cell in all_cells:
            coord = np.array(embryos[pmx_name][cell])
            embryo_coords.append(np.array(embryos[pmx_name][cell]))
            if not np.array_equal(coord, np.array([0, 0, 0])):
                non_zero_cells += 1
        embryo_coords = np.array(embryo_coords).flatten()
        all_embeddings.append(embryo_coords)
        all_labels.append(f"{{pmx_name}}")
        timepoints_labels.append(f"{{pmx_name}}_{{t}}")
        if t % 5 == 0 or t==1:
            if t not in time_cell_counts:
                time_cell_counts[t] = non_zero_cells

all_embeddings = np.array(all_embeddings)

# Compute UMAP embedding
reducer = umap.UMAP()
umap_embedding = reducer.fit_transform(all_embeddings)

# Create a plot
plt.figure(figsize=(12, 8))
unique_labels = list(set(all_labels))
colors = cm.rainbow(np.linspace(0, 1, len(unique_labels)))
label_to_color = {{label: color for label, color in zip(unique_labels, colors)}}

label_positions = {{}}

for label in unique_labels:
    indices = [i for i, lbl in enumerate(all_labels) if lbl == label]
    plt.scatter(umap_embedding[indices, 0], umap_embedding[indices, 1], color=label_to_color[label], label=label)


for i, label in enumerate(all_labels):
    if label not in label_positions:
        label_positions[label] = []
    label_positions[label].append(umap_embedding[i])

for label, positions in label_positions.items():
    positions = np.array(positions)
    plt.scatter(positions[:, 0], positions[:, 1])
    plt.scatter(positions[0, 0], positions[0, 1], color='green', marker='o', s=100)
    plt.scatter(positions[-1, 0], positions[-1, 1], color='red', marker='x', s=100)

    # Draw lines connecting points of the same individual over time
    for i in range(len(positions) - 1):
        plt.plot(positions[i:i+2, 0], positions[i:i+2, 1], color='gray', linestyle='--')

for t, count in time_cell_counts.items():
    idx = timepoints_labels.index(f"{ref_name}_{{t}}")
    plt.annotate(f"t={{t}}, cells={{count}}", (umap_embedding[idx, 0], umap_embedding[idx, 1]), fontsize=9, color='blue')

# Annotate points with labels
#for i, label in enumerate(all_labels):
#    plt.text(umap_embedding[i, 0], umap_embedding[i, 1], label, fontsize=9)

plt.legend()
plt.title("UMAP Embedding of Embryos")
plt.xlabel("UMAP 1")
plt.ylabel("UMAP 2")
# Annotate points with labels
#for i, label in enumerate(all_labels):
#    plt.text(umap_embedding[i, 0], umap_embedding[i, 1], label, fontsize=9)

# Save plot
output_folder = "Desktop/AscidianInsight/distance_btw_individuals"
if not os.path.exists(output_folder):
    os.makedirs(output_folder)
plot_file = os.path.join(output_folder, "umap_embedding.png")
#plt.savefig(plot_file)
plt.show()
plt.close()

print(f"UMAP plot saved to {{plot_file}}")
"""

    script_path = os.path.join(output_folder, f"umap_script.py")
    with open(script_path, "w") as file:
        file.write(script_content)

    print(f"Python script saved to {script_path}")


######Plot distance matrix/MDS
###############################
def write_plot_script_mds_distance_btw_embryos(global_avg_distances_matrix, dimensions_to_normalise, nb_cells,
                                               individuals, outputfolder, distance_type='geometric',
                                               embryo_res='halfs'):
    """
    Writes a script that plots the MDS of the distance matrix to visualize similarities between individuals.

    Parameters
    ----------
    global_avg_distances_matrix : numpy array
        The distance matrix between the different individuals.
    dimensions_to_normalise : numpy array
        Array to divide corresponding distance values to normalize.
    nb_cells : int
        Number of cells at the stage.
    individuals : list
        List of individual names.
    outputfolder : str
        Directory to save the output script.
    distance_type : str, optional
        Type of distance used ('geometric' by default).
    embryo_res : str, optional
        Embryo resolution ('halfs' by default).

    Returns
    -------
    None
    """
    output_dir = os.path.join(outputfolder, f"{distance_type}_mds_distance_btw_embryos")
    if embryo_res == 'halfs':
        output_dir = os.path.join(output_dir, f"{distance_type}_mds_distance_btw_halfs_embryos")
    local_filename = os.path.join(output_dir, f"{distance_type}_mds_distance_btw_embryos_at_{nb_cells}.py")

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    with open(local_filename, "w") as f:
        f.write("import numpy as np\n")
        f.write("import matplotlib.pyplot as plt\n")
        f.write("from sklearn.manifold import MDS\n")
        f.write("import matplotlib.colors as mcolors\n")

        f.write(f"# Average distances matrix\n")
        f.write(f"normalized_distance_matrix = np.array({global_avg_distances_matrix.tolist()})\n\n")
        f.write(f"dimensions_to_normalise = np.array({dimensions_to_normalise.tolist()})\n\n")
        f.write(f"individual_names = {individuals}\n\n")

        # Normalization of the distance matrix
        f.write("# Normalization of the distance matrix\n")
        f.write("#normalized_distance_matrix = global_avg_distances_matrix / dimensions_to_normalise\n")
        f.write("np.fill_diagonal(normalized_distance_matrix, 0)\n")  # Set diagonal elements to 0

        # MDS computation
        f.write("# Perform MDS\n")
        f.write("mds = MDS(n_components=2, dissimilarity='precomputed', random_state=42)\n")
        f.write("mds_coords = mds.fit_transform(normalized_distance_matrix)\n")

        # Assign colors to symmetrical counterparts
        f.write("# Assign colors to symmetrical counterparts\n")
        f.write("colors = {}\n")
        f.write("base_colors = list(mcolors.TABLEAU_COLORS.values())\n")
        f.write("for i, name in enumerate(individual_names):\n")
        f.write("    base_name = name.replace('_left', '').replace('_right', '').replace('_random','')\n")
        f.write("    if base_name not in colors:\n")
        f.write("        colors[base_name] = base_colors[len(colors) % len(base_colors)]\n")

        # Plotting
        f.write("fig, ax = plt.subplots(figsize=(10, 10))\n")
        f.write("for i, txt in enumerate(individual_names):\n")
        f.write("    base_name = txt.replace('_left', '').replace('_right', '').replace('_random','')\n")
        f.write("    ax.scatter(mds_coords[i, 0], mds_coords[i, 1], color=colors[base_name], s=100, label=base_name if base_name not in ax.get_legend_handles_labels()[1] else '')\n")
        f.write("    ax.annotate(txt, (mds_coords[i, 0], mds_coords[i, 1]), fontsize=12, ha='right')\n")

        f.write("ax.set_xlabel('MDS Dimension 1')\n")
        f.write("ax.set_ylabel('MDS Dimension 2')\n")
        if embryo_res == 'halfs':
            f.write(f"ax.set_title('MDS of {distance_type} Distance Matrix at {nb_cells*2} Cell Stage')\n")
        else:
            f.write(f"ax.set_title('MDS of {distance_type} Distance Matrix at {nb_cells} Cell Stage')\n")
        f.write("plt.grid(True)\n")
        f.write("plt.legend()\n")
        f.write("plt.tight_layout()\n")

        file = f"MDSPlot_{distance_type.replace(' ', '_')}.png"
        file = os.path.join(outputfolder, file)
        f.write(f"#plt.savefig('{file}', dpi=300)\n")
        f.write("plt.show()\n")
        f.close()
    print('plot in', local_filename)
########
#################
def write_plot_script_global_distance_btw_embryos(global_avg_distances_matrix, dimensions_to_normalise,
                                                  nb_cells, individuals,
                                                  outputfolder, distance_type='geometric',
                                                  embryo_res='halfs'):
    """
    Writes a script that plots the distance matrix, for te distance between te different individuals
    Parameters
    ----------
    global_avg_distances_matrix
    dimensions_to_normalise nd array nxn : dimension to divide corresponding distance values to, to normalise
    nb_cells
    individuals
    outputfolder

    Returns
    -------

    """
    output_dir = os.path.join(outputfolder, f"{distance_type}_distance_matrix_btw_embryos")
    if embryo_res == 'halfs':
        output_dir = os.path.join(output_dir, f"{distance_type}_distance_matrix_btw_halfs_embryos")
    local_filename = os.path.join(output_dir, f"{distance_type}_global_distance_btw_embryos_at_{nb_cells}.py")
    print('plot in', local_filename)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    else:
        print('Directory already exists:', output_dir)

    with open(local_filename, "w") as f:
        f.write("import numpy as np\n")
        f.write("import matplotlib.pyplot as plt\n\n")
        f.write("import scipy.cluster.hierarchy as sch\n\n")
        f.write("from scipy.spatial.distance import squareform\n")

        f.write(f"# Average distances matrix\n")
        f.write(f"global_avg_distances_matrix = np.array({global_avg_distances_matrix.tolist()})\n\n")
        f.write(f"dimensions_to_normalise = np.array({dimensions_to_normalise.tolist()})\n\n")
        f.write(f"individual_names = {individuals}\n\n")

        # Normalization of the distance matrix
        f.write("# Normalization of the distance matrix\n")
        f.write("normalized_distance_matrix = global_avg_distances_matrix / dimensions_to_normalise\n")
        f.write("np.fill_diagonal(normalized_distance_matrix, 0)\n")  # Set diagonal elements to 0

        f.write("# Perform hierarchical clustering (Ward's method)\n")
        f.write("condensedD = squareform(global_avg_distances_matrix)\n")

        # Create a grid for subplot
        f.write("fig = plt.figure(figsize=(12, 12))\n")
        f.write("ax1 = fig.add_axes([0.09, 0.1, 0.2, 0.8])\n")

        f.write("Y1 = sch.linkage(condensedD, method='ward')\n")
        f.write("Z1 = sch.dendrogram(Y1, orientation='left')\n")
        f.write("ax1.set_xticks([])\n")
        f.write("ax1.set_yticks([])\n")
        f.write("ax1.set_yticklabels([])\n")
        f.write("idx1 = Z1['leaves']\n")
        f.write("D = normalized_distance_matrix[idx1, :]\n")
        f.write("D = D[:, idx1]\n")

        f.write("min_val = np.min(D[np.nonzero(D)])\n")
        f.write("max_val = np.max(D)\n")

        f.write("axmatrix = fig.add_axes([0.3, 0.1, 0.6, 0.8])\n")
        f.write("im = axmatrix.matshow(D, aspect='auto', origin='lower', cmap='viridis', vmin=min_val, vmax=max_val)\n")
        f.write("axmatrix.xaxis.set_ticks_position('bottom')\n")
        f.write("axmatrix.xaxis.set_label_position('bottom')\n")
        f.write("axmatrix.set_xticks(range(len(idx1)))\n")
        f.write("axmatrix.set_xticklabels([individual_names[i] for i in idx1], rotation=90, fontsize=14)\n")
        f.write("axmatrix.set_yticks(range(len(idx1)))\n")
        f.write("axmatrix.set_yticklabels([individual_names[i] for i in idx1], fontsize=14)\n")
        f.write("axcolor = fig.add_axes([0.91, 0.1, 0.02, 0.8])\n")
        f.write("cbar = plt.colorbar(im, cax=axcolor, label='Average Distance')\n")
        f.write("cbar.set_ticks([min_val, max_val])\n")

        # adjust title
        f.write(f"axmatrix.set_title('Distance Matrix {distance_type}  at {nb_cells} cell stage, embryo volume fixed to: 1620000um^3', pad=20, fontsize=14)\n")
        f.write("axmatrix.title.set_position([0.5, 1.05])\n")
        f.write("\n")
        f.write("\n")

        f.write("plt.tight_layout()\n")
        file = "DistanceMatrix.png"
        file = os.path.join(outputfolder, file)
        f.write(f"#plt.savefig('{file}', dpi=300)\n")
        f.write("plt.show()\n")
    print('plot should be written', output_dir, local_filename)

##################
##########


def write_plot_script_global_distance_btw_embryos_without_dendo(global_avg_distances_matrix, dimensions_to_normalise,
                                                  time, individuals,
                                                  outputfolder, distance_type='geometric', d_or_mean='avg',
                                                  embryo_res='halfs'):
    """
    Writes a script that plots the distance matrix, for te distance between te different individuals
    Parameters
    ----------
    global_avg_distances_matrix
    dimensions_to_normalise nd array nxn : dimension to divide corresponding distance values to, to normalise
    nb_cells
    individuals
    outputfolder

    Returns
    -------

    """
    output_dir = os.path.join(outputfolder, f"{distance_type.replace(' ','_')}_distance_matrix_btw_embryos")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    if embryo_res == 'halfs':
        output_dir = os.path.join(output_dir, f"{distance_type.replace(' ','_')}_distance_matrix_btw_halfs_embryos")
    local_filename = os.path.join(output_dir, f"{distance_type.replace(' ','_')}_global_distance_btw_embryos_at_{time}.py")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    with open(local_filename, "w") as f:
        f.write("import numpy as np\n")
        f.write("import matplotlib.pyplot as plt\n\n")

        f.write(f"# Average distances matrix\n")
        f.write(f"global_avg_distances_matrix = np.array({global_avg_distances_matrix.tolist()})\n\n")
        f.write(f"individual_names = {individuals}\n\n")

        f.write("# Plotting the distance matrix\n")
        f.write("plt.figure(figsize=(10, 8))\n")
        f.write("plt.imshow(global_avg_distances_matrix, cmap='viridis', interpolation='nearest')\n")

        if d_or_mean == 'avg':
            f.write("plt.colorbar(label='Average Distance')\n")
        else:
            f.write("plt.colorbar(label='Distance')\n")

        f.write("plt.xticks(ticks=np.arange(len(individual_names)), labels=individual_names, rotation=90)\n")
        f.write("plt.yticks(ticks=np.arange(len(individual_names)), labels=individual_names)\n")
        f.write(f"plt.title('Global Distance Matrix Between Embryos at {time}')\n")
        f.write("plt.xlabel('Individuals')\n")
        f.write("plt.ylabel('Individuals')\n")

        f.write("# Adding the values inside each square\n")
        f.write("#for i in range(global_avg_distances_matrix.shape[0]):\n")
        f.write("#    for j in range(global_avg_distances_matrix.shape[1]):\n")
        f.write(
            "#        plt.text(j, i, f'{int(global_avg_distances_matrix[i, j])}', ha='center', va='center', color='white')\n")

        f.write("plt.tight_layout()\n")
        file = "DistanceMatrix.png"
        file = os.path.join(outputfolder, file)
        f.write(f"plt.savefig('{file}', dpi=300)\n")
        f.write("plt.show()\n")
    print('plot in', local_filename)
    print(output_dir)

def distance_matrix_geom_btw_individuals_at_t(dict_embryos_in_same_ref_at_t, embryos_names, cell_volumes_at_t=None):
    """
    Compute the geometric distance matrix between individuals at a given time point.

    Parameters:
        dict_embryos_in_same_ref_at_t (dict): A dictionary where keys are embryo names and values are dictionaries of cell positions.
        embryos_names (list): A list of embryo names.
        cell_volumes_at_t (dict, optional): A dictionary where keys are cell names and values are cell volumes. Default is None.

    Returns:
        global_avg_distances_matrix (ndarray): A 2D array of average distances between individuals.
        dimensions (ndarray): A 2D array of the number of dimensions (cells) used for each distance calculation.
    """
    num_embryos = len(embryos_names)
    global_avg_distances_matrix = np.zeros((num_embryos, num_embryos))
    dimensions = np.zeros((num_embryos, num_embryos))

    for i in range(num_embryos):
        for j in range(i + 1, num_embryos):
            individual_i = embryos_names[i]
            individual_j = embryos_names[j]

            common_cells = set(dict_embryos_in_same_ref_at_t[individual_i].keys()).intersection(
                dict_embryos_in_same_ref_at_t[individual_j].keys())

            if common_cells:
                if cell_volumes_at_t:
                    total_volume = sum(cell_volumes_at_t[cell][0] for cell in common_cells)
                    avg_distance = np.sum(
                        [cell_volumes_at_t[cell][0] * np.linalg.norm(
                            dict_embryos_in_same_ref_at_t[individual_i][cell] -
                            dict_embryos_in_same_ref_at_t[individual_j][cell])
                         for cell in common_cells]) / total_volume
                else:
                    avg_distance = np.mean(
                        [np.linalg.norm(
                            dict_embryos_in_same_ref_at_t[individual_i][cell] -
                            dict_embryos_in_same_ref_at_t[individual_j][cell])
                            for cell in common_cells])
            else:
                avg_distance = np.inf  # or some large value to indicate no common cells

            global_avg_distances_matrix[i, j] = avg_distance
            global_avg_distances_matrix[j, i] = avg_distance

            dimensions[i, j] = len(common_cells)
            dimensions[j, i] = len(common_cells)

    return global_avg_distances_matrix, dimensions


def get_vectors_before_registration(pm1_names, pmx_barycenters, pmx_names):
    """
    for each name in pm1
        search for its homologue in pmx
        add to filtered_names and filtered_barycentres
        if no index is found in pmx
            get daughters names /get mother name
            search for mother or daughters in pmx
            insert them into vectors
    """
    pmx_barycenters_filtered, pmx_names_filtered = [], []
    err = ""
    for i, name in enumerate(pm1_names):
        em_index = pmx_names.index(name)
        pmx_barycenters_filtered.append(pmx_barycenters[em_index])
        pmx_names_filtered.append(name)
    return pmx_names_filtered, pmx_barycenters_filtered


def sort_lists_according_to_cell_name(ref_name, ref_bary, ref_ids):
    ref_pairs = sorted(zip(ref_name, ref_bary, ref_ids), key=lambda x: x[0])
    ref_name, ref_bary, ref_ids = zip(*ref_pairs)
    return ref_name, ref_bary, ref_ids


def get_embryo_dict_at_time(list_dict_barycentrs, list_dict_names, list_dict_mass, taq):
    pmx_barycenters_at_t_dict = get_embryo_at_t(list_dict_barycentrs, taq)
    pmx_barycenters_at_t_dict = dict(sorted(pmx_barycenters_at_t_dict.items(), key=lambda x: x[0]))
    pmx_barycenters_at_t = pmx_barycenters_at_t_dict

    pmx_names_at_t_dict = get_embryo_at_t(list_dict_names, taq)
    pmx_names_at_t_dict = dict(sorted(pmx_names_at_t_dict.items(), key=lambda x: x[0]))
    pmx_names_at_t = pmx_names_at_t_dict

    pmx_names_at_t_sets = set(pmx_names_at_t_dict.values())

    pmx_mass_at_t_dict = get_embryo_at_t(list_dict_mass, taq)
    pmx_mass_at_t_dict = dict(sorted(pmx_mass_at_t_dict.items(), key=lambda x: x[0]))
    pmx_mass_at_t = pmx_mass_at_t_dict

    return pmx_barycenters_at_t, pmx_names_at_t, pmx_mass_at_t


def get_embryo_data_at_time(list_dict_barycentrs, list_dict_names, list_dict_mass, taq):
    pmx_barycenters_at_t_dict = get_embryo_at_t(list_dict_barycentrs, taq)
    pmx_barycenters_at_t_dict = dict(sorted(pmx_barycenters_at_t_dict.items(), key=lambda x: x[0]))
    pmx_barycenters_at_t = list(pmx_barycenters_at_t_dict.values())

    pmx_names_at_t_dict = get_embryo_at_t(list_dict_names, taq)
    pmx_names_at_t_dict = dict(sorted(pmx_names_at_t_dict.items(), key=lambda x: x[0]))
    pmx_names_at_t = list(pmx_names_at_t_dict.values())

    pmx_names_at_t_sets = set(pmx_names_at_t_dict.values())

    pmx_mass_at_t_dict = get_embryo_at_t(list_dict_mass, taq)
    pmx_mass_at_t_dict = dict(sorted(pmx_mass_at_t_dict.items(), key=lambda x: x[0]))
    pmx_mass_at_t = list(pmx_mass_at_t_dict.values())

    return pmx_barycenters_at_t, pmx_names_at_t, pmx_mass_at_t

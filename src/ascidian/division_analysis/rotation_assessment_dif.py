import numpy as np
import pickle as pkl
import os

from ascidian.division_analysis.analyse_directions_approximations import angle_btw_vectors
from ascidian.division_analysis.registration_assessment_tools import (local_register, get_neighborhood_dicts, get_rotation_angle,
                                                         get_rotation_angles_dif, give_neighbours_ids)
from ascidian.division_analysis.division_analysis_init_tools import (get_first_last_timestamps, get_divided_cells_ids_at_t,
                                                        filter_cells_with_short_life_daughters, get_embryo_at_t,
                                                        get_cells_name, get_cell_ids, get_cell_id_at_div,
                                                        get_daughters_ids_at_tn, get_cell_name_parts,
                                                        reduce_to_closest_ancestry, apply_transformation,
                                                        update_barycenters, estimate_sym_vector,
                                                        apply_symetry_on_vector,
                                                        barycenter_after_symmetry, global_similitude, nb_of_cells)


def rotations_assessment(files, ref_filename, delay, output_folder='', generations=[7, 8, 9]):
    """
    For local and global registration, it will for each division compute rotation angles difference and plot their boxplot
    aim is to see when are the two registrations mostly affecting the division directions variability analysis
    Parameters
    ----------
    files
    ref_filename
    delay

    Returns
    -------

    """
    reg_properties = {}
    reg_properties_ll = {}
    reg_properties_h = {}
    # call fct for each embryo
    with open(ref_filename, 'rb') as file:
        ref_props = pkl.load(file)
        ref_name = ref_filename.split('-')[1]

    for filename in files:
        if filename == ref_filename:
            continue
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)

        embryo_name = filename.split('-')[1]
        print("starting", embryo_name)
        reg_properties = compute_local_global_reg_rotations_angles(pm_props, ref_props,
                                                                   delay, reg_properties, ref_name, embryo_name,
                                                                   'neighbours')
        reg_properties_ll = compute_local_global_reg_rotations_angles(pm_props, ref_props,
                                                                      delay, reg_properties_ll, ref_name, embryo_name,
                                                                      'neighbours_neighbours')
        reg_properties_h = compute_local_global_reg_rotations_angles(pm_props, ref_props,
                                                                      delay, reg_properties_h, ref_name, embryo_name,
                                                                      'half_embryo')
    for gen in generations:
        # plot reg_properties info for each cell
        output_file = os.path.join(output_folder,"_rotations_assessment", f"local_vs_global_rotations_angles_{gen}.py")
        script_plot_rotations_assessment(reg_properties, gen, output_file)

        output_file = os.path.join(output_folder,"_rotations_assessment",
                                   f"local_wider_surrounding_vs_global_rotations_angles_{gen}.py")
        script_plot_rotations_assessment(reg_properties_ll, gen, output_file)

        output_file = os.path.join(output_folder,"_rotations_assessment",
                                   f"half_embryo_vs_global_rotations_angles_{gen}.py")
        script_plot_rotations_assessment(reg_properties_h, gen, output_file)


def script_plot_rotations_assessment(reg_properties, gen, output_file):
    """
    plot boxplot of every cell, rotations angles difference
    Parameters
    ----------
    reg_properties
    output_folder

    Returns
    -------

    """
    os.makedirs(os.path.dirname(output_file), exist_ok=True)
    print("written to", output_file)
    # sort reg_properties by mothers name
    sorted_reg_properties = {k: reg_properties[k] for k in sorted(reg_properties) if
                             int(k.split('.')[0][1:]) == gen}
    # get mother names
    mothers = list(sorted_reg_properties.keys())
    # get lists of values
    values = list(sorted_reg_properties.values())
    values_t = []
    for mother, value in zip(mothers, values):
        values_t.append([theta for theta in value['theta']])
    # write script
    with open(output_file, 'w') as script_file:
            script_file.write(f"""
import matplotlib.pyplot as plt

values_t = {values_t}
mothers = {mothers}

fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(10, 8))

# Plot boxplot for values_t
axes.boxplot(values_t)
axes.set_xlabel('Mother name')
axes.set_ylabel('Deviation Angle (degrees)')
axes.set_title("Global vs local registration's rotations angles differences")
axes.set_xticks(range(1, len(mothers) + 1))
axes.set_xticklabels(mothers, rotation=45, ha='right')
axes.set_ylim(0, 180)

# Adjust layout
plt.tight_layout()
#plt.show()
plt.savefig('combined_figure.png', dpi=300)
    """)


def compute_local_global_reg_rotations_angles(embryo_properties, reference_properties, delay,
                                              reg_properties, ref_name, pmx_name, mode='neighbours'):
    """
    For every division, compute its registration (local and global) and then save their angular differences

    Parameters
    ----------
    mode
    embryo_properties
    reference_properties
    delay
    reg_properties

    Returns
    -------

    """
    # registration properties = {'mother_name':[list of angular difference values]}
    # direction_type can be 'bary' or 'interface'

    # Get embryos names, times, different properties
    ref_properties = reference_properties
    pmx_properties = embryo_properties
    dict_barycenter_pm1 = ref_properties['cell_barycenter']
    dict_barycenter_pmx = pmx_properties['cell_barycenter']
    first_pm1_time, last_pm1_time = get_first_last_timestamps(dict_barycenter_pm1)

    # for each time in ref:
    for t in range(first_pm1_time, last_pm1_time):
        # get divided cells ids at t, filter(delay after division), get cells names
        dict_cell_lineage_ref_at_t = get_embryo_at_t(ref_properties['cell_lineage'], t)
        list_of_divided_cells_ids_at_t = get_divided_cells_ids_at_t(dict_cell_lineage_ref_at_t)

        # filter cells with short life daughters
        list_of_divided_cells_ids_at_t_pm1 = filter_cells_with_short_life_daughters(ref_properties['cell_lineage'],
                                                                                    list_of_divided_cells_ids_at_t,
                                                                                    delay)
        # get list of the cell's names
        list_of_divided_cells_names_at_t_pm1 = get_cells_name(ref_properties['cell_name'],
                                                              list_of_divided_cells_ids_at_t_pm1)

        # get cell ids in the other embryo
        # from cell names, get cell ids
        list_of_cell_divided_ids_pmx, list_of_cell_divided_names_pmx = get_cell_ids(pmx_properties['cell_name'],
                                                                                    list_of_divided_cells_names_at_t_pm1)

        if len(list_of_cell_divided_names_pmx) != len(list_of_divided_cells_names_at_t_pm1):
            # keep only the names and ids that exist in pm1 and pmx
            set_of_names_pmx = set(list_of_cell_divided_names_pmx)

            # Synchronize cell names and IDs in pm1 and pmx
            synced_list_of_cell_divided_ids_pmx = []
            synced_list_of_cell_divided_names_pmx = []

            for id_pm1, name_pm1 in zip(list_of_divided_cells_ids_at_t_pm1, list_of_divided_cells_names_at_t_pm1):
                # Check if the name exists in pm1
                if name_pm1 in set_of_names_pmx:
                    synced_list_of_cell_divided_ids_pmx.append(id_pm1)
                    synced_list_of_cell_divided_names_pmx.append(name_pm1)

            # Update the lists
            list_of_divided_cells_ids_at_t_pm1 = synced_list_of_cell_divided_ids_pmx
            list_of_divided_cells_names_at_t_pm1 = synced_list_of_cell_divided_names_pmx

        ##
        # Iterate over divided cell pairs
        ##
        for divided_cell_id_pmx, divided_cell_id_pm1 in zip(list_of_cell_divided_ids_pmx,
                                                            list_of_divided_cells_ids_at_t_pm1):
            # Make divided_cell_id_pmx: divided_cell_id_pmx at division time
            divided_cell_id_pmx = get_cell_id_at_div(pmx_properties['cell_lineage'], divided_cell_id_pmx)

            if divided_cell_id_pmx is None:
                # Lineage of cell in pmx not continuous
                continue

            # Get embryos at delayed time (t + delay)
            # ref
            dict_barycenters_pm1_at_t_delayed = get_embryo_at_t(ref_properties['cell_barycenter'], t + delay)
            dict_names_pm1_at_t_delayed = get_embryo_at_t(ref_properties['cell_name'], t + delay)
            # pmx
            division_time_pmx = int(divided_cell_id_pmx / 10000)
            dict_barycenters_pmx_at_t_delayed = get_embryo_at_t(pmx_properties['cell_barycenter'],
                                                                division_time_pmx + delay)
            dict_names_pmx_at_t_delayed = get_embryo_at_t(pmx_properties['cell_name'], division_time_pmx + delay)

            # Get daughters' IDs at t + delay
            daughter1_id_pmx, daughter2_id_pmx = get_daughters_ids_at_tn(
                pmx_properties['cell_lineage'], divided_cell_id_pmx, delay
            )

            daughter1_id_pm1, daughter2_id_pm1 = get_daughters_ids_at_tn(
                ref_properties['cell_lineage'], divided_cell_id_pm1, delay
            )

            if daughter1_id_pmx is not None and daughter2_id_pmx is not None and daughter1_id_pm1 is not None and daughter2_id_pm1 is not None:
                if isinstance(daughter1_id_pmx, list):
                    daughter1_id_pmx = int(daughter1_id_pmx[0])
                if isinstance(daughter2_id_pmx, list):
                    daughter2_id_pmx = int(daughter2_id_pmx[0])
                if isinstance(daughter1_id_pm1, list):
                    daughter1_id_pm1 = int(daughter1_id_pm1[0])
                if isinstance(daughter2_id_pm1, list):
                    daughter2_id_pm1 = int(daughter2_id_pm1[0])

                # Get names of daughters at t + delay
                name_daughter_pmx_1 = dict_names_pmx_at_t_delayed.get(daughter1_id_pmx)
                name_daughter_pmx_2 = dict_names_pmx_at_t_delayed.get(daughter2_id_pmx)

                name_daughter_pm1_1 = dict_names_pm1_at_t_delayed.get(daughter1_id_pm1)
                name_daughter_pm1_2 = dict_names_pm1_at_t_delayed.get(daughter2_id_pm1)

                # Check if names of daughters in both embryos are the same
                if (name_daughter_pmx_1 == name_daughter_pm1_1 and name_daughter_pmx_2 == name_daughter_pm1_2) or \
                        (name_daughter_pmx_1 == name_daughter_pm1_2 and name_daughter_pmx_2 == name_daughter_pm1_1):
                    # Daughter names match in both embryos
                    # Process the matching daughters
                    _, _, q1, _ = get_cell_name_parts(name_daughter_pmx_1)

                    if q1 % 2 == 0:
                        # Swap the names and IDs
                        name_daughter_pmx_1, name_daughter_pmx_2 = name_daughter_pmx_2, name_daughter_pmx_1
                        daughter1_id_pmx, daughter2_id_pmx = daughter2_id_pmx, daughter1_id_pmx

                    _, gen, q1, _ = get_cell_name_parts(name_daughter_pm1_1)

                    if q1 % 2 == 0:
                        # Swap the names and IDs
                        name_daughter_pm1_1, name_daughter_pm1_2 = name_daughter_pm1_2, name_daughter_pm1_1
                        daughter1_id_pm1, daughter2_id_pm1 = daughter2_id_pm1, daughter1_id_pm1

                    ##
                    ### Apply local reg: to register pms daughter's barycenters
                    #
                    # get neighboorhoods of daughters of pmx
                    neighbouring_ids_pmx = give_neighbours_ids(pmx_properties, daughter1_id_pmx, daughter2_id_pmx, mode)

                    neighborhood_volumes_pmx = {i: v for i, v in pmx_properties['cell_volume'].items() if
                                                i in neighbouring_ids_pmx}
                    neighborhood_barycenters_pmx = {i: bary for i, bary in pmx_properties['cell_barycenter'].items() if
                                                    i in neighbouring_ids_pmx}
                    neighborhood_names_pmx = {i: name for i, name in pmx_properties['cell_name'].items() if
                                              i in neighbouring_ids_pmx}

                    # get neighbourhoods of daughters of pm1
                    neighbouring_ids_ref = give_neighbours_ids(ref_properties, daughter1_id_pm1, daughter2_id_pm1, mode)

                    neighbouring_ids_ref = [i for i in neighbouring_ids_ref if i % 1000 != 1]
                    neighbouring_ids_pmx = [i for i in neighbouring_ids_pmx if i % 1000 != 1]
                    neighbouring_ids_ref = list(set(neighbouring_ids_ref))
                    neighbouring_ids_pmx = list(set(neighbouring_ids_pmx))

                    neighborhood_volumes_ref = {i: v for i, v in ref_properties['cell_volume'].items() if
                                                i in neighbouring_ids_ref}
                    neighborhood_barycenters_ref = {i: bary for i, bary in ref_properties['cell_barycenter'].items() if
                                                    i in neighbouring_ids_ref}
                    neighborhood_names_ref = {i: name for i, name in ref_properties['cell_name'].items() if
                                              i in neighbouring_ids_ref}

                    # print('in mode:', mode, 'nb of neighbours:', len(neighbouring_ids_pmx))
                    # most common ancestry:
                    reduced_barycenters_ref, reduced_barycenters_pmx, \
                        reduced_cell_name_ref, reduced_cell_name_pmx = reduce_to_closest_ancestry(
                        neighborhood_barycenters_ref, neighborhood_barycenters_pmx, neighborhood_volumes_ref,
                        neighborhood_volumes_pmx, neighborhood_names_ref, neighborhood_names_pmx)

                    # compute transform
                    transform_l, ref, floating, sorted_cell_name = local_register(
                        list(reduced_barycenters_ref.values()),
                        list(reduced_cell_name_ref.values()),
                        list(reduced_barycenters_pmx.values()),
                        list(reduced_cell_name_pmx.values())
                    )

                    # compute global transform
                    ##
                    ### Apply global reg: to register pms daughter's barycenters
                    #
                    # return to most common ancestry
                    reduced_barycenters_pm1, reduced_barycenters_pmx, \
                        reduced_cell_name_pm1, reduced_cell_name_pmx = reduce_to_closest_ancestry(
                        dict_barycenters_pm1_at_t_delayed, dict_barycenters_pmx_at_t_delayed,
                        ref_properties['cell_volume'], pmx_properties['cell_volume'],
                        dict_names_pm1_at_t_delayed, dict_names_pmx_at_t_delayed
                    )
                    nb_cells_pm1_after = nb_of_cells(reduced_barycenters_pm1)
                    nb_cells_pmx_after = nb_of_cells(reduced_barycenters_pmx)

                    if nb_cells_pm1_after == nb_cells_pmx_after:
                        # Global registration
                        transform_g, floating, _, _ = global_similitude(reduced_barycenters_pm1,
                                                                        reduced_cell_name_pm1,
                                                                        reduced_barycenters_pmx,
                                                                        reduced_cell_name_pmx,
                                                                        ref_name=ref_name, registered_ind=pmx_name,
                                                                        t_in_ref=t + delay,
                                                                        t_in_ind=division_time_pmx + delay)

                        # transforms angles
                        theta = get_rotation_angles_dif(transform_l, transform_g)

                        if pmx_properties['cell_name'][divided_cell_id_pmx][:-1] not in reg_properties:
                            reg_properties[pmx_properties['cell_name'][divided_cell_id_pmx][:-1]] = {}
                            reg_properties[pmx_properties['cell_name'][divided_cell_id_pmx][:-1]]['theta'] = []
                        reg_properties[pmx_properties['cell_name'][divided_cell_id_pmx][:-1]]['theta'].append(
                            theta)
                        if abs(theta) > 10:
                            print('theta', theta, 'mode:',mode, 'nb of cells', len(reduced_barycenters_ref),
                                  'mother', pmx_properties['cell_name'][divided_cell_id_pmx], 'pmx_name', pmx_name)
    return reg_properties

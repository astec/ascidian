import numpy as np
import os
import pickle as pkl
from sklearn.decomposition import PCA

from ascidian.division_analysis.figure_variability import compute_avg_division_time
from ascidian.division_analysis.division_analysis_init_tools import apply_symetry_on_vector


#############
######################
def plot_cell_apical_pts_and_directions(divisions_properties, mother, output_folder):
    """
    Given cell name, creates a folder that has for each individual a folder, that contains a py file The latter plots
    cell apical pts, approximated division direction and apical surface approximated the longest direction Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
                                   Example: {
                                    'mother_name': {
                                    'division_directions_after_symmetry':
                                        {'left': {'individual name': division direction vector},
                                        'right': {'individual name': division direction vector} }
                                                    },
                                    'gen': int ,
                                    'apical_surface_pts_at_div_time' : {'direction':{'individual':   } },
                                    'apical_surface_direction' : {'direction':{'individual': [longest_directions,
                                                                                    second_logest_direction]  }}
                                        }
    mother (str): cell considered
    output_folder (str): Path to the output Python script.

    Returns
    -------

    """
    mother_division_props = divisions_properties[mother]
    division_directions_props = mother_division_props.get('apical_surface_pts_at_div_time', {})

    output_folder = os.path.join(output_folder, "apical_surface_analysis", "plot_points_vectors", f"{mother}")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for lr, direction in division_directions_props.items():
        for ind, apical_coords in direction.items():
            if isinstance(apical_coords, np.ndarray):
                # get direction of div, apical longest axis, individual name, apical coordinates array(3,N)
                apical_direction1 = list(mother_division_props['apical_surface_direction'][lr][ind][0])
                apical_direction2 = list(mother_division_props['apical_surface_direction'][lr][ind][1])
                # apply symetry
                if lr == 'left':
                    sym_vector = divisions_properties[mother]['embryo_symmetry_plane_at_t'][ind]
                    apical_direction1 = list(apply_symetry_on_vector(sym_vector, apical_direction1))
                    apical_direction2 = list(apply_symetry_on_vector(sym_vector, apical_direction2))

                division_directions = list(mother_division_props['division_directions_after_symmetry'][lr][ind])
                apical_coordinates = []
                individual = ind + '_' + lr
                for apical_coord in apical_coords:
                    apical_coordinates.append(list(apical_coord))

                # script write fct
                output_file = os.path.join(output_folder, f'{individual}.py')
                write_apical_plot_script(mother, apical_coordinates, apical_direction1, apical_direction2,
                                         division_directions,
                                         individual, output_file)


def write_apical_plot_script(mother, apical_coords, apical_direction1, apical_direction2, division_direction,
                             individual, output_file):
    """
    Writes a Python script to plot the apical 3D coordinates along with the division direction and the two apical longest directions.

    Parameters
    ----------
    apical_coords : array_like
        List of 3D coordinates of apical points.
    apical_direction1 : array_like
        First apical longest direction vector.
    apical_direction2 : array_like
        Second apical longest direction vector.
    division_direction : array_like
        Division direction vector.
    individual : str
        Name of the individual.
    output_file : str
        Path to the output Python script.

    Returns
    -------
    None
    """
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

# Apical coordinates
apical_coords = np.array({apical_coords})

# Apical direction vectors
apical_direction1 = np.array({apical_direction1})
apical_direction2 = np.array({apical_direction2})

# Division direction vector
division_direction = np.array({division_direction})

# Calculate the normal to the plane defined by apical_direction1 and apical_direction2
plane_normal = np.cross(apical_direction1, apical_direction2)
division_direction_projected = division_direction - np.dot(division_direction, plane_normal) * plane_normal
angle = np.arccos(np.dot(apical_direction1, division_direction_projected) / (np.linalg.norm(apical_direction1) * np.linalg.norm(division_direction_projected)))
angle_degrees = np.degrees(angle)


centroid = np.mean(apical_coords, axis=1)

# Compute pairwise Euclidean distances between apical coordinates
diff = apical_coords[:, np.newaxis, :] - apical_coords[:, :, np.newaxis]
distances = np.sqrt(np.sum(diff ** 2, axis=0))
average_distance = np.mean(distances)

# Scale vectors based on average distance
vector_scale = average_distance * 0.3 

# Compute plane defined by the two apical vectors
min_x = np.min(apical_coords[0,:])
max_x = np.max(apical_coords[0,:])
min_y = np.min(apical_coords[1,:])
max_y = np.max(apical_coords[1,:])
centroid_x = centroid[0]
centroid_y = centroid[1]
plane_normal = np.cross(apical_direction1, apical_direction2)
xx, yy = np.meshgrid(np.linspace(centroid_x - (max_x - min_x)/2, centroid_x + (max_x - min_x)/2, 100), 
                     np.linspace(centroid_y - (max_y - min_y)/2, centroid_y + (max_y - min_y)/2, 100))

# Calculate z coordinates of the plane
zz = (-plane_normal[0]*(xx - centroid_x) - plane_normal[1]*(yy - centroid_y) + centroid[2]*plane_normal[2])/plane_normal[2]

# Create 3D plot
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Plot apical coordinates
ax.scatter(apical_coords[0,:], apical_coords[1, :], apical_coords[2, :], c='b', label='Apical Coordinates')

# Plot division direction
ax.quiver(centroid[0], centroid[1], centroid[2], division_direction_projected[0]*vector_scale, division_direction_projected[1]*vector_scale, division_direction_projected[2]*vector_scale, color='r', label='Division Direction')
# Plot apical longest directions
ax.quiver(centroid[0], centroid[1], centroid[2], apical_direction1[0]*vector_scale, apical_direction1[1]*vector_scale, apical_direction1[2]*vector_scale, color='g', label='Apical Longest Direction')
ax.quiver(centroid[0], centroid[1], centroid[2], apical_direction2[0]*vector_scale, apical_direction2[1]*vector_scale, apical_direction2[2]*vector_scale, color='orange', label='Apical Second Longest Direction')

# Plot the plane
ax.plot_surface(xx, yy, zz, alpha=0.5, color='gray')

ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
ax.set_title('Apical Coordinates with Directions for "b7.0007" in ' + "pm3_left")
ax.legend()

plt.show()

""")


def approximate_apical_direction(divisions_properties, generations=[7], output_folder=''):
    """
    For gen 7 cells still have apical surface, we want to test if knowing few cell points (three way junctions) with
    background
    Estimate the apical longest direction and would it be deviated from the approximated division direction ?
    For gen 7 divisons :
        get division in each individual, get apical points, approximate longest apical direction, save in divisions_properties
    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
                                   Example: {
                                    'mother_name': {
                                    'division_directions_after_symmetry':
                                        {'left': {'individual name': division direction vector},
                                        'right': {'individual name': division direction vector} }
                                                    },
                                    'gen': int ,
                                    'apical_surface_pts_at_div_time' : {'direction':{'individual':   } }
                                        } }
    generations (list) : list of division generations to be considered
    output_folder(str): Path to the output Python script.

    Returns
    -------

    """
    if generations is None:
        generations = [7]

    # filter 7th generation divisions in divisions_properties
    for gen in generations:
        # Filter divisions properties for the current generation
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == gen}

    for mother_name, properties in gen_divisions.items():
        division_directions = properties.get('apical_surface_pts_at_div_time', {})
        for lr, direction in division_directions.items():
            for ind, apical_coords in direction.items():
                if isinstance(apical_coords, np.ndarray):
                    # apical_coords is (3,N) np array approximate longest apical direction
                    # given 3d coordinates computes the principal
                    apical_longest_direction, second_longest_direction, explained_variance_ratio = get_pca_components_and_variance(
                        apical_coords)

                    # save in divisions_properties
                    if 'apical_surface_direction' not in divisions_properties[mother_name]:
                        divisions_properties[mother_name]['apical_surface_direction'] = {}
                    if lr not in divisions_properties[mother_name]['apical_surface_direction']:
                        divisions_properties[mother_name]['apical_surface_direction'][lr] = {}
                    divisions_properties[mother_name]['apical_surface_direction'][lr][ind] = [apical_longest_direction,
                                                                                              second_longest_direction]

                    # save variance ratio
                    if 'explained_variance_ratio' not in divisions_properties[mother_name]:
                        divisions_properties[mother_name]['explained_variance_ratio'] = {}
                    if lr not in divisions_properties[mother_name]['explained_variance_ratio']:
                        divisions_properties[mother_name]['explained_variance_ratio'][lr] = {}
                    divisions_properties[mother_name]['explained_variance_ratio'][lr][ind] = explained_variance_ratio

    # save divisions_properties in : divisions_properties.pkl
    # Save divisions_properties
    output_file = os.path.join(output_folder, 'divisions_properties.pkl')
    with open(output_file, 'wb') as f:
        pkl.dump(divisions_properties, f)


def get_pca_components_and_variance(apical_points):
    """
    Approximate the longest  direction based on the (3,N) coordinates.
    PCA eigenvectors = direction of variability in data
    Parameters
    ----------
    apical_points (np.array): 3D coordinates of apical points, size is (3,N), have N points
        Apical surface points.

    Returns
    -------
    array
        Approximated the longest apical direction vector.
    """
    pca = PCA(n_components=3)
    pca.fit(apical_points.T)
    # The first principal component represents the longest direction
    longest_direction = pca.components_[0]
    second_longest_direction = pca.components_[1]

    # get pca values too
    explained_variance_ratio = pca.explained_variance_ratio_

    # test if vectors are orthogonal
    dot_product = np.dot(longest_direction, second_longest_direction)
    if not np.isclose(dot_product, 0):
        print("The vectors are not orthogonal.")

    return longest_direction, second_longest_direction, explained_variance_ratio


def plot_boxplot_deviation_div_direction_apical_surface_longest_axis(divisions_properties, generation=[7],
                                                                     output_folder=''):
    """
    Aim is to compare apical surface longest axis to the approximated division direction
    Plots boxplot ( by default for gen 7 , cause all cells have apical surface) of deviation btween division direction
     and apical longest direction
    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
                                   Example: {
                                    'mother_name': {
                                    'division_directions_after_symmetry':
                                        {'left': {'individual name': direction vector},
                                        'right': {'individual name': direction vector} }
                                                    },
                                    'gen': int,
                                    'apical_surface_direction' : {'dir': {'individual name': apical_direction }

                                        } }
    gen (list): list of divisions generations to be considered
    output_folder(str): Path to the output Python script.

    Returns
    -------

    """
    # Create a figure for each generation
    for gen in set(generation):
        # print('creating figure (cell division time boxplot) for generation:', gen)
        # Filter divisions properties for the current generation
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == gen}

        mother_names = []
        apical_dir = []
        apical_orthogonal_dir = []
        div_dir = []
        variance_ratio = []
        individual = []

        for mother_name, properties in gen_divisions.items():
            # if apical_surface_direction not in dictionary create using approximate_apical_direction()
            #approximate_apical_direction(divisions_properties, generation, output_folder)

            for lr, ind_prop in properties['apical_surface_edges_principal_vectors'].items():
                for embryo_name, apical_direction in ind_prop.items():
                    if (embryo_name in properties['division_directions_before_registration'][lr] and
                            apical_direction is not None):
                        individual.append(embryo_name+'_'+lr)
                        #apical_direction = np.array(apical_direction).T
                        v1 = np.array(apical_direction[0])
                        v2 = np.array(apical_direction[1])
                        # get apical direction
                        # apply symetry on apical directions
                        # delete
                        #if lr == 'left':
                        #    sym_vector = divisions_properties[mother_name]['embryo_symmetry_plane_at_t'][embryo_name]
                        #    v1 = apply_symetry_on_vector(sym_vector, apical_direction[0])
                        #    v2 = apply_symetry_on_vector(sym_vector, apical_direction[1])

                        # get variance ratio
                        variance_ratio.append(properties['apical_surface_edges_principal_values'][lr][embryo_name][1] / properties['apical_surface_edges_principal_values'][lr][embryo_name][0])
                        apical_dir.append(v1)
                        apical_orthogonal_dir.append(v2)

                        # get division direction
                        div_dir.append(properties['division_directions_before_registration'][lr][embryo_name])
                        # get mother name
                        mother_names.append(mother_name)

        # In output_path writes a file that plots the boxplot of all cells for the current generation
        write_boxplot_apical_direction_deviation_script(gen, mother_names, apical_dir, apical_orthogonal_dir, div_dir,
                                                        variance_ratio,individual,
                                                        output_folder)


def angle_vector_direction(apical, div):
    angle = np.arccos(np.dot(apical, div) / (np.linalg.norm(apical) * np.linalg.norm(div)))
    inverted_angle = np.arccos(np.dot(-apical, div) / (np.linalg.norm(apical) * np.linalg.norm(div)))
    angle=np.degrees(angle)
    inverted_angle=np.degrees(inverted_angle)
    min_angle = min(angle, inverted_angle)
    return min_angle


def compute_angle_btw_dir_and_vector(apical_dir, div_dir):
    """
    Computes the angle between direction an orientation

    Parameters:
    apical_dir (list): List of apical direction vectors.
    div_dir (list): List of division direction vectors.

    Returns:
    list: List of angles between apical and division directions.
    """
    angles = []
    if not isinstance(apical_dir, list):
        apical_dir = [apical_dir]
    if not isinstance(div_dir, list):
        div_dir = [div_dir]

    for apical, div in zip(apical_dir, div_dir):
        min_angle = angle_vector_direction(apical, div)
        angles.append(int(min_angle))
    if len(angles) == 1:
        return angles[0]
    return angles


def projection_and_angle_to_plane(apical_dir, apical_orthogonal_dir, div_dir):
    """
    For every vectors apical_dir and apical_orthogonal_dir, get angle of div_dir to plane and its projection to the plane
    Parameters
    ----------
    apical_dir (np.array)
    apical_orthogonal_dir(np.array)
    div_dir(np.array)

    Returns
    -------
    projection_of_plane
    angle_to_plane
    """
    # Compute the normal vector to the plane defined by apical_dir and apical_orthogonal_dir
    plane_normal = np.cross(apical_dir, apical_orthogonal_dir)

    # Compute the angle between div_dir and the normal vector to the plane
    angle_to_normal = np.arccos(
        np.dot(div_dir, plane_normal) / (np.linalg.norm(div_dir) * np.linalg.norm(plane_normal)))

    # Compute the angle between div_dir and the plane
    angle_to_plane = np.abs(90 - np.degrees(angle_to_normal))
    # Compute the projection of div_dir onto the plane
    orthogonal_component = (np.dot(div_dir, plane_normal) / (np.linalg.norm(plane_normal) **2 )) * plane_normal
    projection_on_plane = div_dir - orthogonal_component
    return projection_on_plane, angle_to_plane


def compute_projection_to_plane_and_angles_to_plane(apical_dir, apical_orthogonal_dir, div_dir):
    """
    For all vectors apical_dir and apical_orthogonal_dir, get the angle of div_dir to plane and its projection to the plane
    Parameters
    ----------
    apical_dir (list of np.array)
    apical_orthogonal_dir(list of np.array)
    div_dir(list of np.array)

    Returns
    -------
    projection_of_plane (list)
    angle_to_plane (list)
    """
    projections = []
    angles = []

    if not isinstance(apical_dir, list):
        apical_dir = [apical_dir]
    if not isinstance(apical_orthogonal_dir, list):
        apical_orthogonal_dir = [apical_orthogonal_dir]
    if not isinstance(div_dir, list):
        div_dir = [div_dir]

    for i, apical_direction in enumerate(apical_dir):
        projection_to_plane, angles_to_plane = projection_and_angle_to_plane(apical_direction, apical_orthogonal_dir[i],
                                                                             div_dir[i])
        projections.append(projection_to_plane)
        angles.append(angles_to_plane)
    return projections, angles


def write_boxplot_apical_direction_deviation_script(gen, mother_names, apical_dir, apical_orthogonal_dir, div_dir,
                                                    variance_ratio, individuals , fate_group, output_folder, interphase_delay=1
                                                    , title1='', title2=''):
    """
    Writes a Python script to generate a boxplot for each mother_name.
    The values of each boxplot are the angles between apical_dir and div_dir

    Parameters:
    gen (int): Generation number.
    mother_names (list): List of mother cell names.
    adjusted_times_per_mother (list): List of lists containing adjusted acquisition times for each mother.
    output_file (str): Path to the output Python script.

    Returns
    -------
    None
    """
    # todo make output folder previously chosen

    # Construct the output file path
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    output_file_deviation_from_plane = os.path.join(output_folder, f'boxplot_angle_to_apical_plane_generation_{gen}.py')
    output_file_deviation_in_plane = os.path.join(output_folder, f'boxplot_angle_in_apical_plane_generation_{gen}.py')
    print('written to', output_file_deviation_from_plane)

    # compute angle of div_dir to plan defined by: apical_dir, apical_orthogonal_dir
    projections, angles_to_plane = compute_projection_to_plane_and_angles_to_plane(apical_dir, apical_orthogonal_dir,
                                                                                   div_dir)

    # compute of projected orientation to apical direction
    angles_in_plane = compute_angle_btw_dir_and_vector(apical_dir, projections)


    # handle fate groups
    # Group mother names by fate group
    grouped_mothers = {}

    for i, mother in enumerate(set(mother_names)):
        a=[]
        b=[]
        # get idx of mother in mothers_names list
        fates = []
        for idx, name in enumerate(mother_names):
            if name == mother:
                # get values in idx, construct lists : angles_in_plane, variance_ratio
                a.append(angles_in_plane[idx])
                b.append(variance_ratio[idx])
                if fate_group[idx] not in fates:
                    fates.append(fate_group[idx])

        for fate in fates:
            if fate not in grouped_mothers:
                grouped_mothers[fate] = []
            grouped_mothers[fate].append((mother, a, b))

    # adjust plot : two boxplots
    # Write the Python script
    with open(output_file_deviation_in_plane, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches

angles_grouped_per_mother = [] 
variance_ratios_per_mother = []                         

mothers_ = []

box_colors = {{'Epid.': 'red', 'NS': 'blue', 'Mesoderm': 'green', 'End.': 'purple'}}  

legend_handles = []    
fate_group = []
grouped_mothers = {grouped_mothers}
grouped_mothers = dict(sorted(grouped_mothers.items()))
positions = 0
for fate, mothers_data in grouped_mothers.items():
    sorted_mothers_data = sorted(mothers_data, key=lambda x: x[0])
    for mother, angles, v_ratios in sorted_mothers_data:
         angles_grouped_per_mother.append(angles)
         variance_ratios_per_mother.append(v_ratios)
         mothers_.append(mother)
         fate_group.append(fate)
    
# Plot boxplot
fig, ax1 = plt.subplots(figsize=(12, 6))
ax2 = ax1.twinx()  # Create a secondary y-axis

mother_index = 1
width = 0.4  # Width of each boxplot

for fate, mother, angles, variances in zip(fate_group, mothers_, angles_grouped_per_mother, variance_ratios_per_mother):
    # Plot deviation angle boxplot
    ax1.boxplot(angles, positions=[mother_index - width / 2], showfliers=False, patch_artist=True,
                boxprops=dict(facecolor=box_colors[fate]))
    
    # Plot variance ratio boxplot
    ax2.boxplot(variances, positions=[mother_index + width / 2], showfliers=False, patch_artist=True,
                boxprops=dict(facecolor='orange'))

    mother_index += 1

# Set x-axis ticks and labels
ax1.set_xticks(range(1, len(mothers_) + 1))
ax1.set_xticklabels(mothers_, rotation=45, ha='right')
ax1.set_xlabel('Mother names')

# Set y-axis labels
ax1.set_ylabel('Deviation (degrees)')
ax2.set_ylabel('Variance Ratio', color='orange')
ax2.set_ylim(0,1)

# Set title
ax1.set_title('Boxplot of Deviation between apical interphasic longest axis and the cell division direction and Variance Ratio')

# Add legend
legend_handles = []
for fate, color in box_colors.items():
    legend_handles.append(mpatches.Patch(color=color, label=fate))
ax1.legend(handles=legend_handles)

plt.tight_layout()
plt.show()
""")

    grouped_mothers = {}
    for i, mother in enumerate(set(mother_names)):
        a = []
        b = []
        # get idx of mother in mothers_names list
        for idx, name in enumerate(mother_names):
            if name == mother:
                # get values in idx, construct lists : angles_in_plane, variance_ratio
                a.append(angles_to_plane[idx])
                b.append(variance_ratio[idx])

        fate = fate_group[i]
        if type(fate) is list:
            fate = fate[0]
        if fate not in grouped_mothers:
            grouped_mothers[fate] = []

        grouped_mothers[fate].append((mother, a, b))

    with open(output_file_deviation_from_plane, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches

angles_grouped_per_mother = [] 
variance_ratios_per_mother = []    
fate_group = []
mothers_ = []

grouped_mothers = {grouped_mothers}
grouped_mothers = dict(sorted(grouped_mothers.items()))

box_colors = {{'Epid.': 'red', 'NS': 'blue', 'Mesoderm': 'green', 'End.': 'purple'}} 

for fate, mothers_data in grouped_mothers.items():
    sorted_mothers_data = sorted(mothers_data, key=lambda x: x[0])
    for mother, angles, v_ratios in sorted_mothers_data:
         angles_grouped_per_mother.append(angles)
         variance_ratios_per_mother.append(v_ratios)
         mothers_.append(mother)
         fate_group.append(fate)

# Plot boxplot
fig, ax1 = plt.subplots(figsize=(12, 6))
ax2 = ax1.twinx()  # Create a secondary y-axis

mother_index = 1
width = 0.4  # Width of each boxplot

for fate, mother, angles, variances in zip(fate_group, mothers_, angles_grouped_per_mother, variance_ratios_per_mother):
    # Plot deviation angle boxplot
    ax1.boxplot(angles, positions=[mother_index - width / 2], showfliers=False, patch_artist=True,
                boxprops=dict(facecolor=box_colors[fate]))
    
    # Plot variance ratio boxplot
    ax2.boxplot(variances, positions=[mother_index + width / 2], showfliers=False, patch_artist=True,
                boxprops=dict(facecolor='orange'))

    mother_index += 1

# Set x-axis ticks and labels
ax1.set_xticks(range(1, len(mothers_) + 1))
ax1.set_xticklabels(mothers_, rotation=45, ha='right')
ax1.set_xlabel('Mother names')

# Set y-axis labels
ax1.set_ylabel('Deviation (degrees)')
ax2.set_ylabel('Variance Ratio', color='orange')
ax2.set_ylim(0,1)

# Set title
ax1.set_title('Boxplot of Deviation between apical surface and the cell division direction')

# Add legend
legend_handles = []
for fate, color in box_colors.items():
    legend_handles.append(mpatches.Patch(color=color, label=fate))
ax1.legend(handles=legend_handles)

plt.tight_layout()
plt.show()
""")


############### Tobe deleted eventually
def prediction_histogram(divisions_properties, time_alignment_coefficients, generation=[7], accepted_theta=10,
                         accepted_phi=20, output_folder=''):
    """
    Aim is to test if apical surface longest axis to the approximated division direction
    Generates histogram of the portion of prediction
    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
                                   Example: {
                                    'mother_name': {
                                    'division_directions_after_symmetry':
                                        {'left': {'individual name': direction vector},
                                        'right': {'individual name': direction vector} }
                                                    },
                                    'gen': int,
                                    'apical_surface_direction' : {'dir': {'individual name': apical_direction }

                                        } }
    generation (list): list of division generations to be considered
    accepted_theta (float): angle in degree, angle to apical plane
    accepted_phi (float): angle in degree, angle to apical longest direction
    output_folder(str): Path to the output Python script.

    Returns
    -------

    """
    # Construct the output file path
    output_folder = os.path.join(output_folder, "apical_surface_analysis")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # Create a figure for each generation
    for gen in set(generation):
        # print('creating figure (cell division time boxplot) for generation:', gen)
        # Filter divisions properties for the current generation
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == gen}

        mother_names = []
        binary_good_prediction = []
        mothers_avg_div_time = []
        for mother_name, properties in gen_divisions.items():
            for lr, ind_prop in properties['apical_surface_direction'].items():
                for embryo_name, apical_direction in ind_prop.items():
                    if embryo_name in properties['division_directions_after_symmetry'][lr]:
                        # get mother name
                        mother_names.append(mother_name)
                        # get avg division time
                        avg_div_time = compute_avg_division_time(properties.get('division_aq_time', {}),
                                                                 time_alignment_coefficients)
                        mothers_avg_div_time.append(avg_div_time)
                        # compute theta and alpha
                        projection_of_plane, theta_angle = projection_and_angle_to_plane(apical_direction[0],
                                                                                         apical_direction[1],
                                                                                         properties[
                                                                                             'division_directions_after_symmetry'][
                                                                                             lr][embryo_name])
                        phi_angle = angle_vector_direction(apical_direction[0], projection_of_plane)
                        # predict
                        if theta_angle < accepted_theta and phi_angle < accepted_phi:
                            binary_good_prediction.append(1)
                        else:
                            binary_good_prediction.append(0)

        output_file = os.path.join(output_folder,
                                   f'prediction_histogram_gen{gen}_limits_{accepted_theta}_accepted_phi{accepted_phi}.py')
        generate_histogram_script(mother_names, binary_good_prediction, mothers_avg_div_time, accepted_theta,
                                  accepted_phi, output_file)


# script function
def generate_histogram_script(mother_names, binary_good_prediction, mothers_avg_div_time, accepted_theta, accepted_phi,
                              output_file):
    """
    Writes a Python script to generate a histogram of the portion of prediction
    Parameters
    ----------
    mother_names : list
        List of mother cell names.
    binary_good_prediction : list
        List of binary values indicating whether the prediction is good or not.
    output_file : str
        Path to the output Python script.
    Returns
    -------
    None
    """

    # Write the Python script
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
import numpy as np

mother_names = {mother_names}
binary_good_prediction = {binary_good_prediction}
avg_div_time = {mothers_avg_div_time}

# Get unique mother names
unique_mothers = list(set(mother_names))

# Sort unique mother names based on average division time
sorted_mothers = sorted(unique_mothers, key=lambda mother: avg_div_time[mother_names.index(mother)])

portion_of_true_values = []

# Iterate over each unique mother name
for mother in sorted_mothers:
    # Get the indexes corresponding to the current mother name
    idx = [i for i, m in enumerate(mother_names) if m == mother]
    # Get the corresponding binary predictions for the current mother
    predictions_for_mother = [binary_good_prediction[i] for i in idx]
    # Calculate the portion of true values for the current mother
    portion_of_true_values.append(sum(predictions_for_mother) / len(predictions_for_mother))

# Set x positions for the bars
x_positions = np.arange(len(sorted_mothers))

# Plot the histogram
plt.bar(x_positions, portion_of_true_values)
plt.xlabel('Mother Name')
plt.ylabel('Portion of True Values')
plt.ylim(0,1)
plt.title('Portion of good Predictions (Sorted by Avg Div Time), accepted angles: phi={accepted_phi}, Theta={accepted_theta}')
plt.xticks(x_positions, sorted_mothers, rotation=45, ha='right')
plt.tight_layout()
plt.show()
""")


######################
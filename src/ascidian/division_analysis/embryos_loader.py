import os
import pickle


# example usage in main

def get_atlases_info(parameters):
    """
    Given a parameters object, returns a list of dictionaries containing information about each specified atlas.

    Each dictionary includes the following keys:
    - 'atlas_name': Name of the atlas
    - 'path': Path to the atlas file
    - 'is_reference': True if the atlas is the reference (for the registration), False otherwise
    """
    atlases_info = []
    # Iterate over the atlas files specified in parameters.atlasFiles
    for atlas_path in parameters.atlasFiles:
        atlas_name = os.path.splitext(os.path.basename(atlas_path))[0]
        is_reference = (atlas_path == parameters.atlasRefFile[0])  # Check if it matches the reference file

        atlas_info = {
            'atlas_name': atlas_name,
            'path': atlas_path,
            'is_reference': is_reference
        }

        atlases_info.append(atlas_info)

    return atlases_info


def get_embryos_properties(atlases_info):
    """
    Given folder name and the list of atlases' information,
    returns :
    1. A list of dictionaries containing properties for each embryo.
    2. A dictionary containing properties for the reference embryo.
    """
    embryos_data = {}
    reference_embryo_data = {}

    for atlas_info in atlases_info:
        atlas_path = atlas_info['path']
        atlas_name = atlas_info['atlas_name']

        with open(atlas_path, 'rb') as f:
            atlas_data = pickle.load(f)

            if atlas_info['is_reference']:
                reference_embryo_data[atlas_name] = atlas_data
                embryos_data[atlas_name] = atlas_data
            else:
                embryos_data[atlas_name] = atlas_data

    return embryos_data, reference_embryo_data


def get_embryo_property(folder_name, reference_name):
    embryo_file_path = os.path.join(folder_name, reference_name + '.pkl')
    with open(embryo_file_path, 'rb') as f:
        atlas_data = pickle.load(f)
    return atlas_data


def print_atlases_info(atlases_info):
    """Prints information about each atlas in a formatted manner."""
    for atlas in atlases_info:
        print(f"Atlas Name: {atlas['atlas_name']}")
        print(f"Path: {atlas['path']}")
        print(f"Is Reference: {atlas['is_reference']}")
        print("\n")


def print_embryos_info(embryos_info):
    """Prints information about each embryo in a formatted manner."""
    for embryo in embryos_info:
        print(f"Atlas Name: {embryo['atlas_name']}")
        print(f"Embryo Properties: {embryo['embryo_properties']}")
        print("\n")


def main():
    folder_name = input("Enter the folder name: ")
    reference_name = input("Enter the name of the reference atlas: ")

    atlases_info = get_atlases_info(folder_name, reference_name)
    embryos_data, reference_embryo_data = get_embryos_properties(atlases_info)

    if atlases_info:
        print("Atlases Information:")
        print_atlases_info(atlases_info)
    else:
        print("No atlases found in the specified folder.")

    if embryos_data:
        print("Embryos Information:")
        print_embryos_info(embryos_data)
    else:
        print("No embryos found in the specified folder.")

    if reference_embryo_data:
        print("Reference Embryo Information:")
        print(f"Atlas Name: {reference_embryo_data['atlas_name']}")
        print(f"Embryo Properties: {reference_embryo_data['embryo_properties']}")
    else:
        print("No reference embryo found in the specified folder.")


if __name__ == "__main__":
    main()


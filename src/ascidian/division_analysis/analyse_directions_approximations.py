import pickle as pkl
import os
import numpy as np

from ascidian.division_analysis.division_analysis_init_tools import (get_first_last_timestamps, get_embryo_at_t,
                                                        reduce_to_closest_ancestry, apply_transformation,
                                                        get_divided_cells_ids_at_t,
                                                        filter_cells_with_short_life_daughters,
                                                        get_cells_name, get_daughters_ids_at_tn)
import ascidian.ascidian.name as uname
from ascidian.division_analysis.registration_assessment_tools import (local_register, get_neighborhood_dicts)
from ascidian.division_analysis.cell_apical_surface_analysis import write_boxplot_apical_direction_deviation_script
from ascidian.division_analysis.figure_variability import compute_avg_division_time
from ascidian.division_analysis.division_analysis_tools import determine_fate_group


###########
#####
## Compare division directions across generations
def compare_div_dir_to_mothers(files, delay, output_folder):
    """
    For each divison compare its division direction to its mother division direction

    Parameters
    ----------
    files: embryos files
    delay: delay since division time to approximate division direction
    output_folder : where to put .py script to plot

    Returns
    -------

    """
    # dict : {'embryo:'{'cell_name':'angle': angle_to_mother_div_dir}}
    results = {}

    # grab each embryo
    for filename in files:
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)

        embryo_name = filename.split('-')[1]
        print("Starting", embryo_name)

        pm_lineage = pm_props['cell_lineage']
        pm_names = pm_props['cell_name']
        pm_cell_volumes = pm_props['cell_volume']
        pm_barycenters = pm_props['cell_barycenter']
        pm_fates = pm_props['cell_fate']

        first_time, last_time = get_first_last_timestamps(pm_lineage)

        dividing_cell_names = []
        dividing_cell_times = []
        dividing_cell_id = []

        for t in range(first_time, last_time - delay):
            pm_lineage_at_t = get_embryo_at_t(pm_lineage, t)
            divided_ids = get_divided_cells_ids_at_t(pm_lineage_at_t)
            list_of_divided_cells_ids_at_t = filter_cells_with_short_life_daughters(pm_lineage,
                                                                                        divided_ids,
                                                                                        delay)
            # get list of the cell's names
            list_of_divided_cells_names_at_t = get_cells_name(pm_names,
                                                                  list_of_divided_cells_ids_at_t)

            for i, name in enumerate(list_of_divided_cells_names_at_t):
                dividing_cell_names.append(name)
                dividing_cell_times.append(t)
                dividing_cell_id.append(list_of_divided_cells_ids_at_t[i])

        for division_name in dividing_cell_names:
            # if its mother in list of dividing cells
            if division_name == 'Unknown':
                continue
            if uname.get_mother_name(division_name) in dividing_cell_names:
                # get div dir
                d_id = dividing_cell_id[dividing_cell_names.index(division_name)]
                daughter1_id, daughter2_id = get_daughters_ids_at_tn(
                    pm_lineage, d_id, delay
                )
                if daughter1_id is None or daughter2_id is None:
                    continue
                if isinstance(daughter1_id, list):
                    daughter1_id = daughter1_id[0]
                if isinstance(daughter2_id, list):
                    daughter2_id = daughter2_id[0]
                # get barycenters
                if daughter2_id not in pm_names or daughter1_id not in pm_names:
                    continue
                name_d1 = pm_names[daughter1_id]
                name_d2 = pm_names[daughter2_id]
                _, _, q1, _ = uname.get_cell_name_parts(name_d1)
                if q1 % 2 == 0:
                    # Swap the names and IDs
                    name_d2, name_d1 = name_d1, name_d2
                    daughter2_id, daughter1_id = daughter1_id, daughter2_id

                bary1 = pm_barycenters[daughter1_id]
                bary2 = pm_barycenters[daughter2_id]

                #
                t = dividing_cell_times[dividing_cell_names.index(division_name)]
                dict_names_at_t2 = get_embryo_at_t(pm_names, t+delay)
                dict_barycenters_at_t2 = get_embryo_at_t(pm_barycenters, t+delay)
                dict_cell_volume_at_t2 = get_embryo_at_t(pm_cell_volumes, t+delay)

                # + mother division dir
                m_id = dividing_cell_id[dividing_cell_names.index(uname.get_mother_name(division_name))]
                m1_id, m2_id = get_daughters_ids_at_tn(
                    pm_lineage, m_id, delay
                )
                if m1_id is None or m2_id is None:
                    continue
                if isinstance(m1_id, list):
                    m1_id = m1_id[0]
                if isinstance(m2_id, list):
                    m2_id = m2_id[0]

                #
                # get barycenters and div dir
                if m1_id not in pm_names or m2_id not in pm_names:
                    continue

                # get barycenters
                name_d1 = pm_names[m1_id]
                name_d2 = pm_names[m2_id]
                _, _, q1, _ = uname.get_cell_name_parts(name_d1)
                if q1 % 2 == 0:
                    # Swap the names and IDs
                    name_d2, name_d1 = name_d1, name_d2
                    m2_id, m1_id = m1_id, m2_id

                bary1_m = pm_barycenters[m1_id]
                bary2_m = pm_barycenters[m2_id]

                #
                t = dividing_cell_times[dividing_cell_names.index(uname.get_mother_name(division_name))]
                dict_names_at_t1 = get_embryo_at_t(pm_names, t+delay)
                dict_barycenters_at_t1 = get_embryo_at_t(pm_barycenters, t+delay)
                dict_cell_volume_at_t1 = get_embryo_at_t(pm_cell_volumes, t+delay)

                # + neighbourhood of mother
                # get daughters ids, volumes, barycenters of neighbours
                neighbouring_ids_t1 = list(pm_props['cell_contact_surface'][m1_id].keys())
                neighbouring_ids_t1.extend(list(pm_props['cell_contact_surface'][m2_id].keys()))
                neighbouring_ids_t1 = [id for id in neighbouring_ids_t1 if id != m1_id and id != m2_id]

                neighborhood_names_t1 = {i: name for i, name in dict_names_at_t1.items() if i in neighbouring_ids_t1
                                         and i != m2_id and i!=m1_id}
                neighborhood_volumes_t1 = {i: v for i, v in dict_cell_volume_at_t1.items() if i in neighbouring_ids_t1
                                           and i != m2_id and i!=m1_id}
                neighborhood_barycenters_t1 = {i: bary for i, bary in dict_barycenters_at_t1.items() if
                                               i in neighbouring_ids_t1
                                               and i != m2_id and i!=m1_id}

                # get daughter division neighborhood
                # get daughters ids, volumes, barycenters of neighbours
                neighbouring_ids_t2 = list(pm_props['cell_contact_surface'][daughter1_id].keys())
                neighbouring_ids_t2.extend(list(pm_props['cell_contact_surface'][daughter2_id].keys()))
                neighbouring_ids_t2 = [id for id in neighbouring_ids_t2 if id != daughter2_id and id != daughter1_id]

                neighborhood_names_t2 = {i: name for i, name in dict_names_at_t2.items() if i in neighbouring_ids_t2
                                         and i != daughter1_id and i!=daughter2_id}
                neighborhood_volumes_t2 = {i: v for i, v in dict_cell_volume_at_t2.items() if i in neighbouring_ids_t2
                                           and i != daughter1_id and i!=daughter2_id}
                neighborhood_barycenters_t2 = {i: bary for i, bary in dict_barycenters_at_t2.items() if
                                               i in neighbouring_ids_t2 and i != daughter1_id and i!=daughter2_id}

                if len(neighbouring_ids_t1) < 5 and len(neighbouring_ids_t2)<5:
                    continue

                # todo remove from t1 non-mothers of t2

                #
                # reduction to most common ancester
                reduced_barycenters_t1, reduced_barycenters_t2, \
                    reduced_cell_name_t1, reduced_cell_name_t2 = reduce_to_closest_ancestry(neighborhood_barycenters_t1,
                                                                                            neighborhood_barycenters_t2,
                                                                                            neighborhood_volumes_t1,
                                                                                            neighborhood_volumes_t2,
                                                                                            neighborhood_names_t1,
                                                                                            neighborhood_names_t2)
                #print(len(reduced_cell_name_t1), len(reduced_cell_name_t2))
                if len(reduced_cell_name_t1) < 5 and len(reduced_cell_name_t2) <5:
                    continue
                # + register
                transform, ref, floating, sorted_cell_name = local_register(list(reduced_barycenters_t1.values()),
                                                                            list(reduced_cell_name_t1.values()),
                                                                            list(reduced_barycenters_t2.values()),
                                                                            list(reduced_cell_name_t2.values())
                                                                            )
                # + apply registration on daughter
                div_dir_d = (bary2 - bary1).reshape(3,-1)
                div_dir_d = apply_transformation(transform, div_dir_d, False)

                # angle to : bary2_m - bary1_m
                div_dir_d = div_dir_d.flatten()
                div_dir_m = bary2_m - bary1_m

                # store
                angle = angle_btw_vectors(div_dir_d, div_dir_m, False)

                fate_group = pm_fates[d_id]
                fate_group = determine_fate_group(fate_group, check_multiple=False)

                if embryo_name+division_name[-1] not in results:
                    results[embryo_name+division_name[-1]] = {}
                if division_name not in results[embryo_name+division_name[-1]]:
                    print('embryo', embryo_name+division_name[-1],division_name[:-1], angle)
                    results[embryo_name+division_name[-1]][division_name[:-1]] = (angle, fate_group)

    ##### call to plot
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    script_plot_mother_daughter_div_dir(results, output_folder)

def script_plot_mother_daughter_div_dir(results, output_folder):
    """
    Generates a Python script to plot the angles between division directions.

    Parameters
    ----------
    results: dict, containing the angles between division directions
                {'embryo:'{'cell_name':'angle': angle_to_mother_div_dir}}
    output_folder: str, path to save the plot

    Returns
    -------
    str, script content for the plot
    """
    script_content = f"""
import os
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

results = {results}

data = []
for embryo, cell_data in results.items():
    for cell_name, d in cell_data.items():
        print(d)
        angle, fate_group  = d
        gen = int(cell_name.split('.')[0][1:])
        data.append((embryo, cell_name, angle, fate_group, gen))

df = pd.DataFrame(data, columns=['Embryo', 'CellName', 'Angle', 'Fate', 'Generation'])

df = df.groupby('CellName').filter(lambda x: len(x) >= 5)

for gen in sorted(df['Generation'].unique()):
    plt.figure(figsize=(12, 8))
    gen_df = df[df['Generation'] == gen]
    gen_df.sort_values(by=['CellName'])
    sns.boxplot(x='CellName', y='Angle', hue='Fate', data=gen_df)
    plt.xlabel('Cell Name')
    plt.ylabel('Cell division direction to mothers division direction')
    plt.title(f'Angles Between Cells Division Directions and Mothers Division Directions (Generation {{gen}})')
    plt.xticks(rotation=90)
    plt.ylim(0,180)
    plt.tight_layout()
    plt.show()
    output_path = os.path.join(r'{output_folder}', f'division_direction_comparison_gen{{gen}}.png')
    plt.savefig(output_path)
    #plt.close()
    #print(f"Division direction comparison plot for generation {{gen}} saved to {{output_path}}")
"""
    script_path = os.path.join(output_folder, 'plot_division_direction_comparison.py')
    print('writing plot to:', script_path)
    with open(script_path, 'w') as script_file:
        script_file.write(script_content)

#####
############
def comparing_interphasic_directions(files, mother_gen=[7], div_direction='bary', compare_delay=-9, interphase_delay=12,
                                     scale='2d', output_folder='Hertwig_test'):
    """
    Compare apical longest direction at different times
    Projecting this interphase direction allows us to check if the local registration is gd or if the
    interphase directions are stable
    Parameters
    ----------
    files
    mother_gen
    div_direction='bary' or 'sisters_plane'
    max_delay delay to wait for stability of division direction
    output_folder
    scale (str): 2d or 3d

    Returns
    -------

    """
    output_folder = os.path.join(output_folder, f'Hertwig_test')


    mother_names = []
    individual = []
    angles_without_reg = {}
    angles_with_local_reg = {}

    # Get every file at its turn
    for filename in files:
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)

        embryo_name = filename.split('-')[1]
        print("starting", embryo_name)

        pm_lineage = pm_props['cell_lineage']

        if scale == '3d':
            pm_apical_directions = pm_props['cell_principal_vectors']
            pm_apical_principal_values = pm_props['cell_principal_values']
        else:
            pm_apical_directions = pm_props['apical_surface_edges_principal_vectors']
            pm_apical_principal_values = pm_props['apical_surface_edges_principal_vectors']

        first_time, last_time = get_first_last_timestamps(pm_lineage)

        for t in range(first_time, last_time - interphase_delay):

            pm_names = pm_props['cell_name']
            pm_fates = pm_props['cell_fate']
            dict_names_at_t1 = get_embryo_at_t(pm_names, t)
            dict_names_at_t2 = get_embryo_at_t(pm_names, t + interphase_delay + compare_delay)

            # get barycenters at t1 and t2
            pm_barycenters = pm_props['cell_barycenter']
            dict_barycenters_at_t1 = get_embryo_at_t(pm_barycenters, t)
            dict_barycenters_at_t2 = get_embryo_at_t(pm_barycenters, t + interphase_delay + compare_delay)

            # get cell volumes at t1 and t2
            pm_cell_volumes = pm_props['cell_volume']
            dict_cell_volume_at_t1 = get_embryo_at_t(pm_cell_volumes, t)
            dict_cell_volume_at_t2 = get_embryo_at_t(pm_cell_volumes, t + interphase_delay + compare_delay)

            for id_, mother_name in dict_names_at_t1.items():
                id = id_
                if uname.get_generation_name(mother_name) not in mother_gen:
                    continue
                if id_ not in pm_lineage:
                    continue
                # if cell is dividing in 12 timesteps
                if not will_cell_divide_in_t(pm_lineage, id, interphase_delay ):
                    continue
                if id not in pm_apical_directions:
                    continue
                # get apical longest axis during interphase (t-12)
                v1_t = np.array(pm_apical_directions[id][0])
                v2_t = np.array(pm_apical_directions[id][1])
                l1_t = pm_apical_principal_values[id][0]
                l2_t = pm_apical_principal_values[id][1]

                # get cell neighbourhood at t-12 : ref of local reg, make it a fct
                neighborhood_names_t1, neighborhood_volumes_t1, neighborhood_barycenters_t1 = get_neighborhood_dicts(
                    id,
                    dict_names_at_t1,
                    dict_cell_volume_at_t1,
                    dict_barycenters_at_t1,
                    pm_props)

                # get neighbourhood at t+delay cell divided at t + interphase_delay
                d1, d2 = uname.get_daughter_names(mother_name)
                # get daughters ids, volumes, barycenters of neighbours

                d1_id = next(i for i, name in dict_names_at_t2.items() if name == mother_name)
                neighbouring_ids_t2 = list(pm_props['cell_contact_surface'][d1_id].keys())

                # get neighbouring ids, names, volumes
                neighborhood_names_t2 = {i: name for i, name in dict_names_at_t2.items() if i in neighbouring_ids_t2}
                neighborhood_volumes_t2 = {i: v for i, v in dict_cell_volume_at_t2.items() if i in neighbouring_ids_t2}
                neighborhood_barycenters_t2 = {i: bary for i, bary in dict_barycenters_at_t2.items() if
                                               i in neighbouring_ids_t2}

                reduced_barycenters_t1, reduced_barycenters_t2, \
                    reduced_cell_name_t1, reduced_cell_name_t2 = reduce_to_closest_ancestry(neighborhood_barycenters_t1,
                                                                                            neighborhood_barycenters_t2,
                                                                                            neighborhood_volumes_t1,
                                                                                            neighborhood_volumes_t2,
                                                                                            neighborhood_names_t1,
                                                                                            neighborhood_names_t2)
                # compute local registration
                transform, ref, floating, sorted_cell_name = local_register(list(reduced_barycenters_t1.values()),
                                                                            list(reduced_cell_name_t1.values()),
                                                                            list(reduced_barycenters_t2.values()),
                                                                            list(reduced_cell_name_t2.values())
                                                                            )

                # apply transform on the direction at t+delay: interface sisters
                div_dir_b4_r = np.array(pm_apical_directions[d1_id][0])
                if div_dir_b4_r is None:
                    continue
                div_dir = np.array(div_dir_b4_r).T  # reshape to have 3,N shape
                div_dir = np.reshape(div_dir,(3,1))
                div_dir = apply_transformation(transform, div_dir, scale=False)
                div_dir = div_dir.T

                #  get angle btw registered_sisters_interface: div_dir and v1_t & v2_t
                # angle btw div_dir and v1_t
                alpha = angle_btw_vectors(div_dir, v1_t)[0]
                # angle btw div_dir_b4_r and v1_t
                theta = angle_btw_vectors(div_dir_b4_r, v1_t)
                if alpha > 20:
                    print('after reg:', alpha, 'before reg', theta, 'cell', mother_name, 'indiv', embryo_name + mother_name[-1])
                    print(t,t+interphase_delay+compare_delay)

                mother_names.append(mother_name[:-1])
                individual.append(embryo_name + mother_name[-1])
                if mother_name[:-1] not in angles_without_reg:
                    angles_without_reg[mother_name[:-1]] = []
                    angles_with_local_reg[mother_name[:-1]] = []
                angles_without_reg[mother_name[:-1]].append(theta)
                angles_with_local_reg[mother_name[:-1]].append(alpha)

    # script to plot with local reg
    write_script_plot_boxplot_direction_deviations(angles_without_reg, output_folder,
                                                   f"deviation_btw_interphasic_directions_without_reg.py",
                                                   'Deviation between interphasic directions, without registration')
    # script to plot without local registration
    write_script_plot_boxplot_direction_deviations(angles_with_local_reg, output_folder,
                                                   f"deviation_btw_interphasic_directions_with_local_reg.py",
                                                   'Deviation between interphasic directions, with local registration')


def test_hertwig(files, mother_gen=[7], div_direction='bary', max_delay=2, interphase_delay=12, scale='2d',
                 output_folder='Hertwig_test'):
    """
    When a division in an embryo is detected, get longest apical axis, cell the longest axis(pca of segmented cell)
    daughters barycenters axis/ or normal to surface
    Parameters
    ----------
    files
    mother_gen
    div_direction='bary' or 'sisters_plane'
    max_delay delay to wait for stability of division direction
    output_folder
    scale (str): 2d or 3d

    Returns
    -------

    """
    output_folder = os.path.join(output_folder, f'Hertwig_test')

    variance_ratio = []
    apical_dir = []
    apical_orthogonal_dir = []
    div_directions = []
    mother_names = []
    individual = []
    fate_groups = []

    angle_to_longest_axis = {}

    # Get every file at its turn
    for filename in files:
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)

        embryo_name = filename.split('-')[1]
        print("starting", embryo_name)

        pm_lineage = pm_props['cell_lineage']

        if scale == '3d':
            pm_apical_directions = pm_props['cell_principal_vectors']
            pm_apical_principal_values = pm_props['cell_principal_values']
        else:
            pm_apical_directions = pm_props['apical_surface_principal_vectors']
            pm_apical_principal_values = pm_props['apical_surface_principal_values']

        first_time, last_time = get_first_last_timestamps(pm_lineage)

        for t in range(first_time, last_time - interphase_delay):

            pm_names = pm_props['cell_name']
            pm_fates = pm_props['cell_fate']
            dict_names_at_t1 = get_embryo_at_t(pm_names, t)
            dict_names_at_t2 = get_embryo_at_t(pm_names, t + interphase_delay)

            # get barycenters at t1 and t2
            pm_barycenters = pm_props['cell_barycenter']
            dict_barycenters_at_t1 = get_embryo_at_t(pm_barycenters, t)
            dict_barycenters_at_t2 = get_embryo_at_t(pm_barycenters, t + interphase_delay)

            # get cell volumes at t1 and t2
            pm_cell_volumes = pm_props['cell_volume']
            dict_cell_volume_at_t1 = get_embryo_at_t(pm_cell_volumes, t)
            dict_cell_volume_at_t2 = get_embryo_at_t(pm_cell_volumes, t + interphase_delay)
            for id_, mother_name in dict_names_at_t1.items():
                id = id_
                if uname.get_generation_name(mother_name) not in mother_gen:
                    continue
                if id_ not in pm_lineage:
                    continue

                # if cell name have been passed already (for same mother_name and inividual): continue
                if mother_name[:-1] in mother_names:
                    if individual[mother_names.index(mother_name[:-1])] == (embryo_name + mother_name[-1]):
                        continue

                # if cell is dividing in 12 timesteps
                #if not will_cell_divide_in_t(pm_lineage, id, int(interphase_delay+5)):
                if not will_cell_divide_in_t(pm_lineage, id, interphase_delay):
                    continue
                #else:
                    # get t2 embryos
                #    dict_names_at_t2 = get_embryo_at_t(pm_names, t + int(interphase_delay+7))
                #    dict_barycenters_at_t2 = get_embryo_at_t(pm_barycenters, t + int(interphase_delay+7))
                #    dict_cell_volume_at_t2 = get_embryo_at_t(pm_cell_volumes, t + int(interphase_delay+7))
                if id not in pm_apical_directions:
                    continue
                # get apical longest axis during interphase (t-12)
                if scale == '3d':
                    v1_t = np.array(pm_apical_directions[id][1])
                    v2_t = np.array(pm_apical_directions[id][2])
                    l1_t = pm_apical_principal_values[id][1]
                    l2_t = pm_apical_principal_values[id][2]

                    n = np.array(pm_apical_directions[id][0])  # cell longest axis

                else:
                    v1_t = np.array(pm_apical_directions[id][0])
                    v2_t = np.array(pm_apical_directions[id][1])
                    l1_t = pm_apical_principal_values[id][0]
                    l2_t = pm_apical_principal_values[id][1]

                # get cell neighbourhood at t-12 : ref of local reg, make it a fct
                neighborhood_names_t1, neighborhood_volumes_t1, neighborhood_barycenters_t1 = get_neighborhood_dicts(
                    id,
                    dict_names_at_t1,
                    dict_cell_volume_at_t1,
                    dict_barycenters_at_t1,
                    pm_props)

                # get neighbourhood at t+delay cell divided at t+delay-2
                d1, d2 = uname.get_daughter_names(mother_name)
                # get daughters ids, volumes, barycenters of neighbours
                d1_id = next(i for i, name in dict_names_at_t2.items() if name == d1)
                neighbouring_ids_t2 = list(pm_props['cell_contact_surface'][d1_id].keys())
                d2_id = next(i for i, name in dict_names_at_t2.items() if name == d2)
                neighbouring_ids_t2.extend(list(pm_props['cell_contact_surface'][d2_id].keys()))

                # get neighbouring ids, names, volumes
                neighborhood_names_t2 = {i: name for i, name in dict_names_at_t2.items() if i in neighbouring_ids_t2}
                neighborhood_volumes_t2 = {i: v for i, v in dict_cell_volume_at_t2.items() if i in neighbouring_ids_t2}
                neighborhood_barycenters_t2 = {i: bary for i, bary in dict_barycenters_at_t2.items() if
                                               i in neighbouring_ids_t2}

                reduced_barycenters_t1, reduced_barycenters_t2, \
                    reduced_cell_name_t1, reduced_cell_name_t2 = reduce_to_closest_ancestry(neighborhood_barycenters_t1,
                                                                                            neighborhood_barycenters_t2,
                                                                                            neighborhood_volumes_t1,
                                                                                            neighborhood_volumes_t2,
                                                                                            neighborhood_names_t1,
                                                                                            neighborhood_names_t2)
                # compute local registration
                transform, ref, floating, sorted_cell_name = local_register(list(reduced_barycenters_t1.values()),
                                                                            list(reduced_cell_name_t1.values()),
                                                                            list(reduced_barycenters_t2.values()),
                                                                            list(reduced_cell_name_t2.values())
                                                                            )

                # apply transform on the direction at t+delay: interface sisters
                div_dir = pm_props.get('sisters_interface_vectors', {}).get(d1_id, None)
                if div_dir is None:
                    continue
                div_dir = np.array(div_dir).T  # reshape to have 3,N shape
                div_dir = apply_transformation(transform, div_dir, False)
                div_dir = div_dir.T
                div_dir = div_dir[2]
                if np.isnan(div_dir).all():
                    continue
                # handle fate groups
                if isinstance(determine_fate_group(pm_fates.get(id), True), list):
                    cell_fate_group = []
                    for fate in determine_fate_group(pm_fates.get(id), True):
                        if fate not in cell_fate_group:
                            cell_fate_group.append(fate)
                            fate_groups.append(fate)
                            #  get angle btw registered_sisters_interface: div_dir and v1_t & v2_t
                            variance_ratio.append(l2_t / l1_t)
                            # the highest value is 1: square, lowest value: 0,
                            # the longest axis well-defined
                            apical_dir.append(v1_t)
                            apical_orthogonal_dir.append(v2_t)
                            div_directions.append(div_dir)
                            mother_names.append(mother_name[:-1])
                            individual.append(embryo_name + mother_name[-1])

                            if scale == '3d':
                                if mother_name[:-1] not in angle_to_longest_axis:
                                    angle_to_longest_axis[mother_name[:-1]] = []
                                angle_to_longest_axis[mother_name[:-1]].append(angle_btw_vectors(div_dir, n))

                else:
                    fate_groups.append([determine_fate_group(pm_fates.get(id), True)])
                    #  get angle btw registered_sisters_interface: div_dir and v1_t & v2_t
                    variance_ratio.append(l2_t / l1_t)
                    # the highest value is 1: square, lowest value: 0,
                    # the longest axis well-defined
                    apical_dir.append(v1_t)
                    apical_orthogonal_dir.append(v2_t)
                    div_directions.append(div_dir)
                    mother_names.append(mother_name[:-1])
                    individual.append(embryo_name + mother_name[-1])

                    if scale == '3d':
                        if mother_name[:-1] not in angle_to_longest_axis:
                            angle_to_longest_axis[mother_name[:-1]] = []
                        angle_to_longest_axis[mother_name[:-1]].append(angle_btw_vectors(div_dir, n))

    # script to plot
    if scale == '3d':
        # script to plot
        title1 = f'Boxplot of Deviation Angle between Cell Longest Direction and Approximated Division, in Longest axis Surface,interphasic delay={interphase_delay}, Directions for Generation:{mother_gen}'
        title2 = f'Boxplot of Deviation Angle between Cell Longest Direction and Approximated Division, To Longest axis Surface,interphasic delay={interphase_delay}, Directions for Generation:{mother_gen}'
        output_folder = output_folder + '/cell_geometry_analysis' + f'interphasic_delay_{interphase_delay}'
        write_boxplot_apical_direction_deviation_script(mother_gen, mother_names, apical_dir, apical_orthogonal_dir,
                                                        div_directions, variance_ratio, individual, fate_groups,
                                                        output_folder, interphase_delay, title1, title2)

        # plot div dir to longest axis
        write_script_plot_boxplot_direction_deviations(angle_to_longest_axis, output_folder,
                                                       f"deviation_div_dir_cell_interphase_longest_axis.py",
                                                       'Deviation between the division direction and cell longest axis during interphase')

    else:
        title1 = f'Boxplot of Deviation Angle between Apical Longest Direction and Approximated Division, in Apical Surface,interphasic delay={interphase_delay}, Directions for Generation:{mother_gen}'
        title2 = f'Boxplot of Deviation Angle between Apical Longest Direction and Approximated Division, To Apical Surface,interphasic delay={interphase_delay}, Directions for Generation:{mother_gen}'
        output_folder = output_folder + '/apical_surface_analysis' + f'interphasic_delay_{interphase_delay}'
        write_boxplot_apical_direction_deviation_script(mother_gen, mother_names, apical_dir, apical_orthogonal_dir,
                                                        div_directions, variance_ratio, individual, fate_groups,
                                                        output_folder, interphase_delay, title1, title2)


def will_cell_divide_in_t(pm_lineage, id, delay):
    l = 0
    d = False
    while l != delay:
        l += 1
        if id not in pm_lineage:
            break
        id = pm_lineage[id]
        if isinstance(id, list):
            if len(id) == 2:
                if l == delay:
                    d = True
                break
            else:
                id = id[0]
    return d


def deviation_btw_sisters_interface_approximations_just_after_division(files, mother_gen=[7], folder_path=''):
    """
    Compare between sisters the deviation values

    We can approximate the interface btw sisters using 3way lines common between sisters
    (susceptible to being more sensible to under/over segmentation at the interface region)
    or using all the pixels in common between the two sisters

    Parameters:
        files (list): Path to the pkl files of embryo properties. (same location to store the filtered pickles)
                        sisters_interface_edges_vectors, sisters_surface_edges_principal_values
                        sisters_interface_vectors, sisters_surface_principal_values
        gen (list): list of int, the generations you want to look at
        folder_path (str): output folder
    Returns:
        None

    """
    deviation_values = {}  # 'division_mother_name': [deviation values]

    # Get every file at its turn
    for filename in files:
        print("starting", filename)
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)

        pm_lineage = pm_props['cell_lineage']
        pm_names = pm_props['cell_name']
        first_time, last_time = get_first_last_timestamps(pm_names)

        divisions_passed = []

        for t in range(first_time, last_time):
            pm_names_t = get_embryo_at_t(pm_names, t)
            for id_, mother_name in pm_names_t.items():
                id = id_
                l = 0

                # if mother name already in list ignore or else add it
                if mother_name in divisions_passed:
                    continue
                if uname.get_generation_name(mother_name) not in mother_gen:
                    continue
                if id_ not in pm_lineage:
                    continue
                # if lineage = 2
                if not isinstance(pm_lineage[id], list) or not len(pm_lineage[id]) == 2:
                    continue

                #   get daughters ids
                d1_id = pm_lineage[id][0]
                while l <= 6:

                    if (d1_id not in pm_props['sisters_interface_edges_vectors'] or
                            d1_id not in pm_props['sisters_interface_vectors']):
                        break
                    # get directions and alpha and store in mother dict
                    v_n_to_edges = np.array(pm_props['sisters_interface_edges_vectors'][d1_id][2])
                    v_n_to_surface = np.array(pm_props['sisters_interface_vectors'][d1_id][2])

                    # compute angle
                    alpha = angle_btw_vectors(v_n_to_edges, v_n_to_surface)

                    # store per mother, (for high values I can use the print to detect where they are)
                    if mother_name[:-1] not in deviation_values:
                        deviation_values[mother_name[:-1]] = []
                        deviation_values[mother_name[:-1]].append(alpha)
                    else:
                        deviation_values[mother_name[:-1]].append(alpha)
                    divisions_passed.append(mother_name)
                    if alpha > 10:
                        print('mother', mother_name, 'daughter_id:', d1_id, 'Deviation:', alpha, 'time since div', l)

                    # get cells ids next in lineage
                    # ensure lineage is continuous
                    if d1_id not in pm_lineage:
                        break
                    d1_id = pm_lineage[d1_id]
                    # ensure ids arent lists
                    if isinstance(d1_id, list):
                        if len(d1_id) == 2:
                            break
                        else:
                            d1_id = d1_id[0]
                    l += 1
        write_script_plot_boxplot_direction_deviations(deviation_values, folder_path)


def deviation_btw_sisters_interface_approximations(files, gen=[8], folder_path=''):
    """
    Compare between sisters the deviation values

    We can approximate the interface btw sisters using 3way lines common between sisters
    (susceptible to being more sensible to under/over segmentation at the interface region)
    or using all the pixels in common between the two sisters

    Parameters:
        files (list): Path to the pkl files of embryo properties. (same location to store the filtered pickles)
                        sisters_interface_edges_vectors, sisters_surface_edges_principal_values
                        sisters_interface_vectors, sisters_surface_principal_values
        gen (list): list of int, the generations you want to look at
        folder_path (str): output folder
    Returns:
        None

    """
    deviation_values = {}  # 'division_mother_name': [deviation values]

    # Get every file at its turn
    for filename in files:
        print("starting", filename)
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)

        pm_names = pm_props['cell_name']
        first_time, last_time = get_first_last_timestamps(pm_names)

        ids_passed = []

        for t in range(first_time, last_time):
            pm_names_t = get_embryo_at_t(pm_names, t)
            for id_, name in pm_names_t.items():
                if uname.get_generation_name(name) not in gen:
                    continue
                id = id_
                if id_ not in pm_props['sisters_interface_edges_vectors']:
                    continue
                sister_name = uname.get_sister_name(name)
                s_id = [i for i, n in pm_names_t.items() if n == sister_name]
                if not s_id or not id:
                    continue
                s_id = s_id[0]  # store in a list to not look at it later
                if s_id in ids_passed or id in ids_passed:
                    continue
                ids_passed.append(s_id)
                # get the two vectors
                v_n_to_edges = np.array(pm_props['sisters_interface_edges_vectors'][id_][2])
                v_n_to_surface = np.array(pm_props['sisters_interface_vectors'][id_][2])

                # compute angle
                alpha = angle_btw_vectors(v_n_to_edges, v_n_to_surface)
                # get mother name
                mother_name = uname.get_mother_name(name)
                # store per mother, (for high values I can use the print to detect where they are)
                if mother_name[:-1] not in deviation_values:
                    deviation_values[mother_name[:-1]] = []
                    deviation_values[mother_name[:-1]].append(alpha)
                else:
                    deviation_values[mother_name[:-1]].append(alpha)

                if alpha > 10:
                    print('mother', mother_name, 'daughter_id:', id_, 'Deviation:', alpha)
    # plot the boxplot per mother of deviations between the two approximations of vector normal to surface
    write_script_plot_boxplot_direction_deviations(deviation_values, folder_path)


def compute_stability_of_div_direction(files, output_folder, threshold=10, gen_ignored=[8]):
    """
    Check stability overconsecutive times of a vector stor in properties[key]

    #x get first and last timestep
    #x for each t
    #x   get embryo at t: name, barycenters, ids, lineage, sisters_interface_vectors
    #x   for each cell at t
    #x       get name, sister name, ids, lineage
    #x       if cell and its sister wont divide
    #x           get name, sister name, ids at t+1
    #           get direction at t and t+1
    #           append angle in list
    #   append angles to list

    Parameters:
        files (list): Path to the pkl files of embryos properties. (same location to store the filtered pickles)
        key (str): key of vector
        folder_path (str): output folder of .py that plot stability over consecutive timepoints
        gen_ignored (list)(int): generations of daughters considered
    Returns:
        None
    """
    angles_list_of_list_bary_axis = {}
    angles_list_of_list_interface_normal = {}
    angles_list_deviation_btw_bary_interface = {}
    angles_list_deviation_stability_btw_bary_interface = {}

    # Get every file at its turn
    for filename in files:
        print("starting", filename)
        filtered_props = {}
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)

        pm_names = pm_props['cell_name']
        pm_lineage = pm_props['cell_lineage']
        first_time, last_time = get_first_last_timestamps(pm_names)

        cells_passed_names = []

        for t in range(first_time, last_time):

            pm_names_t = get_embryo_at_t(pm_names, t)
            # todo look for divsions instead of sisters at t
            for id_, name in pm_names_t.items():
                if uname.get_generation_name(name) not in gen_ignored:
                    continue
                id = id_
                l = 1
                sister_name = uname.get_sister_name(name)
                if name in cells_passed_names or sister_name in cells_passed_names:
                    continue
                s_id = [i for i, n in pm_names_t.items() if n == sister_name]
                if not s_id or not id:
                    continue
                s_id = s_id[0]
                if s_id not in pm_lineage or id not in pm_lineage:
                    continue
                while len(pm_lineage[id]) == 1 and len(pm_lineage[s_id]) == 1:
                    if l == 21:
                        break
                    pm_names_t1 = get_embryo_at_t(pm_names, t + l)
                    id1 = [i for i, n in pm_names_t1.items() if n == name][0]
                    s_id1 = [i for i, n in pm_names_t1.items() if n == sister_name][0]
                    # get barycenters axis at t
                    dir_bary_axis_t = pm_props['cell_barycenter'][id] - pm_props['cell_barycenter'][s_id]
                    # get barycenter axis at t+1
                    dir_bary_axis_t1 = pm_props['cell_barycenter'][id1] - pm_props['cell_barycenter'][s_id1]

                    # get sisters plane at t and t=1
                    dir_n_interface = pm_props['sisters_interface_vectors'].get(id, [0])
                    dir_n_interface_t1 = pm_props['sisters_interface_vectors'].get(id1, [0])

                    if len(dir_n_interface) == 1 or len(dir_n_interface_t1) == 1:
                        id = id1
                        s_id = s_id1
                        if id not in pm_lineage or s_id not in pm_lineage:
                            break
                        l += 1
                        continue
                    dir_n_interface = np.array(dir_n_interface)
                    dir_n_interface_t1 = np.array(dir_n_interface_t1)
                    dir_n_interface = dir_n_interface[2]
                    dir_n_interface_t1 = dir_n_interface_t1[2]
                    # get angles put in list
                    if l > 1:
                        a = angle_list_deviation_btw_estimations

                    angle_list_bary_axis = angle_btw_vectors(dir_bary_axis_t, dir_bary_axis_t1)
                    angle_list_interface_normal = angle_btw_vectors(dir_n_interface, dir_n_interface_t1)
                    angle_list_deviation_btw_estimations = angle_btw_vectors(dir_n_interface, dir_bary_axis_t)
                    if l > 1:
                        a_np = np.array(a)
                        angle_list_deviation_btw_estimations_np = np.array(angle_list_deviation_btw_estimations)
                        a_t1 = a_np - angle_list_deviation_btw_estimations_np  # difference between the two lists

                    if l not in angles_list_of_list_bary_axis:
                        angles_list_of_list_bary_axis[l] = [angle_list_bary_axis]
                        angles_list_of_list_interface_normal[l] = [angle_list_interface_normal]
                        angles_list_deviation_btw_bary_interface[l] = [angle_list_deviation_btw_estimations]
                        if l > 1:
                            angles_list_deviation_stability_btw_bary_interface[l] = [a_t1]
                    else:
                        angles_list_of_list_bary_axis[l].append(angle_list_bary_axis)
                        angles_list_of_list_interface_normal[l].append(angle_list_interface_normal)
                        angles_list_deviation_btw_bary_interface[l].append(angle_list_deviation_btw_estimations)
                        if l > 1:
                            angles_list_deviation_stability_btw_bary_interface[l].append(a_t1)
                    cells_passed_names.append(name)
                    cells_passed_names.append(sister_name)
                    id = id1
                    s_id = s_id1
                    ####
                    # print when a vector is not that stable
                    if 8 > l > 1:
                        if a_t1 > threshold:
                            print('cells', name, sister_name, 'aq time', t, 'time since division', l)
                            # print('angle btw div sister interface at consecutive timepoints', angle_list_interface_normal)
                            print('deviation btw estimations btw consecutive timepoints', a_t1)
                    ####
                    if id not in pm_lineage or s_id not in pm_lineage:
                        break
                    l += 1

    # write plot file
    # todo add stability of deviation between the two estimations
    write_script_plot_direction_stability(angles_list_deviation_stability_btw_bary_interface,
                                          'Stability of division direction estimations deviation', output_folder)

    # deviation between the two estimations at t
    write_script_plot_direction_stability(angles_list_deviation_btw_bary_interface,
                                          'Deviation of division direction estimations at each t', output_folder)
    write_script_plot_direction_stability(angles_list_of_list_bary_axis,
                                          'Stability of sisters barycenters axis', output_folder)
    write_script_plot_direction_stability(angles_list_of_list_interface_normal,
                                          'Stability of sisters vector normal to interface', output_folder)


def write_script_plot_direction_stability(dict_values, title='', output_folder=''):
    output_folder = os.path.join(output_folder, "stability_axis")
    os.makedirs(output_folder, exist_ok=True)
    output_file = os.path.join(output_folder, f"{title.replace(' ', '_')}.py")

    sorted_dict = {k: dict_values[k] for k in sorted(dict_values)}
    # Get dictionary keys
    times_since_division = list(sorted_dict.keys())

    # For each key, value is a list, put it in a list of lists
    values_list = list(sorted_dict.values())

    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt

angles = {values_list}
times_since_division = {times_since_division}

# Plot boxplot
plt.figure(figsize=(10, 10))

# Subplot 1: Boxplot
plt.subplot(2, 1, 2)
plt.boxplot(angles)
plt.xlabel('Time since division')
plt.ylabel('Deviation Angle (degrees)')
plt.title('{title}')
plt.xticks(ticks=range(1, len(times_since_division) + 1), labels=times_since_division, rotation=45, ha='right')
plt.tight_layout()
plt.ylim(0, 90)

# Subplot 2: Number of cells
num_cells = [len(angle_list) for angle_list in angles]
plt.subplot(2, 1, 1)
plt.plot(times_since_division, num_cells, marker='o', color='r')
plt.xlabel('Time since division')
plt.ylabel('Number of Cells')
plt.title('Number of Cells over Time')
plt.grid(True)

plt.tight_layout()
plt.show()
""")


def write_script_plot_boxplot_direction_deviations(dict_values, output_folder='',
                                                   output_file=f"deviation_sisters_interface_approximation.py",
                                                   title='Deviation between the two approximations of the vector normal'
                                                         ' to the interface of the two sisters plans'):
    output_folder = os.path.join(output_folder, "deviation_btw_approximations")
    os.makedirs(output_folder, exist_ok=True)
    output_file = os.path.join(output_folder, output_file)
    print('written to :', output_file)
    sorted_dict = {k: dict_values[k] for k in sorted(dict_values)}
    # Get dictionary keys
    mother_names = list(sorted_dict.keys())

    # For each key, value is a list, put it in a list of lists
    values_list = list(sorted_dict.values())

    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt

angles = {values_list}
mother_names = {mother_names}

# Plot boxplot
plt.figure()
plt.boxplot(angles)
plt.xlabel('Mother name')
plt.ylabel('Deviation Angle (degrees)')
plt.title('{title}')
plt.xticks(ticks=range(1, len(mother_names) + 1), labels=mother_names, rotation=45, ha='right')
plt.tight_layout()
plt.ylim(0, 90)

plt.tight_layout()
plt.show()
""")


def projection_and_angle_to_plane(apical_dir, apical_orthogonal_dir, div_dir):
    """
    For every vectors apical_dir and apical_orthogonal_dir, get angle of div_dir to plane and its projection to the plane
    Parameters
    ----------
    apical_dir (np.array)
    apical_orthogonal_dir(np.array)
    div_dir(np.array)

    Returns
    -------
    projection_of_plane
    angle_to_plane
    """
    # Compute the normal vector to the plane defined by apical_dir and apical_orthogonal_dir
    plane_normal = np.cross(apical_dir, apical_orthogonal_dir)

    # Compute the angle between div_dir and the normal vector to the plane
    angle_to_normal = np.arccos(
        np.dot(div_dir, plane_normal) / (np.linalg.norm(div_dir) * np.linalg.norm(plane_normal)))

    # Compute the angle between div_dir and the plane
    angle_to_plane = np.abs(90 - np.degrees(angle_to_normal))
    # Compute the projection of div_dir onto the plane
    orthogonal_component = (np.dot(div_dir, plane_normal) / (np.linalg.norm(plane_normal) ** 2)) * plane_normal
    projection_on_plane = div_dir - orthogonal_component
    return projection_on_plane, angle_to_plane


def angle_btw_vectors(vector1, vector2, consider_direction=True):
    dot_product = np.dot(vector1, vector2)
    if consider_direction and dot_product < 0:
        vector2 = -vector2
        dot_product = np.dot(vector1, vector2)
    # Compute the norms (magnitudes) of the vectors
    norm_vector1 = np.linalg.norm(vector1)
    norm_vector2 = np.linalg.norm(vector2)
    # Compute the cosine of the angle between the vectors
    cosine_theta = dot_product / (norm_vector1 * norm_vector2)
    # Ensure that cosine_theta is within the valid range [-1, 1]
    cosine_theta = max(min(cosine_theta, 1), -1)
    # Compute the angle (in degree) using arccosine
    delta_theta = np.arccos(cosine_theta)
    delta_theta = np.degrees(delta_theta)
    return delta_theta

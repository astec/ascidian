import pickle as pkl
import os
import numpy as np
from collections import defaultdict

from ascidian.algorithms.division_analysis_init import get_temporal_alignment_coefficients_of_embryos
from ascidian.division_analysis.division_analysis_init_tools import get_embryo_at_t
import ascidian.core.temporal_alignment as utimes
from ascidian.core.diagnosis import _diagnosis_tissue_name_at_ncells
from ascidian.division_analysis.division_analysis_tools import determine_fate_group


#########################
######################

def nb_of_cell_per_dev_time_per_tissue(files, ref_filename, tissue_res, output_folder, verbose=True):

    list_dict_names = []
    list_dict_fates = []
    embryo_names = []
    embryos_props = {}
    embryo_idx = {}

    atimes_list = []
    times_list = []

    # todo if embryo resolution is halfs: split embryos

    # get embryos
    with open(ref_filename, 'rb') as f:
        ref_props_data = {}
        ref_name = ref_filename.split('-')[1]
        ref_props = pkl.load(f)
        ref_props_data[ref_name] = ref_props

        list_dict_names.append(ref_props['cell_name'])
        list_dict_fates.append(ref_props['cell_fate'])
        embryo_names.append(ref_name)
        i = len(embryo_idx)
        embryo_idx[ref_name] = i
        ###

    for filename in files:
        if filename == ref_filename:
            continue
        with open(filename, 'rb') as file:
            pm_props = pkl.load(file)
            embryo_name = filename.split('-')[1]

            embryos_props[embryo_name] = {
                'cell_name': pm_props['cell_name'],
                'cell_lineage': pm_props['cell_lineage'],
                'cell_fate': pm_props['cell_fate']
            }
            i = len(embryo_idx)
            embryo_idx[embryo_name] = i
            list_dict_names.append(pm_props['cell_name'])
            list_dict_fates.append(pm_props['cell_fate'])
            embryo_names.append(embryo_name)
            #

    if verbose:
        tissue_n_cells_combinations = {
            'Epid.': [8, 14, 26, 48, 96, 192],
            'End.': [12, 22],
            'Mesoderm': [20, 28, 32, 56],
            'NS': [12, 16, 22, 28, 42]
        }
        for ind_name, prop in embryos_props.items():
            for tissue, n_cells_list in tissue_n_cells_combinations.items():
                for n_cells in n_cells_list:
                    print('embryo', ind_name)
                    _diagnosis_tissue_name_at_ncells(prop, tissue, n_cells=n_cells, div=10000)
                    print('#####################################################')
                    print('#####################################################')

    # do temporal alignment
    temporal_alignment_coefficient_dict, _ = get_temporal_alignment_coefficients_of_embryos(embryos_props,
                                                                                            ref_props_data)
    print('nb of embryos in pop:', len(embryo_names))
    print('ref embryo', ref_name)
    #
    #  delete embryos_props
    del embryos_props
    del ref_props_data

    times_ref = utimes.get_embryo_times(ref_props['cell_lineage'])
    a, b = temporal_alignment_coefficient_dict[ref_name]
    a_times_ref = [a * t + b for t in times_ref]
    print('for ref:', a, b)

    for embryo, i in embryo_idx.items():
        times = utimes.get_embryo_times(list_dict_names[i])
        a, b = temporal_alignment_coefficient_dict[embryo]
        a_times = [a * t + b for t in times]
        atimes_list.append(a_times)
        times_list.append(times)
    ##
    ##
    #
    tissue_count_dict = {} # {'tissue': {'embryo_name': {'t': n}}}

    common_cells_per_timepoint = {}

    for t in a_times_ref:
        t = int(t)
        cells_in_common = None  # Initialize a set for the first embryo's cells
        all_cells = set()  # Track all cells across all embryos at the timepoint

        # Iterate over embryos to find common cells
        for i, embryo in enumerate(embryo_names):
            closest_at = min(atimes_list[i], key=lambda x: abs(x - t))
            taq = times_list[i][atimes_list[i].index(closest_at)]

            # Get the cell names at time 'taq' for the current embryo
            names_dict = get_embryo_at_t(list_dict_names[i], taq)  # {'cell_id': cell_name}

            # Skip if the time difference is larger than 2
            if np.abs(closest_at - t) >= 2:
                continue

            # Get the set of cell names for the current embryo
            current_cell_names = set(names_dict.values())
            all_cells.update(current_cell_names)

            # Initialize cells_in_common with the current embryo's cells,
            # or take the intersection with the current embryo
            if cells_in_common is None:
                cells_in_common = current_cell_names
            else:
                cells_in_common &= current_cell_names  # Intersect with the current embryo's cells

            # If no cells are in common, break early
            if not cells_in_common:
                break

        # Store common cells for this timepoint if any
        if cells_in_common:
            common_cells_per_timepoint[t] = cells_in_common

        cells_not_in_common = all_cells - cells_in_common if cells_in_common else all_cells

        all_non_common_cells_dict = {}

        # Initialize a dictionary to store the counts of cells for each fate across embryos
        for i, embryo in enumerate(embryo_names):
            closest_at = min(atimes_list[i], key=lambda x: abs(x - t))
            taq = times_list[i][atimes_list[i].index(closest_at)]
            fates_dict = get_embryo_at_t(list_dict_fates[i], taq)  # {'cell_id': fate}
            names_dict = get_embryo_at_t(list_dict_names[i], taq)

            # Skip if the time difference is larger than 2
            if np.abs(closest_at - t) >= 2:
                continue

            # Prepare a list of fates, unrolling lists where necessary
            fates = []
            for value in fates_dict.values():
                if isinstance(value, list):
                    fates.extend(value)  # Add each element in the list to the fates list
                else:
                    fates.append(value)

            # If tissue resolution is enabled, group fates into higher-level tissues (e.g., germ layers)
            if tissue_res:
                fates_grouped = []
                for fate in fates:
                    grouped_fates = determine_fate_group(fate, True)
                    if isinstance(grouped_fates, list):
                        fates_grouped.extend(grouped_fates)  # Handle multiple fate groups
                    else:
                        fates_grouped.append(grouped_fates)
                fates = list(set(fates_grouped))  # Remove duplicates across all fate groups

            # Iterate through each unique fate for the current embryo
            for fate in fates:
                # Get the count of cells for this fate group in this embryo
                if tissue_res:
                    fate_count = sum(1 for cell_fate in fates_dict.values()
                                     if any(fate == fg for fg in (determine_fate_group(cell_fate, True)
                                                                  if isinstance(determine_fate_group(cell_fate, True),
                                                                                list)
                                                                  else [determine_fate_group(cell_fate, True)])))
                else:
                    fate_count = sum(1 for cell_fate in fates_dict.values() if cell_fate == fate)

                # Map cell names back to their cell IDs and check fates
                common_cells_for_fate = [
                    names_dict[cell_id] for cell_id, cell_name in names_dict.items()
                    if cell_name in cells_in_common and any(
                        fate == fg for fg in (determine_fate_group(fates_dict.get(cell_id), True)
                                              if isinstance(determine_fate_group(fates_dict.get(cell_id), True), list)
                                              else [determine_fate_group(fates_dict.get(cell_id), True)]))
                ]

                not_common_cells_for_fate = [
                    names_dict[cell_id] for cell_id, cell_name in names_dict.items()
                    if cell_name not in cells_in_common and any(
                        fate == fg for fg in (determine_fate_group(fates_dict.get(cell_id), True)
                                              if isinstance(determine_fate_group(fates_dict.get(cell_id), True), list)
                                              else [determine_fate_group(fates_dict.get(cell_id), True)]))
                ]

                # Save non-common cells for the union calculation later
                if fate not in all_non_common_cells_dict:
                    all_non_common_cells_dict[fate] = {}
                if t not in all_non_common_cells_dict[fate]:
                    all_non_common_cells_dict[fate][t] = set()
                all_non_common_cells_dict[fate][t].update(not_common_cells_for_fate)
                all_non_common_cells_dict[fate][t].update(common_cells_for_fate)

                # Initialize the nested dict structure for `tissue_count_dict` if not already there
                if fate not in tissue_count_dict:
                    tissue_count_dict[fate] = {}
                if embryo not in tissue_count_dict[fate]:
                    tissue_count_dict[fate][embryo] = {}

                # Save the count of total cells for the current fate (tissue) at time `t`
                tissue_count_dict[fate][embryo][t] = {
                    'total_count': fate_count,
                    'common_count': len(set(common_cells_for_fate))
                }

                #if verbose:
                #    print('tissue',fate, 'embryo', embryo, 'nb of cells:', fate_count)
                #    print('cells in common',len(common_cells_for_fate), common_cells_for_fate)
                #    print('cells not in common', len(not_common_cells_for_fate), not_common_cells_for_fate)

        # Now compute the union of non-common cells for each fate and time point
        for fate in all_non_common_cells_dict:
            for time_point in all_non_common_cells_dict[fate]:
                #if verbose:
                #    print('tissue', fate, 'total nb of cells', len(all_non_common_cells_dict[fate][time_point]))
                #    print('cell names', all_non_common_cells_dict[fate][time_point])
                #    print('#############################################')
                all_cells_union = len(all_non_common_cells_dict[fate][time_point])
                for embryo in tissue_count_dict[fate]:
                    if time_point in tissue_count_dict[fate][embryo]:
                        tissue_count_dict[fate][embryo][time_point]['all_cells_count'] = all_cells_union

    tissue_cell_count_plot_script(tissue_count_dict, output_folder)


def tissue_cell_count_plot_script(tissue_count_dict, output_folder):
    """
    Writes a Python script that generates a plot of cell counts over time for each tissue in each embryo.
    The plot includes:
    - Total number of cells in the tissue for each embryo.
    - Number of common cells across embryos.
    - Number of non-common cells for each embryo.
    - Number of embryos available at each timepoint (on the secondary y-axis).

    Parameters
    ----------
    tissue_count_dict : dict
        Dictionary where each tissue contains a dictionary of embryos, and each embryo contains timepoints with
        'total_count' and 'common_count'.
    output_folder : str
        Directory to save the output script.

    Returns
    -------
    None
    """
    output_dir = os.path.join(output_folder, "tissue_plots")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    local_filename = os.path.join(output_dir, "plot_tissue_cell_counts.py")

    with open(local_filename, "w") as f:
        # Write imports
        f.write("import numpy as np\n")
        f.write("import matplotlib.pyplot as plt\n\n")

        # Write the tissue_count_dict
        f.write(f"tissue_count_dict = {tissue_count_dict}\n\n")

        # Start generating plots for each tissue
        f.write("# Generate plots for each tissue\n")
        f.write("for tissue, embryo_data in tissue_count_dict.items():\n")
        f.write("    fig, ax1 = plt.subplots(figsize=(10, 6))\n")
        f.write("    plt.title(f'Cell Count Over Time for Tissue: {tissue}')\n")
        f.write("    ax1.set_xlabel('Developmental Time')\n")
        f.write("    ax1.set_ylabel('Cell Count')\n")

        # Prepare to store the common cell counts across all embryos
        f.write("    common_cells = []\n")
        f.write("    timepoints_common = []\n")

        # Prepare to store the number of embryos available at each timepoint
        f.write("    embryo_count_per_time = {}\n")

        # Plot for each embryo in the current tissue
        f.write("    for embryo, time_data in embryo_data.items():\n")

        # Extract total, common, non-common, and all_cells counts over time
        f.write("        timepoints = sorted(time_data.keys())\n")
        f.write("        total_counts = [time_data[t]['total_count'] for t in timepoints]\n")
        f.write("        common_counts = [time_data[t]['common_count'] for t in timepoints]\n")
        f.write("        non_common_counts = [time_data[t]['total_count'] - time_data[t]['common_count'] for t in timepoints]\n")
        f.write("        all_cells_counts = [time_data[t].get('all_cells_count', 0) for t in timepoints]\n")

        # Add common cells to the list (only done for one embryo to avoid repetition)
        f.write("        if not common_cells:\n")
        f.write("            common_cells = common_counts\n")
        f.write("            timepoints_common = timepoints\n")

        # Update the number of embryos available at each timepoint
        f.write("        for t in timepoints:\n")
        f.write("            if t not in embryo_count_per_time:\n")
        f.write("                embryo_count_per_time[t] = 0\n")
        f.write("            embryo_count_per_time[t] += 1\n")

        # Plot total number of cells (line plot with filled o markers)
        f.write("        ax1.plot(timepoints, total_counts, marker='o', linestyle='-', label=f'Total {embryo}')\n")

        # Plot non-common cells (line plot with unfilled o markers)
        f.write("        ax1.plot(timepoints, non_common_counts, marker='o', linestyle='--', markerfacecolor='none', label=f'Non-common {embryo}')\n")

        # Plot all cells count (line plot with + markers)
        f.write("    ax1.plot(timepoints, all_cells_counts, marker='+', linestyle=':', label=f'All Cells', alpha=0.7)\n")

        # Plot common cells once (for all embryos, using x markers)
        f.write("    ax1.plot(timepoints_common, common_cells, marker='x', linestyle='-', color='black', label='Common Cells')\n")

        # Add secondary y-axis for the number of embryos
        f.write("    ax2 = ax1.twinx()\n")
        f.write("    ax2.set_ylabel('Number of Embryos')\n")
        f.write("    timepoints = sorted(embryo_count_per_time.keys())\n")
        f.write("    embryo_counts = [embryo_count_per_time[t] for t in timepoints]\n")
        f.write("    ax2.plot(timepoints, embryo_counts, color='red', linestyle='-', marker='s', label='Embryo Count', alpha=0.7)\n")

        # Add legends and adjust layout
        f.write("    ax1.legend(loc='upper left', title='Cell Counts')\n")
        f.write("    ax2.legend(loc='upper right', title='Embryo Count')\n")
        f.write("    fig.tight_layout()\n")

        # Save the plot to a file
        f.write("    plt.savefig(f'tissue_cell_count_plot_{tissue}.png', dpi=300)\n")
        f.write("    plt.show()\n")
        f.write("    plt.close()\n")

    print(f"Plot script written to: {local_filename}")

import numpy as np
import os
import pickle as pkl

###############
import numpy as np
import os
import pickle as pkl

from ascidian.division_analysis.cell_apical_surface_analysis import (compute_projection_to_plane_and_angles_to_plane,
                                                        compute_angle_btw_dir_and_vector, compute_avg_division_time,
                                                        angle_vector_direction, projection_and_angle_to_plane)


##########
# Todo compare division direction to pca during interphase


def plot_division_direction_deviation_to_longest_axis_vs_pca_ratio(divisions_properties, mother, output_folder):
    """
    for a mother, the division direction deviation to longest axis, expected to be higher for higher ratio of PCA
    values
    ----------
    divisions_properties (dict): A dictionary containing division properties.
                                   Example: {
                                    'mother_name': {
                                    'division_directions_after_symmetry':
                                        {'left': {'individual name': division direction vector},
                                        'right': {'individual name': division direction vector} }
                                                    },
                                    'gen': int ,
                                    'cell_principal_values' : {'direction':{'individual': 3 np.array values } },
                                    'cell_principal_vectors' : {'direction':{'individual': 3 np.array vectors  } }

                                        }
    mother (str): cell considered
    output_folder (str): Path to the output Python script.

    Returns
    -------

    """
    individuals_names = []
    deviation_values = []
    pca_ratios = []

    mother_division_props = divisions_properties[mother]
    division_directions_props = mother_division_props.get('cell_principal_vectors', {})

    output_folder = os.path.join(output_folder, "cell_shape_analysis", "deviation_to_pca_ratio")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for lr, direction in division_directions_props.items():
        for ind, cell_principal_vectors in direction.items():
            # get direction of div, apical longest axis as PCA 2nd and third components
            apical_direction1 = cell_principal_vectors[0]
            apical_direction2 = cell_principal_vectors[1]
            division_directions = list(mother_division_props['division_directions_after_symmetry'][lr][ind])
            apical_coordinates = []
            individual = ind + '_' + lr
            pca_ratio = mother_division_props.get('cell_principal_values', {})[lr][ind][1] / \
                        mother_division_props.get('cell_principal_values', {})[lr][ind][0]

            # compute deviation in plane
            # projection, angles_to_plane = compute_projection_to_plane_and_angles_to_plane(apical_direction1,
            #                                                                               apical_direction2,
            #                                                                               division_directions)
            # deviation_value = compute_angle_btw_dir_and_vector(apical_direction1, projection)
            projection_on_plane, theta_angle = projection_and_angle_to_plane(apical_direction1,
                                                                             apical_direction2,
                                                                             division_directions)
            phi_angle = angle_vector_direction(apical_direction1, projection_on_plane)

            individuals_names.append(individual)
            deviation_values.append(phi_angle)
            pca_ratios.append(pca_ratio)

    # script write fct
    output_file = os.path.join(output_folder, f'{mother}.py')
    write_div_dir_longest_axis_deviation_vs_pca_ratio_script(mother, deviation_values, pca_ratios,
                                                             individuals_names, output_file)


def write_div_dir_longest_axis_deviation_vs_pca_ratio_script(mother, deviation_values, pca_ratios, individuals_names,
                                                             output_file):
    """
    write scatter plot script
        deviation_values on y axis
        pca_ratios on x axis
        individuals_names as text on scatter positions
    Parameters
    ----------
    mother
    deviation_values : list of float
    pca_ratios : list of float
    individuals_names : list of str
        Name of the individuals.
    output_file : str
        Path to the output Python script.

    Returns
    -------

    """
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt

pca_ratios = {pca_ratios}
deviation_values = {deviation_values}
individuals_names = {individuals_names}

# Deviation values vs PCA ratios scatter plot
plt.figure()
plt.scatter(pca_ratios, deviation_values, c='b', marker='o', label='Deviation vs PCA Ratio')

# Add individual names as text on scatter positions
for i, name in enumerate(individuals_names):
    plt.text(pca_ratios[i], deviation_values[i], name, fontsize=9)

plt.xlabel('Cell PCA Ratio')
plt.ylabel('Deviation Values')
plt.title('Deviation Values vs PCA Ratios for Mother {mother}')
plt.grid(True)
plt.xlim(0,1)
plt.ylim(0,90)
plt.tight_layout()
plt.show()
""")


def plot_cell_pca_directions(divisions_properties, mother, output_folder):
    """
    Given cell name, creates a folder that has for each individual a folder, that contains a py file The latter plots
    cell apical pts, approximated division direction and apical surface approximated the longest direction Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
                                   Example: {
                                    'mother_name': {
                                    'division_directions_after_symmetry':
                                        {'left': {'individual name': division direction vector},
                                        'right': {'individual name': division direction vector} }
                                                    },
                                    'gen': int ,
                                    'cell_principal_values' : {'direction':{'individual': 3 np.array values } },
                                    'cell_principal_vectors' : {'direction':{'individual': 3 np.array vectors  } }

                                        }
    mother (str): cell considered
    output_folder (str): Path to the output Python script.

    Returns
    -------

    """
    mother_division_props = divisions_properties[mother]
    division_directions_props = mother_division_props.get('cell_principal_vectors', {})

    output_folder = os.path.join(output_folder, "cell_shape_analysis", "plot_vectors", f"{mother}")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for lr, direction in division_directions_props.items():
        for ind, cell_principal_vectors in direction.items():
            # get direction of div, apical longest axis as PCA 2nd and third components
            apical_direction1 = cell_principal_vectors[0]
            apical_direction2 = cell_principal_vectors[1]
            division_directions = list(mother_division_props['division_directions_after_symmetry'][lr][ind])
            apical_coordinates = []
            individual = ind + '_' + lr
            pca_ratio = mother_division_props.get('cell_principal_values', {})[lr][ind][1] / \
                        mother_division_props.get('cell_principal_values', {})[lr][ind][0]

            # script write fct
            output_file = os.path.join(output_folder, f'{individual}.py')
            write_cell_pca_plot_script(mother, apical_direction1, apical_direction2, division_directions,
                                       individual, pca_ratio, output_file)


def write_cell_pca_plot_script(mother, apical_direction1, apical_direction2, division_direction,
                               individual, pca_ratio, output_file):
    """
    Writes a Python script to plot the apical 3D coordinates along with the division direction and the two apical longest directions.

    Parameters
    ----------
    apical_coords : array_like
        List of 3D coordinates of apical points.
    cell PCA2 : array_like
        First apical longest direction vector.
    cell PCA3 : array_like
        Second apical longest direction vector.
    division_direction : array_like
        Division direction vector.
    individual : str
        Name of the individual.
    output_file : str
        Path to the output Python script.

    Returns
    -------
    None
    """
    with open(output_file, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

# Apical direction vectors
apical_direction1 = np.array({list(apical_direction1)})
apical_direction2 = np.array({list(apical_direction2)})

# Division direction vector
division_direction = np.array({list(division_direction)}) 

# Compute plane defined by the two apical vectors
plane_normal = np.cross(apical_direction1, apical_direction2)
xx, yy = np.meshgrid(np.linspace(-1, 1, 100), 
                     np.linspace(-1, 1, 100))

# Calculate z coordinates of the plane
centroid = np.array([0, 0, 0])
zz = (-plane_normal[0]*xx - plane_normal[1]*yy + centroid[2]*plane_normal[2])/plane_normal[2]

# Create 3D plot
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Plot division direction
ax.quiver(centroid[0], centroid[1], centroid[2], division_direction[0], division_direction[1], division_direction[2], color='r', label='Division Direction')

# Plot apical longest directions
ax.quiver(centroid[0], centroid[1], centroid[2], apical_direction1[0], apical_direction1[1], apical_direction1[2], color='g', label='Apical Longest Direction')
ax.quiver(centroid[0], centroid[1], centroid[2], apical_direction2[0], apical_direction2[1], apical_direction2[2], color='orange', label='Apical Second Longest Direction')

# Plot the plane
ax.plot_surface(xx, yy, zz, alpha=0.5, color='gray')

ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
ax.set_title('Cell longest direction vs Diviison direction,PCAs ratio={pca_ratio}, and division direction for "{mother}" in ' + "{individual}")
ax.legend()

plt.show()
""")


def plot_boxplot_deviation_div_direction_principal_directions(divisions_properties, time_alignment_coefficients,
                                                              generation, output_folder):
    """
    Aim is to compare cell longest axis to the approximated division direction ( Hertwig rule)
    a Plots boxplot of deviation between division direction angle to plane of PCA 0 & 1
    b Plots boxplot of deviation between division direction angle to PCA 2 in plane of PCA 0 & 1
    Parameters
    ----------
    divisions_properties (dict): A dictionary containing division properties.
                                   Example: {
                                    'mother_name': {
                                    'division_directions_after_symmetry':
                                        {'left': {'individual name': direction vector},
                                        'right': {'individual name': direction vector} }
                                                    },
                                    'gen': int,
                                    'cell_principal_values' : {'direction':{'individual': 3 np.array values } },
                                    'cell_principal_vectors' : {'direction':{'individual': 3 np.array vectors  } }
                                        } }
    gen (list): list of divisions generations to be considered
    output_folder(str): Path to the output Python script.

    Returns
    -------

    """
    # order cells by avg division time
    # todo add fate as color of boxplot
    # the prediction as longest axis instead
    # todo also make at t-12 : add parameter

    # Create a figure for each generation
    for gen in set(generation):
        # print('creating figure (cell division time boxplot) for generation:', gen)
        # Filter divisions properties for the current generation
        gen_divisions = {mother_name: properties for mother_name, properties in divisions_properties.items() if
                         properties.get('gen') == gen}

        mother_names = []
        apical_dir = []
        apical_orthogonal_dir = []
        div_dir = []
        mothers_avg_div_time = []
        for mother_name, properties in gen_divisions.items():
            for lr, ind_prop in properties['cell_principal_vectors'].items():
                for embryo_name, cell_principal_vectors in ind_prop.items():
                    if embryo_name in properties['division_directions_after_symmetry'][lr]:
                        avg_div_time = compute_avg_division_time(properties.get('division_aq_time', {}),
                                                                 time_alignment_coefficients)
                        mothers_avg_div_time.append(avg_div_time)

                        # get apical direction
                        apical_dir.append(cell_principal_vectors[0])
                        apical_orthogonal_dir.append(cell_principal_vectors[1])
                        # get division direction
                        div_dir.append(properties['division_directions_after_symmetry'][lr][embryo_name])
                        # get mother name
                        mother_names.append(mother_name)

        # In output_path writes a file that plots the boxplot of all cells for the current generation
        write_boxplot_pca_components_direction_deviation_script(gen, mother_names, apical_dir, apical_orthogonal_dir,
                                                                mothers_avg_div_time,
                                                                div_dir,
                                                                output_folder)


def write_boxplot_pca_components_direction_deviation_script(gen, mother_names, apical_dir, apical_orthogonal_dir,
                                                            mothers_avg_div_time,
                                                            div_dir,
                                                            output_folder):
    """
    Writes a Python script to generate a boxplot for each mother_name.
    The values of each boxplot are the angles between apical_dir and div_dir

    Parameters:
    gen (int): Generation number.
    mother_names (list): List of mother cell names.
    adjusted_times_per_mother (list): List of lists containing adjusted acquisition times for each mother.
    output_file (str): Path to the output Python script.

    Returns
    -------
    None
    """
    # Construct the output file path
    output_folder = output_folder + '/cell_shape_analysis'
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    output_file_deviation_from_plane = os.path.join(output_folder, f'boxplot_angle_to_apical_plane_generation_{gen}.py')
    output_file_deviation_in_plane = os.path.join(output_folder, f'boxplot_angle_in_apical_plane_generation_{gen}.py')

    # compute angle of div_dir to plan defined by: apical_dir, apical_orthogonal_dir
    projections, angles_to_plane = compute_projection_to_plane_and_angles_to_plane(apical_dir, apical_orthogonal_dir,
                                                                                   div_dir)

    # compute of projected orientation to apical direction
    angles_in_plane = compute_angle_btw_dir_and_vector(apical_dir, projections)

    # adjust plot : two boxplots
    # Write the Python script
    with open(output_file_deviation_in_plane, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
import numpy as np

mothers = {mother_names} 
angles_grouped_per_mother = []                         
angles = {angles_in_plane}
avg_div_time = {mothers_avg_div_time}

mothers_ = []
unique_mothers = list(set(mothers))
unique_mothers = sorted(unique_mothers, key=lambda mother: avg_div_time[mothers.index(mother)])

for mother in unique_mothers:
    # Get indexes of angles corresponding to the current mother name
    indexes = [i for i, m in enumerate(mothers) if m == mother] 
    # Get corresponding angles
    mother_angles = [angles[i] for i in indexes]
    angles_grouped_per_mother.append(mother_angles)
    mothers_.append(mother)

# Plot boxplot
plt.figure()
plt.boxplot(angles_grouped_per_mother)
plt.xlabel('Mother Name')
plt.ylabel('Deviation Angle (degrees)')
plt.title(f'Boxplot of Deviation Angle between Cell Longest Axis and Approximated Division, in Largest Surface, at division time, Directions for Generation {gen}')
plt.xticks(range(1, len(mothers_) + 1), mothers_, rotation=45, ha='right')
plt.ylim(0,90)
plt.tight_layout()
plt.show()
""")
    with open(output_file_deviation_from_plane, 'w') as script_file:
        script_file.write(f"""
import matplotlib.pyplot as plt
import numpy as np

mothers = {mother_names} 
angles_grouped_per_mother = []                         
angles = {angles_to_plane}
avg_div_time = {mothers_avg_div_time}

mothers_ = []
unique_mothers = list(set(mothers))
unique_mothers = sorted(unique_mothers, key=lambda mother: avg_div_time[mothers.index(mother)])

mothers_ = []

for mother in unique_mothers:
    # Get indexes of angles corresponding to the current mother name
    indexes = [i for i, m in enumerate(mothers) if m == mother] 
    # Get corresponding angles
    mother_angles = [angles[i] for i in indexes]
    angles_grouped_per_mother.append(mother_angles)
    mothers_.append(mother)

# Plot boxplot
plt.figure()
plt.boxplot(angles_grouped_per_mother)
plt.xlabel('Mother Name')
plt.ylabel('Deviation Angle (degrees)')
plt.title(f'Boxplot of Deviation Angle between PCA[1,2] Surface Plane and Approximated Division Directions for Generation {gen}')
plt.xticks(range(1, len(mothers_) + 1), mothers_, rotation=45, ha='right')
plt.ylim(0,90)
plt.tight_layout()
plt.show()
""")

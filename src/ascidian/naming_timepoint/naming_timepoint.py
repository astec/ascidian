##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Lun 19 jui 2023 18:54:17 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys
import copy

import astec.utils.common as common
import astec.utils.datadir as datadir
import astec.utils.ioproperties as ioproperties

import ascidian.ascidian.fate as afate
import ascidian.core.icp as icp
import ascidian.core.cell_signature as ccellsign
import ascidian.core.naming_utils as cnamutils
import ascidian.components.embryo as cembryo
import ascidian.components.embryoset as cembryoset
import ascidian.figures.generate as fgenerate

import ascidian.naming_timepoint.parameters as ntparameters
import ascidian.naming_timepoint.naming_by_registration as ntregistration
import ascidian.naming_timepoint.naming_by_assignment as ntassignment
import ascidian.naming_timepoint.naming_by_graph as ntgraph
import ascidian.naming_timepoint.utils as ntutils


#
#
#
#
#

monitoring = common.Monitoring()

_instrumented_ = False


########################################################################################
#
#
#
########################################################################################


def _get_embryo_input_file(f, var=None):
    #
    # read input embryo
    # file name can be a string or a string within a list
    #
    proc = "_get_embryo_input_file"
    if f is None:
        return None
    if isinstance(f, str):
        return f
    elif isinstance(f, list):
        if len(f) == 1:
            if isinstance(f[0], str):
                return f[0]
            else:
                msg = "first element of variable"
                if var is not None:
                    msg += " '" + str(var) + "'"
                msg += " is not a string"
                monitoring.to_log_and_console(proc + ": " + msg)
                return None
        else:
            msg = "variable"
            if var is not None:
                msg += " '" + str(var) + "'"
            msg += " has more than one element"
            monitoring.to_log_and_console(proc + ": " + msg)
            return None
    msg = "unhandled type (" + str(type(f)) + ") for variable"
    if var is not None:
        msg += " '" + str(var) + "'"
    monitoring.to_log_and_console(proc + ": " + msg)
    return None


########################################################################################
#
#
#
########################################################################################

def naming_timepoint_process(experiment, parameters):
    proc = "naming_timepoint_process"

    #
    # parameter type checking
    #

    if not isinstance(experiment, datadir.Experiment):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'experiment' variable: "
                                      + str(type(experiment)))
        sys.exit(1)

    if not isinstance(parameters, ntparameters.NamingTimepointParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    time_digits_for_cell_id = experiment.get_time_digits_for_cell_id()

    #
    # parameter is required here to set the reference atlas (if given)
    #   time warping is done wrt to this reference atlas
    #   properties are copied into each atlas (without normalisation)
    #   volume normalisation is also computed
    #
    cembryoset.monitoring.copy(monitoring)
    atlases = cembryoset.EmbryoSet(parameters=parameters)
    atlases.add_embryos(parameters.atlasEmbryos, parameters,
                        time_digits_for_cell_id=experiment.get_time_digits_for_cell_id())

    if atlases.n_embryos() == 0:
        monitoring.to_log_and_console(str(proc) + ": no atlases have been read?!")
        sys.exit(1)

    #
    # add symmetrical atlases if required
    #
    if parameters.add_symmetric_embryo is True:
        atlases.add_symmetrical_embryos()

    #
    # figure case: either compare atlases on a left-one-out basis,
    # or compare atlases against test_embryos
    #
    if parameters.generate_figure is True or isinstance(parameters.generate_figure, str) is True or \
            isinstance(parameters.generate_figure, list) is True:
        test_embryos = None
        if len(parameters.testEmbryos) > 0:
            test_embryos = cembryoset.EmbryoSet(parameters=parameters)
            test_embryos.add_embryos(parameters.testEmbryos, parameters,
                                     time_digits_for_cell_id=experiment.get_time_digits_for_cell_id())
        fgenerate.monitoring.copy(monitoring)
        fgenerate.generate_figure(atlases, parameters, test_embryos=test_embryos)
        return None

    #
    # this is not the figure case: one single embryo has to be named
    # read input properties to be named
    #
    inputfile = None
    prop = {}

    #
    # test case: read a property file with names
    # copy 'cell_name' into 'ground_truth_cell_name'
    #
    testfile = _get_embryo_input_file(parameters.testFile)
    if testfile is not None:
        prop = ioproperties.read_dictionary(testfile, inputpropertiesdict={}, verbose=(monitoring.debug > 0))
        if 'cell_name' not in prop:
            monitoring.to_log_and_console(str(proc) + ": 'cell_name' is not in '" + str(testfile) + "'")
            sys.exit(1)
        inputfile = testfile
        prop['ground_truth_cell_name'] = copy.deepcopy(prop['cell_name'])
        del prop['cell_name']

    #
    # normal case
    #
    elif parameters.inputFile is not None:
        inputfile = _get_embryo_input_file(parameters.inputFile)
        if inputfile is not None:
            prop = ioproperties.read_dictionary(parameters.inputFile, inputpropertiesdict={},
                                                verbose=(monitoring.debug > 0))

    if inputfile is None:
        monitoring.to_log_and_console(str(proc) + ": no input nor test file?!")
        return prop

    if prop == {}:
        monitoring.to_log_and_console(str(proc) + ": no properties?!")
        sys.exit(1)

    #
    # processing
    #
    # build an atlas from the embryo to be named,
    # temporally align it with the reference embryo
    #
    name = inputfile.split(os.path.sep)[-1]
    if name.endswith(".xml") or name.endswith(".pkl"):
        name = name[:-4]
    embryo = cembryo.Embryo(prop, name=name, time_digits_for_cell_id=time_digits_for_cell_id, add_all_properties=True)

    #
    # temporally align the test/read embryo with the reference embryo
    # althouth it may not be necessary
    #
    # get the time that will be used for the embryo, as well as the cell number
    #
    embryo.temporally_align_with(atlases.get_reference_embryo())
    msg = "   ... "
    msg += "linear time warping of '" + str(name) + "' wrt '" + str(atlases.get_reference_embryo_name()) + "' is "
    msg += "({:.3f}, {:.3f})".format(embryo.temporal_alignment[0], embryo.temporal_alignment[1])
    monitoring.to_log_and_console(msg, 1)
    flo_cells, flo_time, flo_interval = ntutils.get_time_from_ncells(embryo, parameters.cell_number, name=embryo.name,
                                                                     change_cell_number=True)

    msg = "   ... embryo to be named '" + str(name) + "': "
    msg += "chosen time is " + str(flo_time) + " out of " + str(flo_interval) + "\n"
    msg += "                    cell count is " + str(flo_cells)
    monitoring.to_log_and_console(msg)

    #
    # precomputed stuff (if any)
    # - transformations (parameters.naming_by_transfer == 'registration')
    # - cell similarities ((parameters.naming_by_transfer == 'assignment')
    #
    precomputed_filename = None
    if parameters.naming_by_transfer.lower() == 'registration':
        precomputed_filename = parameters.transformation_filename
    elif parameters.naming_by_transfer.lower() == 'assignment':
        precomputed_filename = parameters.cell_similarity_filename
    elif parameters.naming_by_transfer.lower() == 'graph':
        pass
    else:
        pass

    precomputeddata = ntutils.get_precomputeddata(parameters, precomputed_filename)
    precomputed_data_has_to_saved = False

    #
    # get candidate names
    # names = dictionary indexed by cell id, value = list of cell names
    # time_embryo = integer, time where the computation takes place
    # time_atlases = dictionary indexed by atlas name, value = time where the computation takes place
    #
    ntregistration.monitoring.copy(monitoring)
    ntassignment.monitoring.copy(monitoring)
    ntutils.monitoring.copy(monitoring)

    all_atlases = atlases.get_embryos()
    candidate_name = {}
    time_atlases = {}


    for a in all_atlases:
        monitoring.to_log_and_console("... naming with respect to atlas '" + str(a) + "'")

        if (embryo.name == a) or (a[:4] == 'sym-' and embryo.name == a[4:]) \
                or (embryo.name[:4] == 'sym-' and embryo.name[4:] == a):
            msg = "will not compare '" + str(embryo.name) + "' with '" + str(a) + "'. Skip it."
            monitoring.to_log_and_console("   ... " + msg)
            continue

        ref_cells, ref_time, ref_interval = ntutils.get_time_from_ncells(all_atlases[a], flo_cells,
                                                                         name=all_atlases[a].name,
                                                                         change_cell_number=False)
        if ref_cells is None:
            msg = "requested cell count of " + str(flo_cells) + " is not in the found "
            msg += "cell range of atlas '" + str(a) + "'.\n"
            msg += "\t skip atlas '" + str(a) + "'"
            monitoring.to_log_and_console("   ... " + msg)
            continue

        time_atlases[a] = ref_time
        msg = "   ... atlas embryo '" + str(a) + "': "
        msg += "chosen time is " + str(ref_time) + " out of " + str(ref_interval) + "\n"
        msg += "                    cell count is " + str(ref_cells)
        monitoring.to_log_and_console(msg)

        data_has_to_saved = False

        if parameters.naming_by_transfer.lower() == 'registration':
            output = ntregistration.naming_by_registration(embryo, flo_time, all_atlases[a], ref_time, parameters,
                                                           precomputeddata=precomputeddata, verbose=True)
            res_name, precomputeddata, data_has_to_saved = output
        elif parameters.naming_by_transfer.lower() == 'assignment':
            output = ntassignment.naming_by_assignment(embryo, flo_time, all_atlases[a], ref_time, parameters,
                                                       precomputeddata=precomputeddata, verbose=True)
            res_name, precomputeddata, data_has_to_saved = output

        elif parameters.naming_by_transfer.lower() == 'graph':
            res_name = ntgraph.naming_by_graph(embryo, flo_time, all_atlases[a], ref_time, parameters, verbose=True)
        else:
            msg = "naming by transfer method '" + str(parameters.naming_by_transfer) + "' not handled yet"
            monitoring.to_log_and_console(str(proc) + ": " + msg)
            monitoring.to_log_and_console("Exiting")
            sys.exit(1)

        if res_name is None:
            monitoring.to_log_and_console("... naming with respect to atlas '" + str(a) + "' failed")
            continue

        if data_has_to_saved is True:
            precomputed_data_has_to_saved = True

        #
        # collect candidates into a single list
        #
        for c in res_name:
            candidate_name[c] = candidate_name.get(c, []) + [res_name[c]]

    #
    # save computed similarities if required
    #
    if precomputed_data_has_to_saved:
        ntutils.save_precomputeddata(precomputeddata, precomputed_filename)

    #
    # set names
    #
    monitoring.to_log_and_console("... set names by consensus")
    names = ntutils.set_names_from_candidates(candidate_name, parameters.minimal_consensus, verbose=True)

    #
    # eventually remove some names
    #
    if parameters.check_duplicate:
        monitoring.to_log_and_console("... removing duplicates")
        names = ntutils.remove_duplicated_names(names, time_digits_for_cell_id=embryo.time_digits_for_cell_id)

    #
    # set names
    #
    prop['cell_name'] = names

    #
    #
    #
    monitoring.to_log_and_console("... generate assessment in property file")
    prop = ntutils.evaluate_initial_naming(prop, embryo, atlases, flo_time, time_atlases)

    #
    # set fate
    #
    prop = afate.set_fate_from_names(prop)
    prop = afate.set_color_from_fate(prop)

    #
    # is there a test to be conducted
    #
    if parameters.testFile is not None:
        monitoring.to_log_and_console("... assessment wrt ground truth")
        ntutils.assess_naming(prop['cell_name'], prop['ground_truth_cell_name'],
                              time_digits_for_cell_id=time_digits_for_cell_id)
        # right_naming, wrong_naming, unnamed_cells, not_in_reference = output
        # used for figures

    if isinstance(parameters.outputFile, str):
        ioproperties.write_dictionary(parameters.outputFile, prop)

    if 'ground_truth_cell_name' in prop:
        del prop['ground_truth_cell_name']
    return prop

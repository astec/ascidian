##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Lun 19 jui 2023 18:54:17 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

import math
import multiprocessing
import statistics
import pickle as pkl

import numpy as np

from sklearn.neighbors import NearestNeighbors

import astec.utils.common as common

import ascidian.core.icp as icp
import ascidian.core.vector_distribution as vdist
import ascidian.components.embryo as cembryo
import ascidian.components.embryoset as cembryoset
import ascidian.naming_timepoint.parameters as ntparameters

monitoring = common.Monitoring()


########################################################################################
#
#
#
########################################################################################

def get_transformation_filename(parameters):
    if parameters.transformation_filename is None:
        return None
    if isinstance(parameters.transformation_filename, str):
        if parameters.transformation_filename.endswith(".pkl") is True:
            return parameters.transformation_filename
        return parameters.transformation_filename + '.pkl'
    return None


def monitoring_transformation(d, desc=None):
    if monitoring.debug <= 0:
        return
    if desc is not None:
        print("   ... " + str(desc))
    for f in d:
        for tf in d[f]:
            for r in d[f][tf]:
                for tr in d[f][tf][r]:
                    msg = "       " + "'" + str(f) + "' at time #{:2d}".format(tf)
                    msg += " towards '" + str(r) + "' at time #{:2d}".format(tr)
                    monitoring.to_log_and_console("       " + msg, 1)


########################################################################################
#
#
#
########################################################################################

def _get_barycenters(embryo, t, transformation=None):
    barycenters = embryo.cell_barycenter
    div = 10 ** embryo.time_digits_for_cell_id
    lc = [c for c in barycenters if (int(c) // div == t) and int(c) % div != 1]
    b = np.zeros((3, len(lc)))
    if transformation is None:
        for i, c in enumerate(lc):
            b[:, i] = barycenters[c]
    else:
        v = np.ones(4)
        for i, c in enumerate(lc):
            v[:3] = barycenters[c]
            b[:, i] = (np.matmul(transformation, v))[:3]
    return b


########################################################################################
#
# transformations
#
########################################################################################


def _coregister_embryos_one_direction(parameters):
    #
    # transformation index
    # transformation number
    # symmetry vector for floating embryo
    # symmetry vector for reference embryo
    # floating embryo
    # reference embryo
    # registration parameters
    # verboseness
    #
    i_trsf, n_trsf, n, vz, flo_embryo, ref_embryo, registration_par, verbose = parameters

    flo_embryo_bar = flo_embryo[0]
    flo_bars = flo_embryo[1]
    flo_scale = flo_embryo[2]

    ref_embryo_bar = ref_embryo[0]
    ref_bars = ref_embryo[1]
    ref_scale = ref_embryo[2]
    inv_ref_scale = np.linalg.inv(ref_scale)

    if verbose:
        msg = "processing registration[{:3d}/{:3d}] = ".format(i_trsf, n_trsf)
        msg += "({:5.2f} {:5.2f} {:5.2f})".format(n[0], n[1], n[2]) + " vs "
        msg += "({:5.2f} {:5.2f} {:5.2f})".format(vz[0], vz[1], vz[2])
        monitoring.to_log_and_console("      ... " + msg)

    # vz normalisation
    vz /= np.linalg.norm(vz)

    #
    rotz = np.identity(4)
    rotxy = np.identity(4)
    sp = np.dot(n, vz)

    #
    # rotation that put n along vz
    #
    if sp > 0.999:
        # n is aligned with vz
        # do nothing
        pass
    elif sp < -0.999:
        # n is aligned with -vz
        # rotation of pi wrt x x vz (if x has the smallest coordinate)
        if abs(vz[0]) <= abs(vz[1]) and abs(vz[0]) <= abs(vz[2]):
            vrot = np.cross(vz, np.array([1, 0, 0]))
        elif abs(vz[1]) <= abs(vz[0]) and abs(vz[1]) <= abs(vz[2]):
            vrot = np.cross(vz, np.array([0, 1, 0]))
        else:
            vrot = np.cross(vz, np.array([0, 0, 1]))
        vrot *= np.pi / np.linalg.norm(vrot)
        rotz[:3, :3] = icp.rotation_matrix_from_rotation_vector(vrot)
    else:
        # rotation of math.acos(n.z) wrt vector n x z
        # that puts n along z
        vrot = np.cross(n, vz)
        vrot *= math.acos(sp) / np.linalg.norm(vrot)
        rotz[:3, :3] = icp.rotation_matrix_from_rotation_vector(vrot)

    # for 64 cells, the smallest angle (from the barycenter) between 2 adjacent cells
    # is 7 degrees
    rigid_matrice = []
    affine_matrice = []
    average_residual = []
    angles = np.linspace(0, 2 * np.pi, int(360 / registration_par.z_rotation_angle_increment), endpoint=False)
    for a in angles:
        # compute initial transformation
        # scaling matrices can be anistropic
        rotxy[:3, :3] = icp.rotation_matrix_from_rotation_vector(a * vz)
        init_mat = np.matmul(inv_ref_scale, np.matmul(rotxy, np.matmul(rotz, flo_scale)))
        trs = ref_embryo_bar - np.matmul(init_mat, flo_embryo_bar)
        init_mat[:3, 3] = trs[:3]

        flo = np.zeros((3, flo_bars.shape[1]))
        v = np.ones(4)

        # apply initial transformation
        for j in range(flo_bars.shape[1]):
            v[:3] = flo_bars[:, j]
            flo[:, j] = (np.matmul(init_mat, v))[:3]

        # compute a rigid transformation
        rigid_mat = icp.icp(ref=ref_bars, flo=flo, transformation_type="rigid")
        res_rigid_mat = np.matmul(rigid_mat, init_mat)

        #
        # rigid transformations is used as initialization for affine transformation computation
        #
        init_mat = res_rigid_mat

        # apply initial transformation
        for j in range(flo_bars.shape[1]):
            v[:3] = flo_bars[:, j]
            flo[:, j] = (np.matmul(init_mat, v))[:3]

        # compute an affine transformation
        affine_mat = icp.icp(ref=ref_bars, flo=flo, transformation_type="affine")
        res_affine_mat = np.matmul(affine_mat, init_mat)

        # compute residuals
        v = np.ones(4)
        for j in range(flo_bars.shape[1]):
            v[:3] = flo_bars[:, j]
            flo[:, j] = (np.matmul(res_affine_mat, v))[:3]
        nbrs = NearestNeighbors(n_neighbors=1, algorithm='kd_tree').fit(ref_bars.T)
        distances, indices = nbrs.kneighbors(flo.T)
        distances = distances.flatten()
        sort_index = np.argsort(distances)
        # 0.8 is the value used by Gael
        retained_points = int(registration_par.pair_ratio_for_residual * len(distances))
        lr = 0.0
        for j in range(retained_points):
            lr += distances[sort_index[j]]
        lr /= retained_points

        rigid_matrice += [res_rigid_mat]
        affine_matrice += [res_affine_mat]
        average_residual += [lr]

    return rigid_matrice, affine_matrice, average_residual


def _coregister_embryos_one_timepoint(embryo, info_embryo, atlas, info_atlas, parameters, verbose=True):

    # embryo
    # xxx_barycenter is the barycenter in homogeneous coordinates (not corrected for a constant volume embryo)
    #    NB: it is in voxel coordinates
    # xxx_barycenters is an np.array of shape (3, n) of the n cell barycenters at time reference_time
    #    ref_barycenters[0][i] is the the x coordinate of the ith barycenter
    # xxx_scale is a 4x4 matrix that allows to normalize the coordinates
    #   as if all embryos have the same volume

    flo_embryo_barycenter = np.ones(4)
    flo_embryo_barycenter[:3] = info_embryo['barycenter']
    flo_barycenters = _get_barycenters(embryo, info_embryo['first_time'])
    #
    # isotropic scaling to get iso-volume embryos
    #
    flo_scale = embryo.get_voxelsize_correction(info_atlas['first_time']) * np.identity(4)
    flo_scale[3, 3] = 1.0

    # anisotropic scaling to get embryo with an isotropic covariance matrix (variances of 1)
    # was tested as initial conditions for registration for U0126 embryos without improvement
    #
    # flo_scale = np.identity(4)
    # val, vec = np.linalg.eig(info_embryo['covariance'])
    # flo_scale[:3, :3] = np.matmul(vec, np.matmul(np.linalg.inv(np.diag(np.sqrt(val))), vec.T))

    flo_embryo = (flo_embryo_barycenter, flo_barycenters, flo_scale)

    ref_embryo_barycenter = np.ones(4)
    ref_embryo_barycenter[:3] = info_atlas['barycenter']
    ref_barycenters = _get_barycenters(atlas, info_atlas['first_time'])
    #
    # isotropic scaling to get iso-volume embryos
    #
    ref_scale = embryo.get_voxelsize_correction(info_atlas['first_time']) * np.identity(4)
    ref_scale[3, 3] = 1.0

    # anisotropic scaling to get embryo with an isotropic covariance matrix (variances of 1)
    # was tested as initial conditions for registration for U0126 embryos without improvement
    #
    # ref_scale = np.identity(4)
    # val, vec = np.linalg.eig(info_embryo['covariance'])
    # ref_scale[:3, :3] = np.matmul(vec, np.matmul(np.linalg.inv(np.diag(np.sqrt(val))), vec.T))

    ref_embryo = (ref_embryo_barycenter, ref_barycenters, ref_scale)

    #
    # we could improve the computational time by parallelising the calculation
    # with multiprocessing
    #
    pool = multiprocessing.Pool(processes=parameters.processors)
    mapping = []

    #
    # 2D registrations that put info_atlas['symvectors'][:, j] on  info_embryo['symvectors'][:, i]
    #

    n_trsf = info_atlas['symvectors'].shape[1] * info_embryo['symvectors'].shape[1]

    #
    # parallel
    #
    for j in range(info_atlas['symvectors'].shape[1]):
        for i in range(info_embryo['symvectors'].shape[1]):
            i_trsf = j*info_embryo['symvectors'].shape[1]+i+1
            #
            #
            mapping.append((i_trsf, n_trsf, info_embryo['symvectors'][:, i], info_atlas['symvectors'][:, j],
                            flo_embryo, ref_embryo, parameters, verbose))

    outputs = pool.map(_coregister_embryos_one_direction, mapping)
    pool.close()
    pool.terminate()

    rigid_matrice = []
    affine_matrice = []
    average_residual = []
    #
    # outputs is a list of tuples
    #
    for rig_mat, aff_mat, lr in outputs:
        rigid_matrice += rig_mat
        affine_matrice += aff_mat
        average_residual += lr

    # residuals study
    index_min = min(range(len(average_residual)), key=average_residual.__getitem__)
    if verbose:
        min_residual = average_residual[index_min]
        ave_residual = statistics.mean(average_residual)
        stddev_residual = statistics.stdev(average_residual)
        msg = "residuals:  min = {:5.2f}".format(min_residual)
        msg += " - average = {:5.2f}".format(ave_residual)
        msg += " (+/- stddev = {:5.2f}".format(stddev_residual) + ")"
        monitoring.to_log_and_console("   ... " + msg)
    return rigid_matrice[index_min], affine_matrice[index_min]


#
# register one embryo with respect to several named embryos
#
def register_embryo(embryo, time_embryo, atlas, time_atlas, parameters, verbose=True):
    proc = "register_embryo"

    flo_embryo = {'first_time': time_embryo}
    flo_embryo['barycenter'] = embryo.get_embryo_barycenter(time_embryo)
    if parameters.rotation_initialization == 'sphere_wrt_z' or \
            parameters.rotation_initialization == 'sphere_wrt_symaxis':
        #
        # get a v[3, ...] array containing unit vectors sampling the sphere
        # vdist.sphere_vector() return a numpy array os shape (N, 3)
        vdist.monitoring.copy(monitoring)
        flo_embryo['symvectors'] = np.transpose(vdist.sphere_vector(parameters.direction_sphere_radius))
    elif parameters.rotation_initialization == 'symaxis_wrt_symaxis':
        candidates = embryo.get_direction_distribution_candidates(time_embryo, parameters)
        nvectors = candidates.shape[0]
        if isinstance(parameters.direction_maxima_number, int) and 0 < parameters.direction_maxima_number < nvectors:
            nvectors = parameters.direction_maxima_number
        flo_embryo['symvectors'] = np.zeros((3, nvectors))
        for i, p in enumerate(candidates):
            if i >= nvectors:
                break
            flo_embryo['symvectors'][:, i] = p
    else:
        msg = "unknown rotation initialization '" + str(parameters.rotation_initialization) + "'"
        monitoring.to_log_and_console(proc + ": " + msg)
        return None

    ref_atlas = {'first_time': time_atlas}
    ref_atlas['barycenter'] = atlas.get_embryo_barycenter(time_atlas)
    if parameters.rotation_initialization == 'sphere_wrt_z':
        ref_atlas['symvectors'] = np.zeros((3, 1))
        ref_atlas['symvectors'][2, 0] = 1
    elif parameters.rotation_initialization == 'sphere_wrt_symaxis' or \
            parameters.rotation_initialization == 'symaxis_wrt_symaxis':
        ref_atlas['symvectors'] = np.zeros((3, 1))
        ref_atlas['symvectors'][:, 0] = atlas.get_symmetry_axis_from_names(time_atlas)
    else:
        msg = "unknown rotation initialization '" + str(parameters.rotation_initialization) + "'"
        monitoring.to_log_and_console(proc + ": " + msg)
        return None

    rigid_mat, affine_mat = _coregister_embryos_one_timepoint(embryo, flo_embryo, atlas, ref_atlas, parameters,
                                                              verbose=verbose)

    return affine_mat


########################################################################################
#
# naming procedures
#
########################################################################################


def _get_name_from_registration(transformation, embryo, flo_cells, atlas, ref_cells, verbose=True):

    # get reference barycenter coordinates
    ref_barycenters = atlas.cell_barycenter
    bref = np.zeros((3, len(ref_cells)))
    for i, c in enumerate(ref_cells):
        bref[:, i] = ref_barycenters[c]

    # get floating barycenter coordinates
    flo_barycenters = embryo.cell_barycenter
    bflo = np.zeros((3, len(flo_cells)))
    v = np.ones(4)
    for i, c in enumerate(flo_cells):
        v[:3] = flo_barycenters[c]
        bflo[:, i] = (np.matmul(transformation, v))[:3]

    # find reference barycenters that are closest to floating ones
    # i_flo_to_ref[i] is the index (in the list) of the reference cell closest
    # to the ith floating cell in cell_tflo list
    nghbref = NearestNeighbors(n_neighbors=1, algorithm='kd_tree').fit(bref.T)
    d_flo_to_ref, i_flo_to_ref = nghbref.kneighbors(bflo.T)
    i_flo_to_ref = i_flo_to_ref.flatten()

    # find floating barycenters that are closest to reference ones
    nghbflo = NearestNeighbors(n_neighbors=1, algorithm='kd_tree').fit(bflo.T)
    d_ref_to_flo, i_ref_to_flo = nghbflo.kneighbors(bref.T)
    i_ref_to_flo = i_ref_to_flo.flatten()

    cell_name = {}
    total_distance = 0.0

    # find cell/barycenters which are closest to each other
    ref_name = atlas.cell_name
    reciprocal_n = 0
    # i : index of cell c in cell_tflo
    for i, cf in enumerate(flo_cells):
        j = i_flo_to_ref[i]
        cr = ref_cells[j]
        if cr not in ref_name:
            continue
        if i_ref_to_flo[j] == i:
            # here centers of mass are closest to each other
            cell_name[cf] = ref_name[cr]
            total_distance += np.linalg.norm(bref[:, j] - bflo[:, i])
        else:
            # here, we consider a floating center of mass (from embryo)
            # and the closest cell center of mass in the atlas
            # the latter has a different closest cell center of mass in the embryo
            pass

    if len(cell_name) > 0:
        total_distance /= len(cell_name)

    # for j, cr in enumerate(ref_cells):
    #     i = i_ref_to_flo[j]
    #     cf = cell_tflo[i]
    #     if cr not in ref_name:
    #         continue
    #     if i_flo_to_ref[i] == j:
    #         # reciprocical
    #         pass
    #     else:
    #         # here, we consider a reference center of mass (from atlas)
    #         # and the closest cell center of mass in the embryo
    #         # the latter has a different closest cell center of mass in the atlas
    #         pass

    return cell_name, total_distance


########################################################################################
#
#
#
########################################################################################


def naming_by_registration(embryo, time_embryo, atlas, time_atlas, parameters, precomputeddata=None, verbose=True):
    proc = "naming_by_registration"

    if isinstance(embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryo' variable: " + str(type(embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if isinstance(atlas, cembryo.Embryo) is False:
        msg = ": unexpected type for 'atlas' variable: " + str(type(atlas))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if not isinstance(parameters, ntparameters.NamingTimepointParameters):
        msg = "parameters expected type is 'NamingTimepointParameters' but was '"
        msg += str(type(parameters)) + "' instead"
        monitoring.to_log_and_console(proc + ": " + msg)
        return None, None, None

    properties = embryo.property_list()

    if 'rectified_cell_contact_surface' not in properties:
        monitoring.to_log_and_console(str(proc) + ": 'cell_contact_surface' was not in dictionary")
        return None, None, None

    if 'rectified_cell_volume' not in properties:
        monitoring.to_log_and_console(str(proc) + ": 'cell_volume' was not in dictionary")
        return None, None, None

    if 'rectified_cell_barycenter' not in properties:
        monitoring.to_log_and_console(str(proc) + ": 'cell_barycenter' was not in dictionary")
        return None, None, None

    #
    #
    #
    if verbose:
        msg = "registration of time #" + str(time_embryo) + " of embryo '" + str(embryo.name)
        msg += " with time #" + str(time_atlas) + " of atlas '" + str(atlas.name) + "'"
        monitoring.to_log_and_console("   ... " + msg)

    #
    # list of cell ids
    #
    flo_cells = embryo.get_cells(time_embryo, propertyname='cell_barycenter', background=False)
    ref_cells = atlas.get_cells(time_atlas, propertyname='cell_barycenter', sortcriterium='cell_name')

    #
    #
    #
    computeddata = precomputeddata
    if precomputeddata is None:
        computeddata = {}

    precomputed_data_has_to_saved = False
    if embryo.name not in computeddata:
        computeddata[embryo.name] = {}
    if time_embryo not in computeddata[embryo.name]:
        computeddata[embryo.name][time_embryo] = {}
    if atlas.name not in computeddata[embryo.name][time_embryo]:
        computeddata[embryo.name][time_embryo][atlas.name] = {}

    #
    # get transformation from precomputed data or compute transformation
    #
    if time_atlas in computeddata[embryo.name][time_embryo][atlas.name]:
        if verbose:
            msg = "transformation was already computed"
            monitoring.to_log_and_console("      ... " +msg)
        transformation = computeddata[embryo.name][time_embryo][atlas.name][time_atlas]
    else:
        transformation = register_embryo(embryo, time_embryo, atlas, time_atlas, parameters, verbose=verbose)
        if transformation is None:
            msg = "registration failed"
            monitoring.to_log_and_console("   ... " + msg)
            return None, precomputeddata, False
        computeddata[embryo.name][time_embryo][atlas.name][time_atlas] = transformation
        precomputed_data_has_to_saved = True

    #
    # get candidate names
    #
    res_name, mean_distance = _get_name_from_registration(transformation, embryo, flo_cells, atlas, ref_cells,
                                                          verbose=verbose)

    if verbose:
        msg = "registration of time #" + str(time_embryo) + " of embryo '" + str(embryo.name) + "'"
        msg += " with time #" + str(time_atlas) + " of atlas '" + str(atlas.name) + "'"
        msg += " has a mean distance of {:.4f}".format(mean_distance)
        monitoring.to_log_and_console("   ... " + msg)
        msg = str(len(res_name)) + " cells out of " + str(len(flo_cells))
        msg += " have been named from atlas '" + str(atlas.name) + "'"
        monitoring.to_log_and_console("       " + msg)

    if precomputeddata is None:
        return res_name, computeddata, False
    return res_name, computeddata, precomputed_data_has_to_saved


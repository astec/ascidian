##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu 27 jui 2024 17:27:09 CEST
#
##############################################################
#
#
#
##############################################################

import os
import copy
import numpy as np
import statistics
import pickle as pkl

from operator import itemgetter

import astec.utils.common as common

import ascidian.ascidian.name as aname
import ascidian.core.contact_surface_distance as ccsurfdist
import ascidian.core.naming_utils as cnamutils

monitoring = common.Monitoring()

########################################################################################
#
#
#
########################################################################################

def get_precomputeddata(parameters, filename):
    precomputeddata = {}
    if filename is None:
        return precomputeddata
    if isinstance(filename, str):
        if filename.endswith(".pkl") is True:
            inputfilename = filename
        else:
            inputfilename = filename + '.pkl'
        if os.path.isfile(inputfilename):
            inputfile = open(inputfilename, 'rb')
            precomputeddata = pkl.load(inputfile)
            inputfile.close()
    return precomputeddata


def save_precomputeddata(precomputeddata, filename):
    if filename is None:
        return
    if isinstance(filename, str):
        if filename.endswith(".pkl") is True:
            outputfile = open(filename, 'wb')
        else:
            outputfile = open(filename + '.pkl', 'wb')
        pkl.dump(precomputeddata, outputfile)
        outputfile.close()
    return


########################################################################################
#
#
#
########################################################################################


def set_names_from_candidates(candidates, minimal_consensus, verbose=False):

    min_consensus = minimal_consensus
    if not isinstance(minimal_consensus, (int, float)):
        min_consensus = 100
    if verbose:
        msg = "   ... minimal consensus is {:.1f} %".format(min_consensus)
        monitoring.to_log_and_console(msg)

    cell_name = {}

    used_names = []
    iteration = 0
    while True:
        name_count = {}
        name_total_count = {}
        #
        # count the times a name is attributed to a cell
        #
        for c in candidates:
            if c not in name_count:
                name_count[c] = {}
            for n in candidates[c]:
                if n in used_names:
                    continue
                name_count[c][n] = name_count[c].get(n, 0) + 1
                name_total_count[c] = name_total_count.get(c, 0) + 1
        #
        # uses percentages
        #
        for c in name_total_count:
            for n in name_count[c]:
                name_count[c][n] = round(100.0 * name_count[c][n] / name_total_count[c])
        count = [name_count[c][n] for c in name_count for n in name_count[c]]
        #
        # no more names?
        #
        if len(count) == 0:
            break
        #
        # get names with maximum votes
        #
        maxcount = max(count)
        if maxcount < min_consensus:
            if verbose:
                msg = "maximal consensus at iteration #{:d} is {:.1f}%. Stop naming.".format(iteration, maxcount)
                monitoring.to_log_and_console("      ... " + msg)
            break

        #
        # set names
        #
        attributed_names = 0
        for c in name_total_count:
            for n in name_count[c]:
                if name_count[c][n] == maxcount:
                    if n in used_names:
                        if verbose:
                            msg = "weird, name " + str(n) + " has already been attributed."
                            msg += " Can not name cell " + str(c)
                            monitoring.to_log_and_console("         ... " + msg)
                    else:
                        cell_name[c] = n
                        used_names += [n]
                        # del name_count[c][n]
                        attributed_names += 1
        if verbose:
            msg = "found {:d} consensual names at iteration #{:d}".format(attributed_names, iteration)
            monitoring.to_log_and_console("      ... " + msg)
        #
        # no name with global consensus? stop
        #
        if attributed_names == 0:
            break
        #
        # remove used names from possible names
        #
        for c in name_count:
            nc = {n: value for n, value in name_count[c].items() if n not in used_names}
            name_count[c] = nc
        iteration += 1

    return cell_name


##############################################################
#
#
#
##############################################################

def get_time_from_ncells(embryo, cell_number, name=None, change_cell_number=True, verbose=True):
    proc = "get_time_from_ncells"
    ncells, timerange, ininterval = embryo.get_timerange(cell_number, change_cell_number=change_cell_number)
    if ncells is None:
        if verbose:
            msg = "weird, requested cell number was " + str(cell_number) + ". "
            msg += "No cell range was found."
            monitoring.to_log_and_console(proc + ": " + msg)
        return None, None, None

    returned_time = statistics.median_low(timerange)

    if ncells != cell_number:
        if verbose:
            msg = "will consider embryo "
            if name is not None:
                msg += "'" + str(name) + "' "
            msg += "at time " + str(returned_time)
            msg += ", at " + str(ncells) + " cells instead of " + str(cell_number)
            monitoring.to_log_and_console(proc + ": " + msg)

    if ininterval is True:
        returned_cell_number = cell_number
    else:
        returned_cell_number = ncells

    return returned_cell_number, returned_time, timerange


##############################################################
#
#
#
##############################################################


def remove_duplicated_names(names, time_digits_for_cell_id=4, verbose=True):
    proc = "remove_duplicated_names"

    cells = set(list(names.keys()))
    div = 10 ** time_digits_for_cell_id
    times = list(set([int(c) // div for c in cells]))

    if len(times) == 0:
        msg = "no names were set"
        monitoring.to_log_and_console("... " + msg)
        return names
    elif len(times) > 1:
        msg = "weird, names were set on several time points " + str(times) + " in " + proc
        monitoring.to_log_and_console("... " + msg)

    for t in times:
        cells = [c for c in names if c // div == t]

        cell_per_name = {}
        for c in cells:
            cell_per_name[names[c]] = cell_per_name.get(names[c], []) + [c]

        duplicates = [n for n in cell_per_name if len(cell_per_name[n]) > 1]
        if len(duplicates) > 0:
            for n in duplicates:
                for c in cell_per_name[n]:
                    del names[c]
                if verbose:
                    msg = "   ... name " + str(n) + " was given " + str(len(cell_per_name[n])) + "  times"
                    monitoring.to_log_and_console(msg)

    return names


########################################################################################
#
#
#
########################################################################################


def _get_atlases_branch_volume(name, atlases):
    ref_atlases = atlases.get_embryos()
    vols = []
    for a in ref_atlases:
        #
        # get volume correction for the current time
        # to get homogeneized volumes
        #
        ref_name = ref_atlases[a].cell_name
        ref_volume = ref_atlases[a].rectified_cell_volume
        for c in ref_name:
            if ref_name[c] != name:
                continue
            if c not in ref_volume:
                msg = "weird, cell " + str(c) + " is not in volumes of atlas '" + str(a) + "'"
                monitoring.to_log_and_console("      ... " + msg)
                continue
            vols += [ref_volume[c]]
    return vols


def _get_embryo_branch_volume(embryo, cell, lineage, reverse_lineage):
    vols = []
    c = cell
    cell_volume = embryo.rectified_cell_volume
    while True:
        if c in cell_volume:
            vols += [cell_volume[c]]
        if c not in lineage:
            break
        if len(lineage[c]) != 1:
            break
        c = lineage[c][0]

    c = cell
    while True:
        if c not in reverse_lineage:
            break
        c = reverse_lineage[c]
        if len(lineage[c]) != 1:
            break
        if c in cell_volume:
            vols += [cell_volume[c]]
    return vols


def check_initial_naming_volume(prop, embryo, atlases, verbose=True):
    proc = "check_initial_naming_volume"

    times = cnamutils.get_named_timepoints(prop, time_digits_for_cell_id=embryo.time_digits_for_cell_id)
    if len(times) == 0:
        if verbose:
            msg = "no names were set"
            monitoring.to_log_and_console("... " + msg)
    elif len(times) > 1:
        if verbose:
            msg = "weird, names were set on several time points " + str(times) + " in " + proc
            monitoring.to_log_and_console("... " + msg)

    lineage = embryo.cell_lineage
    reverse_lineage = {v: k for k, values in lineage.items() for v in values}

    cells = sorted(list(prop['cell_name'].keys()))
    unnamed_cells = []
    for c in cells:
        branch_volume = _get_embryo_branch_volume(embryo, c, lineage, reverse_lineage)
        cell_volume = np.mean(branch_volume)
        name = prop['cell_name'][c]

        #
        # get volumes of cell with the same name in reference
        #
        volumes = _get_atlases_branch_volume(name, atlases)
        mean_volume = np.mean(volumes)
        std_volume = np.std(volumes)
        if cell_volume >= mean_volume:
            #
            # get volumes of the mother cell in reference
            #
            mname = aname.get_mother_name(name)
            m_volumes = _get_atlases_branch_volume(mname, atlases)
            if len(m_volumes) == 0:
                if verbose:
                    msg = "   ... can not check " + str(c) + " (given name was '" + str(name) + "')."
                    msg += " No mother cell volumes."
                    monitoring.to_log_and_console(msg)
                continue
            m_mean_volume = np.mean(m_volumes)
            if cell_volume >= m_mean_volume:
                unnamed_cells += [(c, name)]
                del prop['cell_name'][c]
                if verbose:
                    msg = "   ... unnamed cell " + str(c) + " (given name was '" + str(name) + "').\n"
                    msg += "       Cell volume larger than reference mother cell volumes."
                    monitoring.to_log_and_console(msg)
                continue
            #   emb_volume[c] < m_mean_volume:
            if len(m_volumes) == 1:
                diff_to_cell = (cell_volume - mean_volume)
                diff_to_mcell = (m_mean_volume - cell_volume)
            else:
                m_std_volume = np.std(m_volumes)
                if std_volume > 0 and m_std_volume > 0:
                    diff_to_cell = (cell_volume - mean_volume) / std_volume
                    diff_to_mcell = (m_mean_volume - cell_volume) / m_std_volume
                else:
                    diff_to_cell = (cell_volume - mean_volume)
                    diff_to_mcell = (m_mean_volume - cell_volume)
            if diff_to_cell >= diff_to_mcell:
                unnamed_cells += [(c, name)]
                del prop['cell_name'][c]
                if verbose:
                    msg = "   ... unnamed cell " + str(c) + " (given name was '" + str(name) + "').\n"
                    msg += "       Cell volume closer to reference mother cell volumes."
                    monitoring.to_log_and_console(msg)
                continue
        else:
            dnames = aname.get_daughter_names(name)
            d_volumes = [0, 0]
            d_mean_volume = [0, 0]
            d_volumes[0] = _get_atlases_branch_volume(dnames[0], atlases)
            d_volumes[1] = _get_atlases_branch_volume(dnames[1], atlases)
            if len(d_volumes[0]) == 0 and len(d_volumes[1]) == 0:
                if verbose:
                    msg = "   ... can not check " + str(c) + " (given name was '" + str(name) + "')."
                    msg += " No daughter cell volumes."
                    monitoring.to_log_and_console(msg)
                continue
            elif len(d_volumes[0]) == 0:
                d_mean_volume[1] = np.mean(d_volumes[1])
                idaughter = 1
            elif len(d_volumes[1]) == 0:
                d_mean_volume[0] = np.mean(d_volumes[0])
                idaughter = 0
            else:
                d_mean_volume = [0, 0]
                d_mean_volume[0] = np.mean(d_volumes[0])
                d_mean_volume[1] = np.mean(d_volumes[1])
                if d_mean_volume[1] > d_mean_volume[0]:
                    idaughter = 1
                else:
                    idaughter = 0
            if cell_volume <= d_mean_volume[idaughter]:
                unnamed_cells += [(c, name)]
                del prop['cell_name'][c]
                if verbose:
                    msg = "   ... unnamed cell " + str(c) + " (given name was '" + str(name) + "').\n"
                    msg += "       Cell volume smaller than the largest reference daughter ('"
                    msg += str(dnames[idaughter]) + "') cell volumes."
                    monitoring.to_log_and_console(msg)
                continue
            # emb_volume[c] > d_mean_volume[idaughter]
            if len(d_volumes[idaughter]) == 1:
                diff_to_cell = (mean_volume - cell_volume)
                diff_to_dcell = (cell_volume - d_mean_volume[idaughter])
            else:
                d_std_volume = np.std(d_volumes[idaughter])
                if std_volume > 0 and d_std_volume > 0:
                    diff_to_cell = (mean_volume - cell_volume) / std_volume
                    diff_to_dcell = (cell_volume - d_mean_volume[idaughter]) / d_std_volume
                else:
                    diff_to_cell = (mean_volume - cell_volume)
                    diff_to_dcell = (cell_volume - d_mean_volume[idaughter])
            if diff_to_cell >= diff_to_dcell:
                unnamed_cells += [(c, name)]
                del prop['cell_name'][c]
                if verbose:
                    msg = "   ... unnamed cell " + str(c) + " (given name was '" + str(name) + "').\n"
                    msg += "       Cell volume closer to the largest reference daughter ('"
                    msg += str(dnames[idaughter]) + "') cell volumes."
                    monitoring.to_log_and_console(msg)
                continue
    if verbose:
        msg = "   unname " + str(len(unnamed_cells)) + " cells"
        monitoring.to_log_and_console(msg)
    return prop



########################################################################################
#
#
#
########################################################################################


def assess_naming(names, reference_names, time_digits_for_cell_id=4, verbose=True):
    proc = "assess_naming"

    cells = set(list(names.keys()))
    div = 10 ** time_digits_for_cell_id
    times = list(set([int(c) // div for c in cells]))

    refcells = set([c for c in reference_names if int(c) // div in times])

    unnamed_cells = refcells.difference(cells)
    unnamed_names = sorted([reference_names[c] for c in unnamed_cells])
    not_in_reference_cells = cells.difference(refcells)

    common_cells = refcells.intersection(cells)
    right_cells = []
    wrong_cells = []
    for c in common_cells:
        if names[c] != reference_names[c]:
            wrong_cells += [c]
            continue
        right_cells += [c]
    wrong_names = [(reference_names[c], names[c]) for c in wrong_cells]
    wrong_names = sorted(wrong_names, key=itemgetter(0))

    if verbose:
        "   ... as"
        "       "
        msg = "ground-truth cells = " + str(len(refcells)) + " for times " + str(times) + "\n"
        msg += "       tested cells = " + str(len(cells)) + "\n"
        msg += "       right retrieved names = {:d}/{:d}\n".format(len(right_cells), len(refcells))
        if len(wrong_cells) > 0:
            msg += "       ... error in naming = {:d}/{:d}\n".format(len(wrong_cells), len(refcells))
            msg += "           reference cells wrongly named (right name, given name): " + str(wrong_names) + "\n"
        if len(unnamed_cells) > 0:
            msg += "        ... unnamed cells = {:d}/{:d}\n".format(len(unnamed_cells), len(refcells))
            msg += "            reference cells unnamed: " + str(unnamed_names) + "\n"
        if len(not_in_reference_cells) > 0:
            msg += "        ... names not in reference = {:d}/{:d}\n".format(len(not_in_reference_cells), len(refcells))
        monitoring.to_log_and_console("   ... " + str(proc) + ": " + msg)

    return len(right_cells), len(wrong_cells), len(unnamed_cells), len(not_in_reference_cells)


########################################################################################
#
#
#
########################################################################################


def evaluate_initial_naming(prop, embryo, atlases, time_embryo, time_atlases):
    proc = "evaluate_initial_naming"

    if 'cell_name' not in prop:
        monitoring.to_log_and_console(str(proc) + ": 'cell_name' was not in dictionary")
        return prop

    times = cnamutils.get_named_timepoints(prop, time_digits_for_cell_id=embryo.time_digits_for_cell_id)
    if len(times) == 0:
        msg = "no names were set"
        monitoring.to_log_and_console("... " + msg)
        return prop
    elif len(times) > 1:
        msg = "weird, names were set on several time points " + str(times)
        monitoring.to_log_and_console("... " + msg)
    elif len(times) == 1 and times[0] != time_embryo:
        msg = "named time point is " + str(times[0]) + " while naming is supposed to be done at time "
        msg += str(time_embryo)
        monitoring.to_log_and_console("... " + msg)

    ref_atlases = atlases.get_embryos()
    keydistance = 'morphonet_float_init_naming_neighborhood_assessment'
    keyassessment = 'morphonet_float_init_naming_neighborhood_assessment_quality'
    prop[keydistance] = {}
    prop[keyassessment] = {}
    div = 10 ** embryo.time_digits_for_cell_id

    #
    # global normalization of the contact surfaces
    #
    cell_contact_surface = embryo.rectified_cell_contact_surface
    cells = sorted(list(prop['cell_name'].keys()))

    msg_already_done = []

    for c in cells:
        if c not in cell_contact_surface:
            msg = "weird, cell '" + str(c) + "' is not in 'cell_contact_surface' dictionary "
            monitoring.to_log_and_console("... " + msg)
            continue
        #
        # get the cell neighborhood
        # normalize it
        # replace cell id with names
        #
        t_flo = c // div
        if t_flo != time_embryo:
            msg = "weird, time point '" + str(t_flo) + "' is different from '" + str(time_embryo) + "'"
            monitoring.to_log_and_console("... " + msg)
            continue
        neighborhood = {'foo': {}}
        neighborhood['foo']['embryo'] = copy.deepcopy(cell_contact_surface[c])

        surface = 0.0
        named_surface = 0.0
        cellids = list(neighborhood['foo']['embryo'].keys())
        for d in cellids:
            surface += neighborhood['foo']['embryo'][d]
            if int(d) % div == 1 or int(d) % div == 0:
                named_surface += neighborhood['foo']['embryo'][d]
                neighborhood['foo']['embryo']['background'] = neighborhood['foo']['embryo'][d]
                del neighborhood['foo']['embryo'][d]
            elif d in prop['cell_name']:
                named_surface += neighborhood['foo']['embryo'][d]
                neighborhood['foo']['embryo'][prop['cell_name'][d]] = neighborhood['foo']['embryo'][d]
                del neighborhood['foo']['embryo'][d]
            else:
                #
                # we kept the cell id as key
                # it may be problematic if the same cell id correspond to an unnamed cell
                # in the neighborhood of the cell with the same name in an atlas
                #
                pass

        prop[keyassessment][c] = named_surface / surface

        #
        # get other neighborhoods
        # transformations[t_flo][atlas][t_ref] is the affine transformation that maps the floating frame
        # (at t_flo) into the reference one (at t_ref)
        #
        for a in ref_atlases:

            if (embryo.name == a) or (a[:4] == 'sym-' and embryo.name == a[4:]) \
                    or (embryo.name[:4] == 'sym-' and embryo.name[4:] == a):
                if (a, t_flo) not in msg_already_done:
                    msg = "will not assess '" + str(embryo.name) + "' with '" + str(a) + "'. Skip it."
                    monitoring.to_log_and_console("   ... " + msg)
                    msg_already_done += [(a, t_flo)]
                continue

            if a not in time_atlases:
                if (a, t_flo) not in msg_already_done:
                    msg = "weird, atlas '" + str(a) + "' is not in used dictionaries at time point "
                    msg += str(t_flo)
                    monitoring.to_log_and_console("... " + msg)
                    msg_already_done += [(a, t_flo)]
                continue
            t_ref = time_atlases[a]
            #
            # find cells at t_ref in atlas 'a' with the same name than c
            #
            adiv = 10 ** ref_atlases[a].time_digits_for_cell_id
            acell = [ac for ac in ref_atlases[a].cell_name if ac // adiv == t_ref and ref_atlases[a].cell_name[ac] ==
                     prop['cell_name'][c]]
            if len(acell) == 0:
                # msg = "weird, atlas '" + str(a) + "' has no cell named " + str(prop['cell_name'][c])
                # msg += " at time point " + str(t_ref)
                # monitoring.to_log_and_console("... " + msg)
                continue
            elif len(acell) > 1:
                msg = "weird, atlas '" + str(a) + "' has several cells named " + str(prop['cell_name'][c])
                msg += " at time point " + str(t_ref)
                monitoring.to_log_and_console("... " + msg)
                continue
            neighborhood['foo'][a] = copy.deepcopy(ref_atlases[a].rectified_cell_contact_surface[acell[0]])
            cellids = list(neighborhood['foo'][a].keys())
            for d in cellids:
                if int(d) % adiv == 1 or int(d) % adiv == 0:
                    neighborhood['foo'][a]['background'] = neighborhood['foo'][a][d]
                    del neighborhood['foo'][a][d]
                elif d in ref_atlases[a].cell_name:
                    neighborhood['foo'][a][ref_atlases[a].cell_name[d]] = neighborhood['foo'][a][d]
                    del neighborhood['foo'][a][d]
                else:
                    #
                    # we kept the cell id as key
                    # it may be problematic if the same cell id correspond to an unnamed cell
                    # in the neighborhood of the cell from the embryo to be tested
                    #
                    pass
        #
        # for named cell c, we built neighborhood['foo'][embryo, a in atlases]
        # which is a dictionary/vector of contact surfaces whose key are cell names (when it is possible)
        # build same contact surfaces
        #
        neighborhood = ccsurfdist.build_same_contact_surfaces(neighborhood, ['foo'])
        distances = []
        for a in neighborhood['foo']:
            if a == 'embryo':
                continue
            nm, n0, n1 = ccsurfdist.contact_surface_distance_elements(neighborhood['foo']['embryo'],
                                                                      neighborhood['foo'][a])
            distances += [1.0 - nm / (n0 + n1)]
        if len(distances) == 0:
            msg = "weird, no atlases are found to name cell " + str(c) + " as " + str(prop['cell_name'][c])
            monitoring.to_log_and_console("... " + msg)
        else:
            prop[keydistance][c] = np.mean(distances)
    return prop

##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu 18 jul 2024 10:55:00 CEST
#
##############################################################
#
#
#
##############################################################

import sys

import copy
import statistics
import math

# from operator import itemgetter

import numpy as np
import scipy.optimize as optimize

import astec.utils.common as common

import ascidian.ascidian.name as aname
import ascidian.core.icp as icp
import ascidian.core.cell_signature as ccellsign
import ascidian.components.embryo as cembryo
import ascidian.components.embryoset as cembryoset
import ascidian.naming_timepoint.parameters as ntparameters

monitoring = common.Monitoring()


##############################################################
#
#
#
##############################################################


def _get_name_from_assignment(cost, embryo, flo_cells, atlas, ref_cells):

    ref_cell_name = atlas.cell_name

    row_ind, col_ind = optimize.linear_sum_assignment(cost)
    total_cost = cost[row_ind, col_ind].sum()
    cell_name = {}

    for i, r in enumerate(row_ind):
        if ref_cells[col_ind[i]] in ref_cell_name:
            # name = ref_cell_name[ref_cells[col_ind[i]]]
            # score = cost[r, col_ind[i]]
            # cell_name[flo_cells[r]] = cell_name.get(flo_cells[r], []) + [(name, score)]
            cell_name[flo_cells[r]] = ref_cell_name[ref_cells[col_ind[i]]]

    return cell_name, total_cost


def _get_name_from_doublemin_assignment(cost, embryo, flo_cells, atlas, ref_cells):

    ref_cell_name = atlas.cell_name

    #
    # cost is a 2D numpy ndarray, of shape ((len(flo_cells), len(ref_cells)))
    #   cost[i,j] is the cost to associate cell #i from flo_cells to cell #j from ref_cells
    # - np.min(cost, axis=1) gives the min for each flo cell along axis 1
    #   np.min(cost, axis=1) has shape (len(ref_cells),)
    # - col_argmin = np.argmin(cost, axis=1) gives the index of the min for each flo cell along axis 1
    #   the minimum has indices [i, col_argmin[i]] in the 2D numpy array 'cost'
    #
    # since the index of the minimum for ref cell #j is [row_argmin[j], j]
    # it comes that the double minimum is characterized by row_argmin[col_argmin[i]] = i
    #
    row_argmin = np.argmin(cost, axis=0)
    col_argmin = np.argmin(cost, axis=1)
    col_min = np.min(cost, axis=1)
    cell_name = {}

    n = 0
    total_cost = 0
    for i, fc in enumerate(flo_cells):
        # msg = "#" + str(i)
        if i == row_argmin[col_argmin[i]]:
            n += 1
            total_cost += col_min[i]
            if ref_cells[col_argmin[i]] in ref_cell_name:
                cell_name[fc] = ref_cell_name[ref_cells[col_argmin[i]]]

    # return the average cost
    return cell_name, total_cost


##############################################################
#
#
#
##############################################################

def _lateralize_names(unlateralized_names, embryo, time_embryo, parameters, verbose=True):

    if isinstance(embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryos' variable: " + str(type(embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if verbose:
        monitoring.to_log_and_console("      ... lateralize names")

    embryo_barycenter = embryo.get_embryo_barycenter(time_embryo)
    barycenter = embryo.cell_barycenter
    candidates = embryo.get_direction_distribution_candidates(time_embryo, parameters, verbose=verbose)
    #
    # get the first candidate vector
    #
    symvector = candidates[0]

    all_unlateralized_names = [unlateralized_names[c] for c in unlateralized_names]
    lateralized_names = {}

    #
    # positive dot product get the left (_) name
    #
    for n in all_unlateralized_names:
        cells = [c for c in unlateralized_names if unlateralized_names[c] == n]
        ps0 = np.dot(barycenter[cells[0]] - embryo_barycenter, symvector)
        if len(cells) == 2:
            ps1 = np.dot(barycenter[cells[1]] - embryo_barycenter, symvector)
            if ps0 >= ps1:
                lateralized_names[cells[0]] = unlateralized_names[cells[0]] + '_'
                lateralized_names[cells[1]] = unlateralized_names[cells[0]] + '*'
            else:
                lateralized_names[cells[0]] = unlateralized_names[cells[0]] + '*'
                lateralized_names[cells[1]] = unlateralized_names[cells[0]] + '_'
        else:
            if ps0 >= 0:
                lateralized_names[cells[0]] = unlateralized_names[cells[0]] + '_'
            else:
                lateralized_names[cells[0]] = unlateralized_names[cells[0]] + '*'

    return lateralized_names


def _fix_laterality(lateralized_names, embryo, atlas, time_atlas, verbose=True):
    proc = "_fix_laterality"
    if isinstance(embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryo' variable: " + str(type(embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if isinstance(atlas, cembryo.Embryo) is False:
        msg = ": unexpected type for 'atlas' variable: " + str(type(atlas))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)
    #
    # reverse dictionary: embryo_cellid[cell_name] = cell-id
    #
    embryo_cellid = {lateralized_names[k]: k for k in lateralized_names}
    embryo_names = sorted(list(embryo_cellid.keys()))
    embryo_snames = [aname.get_symmetric_name(n) for n in embryo_names]

    if len(embryo_cellid) < 4:
        monitoring.to_log_and_console("      ... no enough named cells to check laterality")
        return prop
    if verbose:
        monitoring.to_log_and_console("      ... checking laterality")
    #
    # collect barycenters of embryo to be named
    #
    flo_pts = np.ones((4, len(embryo_cellid)))
    for i, n in enumerate(embryo_names):
        flo_pts[:3, i] = embryo.cell_barycenter[embryo_cellid[n]]

    votes = np.zeros((2, 2))

    # when called by figure_naming_registration.py
    # time_atlases contains the chosen times for all the tested references
    # keys to be used are the ones from ref_atlases

    ref_cells = atlas.get_cells(time_atlas)
    atlas_cellid = {atlas.cell_name[c]: c for c in ref_cells}
    #
    # collect barycenters of atlas with the same name
    # should check whether the name is in cell_barycenter
    ref_pts = np.ones((4, len(embryo_cellid)))
    for i, n in enumerate(embryo_names):
        ref_pts[:3, i] = atlas.cell_barycenter[atlas_cellid[n]]
    #
    # collect barycenters of atlas with the symmetrical name
    # should check whether the name is in cell_barycenter
    #
    ref_spts = np.ones((4, len(embryo_cellid)))
    for i, n in enumerate(embryo_snames):
        ref_spts[:3, i] = atlas.cell_barycenter[atlas_cellid[n]]

    mataff = icp.lts_transformation(ref=ref_pts, flo=flo_pts, transformation_type="affine")
    residual = np.sum(np.square(np.matmul(mataff, flo_pts))) / len(embryo_cellid)

    smataff = icp.lts_transformation(ref=ref_spts, flo=flo_pts, transformation_type="affine")
    sresidual = np.sum(np.square(np.matmul(smataff, flo_pts))) / len(embryo_cellid)

    if np.linalg.det(mataff) < 0:
        if np.linalg.det(smataff) < 0:
            msg = "weird, both transformation determinants are negative for atlas"
            monitoring.to_log_and_console("      ... " + msg)
            votes[0, 0] += 1
        else:
            votes[0, 1] += 1
    else:
        if np.linalg.det(smataff) < 0:
            votes[1, 0] += 1
        else:
            msg = "weird, both transformation determinants are positive for atlas"
            monitoring.to_log_and_console("      ... " + msg)
            votes[1, 1] += 1

    if verbose:
        msg = "residuals of embryo registration with atlas"
        msg += " same = {:.2f}".format(residual)
        msg += " symmetrical = {:.2f}".format(sresidual)
        monitoring.to_log_and_console("      ... " + msg)
        msg = "transformation determinants of embryo registration with atlas:"
        msg += " same = {:.2f}".format(np.linalg.det(mataff))
        msg += " symmetrical = {:.2f}".format(np.linalg.det(smataff))
        monitoring.to_log_and_console("      ... " + msg)

    if votes[0, 1] > votes[1, 0]:
        if verbose:
            msg = "names have to be symmetrized"
            monitoring.to_log_and_console("      ... " + msg)
        for c in lateralized_names:
            lateralized_names[c] = aname.get_symmetric_name(lateralized_names[c])

    return lateralized_names


##############################################################
#
#
#
##############################################################


def cell_signature_from_wingedcontact(embryo, flo_time, flo_cells, atlas, ref_time, ref_cells, parameters,
                                      check_symetry_axis=False, verbose=True):

    #
    # there are multiple symmetry direction candidates
    # candidates = numpy array of shape (N, 3), N being the number of candidates
    #
    candidates = embryo.get_direction_distribution_candidates(flo_time, parameters, verbose=verbose)
    n_symvectors = candidates.shape[0]
    #
    # direction_maxima_number is defined in SymmetryParameters()
    #
    if isinstance(parameters.direction_maxima_number, int) and 0 < parameters.direction_maxima_number < n_symvectors:
        n_symvectors = parameters.direction_maxima_number

    div = 10 ** embryo.time_digits_for_cell_id
    local_flo_cells = [c for c in flo_cells if int(c) % div != 1]
    div = 10 ** atlas.time_digits_for_cell_id
    local_ref_cells = [c for c in ref_cells if int(c) % div != 1]

    d_cost = {}
    d_res_name = {}
    d_total_cost = {}
    for id_symvector in range(n_symvectors):

        msg = "test symmetry axis #" + str(id_symvector) + "/" + str(n_symvectors)
        msg += " = " + str(candidates[id_symvector,:])
        monitoring.to_log_and_console("      ... " + msg)

        #
        # dict_cost[cell-id-flo][cell-id-ref] = (value, angle)
        #
        d_cost[id_symvector] = ccellsign.cell_signature(embryo, flo_time, atlas, ref_time,
                                                        cell_similarity='winged-contact', id_symvector=id_symvector,
                                                        z_rotation_angle_increment=parameters.z_rotation_angle_increment,
                                                        processors=parameters.processors)
        #
        # matrix of bipartite graph
        #
        cost = np.zeros((len(local_flo_cells), len(local_ref_cells)))
        for i, fc in enumerate(local_flo_cells):
            for j, rc in enumerate(local_ref_cells):
                cost[i, j] = d_cost[id_symvector][fc][rc][0]

        #
        # solve
        #
        output = _get_name_from_assignment(cost, embryo, local_flo_cells, atlas, local_ref_cells)
        d_res_name[id_symvector], total_cost = output
        d_total_cost[id_symvector] = total_cost

        if verbose:
            msg = "assignment of time #" + str(flo_time) + " of embryo '" + str(embryo.name) + "'"
            msg += " with time #" + str(ref_time) + " of atlas '" + str(atlas.name) + "'"
            msg += " has a total cost of {:.4f}".format(total_cost)
            monitoring.to_log_and_console("        ... " + msg)

    id_min = 0
    for id_symvector in d_total_cost:
        if d_total_cost[id_min] > d_total_cost[id_symvector]:
            id_min = id_symvector

    msg = "minimal cost is for symmetry axis #" + str(id_min)
    monitoring.to_log_and_console("      ... " + msg)
    if check_symetry_axis:
        dp = np.dot(candidates[id_min,:], embryo.get_symmetry_axis_from_names(flo_time))
        msg = ""
        if dp < 0.0:
            msg += "WARNING: symmetry axis #" + str(id_min) + " has a NEGATIVE "
            msg += " dot product with left-to-right vector of {:4f}".format(dp)
            monitoring.to_log_and_console("          " + msg)

    #
    # output
    #
    dict_cost = {}
    for fc in d_cost[id_min]:
        dict_cost[fc] = {}
        for rc in d_cost[id_min][fc]:
            dict_cost[fc][rc] = d_cost[id_min][fc][rc][0]

    return dict_cost


def new_cell_signature_from_wingedcontact(embryo, flo_time, flo_cells, atlas, ref_time, ref_cells, parameters,
                                      check_symetry_axis=False, verbose=True):

    #
    # there are multiple symmetry direction candidates
    # candidates = numpy array of shape (N, 3), N being the number of candidates
    #
    candidates = embryo.get_direction_distribution_candidates(flo_time, parameters, verbose=verbose)
    n_symvectors = candidates.shape[0]
    #
    # direction_maxima_number is defined in SymmetryParameters()
    #
    if isinstance(parameters.direction_maxima_number, int) and 0 < parameters.direction_maxima_number < n_symvectors:
        n_symvectors = parameters.direction_maxima_number

    div = 10 ** embryo.time_digits_for_cell_id
    local_flo_cells = [c for c in flo_cells if int(c) % div != 1]
    div = 10 ** atlas.time_digits_for_cell_id
    local_ref_cells = [c for c in ref_cells if int(c) % div != 1]

    d_cost = {}
    d_res_name = {}
    d_total_cost = {}
    d_affine_det = {}
    for id_symvector in range(n_symvectors):

        msg = "test symmetry axis #" + str(id_symvector) + "/" + str(n_symvectors)
        msg += " = " + str(candidates[id_symvector, :])
        monitoring.to_log_and_console("      ... " + msg)

        #
        # dict_cost[cell-id-flo][cell-id-ref] = (value, angle)
        #
        d_cost[id_symvector] = ccellsign.cell_signature(embryo, flo_time, atlas, ref_time,
                                                        cell_similarity='winged-contact', id_symvector=id_symvector,
                                                        z_rotation_angle_increment=parameters.z_rotation_angle_increment,
                                                        processors=parameters.processors)
        #
        # matrix of bipartite graph
        #
        cost = np.zeros((len(local_flo_cells), len(local_ref_cells)))
        for i, fc in enumerate(local_flo_cells):
            for j, rc in enumerate(local_ref_cells):
                cost[i, j] = d_cost[id_symvector][fc][rc][0]
        ##################################################################
        #
        # solve (cf _get_name_from_assignment())
        #

        row_ind, col_ind = optimize.linear_sum_assignment(cost)

        d_total_cost[id_symvector] = cost[row_ind, col_ind].sum()
        d_res_name[id_symvector] = {}

        ref_cell_name = atlas.cell_name
        for i, r in enumerate(row_ind):
            name = ref_cell_name[ref_cells[col_ind[i]]]
            # score = cost[r, col_ind[i]]
            # cell_name[flo_cells[r]] = cell_name.get(flo_cells[r], []) + [(name, score)]
            d_res_name[id_symvector][flo_cells[r]] = name

        if verbose:
            msg = "assignment of time #" + str(flo_time) + " of embryo '" + str(embryo.name) + "'"
            msg += " with time #" + str(ref_time) + " of atlas '" + str(atlas.name) + "'"
            msg += " has a total cost of {:.4f}".format(d_total_cost[id_symvector])
            monitoring.to_log_and_console("        ... " + msg)

        ##################################################################
        #
        # collect barycenters of embryo to be named
        #
        flo_pts = np.ones((4, len(row_ind)))
        ref_pts = np.ones((4, len(row_ind)))
        for i, r in enumerate(row_ind):
            flo_pts[:3, i] = embryo.cell_barycenter[flo_cells[r]]
            ref_pts[:3, i] = atlas.cell_barycenter[ref_cells[col_ind[i]]]

        mat = icp.lts_transformation(ref=ref_pts, flo=flo_pts, transformation_type="affine")
        d_det_affine_la[id_symvector] = np.linalg.det(mat)
        if verbose:
            msg = "assignment of time #" + str(flo_time) + " of embryo '" + str(embryo.name) + "'"
            msg += " with time #" + str(ref_time) + " of atlas '" + str(atlas.name) + "'"
            msg += " has a affine (la) matrix determinant {:.4f}".format(d_det_affine_la[id_symvector])
            monitoring.to_log_and_console("        ... " + msg)
        if d_det_affine_la[id_symvector] > 0:
            dp = np.dot(candidates[id_symvector, :], embryo.get_symmetry_axis_from_names(flo_time))
            if dp < 0.0:
                msg += "LA_WARNING: symmetry axis #" + str(id_symvector) + " has a POSITIVE "
                msg += " dot product with left-to-right vector of {:4f}".format(dp)
                monitoring.to_log_and_console("          " + msg)
        else:
            dp = np.dot(candidates[id_symvector, :], embryo.get_symmetry_axis_from_names(flo_time))
            if dp < 0.0:
                msg += "LA_WARNING: symmetry axis #" + str(id_symvector) + " has a NEGATIVE "
                msg += " dot product with left-to-right vector of {:4f}".format(dp)
                monitoring.to_log_and_console("          " + msg)
        ##################################################################
        #
        # collect barycenters of embryo to be named
        #
        row_argmin = np.argmin(cost, axis=0)
        col_argmin = np.argmin(cost, axis=1)
        col_min = np.min(cost, axis=1)
        cell_name = {}

        tmp = [(fc, ref_cells[col_argmin[i]]) for i, fc in enumerate(flo_cells) if i == row_argmin[col_argmin[i]]]
        pairs = []
        for i, fc in enumerate(flo_cells):
            if i == row_argmin[col_argmin[i]]:
                pairs += [(fc, ref_cells[col_argmin[i]])]
        if tmp != pairs:
            print("tmp = " + str (tmp))
            print("pairs = " + str(pairs))

        flo_pts = np.ones((4, len(pairs)))
        ref_pts = np.ones((4, len(pairs)))
        for i, p in enumerate(pairs):
            flo_pts[:3, i] = embryo.cell_barycenter[p[0]]
            ref_pts[:3, i] = atlas.cell_barycenter[p[1]]

        mat = icp.lts_transformation(ref=ref_pts, flo=flo_pts, transformation_type="affine")
        d_det_affine_min[id_symvector] = np.linalg.det(mat)
        if verbose:
            msg = "assignment of time #" + str(flo_time) + " of embryo '" + str(embryo.name) + "'"
            msg += " with time #" + str(ref_time) + " of atlas '" + str(atlas.name) + "'"
            msg += " has a affine (min) matrix determinant {:.4f}".format(d_det_affine_min[id_symvector])
            monitoring.to_log_and_console("        ... " + msg)
        if d_det_affine_min[id_symvector] > 0:
            dp = np.dot(candidates[id_symvector, :], embryo.get_symmetry_axis_from_names(flo_time))
            if dp < 0.0:
                msg += "MIN_WARNING: symmetry axis #" + str(id_symvector) + " has a POSITIVE "
                msg += " dot product with left-to-right vector of {:4f}".format(dp)
                monitoring.to_log_and_console("          " + msg)
        else:
            dp = np.dot(candidates[id_symvector, :], embryo.get_symmetry_axis_from_names(flo_time))
            if dp < 0.0:
                msg += "MIN_WARNING: symmetry axis #" + str(id_symvector) + " has a NEGATIVE "
                msg += " dot product with left-to-right vector of {:4f}".format(dp)
                monitoring.to_log_and_console("          " + msg)

    id_min = 0
    for id_symvector in d_total_cost:
        if d_total_cost[id_min] > d_total_cost[id_symvector]:
            id_min = id_symvector

    msg = "minimal cost is for symmetry axis #" + str(id_min)
    monitoring.to_log_and_console("      ... " + msg)

    if check_symetry_axis:
        dp = np.dot(candidates[id_min,:], embryo.get_symmetry_axis_from_names(flo_time))
        msg = ""
        if dp < 0.0:
            msg += "WARNING: symmetry axis #" + str(id_min) + " has a NEGATIVE "
            msg += " dot product with left-to-right vector of {:4f}".format(dp)
            monitoring.to_log_and_console("          " + msg)


    #
    # output
    #
    dict_cost = {}
    for fc in d_cost[id_min]:
        dict_cost[fc] = {}
        for rc in d_cost[id_min][fc]:
            dict_cost[fc][rc] = d_cost[id_min][fc][rc][0]

    return dict_cost


##############################################################
#
#
#
##############################################################


def naming_by_assignment(embryo, time_embryo, atlas, time_atlas, parameters, precomputeddata=None,
                         check_symetry_axis=False, verbose=True):
    proc = "naming_by_assignment"

    if isinstance(embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryo' variable: " + str(type(embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if isinstance(atlas, cembryo.Embryo) is False:
        msg = ": unexpected type for 'atlas' variable: " + str(type(atlas))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if not isinstance(parameters, ntparameters.NamingTimepointParameters):
        msg = "parameters expected type is 'NamingTimepointParameters' but was '"
        msg += str(type(parameters)) + "' instead"
        monitoring.to_log_and_console(proc + ": " + msg)
        return None, None, None

    properties = embryo.property_list()

    if 'rectified_cell_contact_surface' not in properties:
        monitoring.to_log_and_console(str(proc) + ": 'cell_contact_surface' was not in dictionary")
        return None, None, None

    if 'rectified_cell_volume' not in properties:
        monitoring.to_log_and_console(str(proc) + ": 'cell_volume' was not in dictionary")
        return None, None, None

    if 'rectified_cell_barycenter' not in properties:
        monitoring.to_log_and_console(str(proc) + ": 'cell_barycenter' was not in dictionary")
        return None, None, None

    #
    #
    #
    if verbose:
        msg = "assignment of time #" + str(time_embryo) + " of embryo '" + str(embryo.name) + "'"
        msg += " with time #" + str(time_atlas) + " of atlas '" + str(atlas.name) + "'"
        monitoring.to_log_and_console("   ... " + msg)

    #
    # list of cell ids
    #
    flo_cells = embryo.get_cells(time_embryo, propertyname='cell_contact_surface', background=False)
    ref_cells = atlas.get_cells(time_atlas, propertyname='cell_contact_surface', sortcriterium='cell_name')

    #
    #
    #
    computeddata = precomputeddata
    if precomputeddata is None:
        computeddata = {}

    precomputed_data_has_to_saved = False
    cell_similarity_key = parameters.matching_cell_similarity.lower()
    if cell_similarity_key not in computeddata:
        computeddata[cell_similarity_key] = {}
    if embryo.name not in computeddata[cell_similarity_key]:
        computeddata[cell_similarity_key][embryo.name] = {}
    if time_embryo not in computeddata[cell_similarity_key][embryo.name]:
        computeddata[cell_similarity_key][embryo.name][time_embryo] = {}
    if atlas.name not in computeddata[cell_similarity_key][embryo.name][time_embryo]:
        computeddata[cell_similarity_key][embryo.name][time_embryo][atlas.name] = {}

    #
    # get similarities from precomputed data or compute similarities
    #
    if time_atlas in computeddata[cell_similarity_key][embryo.name][time_embryo][atlas.name]:
        if verbose:
            msg = "cell similarity was already computed"
            monitoring.to_log_and_console("      ... " +msg)
        similarities = computeddata[cell_similarity_key][embryo.name][time_embryo][atlas.name][time_atlas]
    else:
        if parameters.matching_cell_similarity.lower() == 'sorted-contact':
            similarities = ccellsign.cell_signature(embryo, time_embryo, atlas, time_atlas,
                                                    cell_similarity='sorted-contact')
        elif parameters.matching_cell_similarity.lower() == 'winged-contact':
            similarities = cell_signature_from_wingedcontact(embryo, time_embryo, flo_cells, atlas, time_atlas,
                                                             ref_cells, parameters,
                                                             check_symetry_axis=check_symetry_axis, verbose=verbose)
            computeddata[cell_similarity_key][embryo.name][time_embryo][atlas.name][time_atlas] = similarities
            precomputed_data_has_to_saved = True
    #
    # get candidate names
    #
    if parameters.matching_cell_similarity.lower() == 'sorted-contact':
        #
        # matrix from dictionary
        #
        cost = np.zeros((len(flo_cells), len(ref_cells)))
        for i, fc in enumerate(flo_cells):
            for j, rc in enumerate(ref_cells):
                cost[i, j] = similarities[fc][rc]
        #
        # solve
        # res_name: dictionary indexed by cell-di, value = name
        #
        res_name = {}
        res_name, total_cost = _get_name_from_assignment(cost, embryo, flo_cells, atlas, ref_cells)

        #
        # 1. suppress the left/right [*,_], it may be wrong at cellular level
        # 2. put it globally (at embryo level) thanks to embryo symmetry direction and center of mass
        # 3. check the left/right by registering to the atlas
        #
        unlateralized_names = {c: res_name[c][:-1] for c in res_name}
        lateralized_names = _lateralize_names(unlateralized_names, embryo, time_embryo, parameters, verbose=verbose)
        res_name = _fix_laterality(lateralized_names, embryo, atlas, time_atlas, verbose=verbose)

    elif parameters.matching_cell_similarity.lower() == 'winged-contact':
        #
        # matrix from dictionary
        #
        cost = np.zeros((len(flo_cells), len(ref_cells)))
        for i, fc in enumerate(flo_cells):
            for j, rc in enumerate(ref_cells):
                cost[i, j] = similarities[fc][rc]
        res_name, total_cost = _get_name_from_doublemin_assignment(cost, embryo, flo_cells, atlas, ref_cells)
        # res_name, total_cost = _get_name_from_assignment(cost, embryo, flo_cells, atlas, ref_cells)

    if verbose:
        msg = "assignment of time #" + str(time_embryo) + " of embryo '" + str(embryo.name) + "'"
        msg += " with time #" + str(time_atlas) + " of atlas '" + str(atlas.name) + "'"
        msg += " has a total cost of {:.4f}".format(total_cost)
        monitoring.to_log_and_console("   ... " + msg)
        msg = str(len(res_name)) + " cells out of " + str(len(flo_cells))
        msg += " have been named from atlas '" + str(atlas.name) + "'"
        monitoring.to_log_and_console("       " + msg)

    if precomputeddata is None:
        return res_name, computeddata, False
    return res_name, computeddata, precomputed_data_has_to_saved

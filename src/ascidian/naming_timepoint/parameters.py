##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Mer 26 jui 2024 08:46:33 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

import astec.utils.common as common

import ascidian.components.parameters as cparameters

monitoring = common.Monitoring()


########################################################################################
#
# classes
# - computation parameters
#
########################################################################################

class EmbryoRegistrationParameters(common.PrefixedParameter):
    def __init__(self, prefix=None):

        common.PrefixedParameter.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        #
        # initialization of the 3D rigid transformation
        #
        doc = "\t To coregister two embryos, a first 3D rotation is done that align vectors issued from\n"
        doc += "\t the floating embryo (the embryo to be named) onto vectors issued from the reference\n"
        doc += "\t embryo (an already named embryo)\n"
        doc += "\t - 'sphere_wrt_z': an uniform sampling of 3D directions is done for the floating\n"
        doc += "\t    embryo (parameter 'direction_sphere_radius') while the 'z' direction is used\n"
        doc += "\t    for the reference embryo\n"
        doc += "\t - 'sphere_wrt_symaxis': an uniform sampling of 3D directions is done for the floating\n"
        doc += "\t    embryo (parameter 'direction_sphere_radius') while one (out of two) vector\n"
        doc += "\t    defining the symmetry axis direction is used for the reference embryo\n"
        doc += "\t    (to be used for test purposes)\n"
        doc += "\t - 'symaxis_wrt_symaxis': symmetry direction candidates are used for the floating \n"
        doc += "\t    embryo (parameters 'distribution_sphere_radius' and 'distribution_sigma') while\n"
        doc += "\t    one vector defining the symmetry axis (estimated from the cell name) is used for\n"
        doc += "\t    the reference embryo.\n"
        doc += "\t Note: Due to computational performances, the option 'symaxis_wrt_symaxis'\n"
        doc += "\t should be considered first. \n"
        self.doc['rotation_initialization'] = doc
        self.rotation_initialization = 'symaxis_wrt_symaxis'

        doc = "\t To get an uniform sampling of the 3d directions in space (options 'sphere_wrt_z'\n"
        doc += "\t and 'sphere_wrt_symaxis' of 'rotation_initialization'), a discrete sphere is build\n"
        doc += "\t and each point of the outer surface gives a sample. The larger the radius, the more\n"
        doc += "\t vectors and the higher computational time\n"
        doc += "\t radius = 2.0: 26 vectors, angle between neighboring vectors in [36.26, 60.0] degrees\n"
        doc += "\t radius = 2.3: 38 vectors, angle between neighboring vectors in [26.57, 54.74] degrees\n"
        doc += "\t radius = 2.5: 54 vectors, angle between neighboring vectors in [24.09, 43.09] degrees\n"
        doc += "\t radius = 2.9: 66 vectors, angle between neighboring vectors in [18.43, 43.09] degrees\n"
        doc += "\t radius = 3.0: 90 vectors, angle between neighboring vectors in [17.72, 43.09] degrees\n"
        doc += "\t radius = 3.5: 98 vectors, angle between neighboring vectors in [15.79, 32.51] degrees\n"
        doc += "\t radius = 3.7: 110 vectors, angle between neighboring vectors in [15.26, 32.51] degrees\n"
        doc += "\t radius = 3.8: 134 vectors, angle between neighboring vectors in [14.76, 29.50] degrees\n"
        doc += "\t radius = 4.0: 222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees\n"
        doc += "\t radius = 5.0: 222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees\n"
        self.doc['direction_sphere_radius'] = doc
        self.direction_sphere_radius = 2.5

        #
        # 2D rotation
        #
        doc = "\t Increment (in degrees) between two successive angles when enumerating\n"
        doc += "\t rotations along the z or the symmetry axis\n"
        self.doc['z_rotation_angle_increment'] = doc
        self.z_rotation_angle_increment = 15

        #
        # save transformations into a file
        #
        doc = "\t File name to save or to read the transformations between the embryos.\n"
        doc += "\t Useful when parsing name choice parameters (for test purposes then).\n"
        self.doc['transformation_filename'] = doc
        self.transformation_filename = None

        #
        #
        #
        doc = "\t The optimal transformation between two embryos is the one giving the smallest\n"
        doc += "\t residual value. The residual value of a transformation is the sum of the smallest\n"
        doc += "\t distances between paired points. This parameter gives the ratio of\n"
        doc += "\t points to be retained for the computation of the total residual, meaning that\n"
        doc += "\t the largest (1-pair_ratio_for_residual)*100 percent of point residuals are\n"
        doc += "\t discarded (considered as outliers).\n"
        self.doc['pair_ratio_for_residual'] = doc
        self.pair_ratio_for_residual = 0.8

        #
        #
        #
        doc = "\t Number of processors for parallelization\n"
        self.doc['processors'] = doc
        self.processors = 8

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# EmbryoRegistrationParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        self.varprint('rotation_initialization', self.rotation_initialization,
                      self.doc.get('rotation_initialization', None))

        self.varprint('direction_sphere_radius', self.direction_sphere_radius,
                      self.doc.get('direction_sphere_radius', None))

        self.varprint('z_rotation_angle_increment', self.z_rotation_angle_increment,
                      self.doc.get('z_rotation_angle_increment', None))

        self.varprint('transformation_filename', self.transformation_filename,
                      self.doc.get('transformation_filename', None))

        self.varprint('pair_ratio_for_residual', self.pair_ratio_for_residual,
                      self.doc.get('pair_ratio_for_residual', None))

        self.varprint('processors', self.processors, self.doc['processors'])
        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# EmbryoRegistrationParameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'rotation_initialization', self.rotation_initialization,
                      self.doc.get('rotation_initialization', None))

        self.varwrite(logfile, 'direction_sphere_radius', self.direction_sphere_radius,
                      self.doc.get('direction_sphere_radius', None))

        self.varwrite(logfile, 'z_rotation_angle_increment', self.z_rotation_angle_increment,
                      self.doc.get('z_rotation_angle_increment', None))

        self.varwrite(logfile, 'transformation_filename', self.transformation_filename,
                      self.doc.get('transformation_filename', None))

        self.varwrite(logfile, 'pair_ratio_for_residual', self.pair_ratio_for_residual,
                      self.doc.get('pair_ratio_for_residual', None))

        self.varwrite(logfile, 'processors', self.processors, self.doc['processors'])
        logfile.write("\n")

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):

        self.rotation_initialization = self.read_parameter(parameters, 'rotation_initialization',
                                                           self.rotation_initialization)

        self.direction_sphere_radius = self.read_parameter(parameters, 'direction_sphere_radius',
                                                           self.direction_sphere_radius)

        self.z_rotation_angle_increment = self.read_parameter(parameters, 'z_rotation_angle_increment',
                                                              self.z_rotation_angle_increment)

        self.transformation_filename = self.read_parameter(parameters, 'transformation_filename',
                                                           self.transformation_filename)

        self.pair_ratio_for_residual = self.read_parameter(parameters, 'pair_ratio_for_residual',
                                                           self.pair_ratio_for_residual)

        self.processors = self.read_parameter(parameters, 'processors', self.processors)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            monitoring.to_log_and_console("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)


##############################################################
#
#
#
##############################################################

class NamingAssignmentParameters(common.PrefixedParameter):
    def __init__(self, prefix=None):

        common.PrefixedParameter.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        #
        #
        #
        doc = "\t 'sorted-contact' 'winged-contact'\n"
        self.doc['matching_cell_similarity'] = doc
        self.matching_cell_similarity = 'winged-contact'
        doc = "\t integer\n"
        self.doc['matching_cell_similarity_iterations'] = doc
        self.matching_cell_similarity_iterations = 0

        #
        # save transformations into a file
        #
        doc = "\t File name to save or to read the cell similarities between the embryos.\n"
        doc += "\t Useful when parsing name choice parameters (for test purposes then).\n"
        self.doc['cell_similarity_filename'] = doc
        self.cell_similarity_filename = None

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# NamingAssignmentParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        self.varprint('matching_cell_similarity', self.matching_cell_similarity,
                      self.doc.get('matching_cell_similarity', None))
        self.varprint('matching_cell_similarity_iterations', self.matching_cell_similarity_iterations,
                      self.doc.get('matching_cell_similarity_iterations', None))

        self.varprint('cell_similarity_filename', self.cell_similarity_filename,
                      self.doc.get('cell_similarity_filename', None))
        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# NamingAssignmentParameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'matching_cell_similarity', self.matching_cell_similarity,
                      self.doc.get('matching_cell_similarity', None))
        self.varwrite(logfile, 'matching_cell_similarity_iterations', self.matching_cell_similarity_iterations,
                      self.doc.get('matching_cell_similarity_iterations', None))

        self.varwrite(logfile, 'cell_similarity_filename', self.cell_similarity_filename,
                      self.doc.get('cell_similarity_filename', None))

        logfile.write("\n")

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):

        self.matching_cell_similarity = self.read_parameter(parameters, 'matching_cell_similarity',
                                                            self.matching_cell_similarity)
        self.matching_cell_similarity_iterations = self.read_parameter(parameters,
                                                                       'matching_cell_similarity_iterations',
                                                                       self.matching_cell_similarity_iterations)

        self.cell_similarity_filename = self.read_parameter(parameters, 'cell_similarity_filename',
                                                            self.cell_similarity_filename)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            monitoring.to_log_and_console("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)

##############################################################
#
#
#
##############################################################


class NamingGraphParameters(common.PrefixedParameter):
    def __init__(self, prefix=None):

        common.PrefixedParameter.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        #
        #
        #
        doc = "\t 'sorted-contact' 'winged-contact'\n"
        self.doc['graph_edgevalue'] = doc
        self.graph_edgevalue = 'contact_surface'

        doc = "\t 'sorted-contact' 'winged-contact'\n"
        self.doc['graph_initialization'] = doc
        self.graph_initialization = 'barycenter'

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# NamingGraphParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        self.varprint('graph_edgevalue', self.graph_edgevalue, self.doc.get('graph_edgevalue', None))
        self.varprint('graph_initialization', self.graph_initialization, self.doc.get('graph_initialization', None))
        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# NamingGraphParameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'graph_edgevalue', self.graph_edgevalue, self.doc.get('graph_edgevalue', None))
        self.varwrite(logfile, 'graph_initialization', self.graph_initialization,
                      self.doc.get('graph_initialization', None))

        logfile.write("\n")

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):

        self.graph_edgevalue = self.read_parameter(parameters, 'graph_edgevalue', self.graph_edgevalue)
        self.graph_initialization = self.read_parameter(parameters, 'graph_initialization', self.graph_initialization)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            monitoring.to_log_and_console("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)


############################################################
#
#
#
############################################################

class NamingTimepointParameters(cparameters.BaseParameters, EmbryoRegistrationParameters, NamingAssignmentParameters,
                                NamingGraphParameters):
    ############################################################
    #
    # initialisation
    #
    ############################################################

    def __init__(self, prefix=None):

        if "doc" not in self.__dict__:
            self.doc = {}

        cparameters.monitoring.copy(monitoring)

        cparameters.BaseParameters.__init__(self, prefix=prefix)
        EmbryoRegistrationParameters.__init__(self, prefix=prefix)
        NamingAssignmentParameters.__init__(self, prefix=prefix)
        NamingGraphParameters.__init__(self, prefix=prefix)

        doc = "\t Input property file to be named. Must contain lineage, volumes, centers of mass\n"
        doc += "\t contact surfaces\n"
        self.doc['inputFile'] = doc
        self.inputFile = None
        doc = "\t Output property file."
        self.doc['outputFile'] = doc
        self.outputFile = None

        #
        doc = "\t Naming by transfer method:\n"
        doc += "\t - 'registration'\n"
        doc += "\t - 'assignment'\n"
        self.doc['naming_by_transfer'] = doc
        self.naming_by_transfer = 'registration'

        #
        doc = "\t Cell count of the developmental stade to be named\n"
        self.doc['cell_number'] = doc
        self.cell_number = 64

        doc = "\t Percentage of consensus to set a name to a cell\n"
        self.doc['minimal_consensus'] = doc
        self.minimal_consensus = 100

        doc = "\t True or False. After naming, check whether some names are duplicated, and,\n"
        doc += "\t if yes, remove them\n"
        self.doc['check_duplicate'] = doc
        self.check_duplicate = True

        #
        # for test:
        # names will be deleted, and tried to be rebuilt
        doc = "\t Input property file to be tested (must include cell names).\n"
        doc += "\t If given, 'inputFile' is ignored.\n"
        self.doc['testFile'] = doc
        self.testFile = None

        if 'generate_figure' in self.doc:
            doc = self.doc['generate_figure']
        else:
            doc = "\t if True, generate python files (prefixed by 'figures_') that generate figures.\n"
            doc += "\t Those files will be saved into the 'outputDir' directory.\n"
            doc += "\t 'generate_figure' can be\n"
            doc += "\t - a boolean value: if True, all figure files are generated; if False, none of them\n"
            doc += "\t - a string: if 'all', all figure files are generated; else, only the specified\n"
            doc += "\t   figure file is generated (see below for the list)\n"
            doc += "\t - a list of strings: if 'all' is in the list, all figure files are generated;\n"
            doc += "\t   else, only the specified figure files are generated (see below for the list)\n"
            doc += "\t List of figures:\n"
        doc += "\t 'naming_timepoint_wrt_atlases':\n"
        doc += "\t    plot leave-one-out results when naming one atlas out the atlasEmbryos with\n"
        doc += "\t    all combination of the other atlases (from combinations of 1 atlas to \n"
        doc += "\t    combinations of N-1 atlases). Results are plotted wrt the number of atlases\n"
        doc += "\t    used for naming.\n"
        doc += "\t 'atlas-naming-register-leave-one-out-wrt-cells':\n"
        doc += "\t    plot leave-one-out results when naming one atlas out the atlasEmbryos with all \n"
        doc += "\t    the other atlases (for a series of cell counts). Results are plotted wrt cell count.\n"
        self.doc['generate_figure'] = doc
        self.generate_figure = False

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# NamingTimepointParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        cparameters.BaseParameters.print_parameters(self)
        EmbryoRegistrationParameters.print_parameters(self)
        NamingAssignmentParameters.print_parameters(self)
        NamingGraphParameters.print_parameters(self)

        self.varprint('inputFile', self.inputFile, self.doc.get('inputFile', None))
        self.varprint('outputFile', self.outputFile, self.doc.get('outputFile', None))

        self.varprint('naming_by_transfer', self.naming_by_transfer, self.doc.get('naming_by_transfer', None))

        self.varprint('cell_number', self.cell_number, self.doc.get('cell_number', None))

        self.varprint('minimal_consensus', self.minimal_consensus, self.doc.get('minimal_consensus', None))

        self.varprint('check_duplicate', self.check_duplicate, self.doc.get('check_duplicate', None))

        self.varprint('testFile', self.testFile, self.doc.get('testFile', None))
        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# NamingTimepointParameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        cparameters.BaseParameters.write_parameters_in_file(self, logfile)
        EmbryoRegistrationParameters.write_parameters_in_file(self, logfile)
        NamingAssignmentParameters.write_parameters_in_file(self, logfile)
        NamingGraphParameters.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'inputFile', self.inputFile, self.doc.get('inputFile', None))
        self.varwrite(logfile, 'outputFile', self.outputFile, self.doc.get('outputFile', None))

        self.varwrite(logfile, 'naming_by_transfer', self.naming_by_transfer, self.doc.get('naming_by_transfer', None))

        self.varwrite(logfile, 'cell_number', self.cell_number, self.doc.get('cell_number', None))

        self.varwrite(logfile, 'minimal_consensus', self.minimal_consensus, self.doc.get('minimal_consensus', None))

        self.varwrite(logfile, 'check_duplicate', self.check_duplicate, self.doc.get('check_duplicate', None))

        self.varwrite(logfile, 'testFile', self.testFile, self.doc.get('testFile', None))

        logfile.write("\n")

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):

        cparameters.BaseParameters.update_from_parameters(self, parameters)
        EmbryoRegistrationParameters.update_from_parameters(self, parameters)
        NamingAssignmentParameters.update_from_parameters(self, parameters)
        NamingGraphParameters.update_from_parameters(self, parameters)

        self.inputFile = self.read_parameter(parameters, 'inputFile', self.inputFile)
        self.outputFile = self.read_parameter(parameters, 'outputFile', self.outputFile)

        self.naming_by_transfer = self.read_parameter(parameters, 'naming_by_transfer', self.naming_by_transfer)

        self.cell_number = self.read_parameter(parameters, 'cell_number', self.cell_number)

        self.minimal_consensus = self.read_parameter(parameters, 'minimal_consensus', self.minimal_consensus)

        self.check_duplicate = self.read_parameter(parameters, 'check_duplicate', self.check_duplicate)

        self.testFile = self.read_parameter(parameters, 'testFile', self.testFile)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            monitoring.to_log_and_console("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)

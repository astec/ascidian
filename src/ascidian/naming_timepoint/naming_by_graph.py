##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Lun 29 jul 2024 12:06:53 CEST
#
##############################################################
#
#
#
##############################################################

import sys

import numpy as np
import scipy.optimize as optimize
from sklearn.neighbors import NearestNeighbors

import astec.utils.common as common

import ascidian.core.cell_signature as ccellsign
import ascidian.components.embryo as cembryo

import ascidian.naming_timepoint.parameters as ntparameters
import ascidian.naming_timepoint.naming_by_registration as ntregistration
import ascidian.naming_timepoint.naming_by_assignment as ntassignment
import ascidian.naming_timepoint.utils as ntutils

monitoring = common.Monitoring()


##############################################################
#
#
#
##############################################################


def _print_line_entries(m):
    for x in range(m.shape[0]):
        n = [y for y in range(m.shape[1]) if m[x][y] > 0.0]
        print("line #{:3d} has {:d} elements ".format(x, len(n)) + str(n))


def _graph_from_contact(embryo, cells):
    proc = "_graph_from_contact"
    if isinstance(embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryo' variable: " + str(type(embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)
    if embryo.rectified_cell_contact_surface is None:
        msg = ": embryo do not have cell contact surfaces"
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)
    #
    # >>> m = np.zeros((2,3))
    # >>> m.shape
    # (2, 3)
    # >>> m
    # array([[0., 0., 0.],
    #       [0., 0., 0.]])
    #
    m = np.zeros((len(cells), len(cells)))
    id_from_cells = {c: i for i, c in enumerate(cells)}
    contact_surface = embryo.rectified_cell_contact_surface
    for c in cells:
        if c not in contact_surface:
            continue
        for d in contact_surface[c]:
            m[id_from_cells[c], id_from_cells[d]] = contact_surface[c][d]
            m[id_from_cells[d], id_from_cells[c]] = contact_surface[c][d]
    return m


def _graph_from_cellcenter(embryo, cells):
    proc = "_graph_from_cellcenter"
    if isinstance(embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryo' variable: " + str(type(embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)
    if embryo.rectified_cell_barycenter is None:
        msg = ": embryo do not have cell centers of mass"
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)
    #
    # >>> m = np.zeros((2,3))
    # >>> m.shape
    # (2, 3)
    # >>> m
    # array([[0., 0., 0.],
    #       [0., 0., 0.]])
    #
    m = np.zeros((len(cells), len(cells)))
    id_from_cells = {c: i for i, c in enumerate(cells)}
    cell_center = embryo.rectified_cell_barycenter
    for c in cells:
        if c not in cell_center:
            continue
        for d in cells:
            if d not in cell_center:
                continue
            if c == d:
                continue
            cd = np.linalg.norm(cell_center[c] - cell_center[d])
            m[id_from_cells[c], id_from_cells[d]] = cd
            m[id_from_cells[d], id_from_cells[c]] = cd
    return m


def _graph(embryo, cells, method):
    proc = "_graph"
    if method == 'contact_surface':
        return _graph_from_contact(embryo, cells)
    if method == 'cell_distance':
        return _graph_from_cellcenter(embryo, cells)

    msg = ": graph building method '" + str(method) + "' unknown or not handled yet"
    monitoring.to_log_and_console(str(proc) + msg)
    sys.exit(1)


##############################################################
#
#
#
##############################################################


#
# adapted from https://github.com/neurodata/goat/pkg/pkg/gmp/utils.py
#
def _check_init_input(P0):

    row_sum = np.sum(P0, axis=0)
    col_sum = np.sum(P0, axis=1)
    tol = 3e-2
    if np.isclose(row_sum, 1.0, atol=tol).all() is False:
        return False
    if np.isclose(col_sum, 1.0, atol=tol).all() is False:
        return False
    return True


#
# adapted from https://github.com/neurodata/goat/pkg/pkg/gmp/utils.py
#
def _doubly_stochastic(P, tol=1e-3, max_iter=1000):
    # Adapted from @btaba implementation
    # https://github.com/btaba/sinkhorn_knopp
    # of Sinkhorn-Knopp algorithm
    # https://projecteuclid.org/euclid.pjm/1102992505

    n = P.shape[0]
    c = 1 / P.sum(axis=0)
    r = 1 / (P @ c)
    P_eps = P

    for it in range(max_iter):
        if it % 100 == 0:  # only check every so often to speed up
            if (np.abs(P_eps.sum(axis=1) - 1) < tol).all() and (
                    np.abs(P_eps.sum(axis=0) - 1) < tol
            ).all():
                # All column/row sums ~= 1 within threshold
                break

        c = 1 / (r @ P)
        r = 1 / (P @ c)
        #         P_eps = r[:, None] * P * c
        P_eps = r.reshape((n, 1)) * P * c
    return P_eps


##############################################################
#
#
#
##############################################################


def _init_graph_name(embryo, flo_cells, atlas, ref_cells):
    #
    # lengths should be equal
    # first element is the background
    #

    P = np.zeros((len(flo_cells)-1, len(ref_cells)-1))

    if 'cell_name' in embryo.get_properties():
        flo_name = embryo.cell_name
    elif 'ground_truth_cell_name' in embryo.get_properties():
        flo_name = embryo.get_property('ground_truth_cell_name')
    else:
        return None

    if 'cell_name' in atlas.get_properties():
        ref_name = atlas.cell_name
    elif 'ground_truth_cell_name' in atlas.get_properties():
        ref_name = atlas.get_property('ground_truth_cell_name')
    else:
        return None

    for i, c in enumerate(flo_cells[1:]):
        if c not in flo_name:
            continue
        for j, d in enumerate(ref_cells[1:]):
            if d not in ref_name:
                continue
            if flo_name[c] == ref_name[d]:
                P[i, j] = 1
    _check_init_input(P)
    return P


def _init_graph_registration(embryo, time_embryo, flo_cells, atlas, time_atlas, ref_cells, parameters, verbose=True):

    computeddata = ntutils.get_precomputeddata(parameters, parameters.transformation_filename)

    if embryo.name not in computeddata:
        computeddata[embryo.name] = {}
    if time_embryo not in computeddata[embryo.name]:
        computeddata[embryo.name][time_embryo] = {}
    if atlas.name not in computeddata[embryo.name][time_embryo]:
        computeddata[embryo.name][time_embryo][atlas.name] = {}
    if time_atlas in computeddata[embryo.name][time_embryo][atlas.name]:
        if verbose:
            msg = "transformation was already computed"
            monitoring.to_log_and_console("      ... " + msg)
        transformation = computeddata[embryo.name][time_embryo][atlas.name][time_atlas]
    else:
        transformation = ntregistration.register_embryo(embryo, time_embryo, atlas, time_atlas, parameters,
                                                        verbose=False)
        if transformation is None:
            msg = "registration failed"
            monitoring.to_log_and_console("   ... " + msg)
            return None
        computeddata[embryo.name][time_embryo][atlas.name][time_atlas] = transformation
        ntutils.save_precomputeddata(computeddata, parameters.transformation_filename )

    # get reference barycenter coordinates
    local_ref_cells = ref_cells[1:]
    ref_barycenters = atlas.cell_barycenter
    bref = np.zeros((3, len(local_ref_cells)))
    for i, c in enumerate(local_ref_cells):
        bref[:, i] = ref_barycenters[c]

    # get floating barycenter coordinates
    local_flo_cells = flo_cells[1:]
    flo_barycenters = embryo.cell_barycenter
    bflo = np.zeros((3, len(local_flo_cells)))
    v = np.ones(4)
    for i, c in enumerate(local_flo_cells):
        v[:3] = flo_barycenters[c]
        bflo[:, i] = (np.matmul(transformation, v))[:3]

    # find reference barycenters that are closest to floating ones
    # i_flo_to_ref[i] is the index (in the list) of the reference cell closest
    # to the ith floating cell in cell_tflo list
    nghbref = NearestNeighbors(n_neighbors=1, algorithm='kd_tree').fit(bref.T)
    d_flo_to_ref, i_flo_to_ref = nghbref.kneighbors(bflo.T)
    i_flo_to_ref = i_flo_to_ref.flatten()

    # find floating barycenters that are closest to reference ones
    nghbflo = NearestNeighbors(n_neighbors=1, algorithm='kd_tree').fit(bflo.T)
    d_ref_to_flo, i_ref_to_flo = nghbflo.kneighbors(bref.T)
    i_ref_to_flo = i_ref_to_flo.flatten()

    P = np.zeros((len(local_flo_cells), len(local_ref_cells)))
    for i, cf in enumerate(local_flo_cells):
        j = i_flo_to_ref[i]
        if i_ref_to_flo[j] == i:
            # here centers of mass are closest to each other
            P[i, j] = 1

    tol = 3e-2
    n = P.shape[0]
    sumP = np.sum(P)
    #
    # do we get a permutation matrix?
    #
    if np.isclose(np.sum(P), n, atol=tol):
        return P

    rows = np.isclose(np.sum(P, axis=0), 1.0, atol=tol)
    cols = np.isclose(np.sum(P, axis=1), 1.0, atol=tol)
    #
    # matbool has True on cols/rows where there is a '1'
    #
    matbool = cols.reshape(n, 1) * (np.ones((1, n)) == 1) | (np.ones((n, 1)) == 1) * rows.reshape(1, n)
    #
    # by construction P is doubly stochastic
    #
    P = 1/float(n - sumP) * (matbool == False) + P * matbool

    return P


def _init_graph_wingedcontact(embryo, time_embryo, flo_cells, atlas, time_atlas, ref_cells, parameters, verbose=True):

    computeddata = ntutils.get_precomputeddata(parameters, parameters.cell_similarity_filename)

    computed_data_has_to_saved = False
    cell_similarity_key = parameters.matching_cell_similarity.lower()
    if cell_similarity_key not in computeddata:
        computeddata[cell_similarity_key] = {}
    if embryo.name not in computeddata[cell_similarity_key]:
        computeddata[cell_similarity_key][embryo.name] = {}
    if time_embryo not in computeddata[cell_similarity_key][embryo.name]:
        computeddata[cell_similarity_key][embryo.name][time_embryo] = {}
    if atlas.name not in computeddata[cell_similarity_key][embryo.name][time_embryo]:
        computeddata[cell_similarity_key][embryo.name][time_embryo][atlas.name] = {}

    #
    # get similarities from precomputed data or compute similarities
    #
    if time_atlas in computeddata[cell_similarity_key][embryo.name][time_embryo][atlas.name]:
        if verbose:
            msg = "cell similarity was already computed"
            monitoring.to_log_and_console("      ... " + msg)
        similarities = computeddata[cell_similarity_key][embryo.name][time_embryo][atlas.name][time_atlas]
    else:
        if parameters.matching_cell_similarity.lower() == 'sorted-contact':
            similarities = ccellsign.cell_signature(embryo, time_embryo, atlas, time_atlas,
                                                    cell_similarity='sorted-contact')
        elif parameters.matching_cell_similarity.lower() == 'winged-contact':
            similarities = ntassignment.cell_signature_from_wingedcontact(embryo, time_embryo, flo_cells, atlas,
                                                                          time_atlas, ref_cells, parameters,
                                                                          verbose=verbose)
            computeddata[cell_similarity_key][embryo.name][time_embryo][atlas.name][time_atlas] = similarities
            ntutils.save_precomputeddata(computeddata, parameters.cell_similarity_filename)

    local_ref_cells = ref_cells[1:]
    local_flo_cells = flo_cells[1:]

    P = np.zeros((len(local_flo_cells), len(local_ref_cells)))
    for i, fc in enumerate(local_flo_cells):
        for j, rc in enumerate(local_ref_cells):
            P[i, j] = (1.0 - similarities[fc][rc]) * (1.0 - similarities[fc][rc]) * (1.0 - similarities[fc][rc])

    return _doubly_stochastic(P)


def _init_graph(embryo, time_embryo, flo_cells, atlas, time_atlas, ref_cells, parameters, verbose=True):
    proc = "_init_graph"
    #
    # this one is just to check
    #
    if parameters.graph_initialization == 'name':
        return _init_graph_name(embryo, flo_cells, atlas, ref_cells)
    if parameters.graph_initialization == 'registration':
        return _init_graph_registration(embryo, time_embryo, flo_cells, atlas, time_atlas, ref_cells, parameters,
                                        verbose=verbose)
    if parameters.graph_initialization == 'winged-contact':
        return _init_graph_wingedcontact(embryo, time_embryo, flo_cells, atlas, time_atlas, ref_cells, parameters,
                                         verbose=verbose)

    msg = ": graph initialization method '" + str(parameters.graph_initialization) + "' unknown or not handled yet"
    monitoring.to_log_and_console(str(proc) + msg)
    sys.exit(1)


##############################################################
#
#
#
##############################################################


def naming_by_graph(embryo, time_embryo, atlas, time_atlas, parameters, verbose=True):
    proc = "naming_by_graph"

    if isinstance(embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryo' variable: " + str(type(embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if isinstance(atlas, cembryo.Embryo) is False:
        msg = ": unexpected type for 'atlas' variable: " + str(type(atlas))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    if not isinstance(parameters, ntparameters.NamingTimepointParameters):
        msg = "parameters expected type is 'NamingTimepointParameters' but was '"
        msg += str(type(parameters)) + "' instead"
        monitoring.to_log_and_console(proc + ": " + msg)
        return None, None, None

    properties = embryo.property_list()

    if 'rectified_cell_contact_surface' not in properties:
        monitoring.to_log_and_console(str(proc) + ": 'cell_contact_surface' was not in dictionary")
        return None, None, None

    if 'rectified_cell_volume' not in properties:
        monitoring.to_log_and_console(str(proc) + ": 'cell_volume' was not in dictionary")
        return None, None, None

    if 'rectified_cell_barycenter' not in properties:
        monitoring.to_log_and_console(str(proc) + ": 'cell_barycenter' was not in dictionary")
        return None, None, None

    #
    #
    #
    if verbose:
        msg = "graph matching of time #" + str(time_embryo) + " of embryo '" + str(embryo.name)
        msg += " with time #" + str(time_atlas) + " of atlas '" + str(atlas.name) + "'"
        monitoring.to_log_and_console("   ... " + msg)

    #
    # list of cell ids
    # background is the first element of the list
    #
    flo_cells = embryo.get_cells(time_embryo, propertyname='cell_contact_surface', background=True)
    ref_cells = atlas.get_cells(time_atlas, propertyname='cell_contact_surface', sortcriterium='cell_name',
                                background=True)
    #
    # type of graph
    #
    flo_graph = _graph(embryo, flo_cells, parameters.graph_edgevalue)
    ref_graph = _graph(atlas, ref_cells, parameters.graph_edgevalue)

    #
    # background on background
    #
    partial_match = np.zeros((1, 2))
    partial_match[0, 0] = 0
    partial_match[0, 1] = 0
    #
    # if flo_cells uses the same sort criterium as local_ref_cells[a] (sortcriterium='cell_name'),
    # and according that 'ground_truth_cell_name' are in the embryo properties,
    # thus identity is the perfect match (if the cell composition is the same than for the atlas)
    # P0 = np.identity(len(flo_cells)-1)
    #
    # P0 = np.identity(len(flo_cells) - 1)
    P0 = 'barycenter'
    if parameters.graph_initialization == 'barycenter':
        P0 = 'barycenter'
    elif parameters.graph_initialization == 'randomized':
        P0 = 'randomized'
    elif parameters.graph_initialization == 'identity':
        P0 = np.identity(len(flo_cells) - 1)
    else:
        P0 = _init_graph(embryo, time_embryo, flo_cells, atlas, time_atlas, ref_cells, parameters, verbose=verbose)

    trace = True

    if trace:
        Pt = _init_graph_name(embryo, flo_cells, atlas, ref_cells)
        best_l1 = np.sum(np.abs(flo_graph[1:, 1:] - Pt @ ref_graph[1:, 1:] @ Pt.T))
        initial_l1 = best_l1
        if isinstance(P0, np.ndarray):
            initial_l1 = np.sum(np.abs(flo_graph[1:, 1:] - P0 @ ref_graph[1:, 1:] @ P0.T))

    options = {'partial_match': partial_match, 'P0': P0, 'maximize': True}
    #
    # col_ind : Column indices corresponding to the best permutation found of the nodes of B.
    # fun : The objective value of the solution.
    # nit : The number of iterations performed during optimization.
    #
    #
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.quadratic_assignment.html
    # https://docs.scipy.org/doc/scipy-1.12.0/reference/generated/scipy.optimize.quadratic_assignment.html
    #
    res = optimize.quadratic_assignment(flo_graph, ref_graph, options=options)
    # print("res = " + str(res))
    # print("res.keys() = " + str(list(res.keys())))
    # print("col_ind = " + str(res['col_ind']))
    if trace:
        P = np.eye(len(flo_graph), dtype=int)[res['col_ind']]
        final_faq_l1 = np.sum(np.abs(flo_graph[1:, 1:] - P[1:, 1:] @ ref_graph[1:, 1:] @ P[1:, 1:].T))

    guess = np.array([np.arange(len(flo_graph)), res['col_ind']]).T
    options = {'partial_guess': guess, 'maximize': True}
    res = optimize.quadratic_assignment(flo_graph, ref_graph, method="2opt", options=options)

    if trace:
        P = np.eye(len(flo_graph), dtype=int)[res['col_ind']]
        final_2opt_l1 = np.sum(np.abs(flo_graph[1:, 1:] - P[1:, 1:] @ ref_graph[1:, 1:] @ P[1:, 1:].T))

    cell_name = {}
    for i, p in enumerate(res['col_ind']):
        if ref_cells[p] in atlas.cell_name:
            cell_name[flo_cells[i]] = atlas.cell_name[ref_cells[p]]

    if trace:
        if final_2opt_l1 > best_l1:
            print("FINAL = " + str(final_2opt_l1) + " > " + " BEST = " + str(best_l1))
        if final_2opt_l1 < best_l1:
            print("FINAL = " + str(final_2opt_l1) + " < " + " BEST = " + str(best_l1))
        if final_2opt_l1 > best_l1 or final_2opt_l1 < best_l1:
            if 'cell_name' in embryo.get_properties():
                ntutils.assess_naming(cell_name, embryo.get_property('cell_name'),
                                      time_digits_for_cell_id=embryo.time_digits_for_cell_id)
            elif 'ground_truth_cell_name' in embryo.get_properties():
                ntutils.assess_naming(cell_name, embryo.get_property('ground_truth_cell_name'),
                                      time_digits_for_cell_id=embryo.time_digits_for_cell_id)

    return cell_name

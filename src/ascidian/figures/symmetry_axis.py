##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Mar  7 nov 2023 11:08:54 CET
#
##############################################################
#
#
#
##############################################################

import sys
import math
import os
import time

import numpy as np

import astec.utils.common as common

import ascidian.ascidian.name as aname
import ascidian.symmetry.embryo as symemb

import ascidian.components.embryoset as cembryoset
import ascidian.components.parameters as cparameters

monitoring = common.Monitoring()


########################################################################################
#
# symmetry axis assessment
#
########################################################################################


def _print_candidates(candidates):
    for i, p in enumerate(candidates):
        msg = "#{:2d} [{:.2f} {:.2f} {:.2f}]".format(i, p['vector'][0], p['vector'][1], p['vector'][2])
        msg += " error: {:.2f}".format(p['error'])
        print("   " + msg)
        msg = "value: {:.3f} single_pairing: {:.3f}".format(p['value'], p['score_from_single_pairing'])
        msg += " multiple_pairing: {:.3f}".format(p['score_from_multiple_pairing'])
        msg += " symmetry_icp: {:.3f}".format(p['score_from_symmetry_icp'])
        print("      " + msg)


def _symmetry_axis_error_wrt_times(atlas, parameters):

    cells = list(atlas.cell_contact_surface.keys())
    cells = sorted(cells)
    cells_per_time = {}
    div = 10 ** atlas.time_digits_for_cell_id
    for c in cells:
        t = int(c) // div
        cells_per_time[t] = cells_per_time.get(t, 0) + 1

    unnamedcells = [c for c in atlas.cell_volume if c not in atlas.cell_name]
    unnamedtimes = set([int(c) // div for c in unnamedcells])
    if len(unnamedtimes) > 0:
        monitoring.to_log_and_console("    unnamed times = " + str(sorted(unnamedtimes)))
    alltimes = sorted(list(cells_per_time.keys()))

    times = []
    err_distribution = []
    index_from_distribution_value = []

    for i, t in enumerate(alltimes):

        start_time = time.localtime()

        if t in unnamedtimes:
            continue

        times += [t]

        symdir = atlas.get_symmetry_axis_from_names(t)
        #
        # vectors is a (N, 3) numpy array sorted by decreasing distribution value
        #
        vectors = atlas.get_direction_distribution_candidates(t, parameters)
        errors = []
        for p in vectors:
            #
            # the symmetry axis vector can be wither aligned with the
            # symmetry direction fron names or the opposite
            #
            err = math.acos(np.dot(p, symdir))
            if err > np.pi / 2:
                err = np.pi - err
            errors += [err * 180.0 / np.pi]

        end_time = time.localtime()
        msg = "    has processed " + str(i+1) + "/" + str(len(alltimes)-len(unnamedtimes)) + " time " + str(t)
        msg += " for similarity '" + str(parameters.symmetrical_cell_similarity) + "'"
        msg += " (in " + str(time.mktime(end_time) - time.mktime(start_time)) + "s)"
        monitoring.to_log_and_console(msg)

        minimal_error = min(errors)

        err_distribution += [minimal_error]
        index_from_distribution_value += [errors.index(minimal_error) + 1]

        atlas.del_property('direction_distribution')

    ncells = [cells_per_time[t] for t in times]

    return times, ncells, err_distribution, index_from_distribution_value


################################################################################
#
# plot the (angular) error on the computed symmetry axis
# and the rank of the distribution maximum closest to the ground truth symmetry axis
#
################################################################################


def figure_symmetry_axis_assessment(embryos, parameters):
    proc = "figure_symmetry_axis_assessment"

    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        sys.exit(1)
    if isinstance(parameters, cparameters.BaseParameters) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: " +
                                      str(type(parameters)))
        sys.exit(1)

    filename = 'figure_symmetry_axis_assessment'
    figname = 'symmetry_axis_assessment'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")

    ref_embryos = embryos.get_embryos()

    for a in ref_embryos:
        monitoring.to_log_and_console("... processing " + str(a))
        na = a.replace('-', '_')
        times, ncells, edist, idist = _symmetry_axis_error_wrt_times(ref_embryos[a], parameters)
        f.write("temporal_coefficients = " + str(ref_embryos[a].temporal_alignment) + "\n")
        f.write("times_" + na + " = " + str(times) + "\n")
        f.write("atimes_" + na + " = [temporal_coefficients[0] * i + temporal_coefficients[1]")
        f.write(" for i in times_" + na + "]\n")
        f.write("ncells_" + na + " = " + str(ncells) + "\n")
        f.write("edist_" + na + " = " + str(edist) + "\n")
        f.write("idist_" + na + " = " + str(idist) + "\n")
        f.write("\n")

    f.write("\n")
    f.write("fig, (ax1, ax2, ax3) = plt.subplots(ncols=3, sharey=True, figsize=(18, 6), constrained_layout=True)\n")
    for a in ref_embryos:
        na = a.replace('-', '_')
        f.write("p = ax1.plot(ncells_" + na + ", atimes_" + na + ", label='" + na + "')\n")
        f.write("p = ax2.plot(edist_" + na + ", atimes_" + na + ", label='" + na + "')\n")
        f.write("p = ax3.plot(idist_" + na + ", atimes_" + na + ", label='" + na + "')\n")
        f.write("\n")

    f.write("ax1.grid(True)\n")
    f.write("ax1.legend(prop={'size': 10})\n")
    f.write("ax1.set_xlabel('cell count')\n")
    f.write("ax1.set_ylabel('time')\n")
    f.write("ax1.set_title(\"cell count\", fontsize=15)\n")
    f.write("\n")
    f.write("ax2.grid(True)\n")
    f.write("ax2.set_xlabel('error (degrees)')\n")
    f.write("ax2.set_title(\"symmetry axis error of the closest vector\", fontsize=15)\n")
    f.write("\n")
    f.write("ax3.grid(True)\n")
    f.write("ax3.set_xlabel('rank')\n")
    f.write("ax3.set_title(\"rank of the closest vector\", fontsize=15)\n")
    f.write("\n")
    f.write("fig.suptitle('Symmetry axis candidate sorted by distribution value', fontsize=16)\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname)
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.close()


########################################################################################
#
#
#
########################################################################################


def _symmetry_cells(atlas, cell_similarity):

    cells = list(atlas.cell_contact_surface.keys())
    cells = sorted(cells)
    cells_per_time = {}
    div = 10 ** atlas.time_digits_for_cell_id
    for c in cells:
        t = int(c) // div
        cells_per_time[t] = cells_per_time.get(t, 0) + 1

    unnamedcells = [c for c in atlas.cell_volume if c not in atlas.cell_name]
    unnamedtimes = set([int(c) // div for c in unnamedcells])
    if len(unnamedtimes) > 0:
        monitoring.to_log_and_console("    unnamed times = " + str(sorted(unnamedtimes)))
    alltimes = sorted(list(cells_per_time.keys()))

    times = []

    div_cell = 10 ** atlas.time_digits_for_cell_id
    cell_contact_surface = atlas.cell_contact_surface

    tab_first = []
    tab_second = []
    tab_more = []
    tab_length = []

    for i, t in enumerate(alltimes):
        if t in unnamedtimes:
            continue
        start_time = time.localtime()
        times += [t]

        #
        # symcells is a dictionary indexed by cell id c
        # value is an array of tuples (cell id, score)
        # symc = sorted(symc, key=lambda x: x[1])
        #
        symcells, surfaces = symemb.symmetriccells(cell_contact_surface, atlas.cell_barycenter, t, cell_similarity,
                                                   time_digits_for_cell_id=atlas.time_digits_for_cell_id,
                                                   processors=8)

        first = 0
        second = 0
        more = 0

        cells = [c for c in cell_contact_surface if (int(c) // div_cell == t) and int(c) % div_cell != 1]
        names = [atlas.cell_name[c] for c in cells]

        for c in cells:
            symname = aname.get_symmetric_name(atlas.cell_name[c])
            if symname not in names:
                continue
            if atlas.cell_name[symcells[c][0][0]] == symname:
                first += 1
            elif atlas.cell_name[symcells[c][1][0]] == symname:
                second += 1
            else:
                more += 1

        tab_first += [first]
        tab_second += [second]
        tab_more += [more]
        tab_length += [len(cells)]

        end_time = time.localtime()
        msg = "    has processed " + str(i+1) + "/" + str(len(alltimes)-len(unnamedtimes)) + " time " + str(t)
        msg += " for similarity '" + str(cell_similarity) + "'"
        msg += " (in " + str(time.mktime(end_time) - time.mktime(start_time)) + "s)"
        monitoring.to_log_and_console(msg)

    return times, tab_first, tab_second, tab_more, tab_length


################################################################################
#
# plot the percentage of recognized symmetrical cells
# for the two cell similarity ('sorted-contact' and 'winged-contact')
#
################################################################################

def figure_symmetrical_cell_percentage(embryos, parameters):
    proc = "figure_symmetrical_cell_percentage"

    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        sys.exit(1)
    if isinstance(parameters, cparameters.FigureParameters) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: " +
                                      str(type(parameters)))
        sys.exit(1)

    filename = 'figure_symmetrical_cell_percentage'
    figname = 'symmetrical_cell_percentage'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")

    ref_embryos = embryos.get_embryos()

    for a in ref_embryos:
        monitoring.to_log_and_console("... processing " + str(a))
        na = a.replace('-', '_')
        times, ftab, stab, mtab, ltab = _symmetry_cells(ref_embryos[a], 'sorted-contact')
        f.write("\n")
        f.write("temporal_coefficients = " + str(ref_embryos[a].temporal_alignment) + "\n")
        f.write("times_" + na + " = " + str(times) + "\n")
        f.write("atimes_" + na + " = [temporal_coefficients[0] * i + temporal_coefficients[1]")
        f.write(" for i in times_" + na + "]\n")
        f.write("ftabs_" + na + " = " + str(ftab) + "\n")
        f.write("stabs_" + na + " = " + str(stab) + "\n")
        f.write("mtabs_" + na + " = " + str(mtab) + "\n")
        f.write("ltab_" + na + " = " + str(ltab) + "\n")

        f.write("\n")
        f.write("pftabs_" + na + " = [100.0*ftabs_" + na + "[i]")
        f.write("/ltab_" + na + "[i] for i in range(len(ltab_" + na + "))]\n")
        f.write("pstabs_" + na + " = [100.0*(ftabs_" + na + "[i] + stabs_" + na + "[i])")
        f.write("/ltab_" + na + "[i] for i in range(len(ltab_" + na + "))]\n")
        f.write("pmtabs_" + na + " = [100.0*(ftabs_" + na + "[i] + stabs_" + na + "[i] + mtabs_" + na + "[i])")
        f.write("/ltab_" + na + "[i] for i in range(len(ltab_" + na + "))]\n")

        times, ftab, stab, mtab, ltab = _symmetry_cells(ref_embryos[a], 'winged-contact')
        f.write("\n")
        f.write("ftabw_" + na + " = " + str(ftab) + "\n")
        f.write("stabw_" + na + " = " + str(stab) + "\n")
        f.write("mtabw_" + na + " = " + str(mtab) + "\n")

        f.write("\n")
        f.write("pftabw_" + na + " = [100.0*ftabw_" + na + "[i]")
        f.write("/ltab_" + na + "[i] for i in range(len(ltab_" + na + "))]\n")
        f.write("pstabw_" + na + " = [100.0*(ftabw_" + na + "[i] + stabw_" + na + "[i])")
        f.write("/ltab_" + na + "[i] for i in range(len(ltab_" + na + "))]\n")
        f.write("pmtabw_" + na + " = [100.0*(ftabw_" + na + "[i] + stabw_" + na + "[i] + mtabw_" + na + "[i])")
        f.write("/ltab_" + na + "[i] for i in range(len(ltab_" + na + "))]\n")

        f.write("\n")
        f.write("fig, (ax1, ax2, ax3) = plt.subplots(ncols=3, sharey=True, figsize=(18, 6), constrained_layout=True)\n")

        f.write("\n")
        f.write("p = ax1.plot(ltab_" + na + ", atimes_" + na + ", label='" + na + "')\n")
        f.write("p = ax2.plot(pftabs_" + na + ", atimes_" + na + ", label='rank = #1')\n")
        f.write("p = ax2.plot(pstabs_" + na + ", atimes_" + na + ", label='rank = #1 or #2')\n")
        f.write("p = ax2.plot(pmtabs_" + na + ", atimes_" + na + ", label='max. percentage')\n")
        f.write("p = ax3.plot(pftabw_" + na + ", atimes_" + na + ", label='rank = #1')\n")
        f.write("p = ax3.plot(pstabw_" + na + ", atimes_" + na + ", label='rank = #1 or #2')\n")
        f.write("p = ax3.plot(pmtabw_" + na + ", atimes_" + na + ", label='max. percentage')\n")

        f.write("\n")
        f.write("ax1.grid(True)\n")
        f.write("ax1.legend(prop={'size': 10})\n")
        f.write("ax1.set_xlabel('cell count')\n")
        f.write("ax1.set_ylabel('time')\n")
        f.write("ax1.set_title(\"cell count\", fontsize=15)\n")
        f.write("\n")
        f.write("ax2.grid(True)\n")
        f.write("ax2.set_xlabel('% cells')\n")
        f.write("ax2.set_title(\"rank of the symmetrical cell (sorted contact)\", fontsize=15)\n")
        f.write("ax2.set_xlim(0, 110)\n")
        f.write("ax2.legend(prop={'size': 10})\n")
        f.write("\n")
        f.write("ax3.grid(True)\n")
        f.write("ax3.set_xlabel('% cells')\n")
        f.write("ax3.set_title(\"rank of the symmetrical cell (winged contact)\", fontsize=15)\n")
        f.write("ax3.set_xlim(0, 110)\n")
        f.write("ax3.legend(prop={'size': 10})\n")
        f.write("\n")
        f.write("fig.suptitle(\"Symmetrical cell ranking for embryo '{:s}'\", fontsize=16)\n".format(a))

        f.write("\n")
        f.write("if savefig:\n")
        f.write("    plt.savefig('" + figname + "_" + na)
        if file_suffix is not None:
            f.write(file_suffix)
        f.write("'" + " + '.png')\n")
        f.write("else:\n")
        f.write("    plt.show()\n")
        f.write("    plt.close()\n")
        f.write("\n")

    f.close()

##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Mer 10 jul 2024 14:49:41 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

import astec.utils.common as common

import ascidian.ascidian.name as aname
import ascidian.components.division_atlas as cdivatlas
import ascidian.components.parameters as cparameters
import ascidian.figures.write_utils as writeutils

monitoring = common.Monitoring()


################################################################################
#
#
#
################################################################################


def figure_division_distance_histogram(atlases, parameters):
    """
    Computes cell-to-cell and division-to-division distance histograms
    Parameters
    ----------
    atlases
    parameters

    Returns
    -------

    """
    proc = "figure_division_distance_histogram"

    if isinstance(atlases, cdivatlas.DivisionAtlases) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'atlases' variable: " + str(type(atlases)))
        sys.exit(1)
    if isinstance(parameters, cparameters.FigureParameters) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    filename = 'figure_division_distance_histogram'
    figname = 'division_distance_histogram'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    #
    # get the references per mother_name
    #
    divisions = atlases.get_division()
    ccs = not atlases.use_common_neighborhood
    neighborhoods = atlases.get_cell_neighborhood()

    #
    # make generation-dependant calculation
    #
    division_per_generation = {}
    for n in divisions:
        generation = n.split('.')[0][1:]
        division_per_generation[generation] = division_per_generation.get(generation, []) + [n]
    for g in division_per_generation:
        division_per_generation[g] = sorted(division_per_generation[g])
        msg = "    - generation " + str(g) + ": " + str(len(division_per_generation[g])) + " divisions"
        monitoring.to_log_and_console(msg)

    ndivision = 0
    for g in division_per_generation:
        ndivision += len(division_per_generation[g])

    right_dscores_per_generation = {}
    wrong_dscores_per_generation = {}
    diff_dscores_per_generation = {}

    #
    # compute cell-to-cell distances for
    # - similar cells (cell of same name across embryos)
    # - sister cells (only across embryos)
    # and division-to-division distances
    #
    innersurfaces = []
    for g in division_per_generation:
        for n in division_per_generation[g]:
            d = aname.get_daughter_names(n)
            # if parameters.exclude_inner_surfaces:
            #     innersurfaces = [d[0], d[1]]
            #
            # if int(n.split('.')[0][1:]) > 6:
            #     continue
            for r1 in divisions[n]:
                for r2 in divisions[n]:
                    if r2 <= r1:
                        continue

                    div00 = cdivatlas.division_distance(neighborhoods[d[0]][r1], neighborhoods[d[1]][r1],
                                                        neighborhoods[d[0]][r2], neighborhoods[d[1]][r2],
                                                        change_contact_surfaces=ccs, innersurfaces=innersurfaces)

                    # daughter neighborhood have the same neighbors if atlases.get_use_common_neighborhood() is True
                    # there is then no need to change the contact surfaces
                    div01 = cdivatlas.division_distance(neighborhoods[d[0]][r1], neighborhoods[d[1]][r1],
                                                        neighborhoods[d[1]][r2], neighborhoods[d[0]][r2],
                                                        change_contact_surfaces=ccs, innersurfaces=innersurfaces)

                    # trace = n == "b7.0003_" or n == "b7.0008*"
                    # if trace:
                    #     print("division distance of " + n + " between " + r1 + " and " + r2 + ":")
                    #     print("\t right pairing = " + str(div00))
                    #     print("\t wrong pairing = " + str(div01))
                    right_dscores_per_generation[g] = right_dscores_per_generation.get(g, []) + [div00]
                    wrong_dscores_per_generation[g] = wrong_dscores_per_generation.get(g, []) + [div01]
                    diff_dscores_per_generation[g] = diff_dscores_per_generation.get(g, []) + [div01-div00]

    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")
    f.write("import scipy.stats as stats\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    writeutils.write_dict_of_arrays(f, "right_dscores_per_generation", right_dscores_per_generation, length=3)
    f.write("right_dscores = []\n")
    f.write("for g in right_dscores_per_generation:\n")
    f.write("    right_dscores += right_dscores_per_generation[g]\n")

    f.write("\n")
    writeutils.write_dict_of_arrays(f, "wrong_dscores_per_generation", wrong_dscores_per_generation, length=3)
    f.write("wrong_dscores = []\n")
    f.write("for g in wrong_dscores_per_generation:\n")
    f.write("    wrong_dscores += wrong_dscores_per_generation[g]\n")

    f.write("\n")
    f.write("gens = set(right_dscores_per_generation.keys())\n")
    f.write("gens.intersection(set(wrong_dscores_per_generation.keys()))\n")
    f.write("gens = sorted(list(gens))\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(7.5, 7.5), constrained_layout=True)\n")
    f.write("labels = ['right pairing', 'wrong pairing']\n")
    f.write("ax.hist([right_dscores, wrong_dscores], 100, histtype='bar', label=labels)\n")
    f.write("ax.legend(prop={'size': 10})\n")
    f.write("ax.set_title('division-to-division distances in atlases', fontsize=15)\n")
    f.write("ax.tick_params(labelsize=10)\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_1")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.write("\n")
    f.write("fig, axs = plt.subplots(2, 2, figsize=(7.5, 7.5), sharex=True, sharey=True, constrained_layout=True)\n")
    f.write("labels = ['right pairing', 'wrong pairing']\n")
    f.write("if len(gens) >= 1:\n")
    f.write("    axs[0, 0].hist([right_dscores_per_generation[gens[0]], wrong_dscores_per_generation[gens[0]]]")
    f.write(", 100, histtype='bar', label=labels)\n")
    f.write("    axs[0, 0].legend(prop={'size': 10})\n")
    f.write("    title = 'generation = ' + str(gens[0])\n")
    f.write("    axs[0, 0].set_title(title, fontsize=10)\n")
    f.write("    axs[0, 0].tick_params(labelsize=10)\n")
    f.write("if len(gens) >= 2:\n")
    f.write("    axs[0, 1].hist([right_dscores_per_generation[gens[1]], wrong_dscores_per_generation[gens[1]]]")
    f.write(", 100, histtype='bar', label=labels)\n")
    f.write("    axs[0, 1].legend(prop={'size': 10})\n")
    f.write("    title = 'generation = ' + str(gens[1])\n")
    f.write("    axs[0, 1].set_title(title, fontsize=10)\n")
    f.write("    axs[0, 1].tick_params(labelsize=10)\n")
    f.write("if len(gens) >= 3:\n")
    f.write("    axs[1, 0].hist([right_dscores_per_generation[gens[2]], wrong_dscores_per_generation[gens[2]]]")
    f.write(", 100, histtype='bar', label=labels)\n")
    f.write("    axs[1, 0].legend(prop={'size': 10})\n")
    f.write("    title = 'generation = ' + str(gens[2])\n")
    f.write("    axs[1, 0].set_title(title, fontsize=10)\n")
    f.write("    axs[1, 0].tick_params(labelsize=10)\n")
    f.write("if len(gens) >= 4:\n")
    f.write("    axs[1, 1].hist([right_dscores_per_generation[gens[3]], wrong_dscores_per_generation[gens[3]]]")
    f.write(", 100, histtype='bar', label=labels)\n")
    f.write("    axs[1, 1].legend(prop={'size': 10})\n")
    f.write("    title = 'generation = ' + str(gens[3])\n")
    f.write("    axs[1, 1].set_title(title, fontsize=10)\n")
    f.write("    axs[1, 1].tick_params(labelsize=10)\n")
    f.write("fig.suptitle('division-to-division distances in atlases', fontsize=15)\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_2")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.write("\n")
    writeutils.write_dict_of_arrays(f, "diff_dscores_per_generation", diff_dscores_per_generation, length=4)
    f.write("diff_dscores = []\n")
    f.write("for g in diff_dscores_per_generation:\n")
    f.write("    diff_dscores += diff_dscores_per_generation[g]\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(7.5, 7.5), constrained_layout=True)\n")
    f.write("ax.hist(diff_dscores, 100, histtype='bar')\n")
    f.write("ax.set_title('difference of division-to-division distances (wrong pairing - right one)', fontsize=15)\n")
    f.write("ax.tick_params(labelsize=10)\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_3")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.write("\n")

    f.close()

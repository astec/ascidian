##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Ven 21 jui 2024 10:36:27 CEST
#
##############################################################
#
#
#
##############################################################


def write_raw_array(f, a, length=4):
    form = "{:1." + str(length) + "f}"
    last = len(a) - 1
    f.write("[")
    for i, v in enumerate(a):
        f.write(form.format(v))
        if i < last:
            f.write(", ")
    f.write("]")


def write_array(f, varname, a, length=4):
    f.write(str(varname) + " = ")
    write_raw_array(f, a, length=length)
    f.write("\n")


def write_dict_of_arrays(f, varname, a, length=4):
    last = len(a) - 1
    f.write(str(varname) + " = {")
    for i, v in enumerate(a):
        f.write(str(v) + ": ")
        write_raw_array(f, a[v], length=length)
        if i < last:
            f.write(", ")
    f.write("}\n")


def write_tuple_array(f, a, length=4):
    form = "{:1." + str(length) + "f}"
    last = len(a) - 1
    f.write("[")
    for i, v in enumerate(a):
        lt = len(v) - 1
        f.write("(")
        for j, w in enumerate(v):
            f.write(form.format(w))
            if j < lt:
                f.write(", ")
        f.write(")")
        if i < last:
            f.write(", ")
    f.write("]")


def write_dict_of_list(f, varname, d, length=3):
    form = "{:." + str(length) + "f}"
    f.write(str(varname) + " = ")
    f.write("{")
    ks = list(d.keys())
    for j, k in enumerate(ks):
        f.write(str(k) + ": [")
        for i, e in enumerate(d[k]):
            f.write(form.format(e))
            if i+1 < len(d[k]):
                f.write(", ")
        f.write("]")
        if j + 1 < len(ks):
            f.write(", ")
    f.write("}")
    f.write("\n")


def write_dict_of_tuple_arrays(f, varname, a, length=4):
    last = len(a) - 1
    f.write(str(varname) + " = {")
    for i, v in enumerate(a):
        f.write("'" + str(v) + "': ")
        write_tuple_array(f, a[v], length=length)
        if i < last:
            f.write(", ")
    f.write("}\n")


def write_nparray(f, varname, a, length=4):
    form = "{:1." + str(length) + "f}"
    f.write(str(varname) + " = np.array([")
    for j in range(a.shape[0]):
        f.write("[")
        for i in range(a.shape[1]):
            f.write(form.format(a[j, i]))
            if i < a.shape[1]-1:
                f.write(", ")
        f.write("]")
        if j < a.shape[0] - 1:
            f.write(",\n")
    f.write("])\n")


def write_dict_dict(f, varname, a, length=4):
    form = "{:1." + str(length) + "f}"
    f.write(str(varname) + " = {")
    for i, v in enumerate(a):
        f.write("'" + str(v) + "': {")
        for j, w in enumerate(a[v]):
            f.write("'" + str(w) + "': ")
            f.write(form.format(a[v][w]))
            if j < len(a[v]) - 1:
                f.write(", ")
        f.write("}")
        if i < len(a) - 1:
            f.write(", ")
    f.write("}\n")

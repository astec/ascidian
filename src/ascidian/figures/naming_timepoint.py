##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Ven 16 jui 2023 13:16:58 CEST
#
##############################################################
#
#
#
##############################################################

import copy
import itertools
import os
import pickle as pkl
import sys
import multiprocessing

import astec.utils.common as common

import ascidian.components.embryo as cembryo
import ascidian.components.embryoset as cembryoset
import ascidian.core.naming_utils as cnamutils

import ascidian.naming_timepoint.parameters as ntparameters
import ascidian.naming_timepoint.naming_by_registration as ntregistration
import ascidian.naming_timepoint.naming_by_assignment as ntassignment
import ascidian.naming_timepoint.naming_by_graph as ntgraph
import ascidian.naming_timepoint.utils as ntutils

monitoring = common.Monitoring()


#######################################################################################
#
# leave-one-out tests
#
########################################################################################


def _proc_success(local_parameters):
    #
    # parameters
    #
    par_test_atlas, par_ref_atlases, names, parameters, par = local_parameters
    a, test_atlas, time_embryo = par_test_atlas
    ref_atlas_list, ref_atlases, time_atlases = par_ref_atlases
    min_length_combination, verbose = par

    #
    # minimal properties for test atlas
    #
    time_digits_for_cell_id = test_atlas.time_digits_for_cell_id

    right_naming = {}
    wrong_naming = {}
    unnamed_naming = {}

    monitoring.to_log_and_console("   *** compare " + str(a) + " versus " + str(ref_atlas_list))
    for i in range(min_length_combination, len(ref_atlas_list) + 1):
        subset_atlas = list(itertools.combinations(ref_atlas_list, i))
        msg = "    ** compare " + str(a)
        msg += " versus " + str(len(subset_atlas)) + " combinations of " + str(i) + " atlases"
        monitoring.to_log_and_console(msg)
        for j, s in enumerate(subset_atlas):
            msg = "     * " + str(j + 1) + "/" + str(len(subset_atlas)) + " compare " + str(a)
            msg += " versus " + str(s)
            monitoring.to_log_and_console(msg)

            #
            # build references atlases
            # collect names from individual assignment
            #
            candidates = {}
            for b in s:
                for c in names[a][b]:
                    candidates[c] = candidates.get(c, []) + [names[a][b][c]]

            #
            # get consensual names
            # ntregistration.set_names_from_candidates just sets prop['cell_name']
            #
            tmp_names = ntutils.set_names_from_candidates(candidates, parameters.minimal_consensus, verbose=verbose)

            #
            # eventually remove some names
            # ntutils.remove_duplicated_names() just uses prop['cell_name']
            #
            if parameters.check_duplicate:
                if verbose:
                    monitoring.to_log_and_console("... removing duplicates")
                tmp_names = ntutils.remove_duplicated_names(tmp_names, time_digits_for_cell_id=time_digits_for_cell_id,
                                                            verbose=verbose)

            #
            #
            #
            right, wrong, unnamed, not_in =  ntutils.assess_naming(tmp_names, test_atlas.cell_name,
                                                                   time_digits_for_cell_id=time_digits_for_cell_id,
                                                                   verbose=verbose)

            right_naming[i] = right_naming.get(i, []) + [right]
            wrong_naming[i] = wrong_naming.get(i, []) + [wrong]
            unnamed_naming[i] = unnamed_naming.get(i, []) + [unnamed]

    return right_naming, wrong_naming, unnamed_naming


#######################################################################################
#
#
#
########################################################################################


def _get_success_timepoint(test_atlases, test_times, ref_atlases, ref_times, parameters, min_length_combination=1,
                           prefix_symmetric="sym-", verbose=True):
    #
    # embryos/atlases to be tested against the references
    #
    test_atlases_list = list(test_times.keys())
    for a in test_atlases:
        if str(a)[0:len(prefix_symmetric)] == prefix_symmetric:
            monitoring.to_log_and_console("   ... remove symmetrical embryo '" + str(a) + "' from test embryos")
            test_atlases_list.remove(a)

    #
    # precomputed stuff (if any)
    # - transformations (parameters.naming_by_transfer == 'registration')
    # - cell similarities ((parameters.naming_by_transfer == 'assignment')
    #
    precomputed_filename = None
    if parameters.naming_by_transfer.lower() == 'registration':
        precomputed_filename = parameters.transformation_filename
    elif parameters.naming_by_transfer.lower() == 'assignment':
        precomputed_filename = parameters.cell_similarity_filename
    elif parameters.naming_by_transfer.lower() == 'graph':
        pass
    else:
        pass

    precomputeddata = ntutils.get_precomputeddata(parameters, precomputed_filename)
    precomputed_data_has_to_saved = False

    #
    # get candidate names
    # names = dictionary indexed by cell id, value = list of cell names
    # time_embryo = integer, time where the computation takes place
    # time_atlases = dictionary indexed by atlas name, value = time where the computation takes place
    #
    ntregistration.monitoring.copy(monitoring)
    ntassignment.monitoring.copy(monitoring)
    ntutils.monitoring.copy(monitoring)

    names = {}
    for a in test_atlases_list:
        names[a] = {}

        for b in ref_times:
            sym_a = prefix_symmetric + str(a)
            if a == b or sym_a == b:
                continue
            monitoring.to_log_and_console("   *** assign names to " + str(a) + " from " + str(b))

            data_has_to_saved = False

            if parameters.naming_by_transfer.lower() == 'registration':
                output = ntregistration.naming_by_registration(test_atlases[a], test_times[a], ref_atlases[b],
                                                               ref_times[b], parameters,
                                                               precomputeddata=precomputeddata, verbose=False)
                names[a][b], precomputeddata, data_has_to_saved = output
            elif parameters.naming_by_transfer.lower() == 'assignment':
                output = ntassignment.naming_by_assignment(test_atlases[a], test_times[a], ref_atlases[b], ref_times[b],
                                                           parameters, precomputeddata=precomputeddata,
                                                           check_symetry_axis=True, verbose=True)
                names[a][b], precomputeddata, data_has_to_saved = output
            elif parameters.naming_by_transfer.lower() == 'graph':
                names[a][b] = ntgraph.naming_by_graph(test_atlases[a], test_times[a], ref_atlases[b], ref_times[b],
                                                      parameters, verbose=True)
            else:
                msg = "naming by transfer method '" + str(parameters.naming_by_transfer) + "' not handled yet"
                monitoring.to_log_and_console(str(proc) + ": " + msg)
                monitoring.to_log_and_console("Exiting")
                sys.exit(1)

            if names[a][b] is None:
                monitoring.to_log_and_console("... naming with respect to atlas '" + str(a) + "' failed")
                del names[a][b]
                continue

            if data_has_to_saved is True:
                precomputed_data_has_to_saved = True

        #
        # save computed similarities if required
        # do it after comparing embryo 'a' to all references
        #
        if precomputed_data_has_to_saved:
            ntutils.save_precomputeddata(precomputeddata, precomputed_filename)

    #
    # set names
    #
    mapping = []

    for a in test_atlases_list:
        # reference atlas list
        ref_atlas_list = list(ref_times.keys())
        if a in ref_atlas_list:
            ref_atlas_list.remove(a)
        if prefix_symmetric + str(a) in ref_atlas_list:
            ref_atlas_list.remove(prefix_symmetric + str(a))

        par_test_atlas = [a, test_atlases[a], test_times[a]]
        par_ref_atlases = [ref_atlas_list, ref_atlases, ref_times]
        par = [min_length_combination, verbose]
        mapping.append((par_test_atlas, par_ref_atlases, names, parameters, par))

    right_naming = {}
    wrong_naming = {}
    unnamed_naming = {}

    outputs = []

    #
    # there is no gain now with multiprocessing
    #
    # if min_length_combination == 1:
    #     pool = multiprocessing.Pool(processes=parameters.processors)
    #     outputs = pool.map(_proc_success, mapping)
    #     pool.close()
    #     pool.terminate()
    # else:
    for m in mapping:
        outputs += [_proc_success(m)]

    for local_right_naming, local_wrong_naming, local_unnamed_naming in outputs:
        for i in local_right_naming:
            right_naming[i] = right_naming.get(i, []) + local_right_naming[i]
        for i in local_wrong_naming:
            wrong_naming[i] = wrong_naming.get(i, []) + local_wrong_naming[i]
        for i in local_unnamed_naming:
            unnamed_naming[i] = unnamed_naming.get(i, []) + local_unnamed_naming[i]

    return right_naming, wrong_naming, unnamed_naming


############################################################
#
#
#
############################################################



def _get_success_cellcount(embryos, ncells, parameters, test_embryos=None, min_length_combination=1, verbose=False):
    #
    # select the atlases to be used
    # remove the ones that do not contain the targeted number of cells
    msg = "*** Naming transfer assessment by '" + str(parameters.naming_by_transfer) + "'"
    msg += " at " + str(ncells) + " cells"
    monitoring.to_log_and_console(msg)

    ref_atlases = embryos.get_embryos()
    ref_atlases_times = {}
    for a in ref_atlases:
        #
        # get the time range of the floating embryo with the specified number of cells
        #
        ref_cells, ref_time, ref_interval = ntutils.get_time_from_ncells(ref_atlases[a], ncells,
                                                                         name=a, change_cell_number=False,
                                                                         verbose=False)
        if ref_cells is None:
            msg = "requested cell count of " + str(ncells) + " is not in the found "
            msg += "cell range of atlas '" + str(a) + "'.\n"
            msg += "       skip reference atlas '" + str(a) + "'"
            monitoring.to_log_and_console("   ... " + msg)
            continue

        ref_atlases_times[a] = ref_time

    #
    # do the same for test embryos
    #
    if test_embryos is not None:
        test_atlases = test_embryos.get_embryos()
        test_atlases_times = {}
        for a in test_atlases:
            #
            # get the time range of the floating embryo with the specified number of cells
            #
            ref_cells, ref_time, ref_interval = ntutils.get_time_from_ncells(test_atlases[a], ncells,
                                                                             name=a, change_cell_number=False)
            if ref_cells is None:
                msg = "requested cell count of " + str(ncells) + " is not in the found "
                msg += "cell range of atlas '" + str(a) + "'.\n"
                msg += "\t skip test atlas '" + str(a) + "'"
                monitoring.to_log_and_console("   ... " + msg)
                continue

            test_atlases_times[a] = ref_time

    else:
        test_atlases = ref_atlases
        test_atlases_times = ref_atlases_times

    min_length = min_length_combination
    if min_length_combination < 0:
        min_length = len(ref_atlases_times) - 1
    return _get_success_timepoint(test_atlases, test_atlases_times, ref_atlases, ref_atlases_times, parameters,
                                  min_length_combination=min_length, prefix_symmetric=embryos.symprefix,
                                  verbose=verbose)


############################################################
#
#
#
############################################################


def figure_naming_timepoint_wrt_atlases(embryos, parameters, test_embryos=None):
    #
    # plot the count of right, wrong and unknown wrt the number of used atlases
    # for a given cell count
    #
    proc = "figure_naming_timepoint_wrt_atlases"

    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        monitoring.to_log_and_console("\t do not generate figure")
        return
    if test_embryos is not None:
        if isinstance(embryos, cembryoset.EmbryoSet) is False:
            monitoring.to_log_and_console(str(proc) + ": unexpected type for 'test_embryos' variable: "
                                          + str(type(test_embryos)))
            monitoring.to_log_and_console("\t do not generate figure")
            return
    if isinstance(parameters, ntparameters.NamingTimepointParameters) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    #
    # right_naming, wrong_naming, unnamed_naming are dictionaries
    # key = number of used atlases
    #     from 1 to len(embryos) if test_embryos is not None
    #     from 1 to len(embryos)-1 if test_embryos is None
    # value = array of integers (cell counts)
    #
    output =  _get_success_cellcount(embryos, parameters.cell_number, parameters, test_embryos=test_embryos,
                                     min_length_combination=1, verbose=False)
    right_naming, wrong_naming, unnamed_naming = output

    filename = 'figure_naming_timepoint_wrt_atlases'
    figname = 'naming_timepoint_wrt_atlases'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)
    #
    #
    #
    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    f.write("right_naming = " + str(right_naming) + "\n")
    f.write("wrong_naming = " + str(wrong_naming) + "\n")
    f.write("unnamed_naming = " + str(unnamed_naming) + "\n")
    f.write("labels = sorted(list(right_naming.keys()))\n")

    f.write("\n")
    f.write("right_list = []\n")
    f.write("wrong_list = []\n")
    f.write("unnamed_list = []\n")
    f.write("for i, l in enumerate(labels):\n")
    f.write("    right_list += [right_naming[l]]\n")
    f.write("    wrong_list += [wrong_naming[l]]\n")
    f.write("    unnamed_list += [unnamed_naming.get(l, [])]\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(16, 6.5), constrained_layout=True)\n")
    f.write("rbox = ax.boxplot(right_list, patch_artist=False)\n")
    f.write("wbox = ax.boxplot(wrong_list, patch_artist=False)\n")

    f.write("\n")
    f.write("rmed = [b.get_ydata()[0] for b in rbox['medians']]\n")
    f.write("wmed = [b.get_ydata()[0] for b in wbox['medians']]\n")
    f.write("ax.plot(labels, rmed, color='blue', label='right names')\n")
    f.write("ax.plot(labels, wmed, color='cyan', label='wrong names')\n")

    f.write("\n")
    f.write("ax.yaxis.grid(True)\n")
    f.write("ax.set_xticks([y + 1 for y in range(len(right_naming))])\n")
    f.write("ax.set_xlabel('Number of atlases')\n")
    f.write("ax.set_ylabel('#cells')\n")
    f.write("ax.legend()\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_wrong")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")
    f.write("\n")

    f.write("fig, ax = plt.subplots(figsize=(16, 6.5), constrained_layout=True)\n")
    f.write("rbox = ax.boxplot(right_list, patch_artist=False)\n")
    f.write("ubox = ax.boxplot(unnamed_list, patch_artist=False)\n")

    f.write("\n")
    f.write("rmed = [b.get_ydata()[0] for b in rbox['medians']]\n")
    f.write("umed = [b.get_ydata()[0] for b in ubox['medians']]\n")
    f.write("ax.plot(labels, rmed, color='blue', label='right names')\n")
    f.write("ax.plot(labels, umed, color='cyan', label='unnamed cells')\n")

    f.write("\n")
    f.write("ax.yaxis.grid(True)\n")
    f.write("ax.set_xticks([y + 1 for y in range(len(right_naming))])\n")
    f.write("ax.set_xlabel('Number of atlases')\n")
    f.write("ax.set_ylabel('#cells')\n")
    f.write("ax.legend()\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_unnamed")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.close()


def figure_naming_timepoint_wrt_cellcounts(embryos, parameters, test_embryos=None):
    #
    # plot the count of right, wrong and unknown wrt cell counts
    #
    proc = "figure_naming_timepoint_wrt_cellcounts"

    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        monitoring.to_log_and_console("\t do not generate figure")
        return
    if isinstance(parameters, ntparameters.NamingTimepointParameters) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: " +
                                      str(type(parameters)))
        sys.exit(1)

    ncells = [64, 76, 100, 112, 130, 150, 184, 218, 280]

    right_naming = {}
    wrong_naming = {}
    unnamed_naming = {}

    for n in ncells:
        output = _get_success_cellcount(embryos, n, parameters, test_embryos=test_embryos, min_length_combination=-1,
                                        verbose=False)
        right_naming[n], wrong_naming[n], unnamed_naming[n] = output

    filename = 'figure_naming_timepoint_wrt_cellcounts'
    figname = 'naming_timepoint_wrt_cellcounts'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)
    #
    #
    #
    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    f.write("right_naming = " + str(right_naming) + "\n")
    f.write("wrong_naming = " + str(wrong_naming) + "\n")
    f.write("unnamed_naming = " + str(unnamed_naming) + "\n")
    f.write("ncells = sorted(list(right_naming.keys()))\n")

    f.write("\n")
    f.write("right_list = []\n")
    f.write("wrong_list = []\n")
    f.write("unnamed_list = []\n")
    f.write("for n in ncells:\n")
    f.write("    k = list(right_naming[n].keys())[0]\n")
    f.write("    right_list += [[100.0 * float(x)/float(n) for x in right_naming[n][k]]]\n")
    f.write("    wrong_list += [[100.0 * float(x)/float(n) for x in wrong_naming[n][k]]]\n")
    f.write("    unnamed_list += [[100.0 * float(x)/float(n) for x in unnamed_naming[n][k]]]\n")

    f.write("\n")
    f.write("widths = len(ncells) * [4]\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(16, 6.5), constrained_layout=True)\n")
    f.write("rbox = ax.boxplot(right_list, positions=ncells, widths=widths, patch_artist=False)\n")
    f.write("wbox = ax.boxplot(wrong_list, positions=ncells, widths=widths, patch_artist=False)\n")

    f.write("\n")
    f.write("rmed = [b.get_ydata()[0] for b in rbox['medians']]\n")
    f.write("wmed = [b.get_ydata()[0] for b in wbox['medians']]\n")
    f.write("ax.plot(ncells, rmed, color='blue', label='right names')\n")
    f.write("ax.plot(ncells, wmed, color='cyan', label='wrong names')\n")

    f.write("\n")
    f.write("ax.yaxis.grid(True)\n")
    f.write("ax.set_xlabel('Cell count')\n")
    f.write("ax.set_ylabel('percentage')\n")
    f.write("ax.legend()\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_wrong")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")
    f.write("\n")

    f.write("fig, ax = plt.subplots(figsize=(16, 6.5), constrained_layout=True)\n")
    f.write("rbox = ax.boxplot(right_list, positions=ncells, widths=widths, patch_artist=False)\n")
    f.write("ubox = ax.boxplot(unnamed_list, positions=ncells, widths=widths, patch_artist=False)\n")

    f.write("\n")
    f.write("rmed = [b.get_ydata()[0] for b in rbox['medians']]\n")
    f.write("umed = [b.get_ydata()[0] for b in ubox['medians']]\n")
    f.write("ax.plot(ncells, rmed, color='blue', label='right names')\n")
    f.write("ax.plot(ncells, umed, color='cyan', label='unnamed cells')\n")

    f.write("\n")
    f.write("ax.yaxis.grid(True)\n")
    f.write("ax.set_xlabel('Cell count')\n")
    f.write("ax.set_ylabel('percentage')\n")
    f.write("ax.legend()\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_unnamed")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.close()

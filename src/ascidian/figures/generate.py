##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu 15 jui 2023 17:45:25 CEST
#
##############################################################
#
#
#
##############################################################

import astec.utils.common as common

import ascidian.components.embryoset as cembryoset
import ascidian.components.division_atlas as cdivatlas

import ascidian.figures.cell_composition as fcellcomp
import ascidian.figures.cell_distance_along_branch as fcelldistbrch
import ascidian.figures.cell_distance_histogram as fcelldisthist
import ascidian.figures.cell_neighbor_count as fcellneigh
import ascidian.figures.cell_signature as fcellsign
import ascidian.figures.division_dendrogram as fdivdend
import ascidian.figures.division_distance_histogram as fdivdisthist
import ascidian.figures.division_time_couple_misordering as fdivtimecouple
import ascidian.figures.division_time_difference as fdivtimediff
import ascidian.figures.division_time_global_misordering as fdivtimeglobal
import ascidian.figures.naming_timepoint as fnametime
import ascidian.figures.symmetry_axis as fsymaxis
import ascidian.figures.temporal_alignment as ftalign
import ascidian.figures.volume_fitting as fvolfit

monitoring = common.Monitoring()


###########################################################
#
#
#
############################################################

def _generate_figure_embryos(atlases, parameters, test_embryos=None):
    #
    #
    #
    do_generate_figure = (isinstance(parameters.generate_figure, bool) and parameters.generate_figure) or \
                         (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'all') or \
                         (isinstance(parameters.generate_figure, list) and 'all' in parameters.generate_figure)

    ############################################################
    #
    # cembryoset.EmbryoSet
    # cparameters.FigureParameters
    #
    ############################################################

    #
    # cell name composition
    # plot cell composition wrt time without and with temporal registration
    #
    if (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'cell-composition') \
            or (isinstance(parameters.generate_figure, list) and 'cell-composition' in parameters.generate_figure) \
            or do_generate_figure:
        fcellcomp.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate name composition wrt time figure file", 1)
        fcellcomp.figure_cell_composition(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    #
    # cell-to-cell distance along a branch
    #
    if (isinstance(parameters.generate_figure, str) and
        parameters.generate_figure == 'cell-to-cell-distance-along-branch') \
            or (isinstance(parameters.generate_figure, list) and
                'cell-to-cell-distance-along-branch' in parameters.generate_figure) \
            or do_generate_figure:
        fcelldistbrch.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate cell-to-cell distance along branch figure file", 1)
        fcelldistbrch.figure_cell_distance_along_branch(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    #
    # plot cell neighbor counts
    # cell neighbor count wrt total number of cells in the embryo
    #
    if (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'cell-neighbor-count') \
            or (isinstance(parameters.generate_figure, list)
                and 'cell-neighbor-count' in parameters.generate_figure) \
            or do_generate_figure:
        fcellneigh.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate cell neighbor counts file", 1)
        fcellneigh.figure_cell_neighbor_count(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    #
    # plot joint division time distribution for couples of cells
    #
    if (isinstance(parameters.generate_figure, str) and
        parameters.generate_figure == 'division-time-couple-misordering') \
            or (isinstance(parameters.generate_figure, list)
                and 'division-time-couple-misordering' in parameters.generate_figure) \
            or do_generate_figure:
        fdivtimecouple.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate division time couple mis-ordering probability figure file", 1)
        fdivtimecouple.figure_division_time_couple_misordering(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    #
    # plot division time differences between left and right hemispheres
    # and between embryos
    #
    if (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'division-time-difference') \
            or (isinstance(parameters.generate_figure, list) and 'division-time-difference' in
                parameters.generate_figure) \
            or do_generate_figure:
        fdivtimediff.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate division time difference figure file", 1)
        fdivtimediff.figure_division_time_difference(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    #
    # probabilities that couples of divisions are mis-ordered wrt to average divsion time
    #
    if (isinstance(parameters.generate_figure, str) and
        parameters.generate_figure == 'division-time-global-misordering') \
            or (isinstance(parameters.generate_figure, list)
                and 'division-time-global-misordering' in parameters.generate_figure) \
            or do_generate_figure:
        fdivtimeglobal.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate division time global mis-ordering probability figure file", 1)
        fdivtimeglobal.figure_division_time_global_misordering(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    #
    # plot the percentage of recognized symmetrical cells with
    # both 'sorted-contact' and 'winged-contact' cell similarities
    #
    if (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'symmetrical-cell-percentage') \
            or (isinstance(parameters.generate_figure,
                           list) and 'symmetrical-cell-percentage' in parameters.generate_figure) \
            or do_generate_figure:
        fsymaxis.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate symmetrical cell percentage figure file", 1)
        fsymaxis.figure_symmetrical_cell_percentage(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    #
    # temporal alignment:
    # plot cell count wrt time without and with temporal registration (based on total cell count)
    #
    if (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'temporal-alignment') or \
            (isinstance(parameters.generate_figure, list) and 'temporal-alignment' in parameters.generate_figure) \
            or do_generate_figure:
        ftalign.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate temporal alignment wrt cell count figure file", 1)
        ftalign.figure_temporal_alignment(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    #
    # temporal alignment:
    # plot cell count wrt time without and with temporal registration (based on epidermis cell count)
    #
    if (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'temporal-alignment-epidermis') \
            or (isinstance(parameters.generate_figure, list) and
                'temporal-alignment-epidermis' in parameters.generate_figure) \
            or do_generate_figure:
        ftalign.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate temporal alignment wrt epidermis cell count figure file", 1)
        ftalign.figure_temporal_alignment_epidermis(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    #
    # embryo volume linear fit
    # plot embryo volume wrt time without and with temporal registration
    #
    if (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'volume-fitting') \
            or (isinstance(parameters.generate_figure, list) and 'volume-fitting' in parameters.generate_figure) \
            or do_generate_figure:
        fvolfit.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate embryo volume figure file", 1)
        fvolfit.figure_volume_fitting(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    ############################################################
    #
    # cembryoset.EmbryoSet
    # cparameters.BaseParameters
    #
    ############################################################

    #
    # assess
    # both 'sorted-contact' and 'winged-contact' cell similarities for symmetry estimation
    #
    if (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'symmetry-axis-assessment') \
            or (isinstance(parameters.generate_figure, list) and
                'symmetry-axis-assessment' in parameters.generate_figure) \
            or do_generate_figure:
        fsymaxis.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate symmetry axis assessment figure file", 1)
        fsymaxis.figure_symmetry_axis_assessment(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    ############################################################
    #
    # cembryoset.EmbryoSet
    # ntparameters.NamingTimepointParameters
    #
    ############################################################

    #
    #
    #
    if (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'cell-signature') \
            or (isinstance(parameters.generate_figure, list)
                and 'cell-signature' in parameters.generate_figure) \
            or do_generate_figure:
        fcellsign.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate cell signature assessment figure file", 1)
        fcellsign.figure_cell_signature(atlases, parameters, test_embryos=test_embryos)
        monitoring.to_log_and_console("... done", 1)

    #
    # assess the naming of one time point with respect to the cell counts
    #
    if (isinstance(parameters.generate_figure, str) and
        parameters.generate_figure == 'naming_timepoint_wrt_cellcounts') \
            or (isinstance(parameters.generate_figure, list) and
                'naming_timepoint_wrt_cellcounts' in parameters.generate_figure) \
            or do_generate_figure:
        fnametime.monitoring.copy(monitoring)
        msg = "... generate naming timepoint (by transfer) assessment (wrt cell counts) figure file"
        monitoring.to_log_and_console(msg, 1)
        fnametime.figure_naming_timepoint_wrt_cellcounts(atlases, parameters, test_embryos=test_embryos)
        monitoring.to_log_and_console("... done", 1)

    #
    # assess the naming of one time point with respect to the number of atlases
    #
    if (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'naming_timepoint_wrt_atlases') \
            or (isinstance(parameters.generate_figure, list) and
                'naming_timepoint_wrt_atlases' in parameters.generate_figure) \
            or do_generate_figure:
        fnametime.monitoring.copy(monitoring)
        msg = "... generate naming timepoint (by transfer) assessment (wrt atlases) figure file"
        monitoring.to_log_and_console(msg, 1)
        fnametime.figure_naming_timepoint_wrt_atlases(atlases, parameters, test_embryos=test_embryos)
        monitoring.to_log_and_console("... done", 1)

    #
    # if (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'division-order') \
    #         or (isinstance(parameters.generate_figure, list) and 'division-order' in parameters.generate_figure) \
    #         or do_generate_figure:
    #     monitoring.to_log_and_console("... generate division order figure file", 1)
    #     _figure_division_order(atlases, parameters)
    #     _figure_division_proba(atlases, parameters)
    #     monitoring.to_log_and_console("... done", 1)

    #
    # symmetry axis related figures
    #

    # figsymaxis.generate_figure(atlases, parameters)


############################################################
#
#
#
############################################################


def _generate_figure_divatlases(atlases, parameters):
    #
    #
    #
    do_generate_figure = (isinstance(parameters.generate_figure, bool) and parameters.generate_figure) or \
                         (isinstance(parameters.generate_figure, str) and parameters.generate_figure == 'all') or \
                         (isinstance(parameters.generate_figure, list) and 'all' in parameters.generate_figure)

    ############################################################
    #
    # cdivatlas.DivisionAtlases
    # cparameters.FigureParameters
    #
    ############################################################

    #
    #
    #
    if (isinstance(parameters.generate_figure, str) and
        parameters.generate_figure == 'cell-to-cell-distance-histogram') \
            or (isinstance(parameters.generate_figure, list) and
                'cell-to-cell-distance-histogram' in parameters.generate_figure) \
            or do_generate_figure:
        fcelldisthist.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate cell-to-cell distance histogram figure file", 1)
        fcelldisthist.figure_cell_distance_histogram(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    #
    #
    #
    if (isinstance(parameters.generate_figure, str) and
        parameters.generate_figure == 'division-dendrograms') \
            or (isinstance(parameters.generate_figure, list) and 'division-dendrograms' in parameters.generate_figure) \
            or do_generate_figure:
        fdivdend.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate division dendrogram figure file", 1)
        fdivdend.figure_division_dendrogram(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    #
    #
    #
    if (isinstance(parameters.generate_figure, str) and
        parameters.generate_figure == 'division-to-division-distance-histogram') \
            or (isinstance(parameters.generate_figure, list) and
                'division-to-division-distance-histogram' in parameters.generate_figure) \
            or do_generate_figure:
        fcelldisthist.monitoring.copy(monitoring)
        monitoring.to_log_and_console("... generate division-to-division distance histogram figure file", 1)
        fdivdisthist.figure_division_distance_histogram(atlases, parameters)
        monitoring.to_log_and_console("... done", 1)

    #
    #
    #


############################################################
#
#
#
############################################################


def generate_figure(atlases, parameters, test_embryos=None):
    if isinstance(atlases, cembryoset.EmbryoSet) is True:
        _generate_figure_embryos(atlases, parameters, test_embryos=test_embryos)
    if isinstance(atlases, cdivatlas.DivisionAtlases) is True:
        _generate_figure_divatlases(atlases, parameters)

##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu 15 jui 2023 17:45:25 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

from collections import Counter

import astec.utils.common as common

import ascidian.components.embryoset as cembryoset
import ascidian.components.parameters as cparameters

monitoring = common.Monitoring()


################################################################################
#
# cell neighbors number wrt total number of cells in the embryo
#
################################################################################

def _neighbor_histogram(neighbors, atlas, atlasname, threshold=0.05):
    """

    :param neighbors:
    :param atlas:
    :param atlasname:
    :param threshold: threshold to consider a neighbor as 'valid'.
        threshold=0.05 means that only neighbors with more of 5%
        of contact surface are counted
    :return: a dictionary where keys are the counts of cells, and values the
        number of neighbors
    """
    contact = atlas.cell_contact_surface
    cellname = atlas.cell_name

    cell_number_threshold = 10
    #
    # cellpertime is a dictionary
    # cellpertime[t] = #cells at time t
    #
    nodes = list(contact.keys())
    div = 10 ** atlas.time_digits_for_cell_id
    times = [n // div for n in nodes]
    cellpertime = Counter(times)

    for n in contact:
        #
        # total surface
        #
        surface = 0
        for k in contact[n]:
            surface += contact[n][k]
        frac = []
        for k in contact[n]:
            if k % div == 0 or k % div == 1:
                continue
            frac += [contact[n][k]/surface]
        #
        # frac: array of surface fraction (without the background)
        #
        ncells = len([x for x in frac if x >= threshold])
        #
        # print a message for large neighbor number
        #
        if ncells > cell_number_threshold:
            msg = "cell " + str(n)
            if n in cellname:
                msg += " (" + str(cellname[n]) + ")"
            msg += " of properties '" + str(atlasname) + "' has " + str(ncells) + " neighbors"
            msg += " (above " + str(threshold*100) + "%)"
            monitoring.to_log_and_console("\t " + msg)
        t = n // div
        neighbors[cellpertime[t]] = neighbors.get(cellpertime[t], []) + [ncells]
    return neighbors


def _figure_neighbor_histogram_period(f):

    f.write("\n")
    f.write("axtwin = ax.secondary_xaxis('top')\n")
    f.write("axtwin.tick_params('x', direction='inout', length=10, width=2)\n")
    f.write("axtwin.set_xticks([76, 110, 250, 332, 624])\n")

    f.write("\n")
    f.write("x = [2, 76, 76, 2]\n")
    f.write("y = [0, 0, 14, 14]\n")
    f.write("plt.fill(x, y, color='grey', alpha=0.4)\n")
    f.write("plt.text(15, 13.5, 'cleavage', ha='left', va='center', fontsize=12, wrap=True)\n")

    f.write("\n")
    f.write("x = [110, 250, 250, 110]\n")
    f.write("y = [0, 0, 14, 14]\n")
    f.write("plt.fill(x, y, color='grey', alpha=0.4)\n")
    f.write("plt.text(180, 13.5, 'gastrula', ha='center', va='center', fontsize=12, wrap=True)\n")

    f.write("\n")
    f.write("x = [332, 624, 624, 332]\n")
    f.write("y = [0, 0, 14, 14]\n")
    f.write("plt.fill(x, y, color='grey', alpha=0.4)\n")
    f.write("plt.text(480, 13.5, 'neurula', ha='center', va='center', fontsize=12, wrap=True)\n")


def figure_cell_neighbor_count(embryos, parameters):
    """
    Plot cell neighbor number with respect to total cell number
    Parameters
    ----------
    embryos
    parameters

    Returns
    -------

    """
    proc = "figure_cell_neighbor_count"
    threshold = 0.05

    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        sys.exit(1)
    if isinstance(parameters, cparameters.FigureParameters) is False:
        msg = ": unexpected type for 'parameters' variable: " + str(type(parameters))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    filename = 'figure_cell_neighbor_count'
    figname = 'cell_neighbor_count'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    ref_embryos = embryos.get_embryos()

    neighbors = {}
    for a in ref_embryos:
        #
        # get a dictionary of neighbor number (indexed by cell number)
        # valid neighbors have more then 5% of contact
        #
        neighbors = _neighbor_histogram(neighbors, ref_embryos[a], a, threshold=threshold)

    #
    # neighbors is a dictionary indexed by the number of cells of an embryo
    # neighbors[64] is an array containing the number of neighbors
    #
    #
    #
    #
    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")
    f.write("import scipy.stats as stats\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    f.write("neighbors = " + str(neighbors) + "\n")
    f.write("ncells = sorted(list(neighbors.keys()))\n")

    f.write("\n")
    f.write("neighbors_per_ncell = []\n")
    f.write("ticks = []\n")
    f.write("xp = []\n")
    f.write("mp = []\n")
    f.write("sp = []\n")
    f.write("for i in ncells:\n")
    f.write("    xp += [i]\n")
    f.write("    mp += [np.mean(neighbors[i])]\n")
    f.write("    sp += [np.std(neighbors[i])]\n")
    f.write("    neighbors_per_ncell.append(neighbors[i])\n")
    f.write("    if i % 20 == 0:\n")
    f.write("        ticks += [str(i)]\n")
    f.write("    else:\n")
    f.write("        ticks += ['']\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(16, 6.5), constrained_layout=True)\n")

    f.write("\n")
    f.write("ax.set_xlabel('cell count', fontsize=15)\n")
    f.write("ax.set_ylabel('cell neighbor count', fontsize=15)\n")
    f.write("ax.boxplot(neighbors_per_ncell, positions=ncells)\n")
    f.write("ax.set_title(\"cell neighbor counts")
    f.write(" (excluding background and cells with contact less than {:.1f} %)".format(threshold * 100.0))
    f.write("\", fontsize=15)\n")
    f.write("ax.set_xticklabels(ticks)\n")
    f.write("ax.set_xlim(left=0)\n")

    _figure_neighbor_histogram_period(f)

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_1")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(16, 6.5), constrained_layout=True)\n")

    f.write("\n")
    f.write("ax.set_xlabel('cell count', fontsize=15)\n")
    f.write("ax.set_ylabel('average cell neighbor count (+/- std dev)', fontsize=15)\n")
    f.write("ax.errorbar(xp, mp, yerr=sp, color='red', fmt='-', ecolor='blue')\n")
    f.write("ax.set_title(\"cell neighbor counts")
    f.write(" (excluding background and cells with contact less than {:.1f} %)".format(threshold * 100.0))
    f.write("\", fontsize=15)\n")
    f.write("ax.set_xlim(left=0)\n")

    _figure_neighbor_histogram_period(f)

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_2")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")

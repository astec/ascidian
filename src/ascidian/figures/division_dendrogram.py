##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu 11 jul 2024 10:20:28 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

import astec.utils.common as common

import ascidian.components.division_atlas as cdivatlas
import ascidian.naming_propagation.parameters as npparameters
import ascidian.figures.write_utils as writeutils


monitoring = common.Monitoring()


################################################################################
#
# cell neighbors number wrt total number of cells in the embryo
#
################################################################################


def _linkage_balance(z, nlabels):
    """

    Parameters
    ----------
    z: the hierarchical clustering encoded as a linkage matrix
       (see https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html)
    nlabels

    Returns
    -------
    balance: min( #elements of the two last clusters) / max( #elements of the two last clusters)
        1.0 means the two last clusters are balanced
    """
    #
    # z[-1, 0] and z[-1, 1] are the labels of the two last clusters to be fused
    # if label < nlabels, it means the label is the one of an single element (one original observation)
    # else the cluster was made/described at line 'round(z[-1, 0]) - nlabels' of z, and the 3rd
    # element gives the number of original observations of this cluster
    #
    if z[-1, 0] < nlabels:
        ncluster0 = 1
    else:
        ncluster0 = z[round(z[-1, 0]) - nlabels, 3]
    if z[-1, 1] < nlabels:
        ncluster1 = 1
    else:
        ncluster1 = z[round(z[-1, 1]) - nlabels, 3]
    balance = min(ncluster0, ncluster1) / max(ncluster0, ncluster1)
    return balance


def figure_division_dendrogram(atlases, parameters):
    """
    Parameters
    ----------
    atlases: nested dictionary of neighborhood, where the keys are ['cell name']['reference name']
        where 'reference name' is the name of the reference lineage, and neighborhood a dictionary of contact surfaces
        indexed by cell names (only for the first time point after the division)
    parameters

    Returns
    -------

    """
    proc = "figure_division_dendrogram"

    if isinstance(atlases, cdivatlas.DivisionAtlases) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'atlases' variable: " + str(type(atlases)))
        sys.exit(1)
    if isinstance(parameters, npparameters.NamingPropagationParameters) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    filename = 'figure_division_dendrogram'
    figname = 'division_dendrogram'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    #
    # get the references per mother_name
    #
    divisions = atlases.get_division()
    ccs = not atlases.use_common_neighborhood
    cluster_distance = 'single'

    #
    #
    #

    dendro_values = {}
    merge_values = {}

    swmerge_values = {}
    swlastmerge_values = {}

    f = open(filename, "w")

    f.write("import sys\n")
    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")
    f.write("import scipy.cluster.hierarchy as sch\n")

    f.write("\n")
    f.write("cluster_distance = '" + str(cluster_distance) + "'\n")
    f.write("\n")

    cellidentifierlist = []
    innersurfaces = []

    for i, n in enumerate(divisions):
        stage = n.split('.')[0][1:]
        if len(divisions[n]) <= 2:
            continue

        #
        # config is a dictionary indexed par [reference][0 or 1]
        # config[r][0] = neighborhoods[daughters[0]][r]
        # config[r][1] = neighborhoods[daughters[1]][r]
        #
        config = atlases.extract_division_neighborhoods(n)
        #
        # swconfig = config + switched daughters
        #
        swconfig = cdivatlas.switched_division_neighborhoods(config, n)

        #
        # distance array for couples of atlases/references
        #
        # d = aname.get_daughter_names(n)
        # if parameters.exclude_inner_surfaces:
        #     innersurfaces = [d[0], d[1]]
        conddist, z, labels = cdivatlas.division_scipy_linkage(config, cluster_distance=cluster_distance,
                                                               change_contact_surfaces=ccs,
                                                               innersurfaces=innersurfaces)

        merge_values[stage] = merge_values.get(stage, []) + list(z[:, 2])
        # lastmerge_value is the distance between the two last clusters
        lastmerge_value = z[:, 2][-1]
        # balance in [0, 1] reflects the balance between the two last cluster. 1.0 means the two last clusters
        # have the same number of original observations
        balance = _linkage_balance(z, len(labels))
        dendro_values[stage] = dendro_values.get(stage, []) + [(lastmerge_value, balance, len(labels))]

        #
        # distance array for couples of atlases/references plus the switched ones
        # daughter neighborhoods may be in a common reference, if so there is no need to change
        # the contact surfaces
        #
        swconddist, swz, swlabels = cdivatlas.division_scipy_linkage(swconfig, cluster_distance=cluster_distance,
                                                                     change_contact_surfaces=ccs,
                                                                     innersurfaces=innersurfaces)

        swmerge_values[stage] = swmerge_values.get(stage, []) + list(swz[:, 2])
        swlastmerge_value = swz[:, 2][-1]
        swlastmerge_values[stage] = swlastmerge_values.get(stage, []) + [swlastmerge_value]

        #
        # identifier for mother cell
        #
        cellname = n.split('.')[0] + "_" + n.split('.')[1][0:4]
        if n.split('.')[1][4] == '_':
            cellname += 'U'
        elif n.split('.')[1][4] == '*':
            cellname += 'S'
        else:
            cellname += 'S'
        fileidentifier = 'HC{:03d}_'.format(int(lastmerge_value)) + cellname
        cellidentifier = cellname + '_HC{:03d}'.format(int(lastmerge_value))
        cellidentifier += '_BAL{:03d}'.format(round(100.0 * balance))
        cellidentifierlist.append((cellidentifier, n))

        f.write("\n")
        f.write("savefig = True\n")

        f.write("\n")
        f.write("\n")
        f.write("def draw_" + cellidentifier + "(savefig=False):\n")
        f.write("    " + "\n")
        f.write("    " + "print('generate figure for cell " + str(n))
        f.write(" (" + str(i+1) + "/" + str(len(divisions)) + ")')\n")
        f.write("    " + "\n")
        f.write("    ")
        writeutils.write_array(f, "cdist", conddist)
        f.write("    " + "labels = " + str(labels) + "\n")
        f.write("    " + "\n")
        f.write("    ")
        writeutils.write_array(f, "cswdist", swconddist)
        f.write("    " + "swlabels = " + str(swlabels) + "\n")
        f.write("\n")
        f.write("    " + "title = '" + str(n) + " (linkage=' + cluster_distance + '), ")
        f.write("delay={:d}'\n".format(parameters.delay_from_division))
        f.write("\n")
        f.write("    " + "Z = sch.linkage(cdist, method=cluster_distance)\n")
        f.write("    " + "fig = plt.figure(figsize=(16, 8), constrained_layout=True)\n")
        f.write("    " + "dn = sch.dendrogram(Z, labels=labels, orientation='right')\n")
        f.write("    " + "plt.title(title, fontsize=24)\n")
        f.write("    " + "plt.xticks(fontsize=16)\n")
        f.write("    " + "plt.yticks(fontsize=14)\n")
        f.write("    " + "plt.xlim([0, 100])\n")

        f.write("    if savefig:\n")
        f.write("        plt.savefig('" + figname + "_" + str(fileidentifier) + "_' + cluster_distance +'")
        if file_suffix is not None:
            f.write(file_suffix)
        f.write("'" + " + '.png')\n")
        f.write("        plt.savefig('" + figname + "_" + str(cellidentifier) + "_' + cluster_distance +'")
        if file_suffix is not None:
            f.write(file_suffix)
        f.write("'" + " + '.png')\n")
        f.write("    else:\n")
        f.write("        plt.show()\n")
        f.write("    plt.close()\n")
        f.write("\n")

        f.write("    " + "Z = sch.linkage(cswdist, method=cluster_distance)\n")
        f.write("    " + "fig = plt.figure(figsize=(18, 8), constrained_layout=True)\n")
        f.write("    " + "dn = sch.dendrogram(Z, labels=swlabels, orientation='right')\n")
        f.write("    " + "plt.title(title, fontsize=24)\n")
        f.write("    " + "plt.xticks(fontsize=16)\n")
        f.write("    " + "plt.yticks(fontsize=12)\n")
        f.write("    " + "plt.xlim([0, 100])\n")

        f.write("    if savefig:\n")
        f.write("        plt.savefig('" + figname + "_" + str(fileidentifier) + "_' + cluster_distance +'")
        if file_suffix is not None:
            f.write(file_suffix)
        f.write("'" + " + '_SW.png')\n")
        f.write("        plt.savefig('" + figname + "_" + str(cellidentifier) + "_' + cluster_distance +'")
        if file_suffix is not None:
            f.write(file_suffix)
        f.write("'" + " + '_SW.png')\n")
        f.write("    else:\n")
        f.write("        plt.show()\n")
        f.write("    plt.close()\n")

        f.write("\n")

    f.write("\n")
    f.write("\n")
    f.write("def draw_all(celllist=[], savefig=True):\n")
    for cellid in cellidentifierlist:
        f.write("    if celllist == [] or '" + cellid[1] + "' in celllist:\n")
        f.write("        draw_" + cellid[0] + "(savefig=savefig)\n")

    f.write("\n")
    f.write("\n")

    f.write("celllist = []\n")
    f.write("if len(sys.argv) > 1 and sys.argv[1][:2] == '-h':\n")
    f.write("    print('Usage: ' + str(sys.argv[0]) + ' list of cell names (between quotes)')\n")
    f.write("if len(sys.argv) > 1:\n")
    f.write("    for i in range(1, len(sys.argv)):\n")
    f.write("        celllist += [sys.argv[i]]\n")
    f.write("\n")
    f.write("# print(\"celllist=\"+str(celllist))\n")
    f.write("draw_all(celllist=celllist, savefig=savefig)\n")
    f.write("\n")

    #
    # Statistics from the dendrograms
    #

    generations = list(merge_values.keys())
    generations = sorted(generations)
    f.write("generations = " + str(generations) + "\n")
    f.write("merge_values = [")
    for i, g in enumerate(generations):
        writeutils.write_raw_array(f, merge_values[g])
        if i < len(generations) - 1:
            f.write(", ")
    f.write("]\n")
    f.write("\n")

    f.write("dendro_values = [")
    for i, g in enumerate(generations):
        writeutils.write_tuple_array(f, dendro_values[g], length=3)
        if i < len(generations) - 1:
            f.write(", ")
    f.write("]\n")
    f.write("\n")

    f.write("merge_labels = [")
    for i, g in enumerate(generations):
        f.write("'generation " + str(g) + "'")
        if i < len(generations) - 1:
            f.write(", ")
    f.write("]\n")

    f.write("\n")
    f.write("lastmerge_values = [[v[0] for v in dv] for dv in dendro_values]\n")
    f.write("balance_values = [[v[1] for v in dv] for dv in dendro_values]\n")

    f.write("\n")
    f.write("fig, (ax1, ax2) = plt.subplots(ncols=2, figsize=(16, 6.5), constrained_layout=True)\n")

    f.write("ax1.hist(merge_values, bins=list(range(0, 101, 2)), histtype='bar', stacked=True, label=merge_labels)\n")
    f.write("ax1.set_title('dendrogram merge values', fontsize=24)\n")
    f.write("ax1.legend(prop={'size': 10})\n")
    f.write("ax1.tick_params(labelsize=10)\n")

    f.write("ax2.hist(lastmerge_values, bins=list(range(0, 101, 2)), histtype='bar'")
    f.write(", stacked=True, label=merge_labels)\n")
    f.write("ax2.set_title('dendrogram last merge values', fontsize=24)\n")
    f.write("ax2.legend(prop={'size': 10})\n")
    f.write("ax2.tick_params(labelsize=10)\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_merge_histogram_' + cluster_distance +'")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(8, 8), constrained_layout=True)\n")

    f.write("ax.hist(balance_values, bins=50, range=(0, 1), histtype='bar', stacked=True, label=merge_labels)\n")
    f.write("ax.set_title('dendrogram balance values', fontsize=24)\n")
    f.write("ax.legend(prop={'size': 10})\n")
    f.write("ax.tick_params(labelsize=10)\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_balance_histogram_' + cluster_distance +'")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.write("\n")
    f.write("x = [lv for lvg in lastmerge_values for lv in lvg]\n")
    f.write("y = [bv for bvg in balance_values for bv in bvg]\n")
    f.write("c_values = [[int(generations[i])] * len(lvg) for i, lvg in enumerate(lastmerge_values)]\n")
    f.write("c = [cv for cvg in c_values for cv in cvg]\n")
    f.write("s_values = [[v[2] for v in dv] for dv in dendro_values]\n")
    f.write("s = [10 * sv for svg in s_values for sv in svg]\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(8, 8), constrained_layout=True)\n")
    f.write("scatter = ax.scatter(x, y, c=c, s=s, alpha=0.5)\n")
    f.write("ax.set_xlabel('Last merge value (cluster distance * 100)')\n")
    f.write("ax.set_ylabel('Linkage balance')\n")
    f.write("legend1 = ax.legend(*scatter.legend_elements(alpha=0.8), loc='upper left', title='Generations')\n")
    f.write("ax.add_artist(legend1)\n")
    f.write("handles, labels = scatter.legend_elements(prop='sizes', alpha=0.6)\n")
    f.write("labels = [l.replace('0}', '}') for l in labels]\n")
    f.write("legend2 = ax.legend(handles, labels, loc='upper right', title='Sizes')\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_balance_merge_scatter_' + cluster_distance +'")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.write("\n")

    f.close()

##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu 15 jui 2023 17:45:25 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

from operator import itemgetter

import astec.utils.common as common

import ascidian.components.embryoset as cembryoset
import ascidian.components.parameters as cparameters
import ascidian.figures.write_utils as writeutils

monitoring = common.Monitoring()


################################################################################
#
# cell count wrt time with temporal registration
# + cell name discrepancy
#
################################################################################


def _write_tuple_array(f, a, length=4):
    form = "{:1." + str(length) + "f}"
    last = len(a) - 1
    f.write("[")
    for i, v in enumerate(a):
        lt = len(v) - 1
        f.write("(")
        for j, w in enumerate(v):
            if j == 0:
                f.write(form.format(w))
            else:
                f.write("{:d}".format(w))
            if j < lt:
                f.write(", ")
        f.write(")")
        if i < last:
            f.write(", ")
    f.write("]")


def figure_cell_composition(embryos, parameters):
    """
    Plot cell count curves with linear temporal alignment and cell name discrepancy
    Parameters
    ----------
    embryos
    parameters

    Returns
    -------

    """
    proc = "figure_cell_composition"

    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        sys.exit(1)
    if isinstance(parameters, cparameters.FigureParameters) is False:
        msg = ": unexpected type for 'parameters' variable: " + str(type(parameters))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    filename = 'figure_cell_composition'
    figname = 'cell_composition'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    ref_embryos = embryos.get_embryos()
    embryo_names = list(ref_embryos.keys())

    cells_per_time = {}
    names_per_time = {}
    temporal_coefficients = {}
    start_acquisition = {}
    end_acquisition = {}
    for n in embryo_names:
        lineage = ref_embryos[n].cell_lineage
        cell_name = ref_embryos[n].cell_name
        cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
        cells = sorted(cells)
        div = 10 ** ref_embryos[n].time_digits_for_cell_id
        cells_per_time[n] = {}
        names_per_time[n] = {}
        all_times = [int(c) // div for c in cells]
        start_acquisition[n] = min(all_times)
        end_acquisition[n] = max(all_times)
        for c in cells:
            t = int(c) // div
            cells_per_time[n][t] = cells_per_time[n].get(t, []) + [c]
            if c in cell_name:
                names_per_time[n][t] = names_per_time[n].get(t, []) + [cell_name[c]]
        temporal_coefficients[n] = ref_embryos[n].temporal_alignment

    name_composition = []
    cell_composition = {}
    for n in embryo_names:
        cell_composition[n] = []
        cell_name = ref_embryos[n].cell_name
        for i in range(start_acquisition[n], end_acquisition[n]+1):
            t = temporal_coefficients[n][0] * i + temporal_coefficients[n][1]
            names = [(n, i)]
            for m in embryo_names:
                if m == n:
                    continue
                j = int(round((t - temporal_coefficients[m][1])/temporal_coefficients[m][0]))
                if j < start_acquisition[m] or end_acquisition[m] < j:
                    continue
                names += [(m, j)]
            if len(names) <= 1:
                continue
            #
            # ok, we've got at least two atlases
            # - atlas name is names[i][0]
            # - atlas acquisition index is names[i][1]
            #

            #
            # names
            #
            union_set = set(names_per_time[names[0][0]][names[0][1]])
            inter_set = set(names_per_time[names[0][0]][names[0][1]])
            for a in range(1, len(names)):
                union_set = union_set.union(set(names_per_time[names[a][0]][names[a][1]]))
                inter_set = inter_set.intersection(set(names_per_time[names[a][0]][names[a][1]]))
            name_composition += [(t, len(names), len(union_set), len(inter_set))]

            #
            # cells
            #
            unnamed_cell = 0
            inter_cell = 0
            diff_cell = 0
            for c in cells_per_time[n][i]:
                if c not in cell_name:
                    unnamed_cell += 1
                    continue
                if cell_name[c] in inter_set:
                    inter_cell += 1
                    continue
                diff_cell += 1
            cell_composition[n] += [(t, len(cells_per_time[n][i]), inter_cell, diff_cell, unnamed_cell)]

    name_composition = sorted(name_composition, key=itemgetter(0))

    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    writeutils.write_dict_of_tuple_arrays(f, 'cell_composition', cell_composition, length=2)
    f.write("\n")
    f.write("name_composition = ")
    _write_tuple_array(f, name_composition, length=2)
    f.write("\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(8, 8), constrained_layout=True)\n")

    f.write("\n")
    f.write("# cell_composition is a dictionary indexed by atlas name\n")
    f.write("# value = array of tuples (homogenized time, #cells, #common names, #diff names, #unnamed cells)\n")
    f.write("\n")
    f.write("for n in cell_composition:\n")
    f.write("    t = [composition[0] for composition in cell_composition[n]]\n")
    f.write("    ctot = [composition[1] for composition in cell_composition[n]]\n")
    f.write("    ccom = [composition[2] for composition in cell_composition[n]]\n")
    f.write("    ccomdif = [composition[2]+composition[3] for composition in cell_composition[n]]\n")
    f.write("    cunn = [composition[4] for composition in cell_composition[n]]\n")
    f.write("    p = ax.plot(t, ctot, '-', label='#cells ('+ n +')')\n")
    f.write("    # ax.plot(t, ccom, '--', color=p[0].get_color())\n")
    f.write("    # ax.plot(t, ccomdif, '-.', color=p[0].get_color())\n")

    f.write("\n")
    f.write("# name_composition = array of tuples (homogenized time, #atlases, #all cell names, #common cell names)\n")
    f.write("# it is built over all the atlases\n")
    f.write("\n")
    f.write("t = [composition[0] for composition in name_composition]\n")
    f.write("natlas = [composition[1] for composition in name_composition]\n")
    f.write("ntot = [composition[2] for composition in name_composition]\n")
    f.write("ncom = [composition[3] for composition in name_composition]\n")
    f.write("ndif = [composition[2]-composition[3] for composition in name_composition]\n")
    f.write("ax.plot(t, ntot, linewidth=2, label='#total names')\n")
    f.write("ax.plot(t, ncom, linewidth=2, label='#common names')\n")
    f.write("ax.plot(t, ndif, linewidth=2, label='#different names')\n")
    f.write("ax2 = ax.twinx()\n")
    f.write("ax2.plot(t, natlas, '--', linewidth=2, label='#atlas')\n")

    f.write("\n")
    f.write("ax.set_xlabel('normalized time')\n")
    f.write("ax.set_ylabel('cell/name count')\n")
    f.write("ax2.set_ylabel('atlas count')\n")
    f.write("ax.set_title(\"name composition\", fontsize=15)\n")
    f.write("ax.legend(prop={'size': 10})\n")
    f.write("ax2.legend(prop={'size': 10})\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_1")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.write("\n")
    f.write("\n")

    f.write("# dictionary indexed by #cells, whose value gives the corresponding homogenized time\n")
    f.write("# it comes to average (interpolated) times for every value of #cells\n")
    f.write("meantime_per_ncell = {}\n")
    f.write("ntime_per_ncell = {}\n")

    f.write("\n")
    f.write("for n in cell_composition:\n")
    f.write("    for i, c in enumerate(cell_composition[n]):\n")
    f.write("        meantime_per_ncell[c[1]] = meantime_per_ncell.get(c[1], 0) + c[0]\n")
    f.write("        ntime_per_ncell[c[1]] = ntime_per_ncell.get(c[1], 0) + 1\n")
    f.write("        if i == len(cell_composition[n]) - 1:\n")
    f.write("            continue\n")
    f.write("        if cell_composition[n][i + 1][1] == c[1]:\n")
    f.write("            continue\n")
    f.write("        for d in range(int(c[1]) + 1, int(cell_composition[n][i + 1][1])):\n")
    f.write("            dc = (d - c[1]) / (cell_composition[n][i + 1][1] - c[1])\n")
    f.write("            dt = c[0] + dc * (cell_composition[n][i + 1][0] - c[0])\n")
    f.write("            meantime_per_ncell[d] = meantime_per_ncell.get(d, 0) + dt\n")
    f.write("            ntime_per_ncell[d] = ntime_per_ncell.get(d, 0) + 1\n")

    f.write("\n")
    f.write("for nc in meantime_per_ncell:\n")
    f.write("    meantime_per_ncell[nc] /= ntime_per_ncell[nc]\n")

    f.write("\n")
    f.write("fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True, figsize=(8, 16), constrained_layout=True)\n")

    f.write("\n")
    f.write("# to plot the average cell count wrt homogenized time\n")
    f.write("# nct = [(meantime_per_ncell[nc], nc) for nc in meantime_per_ncell]\n")
    f.write("# nct = sorted(nct, key=lambda v: v[0])\n")
    f.write("# meant = [n[0] for n in nct]\n")
    f.write("# meanncell = [n[1] for n in nct]\n")
    f.write("# ax1.plot(meant, meanncell, '--', label='average')\n")

    f.write("\n")
    f.write("for n in cell_composition:\n")
    f.write("    t = [composition[0] for composition in cell_composition[n]]\n")
    f.write("    ctot = [composition[1] for composition in cell_composition[n]]\n")
    f.write("    p = ax1.plot(t, ctot, '-', label='#cells (' + n + ')')\n")

    f.write("\n")
    f.write("ax1.set_ylabel('#cells')\n")
    f.write("ax1.set_title('cell count', fontsize=15)\n")
    f.write("ax1.set_xlim(-5.28, 94.51)\n")
    f.write("ax1.set_ylim(0, 350)\n")
    f.write("ax1.legend(prop={'size': 10})\n")

    f.write("\n")
    f.write("t = [composition[0] for composition in name_composition]\n")
    f.write("natlas = [composition[1] for composition in name_composition]\n")
    f.write("npdif = [100 * (1 - composition[3] / composition[2]) for composition in name_composition]\n")

    f.write("\n")
    f.write("ax2.plot(t, npdif, linewidth=2)\n")
    f.write("ax2.set_ylim(0, 100)\n")
    f.write("# ax2b = ax2.twinx()\n")
    f.write("# ax2b.plot(t, natlas, '--', linewidth=2, label='#atlas')\n")

    f.write("\n")
    f.write("ax2.set_ylabel('percentage')\n")
    f.write("ax2.set_xlabel('normalized time')\n")
    f.write("# ax2b.set_ylabel('atlas count')\n")
    f.write("ax2.set_title('cell composition discrepancy', fontsize=15)\n")
    f.write("# ax2.legend(prop={'size': 10})\n")
    f.write("# ax2b.legend(prop={'size': 10})\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_2")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.write("\n")
    f.close()

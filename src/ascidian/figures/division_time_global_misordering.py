##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu 15 jui 2023 17:45:25 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys
import math

from operator import itemgetter

import numpy as np

import astec.utils.common as common

import ascidian.components.embryoset as cembryoset
import ascidian.components.parameters as cparameters
import ascidian.figures.write_utils as writeutils

monitoring = common.Monitoring()


###########################################################
#
#
#
############################################################


def figure_division_time_global_misordering(embryos, parameters, sigma=0.5):
    proc = "figure_division_time_global_misordering"

    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        sys.exit(1)
    if isinstance(parameters, cparameters.FigureParameters) is False:
        msg = ": unexpected type for 'parameters' variable: " + str(type(parameters))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    filename = 'figure_division_time_global_misordering'
    figname = 'division_time_global_misordering'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    ref_embryos = embryos.get_embryos()
    embryo_names = list(ref_embryos.keys())

    cells_per_time = {}
    temporal_coefficients = {}
    for n in embryo_names:
        lineage = ref_embryos[n].cell_lineage
        cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
        cells = sorted(cells)
        div = 10 ** ref_embryos[n].time_digits_for_cell_id
        cells_per_time[n] = {}
        for c in cells:
            t = int(c) // div
            cells_per_time[n][t] = cells_per_time[n].get(t, 0) + 1
        temporal_coefficients[n] = ref_embryos[n].temporal_alignment

    #
    # timepoints is a dictionary indexed by cell names
    # - value is a dictionary index by atlas name
    #   - value is an array (of one element) = (normalized division time)
    #
    timepoints = cembryoset.get_division_time_by_cell_atlas(embryos)

    # collect the division through atlas (left and right)
    inter_times = {}
    for d in timepoints:
        cname = d[:-1]
        # lr = d[-1]
        # stage = d.split('.')[0][1:]
        for a in timepoints[d]:
            inter_times[cname] = inter_times.get(cname, []) + [timepoints[d][a]]

    # precompute min and max of division times (through atlases, left and right together)
    meandiv_name = []
    for c in inter_times:
        meandiv_name += [(np.mean(inter_times[c]), c)]
    # order (average division time, cell name) by increasing (average) division time
    meandiv_name = sorted(meandiv_name, key=itemgetter(0))

    mean_times = {}
    for mn in meandiv_name:
        mean_times[mn[1]] = mn[0]

    # compute whether the divisions are in increasing order or not
    inc_worder = np.zeros((len(meandiv_name), len(meandiv_name)))
    dec_worder = np.zeros((len(meandiv_name), len(meandiv_name)))
    tot_worder = np.zeros((len(meandiv_name), len(meandiv_name)))
    localsigma = sigma

    for i, mni in enumerate(meandiv_name):
        # consider cell mni[1]
        # the name has nor '*' nor '_'
        for j, mnj in enumerate(meandiv_name):
            if i >= j:
                continue
            for lr in ['_', '*']:
                cnamei = mni[1] + lr
                cnamej = mnj[1] + lr
                if cnamei not in timepoints or cnamej not in timepoints:
                    continue
                for a in timepoints[cnamei]:
                    if a in timepoints[cnamej]:
                        d = timepoints[cnamej][a] - timepoints[cnamei][a]
                        inc_worder[i, j] += 0.5 * (1.0 + math.erf(d / (2.0 * localsigma)))
                        dec_worder[i, j] += 0.5 * (1.0 + math.erf(-d / (2.0 * localsigma)))
                        tot_worder[i, j] += 1

    # compute the fraction of division couples in increasing order
    wbalance = np.zeros((len(meandiv_name), len(meandiv_name)))
    for i, mni in enumerate(meandiv_name):
        for j, mnj in enumerate(meandiv_name):
            if i >= j:
                continue
            if tot_worder[i, j] > 0:
                wbalance[i, j] = dec_worder[i, j] / tot_worder[i, j]

    f = open(filename, "w")

    f.write("from operator import itemgetter\n")
    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    f.write("cells_per_time = " + str(cells_per_time) + "\n")
    f.write("temporal_coefficients = " + str(temporal_coefficients) + "\n")
    f.write("\n")
    f.write("meandiv_name = ")
    f.write(str(meandiv_name))
    f.write("\n")
    f.write("division_names = [mn[1] for mn in meandiv_name]\n")
    f.write("\n")

    f.write("for index64, mn in enumerate(meandiv_name):\n")
    f.write("    stage = mn[1].split('.')[0][1:]\n")
    f.write("    if int(stage) == 7:\n")
    f.write("        break\n")
    f.write("\n")
    f.write("msg = '- stage 7 reached for division #' + str(index64) + ' of cell ' + str(meandiv_name[index64][1])\n")
    f.write("print(msg)\n")
    f.write("\n")

    f.write("time_from_mean = []\n")
    f.write("ncellstime_from_mean = []\n")
    f.write("ncells_from_mean = []\n")
    f.write("ncells_interval = []\n")
    f.write("ncell_targets = [64, 76, 112, 184, 218]\n")
    f.write("for i, mn in enumerate(meandiv_name):\n")
    f.write("    time_from_mean += [mn[0]]\n")
    f.write("    n = 64 + 2 * (i - index64)\n")
    f.write("    ncellstime_from_mean += [mn[0]]\n")
    f.write("    ncells_from_mean += [n]\n")
    f.write("    if i > 0:\n")
    f.write("        ncells_interval += [(n, time_from_mean[-1]-time_from_mean[-2])]\n")
    f.write("    n = 64 + 2 * (i - index64 + 1)\n")
    f.write("    ncellstime_from_mean += [mn[0]]\n")
    f.write("    ncells_from_mean += [n]\n")
    f.write("    for t in ncell_targets:\n")
    f.write("        if ncells_from_mean[-2] < t <= ncells_from_mean[-1]:\n")
    f.write("            msg = '- {:3d} cells reached between division of '.format(t) + str(meandiv_name[i-1][1])\n")
    f.write("            msg  += ' and ' + str(mn[1])\n")
    f.write("            print(msg)\n")
    f.write("            msg = '  divisions {:3d}/{:3d} - {:3d}/{:3d}'.format(i-1, len(meandiv_name), i, len(meandiv_name))\n")
    f.write("            print(msg)\n")
    f.write("\n")

    f.write("ncells_interval = sorted(ncells_interval, key=itemgetter(1), reverse=True)\n")
    f.write("for i in range(8):\n")
    f.write("    msg = '#{:d} longest time interval ({:.2f}) at {:3d} cells'.format(i, ncells_interval[i][1], ncells_interval[i][0])\n")
    f.write("    print(msg)\n")
    f.write("\n")

    f.write("fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True, figsize=(16, 8))\n")
    f.write("labels = []\n")

    f.write("\n")
    f.write("for n in cells_per_time:\n")
    f.write("    labels += [n]\n")
    f.write("    x = list(cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    y = [cells_per_time[n][i] for i in x]\n")
    f.write("    ax1.plot(t, y)\n")
    f.write("ax1.set_title(\"Embryos\", fontsize=15)\n")
    f.write("ax1.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("for n in cells_per_time:\n")
    f.write("    x = list(cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    y = [cells_per_time[n][i] for i in x]\n")
    f.write("    ax2.plot(t, y, '--')\n")
    f.write("ax2.plot(ncellstime_from_mean, ncells_from_mean, linewidth=3)\n")
    f.write("labels += ['aver. div. time']\n")
    f.write("ax2.set_title(\"Embryos + average divisions\", fontsize=15)\n")
    f.write("ax2.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("fig.suptitle('cell count (with alignment)', fontsize=16)\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('temporal_alignment_average")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("plt.close()\n")

    f.write("zoomintervals=[((-10, 50), (40, 130)), ((30, 65), (100, 200))]\n")
    f.write("for i, interval in enumerate(zoomintervals):\n")
    f.write("    fig, ax = plt.subplots(figsize=(8, 8))\n")
    f.write("    labels = []\n")
    f.write("    for n in cells_per_time:\n")
    f.write("        labels += [n]\n")
    f.write("        x = list(cells_per_time[n].keys())\n")
    f.write("        x = sorted(x)\n")
    f.write("        t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("        y = [cells_per_time[n][i] for i in x]\n")
    f.write("        ax.plot(t, y, '--')\n")
    f.write("    ax.plot(ncellstime_from_mean, ncells_from_mean, linewidth=3)\n")
    f.write("    labels += ['aver. div. time']\n")
    f.write("    ax.set_xlim(left=interval[0][0], right=interval[0][1])\n")
    f.write("    ax.set_ylim(bottom=interval[1][0], top=interval[1][1])\n")
    f.write("    ax.set_title(\"cell count (with alignment) + average divisions\", fontsize=15)\n")
    f.write("    ax.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("    if savefig:\n")
    f.write("        plt.savefig('temporal_alignment_average_zoom_' + str(i)")
    if file_suffix is not None:
        f.write(" + '")
        f.write(file_suffix)
        f.write("'")
    f.write(" + '.png')\n")
    f.write("    else:\n")
    f.write("        plt.show()\n")
    f.write("    plt.close()\n")

    f.write("\n")
    writeutils.write_nparray(f, "wbalance", wbalance, length=3)
    f.write("\n")

    f.write("\n")
    f.write("intervals = [('a6.0004', 'b9.0050')")
    f.write(", ('a7.0008', 'a7.0006')")
    f.write(", ('b8.0008', 'b8.0006')")
    f.write(", ('a8.0029', 'b9.0038')")
    f.write("]\n")
    f.write("\n")

    f.write("for i, interval in enumerate(intervals):\n")
    f.write("\n")

    f.write("    b = division_names.index(interval[0])\n")
    f.write("    e = division_names.index(interval[1]) + 1\n")
    f.write("\n")

    f.write("    fig, ax = plt.subplots(figsize=(8, 8))\n")
    f.write("    im = ax.imshow(wbalance[b:e, b:e], cmap='jet_r', vmin=0, vmax=1)\n")
    f.write("    fig.colorbar(im, shrink=0.8)\n")
    f.write("\n")

    f.write("    title = 'probability of mis-ordered divisions [' + interval[0] + ', ' + interval[1] + ']'\n")
    f.write("    title += ' (continuous) (with respect to average division time)'\n")
    f.write("    title += ' (sigma = {:.2f})'\n".format(localsigma))
    f.write("    ax.set_title(title, fontsize=15, wrap=True)\n")
    f.write("\n")

    f.write("    ax.set_xticks(np.arange(len(division_names[b:e])))\n")
    f.write("    ax.set_xticklabels(division_names[b:e])\n")
    f.write("    plt.setp(ax.get_xticklabels(), rotation=90, ha='right', rotation_mode='anchor')\n")
    f.write("    ax.set_yticks(np.arange(len(division_names[b:e])))\n")
    f.write("    ax.set_yticklabels(division_names[b:e])\n")
    f.write("\n")

    f.write("    if savefig:\n")
    f.write("        plt.savefig('" + figname + "' + '_zoom_' + str(i)")
    if file_suffix is not None:
        f.write(" + '" + file_suffix + "'")
    f.write(" + '.png')\n")
    f.write("    else:\n")
    f.write("        plt.show()\n")

    f.write("fig, ax = plt.subplots(figsize=(8, 8))\n")
    f.write("\n")

    f.write("im = ax.imshow(wbalance, cmap='jet_r', vmin=0, vmax=1)\n")
    f.write("fig.colorbar(im, shrink=0.8)\n")
    f.write("\n")

    f.write("title = 'probability of mis-ordered divisions'\n")
    f.write("title += ' (with respect to average division time)'\n")
    f.write("title += ' (continuous) (sigma = {:.2f})'\n".format(localsigma))
    f.write("ax.set_title(title, fontsize=15, wrap=True)\n")
    f.write("\n")

    f.write("ax.set_xticks(np.arange(len(division_names)))\n")
    f.write("ax.set_xticklabels(division_names)\n")
    f.write("plt.setp(ax.get_xticklabels(), rotation=90, ha='right', rotation_mode='anchor')\n")
    f.write("ax.set_yticks(np.arange(len(division_names)))\n")
    f.write("ax.set_yticklabels(division_names)\n")
    f.write("\n")

    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname)
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("\n")

    f.close()

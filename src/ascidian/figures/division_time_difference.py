##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Lun 24 jui 2024 14:13:43 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

import astec.utils.common as common

import ascidian.components.embryoset as cembryoset
import ascidian.components.parameters as cparameters
import ascidian.figures.write_utils as writeutils

monitoring = common.Monitoring()

################################################################################
#
# cell count wrt time without and with temporal registration
# 1. with count of all cells
# 2. with count of Epidermis fate cells (allows to temporally register U0126 embryos)
#
################################################################################


def figure_division_time_difference(embryos, parameters):

    proc = "figure_division_time_difference"

    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        sys.exit(1)
    if isinstance(parameters, cparameters.FigureParameters) is False:
        msg = ": unexpected type for 'parameters' variable: " + str(type(parameters))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    filename = 'figure_division_time_difference'
    figname = 'division_time_difference'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")
    f.write("\n")

    f.write("savefig = True\n")
    f.write("\n")

    #
    # timepoints is a dictionary indexed by cell names
    # - value is a dictionary index by atlas name
    #   - value is an array (of one element) = (normalized division time)
    #
    timepoints = cembryoset.get_division_time_by_cell_atlas(embryos)

    intra_difference = {}
    inter_difference = {}
    # loop on cells
    for d in timepoints:
        cname = d[:-1]
        lr = d[-1]
        stage = d.split('.')[0][1:]

        for a in timepoints[d]:
            #
            # intra left-right difference of division time between Cell_ and Cell*
            #
            if lr == '_':
                sister = cname + '*'
                if sister in timepoints:
                    if a in timepoints[sister]:
                        difference = abs(timepoints[d][a] - timepoints[sister][a])
                        if difference >= 10:
                            msg = "cell {:>9s} divides at {:.2f} in '{:s}'".format(d, timepoints[d][a], a)
                            msg += " while its sister {:>9s} divides at {:.2f}".format(sister, timepoints[sister][a])
                            monitoring.to_log_and_console("    .. " + msg)
                        intra_difference[stage] = intra_difference.get(stage, []) + [difference]
            #
            # inter difference
            #
            for b in timepoints[d]:
                if a <= b:
                    continue
                difference = abs(timepoints[d][a] - timepoints[d][b])
                inter_difference[stage] = inter_difference.get(stage, []) + [difference]
                if lr == '_':
                    sister = cname + '*'
                    if sister in timepoints:
                        if b in timepoints[sister]:
                            difference = abs(timepoints[d][a] - timepoints[sister][b])
                            inter_difference[stage] = inter_difference.get(stage, []) + [difference]

    writeutils.write_dict_of_arrays(f, "intra_interval", intra_difference, length=4)
    f.write("\n")
    writeutils.write_dict_of_arrays(f, "inter_interval", inter_difference, length=4)
    f.write("\n")

    f.write("intra_labels = list(intra_interval.keys())\n")
    f.write("intra_labels = sorted(intra_labels)\n")
    f.write("intra_data = []\n")
    f.write("for l in intra_labels:\n")
    f.write("    intra_data += [intra_interval[l]]\n")
    f.write("\n")

    f.write("inter_labels = list(inter_interval.keys())\n")
    f.write("inter_labels = sorted(inter_labels)\n")
    f.write("inter_data = []\n")
    f.write("for l in inter_labels:\n")
    f.write("    inter_data += [inter_interval[l]]\n")
    f.write("\n")

    f.write("fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True, figsize=(16, 6.5), constrained_layout=True)\n")
    f.write("\n")

    f.write("ax1.set_ylabel('time interval length', fontsize=15)\n")
    f.write("ax1.set_xlabel('division generation', fontsize=15)\n")
    f.write("ax1.boxplot(intra_data, labels=intra_labels)\n")
    f.write("ax1.set_title(\"intra-embryo (ie left/right difference)\", fontsize=15)\n")
    f.write("\n")

    f.write("ax2.set_xlabel('division generation', fontsize=15)\n")
    f.write("ax2.boxplot(inter_data, labels=inter_labels)\n")
    f.write("ax2.set_title(\"inter-embryo (mixing left/right)\", fontsize=15)\n")
    f.write("\n")

    f.write("fig.suptitle('division (normalized) time differences', fontsize=16)\n")
    f.write("\n")

    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname)
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.close()

##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu 15 jui 2023 17:45:25 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

import astec.utils.common as common

import ascidian.components.embryoset as cembryoset
import ascidian.components.parameters as cparameters

monitoring = common.Monitoring()


################################################################################
#
# cell count wrt time without and with temporal registration
# 1. with count of all cells
# 2. with count of Epidermis fate cells (allows to temporally register U0126 embryos)
#
################################################################################

def figure_temporal_alignment(embryos, parameters):
    """
    Plot cell number curves without and with linear temporal alignment
    Parameters
    ----------
    embryos
    parameters

    Returns
    -------

    """
    proc = "figure_temporal_alignment"

    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        sys.exit(1)
    if isinstance(parameters, cparameters.FigureParameters) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    filename = 'figure_temporal_alignment'
    figname = 'temporal_alignment_cell_count'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    ref_embryos = embryos.get_embryos()
    embryo_names = list(ref_embryos.keys())

    cells_per_time = {}
    temporal_coefficients = {}
    for n in embryo_names:
        lineage = ref_embryos[n].cell_lineage
        cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
        cells = sorted(cells)
        div = 10 ** ref_embryos[n].time_digits_for_cell_id
        cells_per_time[n] = {}
        for c in cells:
            t = int(c) // div
            cells_per_time[n][t] = cells_per_time[n].get(t, 0) + 1
        temporal_coefficients[n] = ref_embryos[n].temporal_alignment

    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")
    f.write("from operator import itemgetter\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    if file_suffix is not None:
        f.write("cells_per_time{:s} = ".format(file_suffix) + str(cells_per_time) + "\n")
        f.write("t_coefficients{:s} = ".format(file_suffix) + str(temporal_coefficients) + "\n")
    else:
        f.write("cells_per_time = " + str(cells_per_time) + "\n")
        f.write("t_coefficients = " + str(temporal_coefficients) + "\n")

    f.write("\n")
    f.write("fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True, figsize=(16, 8), constrained_layout=True)\n")
    f.write("labels = []\n")

    f.write("\n")
    if file_suffix is not None:
        f.write("for n in cells_per_time{:s}:\n".format(file_suffix))
    else:
        f.write("for n in cells_per_time:\n")
    f.write("    labels += [n]\n")
    if file_suffix is not None:
        f.write("    x = list(cells_per_time{:s}[n].keys())\n".format(file_suffix))
    else:
        f.write("    x = list(cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    if file_suffix is not None:
        f.write("    y = [cells_per_time{:s}[n][i] for i in x]\n".format(file_suffix))
    else:
        f.write("    y = [cells_per_time[n][i] for i in x]\n")
    f.write("    ax1.plot(x, y)\n")
    f.write("# ax1.set_xlim(0, 100)\n")
    f.write("# ax1.set_ylim(0, 100)\n")
    f.write("# ax1.grid()\n")
    f.write("ax1.set_title(\"cell count (without alignment)\", fontsize=15)\n")
    f.write("ax1.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    if file_suffix is not None:
        f.write("for n in cells_per_time{:s}:\n".format(file_suffix))
        f.write("    x = list(cells_per_time{:s}[n].keys())\n".format(file_suffix))
    else:
        f.write("for n in cells_per_time:\n")
        f.write("    x = list(cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    if file_suffix is not None:
        f.write("    t = [t_coefficients{:s}[n][0] * i + ".format(file_suffix))
        f.write("t_coefficients{:s}[n][1] for i in x]\n".format(file_suffix))
        f.write("    y = [cells_per_time{:s}[n][i] for i in x]\n".format(file_suffix))
    else:
        f.write("    t = [t_coefficients[n][0] * i + t_coefficients[n][1] for i in x]\n")
        f.write("    y = [cells_per_time[n][i] for i in x]\n")
    f.write("    ax2.plot(t, y)\n")
    f.write("# ax2.set_xlim(0, 100)\n")
    f.write("# ax2.grid()\n")
    f.write("# xlim = ax2.get_xlim()\n")
    f.write("# ax2.plot([xlim[0], xlim[1]], [64, 64], linestyle='dashed', color='lightgrey')\n")
    f.write("# ax2.plot([xlim[0], xlim[1]], [76, 76], linestyle='dashed', color='lightgrey')\n")
    f.write("# ax2.plot([xlim[0], xlim[1]], [112, 112], linestyle='dashed', color='lightgrey')\n")
    f.write("ax2.set_title(\"cell count (with alignment)\", fontsize=15)\n")
    f.write("ax2.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_1")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    #
    # this is inspired from cell_composition.py
    # interpolate time for every cell count
    #

    f.write("\n")
    if file_suffix is not None:
        f.write("times_per_ncell{:s}".format(file_suffix))
        f.write(" = {}\n")
        f.write("for n in cells_per_time{:s}:\n".format(file_suffix))
        f.write("    listtuples = [(t_coefficients{:s}[n][0] * t + ".format(file_suffix))
        f.write("t_coefficients{:s}[n][1], c)".format(file_suffix))
        f.write(" for t, c in cells_per_time{:s}[n].items()]\n".format(file_suffix))
    else:
        f.write("times_per_ncell = {}\n")
        f.write("for n in cells_per_time:\n")
        f.write("    listtuples = [(t_coefficients[n][0] * t + t_coefficients[n][1], c)")
        f.write(" for t, c in cells_per_time[n].items()]\n")
    f.write("    listtuples = sorted(listtuples, key=itemgetter(0))\n")
    f.write("    for i, c in enumerate(listtuples):\n")
    if file_suffix is not None:
        f.write("        times_per_ncell{:s}[c[1]] = ".format(file_suffix))
        f.write("times_per_ncell{:s}.get(c[1], []) + [c[0]]\n".format(file_suffix))
    else:
        f.write("        times_per_ncell[c[1]] = times_per_ncell.get(c[1], []) + [c[0]]\n")
    f.write("        if i == len(listtuples) - 1:\n")
    f.write("            continue\n")
    f.write("        if listtuples[i + 1][1] == c[1]:\n")
    f.write("            continue\n")
    f.write("        for d in range(int(c[1]) + 1, int(listtuples[i + 1][1])):\n")
    f.write("            dc = (d - c[1]) / (listtuples[i + 1][1] - c[1])\n")
    f.write("            dt = c[0] + dc * (listtuples[i + 1][0] - c[0])\n")
    if file_suffix is not None:
        f.write("            times_per_ncell{:s}[d]".format(file_suffix))
        f.write(" = times_per_ncell{:s}.get(d, []) + [dt]\n".format(file_suffix))
    else:
        f.write("            times_per_ncell[d] = times_per_ncell.get(d, []) + [dt]\n")

    #
    # statistics on times par cell count
    #
    f.write("\n")
    if file_suffix is not None:
        f.write("ncells{:s} = sorted(list(times_per_ncell{:s}.keys()))\n".format(file_suffix, file_suffix))
        f.write("mint{:s} = [np.min(times_per_ncell{:s}[n])".format(file_suffix, file_suffix))
        f.write(" for n in ncells{:s}]\n".format(file_suffix))
        f.write("maxt{:s} = [np.max(times_per_ncell{:s}[n])".format(file_suffix, file_suffix))
        f.write(" for n in ncells{:s}]\n".format(file_suffix))
        f.write("meant{:s} = [np.mean(times_per_ncell{:s}[n])".format(file_suffix, file_suffix))
        f.write(" for n in ncells{:s}]\n".format(file_suffix))
    else:
        f.write("ncells = sorted(list(times_per_ncell.keys()))\n")
        f.write("mint = [np.min(times_per_ncell[n]) for n in ncells]\n")
        f.write("maxt = [np.max(times_per_ncell[n]) for n in ncells]\n")
        f.write("meant = [np.mean(times_per_ncell[n]) for n in ncells]\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(8, 8), constrained_layout=True)\n")
    if file_suffix is not None:
        f.write("plt.fill(np.append(mint{:s}, maxt{:s}[::-1]),".format(file_suffix, file_suffix))
        f.write(" np.append(ncells{:s}, ncells{:s}[::-1]), 'lightgrey')\n".format(file_suffix, file_suffix))
        f.write("ax.plot(meant{:s}, ncells{:s})\n".format(file_suffix, file_suffix))
    else:
        f.write("plt.fill(np.append(mint, maxt[::-1]), np.append(ncells, ncells[::-1]), 'lightgrey')\n")
        f.write("ax.plot(meant, ncells)\n")
    f.write("# ax.set_xlim(-20, 125)\n")
    f.write("# ax.set_ylim(0, 500)\n")
    f.write("ax.set_title(\"average time per cell count (with alignment)\", fontsize=15)\n")
    f.write("ax.set_xlabel('time')\n")
    f.write("ax.set_ylabel('cell count')\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_2")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.close()


def figure_temporal_alignment_epidermis(embryos, parameters):
    """
    Plot cell number curves without and with linear temporal alignment
    Parameters
    ----------
    embryos
    parameters

    Returns
    -------

    """
    proc = "figure_temporal_alignment_epidermis"

    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        monitoring.to_log_and_console("\t do not generate figure")
        return
    if isinstance(parameters, cparameters.FigureParameters) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: " +
                                      str(type(parameters)))
        sys.exit(1)

    filename = 'figure_temporal_alignment_epidermis'
    figname = 'temporal_alignment_epidermis'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    embryos.compute_temporal_alignment(fate='Epidermis')
    ref_embryos = embryos.get_embryos()
    embryo_names = list(ref_embryos.keys())

    epidermis_cells_per_time = {}
    total_cells_per_time = {}
    temporal_coefficients = {}
    endoderm_cells_per_time = {}
    neuralplate_cells_per_time = {}
    notepidermis_cells_per_time = {}
    other_cells_per_time = {}
    for n in embryo_names:
        epidermis_cells_per_time[n] = ref_embryos[n].get_cells_per_time(fate='Epidermis')
        notepidermis_cells_per_time[n] = ref_embryos[n].get_cells_per_time(nonfate='Epidermis')
        total_cells_per_time[n] = ref_embryos[n].get_cells_per_time()
        temporal_coefficients[n] = ref_embryos[n].get_temporal_alignment(key='Epidermis')
        endoderm_cells_per_time[n] = ref_embryos[n].get_cells_per_time(fate='Endoderm')
        neuralplate_cells_per_time[n] = ref_embryos[n].get_cells_per_time(fate='Neural Plate')
        nonfate = ['Epidermis', 'Endoderm', 'Neural Plate']
        other_cells_per_time[n] = ref_embryos[n].get_cells_per_time(nonfate=nonfate)

    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    f.write("total_cells_per_time = " + str(total_cells_per_time) + "\n")
    f.write("\n")
    f.write("epidermis_cells_per_time = " + str(epidermis_cells_per_time) + "\n")
    f.write("\n")
    f.write("notepidermis_cells_per_time = " + str(notepidermis_cells_per_time) + "\n")
    f.write("\n")
    f.write("endoderm_cells_per_time = " + str(endoderm_cells_per_time) + "\n")
    f.write("\n")
    f.write("neuralplate_cells_per_time = " + str(neuralplate_cells_per_time) + "\n")
    f.write("\n")
    f.write("other_cells_per_time = " + str(other_cells_per_time) + "\n")
    f.write("\n")
    f.write("temporal_coefficients = " + str(temporal_coefficients) + "\n")

    #
    # fig #1
    #

    f.write("\n")
    f.write("fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True, figsize=(16, 8), constrained_layout=True)\n")
    f.write("labels = []\n")
    f.write("for n in epidermis_cells_per_time:\n")
    f.write("    labels += [n]\n")
    f.write("    x = list(epidermis_cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    y = [epidermis_cells_per_time[n][i] for i in x]\n")
    f.write("    ax1.plot(x, y)\n")
    f.write("# ax1.set_xlim(0, 100)\n")
    f.write("# ax1.set_ylim(0, 200)\n")
    f.write("# ax1.grid()\n")
    f.write("ax1.set_title('Epidermis cell count (without alignment)', fontsize=15)\n")
    f.write("ax1.legend(labels, prop={'size': 10})\n")

    f.write("for n in epidermis_cells_per_time:\n")
    f.write("    x = list(epidermis_cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    y = [epidermis_cells_per_time[n][i] for i in x]\n")
    f.write("    ax2.plot(t, y)\n")
    f.write("# ax2.set_xlim(0, 100)\n")
    f.write("# ax2b = ax2.twinx()\n")
    f.write("# ax2b.set_ylim(0, 200)\n")
    f.write("# ax2b.set_yticks([8, 14, 26, 48, 96, 192])\n")
    f.write("# ax2.plot([0, 100], [8, 8], linestyle='dashed', color='grey')\n")
    f.write("# ax2.plot([0, 100], [14, 14], linestyle='dashed', color='grey')\n")
    f.write("# ax2.plot([0, 100], [26, 26], linestyle='dashed', color='grey')\n")
    f.write("# ax2.plot([0, 100], [48, 48], linestyle='dashed', color='grey')\n")
    f.write("# ax2.plot([0, 100], [96, 96], linestyle='dashed', color='grey')\n")
    f.write("# ax2.plot([0, 100], [192, 192], linestyle='dashed', color='grey')\n")
    f.write("ax2.set_title('Epidermis cell count (with alignment)', fontsize=15)\n")
    f.write("ax2.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_1")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    #
    # fig #2
    #

    f.write("\n")
    f.write("\n")
    f.write("fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True, figsize=(16, 8), constrained_layout=True)\n")
    f.write("labels = []\n")
    f.write("color = []\n")
    f.write("for n in epidermis_cells_per_time:\n")
    f.write("    labels += [n]\n")
    f.write("    x = list(epidermis_cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    tc = [total_cells_per_time[n][i] for i in x]\n")
    f.write("    p = ax1.plot(t, tc)\n")
    f.write("    color += [p[0].get_color()]\n")
    f.write("for j, n in enumerate(epidermis_cells_per_time):\n")
    f.write("    x = list(epidermis_cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    ec = [epidermis_cells_per_time[n][i] for i in x]\n")
    f.write("    ax1.plot(t, ec, linestyle='dashed', color=color[j])\n")

    f.write("\n")
    f.write("# ax1.set_xlim(0, 100)\n")
    f.write("ax1.set_ylim(0, 400)\n")
    f.write("xmin, xmax = ax1.get_xlim()\n")
    f.write("ymin, ymax = ax1.get_ylim()\n")
    f.write("# ax1.grid()\n")
    f.write("ax1b = ax1.twinx()\n")
    f.write("ax1b.set_ylim(ymin, ymax)\n")
    f.write("ax1b.set_yticks([26, 48, 64, 76, 96, 112, 192, 184, 218])\n")
    f.write("for y in [26, 48, 96, 192]:\n")
    f.write("    ax1.plot([xmin, xmax], [y, y], linestyle='dashed', color='grey')\n")
    f.write("for x in [-5.55, 20.83, 48.61, 86.1]:\n")
    f.write("    ax1.plot([x, x], [ymin, ymax], linestyle='dashed', color='grey')\n")
    f.write("for y in [64, 76, 112, 184, 218]:\n")
    f.write("    ax1.plot([xmin, xmax], [y, y], linestyle='dotted', color='grey')\n")
    f.write("ax1.set_title('Total and Epidermis cell count (Epidermis alignment)', fontsize=15)\n")
    f.write("ax1.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("labels = []\n")
    f.write("color = []\n")
    f.write("for n in epidermis_cells_per_time:\n")
    f.write("    labels += [n]\n")
    f.write("    x = list(epidermis_cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    tc = [total_cells_per_time[n][i] for i in x]\n")
    f.write("    p = ax2.plot(t, tc)\n")
    f.write("    color += [p[0].get_color()]\n")
    f.write("for j, n in enumerate(epidermis_cells_per_time):\n")
    f.write("    x = list(epidermis_cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    ne = [notepidermis_cells_per_time[n][i] for i in x]\n")
    f.write("    ax2.plot(t, ne, linestyle='dashed', color=color[j])\n")

    f.write("\n")
    f.write("# ax2.set_xlim(0, 100)\n")
    f.write("ax2.set_ylim(0, 400)\n")
    f.write("xmin, xmax = ax2.get_xlim()\n")
    f.write("ymin, ymax = ax2.get_ylim()\n")
    f.write("# ax2.grid()\n")
    f.write("ax2b = ax2.twinx()\n")
    f.write("ax2b.set_ylim(ymin, ymax)\n")
    f.write("ax2b.set_yticks([64, 76, 112, 184, 218])\n")
    f.write("for y in [26, 48, 96, 192]:\n")
    f.write("    ax2.plot([xmin, xmax], [y, y], linestyle='dashed', color='grey')\n")
    f.write("for y in [64, 76, 112, 184, 218]:\n")
    f.write("    ax2.plot([xmin, xmax], [y, y], linestyle='dotted', color='grey')\n")
    f.write("ax2.set_title('Total and not Epidermis cells count', fontsize=15)\n")
    f.write("ax2.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_2")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    #
    # fig #3
    #

    f.write("\n")
    f.write("\n")
    f.write("fig, (ax1, ax2, ax3, ax4) = plt.subplots(nrows=4, sharex=True, figsize=(8, 16), "
            "constrained_layout=True)\n")
    f.write("labels = []\n")
    f.write("for n in epidermis_cells_per_time:\n")
    f.write("    labels += [n]\n")
    f.write("    x = list(epidermis_cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    ec = [epidermis_cells_per_time[n][i] for i in x]\n")
    f.write("    ax1.plot(t, ec)\n")

    f.write("\n")
    f.write("# ax1.set_xlim(0, 100)\n")
    f.write("ax1.set_ylim(0, 250)\n")
    f.write("xmin, xmax = ax1.get_xlim()\n")
    f.write("ymin, ymax = ax1.get_ylim()\n")
    f.write("# ax1.grid()\n")
    f.write("for x in [-5.55, 20.83, 48.61, 86.1]:\n")
    f.write("    ax1.plot([x, x], [ymin, ymax], linestyle='dashed', color='grey')\n")
    f.write("ax1.set_title('Epidermis cell count (Epidermis alignment)', fontsize=15)\n")
    f.write("ax1.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("labels = []\n")
    f.write("for n in epidermis_cells_per_time:\n")
    f.write("    labels += [n]\n")
    f.write("    x = list(endoderm_cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    ec = [endoderm_cells_per_time[n][i] for i in x]\n")
    f.write("    ax2.plot(t, ec)\n")

    f.write("\n")
    f.write("# ax2.set_xlim(0, 100)\n")
    f.write("ax2.set_ylim(0, 60)\n")
    f.write("xmin, xmax = ax2.get_xlim()\n")
    f.write("ymin, ymax = ax2.get_ylim()\n")
    f.write("# ax2.grid()\n")
    f.write("for x in [-5.55, 20.83, 48.61, 86.1]:\n")
    f.write("    ax2.plot([x, x], [ymin, ymax], linestyle='dashed', color='grey')\n")
    f.write("ax2.set_title('Endoderm cell count (Epidermis alignment)', fontsize=15)\n")
    f.write("ax2.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("labels = []\n")
    f.write("for n in epidermis_cells_per_time:\n")
    f.write("    labels += [n]\n")
    f.write("    x = list(neuralplate_cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    ec = [neuralplate_cells_per_time[n][i] for i in x]\n")
    f.write("    ax3.plot(t, ec)\n")

    f.write("\n")
    f.write("# ax3.set_xlim(0, 100)\n")
    f.write("ax3.set_ylim(0, 100)\n")
    f.write("xmin, xmax = ax3.get_xlim()\n")
    f.write("ymin, ymax = ax3.get_ylim()\n")
    f.write("# ax3.grid()\n")
    f.write("for x in [-5.55, 20.83, 48.61, 86.1]:\n")
    f.write("    ax3.plot([x, x], [ymin, ymax], linestyle='dashed', color='grey')\n")
    f.write("ax3.set_title('Neural plate cell count (Epidermis alignment)', fontsize=15)\n")
    f.write("ax3.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("labels = []\n")
    f.write("for n in epidermis_cells_per_time:\n")
    f.write("    labels += [n]\n")
    f.write("    x = list(other_cells_per_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    ec = [other_cells_per_time[n][i] for i in x]\n")
    f.write("    ax4.plot(t, ec)\n")

    f.write("\n")
    f.write("# ax4.set_xlim(0, 100)\n")
    f.write("ax4.set_ylim(0, 100)\n")
    f.write("xmin, xmax = ax4.get_xlim()\n")
    f.write("ymin, ymax = ax4.get_ylim()\n")
    f.write("# ax4.grid()\n")
    f.write("for x in [-5.55, 20.83, 48.61, 86.1]:\n")
    f.write("    ax4.plot([x, x], [ymin, ymax], linestyle='dashed', color='grey')\n")
    f.write("ax4.set_title('Other cell count (Epidermis alignment)', fontsize=15)\n")
    f.write("ax4.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_3")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.close()

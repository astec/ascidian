##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Lun 15 jul 2024 16:14:32 CEST
#
##############################################################
#
#
#
##############################################################

import copy
import os
import statistics
import sys
import time

import numpy as np

import astec.utils.common as common

import ascidian.ascidian.name as aname
import ascidian.core.cell_signature as ccellsign
import ascidian.components.embryoset as cembryoset
import ascidian.naming_timepoint.parameters as ntparameters
import ascidian.naming_timepoint.utils as ntutils

monitoring = common.Monitoring()


############################################################
#
#
#
############################################################


def _cell_signature_atlases(test_embryos, ref_embryos, skipped_embryos, timepoint, parameters, suffix="sym-"):

    scores = {}

    computeddata = ntutils.get_precomputeddata(parameters, parameters.cell_similarity_filename)
    cell_similarity_key = parameters.matching_cell_similarity.lower()
    if cell_similarity_key not in computeddata:
        computeddata[cell_similarity_key] = {}

    for ta in test_embryos:

        if ta in skipped_embryos:
            continue

        if len(ta) > len(suffix) and ta[0:len(suffix)] == suffix:
            msg = "will remove test atlas '" + str(ta) + "' from figure experiment, it has been symmetrized?!"
            monitoring.to_log_and_console("       ... " + msg)
            continue

        if test_embryos[ta].cell_contact_surface is None or test_embryos[ta].cell_barycenter is None:
            skipped_embryos += [ta]
            msg = "will remove test atlas '" + str(ta)
            msg += "' from figure experiment, some properties are missing"
            monitoring.to_log_and_console("       ... " + msg)
            continue

        if test_embryos[ta].cell_name is None:
            skipped_embryos += [ta]
            msg = "will remove test atlas '" + str(ta)
            msg += "' from figure experiment, name property is missing"
            monitoring.to_log_and_console("       ... " + msg)
            continue

        if ta not in computeddata[cell_similarity_key]:
            computeddata[cell_similarity_key][ta] = {}
        if timepoint[ta] not in computeddata[cell_similarity_key][ta]:
            computeddata[cell_similarity_key][ta][timepoint[ta]] = {}

        scores[ta] = {}
        computed_data_has_to_saved = False

        for ra in ref_embryos:

            if ta == ra:
                continue
            if ra in skipped_embryos:
                continue
            if len(ra) == len(suffix) + len(ta) and ra[len(suffix):] == ta:
                # msg = "will not compare test atlas '" + str(ta) + "' with its symmetrized version"
                # monitoring.to_log_and_console("       ... " + msg)
                continue

            if ref_embryos[ra].cell_name is None or ref_embryos[ra].cell_contact_surface is None or \
                    ref_embryos[ra].cell_barycenter is None:
                skipped_embryos += [ra]
                msg = "will remove reference atlas '" + str(ra)
                msg += "' from figure experiment, some properties are missing"
                monitoring.to_log_and_console("       ... " + msg)
                continue

            if ra not in computeddata[cell_similarity_key][ta][timepoint[ta]]:
                computeddata[cell_similarity_key][ta][timepoint[ta]][ra] = {}

            if timepoint[ra] in computeddata[cell_similarity_key][ta][timepoint[ta]][ra]:
                msg = "get scores of test embryo " + str(ta) + " with reference atlas " + str(ra)
                monitoring.to_log_and_console("       ... " + msg)
                scores[ta][ra] = computeddata[cell_similarity_key][ta][timepoint[ta]][ra][timepoint[ra]]
            else:
                msg = "compare test embryo " + str(ta) + " with reference atlas " + str(ra)
                monitoring.to_log_and_console("       ... " + msg)
                start_time = time.localtime()
                scores[ta][ra] = ccellsign.cell_signature(test_embryos[ta], timepoint[ta], ref_embryos[ra],
                                                          timepoint[ra],
                                                          cell_similarity=parameters.matching_cell_similarity,
                                                          z_rotation_angle_increment=parameters.z_rotation_angle_increment,
                                                          processors=parameters.processors, cheat=True)
                end_time = time.localtime()
                msg = "execution time = " + str(time.mktime(end_time) - time.mktime(start_time)) + " s"
                monitoring.to_log_and_console("           " + msg)
                monitoring.update_execution_time(start_time, end_time)
                computeddata[cell_similarity_key][ta][timepoint[ta]][ra][timepoint[ra]] = scores[ta][ra]
                computed_data_has_to_saved = True

        #
        # save computed data (pickle file)
        # the 'naming_by_transfer' variable indicates the file name to be used
        # ie parameters.cell_similarity_filename
        #
        if computed_data_has_to_saved:
            ntutils.save_precomputeddata(computeddata, parameters.cell_similarity_filename)
    
    return scores


############################################################
#
#
#
############################################################


def _name_rank(test_embryos, ref_embryos, scores, symmetric_name=False):
    proc = "_name_rank"

    # score is a dictionary [testEmbryo][refEmbryo][testCell][refCell]
    # values are either a float, the score
    # or a tuple (score, angle)

    ranks = {}
    #
    # loop on test atlases
    #
    for t_atlas in scores:
        if test_embryos[t_atlas].cell_name is None:
            monitoring.to_log_and_console(str(proc) + ": test embryo '" + str(t_atlas) + "' has no names. Skip it")
            continue
        t_names = test_embryos[t_atlas].cell_name

        #
        # loop on reference atlases
        #
        for r_atlas in scores[t_atlas]:
            if ref_embryos[r_atlas].cell_name is None:
                msg = str(proc) + ": reference atlas '" + str(r_atlas) + "' has no names. Skip it"
                monitoring.to_log_and_console(msg)
                continue
            r_names = ref_embryos[r_atlas].cell_name

            #
            # loop on test cells
            #
            for t_cell in scores[t_atlas][r_atlas]:
                t_name = t_names[t_cell]
                if t_name not in ranks:
                    ranks[t_name] = {}
                if t_atlas not in ranks[t_name]:
                    ranks[t_name][t_atlas] = []

                scoresaretuples = False
                for r_cell in scores[t_atlas][r_atlas][t_cell]:
                    if isinstance(scores[t_atlas][r_atlas][t_cell][r_cell], tuple):
                        scoresaretuples = True
                    break

                #
                # sort tuples (name, score) with respect to score
                #
                if scoresaretuples is True:
                    ns = [(r_names[c], scores[t_atlas][r_atlas][t_cell][c][0]) 
                          for c in scores[t_atlas][r_atlas][t_cell]]
                else:
                    ns = [(r_names[c], scores[t_atlas][r_atlas][t_cell][c]) for c in scores[t_atlas][r_atlas][t_cell]]
                ns = sorted(ns, key=lambda x: x[1])

                #
                # get sorted names and retrieve the index
                #
                n = [c[0] for c in ns]

                #
                # rank of cell named t_name (or get_symmetric_name(t_name)) in atlas r_atlas
                #
                if symmetric_name is True:
                    if aname.get_symmetric_name(t_name) not in n:
                        continue
                    i = n.index(aname.get_symmetric_name(t_name))
                else:
                    if t_name not in n:
                        continue
                    i = n.index(t_name)

                #
                # keep rank and score (and angle)
                #
                ranks[t_name][t_atlas] += [(i + 1, ns[i][1])]

    return ranks


def _minvalues(test_embryos, ref_embryos, scores):
    proc = "_minvalues"

    # score is a dictionary [testEmbryo][refEmbryo][testCell][refCell]
    # values are either a float, the score
    # or a tuple (score, angle)

    minvalues = {}
    #
    # loop on test atlases
    #
    for t_atlas in scores:
        if test_embryos[t_atlas].cell_name is None:
            monitoring.to_log_and_console(str(proc) + ": test embryo '" + str(t_atlas) + "' has no names. Skip it")
            continue
        t_names = test_embryos[t_atlas].cell_name

        #
        # loop on reference atlases
        #
        for r_atlas in scores[t_atlas]:
            if ref_embryos[r_atlas].cell_name is None:
                msg = str(proc) + ": reference atlas '" + str(r_atlas) + "' has no names. Skip it"
                monitoring.to_log_and_console(msg)
                continue
            r_names = ref_embryos[r_atlas].cell_name

            #
            # loop on test cells
            #
            for t_cell in scores[t_atlas][r_atlas]:
                t_name = t_names[t_cell]
                if t_name not in minvalues:
                    minvalues[t_name] = {}
                if t_atlas not in minvalues[t_name]:
                    minvalues[t_name][t_atlas] = []

                scoresaretuples = False
                for r_cell in scores[t_atlas][r_atlas][t_cell]:
                    if isinstance(scores[t_atlas][r_atlas][t_cell][r_cell], tuple):
                        scoresaretuples = True
                    break

                #
                # sort tuples (name, score) with respect to score
                #
                if scoresaretuples is True:
                    ns = [(r_names[c], scores[t_atlas][r_atlas][t_cell][c][0])
                          for c in scores[t_atlas][r_atlas][t_cell]]
                else:
                    ns = [(r_names[c], scores[t_atlas][r_atlas][t_cell][c]) for c in scores[t_atlas][r_atlas][t_cell]]
                ns = sorted(ns, key=lambda x: x[1])

                #
                # keep minimal score
                #
                minvalues[t_name][t_atlas] += [ns[0][1]]

    return minvalues


def _name_angle(test_atlases, ref_atlases, scores):
    proc = "_name_angle"

    # score is a dictionary [testAtlas][refAtlas][testCell][refCell]
    # values are either a float, the score
    # or a tuple (score, angle)

    angles = {}
    #
    # loop on test atlases
    #
    for t_atlas in scores:
        if test_atlases[t_atlas].cell_name is None:
            monitoring.to_log_and_console(str(proc) + ": test embryo '" + str(t_atlas) + "' has no names. Skip it")
            continue
        t_names = test_atlases[t_atlas].cell_name
        angles[t_atlas] = {}

        #
        # loop on reference atlases
        #
        for r_atlas in scores[t_atlas]:
            if ref_atlases[r_atlas].cell_name is None:
                msg = str(proc) + ": reference atlas '" + str(r_atlas) + "' has no names. Skip it"
                monitoring.to_log_and_console(msg)
                continue
            r_names = ref_atlases[r_atlas].cell_name
            angles[t_atlas][r_atlas] = {}

            #
            # loop on test cells
            #
            for t_cell in scores[t_atlas][r_atlas]:
                t_name = t_names[t_cell]

                for r_cell in scores[t_atlas][r_atlas][t_cell]:
                    if not isinstance(scores[t_atlas][r_atlas][t_cell][r_cell], tuple):
                        break
                    if r_names[r_cell] == t_name:
                        angles[t_atlas][r_atlas][t_name] = scores[t_atlas][r_atlas][t_cell][r_cell][1]
                        break
    return angles


############################################################
#
#
#
############################################################


def _compute_timepoints(test_atlases, ref_atlases, cell_number):
    proc = "_compute_timepoints"
    #
    # timepoints
    #
    skipped_atlases = []
    timepoint = {}
    for ta in test_atlases:
        cell_count, timerange, ininterval = test_atlases[ta].get_timerange(cell_number, change_cell_number=False)
        if cell_count is None:
            skipped_atlases += [ta]
            msg = "    " + str(proc) + ": will remove test atlas '" + str(ta) + "' from figure experiment\n"
            msg += "        requested cell number is " + str(cell_number)
            monitoring.to_log_and_console(msg)
            continue
        timepoint[ta] = statistics.median_low(timerange)
    for ra in ref_atlases:
        if ra in skipped_atlases:
            continue
        if ra in timepoint:
            continue
        cell_count, timerange, ininterval = ref_atlases[ra].get_timerange(cell_number, change_cell_number=False)
        if cell_count is None:
            skipped_atlases += [ra]
            msg = "    " + str(proc) + ": will remove reference atlas '" + str(ra) + "' from figure experiment\n"
            msg += "        requested cell number is " + str(cell_number)
            monitoring.to_log_and_console(msg)
            continue
        timepoint[ra] = statistics.median_low(timerange)
    return skipped_atlases, timepoint


############################################################
#
#
#
############################################################

def figure_cell_signature(ref_embryos, parameters, test_embryos=None):
    #
    # plot the count of right, wrong and unknown wrt the number of used atlases
    # for a given cell count
    #
    proc = "figure_cell_signature"
    sym_prefix = ref_embryos.symprefix

    if test_embryos is not None:
        if isinstance(test_embryos, cembryoset.EmbryoSet) is False:
            monitoring.to_log_and_console(str(proc) + ": unexpected type for 'testEmbryos' variable: "
                                          + str(type(test_embryos)))
            monitoring.to_log_and_console("\t do not generate figure")
            return
    else:
        test_embryos = copy.deepcopy(ref_embryos)
    if isinstance(ref_embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'refEmbryos' variable: " 
                                      + str(type(ref_embryos)))
        monitoring.to_log_and_console("\t do not generate figure")
        return
    if isinstance(parameters, ntparameters.NamingTimepointParameters) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: " +
                                      str(type(parameters)))

        sys.exit(1)

    #
    # copying the reference atlases
    # it allows to compute different vectors for test and ref atlases
    #
    local_ref_embryos = copy.deepcopy(ref_embryos)
    monitoring.to_log_and_console("   ... symmetrize reference atlases")
    local_ref_embryos.add_symmetrical_embryos()

    #
    # skipped_atlases is a list of atlas names to be skipped
    # timepoint is s dictionary indexed by embryo name, giving the time point to be studied
    #
    monitoring.to_log_and_console("   ... compute time points")
    skipped_atlases, timepoint = _compute_timepoints(test_embryos.get_embryos(), local_ref_embryos.get_embryos(),
                                                     parameters.cell_number)

    #
    # scores is an embedded dictionary [test_atlas_key][ref_atlas_key][test_cell_id][ref_cell_id]
    # values are either float (score) for parameters.cell_similarity == 'sorted-contact'
    # or tuple (score, angle) for parameters.cell_similarity == 'winged-contact':
    #
    monitoring.to_log_and_console("   ... compute scores")
    scores = _cell_signature_atlases(test_embryos.get_embryos(), local_ref_embryos.get_embryos(), skipped_atlases,
                                     timepoint, parameters, suffix=sym_prefix)

    #
    # ranks is an embedded dictionary [cell_name][test_atlas_name] = [list of tuples]
    # a tuple is (rank (start at 1), score)
    # a tuple describes when the cell 'cell_name' is found in reference atlases
    # scores are assumed be real (usually in [0,1], 0 being the best)
    #
    monitoring.to_log_and_console("   ... compute ranks")
    ranks = _name_rank(test_embryos.get_embryos(), local_ref_embryos.get_embryos(), scores, symmetric_name=False)
    symranks = _name_rank(test_embryos.get_embryos(), local_ref_embryos.get_embryos(), scores, symmetric_name=True)

    #
    # angles is computed only if scores are tuples with a length of
    # it is and embedded dictionary [test_atlas_name][ref_atlas_name][cell_name] = angle
    #   it gives the 2D rotation 'angle' found for 'cell_name' when testing 'test_atlas_name' against 'ref_atlas_name'
    #
    monitoring.to_log_and_console("   ... compute angles")
    angles = None
    for ta in scores:
        for ra in scores[ta]:
            for tc in scores[ta][ra]:
                for rc in scores[ta][ra][tc]:
                    #
                    # sorted
                    #
                    if isinstance(scores[ta][ra][tc][rc], np.float64):
                        break
                    if isinstance(scores[ta][ra][tc][rc], tuple) and len(scores[ta][ra][tc][rc]) > 2:
                        monitoring.to_log_and_console("   ... compute angles")
                        angles = _name_angle(test_embryos.get_embryos(), local_ref_embryos.get_embryos(), scores)
                    break
                break
            break
        break

    #
    # minvalues is an embedded dictionary
    # [cell_name][test_atlas_name] = [list of scores]
    #   the list of minimal scores found for 'cell_name' from 'test_atlas_name'
    #
    monitoring.to_log_and_console("   ... compute min scores")
    minvalues = _minvalues(test_embryos.get_embryos(), local_ref_embryos.get_embryos(), scores)

    #
    #
    #
    filename = 'figure_cell_signature'
    figname = 'cell_signature'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    #
    #
    #
    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    if file_suffix is not None:
        f.write("ranks{:s} = ".format(file_suffix) + str(ranks) + "\n")
    else:
        f.write("ranks = ".format(file_suffix) + str(ranks) + "\n")

    f.write("\n")
    if file_suffix is not None:
        f.write("symranks{:s} = ".format(file_suffix) + str(symranks) + "\n")
    else:
        f.write("symranks = ".format(file_suffix) + str(symranks) + "\n")

    if angles is not None:
        f.write("\n")
        if file_suffix is not None:
            f.write("angles{:s} = ".format(file_suffix) + str(angles) + "\n")
        else:
            f.write("angles = ".format(file_suffix) + str(angles) + "\n")

    f.write("\n")
    if file_suffix is not None:
        f.write("min_scores{:s} = ".format(file_suffix) + str(minvalues) + "\n")
    else:
        f.write("min_scores = ".format(file_suffix) + str(minvalues) + "\n")

    f.write("\n")
    if file_suffix is not None:
        f.write("cells{0} = sorted(list(ranks{0}.keys()))\n".format(file_suffix))
        # f.write("rcells{0} = [c for c in cells{0} if c[-1] == '*']\n".format(file_suffix))
        # f.write("lcells{0} = [c for c in cells{0} if c[-1] == '_']\n".format(file_suffix))
    else:
        f.write("cells = sorted(list(ranks.keys()))\n")
        # f.write("rcells = [c for c in cells if c[-1] == '*']\n")
        # f.write("lcells = [c for c in cells if c[-1] == '_']\n")

    f.write("\n")
    # rank of the cell
    if file_suffix is not None:
        f.write("cell_ranks{0} = [[t[0] for a in ranks{0}[c]".format(file_suffix))
        f.write(" for t in ranks{0}[c][a]] for c in cells{0}]\n".format(file_suffix))
    else:
        f.write("cell_ranks = [[t[0] for a in ranks[c] for t in ranks[c][a]] for c in cells]\n")
    # rank of the symmetrical cell
    f.write("\n")
    if file_suffix is not None:
        f.write("cell_symranks{0} = [[t[0] for a in symranks{0}[c]".format(file_suffix))
        f.write(" for t in symranks{0}[c][a]] for c in cells{0}]\n".format(file_suffix))
    else:
        f.write("cell_symranks = [[t[0] for a in symranks[c] for t in symranks[c][a]] for c in cells]\n")

    f.write("\n")
    if file_suffix is not None:
        f.write("cell_scores{0} = [[t[1] for a in ranks{0}[c]".format(file_suffix))
        f.write(" for t in ranks{0}[c][a]] for c in cells{0}]\n".format(file_suffix))
    else:
        f.write("cell_scores = [[t[1] for a in ranks[c] for t in ranks[c][a]] for c in cells]\n")

    f.write("\n")
    if file_suffix is not None:
        f.write("cell_min_scores{0} = [[t for a in min_scores{0}[c]".format(file_suffix))
        f.write(" for t in min_scores{0}[c][a]] for c in cells{0}]\n".format(file_suffix))
    else:
        f.write("cell_min_scores = [[t for a in min_scores[c] for t in min_scores[c][a]] for c in cells]\n")

    f.write("\n")
    if file_suffix is not None:
        f.write("xvalues{0} = [y + 1 for y in range(len(cells{0}))]\n".format(file_suffix))
    else:
        f.write("xvalues = [y + 1 for y in range(len(cells))]\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(16, 6.5))\n")
    if file_suffix is not None:
        f.write("rbox_ranks{0} = ax.boxplot(cell_ranks{0}, patch_artist=False)\n".format(file_suffix))
        f.write("ax.set_xticks(xvalues{0}, cells{0}, rotation=80, fontsize=10)\n".format(file_suffix))
    else:
        f.write("rbox_ranks = ax.boxplot(cell_ranks, patch_artist=False)\n")
        f.write("ax.set_xticks(xvalues, cells, rotation=80, fontsize=10)\n")
    f.write("ax.set_ylim(-5, 69)\n")
    f.write("ax.set_ylabel('cell rank', fontsize=10)\n")
    f.write("ax.set_title(\"rank of the same cell (similarity '" + str(parameters.matching_cell_similarity))
    f.write("', {:d} cells count)\", fontsize=15)\n".format(parameters.cell_number))
    f.write("ax.tick_params(axis='y', labelsize=10)\n")
    f.write("plt.tight_layout()\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_rank_boxplot")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(16, 6.5))\n")
    if file_suffix is not None:
        f.write("rbox_values{0} = ax.boxplot(cell_scores{0}, patch_artist=False)\n".format(file_suffix))
        f.write("ax.set_xticks(xvalues{0}, cells{0}, rotation=80, fontsize=10)\n".format(file_suffix))
    else:
        f.write("rbox_values = ax.boxplot(cell_scores, patch_artist=False)\n")
        f.write("ax.set_xticks(xvalues, cells, rotation=80, fontsize=10)\n")
    f.write("ax.set_ylim(0, 1)\n")
    f.write("ax.set_ylabel('score value', fontsize=10)\n")
    f.write("ax.set_title(\"similarity value of the same cell")
    f.write(" ( similarity '" + str(parameters.matching_cell_similarity))
    f.write("', {:d} cells count)\", fontsize=15)\n".format(parameters.cell_number))
    f.write("ax.tick_params(axis='y', labelsize=10)\n")
    f.write("plt.tight_layout()\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_score_boxplot")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.write("\n")
    f.write("if False:\n")
    f.write("    fig, ax = plt.subplots(figsize=(16, 6.5))\n")
    if file_suffix is not None:
        f.write("    rbox_symranks{0} = ax.boxplot(cell_symranks{0}, patch_artist=False)\n".format(file_suffix))
        f.write("    ax.set_xticks(xvalues{0}, cells{0}, rotation=80, fontsize=10)\n".format(file_suffix))
    else:
        f.write("    rbox_symranks = ax.boxplot(cell_symranks, patch_artist=False)\n")
        f.write("    ax.set_xticks(xvalues, cells, rotation=80, fontsize=10)\n")
    f.write("    ax.set_ylim(-5, 69)\n")
    f.write("    ax.set_ylabel('cell rank', fontsize=10)\n")
    f.write("    ax.set_title(\"symmetrical cell similarity '" + str(parameters.matching_cell_similarity))
    f.write("' assessment by rank (at {:d} cells count)\", fontsize=15)\n".format(parameters.cell_number))
    f.write("    ax.tick_params(axis='y', labelsize=10)\n")
    f.write("    plt.tight_layout()\n")

    f.write("\n")
    f.write("    if savefig:\n")
    f.write("        plt.savefig('" + figname + "_symmetric_rank_boxplot")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("    else:\n")
    f.write("        plt.show()\n")
    f.write("        plt.close()\n")

    f.write("\n")
    f.write("if False:\n")
    if file_suffix is not None:
        f.write("    rmed_ranks{0} = np.array([b.get_ydata()[0] for b in rbox_ranks{0}['medians']])\n".format(file_suffix))
        f.write("    lq_ranks{0} = np.array([b.get_ydata()[0] for b in rbox_ranks{0}['boxes']])\n".format(file_suffix))
        f.write("    hq_ranks{0} = np.array([b.get_ydata()[2] for b in rbox_ranks{0}['boxes']])\n".format(file_suffix))
    else:
        f.write("    rmed_ranks = np.array([b.get_ydata()[0] for b in rbox_ranks['medians']])\n")
        f.write("    lq_ranks = np.array([b.get_ydata()[0] for b in rbox_ranks['boxes']])\n")
        f.write("    hq_ranks  = np.array([b.get_ydata()[2] for b in rbox_ranks['boxes']])\n")

    f.write("\n")
    f.write("    fig, ax = plt.subplots(figsize=(16, 6.5))\n")
    if file_suffix is not None:
        v = "xvalues{0}, rmed_ranks{0}, yerr=[rmed_ranks{0}-lq_ranks{0}, hq_ranks{0}-rmed_ranks{0}]".format(file_suffix)
        f.write("    ax.errorbar(" + v + ", color='red', linewidth=2, ecolor='red', elinewidth=1")
        f.write(", label='{0}')\n".format(parameters.figurefile_suffix))
        f.write("    ax.set_xticks(xvalues{0}, cells{0}, rotation=80, fontsize=10)\n".format(file_suffix))
        f.write("    ax.legend(loc='upper right')\n")
    else:
        v = "xvalues, rmed_ranks, yerr=[rmed_ranks-lq_ranks, hq_ranks-rmed_ranks]"
        f.write("    ax.errorbar(" + v + ", color='red', linewidth=2, ecolor='red', elinewidth=2)\n")
        f.write("    ax.set_xticks(xvalues, cells, rotation=80, fontsize=10)\n")
    f.write("    ax.set_ylim(-5, 69)\n")
    f.write("    ax.set_ylabel('cell rank', fontsize=10)\n")
    f.write("    ax.set_title(\"cell similarity '" + str(parameters.matching_cell_similarity))
    f.write("' assessment by rank (at {:d} cells count)\", fontsize=15)\n".format(parameters.cell_number))
    f.write("    ax.tick_params(axis='y', labelsize=10)\n")
    f.write("    plt.tight_layout()\n")

    f.write("\n")
    f.write("    if savefig:\n")
    f.write("        plt.savefig('" + figname + "_rank_errorbar")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("    else:\n")
    f.write("        plt.show()\n")
    f.write("        plt.close()\n")

    f.write("\n")
    if file_suffix is not None:
        f.write("rbox_values{0} = ax.boxplot(cell_scores{0}, patch_artist=False)\n".format(file_suffix))
        f.write("rmed_values{0} = np.array([b.get_ydata()[0] for b in rbox_values{0}['medians']])\n".format(file_suffix))
        f.write("lq_values{0} = np.array([b.get_ydata()[0] for b in rbox_values{0}['boxes']])\n".format(file_suffix))
        f.write("hq_values{0} = np.array([b.get_ydata()[2] for b in rbox_values{0}['boxes']])\n".format(file_suffix))
    else:
        f.write("rbox_values = ax.boxplot(cell_scores, patch_artist=False)\n")
        f.write("rmed_values = np.array([b.get_ydata()[0] for b in rbox_values{0}['medians']])\n")
        f.write("lq_values{ = np.array([b.get_ydata()[0] for b in rbox_values{0}['boxes']])\n")
        f.write("hq_values = np.array([b.get_ydata()[2] for b in rbox_values{0}['boxes']])\n")
    f.write("\n")
    if file_suffix is not None:
        f.write("rbox_min_values{0} = ax.boxplot(cell_min_scores{0}, patch_artist=False)\n".format(file_suffix))
        f.write("rmed_min_values{0} = np.array([b.get_ydata()[0] for b in rbox_min_values{0}['medians']])\n".format(file_suffix))
        f.write("lq_min_values{0} = np.array([b.get_ydata()[0] for b in rbox_min_values{0}['boxes']])\n".format(file_suffix))
        f.write("hq_min_values{0} = np.array([b.get_ydata()[2] for b in rbox_min_values{0}['boxes']])\n".format(file_suffix))
    else:
        f.write("rbox_min_values = ax.boxplot(cell_min_scores{0}, patch_artist=False)\n")
        f.write("rmed_min_values = np.array([b.get_ydata()[0] for b in rbox_min_values{0}['medians']])\n")
        f.write("lq_min_values = np.array([b.get_ydata()[0] for b in rbox_min_values{0}['boxes']])\n")
        f.write("hq_min_values = np.array([b.get_ydata()[2] for b in rbox_min_values{0}['boxes']])\n")

    f.write("fig, ax = plt.subplots(figsize=(16, 6.5))\n")
    if file_suffix is not None:
        f.write("ax.errorbar(xvalues{0}, rmed_values{0}, yerr=[rmed_values{0}".format(file_suffix))
        f.write(" - lq_values{0}, hq_values{0} - rmed_values{0}],".format(file_suffix))
        f.write(" color='red', linewidth=2, ecolor='red', elinewidth=1, label='same cell score')\n")
        f.write("ax.errorbar(xvalues{0}, rmed_min_values{0}, yerr=[rmed_min_values{0}".format(file_suffix))
        f.write("- lq_min_values{0}, hq_min_values{0} - rmed_min_values{0}],".format(file_suffix))
        f.write(" color='blue', linewidth=2, ecolor='blue', elinewidth=1, label='min score')\n")
        f.write("ax.set_xticks(xvalues{0}, cells{0}, rotation=80, fontsize=10)\n".format(file_suffix))
    else:
        f.write("ax.errorbar(xvalues, rmed_values, yerr=[rmed_values - lq_values, hq_values - rmed_values],")
        f.write(" color='red', linewidth=2, ecolor='red', elinewidth=1, label='same cell score')\n")
        f.write("ax.errorbar(xvalues, rmed_min_values, yerr=[rmed_min_values - lq_min_values,")
        f.write("hq_min_values - rmed_min_values],")
        f.write(" color='blue', linewidth=2, ecolor='blue', elinewidth=1, label='min score')\n")
        f.write("ax.set_xticks(xvalues, cells, rotation=80, fontsize=10)\n")
    f.write("ax.legend(loc='upper right')\n")
    f.write("ax.set_ylim(0, 1)\n")
    f.write("ax.set_ylabel('score value', fontsize=10)\n")
    f.write("ax.set_title(\"similarity values")
    f.write(" (similarity '" + str(parameters.matching_cell_similarity))
    f.write("', {:d} cells count)\", fontsize=15)\n".format(parameters.cell_number))
    f.write("ax.tick_params(axis='y', labelsize=10)\n")
    f.write("plt.tight_layout()\n")

    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_score_errorbar")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    if angles is None:
        return

    f.write("\n")
    if file_suffix is not None:
        f.write("for ta in angles{:s}:\n".format(file_suffix))
    else:
        f.write("for ta in angles:\n")
    f.write("    nta = ta.replace('-', '_')\n")
    f.write("    fig, ax = plt.subplots(figsize=(16, 6.5))\n")
    if file_suffix is not None:
        f.write("    for ra in angles{:s}[ta]:\n".format(file_suffix))
    else:
        f.write("    for ra in angles[ta]:\n")
    f.write("        nra=ra.replace('-', '_')\n")
    if file_suffix is not None:
        f.write("        raw_angles = [angles{:s}[ta][ra][c] for c in cells{:s}]\n".format(file_suffix, file_suffix))
    else:
        f.write("        raw_angles = [angles[ta][ra][c] for c in cells]\n")

    f.write("\n")
    f.write("        median_raw_angles = np.median(raw_angles)\n")
    f.write("        if median_raw_angles > 180:\n")
    f.write("            median_raw_angles -= 360\n")
    f.write("        angles = []\n")
    f.write("        for a in raw_angles:\n")
    f.write("            if abs(a+360-median_raw_angles) < abs(a-median_raw_angles):\n")
    f.write("                angles += [a+360]\n")
    f.write("            elif abs(a-360-median_raw_angles) < abs(a-median_raw_angles):\n")
    f.write("                angles += [a-360]\n")
    f.write("            else:\n")
    f.write("                angles += [a]\n")

    f.write("\n")
    if file_suffix is not None:
        f.write("        ax.plot(xvalues{:s}, angles, label=nra)\n".format(file_suffix))
    else:
        f.write("        ax.plot(xvalues, angles, label=nra)\n".format(file_suffix))
    f.write("    ax.grid()\n")
    if file_suffix is not None:
        f.write("    ax.set_xticks(xvalues{:s}, cells{:s}".format(file_suffix, file_suffix))
    else:
        f.write("    ax.set_xticks(xvalues, cells")
    f.write(", rotation=80, fontsize=10)\n")
    f.write("    ax.set_ylabel('angle (degrees)', fontsize=10)\n")
    f.write("    ax.legend(loc='upper right')\n")
    f.write("    ax.set_title(\"2D rotation angle of the same cell")
    f.write(" (test atlas '\" + nta + \"'")
    f.write(", similarity '{:s}'".format(parameters.matching_cell_similarity))
    f.write(", {:d} cells count)".format(parameters.cell_number))
    f.write("\", fontsize=15)\n")

    f.write("    plt.tight_layout()\n")
    f.write("    if savefig:\n")
    f.write("        plt.savefig('" + figname + "_rotation_angle_' + nta")
    if file_suffix is not None:
        f.write(" + '{:s}'".format(file_suffix))
    f.write("+ '.png')\n")
    f.write("    else:\n")
    f.write("        plt.show()\n")
    f.write("        plt.close()\n")

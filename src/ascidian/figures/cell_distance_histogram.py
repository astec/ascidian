##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Lun  8 jul 2024 15:39:20 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

import astec.utils.common as common

import ascidian.ascidian.name as aname
import ascidian.components.division_atlas as cdivatlas
import ascidian.components.parameters as cparameters
import ascidian.core.contact_surface_distance as ccsurfdist
import ascidian.figures.write_utils as writeutils

monitoring = common.Monitoring()


################################################################################
#
#
#
################################################################################


def figure_cell_distance_histogram(atlases, parameters):
    """
    Computes cell-to-cell and division-to-division distance histograms
    Parameters
    ----------
    atlases
    parameters

    Returns
    -------

    """
    proc = "figure_cell_distance_histogram"

    if isinstance(atlases, cdivatlas.DivisionAtlases) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'atlases' variable: " + str(type(atlases)))
        sys.exit(1)
    if isinstance(parameters, cparameters.FigureParameters) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    filename = 'figure_cell_distance_histogram'
    figname = 'cell_distance_histogram'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    compute_other_scores = True

    #
    # get the references per mother_name
    # here neighborhoods is a dictionary indexed by ['cell name']['reference name']
    #
    neighborhoods = atlases.get_cell_neighborhood()

    #
    # make generation-dependant calculation
    #
    cell_per_generation = {}
    for n in neighborhoods:
        generation = n.split('.')[0][1:]
        cell_per_generation[generation] = cell_per_generation.get(generation, []) + [n]
    for g in sorted(cell_per_generation.keys()):
        cell_per_generation[g] = sorted(cell_per_generation[g])
        print("    - generation " + str(g) + ": " + str(len(cell_per_generation[g])) + " cells")

    ncells = 0
    for g in cell_per_generation:
        # if int(g) > generationmax:
        #    continue
        ncells += len(cell_per_generation[g])

    cell_to_cell_per_generation = {}
    cell_to_sister_per_generation = {}
    cell_to_other_per_generation = {}

    #
    # compute cell-to-cell distances for
    # - similar cells (cell of same name across embryos)
    # - sister cells (only across embryos)
    # and division-to-division distances
    #
    for g in sorted(cell_per_generation.keys()):
        print("    - process generation " + str(g))
        cell_to_cell = []
        cell_to_sister = []
        cell_to_other = []
        for i, c in enumerate(cell_per_generation[g]):
            if i > 0 and i % 10 == 0:
                print("      has processed " + str(i) + "/" + str(len(cell_per_generation[g])))
            #
            # cell-to-cell distance
            #
            for a1 in neighborhoods[c]:
                for a2 in neighborhoods[c]:
                    if a2 <= a1:
                        continue
                    d = ccsurfdist.contact_surface_distance(neighborhoods[c][a1], neighborhoods[c][a2])
                    cell_to_cell += [d]
            #
            # cell-to-other and cell-to-sister distance
            #
            sister = aname.get_sister_name(c)
            if compute_other_scores:
                for c2 in cell_per_generation[g]:
                    if c2 <= c:
                        continue
                    for a1 in neighborhoods[c]:
                        for a2 in neighborhoods[c2]:
                            d = ccsurfdist.contact_surface_distance(neighborhoods[c][a1], neighborhoods[c2][a2])
                            if c2 == sister:
                                cell_to_sister += [d]
                            else:
                                cell_to_other += [d]
            else:
                if sister <= c:
                    continue
                if sister not in cell_per_generation[g]:
                    continue
                for a1 in neighborhoods[c]:
                    for a2 in neighborhoods[sister]:
                        d = ccsurfdist.contact_surface_distance(neighborhoods[c][a1], neighborhoods[sister][a2])
                        cell_to_sister += [d]

        if len(cell_to_cell) > 0:
            cell_to_cell_per_generation[g] = cell_to_cell
        if len(cell_to_sister) > 0:
            cell_to_sister_per_generation[g] = cell_to_sister
        if compute_other_scores:
            if len(cell_to_other) > 0:
                cell_to_other_per_generation[g] = cell_to_other

    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")
    f.write("import scipy.stats as stats\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    writeutils.write_dict_of_arrays(f, "cell_to_cell_per_generation", cell_to_cell_per_generation, length=3)
    f.write("cell_to_cell = []\n")
    f.write("for g in cell_to_cell_per_generation:\n")
    f.write("    cell_to_cell += cell_to_cell_per_generation[g]\n")

    f.write("\n")
    writeutils.write_dict_of_arrays(f, "cell_to_sister_per_generation", cell_to_sister_per_generation, length=3)
    f.write("cell_to_sister = []\n")
    f.write("for g in cell_to_sister_per_generation:\n")
    f.write("    cell_to_sister += cell_to_sister_per_generation[g]\n")

    f.write("\n")
    f.write("fig, ax = plt.subplots(figsize=(7.5, 7.5), constrained_layout=True)\n")
    f.write("labels = ['same cell', 'sister cell']\n")
    f.write("ax.hist([cell_to_cell, cell_to_sister], 100, histtype='bar', label=labels)\n")
    f.write("ax.legend(prop={'size': 10})\n")
    f.write("ax.set_title('cell-to-cell distances in atlases', fontsize=15)\n")
    f.write("ax.tick_params(labelsize=10)\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_1")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    if compute_other_scores:
        f.write("\n")
        writeutils.write_dict_of_arrays(f, "cell_to_other_per_generation", cell_to_other_per_generation, length=3)
        f.write("cell_to_other = []\n")
        f.write("for g in cell_to_other_per_generation:\n")
        f.write("    cell_to_other += cell_to_other_per_generation[g]\n")

        f.write("\n")
        f.write("fig, ax = plt.subplots(figsize=(7.5, 7.5), constrained_layout=True)\n")
        f.write("labels = ['same cell', 'sister cell', 'other cell']\n")
        f.write("ax.hist([cell_to_cell, cell_to_sister, cell_to_other]")
        f.write(", 100, histtype='bar', label=labels, density=True)\n")
        f.write("ax.legend(prop={'size': 10})\n")
        f.write("ax.set_title('cell-to-cell distances in atlases', fontsize=15)\n")
        f.write("ax.tick_params(labelsize=10)\n")

        f.write("\n")
        f.write("if savefig:\n")
        f.write("    plt.savefig('" + figname + "_2")
        if file_suffix is not None:
            f.write(file_suffix)
        f.write("'" + " + '.png')\n")
        f.write("else:\n")
        f.write("    plt.show()\n")
        f.write("    plt.close()\n")

    f.write("\n")

    f.close()

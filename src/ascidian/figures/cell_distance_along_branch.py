##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu  4 jul 2024 15:24:37 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys
import copy

import astec.utils.common as common

import ascidian.components.embryoset as cembryoset
import ascidian.components.parameters as cparameters
import ascidian.core.contact_surface_distance as ccsurfdist

monitoring = common.Monitoring()


################################################################################
#
#
#
################################################################################

def figure_cell_distance_along_branch(embryos, parameters):
    """

    Parameters
    ----------
    embryos
    parameters

    Returns
    -------

    """
    proc = "figure_cell_distance_along_branch"

    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        sys.exit(1)
    if isinstance(parameters, cparameters.FigureParameters) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'parameters' variable: "
                                      + str(type(parameters)))
        sys.exit(1)

    filename = 'figure_cell_distance_along_branch'
    figname = 'cell_distance_along_branch'

    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    contact_distance_along_time = {}

    ref_embryos = embryos.get_embryos()
    for ref in ref_embryos:
        div = 10 ** ref_embryos[ref].time_digits_for_cell_id

        contact_distance_along_time[ref] = {}
        lineage = ref_embryos[ref].cell_lineage
        contact = ref_embryos[ref].rectified_cell_contact_surface
        name = ref_embryos[ref].cell_name
        reverse_lineage = {v: k for k, values in lineage.items() for v in values}

        #
        # get the first and last time points
        #
        cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
        cells = sorted(cells)
        cells_per_time = {}
        for c in cells:
            t = int(c) // div
            #
            # get cells and cell ids at each time point
            #
            cells_per_time[t] = cells_per_time.get(t, []) + [c]
        last_time = max(cells_per_time.keys())

        #
        # study single branches beginning right after a division, but not at the last time
        #
        first_cells = [lineage[c][0] for c in lineage if len(lineage[c]) == 2 and int(c) // div < last_time - 1]
        first_cells += [lineage[c][1] for c in lineage if len(lineage[c]) == 2 and int(c) // div < last_time - 1]

        for cell in first_cells:
            if cell not in contact:
                msg = "    * weird, cell " + str(cell) + " is not in the 'contact surface' dictionary"
                msg += " of atlas '" + str(ref) + "'"
                monitoring.to_log_and_console(msg, 3)
                continue

            if cell not in name:
                msg = "    * weird, cell " + str(cell) + " is not in the 'name' dictionary"
                msg += " of atlas '" + str(ref) + "'"
                monitoring.to_log_and_console(msg, 3)
                keyd = cell
            else:
                keyd = name[cell]

            first_time = int(cell) // div
            pcell = cell
            pneigh = copy.deepcopy(contact[cell])

            #
            # extract next neighborhood, change neighbors wrt first cell and compute distance
            #
            emergency_break = False
            while True:
                if pcell not in lineage or len(lineage[pcell]) > 1:
                    break
                ncell = lineage[pcell][0]
                t = int(ncell) // div
                nneigh = {}
                #
                # build a neighborhood with the same label than at the first time of the branch
                #
                if ncell not in contact:
                    break
                for c in contact[ncell]:
                    contrib = contact[ncell][c]
                    # background
                    if int(c) % div == 1 or int(c) % div == 0:
                        nneigh[1] = nneigh.get(1, 0.0) + contrib
                    else:
                        for i in range(t - first_time):
                            if c not in reverse_lineage:
                                msg = "    * weird, cell " + str(c) + " is not in the reversed lineage"
                                msg += " of atlas '" + str(ref) + "'"
                                monitoring.to_log_and_console(msg)
                                emergency_break = True
                                break
                            c = reverse_lineage[c]
                        if emergency_break:
                            break
                        nneigh[c] = nneigh.get(c, 0.0) + contrib
                if emergency_break:
                    break
                #
                #
                #
                d = ccsurfdist.contact_surface_distance(pneigh, nneigh, change_contact_surfaces=False)
                contact_distance_along_time[ref][keyd] = contact_distance_along_time[ref].get(keyd, []) + [d]
                pcell = ncell
                pneigh = copy.deepcopy(nneigh)
    #
    #
    #
    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")
    f.write("import scipy.stats as stats\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    f.write("contact_distance = " + str(contact_distance_along_time) + "\n")
    f.write("lengths = [len(contact_distance[r][c]) for r in contact_distance for c in contact_distance[r]]\n")

    f.write("\n")
    f.write("dict_dist_per_time = {}\n")
    f.write("for r in contact_distance:\n")
    f.write("    for c in contact_distance[r]:\n")
    f.write("        for i, v in enumerate(contact_distance[r][c]):\n")
    f.write("            dict_dist_per_time[i] = dict_dist_per_time.get(i, []) + [contact_distance[r][c][i]]\n")
    f.write("dist_per_time = []\n")
    f.write("for i in range(max(lengths)):\n")
    f.write("    dist_per_time.append(dict_dist_per_time[i])\n")

    f.write("\n")
    f.write("ticks = [x*10 for x in set([x//10 for x in list(range(max(lengths)))])]\n")

    f.write("\n")
    f.write("fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True, figsize=(16, 6.5), constrained_layout=True)\n")

    f.write("\n")
    f.write("ax1.set_ylim(0, 0.7)\n")
    f.write("ax1.set_xlabel('time from division', fontsize=15)\n")
    f.write("ax1.set_ylabel('cell-to-cell distance', fontsize=15)\n")
    f.write("ax1.boxplot(dist_per_time)\n")
    f.write("ax1.set_title(\"cell-to-cell distances between successive cells\", fontsize=15)\n")
    f.write("ax1.set_xticks(ticks)\n")
    f.write("ax1.set_xticklabels(ticks)\n")

    f.write("\n")
    f.write("ax2.set_ylim(0, 0.7)\n")
    f.write("ax2.set_xlim(0, 20.5)\n")
    f.write("ax2.set_xlabel('time from division', fontsize=15)\n")
    f.write("ax2.set_ylabel('distance', fontsize=15)\n")
    f.write("ax2.boxplot(dist_per_time)\n")
    f.write("ax2.set_title(\"cell-to-cell distances between successive cells (close-up)\", fontsize=15)\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname)
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")
    f.close()

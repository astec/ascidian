##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu 15 jui 2023 17:45:25 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

import astec.utils.common as common

import ascidian.components.embryoset as cembryoset
import ascidian.components.parameters as cparameters
import ascidian.figures.write_utils as writeutils

monitoring = common.Monitoring()


###########################################################
#
#
#
############################################################


def figure_division_time_couple_misordering(embryos, parameters, sigma=0.5):
    proc = "figure_division_time_couple_misordering"
    
    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        sys.exit(1)
    if isinstance(parameters, cparameters.FigureParameters) is False:
        msg = ": unexpected type for 'parameters' variable: " + str(type(parameters))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)
        
    filename = 'figure_division_time_couple_misordering'
    figname = 'division_time_couple_misordering'
    
    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    ref_embryos = embryos.get_embryos()
    embryo_names = list(ref_embryos.keys())

    cells_per_time = {}
    temporal_coefficients = {}
    for n in embryo_names:
        lineage = ref_embryos[n].cell_lineage
        cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
        cells = sorted(cells)
        div = 10 ** ref_embryos[n].time_digits_for_cell_id
        cells_per_time[n] = {}
        for c in cells:
            t = int(c) // div
            cells_per_time[n][t] = cells_per_time[n].get(t, 0) + 1
        temporal_coefficients[n] = ref_embryos[n].temporal_alignment

    #
    # timepoints is a dictionary indexed by cell names
    # - value is a dictionary index by atlas name
    #   - value is an array (of one element) = (normalized division time)
    #
    timepoints = cembryoset.get_division_time_by_cell_atlas(embryos)

    f = open(filename, "w")

    f.write("from operator import itemgetter\n")
    f.write("import math\n")
    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    writeutils.write_dict_dict(f, "timepoints", timepoints, length=2)

    f.write("\n")
    f.write("couplelist = [('a7.0004', 'a7.0003'), ('b7.0011', 'b7.0012'), ('a7.0014', 'a7.0011'), "
            "('b7.0011', 'a7.0011'),")
    f.write(" ('a7.0015', 'b7.0008'), ('b8.0020', 'b7.0007')]\n")
    f.write("step = 0.1\n")
    f.write("sigma = {:f}\n".format(sigma))
    f.write("nconst = 1.0 / (sigma * np.sqrt(2.0 * np.pi))\n")

    f.write("\n")
    f.write("for c in couplelist:\n")
    f.write("    t0 = []\n")
    f.write("    t1 = []\n")
    f.write("    anames = []\n")
    f.write("    for ext in ['_', '*']:\n")
    f.write("        d0 = c[0] + ext\n")
    f.write("        d1 = c[1] + ext\n")
    f.write("        if d0 not in timepoints:\n")
    f.write("            continue\n")
    f.write("        if d1 not in timepoints:\n")
    f.write("            continue\n")
    f.write("        for a in timepoints[d0]:\n")
    f.write("            if a not in timepoints[d1]:\n")
    f.write("                continue\n")
    f.write("            t0 += [timepoints[d0][a]]\n")
    f.write("            t1 += [timepoints[d1][a]]\n")
    f.write("            anames += [a + ext]\n")

    f.write("\n")
    f.write("    if np.mean(t0) < np.mean(t1):\n")
    f.write("        sortc = [c[1], c[0]]\n")
    f.write("        sortt0 = t1\n")
    f.write("        sortt1 = t0\n")
    f.write("    else:\n")
    f.write("        sortc = [c[0], c[1]]\n")
    f.write("        sortt0 = t0\n")
    f.write("        sortt1 = t1\n")
    f.write("\n")
    f.write("    tmin = math.floor(min(min(t0), min(t1))) - 2\n")
    f.write("    tmax = math.ceil(max(max(t0), max(t1))) + 2\n")
    f.write("    trange = np.arange(tmin, tmax + step / 10, step)\n")
    f.write("    irange = range(len(trange))\n")
    f.write("    xpdf = np.zeros(len(trange))\n")
    f.write("    ypdf = np.zeros(len(trange))\n")
    f.write("    totalpdf = np.zeros((len(trange), len(trange)))\n")

    f.write("\n")
    f.write("    for n in range(len(t1)):\n")
    f.write("        x = nconst * np.exp(- 0.5 * ((trange - sortt0[n]) / sigma) * ((trange - sortt0[n]) / sigma))\n")
    f.write("        y = nconst * np.exp(- 0.5 * ((trange - sortt1[n]) / sigma) * ((trange - sortt1[n]) / sigma))\n")
    f.write("        xpdf += x / len(t1)\n")
    f.write("        ypdf += y / len(t1)\n")
    f.write("        totalpdf += np.tile(x, (len(trange), 1)) * np.transpose(np.tile(y, (len(trange), 1))) / len(t1)\n")

    f.write("\n")
    f.write("    totalpdfmax = totalpdf.max()\n")
    f.write("    pdfmax = max(xpdf.max(), ypdf.max())\n")
    f.write("    pdfmax += 0.1 * pdfmax\n")
    #
    # wscore computes the probability that t(c[0]) > t(c[1])
    # couples were ordered such that mean(t(c[0])) <= mean(t(c[1]))
    #
    f.write("\n")
    f.write("    wscore = 0.0\n")
    f.write("    for n in range(len(t1)):\n")
    f.write("        wscore += 0.5 * (1.0 + math.erf((sortt1[n] - sortt0[n]) / (2.0 * sigma)))\n")
    f.write("    wscore /= len(t1)\n")

    f.write("\n")
    figtxt = "    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8)"
    figtxt += ", gridspec_kw={'width_ratios': [0.8, 1]})\n"
    f.write(figtxt)

    f.write("\n")
    f.write("    ax1.scatter(sortt0, sortt1, c='tab:brown', s=35)\n")
    f.write("    ax1.plot([tmin, tmax], [tmin, tmax])\n")
    f.write("    ax1.set_xlim(left=tmin, right=tmax)\n")
    f.write("    ax1.set_ylim(bottom=tmin, top=tmax)\n")
    f.write("    ax1.set_xlabel('division time of ' + sortc[0] + ' (average = {:.2f})'.format(np.mean(sortt0)))\n")
    f.write("    ax1.set_ylabel('division time of ' + sortc[1] + ' (average = {:.2f})'.format(np.mean(sortt1)))\n")
    f.write("    ax1.set_aspect('equal', 'box')\n")
    f.write("    for i, n in enumerate(anames):\n")
    f.write("        ax1.annotate(n, xy=(sortt0[i], sortt1[i]), xycoords='data')\n")
    f.write("    title = 'Couples of division times'\n")
    f.write("    ax1.set_title(title, fontsize=15)\n")

    f.write("\n")
    f.write("    im = ax2.imshow(totalpdf, cmap='jet', vmin=0, vmax=totalpdfmax)\n")
    f.write("    ax2.plot([0, len(trange) - 1], [0, len(trange) - 1])\n")
    f.write("    ax2.invert_yaxis()\n")
    f.write("    plt.colorbar(im, shrink=0.725, ax=ax2)\n")
    f.write("    labels = ['{:d}'.format(round(t)) for t in trange[::10]]\n")
    f.write("    ax2.set_xticks(irange[::10])\n")
    f.write("    ax2.set_xticklabels(labels)\n")
    f.write("    ax2.set_yticks(irange[::10])\n")
    f.write("    ax2.set_yticklabels(labels)\n")
    f.write("    ax2.set_xlabel('division time of ' + sortc[0] + ' (average = {:.2f})'.format(np.mean(sortt0)))\n")
    f.write("    ax2.set_ylabel('division time of ' + sortc[1] + ' (average = {:.2f})'.format(np.mean(sortt1)))\n")
    f.write("    title = 'Probability of mis-ordering = {:.2f}'.format(wscore)\n")
    f.write("    ax2.set_title(title, fontsize=15)\n")

    f.write("\n")
    f.write("    title = 'compared division times of ' + sortc[0] + ' and ' + sortc[1]\n")
    f.write("    fig.suptitle(title, fontsize=15)\n")
    f.write("    plt.tight_layout()\n")

    f.write("\n")
    f.write("    name = '{:s}_' + sortc[0][0:2] + sortc[0][3:]\n".format(figname))
    f.write("    name += '_' + sortc[1][0:2] + sortc[1][3:] + '_1'\n")
    if file_suffix is not None:
        f.write("    name += '{:s}'\n".format(file_suffix))
    f.write("    if savefig:\n")
    f.write("        plt.savefig(name + '.png')\n")
    f.write("    else:\n")
    f.write("        plt.show()\n")
    f.write("        plt.close()\n")

    f.write("\n")
    f.write("    fig, ax = plt.subplots(2, 2, figsize=(8, 8), constrained_layout=True)\n")
    f.write("\n")
    f.write("    im = ax[0][1].imshow(totalpdf, cmap='jet', vmin=0, vmax=totalpdfmax)\n")
    f.write("    ax[0][1].plot([0, len(trange) - 1], [0, len(trange) - 1])\n")
    f.write("    ax[0][1].invert_yaxis()\n")
    f.write("    ax[0][1].set_xticks(irange[::10])\n")
    f.write("    ax[0][1].set_xticklabels(labels)\n")
    f.write("    ax[0][1].set_yticks(irange[::10])\n")
    f.write("    ax[0][1].set_yticklabels(labels)\n")
    f.write("    ax[0][1].set_xlabel('division time of ' + sortc[0])\n")
    f.write("    ax[0][1].set_ylabel('division time of ' + sortc[1])\n")
    f.write("    title = 'Probability of mis-ordering = {:.2f}'.format(wscore)\n")
    f.write("    ax[0][1].set_title(title, fontsize=10)\n")
    f.write("\n")
    f.write("    ax[1][1].plot(irange, xpdf)\n")
    f.write("    ax[1][1].set_xticks(irange[::10])\n")
    f.write("    ax[1][1].set_xticklabels(labels)\n")
    f.write("    ax[1][1].set_xlim(0, irange[-1])\n")
    f.write("    ax[1][1].set_ylim(0, pdfmax)\n")
    f.write("    title = 'division time probability of ' + sortc[0]\n")
    f.write("    ax[1][1].set_title(title, fontsize=10)\n")
    f.write("\n")
    f.write("    ax[0][0].plot(ypdf, irange)\n")
    f.write("    ax[0][0].set_yticks(irange[::10])\n")
    f.write("    ax[0][0].set_yticklabels(labels)\n")
    f.write("    ax[0][0].set_xlim(0, pdfmax)\n")
    f.write("    ax[0][0].set_ylim(0, irange[-1])\n")
    f.write("    title = 'division time probability of ' + sortc[1]\n")
    f.write("    ax[0][0].set_title(title, fontsize=10)\n")
    f.write("\n")
    f.write("    ax[1, 0].axis('off')\n")

    f.write("\n")

    f.write("    name = '{:s}_' + sortc[0][0:2] + sortc[0][3:]\n".format(figname))
    f.write("    name += '_' + sortc[1][0:2] + sortc[1][3:] + '_2'\n")
    if file_suffix is not None:
        f.write("    name += '{:s}'\n".format(file_suffix))
    f.write("    if savefig:\n")
    f.write("        plt.savefig(name + '.png')\n")
    f.write("    else:\n")
    f.write("        plt.show()\n")

    f.close()

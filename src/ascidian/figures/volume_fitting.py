##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu 15 jui 2023 17:45:25 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

import astec.utils.common as common

import ascidian.components.embryoset as cembryoset
import ascidian.components.parameters as cparameters

monitoring = common.Monitoring()


################################################################################
#
#
#
################################################################################

def figure_volume_fitting(embryos, parameters):
    """

    Parameters
    ----------
    embryos
    parameters

    Returns
    -------

    """
    proc = "figure_volume_fitting"

    if isinstance(embryos, cembryoset.EmbryoSet) is False:
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'embryos' variable: " + str(type(embryos)))
        sys.exit(1)
    if isinstance(parameters, cparameters.FigureParameters) is False:
        msg = ": unexpected type for 'parameters' variable: " + str(type(parameters))
        monitoring.to_log_and_console(str(proc) + msg)
        sys.exit(1)

    filename = 'figure_volume_fitting'
    figname = 'volume_fitting'
    
    file_suffix = None
    if parameters.figurefile_suffix is not None and isinstance(parameters.figurefile_suffix, str) and \
            len(parameters.figurefile_suffix) > 0:
        file_suffix = '_' + parameters.figurefile_suffix
    if file_suffix is not None:
        filename += file_suffix
    filename += '.py'

    if parameters.outputDir is not None and isinstance(parameters.outputDir, str):
        if not os.path.isdir(parameters.outputDir):
            if not os.path.exists(parameters.outputDir):
                os.makedirs(parameters.outputDir)
            else:
                monitoring.to_log_and_console(proc + ": '" + str(parameters.outputDir) + "' is not a directory ?!")
        if os.path.isdir(parameters.outputDir):
            filename = os.path.join(parameters.outputDir, filename)

    ref_embryos = embryos.get_embryos()
    embryo_names = list(ref_embryos.keys())

    volume_along_time = {}
    temporal_coefficients = {}
    volume_local_estimation = {}
    surface_along_time = {}
    voxelsize_correction = {}
    for n in embryo_names:
        volume_along_time[n] = {}
        surface_along_time[n] = {}
        voxelsize_correction[n] = {}
        volume = ref_embryos[n].cell_volume
        contact_surface = ref_embryos[n].cell_contact_surface
        div = 10 ** ref_embryos[n].time_digits_for_cell_id
        for c in volume:
            t = int(c) // div
            volume_along_time[n][t] = volume_along_time[n].get(t, 0) + volume[c]
        for c in contact_surface:
            t = int(c) // div
            for d in contact_surface[c]:
                if d % div == 1 or d % div == 0:
                    surface_along_time[n][t] = surface_along_time[n].get(t, 0) + contact_surface[c][d]
        for t in volume_along_time[n]:
            voxelsize_correction[n][t] = ref_embryos[n].get_voxelsize_correction(t)

        temporal_coefficients[n] = ref_embryos[n].temporal_alignment
        volume_local_estimation[n] = ref_embryos[n].embryo_volume_fit
    f = open(filename, "w")

    f.write("import numpy as np\n")
    f.write("import matplotlib.pyplot as plt\n")

    f.write("\n")
    f.write("savefig = True\n")

    f.write("\n")
    f.write("volume_along_time = " + str(volume_along_time) + "\n")
    f.write("\n")
    f.write("surface_along_time = " + str(surface_along_time) + "\n")
    f.write("\n")
    f.write("voxelsize = " + str(voxelsize_correction) + "\n")
    f.write("\n")
    f.write("temporal_coefficients = " + str(temporal_coefficients) + "\n")
    f.write("volume_local_estimation = " + str(volume_local_estimation) + "\n")

    f.write("\n")
    f.write("fig, (ax1, ax2, ax3) = plt.subplots(ncols=3, sharey=True, figsize=(24, 8), constrained_layout=True)\n")
    f.write("labels = []\n")
    f.write("for n in volume_along_time:\n")
    f.write("    labels += [n]\n")
    f.write("    x = list(volume_along_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    y = [volume_along_time[n][i] for i in x]\n")
    f.write("\n")
    f.write("    y2 = [volume_local_estimation[n][0] * i + volume_local_estimation[n][1] for i in x]\n")
    f.write("    p = ax1.plot(x, y, label=n)\n")
    f.write("    ax1.plot(x, y2, color=p[0].get_color())  \n")
    f.write("\n")
    f.write("    ploss = (y[-1] - y[0])/y[0]\n")
    f.write("    ploss2 = (y2[-1] - y2[0])/y2[0]\n")
    f.write("    print(str(n) + ' volume percentage loss from first time point')\n")
    f.write("    print('\\t to the last one, from measures = ' + str(100.0 * (y[-1] - y[0])/y[0]))\n")
    f.write("    print('\\t \\t from linear estimation = ' + str(100.0 * ((y2[-1] - y2[0])/y2[0])))\n")
    f.write("    if len(y2) > 50:\n")
    f.write("        print('\\t to the 50th one, from linear estimation = ' + str(100.0 * ((y2[50] - y2[0])/y2[0])))\n")
    f.write("ax1.set_title(\"embryo volume (raw)\", fontsize=15)\n")
    f.write("ax1.legend(prop={'size': 10})\n")

    f.write("\n")
    f.write("labels = []\n")
    f.write("for n in volume_along_time:\n")
    f.write("    labels += [n]\n")
    f.write("    x = list(volume_along_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    y = [volume_along_time[n][i] for i in x]\n")
    f.write("    y2 = [volume_local_estimation[n][0] * i + volume_local_estimation[n][1] for i in x]\n")
    f.write("    p = ax2.plot(t, y, label=n)\n")
    f.write("    ax2.plot(t, y2, color=p[0].get_color())  \n")
    f.write("ax2.set_title(\"embryo volume (+ temporal alignment)\", fontsize=15)\n")
    f.write("ax2.legend(prop={'size': 10})\n")

    f.write("\n")
    f.write("for n in volume_along_time:\n")
    f.write("    x = list(volume_along_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    y = [volume_along_time[n][i] * voxelsize[n][i] * voxelsize[n][i] * voxelsize[n][i] for i in x]\n")
    f.write("    ax3.plot(t, y)\n")
    f.write("ax3.set_title(\"embryo volume (+ constantness)\", fontsize=15)\n")
    f.write("ax3.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_volumes")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")

    f.write("\n")
    f.write("fig, (ax1, ax2, ax3) = plt.subplots(ncols=3, sharey=True, figsize=(24, 8), constrained_layout=True)\n")
    f.write("labels = []\n")
    f.write("for n in surface_along_time:\n")
    f.write("    labels += [n]\n")
    f.write("    x = list(surface_along_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    y = [surface_along_time[n][i] for i in x]\n")
    f.write("    p = ax1.plot(x, y, label=n)\n")
    f.write("\n")
    f.write("ax1.set_title(\"embryo surface (raw)\", fontsize=15)\n")
    f.write("ax1.legend(prop={'size': 10})\n")

    f.write("\n")
    f.write("for n in surface_along_time:\n")
    f.write("    x = list(surface_along_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    y = [surface_along_time[n][i] for i in x]\n")
    f.write("    ax2.plot(t, y)\n")
    f.write("ax2.set_title(\"embryo surface (+ temporal alignment)\", fontsize=15)\n")
    f.write("ax2.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("for n in surface_along_time:\n")
    f.write("    x = list(surface_along_time[n].keys())\n")
    f.write("    x = sorted(x)\n")
    f.write("    t = [temporal_coefficients[n][0] * i + temporal_coefficients[n][1] for i in x]\n")
    f.write("    y = [surface_along_time[n][i] * voxelsize[n][i] * voxelsize[n][i] for i in x]\n")
    f.write("    ax3.plot(t, y)\n")
    f.write("ax3.set_title(\"embryo surface (+ constantness)\", fontsize=15)\n")
    f.write("ax3.legend(labels, prop={'size': 10})\n")

    f.write("\n")
    f.write("if savefig:\n")
    f.write("    plt.savefig('" + figname + "_surfaces")
    if file_suffix is not None:
        f.write(file_suffix)
    f.write("'" + " + '.png')\n")
    f.write("else:\n")
    f.write("    plt.show()\n")
    f.write("    plt.close()\n")
    f.close()

#!/usr/bin/env python3

import os
import time
import pickle
from argparse import ArgumentParser
import sys

# Local imports
import astec.utils.common as common
from ascidian.algorithms.analyse_divisions_distributions import kde_divisions_grouping, kde_divisions_grouping_lifetime
import ascidian.division_analysis.parameters_distribution as daparameters


def _set_options(my_parser):
    """

    :param my_parser:
    :return:
    """
    proc = "_set_options"
    if not isinstance(my_parser, ArgumentParser):
        print(proc + ": argument is not of type ArgumentParser")
        return
    #
    # common parameters
    #

    my_parser.add_argument('-p', '--parameters',
                           action='store', dest='parameterFile', const=None,
                           help='python file containing parameters definition')

    #
    # control parameters
    #

    my_parser.add_argument('--save-log',
                           action='store_const', dest='save_log',
                           default=False, const=True,
                           help='save log files')
    my_parser.add_argument('--no-save-log',
                           action='store_const', dest='save_log',
                           const=False,
                           help='save log files')
    my_parser.add_argument('--log', '--logfile',
                           action='store', dest='log_filename', const=None,
                           help='log file name')

    my_parser.add_argument('-v', '--verbose',
                           action='count', dest='verbose', default=2,
                           help='incrementation of verboseness')

    my_parser.add_argument('-nv', '--no-verbose',
                           action='store_const', dest='verbose', const=0,
                           help='no verbose at all')

    msg = "print the list of parameters (with explanations) in the console and exit. "
    msg += "If a parameter file is given, it is taken into account"
    my_parser.add_argument('-pp', '--print-param',
                           action='store_const', dest='printParameters',
                           default=False, const=True, help=msg)

    return


#
#
# main function
#
#


def main():

    ############################################################
    #
    # generic part
    #
    ############################################################

    #
    # initialization
    ##
    start_time = time.localtime()
    monitoring = common.Monitoring()
    #experiment = datadir.Experiment()

    # reading command line arguments
    # and update from command line arguments
    #
    parser = ArgumentParser(description='Naming by propagation')
    _set_options(parser)
    args = parser.parse_args()

    #experiment.update_from_args(args)

    #
    # reading parameter files
    # and updating parameters
    #
    parameter_file = common.get_parameter_file(args.parameterFile)
    #experiment.update_from_parameter_file(parameter_file)


    #
    # keep history of command line executions
    # and copy parameter file
    #
    #experiment.update_history_at_start(__file__, start_time, parameter_file, path_to_vt())
    #experiment.copy_stamped_file(start_time, parameter_file)

    #
    # copy monitoring information into other "files"
    # so the log filename is known
    #
    #common.monitoring.copy(monitoring)
    #datadir.monitoring.copy(monitoring)

    #################
    # manage parameters
    # 1. initialize
    # 2. update parameters
    # 3. write parameters into the logfile
    #

    # create object

    parameters = daparameters.DivisionAnalysisParameters(parameter_file)
    #parameters.write_parameters(monitoring.log_filename) # write in
    if args.printParameters:
        parameters.print_parameters()

    #
    # processing
    #
    # Load divisions properties from pickle file
    pickle_file_path = os.path.join(parameters.folder, 'divisions_properties.pkl')
    with open(pickle_file_path, 'rb') as pickle_file:
        divisions_properties = pickle.load(pickle_file)

    divisions_properties = kde_divisions_grouping_lifetime(divisions_properties, parameters.nb_of_modes, parameters.generations,
                                                           parameters.folder,
                                                           parameters.sigma_step, parameters.initial_sigma,
                                                           parameters.sphere_radius)

    # save in outputfolder
    output_file_path = os.path.join(parameters.folder, 'divisions_properties.pkl')

    with open(output_file_path, 'wb') as output_file:
        print('saving updated props in:', output_file_path)
        pickle.dump(divisions_properties, output_file)

    #
    # end of execution
    # write execution time in both log and history file
    #
    end_time = time.localtime()
    #monitoring.update_execution_time(start_time, end_time)
    #experiment.update_history_execution_time(__file__, start_time, end_time)

    monitoring.to_console('Total computation time = ' + str(time.mktime(end_time) - time.mktime(start_time)) + ' s')


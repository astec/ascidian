#!/usr/bin/env python3

import os
import time
import pickle
from argparse import ArgumentParser
import sys

# Local imports
import astec.utils.common as common
import astec.utils.datadir as datadir
import astec.utils.properties as properties
import astec.utils.ioproperties as ioproperties
from astec.wrapping.cpp_wrapping import path_to_vt

import ascidian.ascidian.fate as afate

import ascidian.division_analysis.parameters_analysis as daparameters
import ascidian.division_analysis.figure_variability as ufigure
import ascidian.division_analysis.division_analysis_tools as utools
import ascidian.division_analysis.cell_apical_surface_analysis as uapical
import ascidian.division_analysis.division_direction_from_cell_geometry as ugeom


def _set_options(my_parser):
    """

    :param my_parser:
    :return:
    """
    proc = "_set_options"
    if not isinstance(my_parser, ArgumentParser):
        print(proc + ": argument is not of type ArgumentParser")
        return
    #
    # common parameters
    #

    my_parser.add_argument('-p', '--parameters',
                           action='store', dest='parameterFile', const=None,
                           help='python file containing parameters definition')

    my_parser.add_argument('-e', '--embryo-rep',
                           action='store', dest='embryo_path', const=None,
                           help='path to the embryo data')

    #
    # control parameters
    #

    my_parser.add_argument('-k', '--keep-temporary-files',
                           action='store_const', dest='keepTemporaryFiles',
                           default=False, const=True,
                           help='keep temporary files')

    my_parser.add_argument('-f', '--force',
                           action='store_const', dest='forceResultsToBeBuilt',
                           default=False, const=True,
                           help='force building of results')

    my_parser.add_argument('-v', '--verbose',
                           action='count', dest='verbose', default=2,
                           help='incrementation of verboseness')
    my_parser.add_argument('-nv', '--no-verbose',
                           action='store_const', dest='verbose', const=0,
                           help='no verbose at all')
    my_parser.add_argument('-d', '--debug',
                           action='count', dest='debug', default=0,
                           help='incrementation of debug level')
    my_parser.add_argument('-nd', '--no-debug',
                           action='store_const', dest='debug', const=0,
                           help='no debug information')

    doc = "print the list of parameters (with explanations) in the console and exit. "
    doc += "If a parameter file is given, it is taken into account"
    my_parser.add_argument('-pp', '--print-param',
                           action='store_const', dest='printParameters',
                           default=False, const=True, help=doc)

    return


#
#
# main function
#
#


def main():

    ############################################################
    #
    # generic part
    #
    ############################################################

    #
    # initialization
    ##
    start_time = time.localtime()
    monitoring = common.Monitoring()
    experiment = datadir.Experiment()

    #
    # reading command line arguments
    # and update from command line arguments
    #
    parser = ArgumentParser(description='Generates figures for divisions analysis')
    _set_options(parser)
    args = parser.parse_args()

    monitoring.update_from_args(args)
    experiment.update_from_args(args)

    #
    # reading parameter files
    # and updating parameters
    #
    parameter_file = common.get_parameter_file(args.parameterFile)
    experiment.update_from_parameter_file(parameter_file)
    #
    #################
    # manage parameters
    # 1. initialize
    # 2. update parameters
    # 3. write parameters into the logfile
    #

    parameters = daparameters.DivisionAnalysisParameters(parameter_file)

    # set
    # 1. the working directory
    #    that's where the logfile will be written
    # 2. the log file name
    #    it creates the logfile dir, if necessary
    #
    # experiment.working_dir = experiment.post_dir
    #experiment.working_dir = parameters.folder
    # todo ask how to change path of log file
    monitoring.set_log_filename(experiment, __file__, start_time)

    # write parameters in log
    # todo ask: object has no attribute '_prefixes'
    parameters.write_parameters(monitoring.log_filename) # write in
    if args.printParameters:
        parameters.print_parameters()

    #
    # keep history of command line executions
    # and copy parameter file
    #
    experiment.update_history_at_start(__file__, start_time, parameter_file, path_to_vt())
    experiment.copy_stamped_file(start_time, parameter_file)

    #
    # copy monitoring information into other "files"
    # so the log filename is known
    #
    common.monitoring.copy(monitoring)
    datadir.monitoring.copy(monitoring)

    #
    # copy monitoring information into other "files"
    # so the log filename is known
    #
    properties.monitoring.copy(monitoring)
    ioproperties.monitoring.copy(monitoring)

    afate.monitoring.copy(monitoring)

    #
    # processing
    #

    # depending on parameters order call the needed functions
    process_parameters(parameters)

    #
    # end of execution
    # write execution time in both log and history file
    #
    end_time = time.localtime()
    monitoring.update_execution_time(start_time, end_time)
    experiment.update_history_execution_time(__file__, start_time, end_time)

    monitoring.to_console('Total computation time = ' + str(time.mktime(end_time) - time.mktime(start_time)) + ' s')


def process_parameters(parameters):
    """
    Process the parameters and call corresponding functions based on their values.

    :param parameters: dict containing the input parameters
    :return: None
    """

    proc = "process_parameters"

    # Extract the folder and validate
    folder = getattr(parameters, 'folder', None)
    if not folder:
        raise ValueError("The 'folder' parameter is required but missing.")

    # Load required pickle files
    def load_pickle(filename):
        filepath = os.path.join(folder, filename)
        with open(filepath, 'rb') as file:
            return pickle.load(file)

    divisions_properties = load_pickle('divisions_properties.pkl')
    time_alignment_coefficients = load_pickle('time_alignment_coefficients.pkl')
    cells_per_time_in_ref = load_pickle('cells_per_time_in_ref.pkl')
    cell_name_to_fate = load_pickle('cell_name_to_fate.pkl')

    # Default parameters handling
    output_folder = getattr(parameters, 'output_folder', folder)
    approximate_apical_direction = getattr(parameters, 'approximate_apical_direction', False)
    showFate = getattr(parameters, 'showFate', True)
    colorFlag = getattr(parameters, 'colorFlag', True)
    TissueGroupResolution = getattr(parameters, 'TissueGroupResolution', True)
    cumulutatif = getattr(parameters, 'cumulutatif', True)
    threshold = getattr(parameters, 'threshold', None)
    generations = getattr(parameters, 'generations', [7])
    mother_cells = getattr(parameters, 'mother', [])

    figure_types = getattr(parameters, 'figure', [])

    # Process the figure types
    for figure_type in figure_types:
        if figure_type == 'division_time_boxplot':
            ufigure.plot_boxplot_of_cell_division_time(divisions_properties, time_alignment_coefficients,
                                                       output_folder)
        elif figure_type == 'division_mother_lifetimes_dispersion':
            ufigure.variability_cells_lifetime(divisions_properties, time_alignment_coefficients, generations,
                                               output_folder)
            ufigure.correlation_appearance_to_division_time(divisions_properties, generations,
                                                            time_alignment_coefficients, output_folder)
        elif figure_type == 'division_direction_dispersion_boxplot':
            ufigure.plot_boxplot_of_cell_division_directions(divisions_properties, generations, output_folder,
                                                             showFate)
        elif figure_type == 'division_time_ridgeline':
            ufigure.plot_ridgeline_of_cell_division_time(divisions_properties, time_alignment_coefficients,
                                                         output_folder)
        elif figure_type == 'mother_division_time_vline':
            for mother in mother_cells:
                ufigure.plot_mother_division_times(divisions_properties, time_alignment_coefficients, mother,
                                                   output_folder)
        elif figure_type == 'directions_var_vs_division_time_variability':
            ufigure.plot_scatter_std_division_direction_vs_division_time(divisions_properties,
                                                                         time_alignment_coefficients, generations,
                                                                         output_folder, showFate)
        elif figure_type == 'directions_var_vs_spatial_pos':
            ufigure.div_dir_var_vs_spatial_post(divisions_properties, generations, avg=True,
                                                output_folder=output_folder)
        elif figure_type == 'division_registration_dispersion_vs_sigma_a':
            ufigure.plot_scatter_division_direction_std_vs_division_registration_dispersion(divisions_properties,
                                                                                            generations,
                                                                                            output_folder,
                                                                                            showFate)
        elif figure_type == 'division_registration_dispersion_vs_sigma_t':
            ufigure.plot_scatter_division_time_std_vs_division_registration_dispersion(divisions_properties,
                                                                                       time_alignment_coefficients,
                                                                                       generations, output_folder,
                                                                                       showFate)
        elif figure_type == 'division_registration_dispersion_vs_avg_t':
            ufigure.plot_scatter_division_time_avg_vs_division_registration_dispersion(divisions_properties,
                                                                                       time_alignment_coefficients,
                                                                                       cells_per_time_in_ref,
                                                                                       generations, output_folder,
                                                                                       showFate)
        elif figure_type == '3d_directions':
            for mother in mother_cells:
                ufigure.plot_mother_division_directions(divisions_properties, mother, output_folder)
        elif figure_type == 'var_vs_fate_precision':
            ufigure.plot_variability_vs_fate_precision(divisions_properties, time_alignment_coefficients,
                                                       generations,
                                                       TissueGroupResolution, output_folder)
        elif figure_type == 'var_tissues':
            ufigure.plot_variability_vs_neighbours_tissuetype(divisions_properties, time_alignment_coefficients,
                                                              generations, cell_name_to_fate, TissueGroupResolution,
                                                              output_folder)
        elif figure_type == 'generate_heatmap_of_cell_variability_per_individual':
            ufigure.generate_heatmap_of_cell_variability_per_individual(divisions_properties, generations, folder,
                                                                        colorFlag, output_folder)
        elif figure_type == 'boxplot_of_cells_relative_distance_from_mean_per_individual':
            ufigure.plot_boxplot_of_cells_rltv_dist_per_individual(divisions_properties, generations, mother_cells,
                                                                   output_folder)
        elif figure_type == 'division_direction_and_neighbors_deviation_from_mean':
            for mother in mother_cells:
                ufigure.plot_scatter_div_and_neighbors_deviation_from_mean(divisions_properties, mother, threshold,
                                                                           output_folder)
        elif figure_type == 'division_direction_deviation_vs_surface_of_contacts':
            for mother in mother_cells:
                ufigure.plot_division_direction_deviation_vs_surface_of_contacts(divisions_properties, mother,
                                                                                 cell_name_to_fate, output_folder)
        elif figure_type == 'mother_div_dir_deviation_to_division_order':
            for mother in mother_cells:
                ufigure.plot_deviation_of_division_direction_from_mean_to_division_order(divisions_properties,
                                                                                         time_alignment_coefficients,
                                                                                         mother, output_folder)
        elif figure_type == 'dendrogram_div_directions_surface_of_contact':
            ufigure.plot_dendrogram_directions(divisions_properties, generations, output_folder)
            ufigure.dendogramme_surface_of_contact(divisions_properties, generations, output_folder)
        elif figure_type == 'neighbors_division_time_vs_division_dir_deviation':
            for mother in mother_cells:
                ufigure.plot_direction_deviation_vs_neighbors_life_cycle(divisions_properties, mother,
                                                                         output_folder)
        elif figure_type == 'boxplot_deviation_div_direction_apical_surface_longest_axis':
            uapical.plot_boxplot_deviation_div_direction_apical_surface_longest_axis(divisions_properties,
                                                                                     generations,
                                                                                     output_folder)
        elif figure_type == 'mother_cell_apical_surface_directions':
            for mother in mother_cells:
                uapical.plot_cell_apical_pts_and_directions(divisions_properties, mother, output_folder)
        elif figure_type == 'boxplot_deviation_div_direction_to_longest_axis':
            ugeom.plot_boxplot_deviation_div_direction_principal_directions(divisions_properties,
                                                                            time_alignment_coefficients,
                                                                            generations,
                                                                            output_folder)
        elif figure_type == 'mother_cell_pca_directions':
            for mother in mother_cells:
                ugeom.plot_cell_pca_directions(divisions_properties, mother, output_folder)
                ugeom.plot_division_direction_deviation_to_longest_axis_vs_pca_ratio(divisions_properties, mother,
                                                                                     output_folder)
        elif figure_type == 'sister_plane_deviation_to_barycenter_axis':
            ufigure.plot_sister_plane_deviation_to_barycenter_axis(divisions_properties, generations, output_folder)
        elif figure_type == 'inter_individuals_div_deviations':
            ufigure.compute_dmatrix_based_on_division_deviation_overtime(divisions_properties,
                                                                         time_alignment_coefficients,
                                                                         cells_per_time_in_ref, generations,
                                                                         cumulutatif, output_folder)
        elif figure_type == 'inter_individuals_dmatrix_div_order':
            ufigure.compute_dmatrix_based_on_cells_division_order_overtime(divisions_properties,
                                                                           time_alignment_coefficients,
                                                                           cells_per_time_in_ref, generations,
                                                                           False,
                                                                           output_folder)
            ufigure.compute_dmatrix_based_on_cells_division_time_overtime(divisions_properties,
                                                                          time_alignment_coefficients,
                                                                          cells_per_time_in_ref, generations, False,
                                                                          output_folder)
        elif figure_type == 'inter_individuals_dmatrix_cells_lifetimes':
            ufigure.compute_dmatrix_based_on_cells_lifetimes_overtime(divisions_properties,
                                                                      time_alignment_coefficients,
                                                                      cells_per_time_in_ref, generations, False,
                                                                      output_folder)
        elif figure_type == 'div_dir_deviation_lr_to_other_indiv':
            ufigure.deviation_left_right_to_other_individuals_in_same_pattern(divisions_properties,
                                                                              time_alignment_coefficients,
                                                                              mother_cells,
                                                                              generations, output_folder)
            ufigure.deviation_left_right_to_other_individuals_and_registration_local_residue_in_same_pattern(
                divisions_properties, time_alignment_coefficients, mother_cells, generations, output_folder)
        elif figure_type == 'shape_analysis_between_different_clusters_of_same_cells':
            ufigure.comparision_geom_btw_clusters(divisions_properties, output_folder)
        elif figure_type == 'time_vol_analysis_between_different_clusters_of_same_cells':
            ufigure.comparision_time_vol_btw_clusters(divisions_properties, time_alignment_coefficients,
                                                      output_folder)
        elif figure_type == 'inter_gen_div_dir_var':
            ufigure.dispersion_of_cell_vs_mother(divisions_properties, generations, output_folder)
        else:
            print(f"{proc}: Unknown figure type '{figure_type}'")
#!/usr/bin/env python3

import os
import time
import pickle
from argparse import ArgumentParser
import sys


###Local imports
import astec.utils.common as common
import astec.utils.datadir as datadir
import ascidian.division_analysis.parameters as daparameters

from ascidian.division_analysis.embryos_loader import *
from ascidian.algorithms.division_analysis_init import *

import ascidian.algorithms.filter_property_files as ufilter

import ascidian.division_analysis.analyse_directions_approximations as ustb
from ascidian.division_analysis.rotation_assessment_dif import rotations_assessment
import ascidian.division_analysis.tissues_nb_of_cells as tnbcells

from ascidian.algorithms.test_coordinate_ref_sys import assess_coord_sys_stability
from ascidian.division_analysis.geometric_topological_global_distance_btw_embryos import (all_embryos_in_same_referential_distance_matrix,
                                                                                all_embryos_in_same_topological_referential_distance_matrix,
                                                                             compute_geom_barycenters_distance_at_each_dev_time_only_common_cells,
                                                                             all_embryos_in_same_referential__temporal_geometric)
import ascidian.algorithms.analyse_tissues_surface_contact as tvsc
###


def _set_options(my_parser):
    """

    :param my_parser:
    :return:
    """
    proc = "_set_options"
    if not isinstance(my_parser, ArgumentParser):
        print(proc + ": argument is not of type ArgumentParser")
        return
    #
    # common parameters
    #

    my_parser.add_argument('-p', '--parameters',
                           action='store', dest='parameterFile', const=None,
                           help='python file containing parameters definition')

    #
    # control parameters
    #

    my_parser.add_argument('--save-log',
                           action='store_const', dest='save_log',
                           default=False, const=True,
                           help='save log files')
    my_parser.add_argument('--no-save-log',
                           action='store_const', dest='save_log',
                           const=False,
                           help='save log files')
    my_parser.add_argument('--log', '--logfile',
                           action='store', dest='log_filename', const=None,
                           help='log file name')

    my_parser.add_argument('-v', '--verbose',
                           action='count', dest='verbose', default=2,
                           help='incrementation of verboseness')

    my_parser.add_argument('-nv', '--no-verbose',
                           action='store_const', dest='verbose', const=0,
                           help='no verbose at all')

    msg = "print the list of parameters (with explanations) in the console and exit. "
    msg += "If a parameter file is given, it is taken into account"
    my_parser.add_argument('-pp', '--print-param',
                           action='store_const', dest='printParameters',
                           default=False, const=True, help=msg)

    return


#
#
# main function
#
#


def main():

    ############################################################
    #
    # generic part
    #
    ############################################################

    #
    # initialization
    ##
    start_time = time.localtime()
    monitoring = common.Monitoring()
    #experiment = datadir.Experiment()

    # reading command line arguments
    # and update from command line arguments
    #
    parser = ArgumentParser(description='Naming by propagation')
    _set_options(parser)
    args = parser.parse_args()

    #experiment.update_from_args(args)

    #
    # reading parameter files
    # and updating parameters
    #
    parameter_file = common.get_parameter_file(args.parameterFile)
    #experiment.update_from_parameter_file(parameter_file)


    #
    # keep history of command line executions
    # and copy parameter file
    #
    #experiment.update_history_at_start(__file__, start_time, parameter_file, path_to_vt())
    #experiment.copy_stamped_file(start_time, parameter_file)

    #
    # copy monitoring information into other "files"
    # so the log filename is known
    #
    #common.monitoring.copy(monitoring)
    #datadir.monitoring.copy(monitoring)

    #################
    # manage parameters
    # 1. initialize
    # 2. update parameters
    # 3. write parameters into the logfile
    #

    parameters = daparameters.DivisionAnalysisParameters(parameter_file)
    #parameters.write_parameters(monitoring.log_filename) # write in
    if args.printParameters:
        parameters.print_parameters()
    #
    # processing
    #

    # depending on parameters order call the needed functions
    process_parameters(parameters)

    #
    # end of execution
    # write execution time in both log and history file
    #
    end_time = time.localtime()
    #monitoring.update_execution_time(start_time, end_time)
    #experiment.update_history_execution_time(__file__, start_time, end_time)

    monitoring.to_console('Total computation time = ' + str(time.mktime(end_time) - time.mktime(start_time)) + ' s')


def process_parameters(parameters):
    """
    Calls corresponding functions

    Parameters
    ----------
    parameters

    Returns
    -------

    """
    # Get all pickle file names in the folder
    files = parameters.atlasFiles  # Directly use the list of atlas file paths
    ref_file = parameters.atlasRefFile[0]

    #analyse stability of surface of contacts between tissues at different timepoints after scaling different embryos
    if parameters.stability_tissues_surface_of_contact:
        tvsc.stability_tissues_surface_of_contacts_overtime(files, ref_file,  parameters.output_folder, imaging_res=0.3,
                                                    embryo_res='not_halfs', tissues_resolution=parameters.tissue_res,
                                                            start_time_nb_of_cells=218)
    elif parameters.tissues_nb_of_cells:
        # nb of cells per tissue
        tnbcells.nb_of_cell_per_dev_time_per_tissue(files, ref_file, parameters.tissue_res, parameters.output_folder)

    elif parameters.daughter_mother_div_dir:
        ustb.compare_div_dir_to_mothers(files, parameters.delay, parameters.output_folder)

    elif parameters.coord_ref_sys_assess:
        assess_coord_sys_stability(files, ref_file, 30, parameters.output_folder)

    elif parameters.registration_assessment:
        # Get all pkl file names in folder (not affected by parameters)
        if not os.path.exists(parameters.output_folder):
            os.makedirs(parameters.output_folder)

        # Call rotations assessment
        if parameters.rotations_assessment:
            rotations_assessment(files, ref_file, parameters.delay, parameters.output_folder)
            pass  # Uncomment and call the function when ready

    # Call distance matrix function
    elif parameters.distance_matrix:
        all_embryos_in_same_referential_distance_matrix(
            files, ref_file, 'similitude', 0.75, parameters.output_folder, 'halfs', 0.3, False, 15)

    # Call topological distance matrix function
    elif parameters.topological_distance_matrix:
        all_embryos_in_same_topological_referential_distance_matrix(
            files, ref_file, parameters.output_folder, 'halfs')

    # Call temporal geometric reference function
    elif parameters.temporal_geometric_ref:
        output_folder = os.path.join(parameters.output_folder, 'temporal_spatial_ref_zero_padding_after_mca_at_t')
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
        all_embryos_in_same_referential__temporal_geometric(
            files, ref_file, transformation_type='similitude', retained_fraction=1, output_folder=output_folder,
            d_or_mean='avg', embryo_res='halfs', imaging_res=0.3)

    # Call common cells subspace function
    elif parameters.common_cells_subspace:
        output_folder = os.path.join(parameters.output_folder, 'temporal_spatial_ref_common_cells_subspace')
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
        compute_geom_barycenters_distance_at_each_dev_time_only_common_cells(
            files, ref_file, transformation_type='similitude', retained_fraction=1, output_folder=output_folder,
            embryo_res='halfs', imaging_res=0.3, interval=[8, 58], dif_dev_time=15, weight_by_cell_mass=False,
            tissue_res=True)

    # Perform operations based on the provided parameters
    elif parameters.filter_3_way_junctions:
        # Call the filter function for 3-way junctions
        ufilter.filter_property_file(files, parameters.output_folder)

    elif parameters.stability_of_sisters_approx:
        # Compute stability of division directions and compare interphasic directions
        ustb.compute_stability_of_div_direction(files, parameters.output_folder)
        ustb.comparing_interphasic_directions(files, [7], 'bary', -12, 15, '2d', parameters.output_folder)

    elif parameters.deviation_btw_sisters_interface:
        # Calculate deviation between sisters interface approximations
        ustb.deviation_btw_sisters_interface_approximations_just_after_division(files, [7], parameters.output_folder)

    elif parameters.deviation_apical_to_div_dir:
        # Test the Hertwig rule for division direction
        ustb.test_hertwig(files, [8], 'bary', 2, 12, '2d', parameters.output_folder)
        ustb.test_hertwig(files, [8], 'bary', 2, 12, '3d', parameters.output_folder)
    else:
        ###
        # Grab properties of embryos from folder : embryos data is a list of prop dicts
        #
        atlases_info = get_atlases_info(parameters)
        embryos_data, reference_embryo_data = get_embryos_properties(atlases_info)

        # Compute division properties by comparing embryos with the reference
        divisions_properties = compare_embryos_each_by_its_turn(embryos_data, reference_embryo_data, parameters.delay,
                                                                parameters.local_reg)

        # Perform temporal alignment
        temporal_alignment_coefficient_dict, cells_per_time_in_ref = get_temporal_alignment_coefficients_of_embryos(
            embryos_data, reference_embryo_data)

        # Ensure output folder exists
        if not os.path.exists(parameters.output_folder):
            os.makedirs(parameters.output_folder)

        # Write the division properties to a pickle file
        pickle_file_path = os.path.join(parameters.output_folder, 'divisions_properties.pkl')
        with open(pickle_file_path, 'wb') as pickle_file:
            pickle.dump(divisions_properties, pickle_file)

        # Save temporal alignment coefficients to a pickle file
        pickle_file_path = os.path.join(parameters.output_folder, 'time_alignment_coefficients.pkl')
        with open(pickle_file_path, 'wb') as pickle_file:
            pickle.dump(temporal_alignment_coefficient_dict, pickle_file)

        # Save the number of cells per time point in reference to a pickle file
        pickle_file_path = os.path.join(parameters.output_folder, 'cells_per_time_in_ref.pkl')
        with open(pickle_file_path, 'wb') as pickle_file:
            pickle.dump(cells_per_time_in_ref, pickle_file)

    return 0

#
#
# main call
#
#


if __name__ == '__main__':
    main()


##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2023
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Ven  2 fév 2024 18:12:36 CET
#
##############################################################
#
#
#
##############################################################

from operator import itemgetter

import astec.utils.common as common
import astec.utils.ioproperties as ioproperties
import astec.utils.properties as astec_properties

import ascidian.ascidian.name as aname

#
#
#
#
#

monitoring = common.Monitoring()


########################################################################################
#
#
#
########################################################################################


def _compare_name(d1, d2, name1, name2, description, time_digits_for_cell_id=4):

    e1 = d1[description]
    e2 = d2[description]

    lineage1 = d1['cell_lineage']
    reverse_lineage1 = {v: k for k, values in lineage1.items() for v in values}
    lineage2 = d2['cell_lineage']
    reverse_lineage2 = {v: k for k, values in lineage2.items() for v in values}

    msg = "  === " + str(description) + " comparison between " + str(name1) + " and " + str(name2) + " === "
    monitoring.to_log_and_console(msg, 1)

    intersection = astec_properties.intersection_cell_keys(e1, e2, name1, name2)
    if len(intersection) == 0:
        return

    div = 10 ** time_digits_for_cell_id
    #
    # get the common named time interval
    #
    times1 = list(set([k // div for k in e1]))
    times2 = list(set([k // div for k in e2]))
    msg = "named times are [" + str(min(times1)) + ", " + str(max(times1)) + "] in " + str(name1)
    monitoring.to_log_and_console("    ... " + msg, 1)
    msg = "named times are [" + str(min(times2)) + ", " + str(max(times2)) + "] in " + str(name2)
    monitoring.to_log_and_console("    ... " + msg, 1)
    min_times = max(min(times1), min(times2))
    max_times = min(max(times1), max(times2))
    msg = "comparison will be done in time interval [" + str(min_times) + ", " + str(max_times) + "]"
    monitoring.to_log_and_console("        " + msg, 1)

    cells1 = [k for k in e1 if min_times <= k // div <= max_times]
    cells2 = [k for k in e2 if min_times <= k // div <= max_times]

    intersection = sorted(list(set(cells1).intersection(set(cells2))))
    difference1 = sorted(list(set(cells1).difference(set(cells2))))
    difference2 = sorted(list(set(cells2).difference(set(cells1))))

    #
    # cells with no name
    #
    if len(difference1) > 0:
        msg = str(len(difference1)) + " cells are named in " + str(name1) + " and not in " + str(name2)
        monitoring.to_log_and_console("    ... " + msg, 1)
        monitoring.to_log_and_console("        cell ids are " + str(difference1), 1)
    if len(difference2) > 0:
        msg = str(len(difference2)) + " cells are named in " + str(name2) + " and not in " + str(name1)
        monitoring.to_log_and_console("    ... " + msg, 1)
        monitoring.to_log_and_console("        " + str(difference2), 1)

    #
    # all couple of cells with different names
    # diff_names is a dictionary indexed by couple of (different) names
    # diff_cells is the list of cell ids with different names
    #
    diff_names = {}
    diff_cells = []
    for k in intersection:
        if e1[k] == e2[k]:
            continue
        diff_names[(e1[k], e2[k])] = diff_names.get((e1[k], e2[k]), []) + [k]
        diff_cells += [k]

    if len(diff_names) == 0:
        return

    msg = str(len(diff_names)) + " couples of different names for " + str(len(diff_cells)) + " cells "
    monitoring.to_log_and_console("    ... " + msg, 1)

    cell_starting = []
    cell_with_diff_mothers = []
    cell_with_unnamed_mothers = []
    mother_before_change = []

    for k in intersection:
        if e1[k] == e2[k]:
            continue
        #
        # first cell of a tree/branch
        #
        if k not in reverse_lineage1 or k not in reverse_lineage2:
            cell_starting += [k]
            continue
        #
        # different mother cell id, thus different lineage
        #
        if reverse_lineage1[k] != reverse_lineage2[k]:
            cell_with_diff_mothers += [k]
            continue
        #
        # unnamed mother(s) or with different names
        #
        if reverse_lineage1[k] not in e1 or reverse_lineage2[k] not in e2:
            cell_with_unnamed_mothers += [k]
            continue
        if e1[reverse_lineage1[k]] != e2[reverse_lineage2[k]]:
            cell_with_unnamed_mothers += [k]
            continue
        #
        # both mothers are named, and have the same name
        #
        if e1[reverse_lineage1[k]] == e2[reverse_lineage2[k]]:
            mother_before_change += [reverse_lineage1[k]]

    # processed (ir recognized in displayed errors) tuples
    ptuples = []

    #
    # cells have different names, one is the first cell of a branch
    #
    if len(cell_starting) > 0:
        s = str(len(cell_starting)) + " branches starting with different names"
        monitoring.to_log_and_console("    ... " + s, 1)
        for k in cell_starting:
            ptuples += [(e1[k], e2[k])]
            s = "cell " + str(k) + " (and cells of its branch) has different names: "
            monitoring.to_log_and_console("        '" + s, 1)
            s = str(e1[k]) + "' in '" + str(name1) + "'"
            monitoring.to_log_and_console("          '" + s, 1)
            s = str(e2[k]) + "' in '" + str(name2) + "'"
            monitoring.to_log_and_console("          " + s, 1)

    #
    # cells have different names, mothers have different cell ids
    #
    if len(cell_with_diff_mothers) > 0:
        s = str(len(cell_with_diff_mothers)) + " cells with different names and different mothers"
        monitoring.to_log_and_console("    ... " + s, 1)
        for k in cell_with_diff_mothers:
            ptuples += [(e1[k], e2[k])]
            s = "cell " + str(k) + " has different names and mothers: "
            monitoring.to_log_and_console("        " + s, 1)
            s = "named '" + str(e1[k]) + "' and issued from " + str(reverse_lineage1[k])
            if reverse_lineage1[k] in e1:
                s += "(named '" + str(e1[reverse_lineage1[k]]) + "')"
            else:
                s += "(unnamed)"
            s += " in '" + str(name1) + "'"
            monitoring.to_log_and_console("          " + s, 1)
            s = "named '" + str(e2[k]) + "' and issued from " + str(reverse_lineage2[k])
            if reverse_lineage2[k] in e2:
                s += "(named '" + str(e2[reverse_lineage2[k]]) + "')"
            else:
                s += "(unnamed)"
            s += " in '" + str(name2) + "'"
            monitoring.to_log_and_console("          " + s, 1)

    #
    # cells have different names, mothers have same cell ids
    #
    if len(cell_with_unnamed_mothers) > 0:
        s = str(len(cell_with_unnamed_mothers)) + " cells with unnamed or differently named mothers"
        monitoring.to_log_and_console("    ... " + s, 1)
        for k in cell_with_unnamed_mothers:
            ptuples += [(e1[k], e2[k])]
            s = "cell " + str(k) + " has different names and mothers: "
            monitoring.to_log_and_console("        " + s, 1)
            s = "named '" + str(e1[k]) + "' and issued from " + str(reverse_lineage1[k])
            if reverse_lineage1[k] in e1:
                s += " (named '" + str(e1[reverse_lineage1[k]]) + "')"
            else:
                s += " (unnamed)"
            s += " in '" + str(name1) + "'"
            monitoring.to_log_and_console("          " + s, 1)
            s = "named '" + str(e2[k]) + "' and issued from " + str(reverse_lineage2[k])
            if reverse_lineage2[k] in e2:
                s += " (named '" + str(e2[reverse_lineage2[k]]) + "')"
            else:
                s += " (unnamed)"
            s += " in '" + str(name2) + "'"
            monitoring.to_log_and_console("          " + s, 1)

    #
    # mothers are both named, and have the same name
    #
    if len(mother_before_change) > 0:
        mother_before_change = sorted(list(set(mother_before_change)))
        s = str(len(mother_before_change)) + " cells with differently named daughters"
        monitoring.to_log_and_console("    ... " + s, 1)

        division_switches = []
        division_others = []
        branch_others = []
        other_cases = []
        for c in mother_before_change:
            daughters = sorted(list(set(lineage1[c]).intersection(set(lineage2[c]))))
            if len(lineage1[c]) == 2 and len(lineage2[c]) == 2 and len(daughters) == 2:
                if e1[daughters[0]] == e2[daughters[1]] and e1[daughters[1]] == e2[daughters[0]]:
                    division_switches += [c]
                    continue
                division_others += [c]
                continue
            if len(lineage1[c]) == 1 and len(lineage2[c]) == 1:
                branch_others += [c]
                continue
            other_cases += [c]

        if len(division_switches) > 0:
            s = str(len(division_switches)) + " divisions with switched names"
            monitoring.to_log_and_console("      - " + s, 1)
            zlist = sorted([(c, e1[c]) for c in division_switches], key=itemgetter(1))
            for c, name in zlist:
                s = "cell " + str(c) + " (" + str(name) + ") has daughters with different names:"
                monitoring.to_log_and_console("        " + s, 1)
                daughters = sorted(list(set(lineage1[c]).intersection(set(lineage2[c]))))
                ptuples += [(e1[daughters[0]], e2[daughters[0]])]
                ptuples += [(e1[daughters[1]], e2[daughters[1]])]
                l1 = ""
                l2 = ""
                for i, d in enumerate(daughters):
                    l1 += str(e1[d])
                    l2 += str(e2[d])
                    if i < len(daughters)-1:
                        l1 += ", "
                        l2 += ", "
                s = str(daughters) + " are named [" + str(l1) + "] in '" + str(name1) + "'"
                monitoring.to_log_and_console("          " + s, 1)
                s = " " * len(str(daughters)) + " and named [" + str(l2) + "] in '" + str(name2) + "'"
                monitoring.to_log_and_console("          " + s, 1)

        recognized_switches = sorted(list(set([c[0] for c in ptuples] + [c[1] for c in ptuples])))

        if len(division_others) > 0:
            s = str(len(division_others)) + " divisions with differently named daughters"
            monitoring.to_log_and_console("      - " + s, 1)
            zlist = sorted([(c, e1[c]) for c in division_others], key=itemgetter(1))
            for c, name in zlist:
                s = "cell " + str(c) + " (" + str(name) + ") has daughters with different names:"
                monitoring.to_log_and_console("        " + s, 1)
                daughters = sorted(list(set(lineage1[c]).intersection(set(lineage2[c]))))
                difference1 = sorted(list(set(lineage1[c]).difference(set(lineage2[c]))))
                difference2 = sorted(list(set(lineage2[c]).difference(set(lineage1[c]))))
                ptuples += [(e1[daughters[0]], e2[daughters[0]])]
                ptuples += [(e1[daughters[1]], e2[daughters[1]])]
                l1 = ""
                l2 = ""
                for i, d in enumerate(daughters):
                    l1 += str(e1[d])
                    l2 += str(e2[d])
                    if i < len(daughters)-1:
                        l1 += ", "
                        l2 += ", "
                s = str(daughters) + " are named [" + str(l1) + "] in '" + str(name1) + "'"
                monitoring.to_log_and_console("          " + s, 1)
                s = " " * len(str(daughters)) + " and named [" + str(l2) + "] in '" + str(name2) + "'"
                monitoring.to_log_and_console("          " + s, 1)

                for d in difference1:
                    spaces = ""
                    for i in range(len(str(d))):
                        spaces += " "
                    s = str(d) + " is named " + str(e1[d]) + " in '" + str(name1) + "'"
                    monitoring.to_log_and_console("          " + s, 1)
                    s = spaces + "and is not a daughter of " + str(c) + " in '" + str(name2) + "'"
                    monitoring.to_log_and_console("          " + s, 1)
                for d in difference2:
                    spaces = ""
                    for i in range(len(str(d))):
                        spaces += " "
                    s = str(d) + " is named " + str(e2[d]) + " in '" + str(name2) + "'"
                    monitoring.to_log_and_console("          " + s, 1)
                    s = spaces + "and is not a daughter of " + str(c) + " in '" + str(name1) + "'"
                    monitoring.to_log_and_console("          " + s, 1)

        if len(branch_others) > 0:
            s = str(len(branch_others)) + " branchs with changing names"
            monitoring.to_log_and_console("      - " + s, 1)
            zlist = sorted([(c, e1[c]) for c in branch_others], key=itemgetter(1))
            for c, name in zlist:
                s = "cell " + str(c) + " (" + str(name) + ") has daughter with different names:"
                monitoring.to_log_and_console("        " + s, 1)
                daughters = sorted(list(set(lineage1[c]).intersection(set(lineage2[c]))))
                ptuples += [(e1[lineage1[c][0]], e2[lineage2[c][0]])]
                l1 = ""
                l2 = ""
                for i, d in enumerate(daughters):
                    l1 += str(e1[d])
                    l2 += str(e2[d])
                    if i < len(daughters) - 1:
                        l1 += ", "
                        l2 += ", "
                s = str(lineage1[c][0]) + " is named [" + str(e1[lineage1[c][0]]) + "] in '" + str(name1) + "'"
                monitoring.to_log_and_console("             " + s, 1)
                s = " " * len(str(lineage1[c][0])) + " and named [" + str(e2[lineage2[c][0]]) + "] in '"
                s += str(name2) + "'"
                monitoring.to_log_and_console("             " + s, 1)

        if len(other_cases) > 0:
            s = str(len(other_cases)) + " other cases"
            monitoring.to_log_and_console("      - " + s, 1)
            zlist = sorted([(c, e1[c]) for c in other_cases], key=itemgetter(1))
            for c, name in zlist:
                s = "cell " + str(c) + " (" + str(name) + ") has daughter(s) with different names:"
                monitoring.to_log_and_console("        " + s, 1)
                i1 = ""
                l1 = ""
                for i, d in enumerate(lineage1[c]):
                    i1 += str(d)
                    l1 += str(e1[d])
                    if i < len(lineage1[c]) - 1:
                        i1 += ", "
                        l1 += ", "
                i2 = ""
                l2 = ""
                for i, d in enumerate(lineage2[c]):
                    i2 += str(d)
                    l2 += str(e1[d])
                    if i < len(lineage2[c]) - 1:
                        i2 += ", "
                        l2 += ", "
                s = "[" + i1 + "] are named [" + l1 + "] in '" + str(name1) + "'"
                monitoring.to_log_and_console("          " + s, 1)
                s = "[" + i2 + "] are named [" + l2 + "] in '" + str(name2) + "'"
                monitoring.to_log_and_console("          " + s, 1)

    #
    # display remaining errors
    # discard errors due to previous errors
    #
    induced_tuples = []
    for c in diff_names:
        if c in ptuples:
            continue
        if aname.get_mother_name(c[0]) in recognized_switches and aname.get_mother_name(c[1]) in recognized_switches:
            induced_tuples += [c]
            recognized_switches += [c[0], c[1]]
            continue

    if len(induced_tuples) > 0:
        s = str(len(induced_tuples)) + " induced errors from switched divisions"
        monitoring.to_log_and_console("      - " + s, 1)

    remaining_tuples = set(diff_names).difference(set(ptuples).union(induced_tuples))
    if len(remaining_tuples) > 0:
        s = str(len(remaining_tuples)) + " remaining errors"
        monitoring.to_log_and_console("      - " + s, 1)
        for c in remaining_tuples:
            msg = str(len(diff_names[c])) + " cells are named '"
            msg += str(c[0]) + "' in " + str(name1) + " and '"
            msg += str(c[1]) + "' in " + str(name2)
            monitoring.to_log_and_console("        " + msg)
    return


def comparison(d1, d2, features, name1, name2):
    """

    :param d1:
    :param d2:
    :param features:
    :param name1:
    :param name2:
    :return:
    """

    astec_properties.comparison(d1, d2, features, name1, name2)

    #
    # 1. find common keys
    #
    pairedkeys = astec_properties.compare_keys(d1, d2, features, name1, name2, print_summary=True)

    #
    # 2. perform a comparison key by key
    #

    if len(pairedkeys) == 0:
        return

    #
    # recall that the dictionary keys are the 'output_key' of the ioproperties.keydictionary
    #

    if features is None or len(features) == 0:
        comparison_keys = [k[2] for k in pairedkeys]
    else:
        comparison_keys = features

    monitoring.to_log_and_console("", 1)

    for f in comparison_keys:
        if f not in ioproperties.keydictionary:
            monitoring.to_log_and_console("    unknown property '" + str(f) + "' for comparison", 1)
            continue

        outk = ioproperties.keydictionary[f]['output_key']

        for i in range(len(pairedkeys)):
            if pairedkeys[i][0] == outk:
                if outk == ioproperties.keydictionary['lineage']['output_key']:
                    pass
                elif outk == ioproperties.keydictionary['h_min']['output_key']:
                    pass
                    # monitoring.to_log_and_console("    comparison of '" + str(outk) + "' not implemented yet", 1)
                elif outk == ioproperties.keydictionary['volume']['output_key']:
                    pass
                elif outk == ioproperties.keydictionary['sigma']['output_key']:
                    pass
                    # monitoring.to_log_and_console("    comparison of '" + str(outk) + "' not implemented yet", 1)
                elif outk == ioproperties.keydictionary['label_in_time']['output_key']:
                    pass
                    # monitoring.to_log_and_console("    comparison of '" + str(outk) + "' not implemented yet", 1)
                elif outk == ioproperties.keydictionary['barycenter']['output_key']:
                    pass
                elif outk == ioproperties.keydictionary['fate']['output_key']:
                    pass
                elif outk == ioproperties.keydictionary['all-cells']['output_key']:
                    pass
                elif outk == ioproperties.keydictionary['principal-value']['output_key']:
                    pass
                elif outk == ioproperties.keydictionary['name']['output_key']:
                    _compare_name(d1, d2, name1, name2, outk)
                elif outk == ioproperties.keydictionary['contact']['output_key']:
                    pass
                elif outk == ioproperties.keydictionary['history']['output_key']:
                    pass
                    # monitoring.to_log_and_console("    comparison of '" + str(outk) + "' not implemented yet", 1)
                elif outk == ioproperties.keydictionary['principal-vector']['output_key']:
                    pass
                else:
                    monitoring.to_log_and_console("    unknown key '" + str(outk) + "' for comparison", 1)
                break

    return

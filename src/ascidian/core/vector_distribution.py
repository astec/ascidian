##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2023
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Mar  7 nov 2023 14:14:21 CET
#
##############################################################
#
#
#
##############################################################

"""
Computation of (unit) vector distribution
"""

import os
import math
import copy

import numpy as np
import sklearn.preprocessing as skpreproc


import astec.utils.common as common


monitoring = common.Monitoring()


########################################################################################
#
# sphere discretization
#
########################################################################################

def _size_center_from_radius(r):
    ir = math.ceil(r)
    s = int(2 * ir + 1 + 2)
    return s, ir+1


def _sphere_matrix(r=3):
    """
    Draw an hollow discrete sphere in a 3D numpy array. Sphere points will have the 2 value.

    Parameters
    ----------
    r : float, default=3
        sphere radius
        radius =  2.0:    26 vectors, angle between neighboring vectors in [36.26, 60.0] degrees
        radius =  2.3:    38 vectors, angle between neighboring vectors in [26.57, 54.74] degrees
        radius =  2.5:    54 vectors, angle between neighboring vectors in [24.09, 43.09] degrees
        radius =  2.9:    66 vectors, angle between neighboring vectors in [18.43, 43.09] degrees
        radius =  3.0:    90 vectors, angle between neighboring vectors in [17.72, 43.09] degrees
        radius =  3.5:    98 vectors, angle between neighboring vectors in [15.79, 32.51] degrees
        radius =  3.7:   110 vectors, angle between neighboring vectors in [15.26, 32.51] degrees
        radius =  3.8:   134 vectors, angle between neighboring vectors in [14.76, 29.50] degrees
        radius =  4.0:   222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
        radius =  5.0:   222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
        radius = 10.0:   978 vectors, angle between neighboring vectors in [4.40, 10.58] degrees
        radius = 15.0:  2262 vectors, angle between neighboring vectors in [2.98, 6.93] degrees
        radius = 20.0:  4026 vectors, angle between neighboring vectors in [2.25, 5.16] degrees
        radius = 25.0:  6366 vectors, angle between neighboring vectors in [1.73, 4.01] degrees
        radius = 30.0:  9194 vectors, angle between neighboring vectors in [1.46, 3.40] degrees
        radius = 35.0: 12542 vectors, angle between neighboring vectors in [1.26, 2.90] degrees
        radius = 40.0: 16418 vectors, angle between neighboring vectors in [1.08, 2.53] degrees

    Returns
    -------
    a 3D numpy ndarray where outer sphere points have non-zero value, and the center coordinates

    """

    #
    # r is radius
    # matrix size will be 2*r + 1 (center) + 2 (margins)
    # center is r+1
    #
    s, c = _size_center_from_radius(r)
    m = np.zeros((s, s, s), dtype=np.int8)

    # fill the sphere
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            for k in range(m.shape[2]):
                di = float(i) - float(c)
                dj = float(j) - float(c)
                dk = float(k) - float(c)
                if math.sqrt(di * di + dj * dj + dk * dk) <= r:
                    m[i][j][k] = 1
    # set the border voxel to 2
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            for k in range(m.shape[2]):
                if m[i][j][k] < 1:
                    continue
                if i > 0 and m[i - 1][j][k] < 1:
                    m[i][j][k] = 2
                    continue
                if i < m.shape[0] - 1 and m[i + 1][j][k] < 1:
                    m[i][j][k] = 2
                    continue
                if j > 0 and m[i][j - 1][k] < 1:
                    m[i][j][k] = 2
                    continue
                if j < m.shape[1] - 1 and m[i][j + 1][k] < 1:
                    m[i][j][k] = 2
                    continue
                if k > 0 and m[i][j][k - 1] < 1:
                    m[i][j][k] = 2
                    continue
                if k < m.shape[2] - 1 and m[i][j][k + 1] < 1:
                    m[i][j][k] = 2
                    continue
    # erase the inner sphere
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            for k in range(m.shape[2]):
                if m[i][j][k] == 1:
                    m[i][j][k] = 0

    return m, (c, c, c)


def sphere_vector(r=3):
    """
    Returns the vectors pointing towards voxels of a discretized sphere.

    Parameters
    ----------
    ``r`` : sphere radius, default=3
        * radius =  2.0:    26 vectors, angle between neighboring vectors in [36.26, 60.0] degrees
        * radius =  2.3:    38 vectors, angle between neighboring vectors in [26.57, 54.74] degrees
        * radius =  2.5:    54 vectors, angle between neighboring vectors in [24.09, 43.09] degrees
        * radius =  2.9:    66 vectors, angle between neighboring vectors in [18.43, 43.09] degrees
        * radius =  3.0:    90 vectors, angle between neighboring vectors in [17.72, 43.09] degrees
        * radius =  3.5:    98 vectors, angle between neighboring vectors in [15.79, 32.51] degrees
        * radius =  3.7:   110 vectors, angle between neighboring vectors in [15.26, 32.51] degrees
        * radius =  3.8:   134 vectors, angle between neighboring vectors in [14.76, 29.50] degrees
        * radius =  4.0:   222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
        * radius =  5.0:   222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
        * radius = 10.0:   978 vectors, angle between neighboring vectors in [4.40, 10.58] degrees
        * radius = 15.0:  2262 vectors, angle between neighboring vectors in [2.98, 6.93] degrees
        * radius = 20.0:  4026 vectors, angle between neighboring vectors in [2.25, 5.16] degrees
        * radius = 25.0:  6366 vectors, angle between neighboring vectors in [1.73, 4.01] degrees
        * radius = 30.0:  9194 vectors, angle between neighboring vectors in [1.46, 3.40] degrees
        * radius = 35.0: 12542 vectors, angle between neighboring vectors in [1.26, 2.90] degrees
        * radius = 40.0: 16418 vectors, angle between neighboring vectors in [1.08, 2.53] degrees

    Returns
    -------
    A numpy array of shape (N, 3) of unit vectors

    """
    # m will have non-zero value at sphere border
    m, c = _sphere_matrix(r=r)
    v = []
    angles = []
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            for k in range(m.shape[2]):
                if m[i][j][k] == 0:
                    continue
                di = float(i) - float(c[0])
                dj = float(j) - float(c[1])
                dk = float(k) - float(c[2])
                norm = math.sqrt(di * di + dj * dj + dk * dk)
                u = np.array([di / norm, dj / norm, dk / norm])
                v += [u]
                for di in range(-1, 2):
                    for dj in range(-1, 2):
                        for dk in range(-1, 2):
                            if di == 0 and dj == 0 and dk == 0:
                                continue
                            if m[i + di][j + dj][k + dk] == 0:
                                continue
                            v = np.array([(i + di - c[0]), (j + dj - c[1]), (k + dk - c[2])])
                            angles += [math.acos(np.dot(u, v / np.linalg.norm(v)))]

    v = np.array(v)

    monitoring.to_log_and_console("      ... direction distribution build with r = " + str(r), 2)
    monitoring.to_log_and_console("          vectors = " + str(v.shape[0]), 2)
    min_angles = min(angles) * 180.0 / np.pi
    max_angles = max(angles) * 180.0 / np.pi
    msg = "          angles between adjacent vectors in [{:.2f}, {:.2f}]".format(min_angles, max_angles)
    monitoring.to_log_and_console(msg, 2)

    return v


def _sphere_distribution_support(sphere_radius, verbose=True):
    """

    :param sphere_radius:
    :return:
    a array of dictionaries, each dictionary having the following keys
    - 'voxel': the 3D point in the 3D array correponding to the vector
    - 'vector': the unit vector (joigning the 'voxel' and the array center)
    - 'value': the distribution value

    """

    m, c = _sphere_matrix(sphere_radius)

    #
    # points of the outer sphere have a value of 2, all other points are at 0
    #

    direction_distribution = []

    angles = []
    for i in range(m.shape[0]):
        for j in range(m.shape[1]):
            for k in range(m.shape[2]):
                if m[i][j][k] == 0:
                    continue
                p = {'voxel': (i, j, k)}
                v = np.array([(i - c[0]), (j - c[1]), (k - c[2])])
                p['vector'] = v / np.linalg.norm(v)
                p['value'] = 0.0
                direction_distribution += [p]
                #
                # error evaluation: angles between neighboring vectors
                #
                for di in range(-1, 2):
                    for dj in range(-1, 2):
                        for dk in range(-1, 2):
                            if di == 0 and dj == 0 and dk == 0:
                                continue
                            if m[i+di][j+dj][k+dk] == 0:
                                continue
                            v = np.array([(i + di - c[0]), (j + dj - c[1]), (k + dk - c[2])])
                            angles += [math.acos(np.dot(p['vector'], v / np.linalg.norm(v)))]

    if verbose:
        msg = "      ... direction distribution support build with r = " + str(sphere_radius)
        monitoring.to_log_and_console(msg, 2)
        monitoring.to_log_and_console("          vectors = " + str(len(direction_distribution)), 2)
        min_angles = min(angles)*180.0/np.pi
        max_angles = max(angles) * 180.0 / np.pi
        msg = "          angles between adjacent vectors in [{:.2f}, {:.2f}]".format(min_angles, max_angles)
        monitoring.to_log_and_console(msg, 2)

    return direction_distribution


def _fill_distribution(distribution, vectors, weights, kernel_sigma=0.15, verbose=True):

    #
    # distribution support: it is an array of dictionaries, each dictionary having the following keys
    # - 'voxel': the 3D point in the 3D array corresponding to the vector
    # - 'vector': the unit vector (joigning the 'voxel' and the array center)
    # - 'value': the distribution value
    #
    for i in range(len(distribution)):
        distribution[i]['value'] = 0.0

    contribution_list = np.zeros(len(distribution))

    #
    # initialisation
    #
    div = 2 * kernel_sigma * kernel_sigma
    # do not compute contribution for angles > 3 * sigma
    angle_threshold = 3.0 * kernel_sigma

    if verbose:
        msg = "      ... build direction distribution from {:d} vectors".format(len(vectors))
        monitoring.to_log_and_console(msg, 2)

    for k, v in enumerate(vectors):
        #
        # get contributions for vector
        #
        plist = []
        sum_positive_contributions = 0.0

        for i, p in enumerate(distribution):
            sp = p['vector'][0] * v[0] + p['vector'][1] * v[1] + p['vector'][2] * v[2]
            sp = max(min(sp, 1.0), -1.0)
            #
            # positive contribution
            # math.acos(x) return the arc cosine of x, in radians. The result is between 0 and pi.
            #
            angle = math.acos(sp)
            if angle < angle_threshold:
                contrib = math.exp(- angle * angle / div)
                sum_positive_contributions += contrib
                plist += [(i, contrib)]

        #
        # make sure the sum of contributions of vectors[k] is weights[k]
        #
        for (i, contrib) in plist:
            contribution_list[i] += weights[k] * contrib / sum_positive_contributions

    #
    # set distribution values on distribution support
    # set the maximum of distribution to 1.0
    #
    max_contribution = max(contribution_list)
    for i, v in enumerate(contribution_list):
        distribution[i]['value'] = v / max_contribution

    return distribution


def _array_from_distribution(distribution, sphere_radius):
    s, c = _size_center_from_radius(sphere_radius)
    values = np.zeros((s, s, s))
    for p in distribution:
        values[p['voxel'][0]][p['voxel'][1]][p['voxel'][2]] = p['value']
    return values


########################################################################################
#
#
#
########################################################################################


def _print_distribution(d):
    for i, p in enumerate(d):
        msg = "#{:4d}".format(i)
        msg += " - "
        msg += "({:5.2f}, {:5.2f}, {:5.2f})".format(p['vector'][0], p['vector'][1], p['vector'][2])
        msg += " - "
        msg += "{:5.3f}".format(p['value'])
        print(msg)


########################################################################################
#
#
#
########################################################################################


def _write_3d_array(f, varname, a, length=2, file_suffix=None):
    form = "{:1." + str(length) + "f}"
    f.write(str(varname))
    if file_suffix is not None:
        f.write(str(file_suffix))
    f.write(" = np.array(")
    f.write("[")
    for z in range(a.shape[0]):
        f.write("[")
        for y in range(a.shape[1]):
            f.write("[")
            for x in range(a.shape[2]):
                f.write(form.format(a[z][y][x]))
                if x < a.shape[2]-1:
                    f.write(", ")
            f.write("]")
            if y < a.shape[1] - 1:
                f.write(", \n")
        f.write("]")
        if z < a.shape[0] - 1:
            f.write(", \n")
    f.write("])\n")


########################################################################################
#
# sphere discretization
#
########################################################################################


class VectorDistribution(object):
    def __init__(self, vectors=None, weights=None, sphere_radius=20, kernel_sigma=0.15, add_opposite=False,
                 verbose=True):
        """
        Distribution of vectors on a sphere, estimated by kernel density estimation with a gaussian kernel

        Parameters
        ----------
        vectors: vectors, list of list or np.ndarray of shape (N, 3) where N is the number of sample vectors
        weights: list of weights or np.ndarray of shape (N)
            if None, equal weights (1) are used
        sphere_radius: default=20. Size of the distribution support
            - radius = 3:     90 vectors, angle between neighboring vectors in [17.72, 43.09] degrees
            - radius = 5:    222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
            - radius = 10:   978 vectors, angle between neighboring vectors in [4.40, 10.58] degrees
            - radius = 15:  2262 vectors, angle between neighboring vectors in [2.98, 6.93] degrees
            - radius = 20:  4026 vectors, angle between neighboring vectors in [2.25, 5.16] degrees
            - radius = 25:  6366 vectors, angle between neighboring vectors in [1.73, 4.01] degrees
            - radius = 30:  9194 vectors, angle between neighboring vectors in [1.46, 3.40] degrees
            - radius = 35: 12542 vectors, angle between neighboring vectors in [1.26, 2.90] degrees
            - radius = 40: 16418 vectors, angle between neighboring vectors in [1.08, 2.53] degrees
        kernel_sigma: default=0.15
            Standard deviation of the gaussian kernel (in radians)
        add_opposite: if ``True``, add opposite vectors to the distribution.
            It comes to compute a distribution of directions. As a consequence,
            the number of distribution maxima will be even, and each successive pair of maxima
            will be a couple of opposite vectors.

        Returns
        -------
        self: instance of the distribution

        Examples
        --------
        >>> import ascidian.core.vector_distribution as vdist
        >>> import numpy as np
        >>> import scipy
        >>> f = scipy.stats.multivariate_normal.rvs([2,0,2], [[1,0,0],[0,1,0],[0,0,1]], 500, random_state=0)
        >>> g = scipy.stats.multivariate_normal.rvs([-1,-3,1], [[1,0,0],[0,1,0],[0,0,1]], 400, random_state=0)
        >>> h = np.concatenate((f,g))
        >>> d = vdist.VectorDistribution(vectors=h,sphere_radius=10)
        >>> d.maxima()

        References
        ----------
        .. [Kernel_density_estimation] https://en.wikipedia.org/wiki/Kernel_density_estimation
        """
        #
        # class values
        #
        # inputs
        self._sample_vectors = None
        self._sample_weights = None
        # parameters
        self._sphere_radius = 20
        self._kernel_sigma = 0.15

        # distribution
        self._distribution = None
        self._array = None
        #

        #
        # analyse samples
        #
        local_vectors = None
        if vectors is not None:
            if isinstance(vectors, np.ndarray):
                local_vectors = vectors
            elif isinstance(vectors, list):
                local_vectors = np.array(vectors)
            else:
                msg = "VectorDistribution: vectors type '" + str(type(vectors)) + "' not handled yet"
                monitoring.to_log_and_console(msg)
                return
            if local_vectors.shape[1] != 3:
                msg = "VectorDistribution: vectors shape[1] = " + str(local_vectors.shape[1])
                msg += ", should be 3"
                monitoring.to_log_and_console(msg)
                return
        local_weights = None
        if weights is not None:
            if vectors is None:
                msg = "VectorDistribution: there weights but no vectors ?!"
                monitoring.to_log_and_console(msg)
                return
            if isinstance(weights, np.ndarray):
                local_weights = weights
            elif isinstance(vectors, list):
                local_weights = np.array(weights)
            else:
                msg = "VectorDistribution: weights type '" + str(type(weights)) + "' not handled yet"
                monitoring.to_log_and_console(msg, 2)
                return
            if len(local_weights.shape) != 1 and local_weights.shape[1] > 1:
                msg = "VectorDistribution: weights shape[1] = " + str(local_weights.shape[1])
                msg += ", should be 1"
                monitoring.to_log_and_console(msg)
                return
            if local_weights.shape[0] != local_vectors.shape[0]:
                msg = "VectorDistribution: weights shape[0] = " + str(local_weights.shape[0])
                msg += ", should be equal to vectors shape[0] = " + str(local_vectors.shape[0])
                monitoring.to_log_and_console(msg)
                return

        #
        # build the distribution =
        # a array of dictionaries, each dictionary having the following keys
        #     - 'voxel': the 3D point in the 3D array corresponding to the vector
        #     - 'vector': the unit vector (joining the 'voxel' and the array center)
        #     - 'value': the distribution value
        self._distribution = _sphere_distribution_support(sphere_radius=sphere_radius, verbose=verbose)
        self._sphere_radius = sphere_radius

        #
        # nothing to do
        #
        if local_vectors is None:
            return

        #
        # keep samples, in case we want
        # - to add vectors in a dedicated method (not done yet)
        # - change parameters (sphere radius, kernel sigma)
        #
        if add_opposite is False:
            # normalize vectors to get unit vectors
            self._sample_vectors = skpreproc.normalize(local_vectors)
            if local_weights is not None:
                self._sample_weights = copy.deepcopy(local_weights)
            else:
                self._sample_weights = np.ones(local_vectors.shape[0])
        else:
            # normalize vectors to get unit vectors
            self._sample_vectors = skpreproc.normalize(np.concatenate((local_vectors, -local_vectors)))
            if local_weights is not None:
                self._sample_weights = copy.deepcopy(np.concatenate((local_weights, local_weights)))
            else:
                self._sample_weights = np.ones(2 * local_vectors.shape[0])

        #
        # compute distribution
        #
        self._distribution = _fill_distribution(self._distribution, self._sample_vectors, self._sample_weights, 
                                                kernel_sigma=kernel_sigma, verbose=verbose)
        return

    def maxima(self, maxima_threshold=0.5, maxima_number=None, verbose=True):
        """
        Get maxima of the distribution

        Parameters
        ----------
        maxima_threshold: only select maxima with values above the threshold.
            Distribution has been normalized so that its maximal value is 1.0.
        maxima_number: maximal number of maxima to be returned.
            If None, all maxima (above the threshold) are returned
        verbose: ``True`` or ``False``

        Returns
        -------
        vectors: a numpy array of shape (N, 3) of vectors with local maximal distribution value
            sorted in distribution value decreasing order.
            If `add_opposite` have been set to `True` when building the distribution, each pair
            of successive vectors are opposite vectors.
        values: a numpy array of shape (N) of local maximal distribution values associated to the vectors
        """

        self._array = _array_from_distribution(self._distribution, self._sphere_radius)
        distribution_maxima = []
        for p in self._distribution:
            #
            # hard threshold on maxima
            # recall the maximum of the distribution has to set to 1.0
            #
            if p['value'] <= maxima_threshold:
                continue
            i = p['voxel'][0]
            j = p['voxel'][1]
            k = p['voxel'][2]
            if self._array[i][j][k] == 0.0:
                continue
            if self._array[i][j][k] < self._array[i - 1][j - 1][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i][j - 1][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j - 1][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i][j][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j + 1][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i][j + 1][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j + 1][k - 1]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j - 1][k]:
                continue
            if self._array[i][j][k] < self._array[i][j - 1][k]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j - 1][k]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j][k]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j][k]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j + 1][k]:
                continue
            if self._array[i][j][k] < self._array[i][j + 1][k]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j + 1][k]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j - 1][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i][j - 1][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j - 1][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i][j][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i - 1][j + 1][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i][j + 1][k + 1]:
                continue
            if self._array[i][j][k] < self._array[i + 1][j + 1][k + 1]:
                continue
            distribution_maxima += [p]

        #
        #
        #
        if len(distribution_maxima) == 0:
            return np.array([]), np.array([])

        #
        # sort maxima in descending  order
        #
        distribution_maxima = sorted(distribution_maxima, reverse=True, key=lambda x: x['value'])

        if verbose:
            msg = "      ... found {:d} distribution maxima".format(len(distribution_maxima))
            monitoring.to_log_and_console(msg, 2)

        selected_maxima = len(distribution_maxima)
        if maxima_number is not None and isinstance(maxima_number, int):
            if 0 < maxima_number < len(distribution_maxima):
                selected_maxima = maxima_number

        vectors_maxima = []
        values_maxima = []
        for p in distribution_maxima[:selected_maxima]:
            vectors_maxima += [p['vector']]
            values_maxima += [p['value']]

        return np.array(vectors_maxima), np.array(values_maxima)

    def figure(self, filename='figure_vector_distribution', file_suffix=None, output_dir=None, cmap='autumn'):
        """

        Parameters
        ----------
        filename:
        file_suffix:
        output_dir:
        cmap:

        Returns
        -------

        Example
        -------
        >>> import ascidian.core.vector_distribution as vdist
        >>> import scipy
        >>> f = scipy.stats.multivariate_normal.rvs([2,-2,2], [[1,0,0],[0,1,0],[0,0,1]], 100)
        >>> d = vdist.VectorDistribution(vectors=f,sphere_radius=4)
        >>> d.figure(cmap='autumn')
        """

        proc = "VectorDistribution.figure"
        
        self._array = _array_from_distribution(self._distribution, self._sphere_radius)

        local_filename = filename

        localfile_suffix = None
        if file_suffix is not None and isinstance(file_suffix, str) and len(file_suffix) > 0:
            localfile_suffix = '_' + file_suffix
        if localfile_suffix is not None:
            local_filename += localfile_suffix
        local_filename += '.py'

        if output_dir is not None and isinstance(output_dir, str):
            if not os.path.isdir(output_dir):
                if not os.path.exists(output_dir):
                    os.makedirs(output_dir)
                else:
                    monitoring.to_log_and_console(proc + ": '" + str(output_dir) + "' is not a directory ?!")
            if os.path.isdir(output_dir):
                local_filename = os.path.join(output_dir, local_filename)

        sphere, center = _sphere_matrix(self._sphere_radius)

        f = open(local_filename, "w")

        f.write("import numpy as np\n")
        f.write("import matplotlib.pyplot as plt\n")

        f.write("\n")
        f.write("savefig = True\n")
        f.write("\n")
        _write_3d_array(f, "sphere", sphere, file_suffix=localfile_suffix)
        f.write("\n")
        _write_3d_array(f, "values", self._array, file_suffix=localfile_suffix)
        f.write("\n")
        f.write("cmap = plt.get_cmap('" + str(cmap) + "')\n")
        f.write("colors")
        if localfile_suffix is not None:
            f.write(str(localfile_suffix))
        f.write(" = cmap(values")
        if localfile_suffix is not None:
            f.write(str(localfile_suffix))
        f.write(")\n")
        f.write("\n")

        # and plot everything
        f.write("ax = plt.figure().add_subplot(projection='3d')\n")
        f.write("ax.voxels(sphere")
        if localfile_suffix is not None:
            f.write(str(localfile_suffix))
        f.write(", facecolors=colors")
        if localfile_suffix is not None:
            f.write(localfile_suffix)
        f.write(", edgecolors='k', linewidth=0.5)\n")
        f.write("ax.set(xlabel='x', ylabel='y', zlabel='z')\n")
        f.write("ax.set_aspect('equal')\n")

        f.write("\n")
        f.write("if savefig:\n")
        f.write("    plt.savefig('vector_distribution")
        if localfile_suffix is not None:
            f.write(localfile_suffix)
        f.write("'" + " + '.png')\n")
        f.write("else:\n")
        f.write("    plt.show()\n")
        f.write("    plt.close()\n")
        f.close()

##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2023
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Dim 22 oct 2023 11:36:18 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys
import copy

from collections import Counter

import astec.utils.common as common
import astec.utils.diagnosis as astecdiagnosis
import astec.utils.ioproperties as ioproperties

import ascidian.ascidian.name as aname
import ascidian.core.contact_surface_distance as cs_distance
from ascidian.division_analysis.division_analysis_tools import determine_fate_group

#
#
#
#
#

monitoring = common.Monitoring()


########################################################################################
#
#
#
########################################################################################


class DiagnosisParameters(astecdiagnosis.DiagnosisParameters):

    def __init__(self, prefix=None):

        astecdiagnosis.DiagnosisParameters.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        #
        # parametres pour le diagnostic sur les surfaces de contact
        #
        doc = "\t for diagnosis on cell contact surface. Threshold on cell contact surface\n"
        doc += "\t distance along branches. Time points along branches that have a cell\n"
        doc += "\t contact surface distance above this threshold are displayed (recall that\n"
        doc += "\t the distance is in [0, 1])."
        self.doc['maximal_contact_distance'] = doc
        self.maximal_contact_distance = 0.15

    def print_parameters(self):
        print("")
        print('#')
        print('# DiagnosisParameters (ascidian)')
        print('#')
        print("")

        astecdiagnosis.DiagnosisParameters.print_parameters(self)

        self.varprint('maximal_contact_distance', self.maximal_contact_distance,
                      self.doc.get('maximal_contact_distance', None))

        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# DiagnosisParameters (ascidian)\n")
        logfile.write("# \n")
        logfile.write("\n")

        astecdiagnosis.DiagnosisParameters.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'maximal_contact_distance', self.maximal_contact_distance,
                      self.doc.get('maximal_contact_distance', None))

        logfile.write("\n")
        return

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    def update_from_parameters(self, parameters):

        astecdiagnosis.DiagnosisParameters.update_from_parameters(self, parameters)

        self.maximal_contact_distance = self.read_parameter(parameters, 'maximal_contact_distance',
                                                            self.maximal_contact_distance)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            print("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)

    def update_from_args(self, args):
        astecdiagnosis.DiagnosisParameters.update_from_args(self, args)


########################################################################################
#
#
#
########################################################################################


def _get_time_interval_from_lineage(direct_lineage, time_digits_for_cell_id=4):
    nodes = list(set(direct_lineage.keys()).union(set([v for values in list(direct_lineage.values()) for v in values])))
    first_time = min(nodes) // 10 ** time_digits_for_cell_id
    last_time = max(nodes) // 10 ** time_digits_for_cell_id

    return first_time, last_time


def _decode_cell_id(s, time_digits_for_cell_id=4):
    div = 10**time_digits_for_cell_id
    return "cell #{:4d}".format(int(s) % div) + " of image #{:4d}".format(int(s) // div)


########################################################################################
#
#
#
########################################################################################


def _diagnosis_name_per_generation(prop, time_digits_for_cell_id=4):
    #
    #
    #
    keylineage = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['lineage']['input_keys'])
    if len(keyset) > 0:
        keylineage = list(keyset)[0]
    lineage = prop[keylineage]
    first_time, last_time = _get_time_interval_from_lineage(lineage, time_digits_for_cell_id=time_digits_for_cell_id)

    keyname = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['name']['input_keys'])
    if len(keyset) > 0:
        keyname = list(keyset)[0]
    name = prop[keyname]

    #
    # get cellids by name
    #
    cellid_per_name = {}
    for i, n in name.items():
        cellid_per_name[n] = cellid_per_name.get(n, []) + [i]
    allnames = set(cellid_per_name.keys())

    monitoring.to_log_and_console("")
    monitoring.to_log_and_console("  - names per generation")

    discarded_names = {}

    generation = 6
    while True:
        stagename = aname.get_generation_name(generation=generation)
        d = set(stagename) - set(allnames)
        msg = "    - generation {:2d}: ".format(generation)
        msg += str(len(stagename) - len(d)) + " names are present out of " + str(len(stagename))
        monitoring.to_log_and_console(msg)
        # stop if no names and stage >= 64 cells
        if len(stagename) == len(d) and generation >= 7:
            break
        # for early unnamed stages go to next stage
        if len(stagename) == len(d):
            generation += 1
            continue

        discarded_generation_names = discarded_names.get(generation, [])
        if len(list(set(d) - set(discarded_generation_names))) > 0:
            msg = "                missing names = " + str(sorted(list(set(d) - set(discarded_generation_names))))
            monitoring.to_log_and_console(msg)
        if len(discarded_generation_names) > 0:
            msg = "                " + str(len(discarded_generation_names)) + " names have been discarded"
            monitoring.to_log_and_console(msg)
        p = set(stagename) - set(d)

        p_end_lineage = []
        p_not_in_lineage = []
        p_one_daughter = []
        p_several_daughter = []
        p_partially_unnamed_daughters = []
        p_unnamed_daughters = []

        for n in p:
            if n in discarded_generation_names:
                continue
            i = max(cellid_per_name[n])
            if i // 10 ** time_digits_for_cell_id == last_time:
                p_end_lineage += [n]
                continue
            if i not in lineage:
                p_not_in_lineage += [n]
                continue
            if len(lineage[i]) == 1:
                p_one_daughter += [n]
                continue
            if len(lineage[i]) > 2:
                p_several_daughter += [n]
                continue
            if lineage[i][0] in name and lineage[i][0] not in name:
                p_partially_unnamed_daughters += [n]
                continue
            if lineage[i][0] not in name and lineage[i][0] in name:
                p_partially_unnamed_daughters += [n]
                continue
            if lineage[i][0] not in name and lineage[i][0] not in name:
                p_unnamed_daughters += [n]
                continue

        if len(p_end_lineage) > 0:
            if len(p_not_in_lineage) == 1:
                msg = "      {:d} cell stops at lineage end ".format(len(p_end_lineage))
            else:
                msg = "      {:d} cells stop at lineage end ".format(len(p_end_lineage))
            monitoring.to_log_and_console(msg)
            msg = "                names = " + str(sorted(p_end_lineage))
            monitoring.to_log_and_console(msg)
            discarded_names[generation] = discarded_names.get(generation, []) + p_end_lineage

        if len(p_not_in_lineage) > 0:
            if len(p_not_in_lineage) == 1:
                msg = "      {:d} cell has no descendant (stops before end?)".format(len(p_not_in_lineage))
            else:
                msg = "      {:d} cells have no descendant (stop before end?)".format(len(p_not_in_lineage))
            monitoring.to_log_and_console(msg)
            msg = "                names = " + str(sorted(p_not_in_lineage))
            monitoring.to_log_and_console(msg)
            discarded_names[generation] = discarded_names.get(generation, []) + p_not_in_lineage

        if len(p_one_daughter) > 0:
            msg = "      {:d} cells have only one daughter?!".format(len(p_one_daughter))
            monitoring.to_log_and_console(msg)
            msg = "                names = " + str(sorted(p_one_daughter))
            monitoring.to_log_and_console(msg)

        if len(p_several_daughter) > 0:
            msg = "      {:d} cells have more than two daughter?!".format(len(p_several_daughter))
            monitoring.to_log_and_console(msg)
            msg = "                names = " + str(sorted(p_several_daughter))
            monitoring.to_log_and_console(msg)

        if len(p_partially_unnamed_daughters) > 0:
            msg = "      {:d} cells have only one named daughter".format(len(p_partially_unnamed_daughters))
            monitoring.to_log_and_console(msg)
            msg = "                names = " + str(sorted(p_partially_unnamed_daughters))
            monitoring.to_log_and_console(msg)

        if len(p_unnamed_daughters) > 0:
            msg = "      {:d} cells have no named daughters".format(len(p_unnamed_daughters))
            monitoring.to_log_and_console(msg)
            msg = "                names = " + str(sorted(p_unnamed_daughters))
            monitoring.to_log_and_console(msg)
            discarded_names[generation] = discarded_names.get(generation, []) + p_unnamed_daughters

        for n in discarded_names.get(generation, []):
            discarded_names[generation + 1] = discarded_names.get(generation + 1, []) + aname.get_daughter_names(n)
        generation += 1
    return

def _diagnosis_name_at_ncells(prop, cells_per_time, n_cells=64):
    #
    # check whether the cell names are correct at #cells = 64 or #cells = 112
    #
    keyname = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['name']['input_keys'])
    if len(keyset) > 0:
        keyname = list(keyset)[0]
    name = prop[keyname]

    times = [t for t in cells_per_time if len(cells_per_time[t]) == n_cells]
    if len(times) == 0:
        msg = "    - no time point with " + str(n_cells) + " cells (from lineage)"
        monitoring.to_log_and_console(msg)
        return
    times = sorted(times)

    msg = "    - there are " + str(n_cells) + " cells "
    if len(times) > 1:
        msg += "from time point " + str(times[0]) + " to time point " + str(times[-1])
    else:
        msg += "uniquely at time point " + str(times[0])
    monitoring.to_log_and_console(msg)

    if n_cells == 64:
        refnames = aname.ascidian_cell_names(64)
    elif n_cells == 76:
        refnames = aname.ascidian_cell_names(76)
    elif n_cells == 112:
        refnames = aname.ascidian_cell_names(112)
    else:
        refnames = None

    for t in times:
        names = set()
        for c in cells_per_time[t]:
            if c in name:
                names.add(name[c])
        if len(names) == 0:
            # msg = "      time point #" + str(t) + " is not named"
            # monitoring.to_log_and_console(msg)
            continue

        if len(names) != n_cells:
            msg = "      weird, there are " + str(len(names)) + " different names at time point "
            msg += str(t) + " instead of " + str(n_cells) + "\n"
            msg += "        this suggests a name is duplicated or that names are missing"
            monitoring.to_log_and_console(msg)
            if refnames is not None:
                diff1 = names.difference(refnames)
                diff2 = refnames.difference(names)
                if len(diff1) > 0:
                    diff1 = sorted(list(diff1))
                    msg = "      - names " + str(diff1) + " exists at time point " + str(t)
                    msg += " but not at " + str(n_cells) + " cells stage "
                    monitoring.to_log_and_console(msg)
                if len(diff2) > 0:
                    diff2 = sorted(list(diff2))
                    msg = "      - names " + str(diff2) + " exists at " + str(n_cells) + " cells stage "
                    msg += " but not at time point " + str(t)
                    monitoring.to_log_and_console(msg)
    return


def _diagnosis_name(prop, description, time_digits_for_cell_id=4):
    keyname = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['name']['input_keys'])
    if len(keyset) > 0:
        keyname = list(keyset)[0]

    keylineage = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['lineage']['input_keys'])
    if len(keyset) > 0:
        keylineage = list(keyset)[0]

    keydiagnosis = 'morphonet_selection_diagnosis_name'
    if keydiagnosis in prop:
        del prop[keydiagnosis]
    prop[keydiagnosis] = {}

    monitoring.to_log_and_console("  === " + str(description) + " diagnosis === ", 1)

    name = prop[keyname]

    #
    # check validity of names
    #
    unvalid_cells = []
    unvalid_names = []
    for c in name:
        if aname.is_name_valid(name[c]):
            continue
        unvalid_cells += [c]
        unvalid_names += [name[c]]
        msg = "    cell " + str(c) + " name '" + str(name[c]) + "' is not valid"
        monitoring.to_log_and_console(msg)
    if len(unvalid_cells) > 0:
        msg = "    " + str(len(unvalid_cells)) + " cells have an invalid name"
        monitoring.to_log_and_console(msg)
        i = 0
        length = 6
        while i < len(unvalid_cells):
            msg = "      " + str(unvalid_cells[i:i+length]) + " -> " + str(unvalid_names[i:i+length])
            monitoring.to_log_and_console(msg)
            i += length

    lineage = prop[keylineage]
    reverse_lineage = {v: k for k, values in lineage.items() for v in values}

    div = 10 ** time_digits_for_cell_id

    cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
    cells = sorted(cells)

    cells_per_time = {}
    names_per_time = {}
    missing_name = {}
    error_name = {}

    divisions = [c for c in lineage if len(lineage[c]) >= 2]
    anamed_mother = []
    named_mother_per_generation = {}
    named_division_per_generation = {}
    for c in divisions:
        if c not in name:
            anamed_mother += [c]
            continue
        g = name[c].split('.')[0][1:]
        dnamed = True
        for d in lineage[c]:
            if d not in name:
                dnamed = False
        if dnamed:
            named_division_per_generation[g] = named_division_per_generation.get(g, []) + [c]
        else:
            named_mother_per_generation[g] = named_mother_per_generation.get(g, []) + [c]

    for c in cells:
        t = int(c) // div
        #
        # get cells and cell names at each time point
        #
        cells_per_time[t] = cells_per_time.get(t, []) + [c]
        if c in name:
            names_per_time[t] = names_per_time.get(t, []) + [name[c]]
        else:
            missing_name[t] = missing_name.get(t, []) + [c]

        if c not in name:
            if c in reverse_lineage:
                mother = reverse_lineage[c]
                if len(lineage[mother]) == 1 and mother in name:
                    prop[keydiagnosis][c] = 10
                    msg = "    cell " + str(c) + " has no name"
                    msg += ", but its mother cell " + str(mother) + " has a name " + str(name[mother])
                    monitoring.to_log_and_console(msg)
        elif c in reverse_lineage:
            mother = reverse_lineage[c]
            if mother not in name:
                prop[keydiagnosis][c] = 20
                msg = "    cell " + str(c) + " has a name '" + str(name[c]) + "'"
                msg += ", but its mother cell " + str(mother) + " has no name"
                monitoring.to_log_and_console(msg)
                error_name[t] = error_name.get(t, []) + [c]
            else:
                if len(lineage[mother]) == 1:
                    if name[mother] != name[c]:
                        prop[keydiagnosis][c] = 30
                        msg = "    cell " + str(c) + " has a name = " + str(name[c])
                        msg += " different than its mother cell " + str(mother) + " name = " + str(name[mother])
                        monitoring.to_log_and_console(msg)
                        error_name[t] = error_name.get(t, []) + [c]
                elif len(lineage[mother]) == 2:
                    daughter_names = aname.get_daughter_names(name[mother])
                    #
                    # check whether name[c] is in daughters' names
                    #
                    if name[c] not in daughter_names:
                        prop[keydiagnosis][c] = 40
                        msg = "    cell " + str(c) + " is named '" + str(name[c]) + "'"
                        msg += " but should be in " + str(daughter_names)
                        msg += " since its mother cell " + str(mother) + " is named " + str(name[mother])
                        monitoring.to_log_and_console(msg)
                        error_name[t] = error_name.get(t, []) + [c]
                    #
                    # check whether c's sister name is in daughters' names
                    #
                    else:
                        siblings = copy.deepcopy(lineage[mother])
                        siblings.remove(c)
                        daughter_names.remove(name[c])
                        if siblings[0] not in name:
                            prop[keydiagnosis][c] = 50
                            msg = "    cell " + str(siblings[0]) + " has no name "
                            msg += ", it should be " + str(daughter_names[0])
                            msg += " since its mother cell " + str(mother) + " is named " + str(name[mother])
                            msg += " and its sibling " + str(c) + " is named " + str(name[c])
                            monitoring.to_log_and_console(msg)
                            error_name[t] = error_name.get(t, []) + [siblings[0]]
                        elif name[siblings[0]] == name[c]:
                            prop[keydiagnosis][c] = 60
                            msg = "    cell " + str(siblings[0]) + " as named " + str(name[c])
                            msg += " as its sibling " + str(c)
                            msg += ", their mother cell " + str(mother) + " is named " + str(name[mother])
                            monitoring.to_log_and_console(msg)
                            error_name[t] = error_name.get(t, []) + [c]
                        elif name[siblings[0]] != daughter_names[0]:
                            prop[keydiagnosis][c] = 70
                            msg = "    cell " + str(siblings[0]) + " is named " + str(name[siblings[0]])
                            msg += " but should be " + str(daughter_names[0])
                            msg += " since its mother cell " + str(mother) + " is named " + str(name[mother])
                            msg += " and its sibling " + str(c) + " is named " + str(name[c])
                            monitoring.to_log_and_console(msg)
                            error_name[t] = error_name.get(t, []) + [siblings[0]]
                else:
                    prop[keydiagnosis][mother] = 80
                    msg = "    cell " + str(mother) + " has " + str(len(lineage[mother])) + " daughter cells"
                    msg += ": " + str(lineage[mother])
                    monitoring.to_log_and_console(msg)

    for t in names_per_time:
        repeats = {k: names_per_time[t].count(k) for k in set(names_per_time[t]) if names_per_time[t].count(k) > 1}
        if repeats != {}:
            msg = "  - there are " + str(len(repeats)) + " repeated names at time " + str(t)
            monitoring.to_log_and_console(msg)
            for n, p in repeats.items():
                repeatedcells = [c for c in name if name[c] == n]
                for c in repeatedcells:
                    prop[keydiagnosis][c] = 90
                msg = "    - " + str(n) + " is repeated " + str(p) + " times "
                monitoring.to_log_and_console(msg)

    monitoring.to_log_and_console("  - first time in lineage = " + str(min(cells_per_time.keys())))
    monitoring.to_log_and_console("  - last time in lineage = " + str(max(cells_per_time.keys())))
    monitoring.to_log_and_console("    - cells in lineage = " + str(len(cells)))
    monitoring.to_log_and_console("    - #cells at first time = " +
                                  str(len(cells_per_time[min(cells_per_time.keys())])))
    monitoring.to_log_and_console("    - #cells at last time = " +
                                  str(len(cells_per_time[max(cells_per_time.keys())])))
    monitoring.to_log_and_console("    - names in lineage = " + str(len(list(Counter(list(name.keys())).keys()))))

    monitoring.to_log_and_console("    - divisions in lineage = " + str(len(divisions)))
    generations = list(set(named_mother_per_generation.keys()).union(set(named_division_per_generation.keys())))
    generations = sorted(generations, key=lambda v: int(v))
    for g in generations:
        monitoring.to_log_and_console("      - generation = " + str(g))
        if g in named_division_per_generation:
            msg = "         - named division (mother cell and daughter cells) = "
            msg += str(len(named_division_per_generation[g]))
            monitoring.to_log_and_console(msg)
        if g in named_mother_per_generation:
            msg = "         - named mother cell with anamed daughters = "
            msg += str(len(named_mother_per_generation[g]))
            monitoring.to_log_and_console(msg)
            msg = "           " + str(named_mother_per_generation[g])
            monitoring.to_log_and_console(msg)
    monitoring.to_log_and_console("      - anamed mother cells = " + str(len(anamed_mother)))

    # check whether the cell names are correct at #cells = 64 or #cells = 112
    _diagnosis_name_at_ncells(prop, cells_per_time, n_cells=64)
    _diagnosis_name_at_ncells(prop, cells_per_time, n_cells=76)
    _diagnosis_name_at_ncells(prop, cells_per_time, n_cells=112)

    # check whether the cell names are correct at #cells = 64 or #cells = 112
    _diagnosis_name_per_generation(prop)

    monitoring.to_log_and_console("")
    return prop


def _diagnosis_contact(prop, description, diagnosis_parameters, time_digits_for_cell_id=4):
    proc = "_diagnosis_contact"

    if not isinstance(diagnosis_parameters, DiagnosisParameters):
        monitoring.to_log_and_console(str(proc) + ": unexpected type for 'diagnosis_parameters' variable: "
                                      + str(type(diagnosis_parameters)))
        sys.exit(1)

    keycontact = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['contact']['input_keys'])
    if len(keyset) > 0:
        keycontact = list(keyset)[0]

    keyname = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['name']['input_keys'])
    if len(keyset) > 0:
        keyname = list(keyset)[0]

    keylineage = None
    keyset = set(prop.keys()).intersection(ioproperties.keydictionary['lineage']['input_keys'])
    if len(keyset) > 0:
        keylineage = list(keyset)[0]

    keydiagnosis = 'morphonet_float_diagnosis_contact_surface'
    if keydiagnosis in prop:
        del prop[keydiagnosis]
    prop[keydiagnosis] = {}

    monitoring.to_log_and_console("  === " + str(description) + " diagnosis === ", 1)

    lineage = prop[keylineage]
    name = {}
    if keyname is not None:
        name = prop[keyname]
    contact = prop[keycontact]

    #
    # get the first time point
    #
    cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
    cells = sorted(cells)
    div = 10 ** time_digits_for_cell_id
    cells_per_time = {}
    for c in cells:
        t = int(c) // div
        #
        # get cells and cell names at each time point
        #
        cells_per_time[t] = cells_per_time.get(t, []) + [c]
    last_time = max(cells_per_time.keys())

    #
    # study single branches beginning right after a division, but not at the last time
    #
    first_cells = [lineage[c][0] for c in lineage if len(lineage[c]) == 2 and int(c) // div < last_time - 1]
    first_cells += [lineage[c][1] for c in lineage if len(lineage[c]) == 2 and int(c) // div < last_time - 1]

    reverse_lineage = {v: k for k, values in lineage.items() for v in values}

    score_along_time = {}
    cells_not_in_reversed_lineage = []
    cells_with_problem = []

    for cell in first_cells:
        if cell not in contact:
            msg = "    * weird, cell " + str(cell) + " is not in the 'contact surface' dictionary"
            monitoring.to_log_and_console(msg)
            continue
        first_time = int(cell) // div
        pcell = cell
        pneigh = copy.deepcopy(contact[cell])
        while True:
            if pcell not in lineage or len(lineage[pcell]) > 1:
                break
            ncell = lineage[pcell][0]
            t = int(ncell) // div
            nneigh = {}
            #
            # build a neighborhood with the same label than at the first time of the branch
            #
            if ncell not in contact:
                break
            for c in contact[ncell]:
                contrib = contact[ncell][c]
                if int(c) % div == 1 or int(c) % div == 0:
                    nneigh[1] = nneigh.get(1, 0.0) + contrib
                else:
                    for i in range(t - first_time):
                        if c not in reverse_lineage:
                            if c not in cells_not_in_reversed_lineage:
                                msg = "    * weird, cell " + str(c) + " is not in the reversed lineage"
                                monitoring.to_log_and_console(msg)
                                cells_not_in_reversed_lineage += [c]
                            if cell not in cells_with_problem:
                                msg = "    - diagnosis on "
                                msg += _decode_cell_id(cell, time_digits_for_cell_id=time_digits_for_cell_id)
                                msg += " should be handled with care"
                                monitoring.to_log_and_console(msg)
                                cells_with_problem += [cell]
                            break
                        c = reverse_lineage[c]
                    nneigh[c] = nneigh.get(c, 0.0) + contrib
            #
            #
            #
            score = cs_distance.contact_surface_distance(pneigh, nneigh, change_contact_surfaces=False)
            score_along_time[cell] = score_along_time.get(cell, []) + [score]
            #
            #
            #
            pcell = ncell
            pneigh = copy.deepcopy(nneigh)

    #
    # report
    #
    msg = "  - largest contact surface distance between successive cells in time (threshold set to "
    msg += str(diagnosis_parameters.maximal_contact_distance) + ")"
    monitoring.to_log_and_console(msg, 1)

    for cell in score_along_time:
        first_time = int(cell) // div
        msg = "    " + _decode_cell_id(cell, time_digits_for_cell_id=time_digits_for_cell_id)
        if cell in name:
            msg += " (" + name[cell] + ") "
        msg += " branch length of " + str(len(score_along_time[cell]))
        if cell in cells_with_problem:
            msg += " (warning: some cells were not in the reversed lineage)"
        #
        # branch of length 1
        #
        if len(score_along_time[cell]) <= 1:
            if first_time == last_time - 1:
                continue
            monitoring.to_log_and_console(msg)
            continue
        #
        # branch with all distances (between successive contact surface vectors) below the threshold
        #
        if max(score_along_time[cell][1:]) < diagnosis_parameters.maximal_contact_distance:
            continue
        #
        # here, the branch has at least a couple of successive successive contact surface vectors has
        # a distance above the threshold
        #
        monitoring.to_log_and_console(msg)
        for i in range(1, len(score_along_time[cell])):
            if score_along_time[cell][i] < diagnosis_parameters.maximal_contact_distance:
                continue
            c = cell
            for j in range(i):
                c = lineage[c][0]
            if c not in prop[keydiagnosis]:
                prop[keydiagnosis][c] = score_along_time[cell][i]
            else:
                prop[keydiagnosis][c] = max(prop[keydiagnosis][c], score_along_time[cell][i])
            prop[keydiagnosis][lineage[c][0]] = score_along_time[cell][i]
            msg = "    - distance of {:.4f}".format(score_along_time[cell][i]) + " between "
            msg += "times " + str(first_time + i) + " and " + str(first_time + i + 1)
            msg += " (cells " + str(c) + " and " + str(lineage[c][0]) + ")"
            monitoring.to_log_and_console(msg)

    monitoring.to_log_and_console("")
    return prop


def _diagnosis_one_feature(prop, feature, diagnosis_parameters, time_digits_for_cell_id=4):

    if feature in ioproperties.keydictionary['name']['input_keys']:
        prop = _diagnosis_name(prop, feature, time_digits_for_cell_id=time_digits_for_cell_id)
    elif feature in ioproperties.keydictionary['contact']['input_keys']:
        prop = _diagnosis_contact(prop, feature, diagnosis_parameters=diagnosis_parameters,
                                  time_digits_for_cell_id=time_digits_for_cell_id)
    else:
        if monitoring.debug > 0:
            monitoring.to_log_and_console("    unknown key '" + str(feature) + "' for diagnosis", 1)

    return prop


def diagnosis(prop, features=None, parameters=None, time_digits_for_cell_id=4, verbose=True):
    """

    :param prop: property dictionary
    :param features:
    :param parameters:
    :param time_digits_for_cell_id:
    :param verbose:
    :return:
    """

    if parameters is not None and isinstance(parameters, DiagnosisParameters):
        diagnosis_parameters = parameters
    else:
        diagnosis_parameters = DiagnosisParameters()

    if verbose:
        monitoring.to_log_and_console("... diagnosis", 1)

    astecdiagnosis.monitoring.copy(monitoring)
    astecdiagnosis.diagnosis(prop, features, parameters, time_digits_for_cell_id=time_digits_for_cell_id, verbose=False)

    #
    # diagnosis on all features of the property dictionary, or only on requested features
    #
    propkeys = list(prop.keys())
    if features is None or (isinstance(features, list) and len(features) == 0) or \
            (isinstance(features, str) and len(features) == 0):
        for k in propkeys:
            prop = _diagnosis_one_feature(prop, k, diagnosis_parameters,
                                          time_digits_for_cell_id=time_digits_for_cell_id)
    else:
        if isinstance(features, str):
            prop = _diagnosis_one_feature(prop, features, diagnosis_parameters,
                                          time_digits_for_cell_id=time_digits_for_cell_id)
        elif isinstance(features, list):
            for f in features:
                prop = _diagnosis_one_feature(prop, f, diagnosis_parameters,
                                              time_digits_for_cell_id=time_digits_for_cell_id)

    if verbose:
        monitoring.to_log_and_console("  === end of diagnosis === ", 1)
        monitoring.to_log_and_console("")

    return prop


def _diagnosis_tissue_name_at_ncells(prop, tissue, n_cells=64, div=10000):
    """
    Diagnoses whether a given tissue has the correct cell names at a certain stage.

    a) filters only cells from considered tissue using determine_fate_group(fates_dict.get(cell_id), true)
    b) call a function to construct cells_per_time {}.
    c) process

    Parameters
    ----------
    prop : dict
        Dictionary containing embryo properties (cell_id: cell_fate, cell_id: cell_name).
    tissue : str
        Name of the tissue to diagnose.
    cells_per_time : dict
        Dictionary of cell fate names per timepoint.
    n_cells : int
        Number of cells to check for, default is 64.

    Returns
    -------
    None
    """

    # Determine which key in `prop` holds the cell names
    name = prop['cell_name']
    fates = prop['cell_fate']
    lineage = prop['cell_lineage']

    # filter only the cells from considered tissue type
    # manage if fate_group is list or not
    tissue_cells = {
        cell_id: cell_name for cell_id, cell_name in name.items()
        if isinstance(determine_fate_group(fates.get(cell_id), True), list)
           and tissue in determine_fate_group(fates.get(cell_id), True) or
           tissue == determine_fate_group(fates.get(cell_id), True)
    }
    name = tissue_cells
    tissue_cells = {
        cell_id: cell_lineage for cell_id, cell_lineage in lineage.items()
        if isinstance(determine_fate_group(fates.get(cell_id), True), list)
           and tissue in determine_fate_group(fates.get(cell_id), True) or
           tissue == determine_fate_group(fates.get(cell_id), True)
    }
    lineage = tissue_cells

    # cells_per_time
    cells_per_time = {}
    names_per_time = {}
    for c, n in name.items():
        t = int(c) // div
        #
        # get cells and cell names at each time point
        #
        cells_per_time[t] = cells_per_time.get(t, []) + [c]
        names_per_time[t] = names_per_time.get(t, []) + [n]

    # Find the time points when the tissue has exactly `n_cells`
    times = [t for t in cells_per_time if len(cells_per_time[t]) == n_cells]
    if len(times) == 0:
        msg = f"    - no time point with {n_cells} cells in tissue {tissue} (from lineage)"
        monitoring.to_log_and_console(msg)
        #print(msg)

        # get names in embryo at closest stage(when nb of cells is closest to n_cells)
        closest_time = min(names_per_time.keys(), key=lambda t: abs(len(names_per_time[t]) - n_cells))
        closest_cells = names_per_time[closest_time]

        # Print details of the closest time point
        msg = f"    - closest time point with {len(closest_cells)} cells is {closest_time}"
        monitoring.to_log_and_console(msg)
        #print(msg)
        # print(closest_cells)

        refnames = aname.ascidian_tissue_cell_names(tissue=tissue, n=n_cells)
        diff1 = set(closest_cells).difference(refnames)  # names in names but not refnames
        diff2 = refnames.difference(closest_cells)
        if len(diff1) > 0:
            diff1 = sorted(list(diff1))
            msg = f"      - names {diff1} exist at time point {t} but not at {n_cells} cells stage."
            monitoring.to_log_and_console(msg)
            #print(msg)
        if len(diff2) > 0:
            diff2 = sorted(list(diff2))
            msg = f"      - names {diff2} exist at {n_cells} cells stage but not at time point {t}."
            monitoring.to_log_and_console(msg)
            #print(msg)
        return
    times = sorted(times)

    # Output a message about when the tissue reached the target number of cells
    msg = f"    - there are {n_cells} cells in tissue {tissue}"
    if len(times) > 1:
        msg += f" from time point {times[0]} to time point {times[-1]}"
    else:
        msg += f" uniquely at time point {times[0]}"
    monitoring.to_log_and_console(msg)
    #print(msg)

    # Get the reference cell names for the tissue at the specified number of cells
    refnames = aname.ascidian_tissue_cell_names(tissue=tissue, n=n_cells)
    msg = f"{sorted(refnames)}, {tissue},{n_cells}"
    print(msg)

    # Compare the cell names at each relevant time point with the reference
    for i, t in enumerate(times):
        names = set()
        for c in cells_per_time[t]:
            if c in name:
                names.add(name[c])

        if len(names) == 0:
            continue

        if len(names) != n_cells:
            msg = f"      weird, there are {len(names)} different names at time point {t} instead of {n_cells}\n"
            msg += "        this suggests a name is duplicated or that names are missing"
            monitoring.to_log_and_console(msg)
            #print(msg)

        # Compare the names found in the lineage data with the reference names
        diff1 = names.difference(refnames)  # names in names but not refnames
        diff2 = refnames.difference(names)  # names in refnames but not names
        if len(diff1)>0 or len(diff2)>0:
            msg = f"{sorted(names)}, at {t}"
            monitoring.to_log_and_console(msg)
            #print(msg)
        if len(diff1) > 0:
            diff1 = sorted(list(diff1))
            msg = f"      - names {diff1} exist at time point {t} but not at {n_cells} cells stage."
            monitoring.to_log_and_console(msg)
            #print(msg)
        if len(diff2) > 0:
            diff2 = sorted(list(diff2))
            msg = f"      - names {diff2} exist at {n_cells} cells stage but not at time point {t}."
            monitoring.to_log_and_console(msg)
            #print(msg)
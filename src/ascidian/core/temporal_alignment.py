##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Mer 25 oct 2023 14:57:52 CEST
#
##############################################################
#
#
#
##############################################################

import numpy as np

##############################################################
#
#
#
##############################################################


def _find_t(cells_per_time, n):
    # find time for a given number of cells
    if n in cells_per_time.values():
        # there are times with this number of cells,
        # return middle of interval
        times = [t for t in cells_per_time if cells_per_time[t] == n]
        return (min(times) + max(times))/2.0
    # no times with this number of cells
    # interpolate from neighboring cell counts
    smaller_times = [t for t in cells_per_time if cells_per_time[t] < n]
    larger_times = [t for t in cells_per_time if cells_per_time[t] > n]
    ts = max(smaller_times)
    tl = min(larger_times)
    return ts + (tl - ts) * (n - cells_per_time[ts]) / (cells_per_time[tl] - cells_per_time[ts])


def cellcounts_temporal_alignment(ref_cells_per_time, cells_per_time, mincells=None, maxcells=None):
    first = max(min(ref_cells_per_time.values()), min(cells_per_time.values()))
    if mincells is not None:
        first = max(first, mincells)
    last = min(max(ref_cells_per_time.values()), max(cells_per_time.values()))
    if maxcells is not None:
        last = min(last, maxcells)
    ref_times = []
    times = []
    for n in range(first, last+1):
        if n not in ref_cells_per_time.values() and n not in cells_per_time.values():
            continue
        reft = _find_t(ref_cells_per_time, n)
        if reft is None:
            # print("discard " + str(n) + " cells (1)")
            continue
        flot = _find_t(cells_per_time, n)
        if flot is None:
            # print("discard " + str(n) + " cells (2)")
            continue
        ref_times += [reft]
        times += [flot]
    # Consider $\sum_i \left( a t_i +b - t'_i\right)^2$
    #
    # \begin{eqnarray*}
    # \frac{\partial}{\partial b} \sum_i \left( a t_i +b - t'_i\right)^2 = 0
    # & \Leftrightarrow &
    # \sum_i 2 \left( a t_i +b - t'_i\right) = 0 \\
    # & \Leftrightarrow & a \sum_i t_i + N b - \sum_i t'_i = 0 \\
    # & \Leftrightarrow & b = \frac{\sum_i t'_i - a \sum_i t_i}{N}
    # \end{eqnarray*}
    #
    # \begin{eqnarray*}
    # \frac{\partial}{\partial a} \sum_i \left( a t_i +b - t'_i\right)^2 = 0
    # & \Leftrightarrow &
    # \sum_i 2 \left( a t_i +b - t'_i\right) t_i = 0 \\
    # & \Leftrightarrow &
    # a \sum_i (t_i)^2 + b \sum_i t_i - \sum_i t_i t'_i = 0 \\
    # & \Leftrightarrow &
    # a \sum_i (t_i)^2 +
    # \frac{\left( \sum_i t_i \right) \left( \sum_i t'_i \right)}{N}
    # - a \frac{\left( \sum_i t_i \right)^2}{N} - \sum_i t_i t'_i = 0 \\
    # & \Leftrightarrow &
    # a \left( \sum_i (t_i)^2 - \frac{\left( \sum_i t_i \right)^2}{N} \right)
    # = \sum_i t_i t'_i - \frac{\left( \sum_i t_i \right) \left( \sum_i t'_i \right)}{N}
    # \end{eqnarray*}
    num = sum(np.multiply(times, ref_times)) - sum(times) * sum(ref_times) / len(times)
    den = sum(np.multiply(times, times)) - sum(times) * sum(times) / len(times)
    a = num/den
    b = (sum(ref_times) - a * sum(times)) / len(times)
    return a, b


def temporal_alignment(ref_lineage, lineage, mincells=None, maxcells=None, ref_time_digits_for_cell_id=4,
                       time_digits_for_cell_id=4):
    """

    Parameters
    ----------
    ref_lineage: reference lineage
    lineage: lineage to be aligned with the reference lineage
    mincells:
    maxcells:
    ref_time_digits_for_cell_id: number of digits for the cell number in the unique cell identifier
    time_digits_for_cell_id: number of digits for the cell number in the unique cell identifier

    Returns
    -------
    a, b: coefficients of the linear warping. The number of cells at time 't' of the lineage is
        made comparable to the number of cells at time 'a * t + b' of the reference lineage.

    """
    ref_div = 10 ** ref_time_digits_for_cell_id
    div = 10 ** time_digits_for_cell_id

    cells = list(set(ref_lineage.keys()).union(set([v for values in list(ref_lineage.values()) for v in values])))
    cells = sorted(cells)
    ref_cells_per_time = {}
    for c in cells:
        t = int(c) // ref_div
        ref_cells_per_time[t] = ref_cells_per_time.get(t, 0) + 1

    cells = list(set(lineage.keys()).union(set([v for values in list(lineage.values()) for v in values])))
    cells = sorted(cells)
    cells_per_time = {}
    for c in cells:
        t = int(c) // div
        cells_per_time[t] = cells_per_time.get(t, 0) + 1

    return cellcounts_temporal_alignment(ref_cells_per_time, cells_per_time, mincells=mincells, maxcells=maxcells)

def count_cells_per_time(ref_lineage, time_digits_for_cell_id=4, a=1, b=0):
    """
    Count the number of cells at each time point in the lineage of a reference embryo.

    Parameters:
        ref_lineage (dict): Lineage of the reference embryo.
        time_digits_for_cell_id (int): Number of digits for the cell number in the unique cell identifier.
        a,b (int, int) : coefficients to adjust time

    Returns:
        dict: A dictionary where keys are time points and values are the corresponding cell counts.
    """
    # Initialize an empty dictionary to store cell counts per time
    cells_per_time = {}

    # Define the divisor based on the number of digits for the time in the cell identifier
    div = 10 ** time_digits_for_cell_id

    # Iterate through the lineage
    for cell_id, descendants in ref_lineage.items():
        # Extract the time information from the cell identifier
        time_point = int(cell_id) // div
        time_point = time_point*a + b
        # Increment the cell count for the corresponding time point
        cells_per_time[time_point] = cells_per_time.get(time_point, 0) + 1

    return cells_per_time


def get_embryo_times(pm):
    """
    Get the times of the embryos based on their IDs.

    Parameters:
        pm (dict): Dictionary containing the embryo IDs as keys.

    Returns:
        list: Sorted list of unique embryo times.
    """
    ids = [int(id / 10000) for id in pm.keys()]
    times = sorted(list(set(ids)))
    return times
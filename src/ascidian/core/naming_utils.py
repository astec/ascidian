##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Jeu 27 jui 2024 18:41:25 CEST
#
##############################################################
#
#
#
##############################################################

import astec.utils.common as common

monitoring = common.Monitoring()


##############################################################
#
#
#
##############################################################


def get_named_timepoints(prop, key_name='cell_name', time_digits_for_cell_id=4):
    if key_name not in prop:
        return []
    cells = set(list(prop[key_name].keys()))
    div = 10 ** time_digits_for_cell_id
    times = list(set([int(c) // div for c in cells]))
    return times

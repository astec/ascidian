##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Ven 19 jul 2024 12:30:33 CEST
#
##############################################################
#
#
#
##############################################################

import math
import copy
import multiprocessing
from collections import ChainMap

import numpy as np
import scipy.optimize as optimize

import astec.utils.common as common

import ascidian.core.icp as icp
import ascidian.components.embryo as cembryo

monitoring = common.Monitoring()

############################################################
#
#
#
############################################################


def _cell_signature_sortedcontact(test_embryo, test_time, ref_embryo, ref_time):
    #
    # reference embryo
    # extraction of contact surfaces for the given time points
    # using get_cells() ensures that cell-ids are sorted
    #
    ref_contact = ref_embryo.rectified_cell_contact_surface
    ref_div = 10 ** ref_embryo.time_digits_for_cell_id
    ref_cells = ref_embryo.get_cells(ref_time)

    #
    # test embryo
    #
    flo_contact = test_embryo.rectified_cell_contact_surface
    flo_div = 10 ** test_embryo.time_digits_for_cell_id
    flo_cells = test_embryo.get_cells(test_time)

    #
    # scores computation
    #
    scores = {}
    for fc in flo_cells:
        t_result = {}

        #
        # get cell-id of cell neighbors and of neighboring background
        # sort contact surfaces for cell neighbors
        # compute cell surface
        #
        f_neighbors = [k for k in flo_contact[fc] if int(k) % flo_div > 1]
        f_backgrd = [k for k in flo_contact[fc] if int(k) % flo_div == 1]

        f_contact = np.array(sorted([flo_contact[fc][k] for k in f_neighbors], reverse=True))

        fbck_surface = 0
        if len(f_backgrd) == 1:
            fbck_surface = flo_contact[fc][f_backgrd[0]]
        # total surface
        f_surface = fbck_surface + np.sum(f_contact)

        for rc in ref_cells:
            #
            # get sorted contact surfaces for cell neighbors and for background
            #
            r_neighbors = [k for k in ref_contact[rc] if int(k) % ref_div > 1]
            r_backgrd = [k for k in ref_contact[rc] if int(k) % ref_div == 1]

            r_contact = np.array(sorted([ref_contact[rc][k] for k in r_neighbors], reverse=True))

            rbck_surface = 0
            if len(r_backgrd) == 1:
                rbck_surface = ref_contact[rc][r_backgrd[0]]
            # total surface
            r_surface = rbck_surface + np.sum(r_contact)

            minlength = min(len(f_contact), len(r_contact))
            score = np.abs(fbck_surface - rbck_surface)
            score += np.sum(np.abs(f_contact[:minlength] - r_contact[:minlength]))
            score += np.sum(f_contact[minlength:]) + np.sum(r_contact[minlength:])
            score /= (f_surface + r_surface)
            t_result[rc] = score

        scores[fc] = t_result

    return scores


############################################################
#
#
#
############################################################


def _split_list(li, n):
    lsublist = len(li) // int(n)
    remain = len(li) % int(n)
    res = []
    f = 0
    for i in range(n):
        if remain > 0:
            res += [li[f:f+lsublist+1]]
            f += lsublist+1
            remain -= 1
        else:
            res += [li[f:f+lsublist]]
            f += lsublist
    return res


def _proc_cell_signature_wingedcontact(parameters):

    flo_data, ref_data, angles_degree = parameters
    #
    # for both ref and flo:
    #
    # data['cells'] : list of cells at time ref_time
    # data['contact'] : dictionary of (rectified) contact surfaces (value = dictionary of contact surfaces)
    # data['neighbors'] : dictionary of inner neighbors (value = list of inner neighbors)
    # data['back_surface'] : dictionary of background surfaces (value = surface)
    # data['surface'] : dictionary of total surfaces (value = surface)
    # data['vector'] : dictionary of weighted contact vectors (value = dictionary of vectors)
    #
    # in addition for flo
    # data['transformed_vector'][k][c][d] : dictionary of transformed weighted contact vectors
    #

    scores = {}
    for fc in flo_data['cells']:
        msg = "           ... process cell #" + str(fc)
        monitoring.to_log_and_console(msg, 2)
        #
        # will build results for cell 'tc' of test embryo
        #
        t_result = {}

        #
        # total surface
        #
        f_surface = flo_data['surface'][fc]

        #
        # contact vectors for adjacent cells in the test embryo
        #
        dict_f_ids = {}
        for i, ic in enumerate(flo_data['neighbors'][fc]):
            dict_f_ids[i] = ic

        #
        # compare with cells in the reference atlas
        #
        for rc in ref_data['cells']:
            #
            # total surface
            #
            r_surface = ref_data['surface'][rc]

            #
            # vectors
            #
            dict_r_ids = {}
            for j, jc in enumerate(ref_data['neighbors'][rc]):
                dict_r_ids[j] = jc

            # we will normalize afterwards
            bestscore_fc_rc = f_surface + r_surface
            bestangle_fc_rc = None
            bestpairing_fc_rc = None

            #
            # loop on 2D rotation angles (indexed by k)
            #
            for k in flo_data['transformed_npvector']:

                #
                # we pair contact vectors
                # cost between vectors is the angle (ie arc length)
                #
                # cost = np.zeros((len(flo_data['neighbors'][fc]), len(ref_data['neighbors'][rc])))
                # for i, ic in enumerate(flo_data['neighbors'][fc]):
                #     for j, jc in enumerate(ref_data['neighbors'][rc]):
                #         cost[i, j] = np.linalg.norm(kt_vectors[fc][ic] - ref_data['vector'][rc][jc])

                #
                # flo_data['transformed_npvector'][k][fc] and ref_data['npvector'][rc]
                # are np.array of shapes [3,lf] and [3,lr], ie arrays of contact vectors
                #
                # mflo is a np.array of shape[3, lf, lr]
                #   with mflo[3,lf,:] = flo_data['transformed_npvector'][k][fc]
                # mref is a np.array of shape[3, lf, lr]
                #   with mref[3,:,lr] = ref_data['npvector'][rc]
                # mflo-mref : tensor of coordinates differences
                # (mflo-mref)*(mflo-mref) : square of coordinates differences
                # np.sum((mflo-mref)*(mflo-mref), axis=0) : sum of square of coordinates differences
                #    = matrix of square of L2 norm between vectors
                #
                mflo = np.tensordot(flo_data['transformed_npvector'][k][fc], np.ones(ref_data['n_neighbors'][rc]), 0)
                mref = np.tensordot(ref_data['npvector'][rc], np.ones(flo_data['n_neighbors'][fc]), 0).transpose(0,2,1)
                diff = mflo-mref
                cost =  np.sqrt(np.sum(diff*diff, axis=0))

                #
                # pair (initially) transformed t_vectors and r_vector by linear_sum_assignment()
                # row_ind = indexes from the t_neighbors
                # col_ind = indexes from the r_neighbors
                #
                row_ind, col_ind = optimize.linear_sum_assignment(cost)

                #
                # score: paired surfaces
                #
                score = cost[row_ind, col_ind].sum()

                #
                #
                #
                pairing = []
                for i in range(min(len(row_ind), len(col_ind))):
                    pairing += [(row_ind[i], col_ind[i])]
                #
                # add background
                #
                score += np.abs(flo_data['back_surface'][fc] - ref_data['back_surface'][rc])

                #
                # get unpaired contact
                #

                # paired_f_ids = []
                # paired_r_ids = []
                # for i in range(min(len(row_ind), len(col_ind))):
                #     ic = dict_f_ids[row_ind[i]]
                #     paired_f_ids += [ic]
                #     jc = dict_r_ids[col_ind[i]]
                #     paired_r_ids += [jc]
                # #
                # # score: non-paired surfaces
                # #
                # for ic in set(flo_data['neighbors'][fc]).difference(paired_f_ids):
                #     score += flo_data['contact'][fc][ic]
                # for jc in set(ref_data['neighbors'][rc]).difference(paired_r_ids):
                #     score += ref_data['contact'][rc][jc]

                #
                # unpaired surfaces: substract paired surfaces from the total surface
                #
                unpaired_flo_surface = flo_data['surface'][fc] - flo_data['back_surface'][fc]
                unpaired_ref_surface = ref_data['surface'][rc] - ref_data['back_surface'][rc]
                for i in range(min(len(row_ind), len(col_ind))):
                    unpaired_flo_surface -= flo_data['npcontact'][fc][row_ind[i]]
                    unpaired_ref_surface -= ref_data['npcontact'][rc][col_ind[i]]
                score += unpaired_flo_surface + unpaired_ref_surface

                #
                # is it better?
                #
                if bestscore_fc_rc > score:
                    bestscore_fc_rc = score
                    bestangle_fc_rc = angles_degree[k]
                    bestpairing_fc_rc = pairing

            #
            # end of loop on 2D rotations
            #
            t_result[rc] = (bestscore_fc_rc / (f_surface + r_surface), bestangle_fc_rc, pairing)

        scores[fc] = t_result

    return scores


def _precompute_winged_contact(embryo, timepoint):
    ret = {}

    div = 10 ** embryo.time_digits_for_cell_id

    ret['cells'] = embryo.get_cells(timepoint)
    ret['contact'] = embryo.rectified_cell_contact_surface
    embryo.compute_cell_contact_vector(times=timepoint, normalize=True, force=False)
    contact_vector = embryo.cell_contact_vector

    ret['neighbors'] = {}
    ret['n_neighbors'] = {}
    ret['back_surface'] = {}
    ret['surface'] = {}
    ret['npvector'] = {}
    ret['npcontact'] = {}

    for c in ret['cells']:
        # inner neighbors
        ret['neighbors'][c] = [k for k in ret['contact'][c] if int(k) % div > 1]
        ret['n_neighbors'][c] = len(ret['neighbors'][c])
        # outer neighbor (ie background)
        backgrd = [k for k in ret['contact'][c] if int(k) % div == 1]
        # background contact surface
        if len(backgrd) == 1:
            ret['back_surface'][c] = ret['contact'][c][backgrd[0]]
        else:
            ret['back_surface'][c] = 0.0
        # total surface
        ret['surface'][c] = ret['back_surface'][c] + np.sum(np.array(ret['contact'][c][k]) for k in ret['neighbors'][c])
        # contact surface * vector
        ret['npvector'][c] = np.zeros((3, ret['n_neighbors'][c]))
        ret['npcontact'][c] = np.zeros(ret['n_neighbors'][c])
        for i, d in enumerate(ret['neighbors'][c]):
            ret['npvector'][c][:, i] = ret['contact'][c][d] * contact_vector[c][d]
            ret['npcontact'][c][i] = ret['contact'][c][d]
    return ret


def _cell_signature_wingedcontact(test_embryo, test_time, ref_embryo, ref_time, id_symvector=0,
                                  z_rotation_angle_increment=15, processors=1, cheat=False):
    proc = "_cell_signature_wingedcontact"

    #
    # precompute
    # ref_data['cells'] : dictionary of list of cells at time ref_time
    # ref_data['contact'] : dictionary of (rectified) contact surfaces (value = dictionary of contact surfaces)
    # ref_data['neighbors'] : dictionary of inner neighbors (value = list of inner neighbors)
    # ref_data['n_neighbors'] : dictionary of #inner neighbors (value = number)
    # ref_data['back_surface'] : dictionary of background surfaces (value = surface)
    # ref_data['surface'] : dictionary of total surfaces (value = surface)
    # ref_data['npvector'] : weighted contact vectors (value = numpy array with shape = (3, #vectors))
    # ref_data['npcontact'] : dictionary of contact surfaces (value = numpy array of length #vectors)
    #
    ref_data = _precompute_winged_contact(ref_embryo, ref_time)

    #
    # get symmetry axis orientation of reference atlas
    #
    ref_symaxis = ref_embryo.get_symmetry_axis_from_names(ref_time)

    #
    # test embryo
    #
    flo_data = _precompute_winged_contact(test_embryo, test_time)

    #
    # get symmetry axis orientations of test atlas
    # vectors: numpy array of shape (N, 3), N being the number of candidates
    #
    # work will be done with vector[id_symvector]
    #
    vectors = test_embryo.get_direction_distribution_candidates(test_time)
    local_id_symvector = id_symvector
    if id_symvector < 0 or id_symvector >= vectors.shape[0]:
        local_id_symvector = 0
        msg = "#symmetry vectors was " + str(vectors.shape[0]) + " and requested candidate was " + str(id_symvector)
        msg += "\n\t switch to first candidate"
        monitoring.to_log_and_console(str(proc) + ": " + msg)
    test_symaxis = copy.deepcopy(vectors[local_id_symvector])

    #
    # according the symmetry axis have been computed correctly, the two first candidates
    # are aligned with the "right" symmetry axis vector, ie Left-to-right unit vector
    # for computation cost purpose (eg when computing data for figures), change the
    # orientation to get the right vector direction
    # NOTE: this is not the same vector
    #
    if cheat is True:
        test_gt_symaxis = test_embryo.get_symmetry_axis_from_names(test_time)
        # print("CHEAT: test_symaxis = " + str(test_symaxis))
        # print("       test_gt_symaxis = " + str(test_gt_symaxis))
        if np.dot(test_gt_symaxis, test_symaxis) < 0.0:
            test_symaxis *= -1.0
            # print("       NEW test_symaxis = " + str(test_symaxis))

    #
    # rotation of math.acos(test_symaxis.ref_symaxis) wrt vector test_symaxis x ref_symaxis
    # that puts test_symaxis along ref_symaxis
    #
    sp = np.dot(test_symaxis, ref_symaxis)
    vrot = np.cross(test_symaxis, ref_symaxis)
    vrot *= math.acos(sp) / np.linalg.norm(vrot)
    mrot = icp.rotation_matrix_from_rotation_vector(vrot)

    #
    # set of initial rotations =
    # set of 2D rotations along ref_symaxis combined with
    #     the 3D rotation that align test_symaxis onto ref_symaxis
    #
    # pre-compute weighted transformed contact vectors for each angle
    #
    angles_degree = np.linspace(0, 360, num=round(360.0/z_rotation_angle_increment), endpoint=False)
    angles_radian = angles_degree * np.pi / 180

    flo_data['transformed_npvector'] = {}
    for k, a in enumerate(angles_radian):
        # compute initial transformation
        # scaling matrices can be anisotropic
        init = np.matmul(icp.rotation_matrix_from_rotation_vector(a * ref_symaxis), mrot)
        flo_data['transformed_npvector'][k] = {}
        for c in flo_data['cells']:
            flo_data['transformed_npvector'][k][c] = np.matmul(init, flo_data['npvector'][c])

    # if processors > 1:
    #     print("multiprocessing processors = "+str(processors))
    #     mapping = []
    #     pool = multiprocessing.Pool(processes=processors)
    #     split_flo_cells = _split_list(flo_cells, processors)
    #     for i in split_flo_cells:
    #         print("i = " + str(i))
    #         mapping.append(((i, flo_contact, flo_div), ref_parameters, others_parameters))
    #     print("preparation done")
    #     outputs = pool.map(_proc_cell_signature_wingedcontact, mapping)
    #     pool.close()
    #     pool.terminate()
    #     # scores = {}
    #     # for s in outputs:
    #     #     scores.update(s)
    #     scores = dict(ChainMap(*outputs))
    # else:
    scores = _proc_cell_signature_wingedcontact((flo_data, ref_data, angles_degree))

    return scores


############################################################
#
#
#
############################################################

def cell_signature(test_embryo, test_time, ref_embryo, ref_time, cell_similarity='winged-contact',
                   id_symvector=0, z_rotation_angle_increment=15, processors=1, cheat=False):
    proc = "cell_signature"

    if isinstance(test_embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryos' variable: " + str(type(test_embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        return {}

    if isinstance(ref_embryo, cembryo.Embryo) is False:
        msg = ": unexpected type for 'embryos' variable: " + str(type(ref_embryo))
        monitoring.to_log_and_console(str(proc) + msg)
        return {}

    if cell_similarity == 'sorted-contact':
        return _cell_signature_sortedcontact(test_embryo, test_time, ref_embryo, ref_time)
    elif cell_similarity == 'winged-contact':
        return _cell_signature_wingedcontact(test_embryo, test_time, ref_embryo, ref_time,
                                             id_symvector=id_symvector,
                                             z_rotation_angle_increment=z_rotation_angle_increment,
                                             processors=processors, cheat=cheat)
    else:
        monitoring.to_log_and_console(str(proc) + ": unexpected value for 'parameters' variable: "
                                      + str(cell_similarity))

    #
    # return a dictionary of dictionary: dict[cell-id-test][cell-id-ref] = signature
    # - cell-id-test: cell-ids of cells of test_embryo at time test_time
    # - cell-id-ref: cell-ids of cells of ref_embryo at time ref_time
    #
    # 'sorted-contact': signature is only a value, ie the score
    # 'winged-contact': signature is a tuple (score, angle)
    return {}

##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2023
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Sam 17 jui 2023 13:23:58 CEST
#
##############################################################
#
#
#
##############################################################

import os
import sys

import astec.utils.common as common

monitoring = common.Monitoring()


###########################################################
#
#
#
###########################################################

class SymmetryParameters(common.PrefixedParameter):

    ############################################################
    #
    # initialisation
    #
    ############################################################

    def __init__(self, prefix=""):

        common.PrefixedParameter.__init__(self, prefix=prefix)

        if "doc" not in self.__dict__:
            self.doc = {}

        #
        #
        #
        doc = "\t Samples (vectors) that will be used to computed the direction distribution\n"
        doc += "\t via kernel density estimation.\n"
        doc += "\t In all cases, the used vectors are unit vectors derived from the vector\n"
        doc += "\t joining the barycenters of a couple of cells.\n"
        doc += "\t - 'contact-surface': cell couples are adjacent cells. Samples (ie vectors)\n"
        doc += "\t   are weighted by the contact surfaces of the adjacent cells.\n"
        doc += "\t   It comes to mimic G. Michelin approach to estimate the direction distribution.\n"
        doc += "\t   This is not the most efficient method.\n"
        doc += "\t - 'symmetric-cell': for each cell, a list of potential symmetric cells is \n"
        doc += "\t   build and sorted by decreasing likelihood. A neighborhood-based similarity\n"
        doc += "\t   (in [0,1]) is computed to estimate the likelihood. \n"
        doc += "\t   The first 'symmetric_cell_number' (ie the first 'symmetric_cell_number'\n"
        doc += "\t   cells that are candidate to be the symmetrical cells), hence there can be more \n"
        doc += "\t   than one sample per cell. \n"
        doc += "\t   Samples are weighted by the total cell surface times (1.0 - similarity)\n"
        doc += "\t The most efficient combination is the one set by default parameters.\n"
        doc += "\t It is then advised not to change them.\n"
        self.doc['direction_sample'] = doc
        self.direction_sample = 'symmetric-cell'

        doc = "\t Number of cells used as candidates to be the symmetrical cell\n"
        doc += "\t 'symmetric_cell_number = 1' means there is only one candidate\n"
        doc += "\t per cell.\n"
        self.doc['symmetric_cell_number'] = doc
        self.symmetric_cell_number = 1

        doc = "\t Method to compute the similarity between a cell and an other cell\n"
        doc += "\t candidate to be the symmetrical of the first one.\n"
        doc += "\t For the two cells to be compared, neighbors are paired and a \n"
        doc += "\t difference measure is computed for every neighbor pair and summed up.\n"
        doc += "\t The sum is then divided by the sum of the two cell surfaces, which\n"
        doc += "\t ensures that the computed similarity is in [0, 1].\n"
        doc += "\t - 'sorted-contact': the neighbors of the two cells to be compared are\n"
        doc += "\t   sorted in decreasing order (based on contact surfaces) and then paired.\n"
        doc += "\t   The difference measure is the difference of contact surfaces.\n"
        doc += "\t   This is exactly the distance used to name cells except that the pairing \n"
        doc += "\t   is done based on contact surface values except of cell names.\n"
        doc += "\t - 'winged-contact': (unit) vectors joining a cell to its neighbors are used here.\n"
        doc += "\t   Let c and d be the cells to be compared, c(i) be the contact surface of a \n"
        doc += "\t   neighbor i of c and v(i) the unit vector joining the barycenter of c to the\n"
        doc += "\t   one of i.\n"
        doc += "\t   Let dc be the unit vector joining the barycenter of c and d.\n"
        doc += "\t   Vectors v(j) of neighbors j of d are 'symmetrised' by \n"
        doc += "\t   v'(j) = v(j) - 2 (v(j).dc) dc\n"
        doc += "\t   The difference measure is |c(i)v(i) - c(j)v'(j)| which is the contact surface\n"
        doc += "\t   difference if the two vectors v(i) and v'(j) are equal.\n"
        doc += "\t   Differences are computed for every couple (i,j) and optimal pairing is done\n"
        doc += "\t   through scipy.optimize.linear_sum_assignment()\n"
        self.doc['symmetrical_cell_similarity'] = doc
        self.symmetrical_cell_similarity = 'winged-contact'

        doc = "\t Sphere radius to build the support of the direction distribution.\n"
        doc += "\t The direction distribution is computed onto a discrete sphere, ie a sphere made of voxels.\n"
        doc += "\t The larger the sphere, the more accurate the mode estimation, but the longer the\n"
        doc += "\t computation.\n"
        doc += "\t radius = 10:   978 vectors, angle between neighboring vectors in [4.40, 10.58] degrees\n"
        doc += "\t radius = 15:  2262 vectors, angle between neighboring vectors in [2.98, 6.93] degrees\n"
        doc += "\t radius = 20:  4026 vectors, angle between neighboring vectors in [2.25, 5.16] degrees\n"
        doc += "\t radius = 25:  6366 vectors, angle between neighboring vectors in [1.73, 4.01] degrees\n"
        doc += "\t radius = 30:  9194 vectors, angle between neighboring vectors in [1.46, 3.40] degrees\n"
        doc += "\t radius = 35: 12542 vectors, angle between neighboring vectors in [1.26, 2.90] degrees\n"
        doc += "\t radius = 40: 16418 vectors, angle between neighboring vectors in [1.08, 2.53] degrees\n"
        self.doc['distribution_sphere_radius'] = doc
        self.distribution_sphere_radius = 20

        doc = "\t Sigma (standard deviation) to build the direction distribution (in radian).\n"
        doc += "\t The distribution is built through a Gaussian kernel density estimation.\n"
        self.doc['distribution_kernel_sigma'] = doc
        self.distribution_kernel_sigma = 0.15

        doc = "\t Threshold on the distribution value. Only maxima above this threshold are\n"
        doc += "\t kept. Recall that the distribution values are normalize so that the maximum\n"
        doc += "\t is 1.\n"
        self.doc['direction_maxima_threshold'] = doc
        self.direction_maxima_threshold = 0.5

        doc = "\t Number of distribution maxima to be retained as candidates.\n"
        doc += "\t 'None' or negative number means all of them. Since the distribution\n"
        doc += "\t is computed onto a sphere, both the surface normal and its opposite\n"
        doc += "\t contribute to the distribution estimation. It comes out that each symmetry\n"
        doc += "\t direction is represented by two (opposite) vectors, meaning that this\n"
        doc += "\t parameter has to be even. \n"
        self.doc['direction_maxima_number'] = doc
        self.direction_maxima_number = 12

        doc = "\t Number of processors for parallelization (when required).\n"
        self.doc['processors'] = doc
        self.processors = 8

    ############################################################
    #
    # print / write
    #
    ############################################################

    def print_parameters(self):
        print("")
        print('#')
        print('# SymmetryParameters')
        print('#')
        print("")

        common.PrefixedParameter.print_parameters(self)

        self.varprint('direction_sample', self.direction_sample, self.doc.get('direction_sample', None))
        self.varprint('symmetric_cell_number', self.symmetric_cell_number, self.doc.get('symmetric_cell_number', None))
        self.varprint('symmetrical_cell_similarity', self.symmetrical_cell_similarity,
                      self.doc.get('symmetrical_cell_similarity', None))
        self.varprint('distribution_sphere_radius', self.distribution_sphere_radius,
                      self.doc.get('distribution_sphere_radius', None))
        self.varprint('distribution_kernel_sigma', self.distribution_kernel_sigma,
                      self.doc.get('distribution_kernel_sigma', None))
        self.varprint('direction_maxima_threshold', self.direction_maxima_threshold,
                      self.doc.get('direction_maxima_threshold', None))
        self.varprint('direction_maxima_number', self.direction_maxima_number,
                      self.doc.get('direction_maxima_number', None))
        self.varprint('processors', self.processors, self.doc.get('processors', None))
        print("")

    def write_parameters_in_file(self, logfile):
        logfile.write("\n")
        logfile.write("# \n")
        logfile.write("# SymmetryParameters\n")
        logfile.write("# \n")
        logfile.write("\n")

        common.PrefixedParameter.write_parameters_in_file(self, logfile)

        self.varwrite(logfile, 'direction_sample', self.direction_sample, self.doc.get('direction_sample', None))
        self.varwrite(logfile, 'symmetric_cell_number', self.symmetric_cell_number,
                      self.doc.get('symmetric_cell_number', None))
        self.varwrite(logfile, 'symmetrical_cell_similarity', self.symmetrical_cell_similarity,
                      self.doc.get('symmetrical_cell_similarity', None))
        self.varwrite(logfile, 'distribution_sphere_radius', self.distribution_sphere_radius,
                      self.doc.get('distribution_sphere_radius', None))
        self.varwrite(logfile, 'distribution_kernel_sigma', self.distribution_kernel_sigma,
                      self.doc.get('distribution_kernel_sigma', None))
        self.varwrite(logfile, 'direction_maxima_threshold', self.direction_maxima_threshold,
                      self.doc.get('direction_maxima_threshold', None))
        self.varwrite(logfile, 'direction_maxima_number', self.direction_maxima_number,
                      self.doc.get('direction_maxima_number', None))
        self.varwrite(logfile, 'processors', self.processors, self.doc.get('processors', None))
        logfile.write("\n")
        return

    def write_parameters(self, log_file_name):
        with open(log_file_name, 'a') as logfile:
            self.write_parameters_in_file(logfile)
        return

    ############################################################
    #
    # update
    #
    ############################################################

    def update_from_parameters(self, parameters):

        self.direction_sample = self.read_parameter(parameters, 'direction_sample', self.direction_sample)
        self.symmetric_cell_number = self.read_parameter(parameters, 'symmetric_cell_number',
                                                         self.symmetric_cell_number)
        self.symmetrical_cell_similarity = self.read_parameter(parameters, 'symmetrical_cell_similarity',
                                                               self.symmetrical_cell_similarity)
        self.symmetrical_cell_similarity = self.read_parameter(parameters, 'cell_similarity',
                                                               self.symmetrical_cell_similarity)
        self.distribution_sphere_radius = self.read_parameter(parameters, 'sphere_radius',
                                                              self.distribution_sphere_radius)
        self.distribution_sphere_radius = self.read_parameter(parameters, 'distribution_sphere_radius',
                                                              self.distribution_sphere_radius)
        self.distribution_kernel_sigma = self.read_parameter(parameters, 'sigma', self.distribution_kernel_sigma)
        self.distribution_kernel_sigma = self.read_parameter(parameters, 'distribution_kernel_sigma',
                                                             self.distribution_kernel_sigma)
        self.direction_maxima_threshold = self.read_parameter(parameters, 'maxima_threshold',
                                                              self.direction_maxima_threshold)
        self.direction_maxima_threshold = self.read_parameter(parameters, 'direction_maxima_threshold',
                                                              self.direction_maxima_threshold)
        self.direction_maxima_number = self.read_parameter(parameters, 'maxima_number', self.direction_maxima_number)
        self.direction_maxima_number = self.read_parameter(parameters, 'direction_maxima_number',
                                                           self.direction_maxima_number)
        self.processors = self.read_parameter(parameters, 'processors', self.processors)

    def update_from_parameter_file(self, parameter_file):
        if parameter_file is None:
            return
        if not os.path.isfile(parameter_file):
            print("Error: '" + parameter_file + "' is not a valid file. Exiting.")
            sys.exit(1)

        parameters = common.load_source(parameter_file)
        self.update_from_parameters(parameters)

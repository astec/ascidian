##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Sam 17 jui 2023 13:23:58 CEST
#
##############################################################
#
#
#
##############################################################

import sys
import copy

import multiprocessing

import numpy as np

import scipy.optimize as optimize

import astec.utils.common as common

import ascidian.symmetry.parameters as sparameters
import ascidian.core.vector_distribution as vdist

monitoring = common.Monitoring()


"""
Compute left/right symmetry direction in ascidian embryos
"""


###########################################################
#
#
#
###########################################################

def _split_list(li, n):
    lsublist = len(li) // int(n)
    remain = len(li) % int(n)
    res = []
    f = 0
    for i in range(n):
        if remain > 0:
            res += [li[f:f+lsublist+1]]
            f += lsublist+1
            remain -= 1
        else:
            res += [li[f:f+lsublist]]
            f += lsublist
    return res


###########################################################
#
# symmetry distribution: samples from contact surfaces
# it mimics G. Michelin method
#
###########################################################


def _samples_from_contactsurfaces(cell_contact_surface, barycenters, timepoint, time_digits_for_cell_id=4):

    #
    # cells to be processed
    #
    div_cell = 10 ** time_digits_for_cell_id
    cells = [c for c in cell_contact_surface if (int(c) // div_cell == timepoint) and int(c) % div_cell != 1]

    vectors = []
    weights = []
    v = np.zeros(3)

    # cells do not include the background (label = 10 ** time_digits_for_cell_id + 1)
    # since we only consider d > c, d can not be the background either
    for c in cells:
        for d in cell_contact_surface[c]:
            if d <= c:
                continue
            #
            # approximation of the normal vector of the contact surface between c and d
            #
            v[0] = barycenters[c][0] - barycenters[d][0]
            v[1] = barycenters[c][1] - barycenters[d][1]
            v[2] = barycenters[c][2] - barycenters[d][2]
            v /= np.linalg.norm(v)

            vectors += [copy.deepcopy(v)]
            weights += [cell_contact_surface[c][d]]

    return np.array(vectors), np.array(weights)


###########################################################
#
# symmetrical cells estimation from sorted/winged contact surfaces
#
###########################################################


def _symcells_from_sortedcontact(cell_contact_surface, timepoint, time_digits_for_cell_id=4):
    """
    Compute symmetrical cells based on a score where contact surfaces are paired wrt their order.
    score(c_i, c_j) = (sum_k |s_{i,k} - s_{j,l}| + |s_{i,b} - s_{j,b}|) / (S(c_i) + S(c_j))
    s_{i,b} is the contact surface of c_i with the background (so background is paired with
    the background), contact surfaces s_{i,k} (resp. s_{j,l}) are sorted in decreasing order
    so the largest contact of c_i (in the embryo) is paired to the largest contact of c_j
    (in the embryo), and so on.
    Division by the sum of cell surfaces ensures that the score is in [0,1].

    Parameters
    ----------
    cell_contact_surface: dictionary of cell contact surface
    timepoint: time point to be processed
    time_digits_for_cell_id:

    Returns
    -------
    symcells: dictionary indexed by cell id. Values are the cell id list of potential symmetrical cells
      (ordered by decreasing score): the first cell is then the most likely to be the symmetrical cell.
    surfaces: dictionary (indexed by cell id) of cell surfaces

    """

    #
    # get cells from time = timepoint
    #
    div_cell = 10 ** time_digits_for_cell_id
    cells = [c for c in cell_contact_surface if (int(c) // div_cell == timepoint) and int(c) % div_cell != 1]

    symcells = {}
    surfaces = {}
    for c in cells:
        cn_contact = [k for k in cell_contact_surface[c] if int(k) % div_cell > 1]
        #
        # get background to pair background with background
        #
        cn_backgrd = [k for k in cell_contact_surface[c] if int(k) % div_cell == 1]
        c_backgrd = 0
        if len(cn_backgrd) == 1:
            c_backgrd = cell_contact_surface[c][cn_backgrd[0]]
        #
        # sort cell contact surfaces
        #
        ccontact = np.array(sorted([cell_contact_surface[c][k] for k in cn_contact], reverse=True))
        csurface = c_backgrd + np.sum(ccontact)
        surfaces[c] = csurface
        symc = []
        for d in cells:
            if d == c:
                continue
            dn_contact = [k for k in cell_contact_surface[d] if int(k) % div_cell > 1]
            dn_backgrd = [k for k in cell_contact_surface[d] if int(k) % div_cell == 1]
            d_backgrd = 0
            if len(dn_backgrd) == 1:
                d_backgrd = cell_contact_surface[d][dn_backgrd[0]]
            dcontact = np.array(sorted([cell_contact_surface[d][k] for k in dn_contact], reverse=True))
            dsurface = d_backgrd + np.sum(dcontact)

            minlength = min(len(ccontact), len(dcontact))
            score = np.abs(c_backgrd - d_backgrd)
            score += np.sum(np.abs(ccontact[:minlength] - dcontact[:minlength]))
            score += np.sum(ccontact[minlength:]) + np.sum(dcontact[minlength:])
            score /= (csurface + dsurface)
            symc += [(d, score)]
        symc = sorted(symc, key=lambda x: x[1])
        symcells[c] = symc
    return symcells, surfaces


def _proc_symcells_from_wingedcontact(parameters):
    """
    Compute symmetrical cells based on a score where contact surfaces are paired wrt their order.
    score(c_i, c_j) = (sum_k ||s_{i,k}n_{i,k} - s_{j,l}n'_{j,l}|| + |s_{i,b} - s_{j,b}|) / (S(c_i) + S(c_j))
    s_{i,b} is the contact surface of c_i with the background (so background is paired with
    the background), pairs of (contact surfaces, vector towards the neighboring cell),
    (s_{i,k}, n_{i,k}) are paired to (s_{j,l}, n'_{j,l}) by solving the linear sum assignment problem
    (see https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.linear_sum_assignment.html)
    (see https://en.wikipedia.org/wiki/Assignment_problem)
    n'_{j,l} is the symmetrical vector of n_{j,l} wrt plane of normal vector n_{i,j}
    Division by the sum of cell surfaces ensures that the score is in [0,1].

    Returns
    -------
    symcells: dictionary indexed by cell id. Values are the cell id list of potential symmetrical cells
      (ordered by decreasing score): the first cell is then the most likely to be the symmetrical cell.
    surfaces: dictionary (indexed by cell id) of cell surfaces

    """

    testcells, emb_data, barycenters = parameters

    symcells = {}
    surfaces = {}

    v = np.zeros(3)
    dc = np.zeros(3)

    allcells = emb_data['cells']

    for c in testcells:
        surfaces[c] = emb_data['surface'][c]

        #
        #
        #
        symc = []
        for d in allcells:
            if d == c:
                continue

            #
            # vectors issued from cell #d, symmetrized wrt dc
            # component colinear to dc is (dc.v) dc
            # - removing it once comes to get a vector orthogonal to dc
            # - removing it twice comes to have the opposite colinear component
            #
            # vector joining the barycenters
            dc = (barycenters[c] - barycenters[d]).reshape(3, 1)
            dc /= np.linalg.norm(dc)
            # matrix of vectors: [:,i] is the vector joining the barycenters
            vecdc = np.dot(dc, np.ones((1, emb_data['n_neighbors'][d])))
            # vector of dot products: [:,i] is the vector joining the barycenters
            vecdot = np.sum(emb_data['npvector'][d] * vecdc, axis=0)
            # matrix of dot products: [:,i] is the vector joining the barycenters
            dotdc = np.dot(np.ones((3, 1)), vecdot.reshape((1, emb_data['n_neighbors'][d])))
            # symmetrical vectors
            sym_npvector = emb_data['npvector'][d] - 2 * dotdc * vecdc

            #
            # compute similarity between cvector(i) and dvector(j)
            # it is |s(i)v(i) - s(j)v(j)|
            # v(i) and v(j) are unit vectors, then if they are identical
            # it comes to compute the difference of contact surfaces
            # see also cell_signature.py
            #
            mflo = np.tensordot(emb_data['npvector'][c], np.ones(emb_data['n_neighbors'][d]), 0)
            mref = np.tensordot(sym_npvector, np.ones(emb_data['n_neighbors'][c]), 0).transpose(0, 2, 1)
            diff = mflo - mref
            cost = np.sqrt(np.sum(diff * diff, axis=0))

            #
            # row_ind = indexes from the cvector
            # col_ind = indexes from the dvector
            #
            row_ind, col_ind = optimize.linear_sum_assignment(cost)
            score = cost[row_ind, col_ind].sum()

            #
            # add missing contact surfaces
            #
            score += np.abs(emb_data['back_surface'][c] - emb_data['back_surface'][d])
            #
            # unpaired surfaces: substract paired surfaces from the total surface
            #
            unpaired_c_surface = emb_data['surface'][c] - emb_data['back_surface'][c]
            unpaired_d_surface = emb_data['surface'][d] - emb_data['back_surface'][d]
            for i in range(min(len(row_ind), len(col_ind))):
                unpaired_c_surface -= emb_data['npcontact'][c][row_ind[i]]
                unpaired_d_surface -= emb_data['npcontact'][d][col_ind[i]]
            score += unpaired_c_surface + unpaired_d_surface
            score /= (emb_data['surface'][c] + emb_data['surface'][d])

            symc += [(d, score)]

        symc = sorted(symc, key=lambda x: x[1])
        symcells[c] = symc
    return symcells, surfaces


def _precompute_winged_contact(cell_contact_surface, barycenters, timepoint, time_digits_for_cell_id=4):
    ret = {}

    div = 10 ** time_digits_for_cell_id
    ret['cells'] = [c for c in cell_contact_surface if (int(c) // div == timepoint) and int(c) % div != 1]
    ret['contact'] = cell_contact_surface

    ret['neighbors'] = {}
    ret['n_neighbors'] = {}
    ret['back_surface'] = {}
    ret['surface'] = {}
    ret['vector'] = {}
    ret['npvector'] = {}
    ret['npcontact'] = {}

    v = np.zeros(3)

    for c in ret['cells']:
        # inner neighbors
        ret['neighbors'][c] = [k for k in ret['contact'][c] if int(k) % div > 1]
        ret['n_neighbors'][c] = len(ret['neighbors'][c])
        # outer neighbor (ie background)
        backgrd = [k for k in ret['contact'][c] if int(k) % div == 1]
        # background contact surface
        if len(backgrd) == 1:
            ret['back_surface'][c] = ret['contact'][c][backgrd[0]]
        else:
            ret['back_surface'][c] = 0.0
        # total surface
        ret['surface'][c] = ret['back_surface'][c] + np.sum(np.array(ret['contact'][c][k]) for k in ret['neighbors'][c])
        # contact surface * vector
        ret['npvector'][c] = np.zeros((3, ret['n_neighbors'][c]))
        ret['npcontact'][c] = np.zeros(ret['n_neighbors'][c])
        for i, d in enumerate(ret['neighbors'][c]):
            v = barycenters[d] - barycenters[c]
            v /= np.linalg.norm(v)
            ret['npvector'][c][:, i] = ret['contact'][c][d] * v
            ret['npcontact'][c][i] = ret['contact'][c][d]
    return ret


def _symcells_from_wingedcontact(cell_contact_surface, barycenters, timepoint, time_digits_for_cell_id=4, processors=8):
    """
    Compute symmetrical cells based on a score where contact surfaces are paired wrt their order.
    score(c_i, c_j) = (sum_k ||s_{i,k}n_{i,k} - s_{j,l}n'_{j,l}|| + |s_{i,b} - s_{j,b}|) / (S(c_i) + S(c_j))
    s_{i,b} is the contact surface of c_i with the background (so background is paired with
    the background), pairs of (contact surfaces, vector towards the neighboring cell),
    (s_{i,k}, n_{i,k}) are paired to (s_{j,l}, n'_{j,l}) by solving the linear sum assignment problem
    (see https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.linear_sum_assignment.html)
    (see https://en.wikipedia.org/wiki/Assignment_problem)
    n'_{j,l} is the symmetrical vector of n_{j,l} wrt plane of normal vector n_{i,j}
    Division by the sum of cell surfaces ensures that the score is in [0,1].

    Parameters
    ----------
    cell_contact_surface: dictionary of cell contact surface
    barycenters: dictionary of cell centers of mass
    timepoint: time point to be processed
    time_digits_for_cell_id:

    Returns
    -------
    symcells: dictionary indexed by cell id. Values are the cell id list of potential symmetrical cells
      (ordered by decreasing score): the first cell is then the most likely to be the symmetrical cell.
    surfaces: dictionary (indexed by cell id) of cell surfaces

    """

    #
    # precompute
    # ref_data['cells'] : dictionary of list of cells at time ref_time
    # ref_data['contact'] : dictionary of (rectified) contact surfaces (value = dictionary of contact surfaces)
    # ref_data['neighbors'] : dictionary of inner neighbors (value = list of inner neighbors)
    # ref_data['n_neighbors'] : dictionary of #inner neighbors (value = number)
    # ref_data['back_surface'] : dictionary of background surfaces (value = surface)
    # ref_data['surface'] : dictionary of total surfaces (value = surface)
    # ref_data['vector'] :
    # ref_data['npvector'] : weighted contact vectors (value = numpy array of shape = (3,:))
    # ref_data['npcontact'] : dictionary of contact surfaces (value = numpy array of dim 1)
    #
    emb_data = _precompute_winged_contact(cell_contact_surface, barycenters, timepoint,
                                          time_digits_for_cell_id=time_digits_for_cell_id)

    #
    # localprocessors = processors
    # if len(emb_data['cells']) <= 100:
    #     localprocessors = 1
    #

    # mapping = []
    # if localprocessors > 1:
    #     pool = multiprocessing.Pool(processes=localprocessors)
    #     splitcells = _split_list(emb_data['cells'], localprocessors)
    #     for i in splitcells:
    #         mapping.append((i, emb_data, barycenters))
    #     outputs = pool.map(_proc_symcells_from_wingedcontact, mapping)
    #     pool.close()
    #     pool.terminate()
    #
    #     symcells = {}
    #     surfaces = {}
    #     for lsymcells, lsurfaces in outputs:
    #         symcells.update(lsymcells)
    #         surfaces.update(lsurfaces)
    # else:
    symcells, surfaces = _proc_symcells_from_wingedcontact((emb_data['cells'], emb_data, barycenters))

    return symcells, surfaces


def symmetriccells(cell_contact_surface, barycenters, timepoint, cell_similarity, time_digits_for_cell_id=4,
                   processors=8):
    """
    Computes symmetrical cell candidates, as well as the cell surfaces, without left/right symmetry
    estimation
    :param cell_contact_surface:
    :param barycenters:
    :param timepoint:
    :param cell_similarity: 'sorted-contact' or 'winged-contact'
    - 'sorted-contact': similarity is computed from the differences of the sorted contact surfaces
    - 'winged-contact'
    :param time_digits_for_cell_id:
    :param processors:
    :return:
    - symcells: a dictionary indexed by cell id, whose value is an array of tuples (d, score)
      d being a cell id, and score the similarity score (in [0,1])
      the array is sorted in increasing order, meaning that the first entry is the most
      probable symmetrical cell
    - surfaces: a dictionary indexed by cell id, whose value is the cell surface (not normalized)
    """
    proc = "symmetriccells"

    if cell_contact_surface is None:
        msg = "no cell contact surfaces?!"
        monitoring.to_log_and_console(proc + ": " + msg)
        return None, None

    cell_similarity = cell_similarity
    if cell_similarity == 'sorted-contact':
        pass
    elif cell_similarity == 'winged-contact':
        pass
    else:
        msg = "'cell_similarity' value '" + str(cell_similarity) + "' not handled yet"
        msg += "\t switch to 'sorted-contact'"
        monitoring.to_log_and_console(proc + ": " + msg)
        cell_similarity = 'sorted-contact'

    if cell_similarity == 'winged-contact' and barycenters is None:
        msg = "no cell centers of mass?!"
        msg += "\t switch to 'sorted-contact'"
        monitoring.to_log_and_console(proc + ": " + msg)
        cell_similarity = 'sorted-contact'

    #
    # there seems to have a benefit for a parallel processing for winged contacts
    # but not  for sorted contacts
    #
    if cell_similarity == 'sorted-contact':
        symcells, surfaces = _symcells_from_sortedcontact(cell_contact_surface, timepoint,
                                                          time_digits_for_cell_id=time_digits_for_cell_id)
    elif cell_similarity == 'winged-contact':
        symcells, surfaces = _symcells_from_wingedcontact(cell_contact_surface, barycenters, timepoint,
                                                          time_digits_for_cell_id=time_digits_for_cell_id,
                                                          processors=processors)
    else:
        msg = "weird, this should not occur ..."
        monitoring.to_log_and_console(proc + ": " + msg)
        sys.exit(1)
    return symcells, surfaces


###########################################################
#
# symmetry distribution: samples from contact surfaces
#
###########################################################


def _samples_from_symcells(symcells, surfaces, barycenters, parameters):

    nvectors = 1
    if parameters.symmetric_cell_number is not None and isinstance(parameters.symmetric_cell_number, int):
        if parameters.symmetric_cell_number > 0:
            nvectors = parameters.symmetric_cell_number

    vectors = []
    weights = []
    v = np.zeros(3)

    # symcells = dictionary indexed by cell id
    #   values = sorted (ascending order) array of tuples (id of symmetry candidate, similarity value)
    # similarity is a "distance" in [0,1] between cells (the less, the better)
    #

    for c in symcells:
        for i in range(nvectors):
            # this should never happen
            if i >= len(symcells[c]):
                continue
            #
            # approximation of the normal vector of the contact surface between c and d
            #
            v[0] = barycenters[c][0] - barycenters[symcells[c][i][0]][0]
            v[1] = barycenters[c][1] - barycenters[symcells[c][i][0]][1]
            v[2] = barycenters[c][2] - barycenters[symcells[c][i][0]][2]
            v /= np.linalg.norm(v)

            vectors += [copy.deepcopy(v)]
            #
            # weight is the cell surface times 1-(contact surface similarity)
            # - the worse the similarity, the smaller the weight
            # - the smaller the cell, the smaller the weight
            #
            weights += [surfaces[c] * (1.0 - symcells[c][i][1])]
    return np.array(vectors), np.array(weights)


###########################################################
#
#
#
###########################################################

def leftright_symmetry_candidates(cell_contact_surface, barycenters, timepoint, parameters=None,
                                  time_digits_for_cell_id=4, verbose=True):
    proc = "leftright_symmetry_candidates"

    #
    # called by get_direction_distribution_candidates(timepoint, parameters=None, verbose=True)
    # from Atlas() class object
    #
    if parameters is None:
        local_parameters = sparameters.SymmetryParameters()
    else:
        if isinstance(parameters, sparameters.SymmetryParameters):
            local_parameters = parameters
        else:
            msg = "parameters expected type is 'SymmetryParameters' but was '"
            msg += str(type(parameters)) + "' instead"
            monitoring.to_log_and_console(proc + ": " + msg)
            local_parameters = sparameters.SymmetryParameters()

    direction_sample = local_parameters.direction_sample
    if direction_sample == 'contact-surface':
        pass
    elif direction_sample == 'symmetric-cell':
        pass
    else:
        msg = "'direction_sample' value '" + str(direction_sample) + "' not handled yet\n"
        msg += "\t switch to 'symmetric-cell'"
        monitoring.to_log_and_console(proc + ": " + msg)
        direction_sample = 'symmetric-cell'

    if direction_sample == 'contact-surface':

        vectors, weights = _samples_from_contactsurfaces(cell_contact_surface, barycenters, timepoint,
                                                         time_digits_for_cell_id=time_digits_for_cell_id)

    elif direction_sample == 'symmetric-cell':

        symcells, surfaces = symmetriccells(cell_contact_surface, barycenters, timepoint,
                                            parameters.symmetrical_cell_similarity,
                                            time_digits_for_cell_id=time_digits_for_cell_id)
        vectors, weights = _samples_from_symcells(symcells, surfaces, barycenters, parameters)

    else:
        msg = "weird, this should not occur ..."
        monitoring.to_log_and_console(proc + ": " + msg)
        sys.exit(1)

    vdist.monitoring.copy(monitoring)
    vector_distribution = vdist.VectorDistribution(vectors=vectors, weights=weights,
                                                   sphere_radius=local_parameters.distribution_sphere_radius,
                                                   kernel_sigma=local_parameters.distribution_kernel_sigma,
                                                   add_opposite=True, verbose=verbose)
    return vector_distribution.maxima(maxima_threshold=local_parameters.direction_maxima_threshold,
                                      maxima_number=local_parameters.direction_maxima_number)

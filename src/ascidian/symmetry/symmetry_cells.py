##############################################################
#
#       ASCIDIAN package
#
#       Copyright INRIA 2021-2024
#
#       File author(s):
#           Gregoire Malandain
#       Creation date:
#           Ven  1 sep 2023 17:49:53 CEST
#
##############################################################
#
# Identify the symmetric cells in one embryo from the symmetry direction
# Useless according to P.Lemaire and K. Biasuz
# kept for memory
#
##############################################################

import copy
from operator import itemgetter

import numpy as np
import scipy.optimize as optimize

import astec.utils.common as common
import ascidian.core.naming_utils as cnamutils
import ascidian.ascidian.name as aname

monitoring = common.Monitoring()


#############################################################
#
#
#
#############################################################

def _adjust_barycenter_1(vector, barycenter, cell_barycenter):

    #
    # distance of M_i to a line = |GM_i . v| / |v|
    #
    # sum_i ((M_i - G - lv) . v)^2 = sum_i ( M_i.v - G.v - l |v|^2 )^2
    #
    # derivative = 2 sum_i ( M_i.v - G.v - l |v|^2 ) (- |v|^2 ) = 0
    # N l |v|^2 = sum M_i.v - N G.v
    # l = (sum M_i.v) / N |v|^2 - G.v /  |v|^2

    nv = np.linalg.norm(vector)

    init_cost = 0.0
    for i in cell_barycenter:
        c = np.dot(vector, cell_barycenter[i]) - np.dot(vector, barycenter)
        init_cost += c*c

    sum_dots = 0.0
    for i in cell_barycenter:
        sum_dots += np.dot(vector, cell_barycenter[i])
    d = sum_dots / (len(cell_barycenter) * nv * nv) - np.dot(vector, barycenter) / (nv * nv)

    new_barycenter = copy.deepcopy(barycenter)
    new_barycenter[0] += d * vector[0]
    new_barycenter[1] += d * vector[1]
    new_barycenter[2] += d * vector[2]

    new_cost = 0.0
    for i in cell_barycenter:
        c = np.dot(vector, cell_barycenter[i]) - np.dot(vector, new_barycenter)
        new_cost += c * c
    return new_barycenter


def _check_reciprocal_symmetry(prop):
    toberemoved = []
    for c in prop:
        if prop[c] == c:
            toberemoved += [c]
            continue
        if prop[prop[c]] != c:
            toberemoved += [c]
    if len(toberemoved):
        print("... will remove " + str(toberemoved) + " from symmetrical cells")
    for c in toberemoved:
        prop.pop(c)
    return prop


def _set_symmetric_cells_wrt_vector_1(vector, barycenter, cell_barycenter):

    center = _adjust_barycenter_1(vector, barycenter, cell_barycenter)

    #
    # build a list a cells ids
    #
    idlist = []
    for c in cell_barycenter:
        idlist += [c]
    #
    # build a cost between all cells
    # other solution is to part the cells into two (according the symmetry plane
    # parts the cells into two sets of same cardinal)
    #
    nv = np.linalg.norm(vector)
    cost = np.zeros((len(cell_barycenter), len(cell_barycenter)))
    for i, c in enumerate(idlist):
        #
        # compute symmetric of barycenter wrt the symmetry plane
        #
        gm = cell_barycenter[c] - center
        a = 2 * np.dot(gm, vector) / nv
        symb = cell_barycenter[c] - 2 * a * vector
        for j, d in enumerate(idlist):
            cost[i, j] = np.linalg.norm(cell_barycenter[d] - symb)
    row_ind, col_ind = optimize.linear_sum_assignment(cost)
    res = {}
    for i, r in enumerate(row_ind):
        res[idlist[r]] = idlist[col_ind[i]]
    res = _check_reciprocal_symmetry(res)
    return res


#############################################################
#
#
#
#############################################################

def _adjust_barycenter_2(vector, barycenter, cell_barycenter):

    #
    # distance of M_i to a line = |GM_i . v| / |v|
    #
    # center is adjusted to part the cell barycenters into two (equal) parts
    # and is at the middle of the two closest barycenters
    #

    nv = np.linalg.norm(vector)

    pos_b = []
    neg_b = []
    for i in cell_barycenter:
        c = np.dot(vector, cell_barycenter[i] - barycenter) / nv
        if c < 0:
            neg_b += [(i, c)]
        else:
            pos_b += [(i, c)]

    pos_b = sorted(pos_b, key=itemgetter(1))
    neg_b = sorted(neg_b, key=itemgetter(1), reverse=True)
    #
    # test both if case of odd count of cells
    #
    if len(neg_b) == len(cell_barycenter) // 2 or len(pos_b) == len(cell_barycenter) // 2:
        swift = (neg_b[0][1] + pos_b[0][1]) / 2.0
    elif len(neg_b) < len(pos_b):
        added_b = len(cell_barycenter) // 2 - len(neg_b)
        swift = (pos_b[added_b][1] + pos_b[added_b-1][1]) / 2.0
    else:
        added_b = len(cell_barycenter) // 2 - len(pos_b)
        swift = (neg_b[added_b][1] + neg_b[added_b - 1][1]) / 2.0
    new_barycenter = barycenter + swift * vector / nv

    #
    # return indices of positive and negative barycenters
    #
    pos_b = []
    neg_b = []
    for i in cell_barycenter:
        c = np.dot(vector, cell_barycenter[i] - new_barycenter) / nv
        if c < 0:
            neg_b += [i]
        else:
            pos_b += [i]

    return new_barycenter, neg_b, pos_b


def _set_symmetric_cells_wrt_vector_2(vector, barycenter, cell_barycenter):
    center, neg_b, pos_b = _adjust_barycenter_2(vector, barycenter, cell_barycenter)
    #
    # build a cost between all cells
    # other solution is to part the cells into two (according the symmetry plane
    # parts the cells into two sets of same cardinal)
    #
    nv = np.linalg.norm(vector)
    res = {}

    cost_neg_pos = np.zeros((len(neg_b), len(pos_b)))
    for i, c in enumerate(neg_b):
        #
        # compute symmetric of barycenter wrt the symmetry plane
        #
        gm = cell_barycenter[c] - center
        a = 2 * np.dot(gm, vector) / nv
        symb = cell_barycenter[c] - 2 * a * vector
        for j, d in enumerate(pos_b):
            cost_neg_pos[i, j] = np.linalg.norm(cell_barycenter[d] - symb)
    row_ind, col_ind = optimize.linear_sum_assignment(cost_neg_pos)
    for i, r in enumerate(row_ind):
        res[neg_b[r]] = pos_b[col_ind[i]]

    cost_pos_neg = np.zeros((len(pos_b), len(neg_b)))
    for i, c in enumerate(pos_b):
        #
        # compute symmetric of barycenter wrt the symmetry plane
        #
        gm = cell_barycenter[c] - center
        a = 2 * np.dot(gm, vector) / nv
        symb = cell_barycenter[c] - 2 * a * vector
        for j, d in enumerate(neg_b):
            cost_pos_neg[i, j] = np.linalg.norm(cell_barycenter[d] - symb)
    row_ind, col_ind = optimize.linear_sum_assignment(cost_pos_neg)
    for i, r in enumerate(row_ind):
        res[pos_b[r]] = neg_b[col_ind[i]]

    res = _check_reciprocal_symmetry(res)
    return res


#############################################################
#
#
#
#############################################################


def test_symmetric_cells(prop, reference_prop, time_digits_for_cell_id=4, verbose=True):
    #
    #
    #
    times = cnamutils.get_named_timepoints(prop, key_name='cell_symmetric',
                                           time_digits_for_cell_id=time_digits_for_cell_id)
    if len(times) == 0:
        if verbose:
            msg = "no symmetric cells were set"
            monitoring.to_log_and_console("... " + msg)
    elif len(times) > 1:
        if verbose:
            msg = "weird, symmetric cells were set on several time points " + str(times)
            monitoring.to_log_and_console("... " + msg)

    cells = set(list(prop['cell_symmetric'].keys()))
    div = 10 ** time_digits_for_cell_id
    times = list(set([int(c) // div for c in cells]))

    refcells = set([c for c in reference_prop['cell_name'] if int(c) // div in times])

    unnamed_cells = refcells.difference(cells)
    unnamed_names = sorted([reference_prop['cell_name'][c] for c in unnamed_cells])
    not_in_reference_cells = cells.difference(refcells)

    common_cells = refcells.intersection(cells)
    right_cells = []
    wrong_cells = []
    wrong_names = []
    for c in common_cells:
        cname = reference_prop['cell_name'][c]
        if reference_prop['cell_name'][prop['cell_symmetric'][c]] != aname.get_symmetric_name(cname):
            wrong_cells += [c]
            wrong_names += [(cname, reference_prop['cell_name'][prop['cell_symmetric'][c]])]
            continue
        right_cells += [c]

    wrong_names = sorted(wrong_names, key=itemgetter(0))

    if verbose:
        msg = "ground-truth cells = " + str(len(refcells)) + " for times " + str(times) + "\n"
        msg += "\t tested cells = " + str(len(cells)) + "\n"
        msg += "\t right symmetric names = {:d}/{:d}\n".format(len(right_cells), len(refcells))
        if len(wrong_cells) > 0:
            msg += "\t ... error in symmetric cells = {:d}/{:d}\n".format(len(wrong_cells), len(refcells))
            msg += "\t     reference cells wrongly named (right name, given name): " + str(wrong_names) + "\n"
        if len(unnamed_cells) > 0:
            msg += "\t ... unnamed cells = {:d}/{:d}\n".format(len(unnamed_cells), len(refcells))
            msg += "\t     reference cells unnamed: " + str(unnamed_names) + "\n"
        if len(not_in_reference_cells) > 0:
            msg += "\t ... names not in reference = {:d}/{:d}\n".format(len(not_in_reference_cells), len(refcells))
        monitoring.to_log_and_console("summary" + ": " + msg)

    return len(right_cells), len(wrong_cells), len(unnamed_cells), len(not_in_reference_cells)

#############################################################
#
#
#
#############################################################

def set_symmetric_cells(prop, embryo, timepoint, parameters=None, verbose=True):

    #
    # cells barycenter
    #
    all_barycenter = embryo.cell_barycenter
    div = 10 ** embryo.time_digits_for_cell_id
    cell_barycenter = {}
    for c in all_barycenter:
        if int(c) // div == timepoint:
            cell_barycenter[c] = all_barycenter[c]

    #
    # embryo and cells barycenter
    #
    barycenter = embryo.get_embryo_barycenter(timepoint)

    #
    # candidates for symmetry directions
    # candidates is a list od dictionaries
    # each dictionary having the following keys
    # - 'voxel': the 3D point in the 3D array corresponding to the vector
    # - 'vector': the unit vector (joigning the 'voxel' and the array center)
    # - 'value': the distribution value
    #
    candidates = embryo.get_direction_distribution_candidates(timepoint, parameters=parameters, verbose=verbose)
    vector = candidates[0]
    prop['cell_symmetric'] = _set_symmetric_cells_wrt_vector_1(vector, barycenter, cell_barycenter)
    return prop

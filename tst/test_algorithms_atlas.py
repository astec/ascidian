import unittest

import inspect
import os

import ascidian.algorithms.atlas as asc_atlas


class TestAlgorithmAtlas(unittest.TestCase):
    '''
    Test the algorithms/atlas functions
    '''

    # before the tests
    def setUp(self):
        pass

    # after the tests
    def tearDown(self):
        pass

    def test_01_atlas_process(self):
        '''
        test atlas_process(experiment, parameters) function
        '''

        # initialise expermient
        # initialise parameters
        # run atlas_process(experiment, parameters)
        # verify that the results are OK

        # remove this line in the final code
        self.assertEqual(1, 1)

        pass


if __name__ == '__main__':  # pragma: no cover
    unittest.main()

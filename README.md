# ASCIDIAN

This file contains instructions for installing and running the ASCIDIAN package.

ASCIDIAN is an extension of ASTEC (see [astec.gitlabpages.inria.fr/astec/](https://astec.gitlabpages.inria.fr/astec/)).


## Installation

Was tested only on Linux or MacOs systems.

1. <a href="#user-installation">User installation</a>: recommended for a basic use
2. <a href="#user-installation">User installation (with git)</a>
3. <a href="#dev-installation">Developer installation (with git)</a>
4. <a href="#doc-installation">Documentation update (with git)</a>

### [I - User installation (without git)](#user-installation)

If you do not need to get last update (ie if you have no interaction with developer(s)), this should 
be your installation of choice. Requires `conda`.

1. Create a conda environment (here the environment is named
`ascidian`)

    ```bash
    conda create -n ascidian -c morpheme -c conda-forge ascidian
    ```

2. Activate the built conda environment

    ```bash
    conda activate ascidian
    ```

Update of packages can be done with

```bash
conda update -n ascidian -c morpheme -c conda-forge ascidian
```

### [II - User installation (with git)](#user-installation-git)

Requires `conda` and `git`.

1. Ascidian code can be found at
   [gitlab.inria.fr/astec/ascidian](http://gitlab.inria.fr/astec/ascidian). It
   can be downloaded with

    ```bash
    git clone https://gitlab.inria.fr/astec/ascidian.git
    ```

    It creates an `ascidian` directory.

2. Create a conda environment named `ascidian`

    ```bash
    cd ascidian
    conda env create -f pkg/env/ascidian.yaml
    ```

   If already created, the conda environment can be updated with
    ```bash
    conda env update -f pkg/env/ascidian.yaml
    ```

3. Activate the built conda environment

    ```bash
    conda activate ascidian
    ```

### [III - Developer  installation (with git)](#dev-installation)

Requires `conda` and `git`.

1. Ascidian code can be found at
   [gitlab.inria.fr/astec/ascidian](http://gitlab.inria.fr/astec/ascidian). It
   can be downloaded with

    ```bash
    git clone https://gitlab.inria.fr/astec/ascidian.git
    ```

    It creates an `ascidian` directory.

2. Create a conda environment named `ascidian-dev`

    ```bash
    cd ascidian
    conda env create -f pkg/env/ascidian-dev.yaml
    ```

   If already created, the conda environment can be updated with
    ```bash
    conda env update -f pkg/env/ascidian-dev.yaml
    ```

3. Activate the built conda environment

    ```bash
    conda activate ascidian-dev
    ```

4. Install ascidian package for use

    ```bash
    python -m pip install -e .
    ```

    The `-e` option install the package in "editable" mode, this is
    want you want if you aim at contributing to the ascidian
    project. This last command has to be repeated (within the conda
    environment every time the ascidian code has been modified).

### [IV - Documentation update (with git)](#doc-installation)

This environment only allows to update the documentation, not to test code changes (use ``ascidian-dev`` then).
Requires `conda` and `git`.

1. Ascidian code can be found at
   [gitlab.inria.fr/astec/astec](http://gitlab.inria.fr/astec/ascidian). It
   can be downloaded with

    ```bash
    git clone https://gitlab.inria.fr/astec/ascidian.git
    ```

    It creates an `ascidian` directory.

2. Create a conda environment named `ascidian-doc`

    ```bash
    cd ascidian
    conda env create -f pkg/env/ascidian-doc.yaml
    ```

   If already created, the conda environment can be updated with
    ```bash
    conda env update -f pkg/env/ascidian-doc.yaml
    ```

3. Activate the built conda environment

    ```bash
    conda activate ascidian-doc
    ```

4. Build documentation

    Assume you are in the ``ascidian`` directory created at step 1:
    ```bash
    cd doc
    make html
    ```

    The documentation will be built into the ``ascidian/doc/build/`` directory, and can be accessed
    through ``ascidian/doc/build/html/index.html`` with your favorite browser. 
    Documentation can also be built within the ``ascidian-dev`` conda environment. 


## Documentation

Ascidian documentation can be found at [astec.gitlabpages.inria.fr/ascidian/](https://astec.gitlabpages.inria.fr/ascidian/).

Remark:
Astec documentation can be found at [astec.gitlabpages.inria.fr/astec/](https://astec.gitlabpages.inria.fr/astec/).

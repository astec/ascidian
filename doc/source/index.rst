=====================================
Ascidian package
=====================================

.. image:: https://anaconda.org/morpheme/ascidian/badges/version.svg
   :target: https://anaconda.org/morpheme/ascidian

.. image:: https://anaconda.org/morpheme/astec/badges/latest_release_date.svg
   :target: https://anaconda.org/morpheme/ascidian

.. image:: https://anaconda.org/morpheme/astec/badges/platforms.svg
   :target: https://anaconda.org/morpheme/ascidian

.. image:: https://anaconda.org/morpheme/astec/badges/license.svg
   :target: https://anaconda.org/morpheme/ascidian

.. image:: https://anaconda.org/morpheme/ascidian/badges/downloads.svg
   :target: https://conda.anaconda.org/morpheme/ascidian



The ASCIDIAN package aims at exploiting results issued from the processing
of 3D+t sequences of developing embryos imaged by a light-sheet microscope.
The processing, done by the
`ASTEC suite <https://astec.gitlabpages.inria.fr/astec/>`_, results in
a so-called property file.

Assuming the imaged embryos are
`ascidians <https://en.wikipedia.org/wiki/Ascidiacea>`_, this suite provides
tools to name (after Conklin) one particular time point
(see :ref:`cli-ascidian-naming-timepoint`),
to propagate the naming to other time points
(see :ref:`cli-ascidian-naming-propagation`),
or to study a population of embryos
(see :ref:`cli-ascidian-embryo` and :ref:`cli-gallery`).

.. toctree::
   :maxdepth: 3
   :caption: Contents
   :numbered:

   releasenotes.rst
   installation.rst
   cli.rst
   ascidian_embryo.rst
   ascidian_naming_timepoint.rst
   ascidian_naming_propagation.rst
   ascidian_properties.rst
   ascidian_parameters.rst
   gallery.rst
   reference.rst
   publications.rst

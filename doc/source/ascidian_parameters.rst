Parameters
==========

The different command line interfaces, or CLIs, (``ascidian_embryo``, etc.) requires a parameter file (which is nothing but a ``python`` file) that contains specific parameters for the CLIs.





Prefixed parameters
-------------------
See the `prefixed parameters section of Astec documentation <https://astec.gitlabpages.inria.fr/astec/astec_parameters.html#prefixed-parameters>`_.



.. _cli-parameters-diagnosis:

Diagnosis parameters
--------------------

* `Astec Diagnosis parameters <https://astec.gitlabpages.inria.fr/astec/astec_parameters.html#diagnosis-parameters>`_

* ``maximal_contact_distance``: for diagnosis on cell contact surface. Threshold on cell contact surface distance
  along branches. Time points along branches that have a cell contact surface distance
  above this threshold are displayed (recall that the distance is in [0, 1]).



.. _cli-parameters-embryo-symmetry:

Embryo symmetry parameters
--------------------------

Embryo co-registration can be made efficiently by using embryo symmetry axis candidates,
by first computing the distribution of the surface normals, and then extracting the maxima.

* ``direction_sample``: Samples (vectors) that will be used to computed the direction distribution
  via kernel density estimation.
  In all cases, the used vectors are unit vectors derived from the vector
  joining the barycenters of a couple of cells.

  * ``'contact-surface'``: cell couples are adjacent cells. Samples (ie vectors)
    are weighted by the contact surfaces of the adjacent cells.
    It comes to mimic G. Michelin approach to estimate the direction distribution
  * ``'symmetric-cell'``: for each cell, a list of potential symmetric cells is
    build and sorted by decreasing likelihood. A neighborhood-based similarity
    (in [0,1]) is computed to estimate the likelihood.
    The first ``'symmetric_cell_number'`` (ie the first ``'symmetric_cell_number'``
    cells that are candidate to be the symmetrical cells), hence there can be more
    than one sample per cell.
    Samples are weighted by the total cell surface times (1.0 - similarity)

* ``symmetric_cell_number``: Number of cells used as candidates to be the symmetrical cell.
  ``'symmetric_cell_number = 1'`` means there is only one candidate
  per cell.

* ``cell_similarity``: Method to compute the similarity between a cell and an other cell
  candidate to be the symmetrical of the first one.
  For the two cells to be compared, neighbors are paired and a
  difference measure is computed for every neighbor pair and summed up.
  The sum is then divided by the sum of the two cell surfaces, which
  ensures that the computed similarity is in [0, 1].

  * ``'sorted-contact'``: the neighbors of the two cells to be compared are
    sorted in decreasing order (based on contact surfaces) and then paired.
    The difference measure is the difference of contact surfaces.
    This is exactly the distance used to name cells except that the pairing
    is done based on contact surface values except of cell names.
  * ``'winged-contact'``: (unit) vectors joining a cell to its neighbors are used here.
    Let c and d be the cells to be compared, :math:`c(i)` be the contact surface of a
    neighbor :math:`i` of :math:`c` and :math:`\mathbf{v}(i)`
    the unit vector joining the barycenter of :math:`c` to the
    one of :math:`i`.
    Let :math:`\mathbf{dc}` be the unit vector joining the barycenter of
    :math:`c` and :math:`d`.
    Vectors :math:`\mathbf{v}(j)` of neighbors :math:`j` of
    :math:`d` are 'symmetrised' by
    :math:`\mathbf{v}'(j) = \mathbf{v}(j) - 2 (\mathbf{v}(j).\mathbf{dc}) \mathbf{dc}`
    The difference measure is
    :math:`\|c(i)\mathbf{v}(i) - c(j)\mathbf{v}'(j)\|` which is the contact surface
    difference if the two vectors :math:`\mathbf{v}(i)` and :math:`\mathbf{v}'(j)` are equal.
    Differences are computed for every couple :math:`(i,j)` and optimal pairing is done
    through ``scipy.optimize.linear_sum_assignment()``

* ``distribution_sphere_radius``: sphere radius to build the distribution support.
  The direction distribution is computed onto a discrete sphere, ie a sphere made of voxels.
  The larger the sphere, the more accurate the mode estimation, but the longer the
  computation.

  * radius = 10:   978 vectors, angle between neighboring vectors in [4.40, 10.58] degrees
  * radius = 15:  2262 vectors, angle between neighboring vectors in [2.98, 6.93] degrees
  * radius = 20:  4026 vectors, angle between neighboring vectors in [2.25, 5.16] degrees
  * radius = 25:  6366 vectors, angle between neighboring vectors in [1.73, 4.01] degrees
  * radius = 30:  9194 vectors, angle between neighboring vectors in [1.46, 3.40] degrees
  * radius = 35: 12542 vectors, angle between neighboring vectors in [1.26, 2.90] degrees
  * radius = 40: 16418 vectors, angle between neighboring vectors in [1.08, 2.53] degrees

* ``distribution_kernel_sigma``: Sigma (standard deviation) to build the direction distribution (in radian).
  The distribution is built through a Gaussian kernel density estimation.

* ``direction_maxima_threshold``: Threshold on the distribution value. Only maxima above this threshold are
  kept. Recall that the distribution values are normalize so that the maximum is 1.

* ``direction_maxima_number``: Number of distribution maxima to be retained as symmetry axis candidates.
  ``None`` or negative number means all of them. Since the distribution is computed onto a sphere,
  both the surface normal and its opposite contribute to the distribution estimation.
  It comes out that each symmetry direction is represented by two (opposite) vectors, meaning that
  this parameter has to be even.

* ``processors``: Number of processors for parallelization (when required).



.. _cli-parameters-figure:

Figure parameters
-----------------

Figures can ease the visual assessment of some charateristics or concepts
used to manipulate a set of embryos (or atlas), see :ref:`cli-gallery` for some
examples.
To ease figure manipulation (to build a combination of figures,
or to chnage title, tickes, etc.), for each figure a ``python`` file, prefixed
par ``figure_`` is generated. Then, processing the above file with python
generates the figure itself.


* ``outputDir``: output directory where to write atlas-individualized output files,
  ie morphonet selection files or figure files.

* ``figurefile_suffix``: suffix used to named the above python files as well as the generated figures.


* ``generate_figure``: if ``True``, generate python files (prefixed by ``figure_``) that generate figures.
  Those files will be saved into the ``outputDir`` directory.
  ``generate_figure`` can be

  * a boolean value: if ``True``, all figure files are generated; if ``False``, none of them
  * a string: if ``'all'``, all figure files are generated; else, only the specified
    figure file is generated (see below for the list)
  * a list of strings: if ``'all'`` is in the list, all figure files are generated; else, only
    the specified figure files are generated (see below for the list)

List of figures (see section :ref:`cli-gallery`):

  * ``'cell-composition'``: plot the number of names wrt time point (normalized time
    ie after temporal registration), and separate between names present in all embryos
    and others. It offers a means to visually assess the heterechronocity of divisions.
    (see :ref:`cli-gallery-cell-composition`).

  * ``'cell-to-cell-distance-along-branch'``: plot the cell-to-cell distance between successive
    along a branch (a cell without division) wrt the distance to the first cell. Cell neighborhoods
    are expressed with the neighbors of the first cell of the branch (thus it ignores divisions
    occurring in the cell neighborhood during the cell life).
    (see :ref:`cli-gallery-cell-to-cell-distance`).

  * ``'cell-to-cell-distance-histogram'``: plot the cell-to-cell distance
    between the same cell (cell of same name) between embryos, or
    between a cell and its sister cells (mostly inter-embryo, but one measure is
    within the same embryo), or between a cell and other (than the same and the sister) cells.
    (see :ref:`cli-gallery-cell-to-cell-distance-histogram`).

  * ``'cell-neighbor-count'``: plot the cell number in the cell neighborhood wrt
    the total cell number in the embryo.
    (see :ref:`cli-gallery-cell-neighbor-count`).

  * ``'division-to-division-distance-histogram'``: plot the division-to-division distance
    for both right and wrong pairings of the daughter cells.
    (see :ref:`cli-gallery-division-to-division-distance-histogram`).

  * ``'division-time-couple-misordering'``: assess the heterochrony of divisions
    in a set of embryos for a couple of divisions. The list of couples
    to be studied is to be edited in the ``figure_`` python file.
    (see (see :ref:`cli-gallery-division-time-mis-ordering-cell`).

  * ``'division-time-difference'``: assess the synchrony of division
    for bilateral cells (ie the pair of corresponding right and left cells)
    with their difference in division times for a given embryo.
    (see :ref:`cli-gallery-division-time-differences`).

  * ``'division-time-global-misordering'``: assess the heterochrony of divisions
    in a set of embryos, by plotting the probablity that a couple of divisions
    does not follow the *natural* order (given by ordering the average times of division).
    (see (see :ref:`cli-gallery-division-time-mis-ordering-global`).

  * ``'symmetrical-cell-percentage'``: assess the recognition of symmetrical cells
    when computing the symmetry axis of an embryo, by plotting the percentage
    of well recognized symmetrical cells (the symmetry axis will be computed
    from the direction distribution issued from pair of symmetrical cells).
    (see :ref:`cli-gallery-symmetrical-cells`).

  * ``'symmetry-axis-assessment'``: assess the computation of the symmetry axis.
    The symmetry axis is computed as a maximum of a direction distribution:
    it is assessed by the rank of the (ordered) maximum closest to the
    true symmetry axis, and by the angle between this maximum and the true symmetry axis.
    (see :ref:`cli-gallery-symmetry-axis`).

  * ``'temporal-alignment'``: for comparison, embryos may be required to be
    temporally aligned. This is achieved by a time linear warping
    based on the (total) cell counts (it is assumed that cell counts reflects the
    *developmental* time).
    In a linear warping, :math:`a * t + t`, :math:`a` corrects for the development speed,
    that depends on the temperature, and :math:`b` corrects for the
    initial acquisition time, that differs from one embryo to the other.
    (see :ref:`cli-gallery-temporal-alignment`).

  * ``'temporal-alignment-epidermis'``: for perturbed development (eg when adding
    the MEK inhibitor `U0126 <https://en.wikipedia.org/wiki/U0126>`_), the total
    cell count does not reflect the *developmental* time anymore.
    Since U0126 does not perturb the epidermis cells, it is then
    possible to compute the linear warping on the epidermis cell counts.
    (see :ref:`cli-gallery-temporal-alignment`).

  * ``'volume-fitting'``: plot the embryo volume (in voxel) without and with temporal registration
    (computed from cell number). The decreasing volume of embryos can be visually assessed,
    as well as the linear regression (for intra- and inter-embryo volume normalization).
    This linear regression is used to compare embryos (eg surface contact values)
    at any times by normalizing
    their volumes to a common value.
    (see :ref:`cli-gallery-volume-normalization`).








.. _cli-parameters-base:

Base parameters
---------------

* Diagnosis parameters
  (see section :ref:`cli-parameters-diagnosis`)

* Embryo symmetry parameters
  (see section :ref:`cli-parameters-embryo-symmetry`)

* Figure parameters
  (see section :ref:`cli-parameters-figure`)

* ``'atlasEmbryos'``: list of atlas embryo files. An atlas file is a property
  file that contains lineage, names, and contact surfaces for an embryo.
  They will be used as reference for naming or figures may be built from
  their content.

* ``'referenceEmbryo'``: reference embryo. Use for time alignment. If not provided,
  the first atlas of 'atlasEmbryos' is used as reference. Warning, the reference
  atlas has to be in 'atlasEmbryos' list also.

* ``'testEmbryos'``: list of atlas embryo files. When figures are built by
  comparing one or several embryos against a set of atlas embryos, it can be done
  on a leave-one-out basis, or by comparing test embryos against atlas embryos.

* ``'add_symmetric_embryo'``: ``None``, ``True`` or ``False``. If ``True``,
  add symmetrical embryos to the atlas embryos. If ``None``, adopt a
  contextual default behavior.

* ``'atlas_diagnosis'``: ``True`` or ``False``. Performs some diagnosis
  when reading an additional property file into the atlases. Incrementing
  the verboseness ('-v' in the command line) may give more details.

* ``'temporal_alignment_min_cell_count'``:
* ``'temporal_alignment_max_cell_count'``: to put embryos in the same
  time reference, acquisition times (image index) are linearly registered
  against the reference embryo. Registration is done by comparing cell
  counts in the given interval. ``None`` means that interval (lower or upper)
  limit is calculated from data.
  **Note:** this was added for test purposes. No benefit has been shown so far
  to set this value.

* ``'temporal_alignment_fate'``: to put embryos in the same time reference,
  acquisition times (image index) are linearly registered against the
  reference embryo. Registration is done by comparing cell counts of the
  given fate. ``None`` means that all cells are used.
  ``'Epidermis'`` should be used when U0126 embryos are in the set.





.. _cli-parameters-naming-propagation:

Naming propagation parameters
-----------------------------

* Base parameters
  (see section :ref:`cli-parameters-base`)

* ``inputFile``: input property file to be named. Must contain lineage and contact surfaces
  as well as some input names (one time point should be entirely named).

* ``outputFile``: output property file.

* ``delay_from_division``: Delay from the division to extract the
  neighborhooods used for atlas building, and thus for naming. 0 means right
  after the division. Negative values means that the delay is counted
  backwards from the end of the branch.

* ``use_common_neighborhood``: the same cell has different neighbors from an atlas to the other.
  If 'True' build and keep an unique common neighborhood (set of neighbors) for all atlases by
  keeping the closest ancestor for neighboring cells. Eg, if a division has occurred in some
  embryos and not in others, daughter cells will be somehow fused so that all neighborhoods only
  exhibit the parent cell.
  Please do not consider changing its default value (``True``).

* ``selection_method``: decision method to name the daughters after a division. Distances are computed
  between the couple of daugthers to be named as well as the couple of switched daughters and
  all the atlas divisions.

  * ``'mean'``: choose the couple of names that yield the minimal average distance over all the atlases.
  * 'minimum': choose the couple of names that yield a minimal distance.
    It comes to name after the closest atlas (for this division).
  * ``'sum'``: same as ``mean``.
  * ``'majority'``: for each choice, order the distances in increasing order then compute the cumulated
    sum over the n first elements (so the first is the minimum distance, and the last one is the
    average over all atlases). Counts then the number of times when a choice is better than
    the switched one, and choose the name couple with majority choices.

* ``confidence_atlases_nmin``: minimum number of atlases required to assess naming confidence.
  If there is not enough atlases in the database for the aimed division, naming is not assessed.

* ``confidence_atlases_percentage``: percentage of atlases (for a given division) used to assess naming
  confidence. If the percentage is less than 'confidence_atlases_nmin', 'confidence_atlases_nmin' atlases are used.

* ``testFile``: input property file to be tested (must include cell names), for instance for
   leave-one-out test.
   64-cells time point is searched, cell names at other time points are deleted and the embryo
   is entirely renamed from this given time point. Comparison between new names and actual ones are reported.
   If given, ``inputFile`` is ignored.

* ``test_diagnosis``: if ``True``, some diagnosis are conducted on the property file to be tested.

* ``division_diagnosis``: ``True`` or ``False``.
  Performs some diagnosis after building the division atlas.
  Incrementing the verboseness ('-v' in the command line) may give more details.

* ``division_permutation_proposal``: it ``True``, will propose some daughters switches in the atlases.
  For a given division, a global score is computed as the sum of all pairwise division similarity.
  A switch is proposed for an atlas if it allows to decrease this global score.

* ``generate_figure``: see section :ref:`cli-parameters-figure`. Additional figure file generations
  are available (see section :ref:`cli-gallery`):

  * ``'division-dendrograms'``: plot dendrograms (hierarchical clustering) of embryos
    for each division, based on the division-to-division distance.

* ``cells_to_be_traced``:



.. _cli-parameters-embryo-registration:

Embryo registration parameters
------------------------------

* ``rotation_initialization``: to coregister two embryos, a first 3D rotation is done that align vectors issued
  from the floating embryo (the embryo to be named) onto vectors issued from the reference embryo
  (an already named embryo)

  * ``'sphere_wrt_z'``: an uniform sampling of 3D directions is done for the floating embryo
    (parameter 'direction_sphere_radius') while the 'z' direction is used for the reference embryo
  * ``'sphere_wrt_symaxis'``: an uniform sampling of 3D directions is done for the floating embryo
    (parameter ``direction_sphere_radius``) while one (out of two) vector defining the symmetry axis
    direction is used for the reference embryo (to be used for test purposes)
  * ``'symaxis_wrt_symaxis'``: symmetry axis vector candidates are used for the floating embryo
    (embryo symmetry parameters, see section :ref:`cli-parameters-embryo-symmetry`) while one
    (out of the two) vector defining the symmetry axis (estimated from the cell name) is used for the reference embryo.

* ``direction_sphere_radius``: to get an uniform sampling of the 3d directions in space (options ``'sphere_wrt_z'``
  and ``'sphere_wrt_symaxis'`` of ``rotation_initialization``), a discrete sphere is build and each point of the
  outer surface gives a sample. The larger the radius, the more vectors and the higher computational time.

  * radius = 2.0: 26 vectors, angle between neighboring vectors in [36.26, 60.0] degrees
  * radius = 2.3: 38 vectors, angle between neighboring vectors in [26.57, 54.74] degrees
  * radius = 2.5: 54 vectors, angle between neighboring vectors in [24.09, 43.09] degrees
  * radius = 2.9: 66 vectors, angle between neighboring vectors in [18.43, 43.09] degrees
  * radius = 3.0: 90 vectors, angle between neighboring vectors in [17.72, 43.09] degrees
  * radius = 3.5: 98 vectors, angle between neighboring vectors in [15.79, 32.51] degrees
  * radius = 3.7: 110 vectors, angle between neighboring vectors in [15.26, 32.51] degrees
  * radius = 3.8: 134 vectors, angle between neighboring vectors in [14.76, 29.50] degrees
  * radius = 4.0: 222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees
  * radius = 5.0: 222 vectors, angle between neighboring vectors in [10.31, 22.57] degrees

* ``z_rotation_angle_increment``: increment (in degrees) between two successive angles when enumerating
  rotations along the z or the symmetry axis

* ``transformation_filename``: file name to save or to read the transformations between the embryos.
  Useful when parsing name choice parameters (for test purposes then).

* ``pair_ratio_for_residual``: the optimal transformation between two embryos is the one giving the smallest
  residual value. The residual value of a transformation is the sum of the smallest  distances between paired points.
  This parameter gives the ratio of points to be retained
  (default value issued from G. Michelin's thesis :cite:p:`michelin:tel-01451608`).

* ``processors``: number of processors for parallelization



.. _cli-parameters-naming-assignment:

Naming assignment parameters
----------------------------

* ``matching_cell_similarity``

  * ``'sorted-contact'``
  * ``'winged-contact'``


.. _cli-parameters-naming-timepoint:

Naming timepoint parameters
---------------------------

* Base parameters
  (see section :ref:`cli-parameters-base`)

* Embryo registration parameters
  (see section :ref:`cli-parameters-embryo-registration`)

* Naming assignment parameters
  (see section :ref:`cli-parameters-naming-assignment`)

* ``inputFile``: input property file to be named. Must contain lineage and contact surfaces
  as well as some input names (one time point should be entirely named).

* ``outputFile``: output property file.

* ``naming_by_transfer``: naming by transfer method

  * ``'registration'``

* ``cell_number``: cell count of the developmental stade to be named

* ``minimal_consensus``: percentage of consensus to set a name to a cell

* ``check_duplicate``: ``True`` or ``False``.
  After naming, check whether some names are duplicated, and, if yes, remove them.

* ``testFile``: input property file to be tested (must include cell names).
   If given, ``inputFile`` is ignored.


* ``generate_figure``: see section :ref:`cli-parameters-figure`. Additional figure file generations
  are available (see section :ref:`cli-gallery`):

  * ``'naming_timepoint_wrt_atlases'``: plot leave-one-out results when naming
    one atlas out the ``atlasEmbryos`` with all combination of the other atlases (from combinations
    of 1 atlas to combinations of N-1 atlases). Results are plotted wrt the number
    of atlases used for naming.
    Setting ``'transformation_filename'`` (see section :ref:`cli-parameters-embryo-registration`)
    allows to keep/reuse computed embryo-to-embryo transformations.
    Warning: it may be long (embryo-to-embryo transformation calculation and
    quality estimation).

  * ``'naming_timepoint_wrt_cellcounts'`` plot leave-one-out results when naming
    one atlas out the ``atlasEmbryos`` with all the other atlases (for a series of cell counts).
    Results are plotted wrt cell count.
    Setting ``'transformation_filename'`` (see section :ref:`cli-parameters-embryo-registration`)
    allows to keep/reuse computed embryo-to-embryo transformations.
    Warning: it may be long (embryo-to-embryo transformation calculation).

-------------
Release notes
-------------

Developer version
=================

Version 1.1.2
=============

* update tissue fates (add 'Tail-tip Cells' fate)
* enrich morphonet file writing: enables more properties to be written out

Version 1.1.1
=============

* add __init__.py  in  src/ascidian/components

Version 1.1.0
=============

* division variation assessment

* morphonet name map

* embryo naming with graph matching

* optimization of winged contact cell signature

* optimization of symmetry axis computation with winged contacts

Version 1.0.0
=============

* major refactoring

* introduce the naming of a timepoint with linear assignment from cell similarity

* correct wrong behavior in name initialization when the requested cell count is not found

* correct bug on cell fate computation

* enrich diagnosis on cell names

Version 0.3.1
=============

* vector distribution, correct handling of file suffix in figure()

Version 0.3.0
=============

* documentation updating

* refactoring symmetry related code; vector distribution dedicated code

* pursued code splitting into astec/ascidian repositories

* documentation splitting into astec/ascidian

Version 0.2.0
=============

Version 0.1.0
=============

Version 0.0.0
=============

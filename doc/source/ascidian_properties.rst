
.. _cli-properties:

Image sequence properties
=========================

Cell unique id
--------------

Properties are computed over a temporal series of 3D images. The time point
corresponding to an image is encoded into the image name by a string
``_t<ttt>`` where ``<ttt>`` are three digits. Time points may start
at 0 (ie ``_t000``), 1 (ie ``_t001``), or later, but are a continuous series
of integers.

In one 3D image, or snapshot, objects
of interest (cells) are individualized by a *label* (ie an integer number).
By convention, the background used the 1 label, and
the cell labels are strictly larger than 1, but they are not necessarily
a continuous series of integer numbers starting at 2.

A same *label* can be used in different snapshots. To differentiate cells
from the different snapshots, each cell is being given an unique id,
defined by :math:`T \times 10000 + label`, where *T* is the time point
of the image and *label* the cell identifier in the image.
Thus the id 100256 designates
the cell of label 256 in the snapshot of time point 10.

Properties file format
----------------------

The handled format are

* ``pkl``: a native python format. It can be used for its efficiency.
  Properties are stored as a python dictionary, each entry corresponding to a
  given property.
* ``xml``: as a text format, it can be easily parsed.
  Properties are enclosed in the element defined by the *data* tag as above.

.. code-block:: xml

  <data>
    ...
  </data>

Properties
----------

Each given property are identified by a *tag* in ``xml`` files, which is also
the *key* for the same properties in the python dictionary stored
in ``pkl`` files.

The command

  .. code-block:: bash

     $ astec_embryoproperties -i file.pkl --print-content

displays the properties stored in a property file. The same stands for ``xml`` files.

In ``pkl`` files, each property is also a dictionary whose keys are cell unique ids,
while in ``xml`` files, each property is an element tagged by *cell* with the
*cell-id* attribute.

Morphometric properties
^^^^^^^^^^^^^^^^^^^^^^^

Morphometric properties, computed by astec, have already been described in
the dedicated astec documentation section
`image sequence properties <https://astec.gitlabpages.inria.fr/astec/astec_properties.html>`_

Ascidian dedicated properties
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``cell_name``: cell name (Conklin's nomenclature)
  Cell names are computed at one time point by ``ascidian_naming_timepoint``
  (cf section :ref:`cli-ascidian-naming-timepoint`)
  and propagated all over the temporal series  by ``ascidian_naming_propagation``
  (cf section :ref:`cli-ascidian-naming-propagation`).

    .. code-block:: xml

      <data>
        ...
        <cell_name>
          ...
          <cell cell-id="100256">'a7.0004_'</cell>

          ...
        </cell_name>
        ...
      </data>

  indicates that cell labeled 256 in image of time 10 is named ``a7.0004_``.


* ``cell_fate``: cell fate depends only on cell name
  (see section :ref:`cli-properties-fates`). It is computed by
  ``ascidian_naming_propagation`` after all cells have been named. It can also
  be recomputed by ``ascidian_embryo``
  (cf section :ref:`cli-ascidian-naming-propagation`).

    .. code-block:: xml

      <data>
        ...
        <cell_fate>
          ...
          <cell cell-id="100256">'Trunk Lateral Cell'</cell>

          ...
        </cell_fate>
        ...
      </data>

  indicates that cell labeled 256 in image of time 10 has ``Trunk Lateral Cell`` as fate.

* ``morphonet`` dedicated properties

  Some computed properties are dedicated for visualisation with
  `morphonet <https://morphonet.org/>`_.
  In ``xml`` files, such elements have a ``mn_type`` attribute
  (whose value is either ``selection`` or ``float``), as in

    .. code-block:: xml

      <data>
        ...
        <tissuefate_lemaire_2009 mn_type="selection">
          ...
          <cell cell-id="100256">[24]</cell>

          ...
        </tissuefate_lemaire_2009>
        ...
      </data>

    The ``tissuefate_lemaire_2009`` property allows to colorize cells
    according to their fate.
    ``ascidian_embryo`` allows to write a morphonet text file
    per morphonet property
    from a property file
    (cf section :ref:`cli-ascidian-naming-propagation`).


.. _cli-properties-fates:

Ascidian cell fates
^^^^^^^^^^^^^^^^^^^

The fates are set for a number of cells. For the other cells

* progeny: fates are just propagated. Eg, both ``a7.1`` and ``a7.2`` will have
  ``Anterior Head Endoderm`` as fate
* ancestors: their fates are the union of progeny fates.
  Eg, ``a6.2`` has both ``1st Lineage, Notochord`` and ``Posterior Ventral Neural Plate``
  as fates.

.. code-block:: none

   a cells
   ├── a6.1 Anterior Head Endoderm
   ├── a6.2
   │  ├── a7.3 1st Lineage, Notochord
   │  └── a7.4 Posterior Ventral Neural Plate
   ├── a6.3
   │  ├── a7.5 Anterior Head Endoderm
   │  └── a7.6 Trunk Lateral Cell
   ├── a6.4
   │  ├── a7.7 1st Lineage, Notochord
   │  └── a7.8
   │     ├── a8.15 Posterior Lateral Neural Plate
   │     └── a8.16
   │        ├── a9.31 2nd Lineage, Tail Muscle
   │        └── a9.32 Posterior Lateral Neural Plate
   ├── a6.5 Anterior Ventral Neural Plate
   ├── a6.6 Head Epidermis
   ├── a6.7
   │  ├── a7.13 Anterior Dorsal Neural Plate
   │  └── a7.14 Head Epidermis
   └── a6.8 Head Epidermis


.. code-block:: none

   b cells
   ├── b6.1
   │  ├── b7.1 Posterior Head Endoderm
   │  └── b7.2
   │     ├── b8.3 Posterior Head Endoderm
   │     └── b8.4 Endodermal Strand
   ├── b6.2
   │  ├── b7.3
   │  │  ├── b8.5 Mesenchyme
   │  │  └── b8.6 2nd Lineage, Notochord
   │  └── b7.4 1st Lineage, Tail Muscle
   ├── b6.3
   │  ├── b7.5
   │  │  ├── b8.9
   │  │  │  ├── b9.17 1st Lineage, Tail Muscle
   │  │  │  └── b9.18 Trunk Ventral Cell
   │  │  └── b8.10
   │  │     ├── b9.19 1st Lineage, Tail Muscle
   │  │     └── b9.20 Trunk Ventral Cell
   │  └── b7.6 Germ Line
   ├── b6.4
   │  ├── b7.7 Mesenchyme
   │  └── b7.8 1st Lineage, Tail Muscle
   ├── b6.5
   │  ├── b7.9
   │  │  ├── b8.17
   │  │  │  ├── b9.33 2nd Lineage, Neural Plate
   │  │  │  └── b9.34 2nd Lineage, Tail Muscle
   │  │  └── b8.18 Midline Tail Epidermis
   │  └── b7.10 Germ Line
   │     ├── b8.19 Posterior Dorsal Neural Plate
   │     └── b8.20 Midline Tail Epidermis
   ├── b6.6 Head Epidermis
   │  ├── b7.11
   │  │  ├── b8.21
   │  │  │  ├── b9.41 Midline Tail Epidermis
   │  │  │  └── b9.42 Medio-Lateral Tail Epidermis
   │  │  └── b8.22
   │  │     ├── b9.43 Medio-Lateral Tail Epidermis
   │  │     └── b9.44 Lateral Tail Epidermis
   │  └── b7.12
   │     ├── b8.23 Medio-Lateral Tail Epidermis
   │     └── b8.24
   │        ├── b9.47 Lateral Tail Epidermis
   │        └── b9.48 Medio-Lateral Tail Epidermis
   ├── b6.7
   │  ├── b7.13
   │  │  ├── b8.25
   │  │  │  ├── b9.49 Medio-Lateral Tail Epidermis
   │  │  │  └── b9.50 Lateral Tail Epidermis
   │  │  └── b8.26 Lateral Tail Epidermis
   │  └── b7.14
   │     ├── b8.27 Midline Tail Epidermis
   │     └── b8.28
   │        ├── b9.55 Medio-Lateral Tail Epidermis
   │        └── b9.56 Midline Tail Epidermis
   └── b6.8
      ├── b7.15 Lateral Tail Epidermis
      └── b7.16
         ├── b8.31
         │  ├── b9.61 Medio-Lateral Tail Epidermis
         │  └── b9.62 Midline Tail Epidermis
         └── b8.32
            ├── b9.63 Medio-Lateral Tail Epidermis
            └── b9.64 Midline Tail Epidermis


------------
Installation
------------
   
Only works for Linux or MacOs systems.

User installation (without git)
===============================

If you do not need to get last update (ie if you have no interaction with developer(s)), this should
be your installation of choice.
Requires ``conda``.

1. Create a conda environment (here the environment is named astec-test)

    .. code-block:: bash

       conda create -n ascidian-test -c morpheme -c conda-forge ascidian
	
2. Activate the built conda environment
   
    .. code-block:: bash

       conda activate ascidian-test

    Update of packages can be done with

    .. code-block:: bash

       conda update -n ascidian-test -c morpheme -c conda-forge ascidian


User installation (with git)
============================

Requires ``conda`` and ``git``.

1. Ascidian code can be found at gitlab.inria.fr/astec/ascidian. It can be downloaded with

    .. code-block:: bash

       git clone https://gitlab.inria.fr/astec/ascidian.git
	
It creates an ascidian directory.

2. Create a conda environment named ascidian

    .. code-block:: bash

       cd ascidian
       conda env create -f pkg/env/ascidian.yaml

3. Activate the built conda environment

    .. code-block:: bash

       conda activate ascidian

Developer installation (with git)
=================================

Requires ``conda`` and ``git``.

1. Ascidian code can be found at gitlab.inria.fr/astec/ascidian. It can be downloaded with

    .. code-block:: bash

       git clone https://gitlab.inria.fr/astec/ascidian.git

    It creates an ``ascidian`` directory.

2. Create a conda environment named ascidian

    .. code-block:: bash

       cd ascidian
       conda env create -f pkg/env/ascidian-dev.yaml
   
3. Activate the built conda environment

    .. code-block:: bash

       conda activate ascidian-dev
   
4. Install ascidian package for use

    .. code-block:: bash

        python -m pip install -e .

    The -e option install the package in "editable" mode, this is want you want if you aim at contributing to
    the ascidian project. This last command has to be repeated (within the conda environment every time the ascidian
    code has been modified).


Documentation update (with git)
===============================

This environment only allows to update the documentation, not to test code changes (use ``ascidian-dev`` then).
Requires ``conda`` and ``git``.

1. Astec code can be found at ``gitlab.inria.fr/astec/ascidian``. It can be downloaded with

    .. code-block:: bash

       git clone https://gitlab.inria.fr/astec/ascidian.git

    It creates an ``ascidian`` directory.

2. Create a conda environment named ``ascidian-doc``

    .. code-block:: bash

       cd ascidian
       conda env create -f pkg/env/ascidian-doc.yaml

3. Activate the built conda environment

    .. code-block:: bash

       conda activate ascidian-doc

4. Build documentation

    Assume you are in the ``ascidian`` directory created at step 1:

    .. code-block:: bash

        cd doc
        make html

    The documentation will be built into the ``ascidian/doc/build/`` directory, and can be accessed
    through ``ascidian/doc/build/html/index.html`` with your favorite browser.
    Documentation can also be built within the ``ascidian-dev`` conda environment.

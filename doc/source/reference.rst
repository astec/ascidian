Reference API
=============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: ascidian.core.contact_surface_distance
  :members:

.. automodule:: ascidian.core.icp
  :members:

.. automodule:: ascidian.core.temporal_alignment
  :members:

.. automodule:: ascidian.core.vector_distribution
   :members:

.. automodule:: ascidian.symmetry.embryo
   :members:
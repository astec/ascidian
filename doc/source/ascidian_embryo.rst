.. role:: python(code)
   :language: python

.. _cli-ascidian-embryo:

``ascidian_embryo``
===================

``ascidian_embryo`` can be used to assess the quality/coherency of a set of already 
named ascidian embryos (or atlases) as well to point out potential corrections.
It assumes that the set (or mathematically speaking the vector) of valued surface
contact of a named cell can be used as its signature.


``ascidian_embryo`` can be used to

* either, with a parameter file, assess the quality/coherency of a set of already
  named ascidian embryos (or atlases) as well to point out potential corrections;
  it assumes that the set (or mathematically speaking the vector) of valued surface
  contact of a named cell can be used as its signature;
* or manipulate a property file (pkl or xml), in order to
  * convert an xml file into a pkl one (or conversely)
  * extract some features from a property file
  * compute cell fates from cell names
  * ...


``ascidian_embryo`` additional options
--------------------------------------

The following options are available:

``-i files ...``
   input files (pkl or xml) to be read

``-o files ...``
   output files (pkl or xml) to be read

``-c files ...``
   files (pkl or xml) to be compared to those given by ``-i``

``-feature features ...``
   features to be extracted from the input files, that are to be
   written in the output files. Features have to be chosen in
   'lineage',  'h_min', 'volume', 'surface', 'sigma',
   'label_in_time', 'barycenter', 'fate',
   'all-cells', 'principal-value',
   'name', 'contact', 'history', 'principal-vector',
   'name-score', 'cell-compactness'

``-property features ...``
   same as ``-feature``

``-write-selection | --write-selection | -write-selections | --write-selections``
  write out morphonet selection files

``-fate | --compute-fate``
  delete previous cell fate (if any) and recompute it, as well as a morphonet
  selection for cell colorization.

``-name-map, --compute-name-map``
  compute name map for morphonet

``--diagnosis``
   performs some test on the read properties

``--diagnosis-minimal-volume DIAGNOSIS_MINIMAL_VOLUME``
   displays all cells with volume smaller than ``DIAGNOSIS_MINIMAL_VOLUME``

``--diagnosis-items DIAGNOSIS_ITEMS``
   minimal number of items to be displayed

``-write-selection, --write-selection``
   convert xml selections into morphonet files

``--print-content``
   print the keys of the input file(s) (read as python dictionary)

``--print-keys``
   same as ``--print-content``

``--print-types``
   print types of read features (for debug purpose)


Assess the quality of a set of named ascidian embryos
-----------------------------------------------------

Each individual cell :math:`c^{R}_i` (the cell :math:`c_i` of atlas :math:`R`) is represented by the
vector of its contact surfaces
:math:`c^{R}_i = \left( \begin{array}{c} 
s^{R}_{i,1} \\
\vdots \\
s^{R}_{i,j} \\
\vdots
\end{array}
\right)`.

From the cell-to-cell distance, a division-to-division similarity can be built, a division being represented by 
the couple of daughter cells (extracted at the distance ``delay_from_division`` from the division).

To enrich division exemplars, the symmetric division (of the other hemi-embryo) can be symmetrized
(ie ``a7.0002_`` will be changed in ``a7.0002*``). This is governed by ``add_symmetric_neighborhood``.

Thus, to assess the quality of a set of atlases, a typical parameter file may be

.. code-block:: python

   atlasFiles = []
   atlasFiles += ['/path_to_reference_embryos/Astec-pm1.pkl']
   atlasFiles += ['/path_to_reference_embryos/Astec-pm3.pkl']
   atlasFiles += ['/path_to_reference_embryos/Astec-pm4.pkl']
   atlasFiles += ['/path_to_reference_embryos/Astec-pm5.pkl']
   atlasFiles += ['/path_to_reference_embryos/Astec-pm7.pkl']
   atlasFiles += ['/path_to_reference_embryos/Astec-pm8.pkl']
   atlasFiles += ['/path_to_reference_embryos/Astec-pm9.pkl']
   #
   # how to select cells
   #
   add_symmetric_neighborhood = True
   use_common_neighborhood = True
   delay_from_division = 3
   #
   # how to extract/display information
   #
   atlas_diagnosis = True
   division_diagnosis = True
   division_permutation_proposal = True
   generate_figure = True
   figurefile_suffix = 'some_suffix'

* ``atlas_diagnosis`` and ``division_diagnosis`` may be quite verbose.
  It may be adviced to set them to ``True`` when
  introducing a new atlas, but not when using a set of already curated atlases.
  Two kinds of diagnosis are conducted.

  * ``atlas_diagnosis``

    * on each single atlas/reference file, the ``name`` and the ``contact`` properties are assessed. 

    * on the population of division neighborhoods:

  * ``division_diagnosis``

    * pairwise disagreements: for a given cell and every couple of reference embryos, 
      the distance of the two divisions (one per reference) is compared to the distance
      of one division compared to the other being switched. If the later is prefered, 
      it is denoted as a disagreement.
    * linkage/dendrogram analysis: it is checked whether adding the switched divisions
      to the set of divisions changes the largest value of cluster distance in
      a dendrogram. If yes, it also suggest that some divisions may be switched.
      Individualized morphonet selection files are written (if ``write_selection`` is set to ``True``)
      in the ``outputDir`` directory.


* ``division_permutation_proposal`` may propose to switch the daughter names of some divisions. It calculates
  whether a name switch result in a global score improvement, and, if so, proposes the switch.
  It is somehow computationally costly.
  Individualized ``morphonet`` selection files are written (if ``write_selection`` is set to ``True``)
  in the ``outputDir`` directory.

* ``generate_figure`` will generate python files (in the ``outputDir`` directory) 
  that, when executed, generates some figures.
  It is somehow computationally costly.


Section :ref:`cli-parameters-naming-propagation` provides a view on all the parameters.


Handling property files
-----------------------

Computing cell fates
^^^^^^^^^^^^^^^^^^^^

  .. code-block:: bash

     $ ascidian_atlas -i input.pkl -o output.pkl -fate

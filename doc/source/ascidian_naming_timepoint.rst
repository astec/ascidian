.. role:: python(code)
   :language: python

.. _cli-ascidian-naming-timepoint:

``ascidian_naming_timepoint``
=============================

``ascidian_naming_timepoint`` can be used to name one time point of an ascidian embryo 
(for which the lineage and the contact surfaces are known), by name transfer
from a database of named embryos (atlases).

The targeted time point is chosen by its cell count (variable ``cell_number``).
Thus, it is more convenient to
choose a stage for which the cell composition is the same among all embryos
(ie the 64-, 76- or 112-cells).

Then, each atlas (at the same targeted cell count) is co-registered with the embryo
to be named, and cells of the atlas are paired with the ones of the unnamed embryos,
yielding name candidates. Cells are paired when, after registration, their centers of mass
are the closest to each other. It may then happen that some cells are not paired.

If several atlases are given, each unnamed cell may have a list of candidate names.
An iterative voting procedure aloows then to propose names for the unnamed embryo.

Section :ref:`cli-parameters-naming-timepoint` provides a view on all the parameters.

``ascidian_naming_timepoint`` additional options
------------------------------------------------

The following options are available:

``-write-selection, --write-selection``
   write out ``morphonet`` selection files.


Initial naming of an embryo
---------------------------

To name an embryo, the minimal parameter file has to contain:

* the input property file, containing the lineage, the contact surfaces, and the volumes.

* the output property file,

* the list of atlases (already named embryos),

* the cell number of the time point to be named (typically 64 or 112)

.. code-block:: python

   inputFile = 'property_file_partially_named.xml'
   outputFile = 'property_file_named_after_the_atlases.xml'

   atlasEmbryos = []
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm1.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm3.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm4.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm5.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm7.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm8.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm9.pkl']

   cell_number = 64

Section :ref:`cli-parameters-naming-timepoint` provides a view on all the parameters.

Initial naming: parameter file examples
---------------------------------------

Use of ``transformation_filename``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Most of the computation time is spend in co-registering embryos,
thus in transformation computation.

When adding progressively the atlases in ``atlasEmbryos``,
or when selecting retrospectively only some atlases,
it is desirable
not to recompute at every experiment all the transformations.
The ``transformation_filename`` variable allows to specify a file name (pickle format)
where they can be saved for further re-use.

This first parameter file will name the embryo summarized in
``'property_file_partially_named.xml'`` with the two atlases
``'Astec-pm1.pkl'`` and ``'Astec-pm3.pkl'``.
Computed transformations will be saved in file ``'trsf.pkl'``.


.. code-block:: python

   inputFile = 'property_file_partially_named.xml'
   outputFile = 'property_file_named_after_the_atlases.xml'

   atlasEmbryos = []
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm1.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm3.pkl']

   transformation_filename = 'trsf.pkl'

   cell_number = 64

In this second parameter file, atlases are enriched with new
exemplars. Since we provided the already omputed transformations
in ``transformation_filename``, co-registrations with
``'Astec-pm1.pkl'`` and ``'Astec-pm3.pkl'`` will not be re-calculated.
In addition, ``'trsf.pkl'`` will be updated and will contain
all co-registration transformations.

.. code-block:: python

   inputFile = 'property_file_partially_named.xml'
   outputFile = 'property_file_named_after_the_atlases.xml'

   atlasEmbryos = []
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm1.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm3.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm4.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm5.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm7.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm8.pkl']
   atlasEmbryos += ['/path_to_reference_embryos/Astec-pm9.pkl']

   transformation_filename = 'trsf.pkl'

   cell_number = 64

Initial naming: Morphonet dedicated generated field
---------------------------------------------------

Generated morphonet dedicated properties (in the property file) are

* ``name_choice_agreement`` (``mn_type`` is "float")

* ``name_choice_min_distance`` (``mn_type`` is "float")

* ``name_choice_min_distance_homogeneize`` (``mn_type`` is "float")
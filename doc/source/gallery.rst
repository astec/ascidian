
.. _cli-gallery:

Gallery
=======

Embryo set assessment
---------------------



.. _cli-gallery-temporal-alignment:

Temporal alignment of embryo development
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Embryo development can be temporally aligned. Alignment is computed from the cell
count during development, ie the linear time warping that superimposed
the cell counts of two embryos during their development is computed.
Mathematically speaking, it comes to compute
:math:`(\hat{a},\hat{b}) = \arg \ min _{a,b} \sum_n \left( a t_f(n) +b - t_r(n)\right)^2`
where :math:`n` is a cell count,
:math:`t_f(n)` is the acquisition time where the floating embryo has :math:`n` cells
while :math:`t_r(n)` is the acquisition time where the reference embryo has :math:`n` cells.

Running

.. code-block:: bash

    ascidian_embryo -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    generate_figure = ['temporal-alignment']

generates a ``python`` file allowing to generate the following figure. The reference embryo
is either specified with the variable ``referenceEmbryo`` or it is
the first element of the list ``atlasEmbryos``.

.. _fig_temporal_alignment_1:
.. figure:: ./latex/figures/temporal_alignment_cell_count_1.png
   :alt: Cell count wrt acquisition time point (ie image indices) and developemental time.
   :width: 500px

   Cell count wrt acquisition time point (ie image indices) and developemental time (ie after temporal alignment).

.. _fig_temporal_alignment_2:
.. figure:: ./latex/figures/temporal_alignment_cell_count_2.png
   :alt: Cell count wrt developemental time (range and average)
   :width: 500px

   Cell count wrt acquisition time point (ie image indices) and developemental time (ie after temporal alignment).

Using the count of all cells implicitly assumes that the development is not
perturbed. When adding the MEK inhibitor `U0126 <https://en.wikipedia.org/wiki/U0126>`_,
that impeach the invagination, the development is no
more following a normal scheme. Since U0126 is not perturbing the epidermis cell,
the temporal normalization can be done with the count of epidermis cells only
as exemplified here.

Running

.. code-block:: bash

    ascidian_embryo -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    generate_figure = ['temporal-alignment-epidermis']

generates a ``python`` file allowing to generate the following figures.


.. _fig_temporal_alignment_epidermis_1:
.. figure:: ./latex/figures/temporal_alignment_epidermis_1_02.png
   :alt: Epidermis cell count wrt acquisition time point (ie image indices) and developemental time.
   :width: 500px

   Epidermis cell count wrt acquisition time point (ie image indices) and developemental time
   (ie after temporal alignment).
   Temporal alignment is done wrt epidermis cell counts.

.. _fig_temporal_alignment_epidermis_2:
.. figure:: ./latex/figures/temporal_alignment_epidermis_2_02.png
   :alt: Epidermis cell and all cell count wrt acquisition time point (ie image indices) and developemental time.
   :width: 500px

   All cell and epidermis cell count wrt acquisition time point (ie image indices) and developemental time
   (ie after temporal alignment).
   Temporal alignment is done wrt epidermis cell counts.

.. _fig_temporal_alignment_epidermis_3:
.. figure:: ./latex/figures/temporal_alignment_epidermis_3_02.png
   :alt: Epidermis cell count wrt acquisition time point (ie image indices) and developemental time.
   :width: 500px

   Temporally aligned cell count wrt acquisition time point for different cell fates.
   Temporal alignment is done wrt epidermis cell counts.

Note that the temporal alignment based on epidermis cells
can be achieved by setting the variable ``temporal_alignment_fate``
to ``'Epidermis'`` in a parameter file, ie by adding the line

.. code-block:: python

    temporal_alignment_fate = 'Epidermis'

in the parameter file.



.. _cli-gallery-volume-normalization:

Volume normalization of embryos
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The embryo volume slightly decreases during the development, and embryos
may have different volumes.
To compare morphometric features across embryos, their size have to be
homogenized/normalized. A robust linear fit is done on embryo volumes to get
a scaling correction (that varies with time). Normalization is achieved
by setting the same target volume for all embryos.

Running

.. code-block:: bash

    ascidian_embryo -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasFiles = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    generate_figure = ['volume-fitting']

generates a ``python`` file allowing to generate the following figures.

.. _fig_volume_fitting_volumes:
.. figure:: ./latex/figures/volume_fitting_volumes_03.png
   :alt: Embryo volume temporal variation
   :width: 500px

   Embryo volume temporal variation with linear fit (left and middle: without and with temporal
   alignment). Right: volume normalization to the same target volume.


.. _fig_volume_fitting_surfaces:
.. figure:: ./latex/figures/volume_fitting_surfaces_03.png
   :alt: Embryo surface temporal variation
   :width: 500px

   Embryo external surface temporal variation (left and middle: without and with temporal
   alignment). Right: surface normalization computed from the volume normalization.



.. _cli-gallery-cell-composition:

Cell composition
^^^^^^^^^^^^^^^^

The variability of cell composition (ie cell names) indirectly measures the heterochrony
of cell division.
It is assessed by computing the cells that have the same name across the set of embryos
and the cells that have different names, after temporal alignment of all embryos.

Running

.. code-block:: bash

    ascidian_embryo -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasFiles = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    generate_figure = ['cell-composition']

generates a ``python`` file allowing to generate the following figures.

.. _fig_cell_composition:
.. figure:: ./latex/figures/cell_composition_1_04.png
   :alt: Temporally aligned cell count and cell variability
   :width: 500px

   Temporally aligned cell count and cell variability in one plot


.. _fig_temporal_alignment_name_composition_2:
.. figure:: ./latex/figures/cell_composition_2_04.png
   :alt: Temporally aligned cell count and cell variability
   :width: 500px

   Temporally aligned cell count and cell composition difference in different plots.
   Cell composition difference also indicates the round of division.



.. _cli-gallery-cell-neighbor-count:

Cell neighbor count
^^^^^^^^^^^^^^^^^^^

The following
figures assessed whether the neighbor count is stable during development.

Running

.. code-block:: bash

    ascidian_embryo -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasFiles = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    generate_figure = ['neighbors-wrt-cell-number']

generates a ``python`` file allowing to generate the following figures.

.. _fig_cell_neighbor_count_1:
.. figure:: ./latex/figures/cell_neighbor_count_1_05.png
   :alt: Average and standard deviation of cell neighbor counts
   :width: 500px

   Box plots of cell neighbor counts



.. _fig_cell_neighbor_count_2:
.. figure:: ./latex/figures/cell_neighbor_count_2_05.png
   :alt: Box plots of cell neighbor counts
   :width: 500px

   Average and standard deviation of cell neighbor counts



.. _cli-gallery-division-time-differences:

Division time differences
^^^^^^^^^^^^^^^^^^^^^^^^^

We study here whether the division are more synchronized within an embryo (between
the left and the right part) than between different embryos.


Running

.. code-block:: bash

    ascidian_embryo -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasFiles = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    generate_figure = ['division-time-difference']

generates a ``python`` file allowing to generate the following figure.

.. _fig_division_time_difference:
.. figure:: ./latex/figures/division_time_difference_06.png
   :alt: Time interval of cell division
   :width: 500px

   Left: (developmental) time difference between the division of the
   right cell and the left one (intra-embryo difference);
   Right: time difference between the division of the same cell, or right and
   left cells, from different embryos (inter-embryo difference).
   Since generation 10 cell divisions are mostly in Pm1, there are not many
   measurements for generation 10 inter-embryo differences.



.. _cli-gallery-division-time-mis-ordering-cell:

Division time mis-ordering: couple of division view
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We study here whether the order of division is preserved across embryos. To do so,
we first compute a "natural" division order (from the average developmental
division time computed from all embryos), and then compute a probability
that this natural order is not respected.

Running

.. code-block:: bash

    ascidian_embryo -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasFiles = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    generate_figure = ['division-time-couple-misordering']

generates a ``python`` file allowing to generate the following figures

.. _fig_division_time_couple_misordering_a70003_a70004_1:
.. figure:: ./latex/figures/division_time_couple_misordering_a70003_a70004_1_07.png
   :alt: Probability of mis-ordering for the couple of cells a7.0003 and a7.0004
   :width: 500px

   Joint division time distribution for the couple of cells ``a7.0003`` and ``a7.0004``.
   Left: couple of division times for the different embryos ('*' denotes
   the right part, while '_' denotes the left part); right:
   kernel based joint density estimation. Here, the probability of mis-ordering is low.

.. _fig_division_time_couple_misordering_b70007_b80020_1:
.. figure:: ./latex/figures/division_time_couple_misordering_b70007_b80020_1_07.png
   :alt: Probability of mis-ordering for the couple of cells b7.0007 and b8.0020
   :width: 500px

   Joint division time distribution for the couple of cells ``b7.0007`` and ``b7.0020``.
   Left: couple of division times for the different embryos ('*' denotes
   the right part, while '_' denotes the left part); right:
   kernel based joint density estimation. Here, the probability of mis-ordering is high,
   due to the bimodal division time distribution of ``b7.0007``.

.. _fig_division_time_couple_misordering_b70007_b80020_2:
.. figure:: ./latex/figures/division_time_couple_misordering_b70007_b80020_2_07.png
   :alt: Probability of mis-ordering for the couple of cells b7.0007 and b8.0020
   :width: 500px

   Joint division time distribution for the couple of cells ``b7.0007`` and ``b7.0020``,
   together with division time distribution for each cell.
   It exemplifies the bimodal division time distribution of ``b7.0007``.



.. _cli-gallery-division-time-mis-ordering-global:

Division time mis-ordering: global view
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We study here whether the order of division is preserved across embryos. To do so,
we collect the probabilty of mis-ordering (see above)
and present a global view of it.

Running

.. code-block:: bash

    ascidian_embryo -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasFiles = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    generate_figure = ['division-time-global-misordering']

generates a ``python`` file allowing to generate the following figures

.. _fig_division_time_global_misordering_zoom_0:
.. figure:: ./latex/figures/division_time_global_misordering_zoom_0_08.png
   :alt: Probabilities of mis-ordering with respect to "natural" order
   :width: 500px

   Probabilities of mis-ordering divisions with respect to "natural" order
   from division of cell ``a6.0004`` to ``b9.0050``.

.. _fig_division_time_global_misordering_zoom_1:
.. figure:: ./latex/figures/division_time_global_misordering_zoom_1_08.png
   :alt: Probabilities of mis-ordering with respect to "natural" order
   :width: 500px

   Probabilities of mis-ordering divisions with respect to "natural" order
   from division of cell ``a7.0008`` to ``a7.0006``, ie from the 64-cells
   stage tp the 112-cells stage.

.. _fig_temporal_alignment_average_zoom_0:
.. figure:: ./latex/figures/temporal_alignment_average_zoom_0_08.png
   :alt: Aligned cell counts + counts from average division times
   :width: 500px

   Temporally aligned cell counts
   + cell counts from a virtual embryo whose cells divide at their average
   division time,
   in the normalized
   (developmental) time interval [-10, 50].



Embryo symmetry assessment
--------------------------



.. _cli-gallery-symmetry-axis:

Detection of the symmetry axis
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Symmetry axis was first investigated by G. Michelin :cite:p:`michelin:hal-01113166`
(see also :cite:p:`michelin:tel-01451608`) where the
symmetry axis (the left-right symmetry) is computed as a mode
in the distribution of (orthogonal) directions
of cell-to-cell membranes (issued from images).
Mimicking this approach with data issued from the
property file did not performed well (see :cite:p:`malandain:hal-04609482`).

However, using the contact surfaces of a cell as its signature
offers a means to recognize its symmetrical cell.
The symmetry axis can then be as a mode of the distribution
of vectors joining a pair of symmetrical cells :cite:p:`malandain:hal-04609482`.

We investigated two cell signatures between cells :math:`c_k` and :math:`c_\ell`
from the same embryo, inspired by the cell distance introduced for embryo naming
(equation :eq:`eq:cell-naming-distance`).

.. math::
   d(c_k, c_\ell ; P)  = \frac{\sum_{(k_i, \ell_j) \in P} |s_{k,k_i} - s_{\ell,\ell_j} |}{\sum_{i_k} s_{k,i_k} +
   \sum_{j_\ell} s_{\ell,j_\ell}}
   :label: eq:cell-sorted-distance

However, we do not have the cell names to build the pairing :math:`P`. Then, to compare
two cells, we use the optimal pairing, ie the one giving the minimal cell-to-cell distance:
it comes to pair the largest contact surface of cell :math:`c_k` with the
largest contact surface of cell :math:`c_\ell` and so on and so forth.
We denote this cell similarity as ``'sorted-contact'``.


Running

.. code-block:: bash

    ascidian_embryo -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl', 'U0126-Pm1.pkl', 'U0126-Pm2.pkl']
    temporal_alignment_fate = 'Epidermis'
    generate_figure = ['symmetry-axis-assessment']
    cell_similarity = 'sorted-contact'

generates a ``python`` file allowing to generate the following figures

.. _fig_symmetry_axis_assessment_10_sorted:
.. figure:: ./latex/figures/symmetry_axis_assessment_10_sorted.png
   :alt: Symmetry axis detection assessment with symmetrical cells recohnized with the sorted contacts
   :width: 500px

   The Y axis is the developmental time (aligned acquisition times).
   Notice ``temporal_alignment_fate = 'Epidermis'`` that
   allows to properly align cell counts for U0126 embryos.
   Symmetrical cells are computed with only the
   cell contact surfaces (``cell_similarity = 'sorted-contact'``).
   Left: cell counts. Middle: symmetry axis error, ie angle between
   ground truth, and the closest distribution mode.
   Right: rank of the closest mode.
   See :cite:p:`malandain:hal-04609482` for details.


However the ``'sorted-contact'`` cell similarity does not take into account the
neighbor spatial ordering. Considering now the unit vector
:math:`\mathbf{n}_{k, k_i}` from cell :math:`c_k` to cell :math:`c_{k_i}`
(eg derived from the cell centers of mass), the above cell similarity can be
complexified into

.. math::
   d_v(c_k, c_\ell ; P)  = \frac{\sum_{(k_i, \ell_j) \in P}
   \| s_{k,k_i} \mathbf{n}_{k, k_i} - s_{\ell,\ell_j} \mathbf{n}_{\ell, \ell_j} \|}
   {\sum_{i_k} s_{k,i_k} + \sum_{j_\ell} s_{\ell,j_\ell}}
   :label: eq:cell-winged-distance

The optimal pairing comes to solve the minimum weight matching problem in a bipartite graph,
also known as
`linear assignment problem <https://en.wikipedia.org/wiki/Assignment_problem>`_.
To that end, we use the scipy
`linear_sum_assignment() <https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.linear_sum_assignment.html>`_
function.

When it comes to detect symetrical cells, it has to be adapted to effectively
compare a cell with its potential symmetrical counterpart
(see :cite:p:`malandain:hal-04609482` for details).
We denote this cell similarity as ``'winged-contact'``.

Running

.. code-block:: bash

    ascidian_embryo -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl', 'U0126-Pm1.pkl', 'U0126-Pm2.pkl']
    temporal_alignment_fate = 'Epidermis'
    generate_figure = ['symmetry-axis-assessment']
    cell_similarity = 'winged-contact'

generates a ``python`` file allowing to generate the following figures

.. _fig_symmetry_axis_assessment_10_winged:
.. figure:: ./latex/figures/symmetry_axis_assessment_10_winged.png
   :alt: Symmetry axis detection assessment with symmetrical cells recohnized with the winged contacts
   :width: 500px

   The Y axis is the developmental time (aligned acquisition times).
   Notice ``temporal_alignment_fate = 'Epidermis'`` that
   allows to properly align cell counts for U0126 embryos.
   Symmetrical cells are computed with the
   cell contact surfaces and the contact vectors
   (``cell_similarity = 'winged-contact'``).
   Left: cell counts. Middle: symmetry axis error, ie angle between
   ground truth, and the closest distribution mode.
   Right: rank of the closest mode.
   See :cite:p:`malandain:hal-04609482` for details.



.. _cli-gallery-symmetrical-cells:

Detection of symmetrical cells
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The detection of the symmetry axis is based on
the recognition of pairs of symmetrical cells (apart of mimicking
G. Michelin's method :cite:p:`michelin:hal-01113166`).

This recognition is founded on a similarity measure between
cell neighborhood, based either
solely on contact surfaces
(``cell_similarity = 'sorted-contact'``)
or on contact surfaces and vectors
(``cell_similarity = 'winged-contact'``) with a
little bit higher computational cost.

These two measures can be assessed
by running

.. code-block:: bash

    ascidian_embryo -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl', 'U0126-Pm1.pkl', 'U0126-Pm2.pkl']
    temporal_alignment_fate = 'Epidermis'
    generate_figure = ['symmetrical-cell-percentage']

that generates a ``python`` file allowing to generate the following figures
(one per embryo)

.. _fig_symmetrical_cell_percentage_Pm1:
.. figure:: ./latex/figures/symmetrical_cell_percentage_Pm1_09.png
   :alt: Symmetrical cell detection estimation
   :width: 500px

   Symmetrical cell detection estimation for embryo Pm1.

.. _fig_symmetrical_cell_percentage_Pm4:
.. figure:: ./latex/figures/symmetrical_cell_percentage_Pm4_09.png
   :alt: Symmetrical cell detection estimation
   :width: 500px

   Symmetrical cell detection estimation for embryo Pm4.

.. _fig_symmetrical_cell_percentage_Pm8:
.. figure:: ./latex/figures/symmetrical_cell_percentage_Pm8_09.png
   :alt: Symmetrical cell detection estimation
   :width: 500px

   Symmetrical cell detection estimation for embryo Pm8.

.. _fig_symmetrical_cell_percentage_U0126_Pm1:
.. figure:: ./latex/figures/symmetrical_cell_percentage_U0126_Pm1_09.png
   :alt: Symmetrical cell detection estimation
   :width: 500px

   Symmetrical cell detection estimation for embryo Pm1 pertubed with U0126.




Naming one time point
---------------------

Naming one time point by embryo co-registration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To name an embryo at a given time point, atlases  (embryos already named) are co-registered
with the embryo to be named, cell-to-cell correspondences
(by pairing closest cell centers of mass together) are established
after registration, and a final voting procedure allows to set names.

The cell-to-cell correspondences implies implicitly that the cell composition is
identical through the embryo population, thus it is preferable to choose a time point at
64, 76 or 112 cells.

Running

.. code-block:: bash

    ascidian_naming_timepoint -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl']
    cell_number = 64
    generate_figure = ['naming_timepoint_wrt_atlases']

generates a ``python`` file allowing to generate the following figure,
depicting the results of naming where one embryo is named after a set built
from the other embryos (for a time point where embryos have 64 cells).
Results seem to improve with the number of atlases. However, using adding symmetrical atlases
do not seem to bring improvement.

.. _fig_naming_timepoint_wrt_atlases_wrong_11_064_134578_nonsym_false:
.. figure:: ./latex/figures/naming_timepoint_wrt_atlases_wrong_11_064_134578_nonsym_false.png
   :alt: Box plots of right named cells and wrong named cells wrt the number of atlases (leave-one-out tests)
   :width: 500px

   Box plots of right named cells and wrong named cells wrt the number of atlases (leave-one-out tests)
   for registration-based naming at 64 cells.

Modifying the above parameter file ( and adding the ``'Pm9.pkl'`` embryo)
allows to get the following figures for the 76 and 112 cell counts:
some (few) cells remain unnamed (figures depicting the number
of unnamed cells are also generated, but not shown here).

.. _fig_naming_timepoint_wrt_atlases_wrong_11_076_134578_nonsym_false:
.. figure:: ./latex/figures/naming_timepoint_wrt_atlases_wrong_11_076_134578_nonsym_false.png
   :alt: Box plots of right named cells and wrong named cells wrt the number of atlases (leave-one-out tests)
   :width: 500px

   Box plots of right named cells and wrong named cells wrt the number of atlases (leave-one-out tests)
   for registration-based naming at 76 cells.

.. _fig_naming_timepoint_wrt_atlases_wrong_11_112_134578_nonsym_false:
.. figure:: ./latex/figures/naming_timepoint_wrt_atlases_wrong_11_112_134578_nonsym_false.png
   :alt: Box plots of right named cells and wrong named cells wrt the number of atlases (leave-one-out tests)
   :width: 500px

   Box plots of right named cells and wrong named cells wrt the number of atlases (leave-one-out tests)
   for registration-based naming at 112 cells.


Running

.. code-block:: bash

    ascidian_naming_timepoint -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl']
    generate_figure = ['naming_timepoint_wrt_cellcounts']

generates a ``python`` file allowing to generate the following figure,
depicting the results of naming where one embryo is named after a set built
from all the other available (at the given cell count) embryos
for different cell counts. It illustrates the difficulty to name,
with the implemented registration-based procedure, an embryo at time points
where the cell composition is heteregeneous.

.. _fig_naming_timepoint_wrt_cellcounts_wrong_1345789_nonsym_false:
.. figure:: ./latex/figures/naming_timepoint_wrt_cellcounts_wrong_1345789_nonsym_false.png
   :alt: Box plots of right named cells and wrong named cells wrt cell counts (leave-one-out tests)
   :width: 500px

   Box plots of right named cells and wrong named cells wrt cell counts (leave-one-out tests)
   for registration-based naming.

.. _fig_naming_timepoint_wrt_cellcounts_unnamed_1345789_nonsym_false:
.. figure:: ./latex/figures/naming_timepoint_wrt_cellcounts_unnamed_1345789_nonsym_false.png
   :alt: Box plots of right named cells and unnamed cells wrt cell counts (leave-one-out tests)
   :width: 500px

   Box plots of right named cells and unnamed cells wrt cell counts (leave-one-out tests)
   for registration-based naming.


Naming one time point by cell signature
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cell signatures are efficient to characterize the symmetrical counter-part of
a cell within the same embryo (:ref:`cli-gallery-symmetrical-cells`).
It can then be questioned whether they can be used to recognize the same cell across embryos.
Indeed, the registration strategy does not work for perturbed development
(U0126 embryos).

Equation :eq:`eq:cell-sorted-distance` can be used to compare cells across embryos
(according that embryo volumes have been normalized, so that contact surfaces are comparable).

Running

.. code-block:: bash

    ascidian_naming_timepoint -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    add_symmetric_embryo = True
    matching_cell_similarity = 'sorted-contact'
    generate_figure = ['cell-signature']

generates a ``python`` file allowing to generate the following figure,
that assesses whether the cell signature is able to retrieve the same cell
across embryos.
For a given embryos, each cell is compared to all other cells in all
other embryos (including symmetrized ones), cells are sorted according to
the distance, and the rank of the same cell is plotted (1 is the best).

depicting the results of naming where one embryo is named after a set built
from all the other available (at the given cell count) embryos
for different cell counts. It illustrates the difficulty to name,
with the implemented registration-based procedure, an embryo at time points
where the cell composition is heteregeneous.

.. _fig_cell_signature_rank_boxplot_17_1345789_sorted_contact:
.. figure:: ./latex/figures/cell_signature_rank_boxplot_17_1345789_sorted_contact.png
   :alt: Box plots of ranks
   :width: 500px

   While comparing cells across embryos, rank of the corresponding cell (cell of same name)
   in other embryos with the ``'sorted-contact'`` cell-to-cell distance.

However, the ``'sorted-contact'`` cell-to-cell distance do not allow to differentiate between
left and right cells, while the ``'winged-contact'`` one
(see equation :eq:`eq:cell-winged-distance`)
could do so. However, to compare vectors, one have to express them in the same referential, as in

.. math::
   d_v(c_k, c_\ell ; P)  = \frac{\sum_{(k_i, \ell_j) \in P}
   \| s_{k,k_i} \mathbf{n}_{k, k_i} - s_{\ell,\ell_j} \mathbf{R}_{sym} \mathbf{R}(\theta)
   \mathbf{n}_{\ell, \ell_j} \|}{\sum_{i_k} s_{k,i_k} + \sum_{j_\ell} s_{\ell,j_\ell}}

Here, the (local) registration between vectors issued
from cells :math:`c_k` and :math:`c_\ell` is achieved
by the rotation matrix :math:`\mathbf{R}_{sym} \mathbf{R}(\theta)` where
the rotation matrix :math:`\mathbf{R}_{sym}` aligns the symmetry axis
of both embryos and :math:`\mathbf{R}(\theta)` is a 2D rotation along one symmetry axis.
The optimal pairing is then computed for each 2D rotation and the final distance
is the minimal one over the angle of 2D rotations.

Running

.. code-block:: bash

    ascidian_naming_timepoint -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    add_symmetric_embryo = True
    matching_cell_similarity = 'winged-contact'
    z_rotation_angle_increment = 15
    generate_figure = ['cell-signature']

generates a ``python`` file allowing to generate the following figure,
that assesses whether the cell signature is able to retrieve the same cell
across embryos.

.. _fig_cell_signature_rank_boxplot_17_1345789_winged_contact:
.. figure:: ./latex/figures/cell_signature_rank_boxplot_17_1345789_winged_contact.png
   :alt: Box plots of ranks
   :width: 500px

   While comparing cells across embryos, rank of the corresponding cell (cell of same name)
   in other embryos with the ``'winged-contact'`` cell-to-cell distance.

.. _fig_cell_signature_rotation_angle_Pm1_17_1345789_winged_contact:
.. figure:: ./latex/figures/cell_signature_rotation_angle_Pm1_17_1345789_winged_contact.png
   :alt: Angles
   :width: 500px

   While comparing cells from ``Pm1``  to other embryos, angle of the optimal angle :math:`\theta`.


Running

.. code-block:: bash

    ascidian_naming_timepoint -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl']
    cell_number = 64
    minimal_consensus = 60
    naming_by_transfer = 'assignment'
    matching_cell_similarity = 'sorted-contact'
    generate_figure = ['naming_timepoint_wrt_atlases']

generates a ``python`` file allowing to generate the following figure,
depicting the results of naming where one embryo is named after a set built
from the other embryos (for a time point where embryos have 64 cells), where
cell pairings is issued from linear assignment (minimum weight matching problem in a bipartite graph)
with a cost matrix built from ``'sorted-contact'`` similarity. Note that

.. _fig_naming_timepoint_wrt_atlases_wrong_assign_sorted_060:
.. figure:: ./latex/figures/naming_timepoint_wrt_atlases_wrong_assign_sorted_060.png
   :alt: Box plots of right named cells and wrong named cells wrt the number of atlases (leave-one-out tests)
   :width: 500px

   Box plots of right named cells and wrong named cells wrt the number of atlases (leave-one-out tests)
   for assignment-based naming at 64 cells ('sorted-contact') similarity.

Changing the similarity for the 'winged-contact' does improve the naming.
Indeed, running

.. code-block:: bash

    ascidian_naming_timepoint -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl']
    cell_number = 64
    minimal_consensus = 60
    naming_by_transfer = 'assignment'
    matching_cell_similarity = 'winged-contact'
    generate_figure = ['naming_timepoint_wrt_atlases']

generates a ``python`` file allowing to generate the following figure,
depicting the results of naming where one embryo is named after a set built
from the other embryos (for a time point where embryos have 64 cells), where
cell pairings is issued from linear assignment (minimum weight matching problem in a bipartite graph)
with a cost matrix built from ``'winged-contact'`` similarity. Note that

.. _fig_naming_timepoint_wrt_atlases_wrong_assign_winged_060:
.. figure:: ./latex/figures/naming_timepoint_wrt_atlases_wrong_assign_winged_060.png
   :alt: Box plots of right named cells and wrong named cells wrt the number of atlases (leave-one-out tests)
   :width: 500px

   Box plots of right named cells and wrong named cells wrt the number of atlases (leave-one-out tests)
   for assignment-based naming at 64 cells ('sorted-contact') similarity.




Contact surface based cell-to-cell distance
-------------------------------------------

We introduce a *distance* between two cells,
:math:`c_k` and :math:`c_\ell`,
as a L1 norm between the two cell contact surface vectors,
paired by :math:`P`, and
normalized by the sum of the two cell surfaces.

.. math::
   d(c_k, c_\ell ; P)  = \frac{\sum_{(k_i, \ell_j) \in P} |s_{k,k_i} - s_{\ell,\ell_j} |}{\sum_{i_k} s_{k,i_k} +
   \sum_{j_\ell} s_{\ell,j_\ell}}
   :label: eq:cell-naming-distance

Because of the normalization, this is not a distance in a mathematical
sense (the triangular inequality is not verified). On the other
hand, thanks  to the normalization this distance lives
in :math:`[0, 1]`.
When embryos are named, the pairing :math:`P` is built after the neighboring cell names.



.. _cli-gallery-cell-to-cell-distance:

Cell-to-cell distance along branches
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For a given embryo, we can compute the distance between the same cell at
two consecutive time points.


Running

.. code-block:: bash

    ascidian_embryo -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    generate_figure = ['cell-to-cell-distance-along-branch']

generates a ``python`` file allowing to generate the following figure,

.. _fig_cell_distance_along_branch:
.. figure:: ./latex/figures/cell_distance_along_branch_13.png
   :alt: Cell-to-cell distance along a branch
   :width: 500px

   Cell-to-cell distances along branches, from thhe cell appearance
   (after a division) to its disappearance (when the cell
   divides, or at the end of the time series).

It demonstrates that this distance stabilizes a few timepoints
after the division.



.. _cli-gallery-cell-to-cell-distance-histogram:

Cell-to-cell distance for same, sister or other cells
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To assess whether the contact surfaces can be considered as a aignature
of a cell, we compute the distances between the same cell (cell of
same name) between embryos, or
between a cell and its sister cells (mostly inter-embryo, but one measure is
within the same embryo),
or between a cell and other (than the same and the sister) cells.

Running

.. code-block:: bash

    ascidian_naming_propagation -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    add_symmetric_embryo = True
    generate_figure = ['cell-to-cell-distance-histogram']

generates a ``python`` file allowing to generate the following figures. Note
the line ``add_symmetric_embryo = True``: symmetrized embryos are built and included
in the analysis.

.. _fig_cell_distance_histogram_2:
.. figure:: ./latex/figures/cell_distance_histogram_2_14s.png
   :alt: Cell-to-cell distances
   :width: 500px

   Cell-to-cell distance histograms for same cells (cell of same name),
   sister cells and other cells. The peak at 1.0 corresponds to cells
   that do not share any neighbors.

.. _fig_cell_distance_histogram_1:
.. figure:: ./latex/figures/cell_distance_histogram_1_14s.png
   :alt: Cell-to-cell distances
   :width: 500px

   Cell-to-cell distance histograms for same cells (cell of same name)
   and sister cells.



Contact surface based division-to-division distance
---------------------------------------------------

We introduce a *distance* between two divisions, a division
being represented here by a couple or ordered cells,
:math:`(c_k,c_m)` and :math:`(c_\ell, c_n)`.
This distance is built after the cell-to-cell distance,
where the ordered cells are paired (ie
:math:`c_k` and :math:`c_m` are respectively paired with
:math:`c_\ell` and :math:`c_n`)
as a L1 norm between the two cell contact surface vectors of paired cells,
normalized by the sum of the cell surfaces.

.. math::
   d\left( \left(\begin{array}{c} c_k \\ c_m \end{array}\right),
   \left(\begin{array}{c} c_\ell \\ c_n\end{array}\right) ; P_{k,\ell}, P_{m,n}\right)
   = \frac{\sum_{(k_i, \ell_j) \in P_{k,\ell}} |s_{k,k_i} - s_{\ell,\ell_j} |
   + \sum_{(m_i, n_j) \in P_{m,n}} |s_{m,m_i} - s_{n,n_j} |}{\sum_{i_k} s_{k,i_k} +
   \sum_{j_\ell} s_{\ell,j_\ell} + \sum_{i_n} s_{n,i_n} + \sum_{j_m} s_{m,j_m}}

Because of the normalization, this is not a distance in a mathematical
sense (the triangular inequality is not verified). On the other
hand, thanks  to the normalization this distance lives
in :math:`[0, 1]`.
When embryos are named, pairings :math:`P_{k,\ell}` and :math:`P_{m,n}`
are built after the neighboring cell names.



.. _cli-gallery-division-to-division-distance-histogram:

Division-to-division distance for right/wrong cell pairings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The purpose of the division-to-division distance is to
name the daughter cells after a cell division.
In Conklin nomenclature :cite:p:`conklin:1905aa`,
cells are named as :math:`[a,b]p.q[\_,*]`
where :math:`[a,b]` indicates a anterior or posterior position,
:math:`p` denotes the generation,
:math:`q` allows to individualize the cell within a generation,
and :math:`[\_,*]`  indicates whether the cell is in the left or right hemisphere.
When dividing, the :math:`[a,b]` and :math:`[\_,*]` descriptors are kept
from the mother cell name to the daughter cell, and
the descriptors :math:`[p,q]` yield a couple of descriptors
:math:`[p+1,2q-1]` and :math:`[p+1,2q]`, where
:math:`p+1` denotes the next generation.
Then, naming the two daughter cells after a cell division comes to decide
which one will inherit from the :math:`2q-1` identifier
and which one  will inherit from the :math:`2q` identifier.

To assess whether the contact surfaces can distinguish between
right and wrong pairings of the daughter cells (after) division,
we compute the distances between the same division (across embryos)
with either the right pairing of daughter cells, or the wrong one.

Running

.. code-block:: bash

    ascidian_naming_propagation -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    add_symmetric_embryo = True
    generate_figure = ['division-to-division-distance-histogram']

generates a ``python`` file allowing to generate the following figures. Note
the line ``add_symmetric_embryo = True``: symmetrized embryos are built and included
in the analysis.

.. _fig_division_distance_histogram_1:
.. figure:: ./latex/figures/division_distance_histogram_1_15.png
   :alt: Division-to-division distances
   :width: 500px

   Division-to-division distance histograms.
   Distances are computed between the division of the same cell
   across atlases, while making the right pairing of daughter cells
   or the wrong ones.

.. _fig_division_distance_histogram_2:
.. figure:: ./latex/figures/division_distance_histogram_2_15.png
   :alt: Division-to-division distances
   :width: 500px

   Division-to-division distance histograms.
   Distances are computed between the division of the same cell
   across atlases, while making the right pairing of daughter cells
   or the wrong ones. One histogram is done per generation.


.. _fig_division_distance_histogram_3:
.. figure:: ./latex/figures/division_distance_histogram_3_15.png
   :alt: Division-to-division distances
   :width: 500px

   Division-to-division distance difference histograms.
   This is the difference between the distance with a wrong pairing
   minus the distance with the right pairing.



Dendrograms from division-to-division distances
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Dendrograms <https://en.wikipedia.org/wiki/Dendrogram>`_ can be used
to represent a hierarchical clustering of the divisions (issued from several embryos),
clustering that can be based on the division-to-division distances
(see also
`Hierarchical clustering <https://docs.scipy.org/doc/scipy/reference/cluster.hierarchy.html>`_
from scipy).

Running

.. code-block:: bash

    ascidian_naming_propagation -p parameters-atlas.py

with the following parameter file

.. code-block:: python

    atlasEmbryos = ['Pm1.pkl', 'Pm3.pkl', 'Pm4.pkl', 'Pm5.pkl', 'Pm7.pkl', 'Pm8.pkl', 'Pm9.pkl']
    add_symmetric_embryo = True
    generate_figure = ['division-dendrograms']

generates a ``python`` file, here named ``figure_division_dendrogram.py``
allowing to generate the dendrograms. Note
the line ``add_symmetric_embryo = True``: symmetrized embryos are built and included
in the analysis.

Running

.. code-block:: bash

    python figure_division_dendrogram.py

will generate dendrograms for all divisions, which may be pretty long. Alternatively, dendrograms
for chosen divisions may be generated by giving cell names as arguments as in

.. code-block:: bash

    python figure_division_dendrogram.py 'b7.0002_' 'a7.0002_'

There are four figures generated by division, here are the ones for the division of cell ``'b7.0002_'``:

* ``'division_dendrogram_b7_0002U_HC028_BAL008_single.png'``: dendrograms of cell divisions, the cost function being the
  division-to-dision distance.
  ``b7_0002U`` represents the cell name (``U`` stands for ``_`` (ie right cells), ``S`` for ``*`` ()ie left cells)
  In the dendrogram, embryos prefixed by ``'sym-'`` designated the symmetric cell (cell from the other hemisphere)

* ``'division_dendrogram_b7_0002U_HC028_BAL008_single_SW.png'``: id as
  ``'division_dendrogram_b7_0002U_HC028_BAL008_single.png'`` but with additional
  (fake) divisions are added, where the daughter names are switched (prefixed by switched).
  It allows to assess whether daughter name couples are consistent.

* ``'division_dendrogram_HC028_b7_0002U_single.png'``: id as
  ``'division_dendrogram_b7_0002U_HC028_BAL008_single.png'``, only the name has changed.
  The prefix ``'HC028'`` indicates the highest value of the dendrogram (the higher, the more likely there are
  several division geometry modes).

* ``'division_dendrogram_HC028_b7_0002U_single_SW.png'``:
  id as ``'division_dendrogram_b7_0002U_HC028_BAL008_single_SW.png'``

.. _fig_division_dendrogram_b7_0002U_HC028_BAL008_single:
.. figure:: ./latex/figures/division_dendrogram_b7_0002U_HC028_BAL008_single_16.png
   :alt: Cell division dendrogram of cell ``b7.0002_``
   :width: 500px

   Cell division dendrogram of cell ``b7.0002_``

.. _fig_division_dendrogram_b7_0002U_HC028_BAL008_single_SW:
.. figure:: ./latex/figures/division_dendrogram_b7_0002U_HC028_BAL008_single_16_SW.png
   :alt: Cell division dendrogram of cell ``b7.0002_``, with fake switched daughters division
   :width: 500px

   Cell division dendrogram of cell ``b7.0002_``, with fake switched daughters division

In addition, to the cell division dendrograms, some other figures are generated

.. _fig_division_dendrogram_merge_histogram_single:
.. figure:: ./latex/figures/division_dendrogram_merge_histogram_single_16.png
   :alt: Histograms of merge and last merge values
   :width: 500px

   Histograms of merge and last merge values. The higher the last merge value,
   the more separated are the two last clusters.

.. _fig_division_dendrogram_balance_histogram_single:
.. figure:: ./latex/figures/division_dendrogram_balance_histogram_single_16.png
   :alt: Histogram of balance values
   :width: 500px

   Histogram of balance values. Balance in [0, 1] reflects the balance between the two last cluster.
   1.0 means the two last clusters have the same number of original observations.

.. _fig_division_dendrogram_balance_merge_scatter_single:
.. figure:: ./latex/figures/division_dendrogram_balance_merge_scatter_single_16.png
   :alt: Scatter plot of last merge versus balance values
   :width: 500px

   Scatter plot of large merge versus balance values. A division with both large last merge and balance
   values (with a large size of observation) is more likely to exhibit several division geometry modes.


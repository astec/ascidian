# Change Log

## develop

## version 1.1.2

- update tissue fates
- enrich morphonet file writing: enables more properties to be written out

## version 1.1.1

- add __init__.py  in  src/ascidian/components

## version 1.1.0

- division variation assessment
- morphonet name map
- embryo naming with graph matching
- optimization of winged contact cell signature
- optimization of symmetry axis computation with winged contacts

## version 1.0.0

- major refactoring
- add naming timepoint by assignment
- correct wrong behavior in name initialization when the requested cell count is not found
- correct bug on cell fate computation
- enrich diagnosis on cell names

## version 0.3.1

- vector distribution, correct handling of file suffix in figure()

## version 0.3.0

- documentation updating
- refactoring symmetry related code; vector distribution dedicated code
- pursued code splitting into astec/ascidian repositories
- documentation splitting into astec/ascidian

## version 0.2.0

- refactor setup.py (adding setup.cfg)
- test infrastructure

## version 0.1.0

- renamed ascidians -> ascidian

## version 0.0.0

- first release: get sources from astec/ascidian and astec/algoatlas
